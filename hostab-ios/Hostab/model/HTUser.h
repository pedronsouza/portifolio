//
//  HTUser.h
//  Hostab
//
//  Created by Pedro Souza on 20/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTUser : NSObject<NSCoding>

// Ex: "company_id" = 1;
@property (strong, nonatomic) NSNumber *companyId;

// Ex: "created_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *createdAt;

// Ex: id = 1;
@property (strong, nonatomic) NSNumber *userId;

// Ex: name = "Lori Linda"
@property (strong, nonatomic) NSString *name;

// Ex: password = 74235
@property (strong, nonatomic) NSString *accessCode;

// Ex: room = 1;
@property (strong, nonatomic) NSNumber *room;

// Ex: "selected_locale" = "pt-BR";
@property (strong, nonatomic) NSString *selectedLocale;

// Ex: "updated_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *updatedAt;

+ (id)userWithProperties:(NSDictionary *)properties;

@end
