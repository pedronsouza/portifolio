//
//  HTYahooWeatherEngine.m
//  CompanionHotel
//
//  Created by Pedro Souza on 19/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTYahooWeatherEngine.h"

#define HT_YQL_WEATHER_QUERY(woeid, unit) [NSString stringWithFormat:@"select item.condition from weather.forecast where woeid=%@ and u='%@'", woeid, unit]

#define HT_YQL_WEATHER_URL(query) [NSString stringWithFormat:@"v1/public/yql?q=%@&format=json", query]

@implementation HTYahooWeatherEngine

- (id)initWithWOEID:(NSString *)woeid
            andUnit:(NSString *)temperatureUnit {
    
    self = [super initWithHostName:@"query.yahooapis.com"
                customHeaderFields:nil];
    
    self.WOEID = woeid;
    self.unit = temperatureUnit;
    
    return self;
}

- (MKNetworkOperation*)getWeather:(HTWeatherResponseBlock)completionBlock
                          onError:(MKNKErrorBlock)errorBlock {
    
    NSString *query = HT_YQL_WEATHER_QUERY(self.WOEID, self.unit);
    NSString *path = HT_YQL_WEATHER_URL([query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:nil
                                          httpMethod:@"GET"];
    NSLog(@"yql weather: %@", op.url);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        NSLog(@"yql result: %@", [completedOperation responseString]);
        
        NSDictionary *weatherResponse = [completedOperation responseJSON];
        NSDictionary *condition = [weatherResponse valueForKeyPath:@"query.results.channel.item.condition"];
        
        condition = ([condition isKindOfClass:[NSNull class]] ? nil : condition);
        
        completionBlock(condition);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)getIconForCondition:(NSNumber *)code
                              onCompletion:(MKNKImageBlock)completionBlock
                                   onError:(MKNKErrorBlock)errorBlock {
    
    // Get hour component of current date
    NSDateComponents *nowComponents = [[NSCalendar currentCalendar] components:NSHourCalendarUnit
                                                                      fromDate:[NSDate date]];
    
    // Determine if the icon variation will be Day or Night
    NSString *iconVariation = (nowComponents.hour >= 6 && nowComponents.hour < 17) ? @"d" : @"n";
    
    // Build the weather icon URL according to condition code and variation
    NSString *path = [NSString stringWithFormat:@"http://l.yimg.com/os/mit/media/m/weather/images/icons/l/%@%@-100567.png", code, iconVariation];
    
    MKNetworkOperation *op = [self operationWithURLString:path];
    NSLog(@"icon path: %@", op.url);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        UIImage *weatherIcon = [completedOperation responseImage];
        completionBlock(weatherIcon, nil, NO);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
