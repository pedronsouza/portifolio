//
//  HTFoodModel.h
//  Hostab
//
//  Created by Pedro Souza on 11/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTResponseBlock.h"
#import "HTFood.h"
#import "HTFoodType.h"

@interface HTFoodModel : NSObject

- (MKNetworkOperation *)getAllFoodTypes:(HTArrayBlock)completionBlock
                                onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)getFoodsWithType:(NSNumber *)foodType
                            onCompletion:(HTArrayBlock)completionBlock
                                 onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)getFoodImageAtURL:(NSString *)url
                        completionHandler:(MKNKImageBlock)imageFetchedBlock
                             errorHandler:(MKNKResponseErrorBlock)errorBlock;
@end
