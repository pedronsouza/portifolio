//
//  HTUser.m
//  Hostab
//
//  Created by Pedro Souza on 20/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTUser.h"
#import "NSDate+HTUtils.h"

@implementation HTUser

+ (id)userWithProperties:(NSDictionary *)properties {
    HTUser *user = [[HTUser alloc] init];
    
    if (user) {
        user.companyId = properties[@"company_id"];
        user.createdAt = [NSDate dateFromRFC3339:properties[@"created_at"]];
        user.userId = properties[@"id"];
        user.name = properties[@"name"];
        user.accessCode = properties[@"password"];
        user.room = properties[@"room"];
        user.selectedLocale = properties[@"selected_locale"];
        user.updatedAt = [NSDate dateFromRFC3339:properties[@"updated_at"]];
    }
    
    return user;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.companyId = [decoder decodeObjectForKey:@"company_id"];
        self.createdAt = [decoder decodeObjectForKey:@"created_at"];
        self.userId = [decoder decodeObjectForKey:@"id"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.accessCode = [decoder decodeObjectForKey:@"password"];
        self.room = [decoder decodeObjectForKey:@"room"];
        self.selectedLocale = [decoder decodeObjectForKey:@"selected_locale"];
        self.updatedAt = [decoder decodeObjectForKey:@"updated_at"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.companyId forKey:@"company_id"];
    [encoder encodeObject:self.createdAt forKey:@"created_at"];
    [encoder encodeObject:self.userId forKey:@"id"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.accessCode forKey:@"password"];
    [encoder encodeObject:self.room forKey:@"room"];
    [encoder encodeObject:self.selectedLocale forKey:@"selected_locale"];
    [encoder encodeObject:self.updatedAt forKey:@"updated_at"];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ id: %@, name: %@, room: %@", [super description], self.userId, self.name, self.room];
}

@end
