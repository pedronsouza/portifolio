//
//  HTFood.m
//  Hostab
//
//  Created by Pedro Souza on 13/05/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTFood.h"
#import "NSDate+HTUtils.h"

@implementation HTFood

/* SAMPLE
 {
     "created_at" = "2013-05-08T01:26:32Z";
     description = "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equaliza\U00e7\U00e3o das condi\U00e7\U00f5es financeiras e administrativas exigidas. Pensando mais a longo prazo";
     "food_images" =         (
         {
             "created_at" = "2013-05-08T02:14:23Z";
             "plate_content_type" = "image/jpeg";
             "plate_file_name" = "maior-hamburguer-do-mundo.jpg";
             "plate_file_size" = 836252;
             "plate_image_url" = "http://hostab-food-images.s3.amazonaws.com/food_images/plates/000/000/003/maior-hamburguer-do-mundo.jpg?1367979262";
         }
     );
     "food_type_id" = 1;
     id = 1;
     locale = "pt-BR";
     price = "29.99";
     title = "Hamburg\U00e3o LeLek";
     "updated_at" = "2013-05-08T01:26:32Z";
 }
 */
+ (id)foodWithProperties:(NSDictionary *)properties {
    HTFood *food = [[HTFood alloc] init];
    
    if (food) {
        food.createdAt = [NSDate dateFromRFC3339:[properties objectForKey:@"created_at"]];
        food.foodDescription = [properties objectForKey:@"description"];
        if (![[properties objectForKey:@"food_images"] isKindOfClass:[NSNull class]]) {
            
            food.imageURLs = [[NSMutableArray alloc] initWithArray:[[properties objectForKey:@"food_images"]
                                                                     valueForKeyPath:@"@distinctUnionOfObjects.plate_image_url"]];
            
            
        }

        food.foodTypeId = [properties objectForKey:@"food_type_id"];
        food.foodId = [properties objectForKey:@"id"];
        food.locale = [properties objectForKey:@"locale"];
        food.price = [properties objectForKey:@"price"];
        food.title = [properties objectForKey:@"title"];
        food.updatedAt = [NSDate dateFromRFC3339:[properties objectForKey:@"updated_at"]];
    }
    
    return food;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ id: %@, title: %@", [super description], self.foodTypeId, self.title];
}

@end
