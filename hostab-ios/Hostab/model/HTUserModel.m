//
//  HTUserModel.m
//  Hostab
//
//  Created by Pedro Souza on 20/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTUserModel.h"
#import "HTBackendEngine.h"

#define HT_USER_KEY @"HT_USER_KEY"

@interface HTUserModel ()

@end

@implementation HTUserModel

+ (id)sharedInstance {
    
    static dispatch_once_t pred = 0;
    __strong static HTUserModel *_sharedObject = nil;
    
    dispatch_once(&pred, ^{
        
        _sharedObject = [[super alloc] init];
        
    });
    
    return _sharedObject;
}

- (MKNetworkOperation *)authWithCode:(NSString *)accessCode
                        onCompletion:(HTObjectBlock)completionBlock
                             onError:(HTErrorBlock)errorBlock {
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    MKNetworkOperation *nkOperation = nil;
    
    // Declare the completion block in advance
    HTResponseBlock authComplete = ^(NSDictionary *dict) {
        
        // Trigger a KVO notification and create an user from its props
        [self willChangeValueForKey:@"currentUser"];
        _currentUser = [HTUser userWithProperties:dict];
        [self didChangeValueForKey:@"currentUser"];
        
        [self saveCurrentUser];
         
        // Invoke the completion block with the created user
        completionBlock(_currentUser);
    };
    
    HTErrorBlock authFailure = ^(NSError *error) {
        
        // Check if we got an error due to an invalid access code
        if (error.code == 404) {
            
            NSString *message = @"O código de acesso digitado não é válido. Verifique se o código de acesso foi digitado corretamente ou procure a recepção para mais informações.";
            NSDictionary *errorInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                       message, NSLocalizedDescriptionKey, nil];
            
            NSError *newError = [NSError errorWithDomain:@"br.com.hostab.apierror"
                                                 code:error.code
                                             userInfo:errorInfo];
            
            errorBlock(newError);
            
        } else {
            
            errorBlock(error);
        }
    };
    
    // Build the network operation
    nkOperation = [backend authWithCode:accessCode
                           onCompletion:authComplete
                                onError:authFailure];
    
    return nkOperation;
}

- (void)logout {
    
    // Trigger a KVO notification and discard the current user
    [self willChangeValueForKey:@"currentUser"];
    _currentUser = nil;
    [self didChangeValueForKey:@"currentUser"];
    
    [self saveCurrentUser];
}

- (HTUser *)loadPreviousUser {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedUser = [defaults objectForKey:HT_USER_KEY];
    HTUser *previousUser = nil;
    
    if (encodedUser) {
        previousUser = (HTUser *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedUser];
    }
    
    NSLog(@"Loaded previous user: %@", previousUser);
    
    return previousUser;
}

- (void)saveCurrentUser {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"Saving current user: %@", _currentUser);
    
    if (_currentUser) {
        
        NSData *encodedUser = [NSKeyedArchiver archivedDataWithRootObject:_currentUser];
        
        [defaults setObject:encodedUser forKey:HT_USER_KEY];
        
    } else {
        
        [defaults removeObjectForKey:HT_USER_KEY];
    }
    
    [defaults synchronize];
}

@end
