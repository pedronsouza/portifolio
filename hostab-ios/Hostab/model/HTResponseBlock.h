//
//  HTResponseBlock.h
//  Hostab
//
//  Created by Pedro Souza on 11/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

typedef void (^HTObjectBlock)(NSObject *object);
typedef void (^HTArrayBlock)(NSArray *array);
typedef void (^HTSimpleBlock)(void);
typedef void (^HTErrorBlock)(NSError *error);