//
//  HTClientSettings.h
//  CompanionHotel
//
//  Created by Pedro Souza on 19/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>


#define HTClientSettingsKeyWeatherID             @"WEATHER_ID"
#define HTClientSettingsKeyAccessToken           @"ACCESS_TOKEN"
#define HTClientSettingsKeyHotelName             @"HOTEL_NAME"
#define HTClientSettingsKeyMinimumDeliveryWait   @"MINIMUM_DELIVERY_WAIT"
#define HTClientSettingsKeyBoxColorBottom        @"UI_BOX_COLOR_GRADIENT_BOTTOM"
#define HTClientSettingsKeyBoxColorTop           @"UI_BOX_COLOR_GRADIENT_TOP"
#define HTClientSettingsKeyAccentColor           @"UI_ACCENT_COLOR"
#define HTClientSettingsKeyBgTextColor           @"UI_BG_TEXT_COLOR"
#define HTClientSettingsKeyBtnTextColor          @"UI_BTN_TEXT_COLOR"
#define HTClientSettingsKeyContrastBtnTextColor  @"UI_CONTRAST_BTN_TEXT_COLOR"
#define HTClientSettingsKeyToolbarItemTintColor  @"UI_TOOLBARITEM_TINTCOLOR"
#define HTClientSettingsKeyTopTextColor          @"UI_TOP_TEXT_COLOR"

@interface HTClientSettings : NSObject

/// Get the unique instance of the client settings
+ (id)sharedInstance;

/// Get a setting stored under the given key
- (id)settingForKey:(NSString *)keyName;

/// Get a color stored under the given key
- (UIColor *)colorForKey:(NSString *)keyName;

@end
