//
//  HTBackendEngine.h
//  Hostab
//
//  Created by Pedro Souza on 02/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "MKNetworkEngine.h"

/// Generic response block definition
typedef void (^HTResponseBlock)(id response);

/*!
 *  @class HTBackendEngine
 *  @abstract The network engine that access the Hostab backend.
 */
@interface HTBackendEngine : MKNetworkEngine


/*!
 *  @abstract Returns the singleton backend network engine instance. 
 */
+ (id)sharedInstance;

/*!
 *  @abstract Returns a network operation authenticates a user with an access code.
 *
 *  @discussion This operation connects to the endpoint /api/v1/auth
 *
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation *)authWithCode:(NSString *)accessCode
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock;
/*!
 *  @abstract Returns a network operation that fetches all food types.
 *
 *  @discussion This operation connects to the endpoint /api/v1/food_types
 *
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)foodTypes:(HTResponseBlock)completionBlock
                         onError:(MKNKErrorBlock)errorBlock;

/*!
 *  @abstract Returns a network operation that fetches all foods for a given type.
 *
 *  @discussion This operation connects to the endpoint /api/v1/food_types/:food_type_id/foods
 *
 *  @param foodTypeId      The ID of the food type
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)foodsWithType:(NSNumber *)foodTypeId
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock;

/*!
 *  @abstract Returns a network operation that fetches a food for a given type and ID.
 *
 *  @discussion This operation connects to the endpoint /api/v1/food_types/:food_type_id/foods/:id
 *
 *  @param foodTypeId      The ID of the food type
 *  @param foodId          The ID of the desired food 
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)foodsWithType:(NSNumber *)foodTypeId
                               andId:(NSNumber *)foodId
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock;

/*!
 *  @abstract Returns a network operation that fetches an user orders.
 *
 *  @discussion This operation connects to the endpoint users/:user_id/orders
 *
 *  @param userId          The ID of the user
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)ordersForUser:(NSNumber *)userId
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock;

/*!
 *  @abstract Returns a network operation that create a new order for an user.
 *
 *  @discussion This operation connects to the endpoint users/:user_id/orders
 *
 *  @param userId          The ID of the user
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)postOrderForUser:(NSNumber *)userId
                           onCompletion:(HTResponseBlock)completionBlock
                                onError:(MKNKErrorBlock)errorBlock;
/*!
 *  @abstract Returns a network operation that create an user's order.
 *
 *  @discussion This operation connects to the endpoint users/:user_id/orders/:order_id
 *
 *  @param userId          The ID of the user
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)putOrder:(NSNumber *)orderId
                        forUser:(NSNumber *)userId
                     withStatus:(NSNumber *)status
                         atTime:(NSDate *)deliveryTime
                     atLocation:(NSNumber *)deliveryLocation
                   onCompletion:(HTResponseBlock)completionBlock
                        onError:(MKNKErrorBlock)errorBlock;

/*!
 *  @abstract Returns a network operation that adds an item to an order.
 *
 *  @discussion This operation connects to the endpoint orders/:order_id/order_items
 *
 *  @param orderId         The ID of the order
 *  @param foodId          The ID of the food
 *  @param amount          The quantity of the item 
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)postItemForOrder:(NSNumber *)orderId
                               withFood:(NSNumber *)foodId
                                 amount:(NSNumber *)amount
                                   note:(NSString *)note
                           onCompletion:(HTResponseBlock)completionBlock
                                onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation*)putItemForOrder:(NSNumber *)orderId
                              withItem:(NSNumber *)orderItemId
                                  food:(NSNumber *)foodId
                                amount:(NSNumber *)amount
                                  note:(NSString *)note
                          onCompletion:(HTResponseBlock)completionBlock
                               onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation*)deleteItemForOrder:(NSNumber *)orderId
                                 withItem:(NSNumber *)orderItemId
                             onCompletion:(HTResponseBlock)completionBlock
                                  onError:(MKNKErrorBlock)errorBlock;

/*!
 *  @abstract Returns a network operation that fetches all delivery locations
 *
 *  @discussion This operation connects to the endpoint XXXX
 *
 *  @param completionBlock The block that will be executed if the operation succeeds
 *  @param errorBlock      The block that will be executed if the operation fails
 *
 */
- (MKNetworkOperation*)deliveryLocations:(HTResponseBlock)completionBlock
                                 onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getImageAtURL:(NSString *)url
                    completionHandler:(MKNKImageBlock)imageFetchedBlock
                         errorHandler:(MKNKResponseErrorBlock)errorBlock;

@end

