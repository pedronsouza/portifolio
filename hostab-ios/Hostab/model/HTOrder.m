//
//  HTOrder.m
//  Hostab
//
//  Created by Pedro Souza on 22/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTOrder.h"
#import "HTBackendEngine.h"
#import "HTUserModel.h"
#import "HTDeliveryLocation.h"
#import "NSDate+HTUtils.h"

@interface HTOrder ()

@property (strong, nonatomic) NSMutableArray *itemList;

@end

@implementation HTOrder

- (NSArray *)allItems {
    return [self.itemList copy];
}

- (float)totalValue {
    NSNumber *total = [self.itemList valueForKeyPath:@"@sum.totalValue"];
    
    return [total floatValue];
}

- (id)init {
    self = [super init];
    
    if (self) {
        self.itemList = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (MKNetworkOperation *)createOrder:(HTSimpleBlock)completionBlock
                            onError:(HTErrorBlock)errorBlock {
    
    HTUserModel *userModel = [HTUserModel sharedInstance];
    HTUser *currentUser = [userModel currentUser];
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    HTResponseBlock responseBlock = ^(id response) {
        
        self.createdAt = [NSDate dateFromRFC3339:response[@"created_at"]];
        self.orderId = response[@"id"];
        self.status = response[@"status"];
        self.updatedAt = [NSDate dateFromRFC3339:response[@"updated_at"]];
        
        completionBlock();
    };
    
    MKNetworkOperation *op = [backend postOrderForUser:currentUser.userId
                                          onCompletion:responseBlock
                                               onError:errorBlock];
    
    return op;
}

- (MKNetworkOperation *)finishOrder:(HTSimpleBlock)completionBlock
                            onError:(HTErrorBlock)errorBlock {
    
    if (!self.orderId) {
        
        NSString *message = @"O pedido não foi criado. Insira ao menos um item no pedido para criá-lo.";
        
        NSDictionary *errorInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   message, NSLocalizedDescriptionKey, nil];
        
        NSError *newError = [NSError errorWithDomain:@"br.com.hostab.apierror"
                                                code:5000
                                            userInfo:errorInfo];
        
        errorBlock(newError);
        
        return nil;
    }
    
    if (![self.itemList count]) {
        
        NSString *message = @"Seu pedido não tem itens. Insira itens no pedido antes finalizar.";
        
        NSDictionary *errorInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   message, NSLocalizedDescriptionKey, nil];
        
        NSError *newError = [NSError errorWithDomain:@"br.com.hostab.apierror"
                                                code:5001
                                            userInfo:errorInfo];
        
        errorBlock(newError);
        
        return nil;
    }
    
    HTUserModel *userModel = [HTUserModel sharedInstance];
    HTUser *currentUser = [userModel currentUser];
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    HTResponseBlock responseBlock = ^(id response) {
        
        self.createdAt = [NSDate dateFromRFC3339:response[@"created_at"]];
        self.orderId = response[@"id"];
        self.status = response[@"status"];
        self.updatedAt = [NSDate dateFromRFC3339:response[@"updated_at"]];
        
        completionBlock();
    };
    
    MKNetworkOperation *op = [backend putOrder:self.orderId
                                       forUser:currentUser.userId
                                    withStatus:@(HTOrderStatusProcessing)
                                        atTime:self.deliveryTime
                                    atLocation:self.deliveryLocation.deliveryLocationId
                                  onCompletion:responseBlock
                                       onError:errorBlock];
    
    return op;
}

- (MKNetworkOperation *)cancelOrder:(HTSimpleBlock)completionBlock
                            onError:(HTErrorBlock)errorBlock {
    
    HTUserModel *userModel = [HTUserModel sharedInstance];
    HTUser *currentUser = [userModel currentUser];
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    HTResponseBlock responseBlock = ^(id response) {
        
        self.createdAt = [NSDate dateFromRFC3339:response[@"created_at"]];
        self.orderId = response[@"id"];
        self.status = response[@"status"];
        self.updatedAt = [NSDate dateFromRFC3339:response[@"updated_at"]];
        
        completionBlock();
    };
    
    MKNetworkOperation *op = [backend putOrder:self.orderId
                                       forUser:currentUser.userId
                                    withStatus:@(HTOrderStatusCanceled)
                                        atTime:self.deliveryTime
                                    atLocation:self.deliveryLocation.deliveryLocationId
                                  onCompletion:responseBlock
                                       onError:errorBlock];
    
    return op;
}

- (MKNetworkOperation *)addItem:(HTOrderItem *)item
                   onCompletion:(HTSimpleBlock)completionBlock
                        onError:(HTErrorBlock)errorBlock {
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    HTResponseBlock responseBlock = ^(id response) {

        item.createdAt = [NSDate dateFromRFC3339:response[@"created_at"]];
        item.orderItemId = response[@"id"];
        item.updatedAt = [NSDate dateFromRFC3339:response[@"updated_at"]];
        
        [self.itemList addObject:item];
        
        completionBlock();
    };
    
    MKNetworkOperation *op = [backend postItemForOrder:self.orderId
                                              withFood:item.food.foodId
                                                amount:@(item.quantity)
                                                  note:item.note
                                          onCompletion:responseBlock
                                               onError:errorBlock];
    
    return op;
}

- (MKNetworkOperation *)updateItem:(HTOrderItem *)item
                      onCompletion:(HTSimpleBlock)completionBlock
                           onError:(HTErrorBlock)errorBlock {

    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    HTResponseBlock responseBlock = ^(id response) {
        
        item.updatedAt = [NSDate dateFromRFC3339:response[@"updated_at"]];
        
        completionBlock();
    };
    
    MKNetworkOperation *op = [backend putItemForOrder:self.orderId
                                             withItem:item.orderItemId
                                                 food:item.food.foodId
                                               amount:@(item.quantity)
                                                 note:item.note
                                         onCompletion:responseBlock
                                              onError:errorBlock];
    
    return op;
}

- (MKNetworkOperation *)removeItem:(HTOrderItem *)item
                      onCompletion:(HTSimpleBlock)completionBlock
                           onError:(HTErrorBlock)errorBlock {
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    HTResponseBlock responseBlock = ^(id response) {
        [self.itemList removeObject:item];
        completionBlock();
    };
    
    MKNetworkOperation *op = [backend deleteItemForOrder:self.orderId
                                                withItem:item.orderItemId
                                            onCompletion:responseBlock
                                                 onError:errorBlock];
    
    return op;
}

- (MKNetworkOperation *)allDeliveryLocations:(HTArrayBlock)completionBlock
                                     onError:(HTErrorBlock)errorBlock {
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    HTResponseBlock responseBlock = ^(id response) {
        
        // Create the objects from their properties retrieved from the JSON
        NSMutableArray *locationList = [[NSMutableArray alloc] initWithCapacity:[response count]];
        for (NSDictionary *objectProps in response) {
            
            HTDeliveryLocation *location = [HTDeliveryLocation deliveryLocationWithProperties:objectProps];
            [locationList addObject:location];
        }
        
        // Invoke the completion block with the created objects
        completionBlock(locationList);
    };
    
    MKNetworkOperation *op = [backend deliveryLocations:responseBlock
                                                onError:errorBlock];
    
    return op;
}

- (HTOrderItem *)itemWithFood:(HTFood *)food {
    HTOrderItem *item = nil;
    
    for (HTOrderItem *listItem in self.itemList) {
        if ([listItem.food.foodId isEqualToNumber:food.foodId]) {
            item = listItem;
            break;
        }
    }
    
    return item;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ id: %@, status: %@, numItems: %i, totalValue: %.2f", [super description], self.orderId, self.status, [self.itemList count], self.totalValue];
}

@end
