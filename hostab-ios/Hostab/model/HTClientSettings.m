//
//  HTClientSettings.m
//  CompanionHotel
//
//  Created by Pedro Souza on 19/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTClientSettings.h"

@interface HTClientSettings ()

/// The settings stored on the Client.plist file
@property (strong, nonatomic) NSDictionary *settings;

@end

@implementation HTClientSettings

+ (id)sharedInstance {
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    
    dispatch_once(&pred, ^{
        _sharedObject = [[super alloc] init];
        
    });
    
    return _sharedObject;
}

- (id)settingForKey:(NSString *)keyName {
    if (!self.settings) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"ClientSettings" ofType:@"plist"];
        self.settings = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    
    return [self.settings objectForKey:keyName];
}

- (UIColor *)colorForKey:(NSString *)keyName {
    NSString *colorHex = [self settingForKey:keyName];
    UIColor *decodedColor = nil;
    
    // The color format that we are using is #RRGGBB
    if ([colorHex length] == 7) {
        const char *colorStr = [colorHex cStringUsingEncoding:NSASCIIStringEncoding];
        
        // Ignore the # char
        colorStr++;
        
        int red = 0;
        int green = 0;
        int blue = 0;
        
        // Extract each component
        sscanf(colorStr, "%2x%2x%2x", &red, &green, &blue);
        
        // Build the UIColor instance
        decodedColor = [UIColor colorWithRed:(CGFloat)red / 255.0f
                                       green:(CGFloat)green / 255.0f
                                        blue:(CGFloat)blue / 255.0f
                                       alpha:1.0f];

    } else {
        decodedColor = [UIColor purpleColor];
    }
    
    return decodedColor;
}

@end
