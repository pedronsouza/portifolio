//
//  UITextField+HTUtils.m
//  Hostab
//
//  Created by Pedro Souza on 27/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "UITextField+HTUtils.h"

@implementation UITextField (HTUtils)

- (void)selectAllText {
    UITextRange *allText = [self textRangeFromPosition:self.beginningOfDocument toPosition:self.endOfDocument];
    
    [self setSelectedTextRange:allText];
}

@end
