//
//  NSDate+HTUtils.m
//  Hostab
//
//  Created by Pedro Souza on 13/05/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "NSDate+HTUtils.h"

@interface NSDate (HTUtils_Private)

+ (NSDateFormatter *)RFC3339DateFormatter;

@end

@implementation NSDate (HTUtils)

+ (NSDate *)dateFromRFC3339:(NSString *)value {

    // Returns a user-visible date time string that corresponds to the
    // specified RFC 3339 date time string. Note that this does not handle
    // all possible RFC 3339 date time strings, just one of the most common
    // styles.
    
    NSDateFormatter *sRFC3339DateFormatter = [NSDate RFC3339DateFormatter];
    NSDate *date;
    
    // Convert the RFC 3339 date time string to an NSDate.
    // Then convert the NSDate to a user-visible date string.
    date = [sRFC3339DateFormatter dateFromString:value];
    
    return date;
}

- (NSString *)toRFC3339String {
    // Returns a user-visible date time string that corresponds to the
    // specified RFC 3339 date time string. Note that this does not handle
    // all possible RFC 3339 date time strings, just one of the most common
    // styles.
    
    NSDateFormatter *sRFC3339DateFormatter = [NSDate RFC3339DateFormatter];
    NSString *value;
    
    // Convert the RFC 3339 date time string to an NSDate.
    // Then convert the NSDate to a user-visible date string.
    value = [sRFC3339DateFormatter stringFromDate:self];
    
    return value;
}

- (NSString *)timeString {
    
    static NSDateFormatter *timeDateFormatter;

    // If the date formatters aren't already set up, do that now and cache them
    // for subsequence reuse.
    if (timeDateFormatter == nil) {
        timeDateFormatter = [[NSDateFormatter alloc] init];
        timeDateFormatter.dateFormat = @"HH':'mm";
    }
    
    NSString *value = [timeDateFormatter stringFromDate:self];
    
    return value;
}

#pragma mark - Class extension methods

+ (NSDateFormatter *)RFC3339DateFormatter {
    
    static NSDateFormatter *sRFC3339DateFormatter;
    
    // If the date formatters aren't already set up, do that now and cache them
    // for subsequence reuse.
    
    if (sRFC3339DateFormatter == nil) {
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        assert(enUSPOSIXLocale != nil);
        
        sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
        assert(sRFC3339DateFormatter != nil);
        
        [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
        [sRFC3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    }
    
    return sRFC3339DateFormatter;
}

@end
