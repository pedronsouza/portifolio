//
//  NSDate+HTUtils.h
//  Hostab
//
//  Created by Pedro Souza on 13/05/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (HTUtils)

+ (NSDate *)dateFromRFC3339:(NSString *)value;

- (NSString *)toRFC3339String;

- (NSString *)timeString;

@end
