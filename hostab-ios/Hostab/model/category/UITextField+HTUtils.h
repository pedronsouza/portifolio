//
//  UITextField+HTUtils.h
//  Hostab
//
//  Created by Pedro Souza on 27/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (HTUtils)

/*!
 *  @abstract Select all text from the sender.
 */
- (void)selectAllText;

@end
