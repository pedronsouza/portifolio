//
//  HTOrderItem.m
//  Hostab
//
//  Created by Pedro Souza on 22/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTOrderItem.h"

@implementation HTOrderItem

- (float)totalValue {
    return [self.food.price floatValue] * self.quantity;
}

- (id)initWithFood:(HTFood *)food andQuantity:(float)quantity {
    self = [super init];
    
    if (self) {
        self.food = food;
        self.quantity = quantity;
    }
    
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ id: %@, food: %@, quantity: %.0f", [super description], self.orderItemId, self.food.title, self.quantity];
}

@end
