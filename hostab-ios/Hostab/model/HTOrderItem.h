//
//  HTOrderItem.h
//  Hostab
//
//  Created by Pedro Souza on 22/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTFood.h"

@interface HTOrderItem : NSObject

// Ex: "created_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *createdAt;

// Ex: "created_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *updatedAt;

@property (strong, nonatomic) NSNumber *orderItemId;

@property (strong, nonatomic) HTFood *food;

@property (assign, nonatomic) float quantity;

@property (strong, nonatomic) NSString *note;

@property (readonly, nonatomic) float totalValue;

- (id)initWithFood:(HTFood *)food andQuantity:(float)quantity;

@end
