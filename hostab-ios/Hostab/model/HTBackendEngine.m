//
//  HTBackendEngine.m
//  Hostab
//
//  Created by Pedro Souza on 02/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTBackendEngine.h"
#import "HTClientSettings.h"
#import "NSDate+HTUtils.h"

// API Hosts
#define HT_BACKEND_DEVELOPMENT  @"hostab-dev.herokuapp.com"
#define HT_BACKEND_PRODUCTION   @""

// API versions path
#define HT_API_PATH_V1          @"api/v1"

// API resources path 
#define HT_RESOURCE_AUTH                     @"auth"
#define HT_RESOURCE_ALLFOODTYPES             @"food_types"
#define HT_RESOURCE_ALLFOODS(type)           [NSString stringWithFormat:@"food_types/%@/foods", type]
#define HT_RESOURCE_FOOD(type, foodId)       [NSString stringWithFormat:@"food_types/%@/foods/%@", type, foodId]
#define HT_RESOURCE_ALLORDERS(userId)        [NSString stringWithFormat:@"users/%@/orders", userId]
#define HT_RESOURCE_ORDER(userId,orderId)    [NSString stringWithFormat:@"users/%@/orders/%@", userId, orderId]
#define HT_RESOURCE_ALLORDERITEMS(orderId)   [NSString stringWithFormat:@"orders/%@/order_items", orderId]
#define HT_RESOURCE_ORDERITEM(orderId, orderItemId)      [NSString stringWithFormat:@"orders/%@/order_items/%@", orderId, orderItemId]
#define HT_RESOURCE_DELIVERYLOCATIONS        @"delivery_locations"

// Configuration of current backend server and API version
#define HT_BACKEND    HT_BACKEND_DEVELOPMENT
#define HT_API_PATH   HT_API_PATH_V1


@implementation HTBackendEngine

+ (id)sharedInstance {
    
    static dispatch_once_t pred = 0;
    __strong static HTBackendEngine *_sharedObject = nil;
    
    dispatch_once(&pred, ^{
        
        NSString *accessToken = [[HTClientSettings sharedInstance] settingForKey:HTClientSettingsKeyAccessToken];
        NSString *tokenToken = [NSString stringWithFormat:@"Token token=\"%@\"", accessToken];
        
        //Authorization: Token token=
        NSString *language = @"pt-BR";
        
        NSDictionary *headerFields = [NSDictionary dictionaryWithObjectsAndKeys:tokenToken, @"authorization", language, @"x-api-locale", nil];
        
        _sharedObject = [[super alloc] initWithHostName:HT_BACKEND
                                                apiPath:HT_API_PATH
                                     customHeaderFields:headerFields];
        
        // Enable cache
        [_sharedObject useCache];
    });
    
    return _sharedObject;
}

- (MKNetworkOperation *)authWithCode:(NSString *)accessCode
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_AUTH;
    
    NSDictionary *paramCode = [NSDictionary dictionaryWithObject:accessCode forKey:@"user[password]"];
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:paramCode
                                          httpMethod:@"POST"];
    
    NSLog(@"auth: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"auth: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)foodTypes:(HTResponseBlock)completionBlock
                         onError:(MKNKErrorBlock)errorBlock {

    NSString *path = HT_RESOURCE_ALLFOODTYPES;
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:nil
                                          httpMethod:@"GET"];
    NSLog(@"foodTypes: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"foodTypes: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)foodsWithType:(NSNumber *)foodTypeId
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_ALLFOODS(foodTypeId);
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:nil
                                          httpMethod:@"GET"];
    NSLog(@"foodsWithType: request: %@", op.url);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        NSDictionary *response = [completedOperation responseJSON];
        
        NSLog(@"foodsWithType: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)foodsWithType:(NSString *)foodTypeId
                               andId:(NSString *)foodId
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock {
   
    NSString *path = HT_RESOURCE_FOOD(foodTypeId, foodId);
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:nil
                                          httpMethod:@"GET"];
    NSLog(@"foodsWithType:andId: request: %@", op.url);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        NSDictionary *response = [completedOperation responseJSON];
        
        NSLog(@"foodsWithType:andId: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)ordersForUser:(NSNumber *)userId
                        onCompletion:(HTResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_ALLORDERS(userId);
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:nil
                                          httpMethod:@"GET"];
    
    NSLog(@"ordersForUser: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"ordersForUser: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)postOrderForUser:(NSNumber *)userId
                           onCompletion:(HTResponseBlock)completionBlock
                                onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_ALLORDERS(userId);
    
    NSDictionary *params = [NSDictionary dictionaryWithObject:userId forKey:@"order[user_id]"];
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:params
                                          httpMethod:@"POST"];
    
    NSLog(@"postOrderForUser: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"postOrderForUser: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)putOrder:(NSNumber *)orderId
                        forUser:(NSNumber *)userId
                     withStatus:(NSNumber *)status
                         atTime:(NSDate *)deliveryTime
                     atLocation:(NSNumber *)deliveryLocation
                   onCompletion:(HTResponseBlock)completionBlock
                        onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_ORDER(userId, orderId);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:orderId, @"id",
                            status, @"order[status]",
                            [deliveryTime toRFC3339String], @"order[delivery_time]",
                            deliveryLocation, @"order[delivery_location_id]",
                            nil];
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:params
                                          httpMethod:@"PUT"];
    
    NSLog(@"putOrder: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"putOrder: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)postItemForOrder:(NSNumber *)orderId
                               withFood:(NSNumber *)foodId
                                 amount:(NSNumber *)amount
                                   note:(NSString *)note
                           onCompletion:(HTResponseBlock)completionBlock
                                onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_ALLORDERITEMS(orderId);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:orderId, @"order_id",
                            foodId, @"item[food_id]",
                            amount, @"item[amount]",
                            note, @"item[observations]",
                            nil];
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:params
                                          httpMethod:@"POST"];
    
    NSLog(@"postItemForOrder: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"postItemForOrder: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)putItemForOrder:(NSNumber *)orderId
                              withItem:(NSNumber *)orderItemId
                                  food:(NSNumber *)foodId
                                amount:(NSNumber *)amount
                                  note:(NSString *)note
                          onCompletion:(HTResponseBlock)completionBlock
                               onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_ORDERITEM(orderId, orderItemId);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:orderId, @"order_id",
                            orderItemId, @"id",
                            foodId, @"item[food_id]",
                            amount, @"item[amount]",
                            note, @"item[observations]",
                            nil];
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:params
                                          httpMethod:@"PUT"];
    
    NSLog(@"putItemForOrder: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"putItemForOrder: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)deleteItemForOrder:(NSNumber *)orderId
                                 withItem:(NSNumber *)orderItemId
                             onCompletion:(HTResponseBlock)completionBlock
                                  onError:(MKNKErrorBlock)errorBlock {
    
    NSString *path = HT_RESOURCE_ORDERITEM(orderId, orderItemId);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:                     orderItemId, @"id",
                            nil];
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:params
                                          httpMethod:@"DELETE"];
    
    NSLog(@"deleteItemForOrder: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = nil;
        
        NSLog(@"deleteItemForOrder: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation*)deliveryLocations:(HTResponseBlock)completionBlock
                                 onError:(MKNKErrorBlock)errorBlock {
    NSString *path = HT_RESOURCE_DELIVERYLOCATIONS;
    
    MKNetworkOperation *op = [self operationWithPath:path
                                              params:nil
                                          httpMethod:@"GET"];
    NSLog(@"deliveryLocations: request: %@", op.url);
    NSLog(@"op: %@", op);
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        id response = [completedOperation responseJSON];
        
        NSLog(@"deliveryLocations: response: %@", response);
        
        completionBlock(response);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
        errorBlock(error);
        
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

// Overide this method just because the original one was adding the auth header and my backend complains about it...
- (MKNetworkOperation*)imageAtURL:(NSURL *)url completionHandler:(MKNKImageBlock) imageFetchedBlock errorHandler:(MKNKResponseErrorBlock) errorBlock {
            
        if (url == nil) {
            return nil;
        }
    
    MKNetworkOperation *op = [[MKNetworkOperation alloc] initWithURLString:[url absoluteString]
                                                                    params:nil
                                                                httpMethod:@"GET"];
    op.shouldCacheResponseEvenIfProtocolIsHTTPS = YES;

    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        if (imageFetchedBlock)
            imageFetchedBlock([completedOperation responseImage],
                              url,
                              [completedOperation isCachedResponse]);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        if (errorBlock)
            errorBlock(completedOperation, error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getImageAtURL:(NSString *)url
                    completionHandler:(MKNKImageBlock)imageFetchedBlock
                         errorHandler:(MKNKResponseErrorBlock)errorBlock {
    
    MKNetworkOperation *op = [self imageAtURL:[NSURL URLWithString:url]
                            completionHandler:imageFetchedBlock
                                 errorHandler:errorBlock];
    
    return op;
}

@end
