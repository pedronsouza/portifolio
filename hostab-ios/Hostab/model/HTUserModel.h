//
//  HTUserModel.h
//  Hostab
//
//  Created by Pedro Souza on 20/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTResponseBlock.h"
#import "HTUser.h"

@interface HTUserModel : NSObject

/*!
 *  @abstract Returns the singleton user model instance.
 */
+ (id)sharedInstance;

/*!
 *  @abstract Authenticate using the given access code.
 */
- (MKNetworkOperation *)authWithCode:(NSString *)accessCode
                        onCompletion:(HTObjectBlock)completionBlock
                             onError:(HTErrorBlock)errorBlock;

/*!
 *  @abstract Logout, erasing the current logged user.
 */
- (void)logout;

/*!
 *  @abstract Load a previous user, saved in the last authentication.
 */
- (HTUser *)loadPreviousUser;

/*!
 *  @abstract Save the current logged user.
 */
- (void)saveCurrentUser;

/*!
 *  @abstract The current user logged in
 */
@property (strong, nonatomic, readonly) HTUser *currentUser;

@end
