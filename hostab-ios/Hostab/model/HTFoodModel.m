//
//  HTFoodModel.m
//  Hostab
//
//  Created by Pedro Souza on 11/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTFoodModel.h"
#import "HTBackendEngine.h"
#import "HTFood.h"

@interface HTFoodModel ()

@end

@implementation HTFoodModel

- (id)init {
    self = [super init];
    
    if (self) {

    }
    
    return self;
}

- (MKNetworkOperation *)getAllFoodTypes:(HTArrayBlock)completionBlock
                                onError:(HTErrorBlock)errorBlock {
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    MKNetworkOperation *nkOperation = nil;
    
    // Declare the food type completion block in advance
    HTArrayBlock foodTypesCompletion = ^(NSArray *array) {
        
        // Create the objects from their properties retrieved from the JSON
        NSMutableArray *foodTypeList = [[NSMutableArray alloc] initWithCapacity:[array count]];
        for (NSDictionary *objectProps in array) {
            
            HTFoodType *foodType = [HTFoodType foodTypeWithProperties:objectProps];
            [foodTypeList addObject:foodType];
        }
        
        // Invoke the completion block with the created objects
        completionBlock(foodTypeList);
    };
    
    // Build the food types network operation
    nkOperation = [backend foodTypes:foodTypesCompletion
                             onError:errorBlock];
    
    return nkOperation;
}

- (MKNetworkOperation *)getFoodsWithType:(NSNumber *)foodType
                            onCompletion:(HTArrayBlock)completionBlock
                                 onError:(HTErrorBlock)errorBlock {

    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    MKNetworkOperation *nkOperation = nil;
    
    // Declare the food type completion block in advance
    HTArrayBlock foodsCompletion = ^(NSArray *array) {
        
        // Create the objects from their properties retrieved from the JSON
        NSMutableArray *foodList = [[NSMutableArray alloc] initWithCapacity:[array count]];
        for (NSDictionary *objectProps in array) {
            
            HTFood *food = [HTFood foodWithProperties:objectProps];
            [foodList addObject:food];
        }
        
        // Invoke the completion block with the created objects
        completionBlock(foodList);
    };
    
    // Build the food types network operation
    nkOperation = [backend foodsWithType:foodType
                            onCompletion:foodsCompletion
                                 onError:errorBlock];
    
    return nkOperation;
}

- (MKNetworkOperation *)getFoodImageAtURL:(NSString *)url
                        completionHandler:(MKNKImageBlock)imageFetchedBlock
                             errorHandler:(MKNKResponseErrorBlock)errorBlock {
    
    HTBackendEngine *backend = [HTBackendEngine sharedInstance];
    
    return [backend imageAtURL:[NSURL URLWithString:url]
             completionHandler:imageFetchedBlock
                  errorHandler:errorBlock];
}

@end
