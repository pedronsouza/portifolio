//
//  HTDeliveryLocation.m
//  Hostab
//
//  Created by Pedro Souza on 01/07/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTDeliveryLocation.h"
#import "NSDate+HTUtils.h"

@implementation HTDeliveryLocation

+ (id)deliveryLocationWithProperties:(NSDictionary *)properties {
    
    HTDeliveryLocation *deliveryLocation = [[HTDeliveryLocation alloc] init];
    
    if (deliveryLocation) {
        deliveryLocation.companyId = properties[@"company_id"];
        deliveryLocation.createdAt = [NSDate dateFromRFC3339:properties[@"created_at"]];
        deliveryLocation.deliveryLocationId = properties[@"id"];
        deliveryLocation.name = properties[@"name"];
        deliveryLocation.updatedAt = [NSDate dateFromRFC3339:properties[@"updated_at"]];
    }
    
    return deliveryLocation;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ id: %@, name: %@", [super description], self.deliveryLocationId, self.name];
}

@end
