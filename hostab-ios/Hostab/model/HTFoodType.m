//
//  HTFoodType.m
//  Hostab
//
//  Created by Pedro Souza on 13/05/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTFoodType.h"
#import "NSDate+HTUtils.h"

@implementation HTFoodType

/*
 SAMPLE
 
 {
     "company_id" = 1;
     "created_at" = "2013-05-08T01:26:32Z";
     id = 1;
     locale = "pt-BR";
     name = Lanches;
     "updated_at" = "2013-05-08T01:26:32Z";
 },
 
 */
+ (id)foodTypeWithProperties:(NSDictionary *)properties {
    HTFoodType *foodType = [[HTFoodType alloc] init];
    
    if (foodType) {
        foodType.companyId = [properties objectForKey:@"company_id"];
        foodType.createdAt = [NSDate dateFromRFC3339:[properties objectForKey:@"created_at"]];
        foodType.foodTypeId = [properties objectForKey:@"id"];
        foodType.locale = [properties objectForKey:@"locale"];
        foodType.name = [properties objectForKey:@"name"];
        foodType.updatedAt = [NSDate dateFromRFC3339:[properties objectForKey:@"updated_at"]];
    }
    
    return foodType;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ id: %@, name: %@", [super description], self.foodTypeId, self.name];
}

@end
