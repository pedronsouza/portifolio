//
//  HTOrder.h
//  Hostab
//
//  Created by Pedro Souza on 22/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTResponseBlock.h"
#import "HTOrderItem.h"
#import "HTDeliveryLocation.h"

typedef enum {
    HTOrderStatusRequested = 0,
    HTOrderStatusProcessing = 1,
    HTOrderStatusDelivered = 2,
    HTOrderStatusCanceled = 3,
} HTOrderStatus;

@interface HTOrder : NSObject

// Ex: "created_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *createdAt;

// 
@property (strong, nonatomic) HTDeliveryLocation *deliveryLocation;

//
@property (strong, nonatomic) NSDate *deliveryTime;

//
@property (strong, nonatomic) NSNumber *orderId;

//
@property (strong, nonatomic) NSNumber *status;

// Ex: "updated_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *updatedAt;

@property (readonly, nonatomic) NSArray *allItems;

@property (readonly, nonatomic) float totalValue;

- (MKNetworkOperation *)createOrder:(HTSimpleBlock)completionBlock
                            onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)finishOrder:(HTSimpleBlock)completionBlock
                            onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)cancelOrder:(HTSimpleBlock)completionBlock
                            onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)addItem:(HTOrderItem *)item
                   onCompletion:(HTSimpleBlock)completionBlock
                        onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)updateItem:(HTOrderItem *)item
                      onCompletion:(HTSimpleBlock)completionBlock
                           onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)removeItem:(HTOrderItem *)item
                      onCompletion:(HTSimpleBlock)completionBlock
                           onError:(HTErrorBlock)errorBlock;

- (MKNetworkOperation *)allDeliveryLocations:(HTArrayBlock)completionBlock
                                     onError:(HTErrorBlock)errorBlock;

- (HTOrderItem *)itemWithFood:(HTFood *)food;

@end
