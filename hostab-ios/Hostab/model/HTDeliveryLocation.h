//
//  HTDeliveryLocation.h
//  Hostab
//
//  Created by Pedro Souza on 01/07/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTDeliveryLocation : NSObject

// Ex: "company_id" = 1;
@property (strong, nonatomic) NSNumber *companyId;

// Ex: "created_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *createdAt;

// Ex: id = 1;
@property (strong, nonatomic) NSNumber *deliveryLocationId;

// Ex: locale = "pt-BR";
//@property (strong, nonatomic) NSString *locale;

// Ex: name = Lanches;
@property (strong, nonatomic) NSString *name;

// Ex: "updated_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *updatedAt;

+ (id)deliveryLocationWithProperties:(NSDictionary *)properties;

@end
