//
//  HTYahooWeatherEngine.h
//  CompanionHotel
//
//  Created by Pedro Souza on 19/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "MKNetworkEngine.h"

typedef void (^HTWeatherResponseBlock)(NSDictionary *weatherCondition);

@interface HTYahooWeatherEngine : MKNetworkEngine

/// The city WOEID
@property (strong, nonatomic) NSString *WOEID;

/// The temperature unit, c for Celsius or f for Farenheit
@property (strong, nonatomic) NSString *unit;

/// Initialize with a given city and temperature unit
- (id)initWithWOEID:(NSString *)woeid
            andUnit:(NSString *)temperatureUnit;

/// Retrieve the weather
- (MKNetworkOperation*)getWeather:(HTWeatherResponseBlock)completionBlock
                          onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation*)getIconForCondition:(NSNumber *)code
                              onCompletion:(MKNKImageBlock)completionBlock
                                   onError:(MKNKErrorBlock)errorBlock;

@end
