//
//  HTFood.h
//  Hostab
//
//  Created by Pedro Souza on 13/05/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTFood : NSObject

/* SAMPLE
 {
 "created_at" = "2013-05-08T01:26:32Z";
 description = "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equaliza\U00e7\U00e3o das condi\U00e7\U00f5es financeiras e administrativas exigidas. Pensando mais a longo prazo";
 "food_images" =         (
 {
 "created_at" = "2013-05-08T02:14:23Z";
 "plate_content_type" = "image/jpeg";
 "plate_file_name" = "maior-hamburguer-do-mundo.jpg";
 "plate_file_size" = 836252;
 "plate_image_url" = "http://hostab-food-images.s3.amazonaws.com/food_images/plates/000/000/003/maior-hamburguer-do-mundo.jpg?1367979262";
 }
 );
 "food_type_id" = 1;
 id = 1;
 locale = "pt-BR";
 price = "29.99";
 title = "Hamburg\U00e3o LeLek";
 "updated_at" = "2013-05-08T01:26:32Z";
 }
 */

// Ex: "created_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *createdAt;

// Ex: description = "Caros amigos, a complexidade dos estudos efetuados desafia [...]"
@property (strong, nonatomic) NSString *foodDescription;

@property (strong, nonatomic) NSMutableArray *imageURLs;

@property (strong, nonatomic) NSMutableArray *images;

// Ex: "food_type_id" = 1;
@property (strong, nonatomic) NSNumber *foodTypeId;

// Ex: id = 1;
@property (strong, nonatomic) NSNumber *foodId;

// Ex: locale = "pt-BR";
@property (strong, nonatomic) NSString *locale;

// Ex: price = "29.99";
@property (strong, nonatomic) NSNumber *price;

// Ex: title = "Hamburg\U00e3o LeLek";
@property (strong, nonatomic) NSString *title;

// Ex: "updated_at" = "2013-05-08T01:26:32Z";
@property (strong, nonatomic) NSDate *updatedAt;

+ (id)foodWithProperties:(NSDictionary *)properties;

@end
