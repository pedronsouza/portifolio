//
//  HTAppDelegate.m
//  CompanionHotel
//
//  Created by Pedro Souza on 05/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTAppDelegate.h"
#import "HTClientSettings.h"

@implementation HTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [TestFlight takeOff:@"6add9e5e-24d6-474a-94d2-e8fc6142a1e0"];
    
    NSLog(@"Ready to roll down! *.*");
    
    [self customizeAppearance];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:HT_REFRESH_UI_NOTIFICATION
                                                        object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    //[self saveContext];
}

#pragma mark - Interface customization

- (void)customizeAppearance {
    HTClientSettings *settings = [HTClientSettings sharedInstance];
    
    [[UIBarButtonItem appearance] setTintColor:[settings colorForKey:HTClientSettingsKeyToolbarItemTintColor]];
    
    if (IS_IPHONE) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar-iphone"] forBarMetrics:UIBarMetricsDefault];
        
        [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"toolbar-iphone"]
                                forToolbarPosition:UIToolbarPositionAny
                                        barMetrics:UIBarMetricsDefault];
    }
}

@end
