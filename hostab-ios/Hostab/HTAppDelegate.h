//
//  HTAppDelegate.h
//  CompanionHotel
//
//  Created by Pedro Souza on 05/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
