//
//  main.m
//  CompanionHotel
//
//  Created by Pedro Souza on 05/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HTAppDelegate class]));
    }
}
