//
//  HTErrorMessage.h
//  Hostab
//
//  Created by Pedro Souza on 13/05/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTErrorMessage : NSObject

+ (void)showMessageFromError:(NSError *)error;

@end
