//
//  HTInterfaceCustomization.m
//  Hostab
//
//  Created by Pedro Souza on 23/07/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTInterfaceCustomization.h"
#import "HTClientSettings.h"

#define HT_CARD_SHADOW_OFFSET (5.5f)

@implementation HTInterfaceCustomization

+ (void)addBoxGradientOnView:(UIView *)view {
    
    // Retrive the colors stored in the client settings file
    HTClientSettings *settings = [HTClientSettings sharedInstance];
    UIColor *topColor = [settings colorForKey:HTClientSettingsKeyBoxColorTop];
    UIColor *bottomColor = [settings colorForKey:HTClientSettingsKeyBoxColorBottom];
    
    // Create a gradient using the loaded colors
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = view.layer.bounds;
    
    gradientLayer.colors = [NSArray arrayWithObjects:
                            (id)topColor.CGColor,
                            (id)bottomColor.CGColor,
                            nil];
    
    gradientLayer.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];
    
    // Insert the new gradient on the top of the hierarchy
    [view.layer insertSublayer:gradientLayer
                       atIndex:0];
}

+ (void)addCardShadowOnView:(UIView *)view {
    
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.4f;
    view.layer.shadowOffset = CGSizeMake(3.5f, 3.5f);
    view.layer.shadowRadius = 4.0f;
    view.layer.masksToBounds = NO;
    
    CGSize size = view.frame.size;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPath];
    
    [shadowPath moveToPoint:CGPointMake(HT_CARD_SHADOW_OFFSET, HT_CARD_SHADOW_OFFSET)];
    CGPoint pt = CGPointMake(size.width - HT_CARD_SHADOW_OFFSET,
                             0.0f + HT_CARD_SHADOW_OFFSET);
    [shadowPath addLineToPoint:pt];
    pt = CGPointMake(size.width,
                     size.height);
    [shadowPath addLineToPoint:pt];
    pt = CGPointMake(0.0f + HT_CARD_SHADOW_OFFSET,
                     size.height - HT_CARD_SHADOW_OFFSET);
    [shadowPath addLineToPoint:pt];
    [shadowPath addLineToPoint:CGPointMake(HT_CARD_SHADOW_OFFSET, HT_CARD_SHADOW_OFFSET)];
    
    view.layer.shadowPath = shadowPath.CGPath;
}

@end
