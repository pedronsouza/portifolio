//
//  HTErrorMessage.m
//  Hostab
//
//  Created by Pedro Souza on 13/05/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTErrorMessage.h"

@implementation HTErrorMessage

+ (void)showMessageFromError:(NSError *)error {
    
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                         message:error.localizedDescription
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    
    [errorAlert show];
}

@end
