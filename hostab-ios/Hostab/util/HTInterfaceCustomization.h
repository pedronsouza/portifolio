//
//  HTInterfaceCustomization.h
//  Hostab
//
//  Created by Pedro Souza on 23/07/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTInterfaceCustomization : NSObject

+ (void)addBoxGradientOnView:(UIView *)view;

+ (void)addCardShadowOnView:(UIView *)view;

@end
