//
//  HTWeatherViewController.m
//  CompanionHotel
//
//  Created by Pedro Souza on 20/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTWeatherViewController.h"
#import "HTYahooWeatherEngine.h"
#import "HTClientSettings.h"

@interface HTWeatherViewController ()

@property (strong, nonatomic) HTYahooWeatherEngine *weatherEngine;

- (void)loadWeatherData;
- (void)customizeAppearance;

@end

@implementation HTWeatherViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
    
        // Start observing the refresh UI notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleRefreshUINotification:)
                                                     name:HT_REFRESH_UI_NOTIFICATION
                                                   object:nil];
    }
    
    return self;
}

- (void)dealloc {
    
    // Stop observing any notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadWeatherData];
    
    [self customizeAppearance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Refresh notification handler

- (void) handleRefreshUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:HT_REFRESH_UI_NOTIFICATION]) {
        [self loadWeatherData];
    }
}

#pragma mark - Class extension methods

- (void)loadWeatherData {
    
    NSLog(@"Loading weather data...");
    
    self.labelCity.hidden = YES;
    self.labelTemperature.hidden = YES;
    self.imageIcon.hidden = YES;
    
    NSString *clientWOEID = [[HTClientSettings sharedInstance] settingForKey:HTClientSettingsKeyWeatherID];
    
    self.weatherEngine = [[HTYahooWeatherEngine alloc] initWithWOEID:clientWOEID
                                                             andUnit:@"c"];
    
    [self.weatherEngine getWeather:^(NSDictionary *weatherCondition) {
        
        if (weatherCondition) {
            self.labelCity.hidden = NO;
            self.labelTemperature.hidden = NO;
            self.labelTemperature.text = [NSString stringWithFormat:@"%@ºC", [weatherCondition objectForKey:@"temp"]];
            
            [self.weatherEngine getIconForCondition:[weatherCondition objectForKey:@"code"]
                                       onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
                                           self.imageIcon.hidden = NO;
                                           self.imageIcon.image = fetchedImage;
                                       } onError:^(NSError *error) {
                                           
                                           NSLog(@"ERROR %@", error);
                                           
                                       }];

        }
    } onError:^(NSError *error) {
        
        NSLog(@"ERROR %@", error);
        
    }];
}

- (void)customizeAppearance {
    
    HTClientSettings *settings = [HTClientSettings sharedInstance];
    
    self.labelCity.textColor = [settings colorForKey:HTClientSettingsKeyTopTextColor];
    self.labelTemperature.textColor = [settings colorForKey:HTClientSettingsKeyTopTextColor];
}

@end
