//
//  HTDeliveryLocationViewController.h
//  Hostab
//
//  Created by Pedro Souza on 02/07/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HTDeliveryLocationDelegate <NSObject>

- (void)didSelectItemAtIndex:(NSInteger)index;

@end

@interface HTDeliveryLocationViewController : UIViewController<UITableViewDataSource>

@property (weak, nonatomic) NSArray *deliveryLocations;
@property (weak, nonatomic) id<HTDeliveryLocationDelegate> delegate;

@end
