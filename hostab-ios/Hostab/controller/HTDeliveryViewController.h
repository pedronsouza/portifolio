//
//  HTDeliveryViewController.h
//  Hostab
//
//  Created by Pedro Souza on 19/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTDeliveryLocationViewController.h"
#import "HTDeliveryTimeViewController.h"
#import "HTOrder.h"

@protocol HTDeliveryDelegate <NSObject>

- (void)didFinishOrdering:(BOOL)finished;

@end

@interface HTDeliveryViewController : UIViewController<HTDeliveryLocationDelegate, HTDeliveryTimeDelegate>

@property (strong, nonatomic) HTOrder *order;

@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *hourLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *hourButton;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIView *separator;

@property (weak, nonatomic) IBOutlet UIView *hourPickerContainer;

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

@property (weak, nonatomic) id<HTDeliveryDelegate> delegate;

- (IBAction)tapCloseButton:(id)sender;
- (IBAction)tapCancelButton:(id)sender;
- (IBAction)tapHourButton:(id)sender;

@end
