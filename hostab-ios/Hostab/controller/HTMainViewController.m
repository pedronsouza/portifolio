//
//  HTMainViewController.m
//  Hostab
//
//  Created by Pedro Souza on 18/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTMainViewController.h"
#import "HTUserModel.h"
#import "HTClientSettings.h"

@interface HTMainViewController ()

@property (strong, nonatomic) UIImageView *iPhoneLogo;

- (void)validatePreviousUser;
- (void)logoutCurrentUser;
- (void)customizeAppearance;

@end

@implementation HTMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HTUserModel *userModel = [HTUserModel sharedInstance];
    
    [userModel addObserver:self
                forKeyPath:@"currentUser"
                   options:NSKeyValueObservingOptionNew
                   context:NULL];
    
    self.userNameLabel.text = @"";
    
    [self customizeAppearance];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (IS_IPHONE) {
        if (!self.iPhoneLogo) {
            self.iPhoneLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-iphone"]];
            
            self.iPhoneLogo.frame = CGRectMake(160.0 - self.iPhoneLogo.frame.size.width / 2,
                                             0.0,
                                             self.iPhoneLogo.frame.size.width,
                                             self.iPhoneLogo.frame.size.height);
        }
        
        self.navigationItem.titleView = self.iPhoneLogo;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self validatePreviousUser];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
        
    if (IS_IPHONE) {
        self.navigationItem.titleView = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    HTUserModel *userModel = [HTUserModel sharedInstance];
    
    [userModel removeObserver:self forKeyPath:@"currentUser"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AccessCodeSegue"]) {
        HTAccessCodeViewController *accessCodeVC = segue.destinationViewController;
        
        accessCodeVC.delegate = self;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([keyPath isEqual:@"currentUser"]) {
        
        NSObject *newUser = [change objectForKey:NSKeyValueChangeNewKey];
        
        if ([newUser isKindOfClass:[NSNull class]]) {
            self.userNameLabel.text = @"";
        } else {
            HTUser *user = (HTUser *)newUser;
            
            self.userNameLabel.text = [NSString stringWithFormat:@"%@ - Quarto %@", user.name, user.room];
        }
    } else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}

- (IBAction)tapExitButton:(id)sender {
    [self logoutCurrentUser];
}

#pragma mark - Access Code view controller delegate

- (void)accessCodeViewControllerDidAuthenticate:(HTAccessCodeViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Class extension methods

- (void)validatePreviousUser {
    
    HTUserModel *userModel = [HTUserModel sharedInstance];
    
    // Load the previous user, if available
    HTUser *previousUser = [userModel loadPreviousUser];
    
    if (previousUser) {
        
        // Load the previous user details and try to login using the
        // same access code used previously
        self.userNameLabel.text = [NSString stringWithFormat:@"%@ - Quarto %@", previousUser.name, previousUser.room];
        
        HTObjectBlock authComplete = ^(NSObject *object) {
            
            NSLog(@"Reauthenticated user: %@", object);
        };
        
        HTErrorBlock authError = ^(NSError *error) {
            
            NSLog(@"Couldn't reauthenticate previous user: %@", error.localizedDescription);
            
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                                 message:error.localizedDescription
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
            
            [errorAlert show];
        };
        
        NSLog(@"Trying to reauthenticate previous user: %@", previousUser);
        
        [userModel authWithCode:previousUser.accessCode
                   onCompletion:authComplete
                        onError:authError];

        
    } else {
        
        NSLog(@"No previously authenticated user, asking for Access Code");
        
        // If there wasn't a previous session, start the
        // authentication process
        [self performSegueWithIdentifier:@"AccessCodeSegue" sender:self];
    }
}

- (void)logoutCurrentUser {
    
    HTUserModel *userModel = [HTUserModel sharedInstance];
    
    [userModel logout];
    
    [self performSegueWithIdentifier:@"AccessCodeSegue" sender:self];
}

- (void)customizeAppearance {
    
    HTClientSettings *settings = [HTClientSettings sharedInstance];
    
    self.userNameLabel.textColor = [settings colorForKey:HTClientSettingsKeyTopTextColor];
}

@end
