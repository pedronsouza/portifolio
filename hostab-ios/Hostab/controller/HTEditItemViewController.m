//
//  HTEditItemViewController.m
//  Hostab
//
//  Created by Pedro Souza on 19/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "HTEditItemViewController.h"
#import "UITextField+HTUtils.h"
#import "HTInterfaceCustomization.h"
#import "HTOrder.h"

#define NUMBERS_ONLY @"1234567890"
#define CHARACTER_LIMIT 3

@interface HTEditItemViewController ()

@property (assign, nonatomic) BOOL addNew;

@property (strong, nonatomic) HTOrder *order;
@property (strong, nonatomic) HTOrderItem *item;

@property (assign, nonatomic) CGRect defaultBounds;

- (void)loadItem;

@end

@implementation HTEditItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    if (!IS_IPHONE) {
        self.defaultBounds = self.view.bounds;
    }
    
    [super viewDidLoad];
    
    [self customizeAppearance];
    
    if (IS_IPHONE) {
        self.scrollView.contentSize = self.contentView.frame.size;
        
        [self registerForKeyboardNotifications];
        
        NSMutableArray *items = [[self.toolbar items] mutableCopy];
        
        UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [items addObject:spacer];
        
        // Build the toolbar total value item
        self.totalValue = [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, self.view.frame.size.width, 21.0f)];
        [self.totalValue setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        [self.totalValue setBackgroundColor:[UIColor clearColor]];
        [self.totalValue setTextColor:[UIColor whiteColor]];
        [self.totalValue setShadowOffset:CGSizeMake(1.0f, 1.0f)];
        [self.totalValue setShadowColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
        [self.totalValue setTextAlignment:UITextAlignmentRight];
        
        UIBarButtonItem *total = [[UIBarButtonItem alloc] initWithCustomView:self.totalValue];
        [items addObject:total];
        
        [self.toolbar setItems:items animated:YES];
    }
    
    self.foodName.text = @"";
    self.foodDescription.text = @"";
    self.price.text = @"";
    self.note.text = @"";
    self.quantity.text = @"";
    self.totalValue.text = @"";
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    if (!IS_IPHONE) {
        self.view.superview.bounds = self.defaultBounds;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createItemWithFood:(HTFood *)food
                  forOrder:(HTOrder *)order {
    
    self.addNew = YES;
    
    self.order = order;
    self.item = [[HTOrderItem alloc] initWithFood:food
                                      andQuantity:1.0f];
    
    [self loadItem];
}

- (void)editItem:(HTOrderItem *)item
       fromOrder:(HTOrder *)order {
    
    self.addNew = NO;
    
    self.order = order;
    self.item = item;
    
    [self loadItem];
}

#pragma mark - Buttons actions

- (IBAction)tapAdd:(id)sender {
    
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber *quantity = [nf numberFromString:self.quantity.text];
    float quantityValue = [quantity floatValue];
    
    if (quantityValue < 1) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Quantidade inválida"
                                                             message:@"Digite a quantidade de itens que deseja adicionar ao pedido."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        
        [errorAlert show];
        
        return;
    }
    
    self.item.quantity = quantityValue;
    
    self.item.note = self.note.text;
 
    id<HTEditItemDelegate> delegate = self.delegate;
    HTOrderItem *currentItem = self.item;
    
    HTSimpleBlock completionBlock = ^{
        if ([delegate respondsToSelector:@selector(didFinishEditingItem:)]) {
            
            if (IS_IPHONE) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            [delegate didFinishEditingItem:currentItem];
        }
    };
    
    HTErrorBlock errorBlock = ^(NSError *error) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                             message:error.localizedDescription
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        
        [errorAlert show];
    };
    
    if (self.addNew) {
        [self.order addItem:self.item
               onCompletion:completionBlock
                    onError:errorBlock];
    } else {
        [self.order updateItem:self.item
                  onCompletion:completionBlock
                       onError:errorBlock];
    }
    
}

- (IBAction)tapCancel:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didFinishEditingItem:)]) {
        [self.delegate didFinishEditingItem:nil];
    }
}

#pragma mark - UITextField delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    BOOL shouldChange = ([string isEqualToString:filtered]) && (newLength <= CHARACTER_LIMIT);
    
    if (shouldChange) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        float value = [newText floatValue];
        float total = value * [self.item.food.price floatValue];
        
        self.totalValue.text = [NSString stringWithFormat:@"R$ %.2f", total];
    }
    
    return shouldChange;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField selectAllText];
    
    if (IS_IPHONE) {
        self.activeField = textField;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (IS_IPHONE) {
        self.activeField = nil;
    }
}

#pragma mark - UITextView delegate (for iPhone keyboard handling)

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (IS_IPHONE) {        
        self.activeField = textView;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (IS_IPHONE) {
        self.activeField = nil;
    }    
}

#pragma mark - iPhone keyboard handling

- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect tbFrame = self.toolbar.frame;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + tbFrame.size.height, 0.0);
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    self.scrollView.showsVerticalScrollIndicator = YES;
    
    // Reposition the toolbar
    self.toolbar.frame = CGRectMake(tbFrame.origin.x,
                                    tbFrame.origin.y - kbSize.height,
                                    tbFrame.size.width,
                                    tbFrame.size.height);
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= (kbSize.height + tbFrame.size.height);
    
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        
        CGPoint scrollPoint = CGPointMake(0.0,
                                          self.activeField.frame.origin.y + self.activeField.frame.size.height - aRect.size.height);
        
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Class extension methods

// Interface customization
- (void)customizeAppearance {
    
    if (!IS_IPHONE) {
        
        // Apply a gradient
        [HTInterfaceCustomization addBoxGradientOnView:self.view];
    }
    
    // Customize the note border and corners
    self.note.layer.cornerRadius = 8.0;
    self.note.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.note.layer.borderWidth = 1.25;
}

- (void)loadItem {
    
    if (self.item.food.images) {
        self.foodImage.image = self.item.food.images[0];
    }
    
    self.foodName.text = self.item.food.title;
    [self.foodName sizeToFit];
    
    self.foodDescription.text = self.item.food.foodDescription;
    [self.foodDescription sizeToFit];
    
    self.price.text = [NSString stringWithFormat:@"R$ %.2f", [self.item.food.price floatValue]];
    self.note.text = self.item.note;
    self.quantity.text = [NSString stringWithFormat:@"%.0f", self.item.quantity];
    self.totalValue.text = [NSString stringWithFormat:@"R$ %.2f", self.item.totalValue];
    
    if (self.addNew) {
        [self.addButton setTitle:@"Adicionar"
                        forState:UIControlStateNormal];
    } else {
        [self.addButton setTitle:@"Salvar"
                        forState:UIControlStateNormal];
    }
}

@end
