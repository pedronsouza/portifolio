//
//  HTAccessCodeViewController.m
//  Hostab
//
//  Created by Pedro Souza on 18/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UITextField+HTUtils.h"
#import "HTAccessCodeViewController.h"
#import "HTUserModel.h"
#import "HTClientSettings.h"
#import "HTInterfaceCustomization.h"

@interface HTAccessCodeViewController ()

- (void)customizeAppearance;

- (void)doUserAuthenticationWithCode:(NSString *)accessCode;

@end

@implementation HTAccessCodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customizeAppearance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Field delegate implementation

- (void)textFieldDidBeginEditing:(UITextField *)textField {
        [textField selectAllText];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    [self doUserAuthenticationWithCode:textField.text];
    
    return YES;
}

#pragma mark - Class extension methods

- (void)doUserAuthenticationWithCode:(NSString *)accessCode {
    
    if (![accessCode length]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Digite seu código de acesso."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    [self.activityIndicator startAnimating];
    
    HTUserModel *userModel = [HTUserModel sharedInstance];
    
    HTObjectBlock authComplete = ^(NSObject *object) {
        
        HTUser *user = (HTUser *)object;
        NSString *hotelName = [[HTClientSettings sharedInstance] settingForKey:HTClientSettingsKeyHotelName];
        NSString *welcomeMessage = [NSString stringWithFormat:@"Olá %@, desejamos nossas boas vindas ao %@.", user.name, hotelName];
        
        UIAlertView *welcomeAlert = [[UIAlertView alloc] initWithTitle:@""
                                                               message:welcomeMessage
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
        
        [welcomeAlert show];
        
        if ([self.delegate respondsToSelector:@selector(accessCodeViewControllerDidAuthenticate:)]) {
            [self.delegate accessCodeViewControllerDidAuthenticate:self];
        }
        
        [self.activityIndicator stopAnimating];
    };
    
    HTErrorBlock authError = ^(NSError *error) {
        
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                             message:error.localizedDescription
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        
        [errorAlert show];
        
        [self.activityIndicator stopAnimating];
    };
    
    [userModel authWithCode:accessCode
               onCompletion:authComplete
                    onError:authError];
}

#pragma mark Interface customization

- (void)customizeAppearance {
    
    // Set a drop shadow and rounded corners on the hotel icon
    self.imageHotelIcon.layer.cornerRadius = 8.0;
    self.containerHotelIcon.layer.cornerRadius = 8.0;
    self.containerHotelIcon.layer.shadowOpacity = 0.75;
    self.containerHotelIcon.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.containerHotelIcon.layer.shadowRadius = 2.0;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.mainContainer.layer.cornerRadius = 8.0;
        self.mainContainer.layer.shadowOpacity = 0.5;
        self.mainContainer.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        self.mainContainer.layer.shadowRadius = 6.0;
        [HTInterfaceCustomization addBoxGradientOnView:self.mainContainer];
    }
}

- (IBAction)tapOKButton:(id)sender {
    [self.textAccessCode resignFirstResponder];
    
    [self doUserAuthenticationWithCode:self.textAccessCode.text];
}
@end
