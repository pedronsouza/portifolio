//
//  HTOrderViewController.m
//  Hostab
//
//  Created by Pedro Souza on 27/08/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTOrderViewController.h"
#import "HTDeliveryViewController.h"
#import "HTPedidoCell.h"
#import "HTClientSettings.h"
#import "HTErrorMessage.h"

@interface HTOrderViewController ()

// The item editor
@property (weak, nonatomic) HTEditItemViewController *currentEditItem;

@end

@implementation HTOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if (IS_IPHONE) {
        NSMutableArray *items = [[self.toolbar items] mutableCopy];
        
        UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [items addObject:spacer];
        
        // Build the toolbar total value item
        self.totalPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, self.view.frame.size.width, 21.0f)];
        [self.totalPriceLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        [self.totalPriceLabel setBackgroundColor:[UIColor clearColor]];
        [self.totalPriceLabel setTextColor:[UIColor whiteColor]];
        [self.totalPriceLabel setShadowOffset:CGSizeMake(1.0f, 1.0f)];
        [self.totalPriceLabel setShadowColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
        [self.totalPriceLabel setTextAlignment:UITextAlignmentRight];
        
        UIBarButtonItem *total = [[UIBarButtonItem alloc] initWithCustomView:self.totalPriceLabel];
        [items addObject:total];
        
        [self.toolbar setItems:items animated:YES];
    }
    
    self.orderItems = self.order.allItems;
    
    self.totalPriceLabel.text = [NSString stringWithFormat:@"R$ %.2f", self.order.totalValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView datasource and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.orderItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HTPedidoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HTPedidoCell"];
    
    if (!cell) {
        cell = [[HTPedidoCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:@"HTPedidoCell"];
    }
    
    HTOrderItem *item = self.orderItems[indexPath.row];
    
    cell.owner = self;
    cell.eraseButtonAction = @selector(eraseOrderItem:);
    
    [cell loadOrderItem:item];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"EditItemSegue"
                              sender:nil];
    
    HTOrderItem *item = self.orderItems[indexPath.row];
    self.currentEditItem.delegate = self;
    [self.currentEditItem editItem:item
                         fromOrder:self.order];
    
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
}

#pragma mark HTPedidoCell button action

- (void)eraseOrderItem:(HTOrderItem *)orderItem {
    
    if (self.order) {
        [self.order removeItem:orderItem
                  onCompletion:^{
                      self.totalPriceLabel.text = [NSString stringWithFormat:@"R$ %.2f", self.order.totalValue];
                      
                      self.orderItems = self.order.allItems;
                      [self.tableView reloadData];
                  }
                       onError:^(NSError *error) {
                           UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                                                message:error.localizedDescription
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                           
                           [errorAlert show];
                       }];
    }
}

#pragma mark - Segue handling

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    BOOL shouldPerform = YES;
    
    NSLog(@"shouldPerformSegueWithIdentifier: %@ %@", identifier, sender);
    
    if ([identifier isEqualToString:@"OrderDeliverySegue"]) {
        if ([self.orderItems count] == 0) {
            shouldPerform = NO;
            
            UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Pedido Vazio"
                                                             message:@"Você ainda não adicionou itens ao seu pedido."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            [alerta show];
        }
    }
    
    return shouldPerform;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"prepareForSegue: %@ %@", segue.identifier, sender);
    
    if ([segue.identifier isEqualToString:@"OrderDeliverySegue"]) {
        
        HTDeliveryViewController *deliveryViewController = segue.destinationViewController;
        
        deliveryViewController.order = self.order;
        deliveryViewController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"EditItemSegue"]) {
        
        self.currentEditItem = segue.destinationViewController;
        self.currentEditItem.delegate = self;
        
    }
}

#pragma mark - Item Editing Delegate

- (void)didFinishEditingItem:(HTOrderItem *)item {
    
    // Dismiss the modal view controller
    if (!IS_IPHONE) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
    if (item) {
        self.totalPriceLabel.text = [NSString stringWithFormat:@"R$ %.2f", self.order.totalValue];
        
        self.orderItems = self.order.allItems;
        [self.tableView reloadData];
    }
}

#pragma mark - Delivery Delegate

- (void)didFinishOrdering:(BOOL)finished {
    if (finished) {
        [self.order finishOrder:^{
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Pedido Enviado"
                                                                   message:@"Seu pedido foi enviado para nossa cozinha e em breve você receberá sua deliciosa refeição."
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
            [successAlert show];
            
            self.order = nil;
            self.orderItems = nil;
            self.totalPriceLabel.text = @"R$ 0.00";
            
            [self.tableView reloadData];
            
        } onError:^(NSError *error) {
            [HTErrorMessage showMessageFromError:error];
        }];
        
        
    }
    
    // Dismiss the modal view controller
    if (!IS_IPHONE) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark - Class extension

// Interface customization
- (void)customizeAppearance {
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titulo_cardapio"] forBarMetrics:UIBarMetricsDefault];
    
    HTClientSettings *settings = [HTClientSettings sharedInstance];

    [self.sendOrderButton setTitleColor:[settings colorForKey:HTClientSettingsKeyContrastBtnTextColor] forState:UIControlStateNormal];

}

@end
