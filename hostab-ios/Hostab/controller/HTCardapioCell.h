//
//  HTCardapioCell.h
//  Hostab
//
//  Created by Pedro Souza on 17/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTFood.h"

@interface HTCardapioCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *foodImage;
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *foodDescription;
@property (weak, nonatomic) IBOutlet UILabel *foodPrice;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (strong, nonatomic) NSObject *owner;
@property (assign, nonatomic) SEL photoButtonAction;
@property (assign, nonatomic) SEL addButtonAction;

- (IBAction)touchedPhotoButton:(id)sender;
- (IBAction)touchedAddButton:(id)sender;

- (void)loadFood:(HTFood *)food;

@end
