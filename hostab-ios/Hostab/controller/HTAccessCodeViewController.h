//
//  HTAccessCodeViewController.h
//  Hostab
//
//  Created by Pedro Souza on 18/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HTAccessCodeViewController;
@protocol HTAccessCodeViewControllerDelegate <NSObject>

- (void)accessCodeViewControllerDidAuthenticate:(HTAccessCodeViewController *)controller;

@end

@interface HTAccessCodeViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) id<HTAccessCodeViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITextField *textAccessCode;
@property (weak, nonatomic) IBOutlet UIImageView *imageHotelIcon;
@property (weak, nonatomic) IBOutlet UIView *containerHotelIcon;
@property (weak, nonatomic) IBOutlet UIView *mainContainer;

- (IBAction)tapOKButton:(id)sender;

@end
