//
//  HTOrderViewController.h
//  Hostab
//
//  Created by Pedro Souza on 27/08/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTEditItemViewController.h"
#import "HTDeliveryViewController.h"
#import "HTOrder.h"

@interface HTOrderViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, HTEditItemDelegate, HTDeliveryDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendOrderButton;

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

@property (strong, nonatomic) HTOrder *order;
@property (strong, nonatomic) NSArray *orderItems;

@end
