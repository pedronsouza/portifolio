//
//  HTWeatherViewController.h
//  CompanionHotel
//
//  Created by Pedro Souza on 20/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTWeatherViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelCity;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *labelTemperature;

- (void) handleRefreshUINotification:(NSNotification *) notification;

@end
