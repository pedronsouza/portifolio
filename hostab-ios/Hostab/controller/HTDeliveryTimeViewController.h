//
//  HTDeliveryTimeViewController.h
//  Hostab
//
//  Created by Pedro Souza on 21/07/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HTDeliveryTimeDelegate <NSObject>

- (void)didSelectTime:(NSDate *)time;

@optional

- (void)didClose:(UIViewController *)deliveryTimeController;

@end

@interface HTDeliveryTimeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) id<HTDeliveryTimeDelegate> delegate;

- (IBAction)tapClose:(id)sender;

@end
