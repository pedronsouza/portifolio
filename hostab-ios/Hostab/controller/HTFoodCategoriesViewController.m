//
//  HTFoodCategoriesViewController.m
//  Hostab
//
//  Created by Pedro Souza on 11/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTFoodCategoriesViewController.h"
#import "HTFoodType.h"

@interface HTFoodCategoriesViewController ()

@end

@implementation HTFoodCategoriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    HTFoodType *category = self.categories[indexPath.row];
    cell.textLabel.text = category.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([self.delegate respondsToSelector:@selector(didSelectCategoryAtIndex:)]) {
        [self.delegate didSelectCategoryAtIndex:indexPath.row];
    }
}

- (IBAction)tapClose:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didClose:)]) {
        [self.delegate didClose:self];
    }
}

@end
