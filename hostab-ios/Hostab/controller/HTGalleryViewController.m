//
//  HTGalleryViewController.m
//  Hostab
//
//  Created by Pedro Souza on 06/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTGalleryViewController.h"
#import "ReflectionView.h"
#import "HTClientSettings.h"

@interface HTGalleryViewController ()

@property (assign, nonatomic) CGRect defaultBounds;

- (void)customizeAppearance;

@end

@implementation HTGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    if (!IS_IPHONE) {
        self.defaultBounds = self.view.bounds;
    }
    
    [super viewDidLoad];
    
    self.carousel.type = iCarouselTypeCoverFlow;
    self.carousel.vertical = NO;
    self.carousel.centerItemWhenSelected = YES;
    self.carousel.stopAtItemBoundary = YES;
    self.carousel.scrollToItemBoundary = YES;
    self.carousel.scrollOffset = 1.0;
    self.carousel.bounceDistance = 0.125;
    
    [self customizeAppearance];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    if (!IS_IPHONE) {
        self.view.superview.bounds = self.defaultBounds;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    //return the total number of items in the carousel
    return [self.images count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view {

    //create new view if no view is available for recycling
    if (!view) {
        
        // Set up the reflection view. It's sightly larger then content in order to
        // have some horizontal padding
        UIImageView *imageView = nil;
        if (IS_IPHONE) {
            view = [[ReflectionView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 288.4f, 200.0f)];
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 272.4f, 200.0f)];
        } else {
            view = [[ReflectionView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 476.7f, 350.0f)];
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 466.7f, 350.0f)];
        }
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        imageView.layer.borderColor = [UIColor blackColor].CGColor;
        imageView.layer.backgroundColor = [UIColor whiteColor].CGColor;
        imageView.layer.borderWidth = 2.0f;
        imageView.layer.cornerRadius = 8.0f;
        
        [view addSubview:imageView];
    }
    
    UIImageView *imageView = [view.subviews objectAtIndex:0];
    imageView.image = [self.images objectAtIndex:index];
    
    [(ReflectionView *)view update];
    
    return view;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel {
    CGFloat width = 0.0f;
    
    if (IS_IPHONE) {
        width = 216.0f;
    } else {
        width = 476.7f;
    }
    
    return width;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    
    CGFloat optionValue = value;
    
    if (option == iCarouselOptionVisibleItems) {
        optionValue = 3.0f;
    }
    
    return optionValue;
    
}

#pragma mark - Class extension methods

// Interface customization
- (void)customizeAppearance {
  
    HTClientSettings *settings = [HTClientSettings sharedInstance];
    
    self.titleLabel.textColor = [settings colorForKey:HTClientSettingsKeyBgTextColor];
    [self.closeButton setTitleColor:[settings colorForKey:HTClientSettingsKeyContrastBtnTextColor] forState:UIControlStateNormal];

    self.view.backgroundColor = [UIColor whiteColor];
}

- (IBAction)tapCloseButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didCloseGallery)]) {
        [self.delegate didCloseGallery];
    }
}

@end
