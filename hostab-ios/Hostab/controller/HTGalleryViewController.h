//
//  HTGalleryViewController.h
//  Hostab
//
//  Created by Pedro Souza on 06/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@protocol HTGalleryDelegate <NSObject>

- (void)didCloseGallery;

@end

@interface HTGalleryViewController: UIViewController <iCarouselDataSource, iCarouselDelegate>

@property (weak, nonatomic) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (strong, nonatomic) NSArray *images;
@property (weak, nonatomic) id<HTGalleryDelegate> delegate;

- (IBAction)tapCloseButton:(id)sender;

@end
