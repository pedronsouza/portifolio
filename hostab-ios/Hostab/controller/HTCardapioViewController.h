//
//  HTCardapioViewController.h
//  CompanionHotel
//
//  Created by Pedro Souza on 23/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTFoodCategoriesViewController.h"
#import "HTGalleryViewController.h"
#import "HTEditItemViewController.h"

@interface HTCardapioViewController : UIViewController <UICollectionViewDataSource, HTFoodCategoriesDelegate, HTGalleryDelegate, HTEditItemDelegate>


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *categoryActivityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIView *categoryContainer;

- (IBAction)tapFoodCategory:(id)sender;


@end
