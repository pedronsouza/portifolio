//
//  HTCardapioCell.m
//  Hostab
//
//  Created by Pedro Souza on 17/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTCardapioCell.h"
#import "HTFoodModel.h"
#import "HTInterfaceCustomization.h"
#import "HTClientSettings.h"

@interface HTCardapioCell ()

@property (strong, nonatomic) HTFood *food;
@property (strong, nonatomic) NSMutableArray *foodImages;
@property (strong, nonatomic) NSMutableArray *imageOperations;

- (void)loadImages:(NSArray *)imagesURLs;
- (void)customizeAppearance;

@end

@implementation HTCardapioCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadFood:(HTFood *)food {
    
    self.food = food;
    
    self.foodName.text = food.title;
    self.foodDescription.text = food.foodDescription;
    self.foodPrice.text = [NSString stringWithFormat:@"R$ %.2f", [food.price floatValue]];
    
    [self customizeAppearance];
    
    [self loadImages:food.imageURLs];
    
    
}

- (IBAction)touchedPhotoButton:(id)sender {
    
    if ([self.owner respondsToSelector:self.photoButtonAction]) {
        [self.owner performSelector:self.photoButtonAction
                         withObject:self.foodImages
                         withObject:self.food.title];
    }
    
}

- (IBAction)touchedAddButton:(id)sender {
    
    if ([self.owner respondsToSelector:self.addButtonAction]) {
        [self.owner performSelector:self.addButtonAction
                                withObject:self.food];
    }
    
}

- (void)prepareForReuse {
    for (MKNetworkOperation *op in self.imageOperations) {
        [op cancel];
    }
    
    self.imageOperations = nil;
    self.foodImages = nil;
}

#pragma mark - Class extension

- (void)loadImages:(NSArray *)imagesURLs {
    
    HTFoodModel *foodModel = [[HTFoodModel alloc] init];
    
    self.foodImages = [[NSMutableArray alloc] initWithCapacity:[imagesURLs count]];
    self.imageOperations = [[NSMutableArray alloc] initWithCapacity:[imagesURLs count]];
    self.photoButton.hidden = YES;
    
    // Download all images
    for (NSString *imageURL in imagesURLs) {
        
        MKNetworkOperation *op;
        op = [foodModel getFoodImageAtURL:imageURL
                        completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
                            [self.foodImages addObject:fetchedImage];
                            
                            if ([self.foodImages count] == [imagesURLs count]) {
                                self.foodImage.image = self.foodImages[0];
                                self.food.images = self.foodImages;
                                self.photoButton.hidden = NO;
                            }
                            
                            [op cancel];
                            
                        } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
                            NSLog(@"Error downloading image from URL ( %@ ): %@", imageURL, error);
                        }];
    }
}

- (void)customizeAppearance {
    
    if (!self.backgroundView) {
        self.layer.cornerRadius = 6.0f;
        
        self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.backgroundView.opaque = NO;
        self.backgroundView.backgroundColor = [UIColor redColor];
        self.backgroundView.layer.cornerRadius = 6.0f;
        self.backgroundView.layer.masksToBounds = YES;
        
        [HTInterfaceCustomization addBoxGradientOnView:self.backgroundView];
        [HTInterfaceCustomization addCardShadowOnView:self];
    }
    
    self.foodImage.image = [UIImage imageNamed:@"placeholder_cardapio.png"];
    
    self.foodName.frame = CGRectMake(11.0f, 114.0f, 292.0f, 21.0f);
    [self.foodName sizeToFit];
    
    self.foodDescription.frame = CGRectMake(11.0f, 136.0f, 292.0f, 71.0f);
    [self.foodDescription sizeToFit];
    
    HTClientSettings *settings = [HTClientSettings sharedInstance];
    UIColor *accentColor = [settings colorForKey:HTClientSettingsKeyAccentColor];
    self.foodPrice.textColor = accentColor;
    
    
}

@end
