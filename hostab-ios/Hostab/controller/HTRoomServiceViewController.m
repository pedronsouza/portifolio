//
//  HTRoomServiceViewController.m
//  CompanionHotel
//
//  Created by Pedro Souza on 24/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTRoomServiceViewController.h"

@interface HTRoomServiceViewController ()

- (void)customizeAppearance;

@end

@implementation HTRoomServiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self customizeAppearance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface customization

- (void)customizeAppearance {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titulo_servico"] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - UITableView datasource and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numRows = (tableView.tag == 1) ? 10 : 2;
    
    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = (tableView.tag == 1) ? @"CelulaServico" : @"CelulaItemServico";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    if (tableView.tag == 1) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"Travesseiro";
                break;
                
            case 1:
                cell.textLabel.text = @"Lençol de casal";
                break;
            
            case 2:
                cell.textLabel.text = @"Lençol de solteiro";
                break;
                
            case 3:
                cell.textLabel.text = @"Cobertor de casal";
                break;
                
            case 4:
                cell.textLabel.text = @"Cobertor de solteiro";
                break;
                
            case 5:
                cell.textLabel.text = @"Toalha de banho";
                break;
                
            case 6:
                cell.textLabel.text = @"Toalha de rosto";
                break;
                
            case 7:
                cell.textLabel.text = @"Kit de shampoo e sabonete";
                break;
            
            case 8:
                cell.textLabel.text = @"Kit de escova e pasta de dente";
                break;
                
            case 9:
                cell.textLabel.text = @"Papel higiênico";
                break;
                
            default:
                break;
        }
    } else {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Travesseiro";
            cell.detailTextLabel.text = @"2";
        } else {
            cell.textLabel.text = @"Cobertor de solteiro";
            cell.detailTextLabel.text = @"1";
        }
    }
    
    return cell;
}

@end
