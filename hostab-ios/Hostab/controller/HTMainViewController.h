//
//  HTMainViewController.h
//  Hostab
//
//  Created by Pedro Souza on 18/06/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTAccessCodeViewController.h"

@interface HTMainViewController : UIViewController<HTAccessCodeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

- (IBAction)tapExitButton:(id)sender;

@end
