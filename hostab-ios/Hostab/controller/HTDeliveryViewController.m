//
//  HTDeliveryViewController.m
//  Hostab
//
//  Created by Pedro Souza on 19/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTDeliveryViewController.h"
#import "HTOrder.h"
#import "HTErrorMessage.h"
#import "HTInterfaceCustomization.h"
#import "NSDate+HTUtils.h"

@interface HTDeliveryViewController ()

@property (assign, nonatomic) CGRect defaultBounds;

/// The current network operation
@property (strong, nonatomic) MKNetworkOperation *currentOperation;

/// The delivery locations
@property (strong, nonatomic) NSArray *allDeliveryLocations;
@property (strong, nonatomic) HTDeliveryLocation *currentDeliveryLocation;

/// Delivery time
@property (strong, nonatomic) NSDate *currentDeliveryTime;

/// The current popover
@property (weak, nonatomic) UIPopoverController *currentPopover;

- (void)customizeAppearance;
- (void)showActivityIndicator;
- (void)hideActivityIndicator;
- (void)loadDeliveryLocations;

@end

@implementation HTDeliveryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    if (!IS_IPHONE) {
        self.defaultBounds = self.view.bounds;
    }
    
    [super viewDidLoad];
	
    [self customizeAppearance];
    
    if (IS_IPHONE) {
        NSMutableArray *items = [[self.toolbar items] mutableCopy];
        
        UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [items addObject:spacer];
        
        // Build the toolbar total value item
        self.totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, self.view.frame.size.width, 21.0f)];
        [self.totalLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        [self.totalLabel setBackgroundColor:[UIColor clearColor]];
        [self.totalLabel setTextColor:[UIColor whiteColor]];
        [self.totalLabel setShadowOffset:CGSizeMake(1.0f, 1.0f)];
        [self.totalLabel setShadowColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
        [self.totalLabel setTextAlignment:UITextAlignmentRight];
        
        UIBarButtonItem *total = [[UIBarButtonItem alloc] initWithCustomView:self.totalLabel];
        [items addObject:total];
        
        [self.toolbar setItems:items animated:YES];
    }
    
    self.totalLabel.text = [NSString stringWithFormat:@"R$ %.2f", self.order.totalValue];
    self.hourLabel.text = @"";
    self.locationLabel.text = @"";
    
    [self loadDeliveryLocations];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    if (!IS_IPHONE) {
        self.view.superview.bounds = self.defaultBounds;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue management

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"prepareForSegue: %@ %@", segue.identifier, sender);
    
    if ([segue.identifier isEqualToString:@"DeliveryLocationSegue"]) {
        
        HTDeliveryLocationViewController *viewController = segue.destinationViewController;
        
        // Set the data and delegate of the delivery location selecting controller
        viewController.deliveryLocations = self.allDeliveryLocations;
        viewController.delegate = self;
        
        // Hold a reference of the popover controller to manually dismiss it
        UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue *)segue;
        self.currentPopover = popoverSegue.popoverController;
    } else if ([segue.identifier isEqualToString:@"DeliveryTimeSegue"]) {
        
        HTDeliveryTimeViewController *viewController = segue.destinationViewController;
        
        // Set the data and delegate of the delivery location selecting controller
        viewController.delegate = self;
    }
}

- (IBAction)tapCloseButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didFinishOrdering:)]) {
        if (!self.currentDeliveryTime) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Escolha um horário de entrega para seu pedido."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
            return;
        }
        
        if (!self.currentDeliveryLocation) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Escolha um local de entrega para seu pedido."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
            return;
        }
        
        self.order.deliveryLocation = self.currentDeliveryLocation;
        self.order.deliveryTime = self.currentDeliveryTime;
        
        [self.delegate didFinishOrdering:YES];
        
        if (IS_IPHONE) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (IBAction)tapCancelButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didFinishOrdering:)]) {
        [self.delegate didFinishOrdering:NO];
    }
}

- (IBAction)tapHourButton:(id)sender {
    self.hourPickerContainer.hidden = !self.hourPickerContainer.hidden;
}

#pragma mark - Delivery Location delegate

- (void)didSelectItemAtIndex:(NSInteger)index {
    
    // Dismiss the popover manually
    [self.currentPopover dismissPopoverAnimated:YES];
    
    self.currentDeliveryLocation = self.allDeliveryLocations[index];
    self.locationLabel.text = self.currentDeliveryLocation.name;
}

#pragma mark - Delivery Time delegate

- (void)didSelectTime:(NSDate *)time {
    self.currentDeliveryTime = time;
    self.hourLabel.text = [time timeString];
}

- (void)didClose:(UIViewController *)deliveryTimeController {
    self.hourPickerContainer.hidden = YES;
}

#pragma mark - Class extension

// Interface customization
- (void)customizeAppearance {
    
    // Apply a gradient
    [HTInterfaceCustomization addBoxGradientOnView:self.view];
    
    self.separator.layer.cornerRadius = 2.0;
    self.separator.layer.shadowOpacity = 0.5;
    self.separator.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.separator.layer.shadowRadius = 1.5;
}

- (void)showActivityIndicator {
    
    // Hide the controls and show the indicator
    [self.activityIndicator startAnimating];
    self.locationButton.enabled = NO;
    self.locationLabel.hidden = YES;
    self.hourButton.enabled = NO;
    self.hourLabel.hidden = YES;
}

- (void)hideActivityIndicator {
    
    // Show up the controls and hide the indicator
    [self.activityIndicator stopAnimating];
    self.locationButton.enabled = YES;
    self.locationLabel.hidden = NO;
    self.hourButton.enabled = YES;
    self.hourLabel.hidden = NO;
}

- (void)loadDeliveryLocations {
    [self showActivityIndicator];
    
    // Load the delivery locations
    self.currentOperation = [self.order allDeliveryLocations:^(NSArray *array) {
        self.allDeliveryLocations = array;
        
        if ([self.allDeliveryLocations count] == 1) {
            self.currentDeliveryLocation = self.allDeliveryLocations[0];
            self.locationLabel.text = self.currentDeliveryLocation.name;
            
            self.locationButton.hidden = YES;
        }
        
        [self hideActivityIndicator];
        
        NSLog(@"Delivery locations: %i items", [self.allDeliveryLocations count]);
    } onError:^(NSError *error) {
        
        [self hideActivityIndicator];
        
        // Display the error message
        [HTErrorMessage showMessageFromError:error];
        
        NSLog(@"Delivery locations error: %@", error);
    }];
}

@end
