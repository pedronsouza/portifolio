//
//  HTPedidoCell.m
//  Hostab
//
//  Created by Pedro Souza on 19/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTPedidoCell.h"

@interface HTPedidoCell ()

@property (strong, nonatomic) HTOrderItem *orderItem;

@end

@implementation HTPedidoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadOrderItem:(HTOrderItem *)orderItem {
    self.orderItem = orderItem;
    
    self.foodName.text = orderItem.food.title;
    self.foodName.frame = CGRectMake(77.0f, 6.0f, 146.0f, 40.0f);
    [self.foodName sizeToFit];
    
    self.note.text = orderItem.note;
    
    self.price.text = [NSString stringWithFormat:@"R$ %.2f", orderItem.totalValue];
    
    if (orderItem.food.images) {
        self.foodImage.image = orderItem.food.images[0];
    }
}

- (IBAction)touchedEraseButton:(id)sender {
    if ([self.owner respondsToSelector:self.eraseButtonAction]) {
        [self.owner performSelector:self.eraseButtonAction
                         withObject:self.orderItem];
    }
}

@end
