//
//  HTFoodCategoriesViewController.h
//  Hostab
//
//  Created by Pedro Souza on 11/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HTFoodCategoriesDelegate <NSObject>

- (void)didSelectCategoryAtIndex:(NSInteger)index;

@optional

- (void)didClose:(UIViewController *)foodCategoryViewController;

@end

@interface HTFoodCategoriesViewController : UIViewController<UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) NSArray *categories;
@property (weak, nonatomic) id<HTFoodCategoriesDelegate> delegate;

- (IBAction)tapClose:(id)sender;

@end
