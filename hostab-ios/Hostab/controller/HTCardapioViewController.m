//
//  HTCardapioViewController.m
//  CompanionHotel
//
//  Created by Pedro Souza on 23/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTCardapioViewController.h"
#import "HTFoodCategoriesViewController.h"
#import "HTEditItemViewController.h"
#import "HTFoodModel.h"
#import "HTCardapioCell.h"
#import "HTErrorMessage.h"
#import "HTGalleryViewController.h"
#import "HTClientSettings.h"
#import "HTOrder.h"
#import "HTOrderViewController.h"

@interface HTCardapioViewController ()

/// The food model
@property (strong, nonatomic) HTFoodModel *foodModel;

/// The current network operation
@property (strong, nonatomic) MKNetworkOperation *currentOperation;

/// The food types
@property (strong, nonatomic) NSArray *foodTypes;

/// The foods from the selected type
@property (strong, nonatomic) NSArray *foods;

@property (strong, nonatomic) HTFoodCategoriesViewController *currentCategories;

@property (weak, nonatomic) UIPopoverController *currentPopover;

@property (strong, nonatomic) UIColor *popoverColor;

@property (weak, nonatomic) HTGalleryViewController *currentGallery;

/// The order
@property (weak, nonatomic) HTOrderViewController *currentOrder;
@property (strong, nonatomic) HTOrder *order;

// The item editor
@property (weak, nonatomic) HTEditItemViewController *currentEditItem;

- (IBAction)unwindFromOrder:(UIStoryboardSegue *)segue;

- (void)createItemFromFood:(HTFood *)food;

- (void)customizeAppearance;

- (void)showCategoryActivityIndicator;

- (void)hideCategoryActivityIndicator;

- (void)loadFoodsWithCategoryFromIndex:(NSInteger)index;

@end

@implementation HTCardapioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showCategoryActivityIndicator];
    
    [self customizeAppearance];
    
    // Load the food categories (food types)
    self.foodModel = [[HTFoodModel alloc] init];
    self.currentOperation = [self.foodModel getAllFoodTypes:^(NSArray *list) {
        self.foodTypes = list;
        
        // Load all foods from the first category
        [self loadFoodsWithCategoryFromIndex:0];
        
        [self hideCategoryActivityIndicator];
        
        NSLog(@"Categories: %i items", [self.foodTypes count]);
    } onError:^(NSError *error) {
        
        [self hideCategoryActivityIndicator];
        
        // Display the error message
        [HTErrorMessage showMessageFromError:error];
        
        NSLog(@"Categories error: %@", error);
    }];
    
    //self.orderItems = [NSMutableArray arrayWithCapacity:10];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    [self customizeAppearance];    
}

-(void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        if (self.order) {
            if ([self.order.status isEqualToNumber:@(HTOrderStatusRequested)]) {
                [self.order cancelOrder:^{
                    NSLog(@"Order canceled: %@", self.order);
                } onError:^(NSError *error) {
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                                         message:error.localizedDescription
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                    
                    [errorAlert show];
                }];
            }
        }
    }
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.foods count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = nil;
    NSString *cellIdentifier = nil;
    
    HTFood *food = self.foods[indexPath.row];
    
    cellIdentifier = @"HTCardapioCell";
    cell = [cv dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                         forIndexPath:indexPath];
    
    HTCardapioCell *cardapioCell = (HTCardapioCell *)cell;
    

    
    [cardapioCell loadFood:food];
    
    cardapioCell.owner = self;
    cardapioCell.photoButtonAction = @selector(openGalleryWithImages:andTitle:);
    cardapioCell.addButtonAction = @selector(addFood:);
    
    cardapioCell.backgroundColor = [UIColor whiteColor];
    
    return cardapioCell;
}

#pragma mark HTCardapioCell buttons actions

- (void)openGalleryWithImages:(NSArray *)images andTitle:(NSString *)title {
    self.currentGallery.images = images;
    self.currentGallery.titleLabel.text = title;
    [self.currentGallery.carousel reloadData];
}

- (void)addFood:(HTFood *)food {
    [self createItemFromFood:food];
}

#pragma mark - Segue handling

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"prepareForSegue: %@ %@", segue.identifier, sender);
    
    if ([segue.identifier isEqualToString:@"FoodCategoriesSegue"]) {
        
        HTFoodCategoriesViewController *categoriesViewController = segue.destinationViewController;
        
        // Set the categories and delegate of the category selecting controller
        categoriesViewController.categories = self.foodTypes;
        categoriesViewController.delegate = self;
        
        // Hold a reference of the popover controller to manually dismiss it
        if (IS_IPHONE) {
            self.currentCategories = categoriesViewController;
        } else {
            UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue *)segue;
            self.currentPopover = popoverSegue.popoverController;
        }
        
    } else if ([segue.identifier isEqualToString:@"GallerySegue"]) {
        
        self.currentGallery = segue.destinationViewController;
        self.currentGallery.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"OrderSegue"]) {
        
        HTOrderViewController *orderViewController = segue.destinationViewController;
        orderViewController.order = self.order;
        
        self.currentOrder = orderViewController;
        
    } else if ([segue.identifier isEqualToString:@"AddItemSegue"]) {
        
        self.currentEditItem = segue.destinationViewController;
        self.currentEditItem.delegate = self;
        
    }
}

- (IBAction)unwindFromOrder:(UIStoryboardSegue *)segue {
    NSLog(@"Unwinding from order view controller");
}

#pragma mark - Food Category Delegate

- (void)didSelectCategoryAtIndex:(NSInteger)index {
    
    // Reload all foods from that category
    [self loadFoodsWithCategoryFromIndex:index];
    
    if (IS_IPHONE) {
        
        // Hide the container
        self.categoryContainer.hidden = YES;
    } else {
        
        // Dismiss the popover manually 
        [self.currentPopover dismissPopoverAnimated:YES];
    }
}

- (void)didClose:(UIViewController *)foodCategoryViewController {
    
    // Hide the container
    self.categoryContainer.hidden = YES;
}

#pragma mark - Gallery Delegate

- (void)didCloseGallery {
    
    // Dismiss the modal gallery view controller
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Item Editing Delegate

- (void)didFinishEditingItem:(HTOrderItem *)item {
    
    if (!IS_IPHONE) {
        
        // Dismiss the modal view controller
        [self dismissViewControllerAnimated:YES completion:NULL];
        
        self.currentOrder.order = self.order;
        self.currentOrder.orderItems = self.order.allItems;
        self.currentOrder.totalPriceLabel.text = [NSString stringWithFormat:@"R$ %.2f", self.order.totalValue];
        [self.currentOrder.tableView reloadData];
    }
}

#pragma mark - Class extension

- (void)createItemFromFood:(HTFood *)food {
    
    if (self.order && [self.order.status isEqualToNumber:@(HTOrderStatusRequested)]) {
        self.currentEditItem.delegate = self;
        
        HTOrderItem *item = [self.order itemWithFood:food];
        if (item) {
            [self.currentEditItem editItem:item
                                 fromOrder:self.order];
        } else {
            [self.currentEditItem createItemWithFood:food
                                            forOrder:self.order];
        }
    } else {
        
        self.order = [[HTOrder alloc] init];
        [self.order createOrder:^{
            
            self.currentEditItem.delegate = self;
            [self.currentEditItem createItemWithFood:food
                                            forOrder:self.order];
            
        } onError:^(NSError *error) {
            
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                                 message:error.localizedDescription
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
            
            [errorAlert show];
        }];
    }
    
    if (!IS_IPHONE) {
        self.currentOrder.order = self.order;
    }
}

// Interface customization
- (void)customizeAppearance {
    
    if (!IS_IPHONE) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titulo_cardapio"] forBarMetrics:UIBarMetricsDefault];
    }
    
    HTClientSettings *settings = [HTClientSettings sharedInstance];
    self.categoryLabel.textColor = [settings colorForKey:HTClientSettingsKeyBgTextColor];
    self.categoryActivityIndicator.color = [settings colorForKey:HTClientSettingsKeyBgTextColor];
    self.popoverColor = [settings colorForKey:HTClientSettingsKeyAccentColor];
}

- (void)showCategoryActivityIndicator {
    
    // Hide the category controls and show the indicator
    [self.categoryActivityIndicator startAnimating];
    self.categoryButton.enabled = NO;
    self.categoryLabel.hidden = YES;
}

- (void)hideCategoryActivityIndicator {
    
    // Show up the category controls and hide the indicator
    [self.categoryActivityIndicator stopAnimating];
    self.categoryButton.enabled = YES;
    self.categoryLabel.hidden = NO;
}

- (void)loadFoodsWithCategoryFromIndex:(NSInteger)index {
    
    if (index == NSNotFound) {
        NSLog(@"Invalid food category index!");
        return;
    }
    
    HTFoodType *currentCategory = self.foodTypes[index];
    NSLog(@"currentCategory: %@", currentCategory);
    self.categoryLabel.text = currentCategory.name;
    
    // Load all foods within that category
    HTArrayBlock completionBlock = ^(NSArray *array) {
        
        self.foods = array;
        
        [self.collectionView reloadData];
        
        NSIndexPath *topIndex = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.collectionView scrollToItemAtIndexPath:topIndex
                                    atScrollPosition:UICollectionViewScrollPositionTop
                                            animated:YES];
        
        NSLog(@"Foods: %i items", [array count]);
    };
    
    MKNKErrorBlock errorBlock = ^(NSError *error) {

        self.foods = nil;
        NSLog(@"Foods error: %@", error);
    };
    
    self.currentOperation = [self.foodModel getFoodsWithType:currentCategory.foodTypeId
                                                onCompletion:completionBlock
                                                     onError:errorBlock];
}

- (IBAction)tapFoodCategory:(id)sender {
    
    // THIS IS SPARTA
    self.currentCategories.categories = self.foodTypes;
    [self.currentCategories.tableview reloadData];
    
    self.categoryContainer.hidden = !self.categoryContainer.hidden;
}

@end
