//
//  HTMenuViewController.m
//  CompanionHotel
//
//  Created by Pedro Souza on 23/03/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTMenuViewController.h"

@interface HTMenuViewController ()

- (void)customizeAppearance;

@end

@implementation HTMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self customizeAppearance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interface customization

- (void)customizeAppearance {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titulo_menu"] forBarMetrics:UIBarMetricsDefault];
    }
}

@end
