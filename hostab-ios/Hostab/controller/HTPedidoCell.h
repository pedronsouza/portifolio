//
//  HTPedidoCell.h
//  Hostab
//
//  Created by Pedro Souza on 19/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTOrderItem.h"

@interface HTPedidoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *foodImage;
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *note;

@property (strong, nonatomic) NSObject *owner;
@property (assign, nonatomic) SEL eraseButtonAction;

- (void)loadOrderItem:(HTOrderItem *)orderItem;

- (IBAction)touchedEraseButton:(id)sender;

@end
