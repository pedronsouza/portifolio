//
//  HTEditItemViewController.h
//  Hostab
//
//  Created by Pedro Souza on 19/04/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTOrder.h"

@protocol HTEditItemDelegate <NSObject>

- (void)didFinishEditingItem:(HTOrderItem *)item;

@end

@interface HTEditItemViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *foodImage;
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *foodDescription;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UITextView *note;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UITextField *quantity;
@property (strong, nonatomic) IBOutlet UILabel *totalValue;

@property (weak, nonatomic) UIView *activeField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

@property (weak, nonatomic) id<HTEditItemDelegate> delegate;

- (void)createItemWithFood:(HTFood *)food
                  forOrder:(HTOrder *)order;

- (void)editItem:(HTOrderItem *)item
       fromOrder:(HTOrder *)order;

- (IBAction)tapAdd:(id)sender;
- (IBAction)tapCancel:(id)sender;

@end
