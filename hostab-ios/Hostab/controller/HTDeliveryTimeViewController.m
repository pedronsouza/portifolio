//
//  HTDeliveryTimeViewController.m
//  Hostab
//
//  Created by Pedro Souza on 21/07/13.
//  Copyright (c) 2013 HosTab. All rights reserved.
//

#import "HTDeliveryTimeViewController.h"
#import "HTClientSettings.h"

@interface HTDeliveryTimeViewController ()

- (void)changedPickerValue:(id)sender;

@end

@implementation HTDeliveryTimeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSNumber *minimumWait = [[HTClientSettings sharedInstance] settingForKey:HTClientSettingsKeyMinimumDeliveryWait];
    
    NSDate *minimumDate = [NSDate dateWithTimeInterval:[minimumWait floatValue]
                                             sinceDate:[NSDate date]];
    
    self.datePicker.date = minimumDate;
    self.datePicker.minimumDate = minimumDate;
    [self.datePicker addTarget:self
                        action:@selector(changedPickerValue:)
              forControlEvents:UIControlEventValueChanged];
    
    if ([self.delegate respondsToSelector:@selector(didSelectTime:)]) {
        [self.delegate didSelectTime:self.datePicker.date];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changedPickerValue:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didSelectTime:)]) {
        [self.delegate didSelectTime:self.datePicker.date];
    }
}

- (IBAction)tapClose:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didClose:)]) {
        [self.delegate didClose:self];
    }
}

@end
