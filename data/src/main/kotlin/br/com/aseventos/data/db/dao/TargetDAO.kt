package br.com.aseventos.data.db.dao

import android.arch.persistence.room.*
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.MediaEntity
import br.com.aseventos.data.db.entities.TargetEntity

@Dao
interface TargetDAO {
    @Query("SELECT * FROM targets WHERE mediaId = :mediaId")
    fun getByMediaId(mediaId : Int) : List<TargetEntity>

    @Insert
    fun insert(media : TargetEntity) : Long

    @Delete
    fun remove(media : TargetEntity)

    @Update
    fun update(media : TargetEntity)

    @Query("SELECT id, mediaId FROM targets WHERE mediaId = :mediaId")
    fun getFirstByMediaId(mediaId: Int): TargetEntity?

    @Query("SELECT * FROM targets WHERE id = :targetId")
    fun getTargetWithFileBlobById(targetId: Int): TargetEntity?
}