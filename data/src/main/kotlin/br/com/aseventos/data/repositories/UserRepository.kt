package br.com.aseventos.data.repositories

import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.models.CustomTargetVideo
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import rx.Observable
import java.io.File
import javax.inject.Inject

class UserRepository {
    private var dataSource: UserDataSource
    @Inject constructor(dataSource: UserDataSource) {
        this.dataSource = dataSource
    }
    fun authenticateUserWithCredentials(email : String, password : String) =
            dataSource.authenticateUserWithCredentials(email, password)
    fun createNewUserWithEmail(email: String, ignoreValidation : Boolean): Observable<Session> =
            dataSource.validateUserEmailExistence(email, ignoreValidation)
    fun createPasswordForUser(token: String, password: String): Observable<User> =
            dataSource.createPasswordForUser(token, password)

    fun searchUsersAndDirectories(query: String): Observable<List<SearchResult>> =
            dataSource.searchUsersAndDirectories(query)

    fun generateAdminToken(): Observable<User> =
            dataSource.generateAdminToken()

    fun requestChangePasswordToken(email : String) : Observable<User> =
            dataSource.requestChangePasswordToken(email)

    fun sendCustomTargetVideo(file : File) : Observable<CustomTargetVideo> =
            dataSource.sendCustomTargetVideoToProvider(file)

    fun sendCustomTargetVideoImages(file: File, videoUrl: String): Observable<Void> =
            dataSource.sendCustomTargetVideoImages(file, videoUrl)

}