package br.com.aseventos.data.usecases.users

import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.domain.models.CustomTargetVideo
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import java.io.File
import javax.inject.Inject


class UploadCustomTargetVideoUseCase : BaseUseCase<CustomTargetVideo> {

    private val userRepository: UserRepository
    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        userRepository: UserRepository) : super(threadExecutor, postExecutionThread) {
        this.userRepository = userRepository
    }

    var imageFile : File? = null

    override fun observableFor(): Observable<CustomTargetVideo> =
            this.userRepository.sendCustomTargetVideo(imageFile!!)


}