package br.com.aseventos.data.db.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "medias",
        foreignKeys = [ForeignKey(entity = DemoEntity::class,
                                    parentColumns = arrayOf("id"),
                                    childColumns = arrayOf("demoId"),
                                    onDelete = CASCADE)]
)

data class MediaEntity(
        @PrimaryKey(autoGenerate = true)
        @NotNull
        var id : Int,

        @NotNull
        var demoId : Int,

        @NotNull
        var sourceUri : String,

        @NotNull
        var status : String,

        @NotNull
        var iSCustomTarget : Boolean,

        var localPathUri : String?) {

        constructor() : this(0, 0, "", "", false, "")
}