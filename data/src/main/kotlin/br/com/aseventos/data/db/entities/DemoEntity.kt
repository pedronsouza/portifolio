package br.com.aseventos.data.db.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "demos")
data class DemoEntity(
        @PrimaryKey(autoGenerate = true)
        @NotNull
        var id : Int,
        @NotNull
        var baseSystemId : String,

        @NotNull
        var name : String,

        var localConfigPath : String? = null,

        @NotNull
        var status : String = STATUS_IDLE) {

    constructor() : this(0, "", "", "")
    companion object {
        val STATUS_IDLE = "idle"
        val STATUS_DOWNLOADING = "downloading"
        val STATUS_STORED = "stored"
        val STATUS_ERROR = "error"
    }
}