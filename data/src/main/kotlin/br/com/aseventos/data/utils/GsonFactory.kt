package br.com.aseventos.data.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder

class GsonFactory {
    companion object {
        fun create() : Gson {
            return GsonBuilder().create()
        }
    }
}