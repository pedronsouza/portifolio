package br.com.aseventos.data.net.v2.endpoints

import br.com.aseventos.data.net.helpers.DataResponse
import br.com.aseventos.data.net.v2.helpers.SaveUserPasswordHelper
import br.com.aseventos.data.net.v2.helpers.ValidateUserEmailHelper
import br.com.aseventos.domain.models.v2.*
import retrofit2.http.*
import rx.Observable

interface UserV2Endpoints {
    @GET("/users/files")
    fun findUserFiles(@Query("id") baseSystemId : String) : Observable<DataResponse<List<AppMedia>>>

    @GET("/users/find")
    fun findUserByQuery(@Query("query") query : String) : Observable<DataResponse<AppUser>>

    @POST("/users/save")
    fun saveUserPassword(@Body helper : SaveUserPasswordHelper) : Observable<DataResponse<AppSession>>

    @POST("/users/validate")
    fun validateUserEmail(@Body helper : ValidateUserEmailHelper) : Observable<DataResponse<AppCreationToken>>

    @POST("/users/authorize")
    fun authorizeUserV2(@Body helper : AuthorizeUserV2Helper) : Observable<DataResponse<AppSession>>
}