package br.com.aseventos.data.net.endpoints

import br.com.aseventos.data.net.helpers.*
import br.com.aseventos.domain.models.*

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import rx.Observable

interface UserEndpoints {
    @POST("auth")
    fun authenticateUserWithCredentials(@Body authBodyHelper : AuthBodyHelper) : Observable<DefaultHttpResponse<User>>

    @POST("admin/auth")
    fun generateAdminToken(@Body authBodyHelper: AdminAuthBodyHelper) : Observable<DefaultHttpResponse<User>>

    @POST("user/new")
    fun validateUserEmailExistence(@Body authBodyHelper: AuthBodyHelper) : Observable<DefaultHttpResponse<Session>>

    @POST("user/new-password")
    fun createPasswordForUser(@Query("token") token : String, @Body user : User) : Observable<DefaultHttpResponse<User>>

    @GET("search")
    fun searchUsersAndDirectories(@Query("searchParam") searchParam : String) : Observable<DefaultHttpResponse<GeneralSearchBodyHelper>>

    @POST("auth/request-password")
    fun requestChangePasswordToken(@Body requestTokenHelper: RequestTokenHelper) : Observable<DefaultHttpResponse<User>>

    @POST("videos/upload")
    @Multipart
    fun sendCustomTargetVideoToProvider(@Part filePart : MultipartBody.Part) : Observable<DefaultHttpResponse<CustomTargetVideo>>

    @POST("photos/upload")
    @Multipart
    fun sendCustomTargetVideoImages(@Part filePart : MultipartBody.Part, @Part("videoUrl") videoUrl : RequestBody) : Observable<Void>


    @POST("videos/upload")
    @Multipart
    fun sendCustomTargetVideoToProviderSync(@Part filePart : MultipartBody.Part) : Call<DefaultHttpResponse<CustomTargetVideo>>

    @POST("photos/upload")
    @Multipart
    fun sendCustomTargetVideoImagesSync(@Part filePart : MultipartBody.Part, @Part("videoUrl") videoUrl : RequestBody) : Call<DefaultHttpResponse<PhotoUploadHelper>>

    @POST("photos/upload")
    @Multipart
    fun sendCustomTargetVideoImagesSyncMultiplie(@Part filePart : List<MultipartBody.Part>, @Part("videoUrl") videoUrl : String) : Call<DefaultHttpResponse<PhotoUploadHelper>>

    @POST("/recon")
    fun saveCustomTargetInfo(@Body info : CustomTargetInfoHelper) : Call<DefaultHttpResponse<IdModel>>
}