package br.com.aseventos.data.db.dao

import android.arch.persistence.room.*
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.MediaEntity

@Dao
interface MediaDAO {
    @Query("SELECT * FROM medias")
    fun getAll() : List<MediaEntity>

    @Query("SELECT * FROM medias WHERE demoId = :demoId")
    fun getByDemoId(demoId : Int) : List<MediaEntity>

    @Query("SELECT * FROM medias WHERE demoId = :demoId LIMIT 1")
    fun getFirstByDemoId(demoId: Int) : MediaEntity?

    @Query("SELECT * FROM medias WHERE id = :id LIMIT 1")
    fun getById(id : Int) : MediaEntity?

    @Insert
    fun insert(media : MediaEntity) : Long

    @Delete
    fun remove(media : MediaEntity)

    @Update
    fun update(media : MediaEntity)

    @Query("SELECT * FROM medias WHERE demoId = :id")
    fun getMediasForDemoId(id: Int): List<MediaEntity>

    @Query("SELECT * FROM medias WHERE status != :status AND demoId = :demoId")
    abstract fun getNotCompletedByDemoId(demoId: Int, status : String = DemoEntity.STATUS_STORED): List<MediaEntity>

    @Query("UPDATE medias SET status=:status WHERE id = :reconId")
    fun setStatusById(status: String, reconId: Int)

    @Query("DELETE FROM medias WHERE demoId = :demoId")
    fun removeByDemoId(demoId: Int)

}