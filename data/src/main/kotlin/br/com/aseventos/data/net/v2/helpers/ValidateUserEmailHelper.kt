package br.com.aseventos.data.net.v2.helpers

import com.google.gson.annotations.Expose

data class ValidateUserEmailHelper(@Expose val email : String)