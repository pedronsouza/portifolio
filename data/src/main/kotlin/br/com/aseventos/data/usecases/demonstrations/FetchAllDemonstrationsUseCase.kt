package br.com.aseventos.data.usecases.demonstrations

import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class FetchAllDemonstrationsUseCase : BaseUseCase<List<DemoEntity>> {
    private val appDatabase: ReconDatabase?
    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        appDatabase: ReconDatabase?) : super(threadExecutor, postExecutionThread) {
        this.appDatabase = appDatabase

    }

    override fun observableFor(): Observable<List<DemoEntity>> =
            Observable.just(appDatabase?.demoDao()?.getAll())
}