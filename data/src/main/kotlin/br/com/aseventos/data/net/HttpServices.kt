package br.com.aseventos.data.net

import br.com.aseventos.data.net.endpoints.DirectoryEndpoints
import br.com.aseventos.data.net.endpoints.UserEndpoints
import retrofit2.Retrofit

class HttpServices(private val retrofit: Retrofit) {
    val user by lazy { retrofit.create(UserEndpoints::class.java) }
    val media by lazy { retrofit.create(DirectoryEndpoints::class.java) }
}