package br.com.aseventos.data.net.helpers

import com.google.gson.annotations.Expose

data class ResultHelper<T>(@Expose val result : T) {
}