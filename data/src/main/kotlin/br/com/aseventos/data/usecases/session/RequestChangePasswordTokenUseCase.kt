package br.com.aseventos.data.usecases.session

import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class RequestChangePasswordTokenUseCase : BaseUseCase<User> {

    private val userRepository: UserRepository

    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        userRepository: UserRepository) : super(threadExecutor, postExecutionThread) {
        this.userRepository = userRepository
    }

    var email : String? = null

    override fun observableFor(): Observable<User> =
            userRepository.requestChangePasswordToken(email!!)
}