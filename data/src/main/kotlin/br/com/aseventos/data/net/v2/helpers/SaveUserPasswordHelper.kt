package br.com.aseventos.data.net.v2.helpers

import com.google.gson.annotations.Expose

data class SaveUserPasswordHelper(@Expose val password_creation_token : String, @Expose val password : String)