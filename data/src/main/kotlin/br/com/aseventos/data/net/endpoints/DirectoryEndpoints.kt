package br.com.aseventos.data.net.endpoints

import br.com.aseventos.data.net.helpers.DefaultHttpResponse
import br.com.aseventos.data.net.helpers.ResultHelper
import br.com.aseventos.domain.models.Directory
import br.com.aseventos.domain.models.Recon
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable


interface DirectoryEndpoints {
    @GET("search/recons")
    fun getAllMediasFromCategory(@Query("category") categoryId : String?,
                                 @Query("collegeClass") collegeClass : String?,
                                 @Query("user") userId : String?) : Observable<DefaultHttpResponse<ResultHelper<List<Recon>>>>
}