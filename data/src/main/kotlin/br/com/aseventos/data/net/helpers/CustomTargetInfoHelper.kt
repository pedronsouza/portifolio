package br.com.aseventos.data.net.helpers

import com.google.gson.annotations.Expose

data class CustomTargetInfoHelper(@Expose val video : String,
                                  @Expose val photos : List<String>)