package br.com.aseventos.data.local

import android.content.SharedPreferences
import br.com.aseventos.domain.models.Demonstration
import br.com.aseventos.domain.models.Recon
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import javax.inject.Inject


class DemonstrationManager {
    private  val gson : Gson
    private  val sharedPreferences: SharedPreferences

    @Inject constructor(gson : Gson,
                        sharedPreferences: SharedPreferences) {
        this.gson = gson
        this.sharedPreferences = sharedPreferences
        load()
    }

    private val DEMOS_KEY = "demos_key"

    lateinit var demonstrations : MutableList<Demonstration>


    private fun load() {
        val rawDemos = sharedPreferences.getString(DEMOS_KEY, null)
        demonstrations = if (rawDemos != null) {
            val listType = object : TypeToken<MutableList<Demonstration>>() {}.type
            gson.fromJson(rawDemos, listType)
        } else {
            arrayListOf()
        }
    }

    private fun store() {
        val raw = gson.toJson(demonstrations)
        sharedPreferences.edit()
                .putString(DEMOS_KEY, raw)
                .apply()
    }

    fun findDemoById(id : String) : Demonstration? =
        demonstrations.firstOrNull {it.id.contentEquals(id) }

    fun fetchStoredDemos() : List<Demonstration> =
            demonstrations.filter { it.status == Demonstration.Status.STORED }


    fun add(demo : Demonstration) {
        if (findDemoById(demo.id) == null) {
            demonstrations.plusAssign(demo)
            store()
        }
    }

    fun save(demo : Demonstration) {
        val d = findDemoById(demo.id)
        if (d != null) {
            val i = demonstrations.indexOf(d)
            demonstrations[i] = demo
        } else {
            add(demo)
        }

        store()
    }

    fun saveItensForDemo(demonstration: Demonstration, recon : List<Recon>) {
        val indexOf = demonstrations.indexOf(demonstration)
        if (indexOf != -1) {
            demonstration.items = recon.toMutableList()
            demonstrations[indexOf] = demonstration
            store()
        }
    }

    fun remove(demo : Demonstration) {
        val target = demonstrations.firstOrNull { it.id.contentEquals(demo.id) }
        if(target != null) {
            demonstrations.remove(target)
            store()
        }
    }

}