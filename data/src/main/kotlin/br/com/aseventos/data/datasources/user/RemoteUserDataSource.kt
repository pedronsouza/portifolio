package br.com.aseventos.data.datasources.user

import br.com.aseventos.data.net.HttpServices
import br.com.aseventos.data.net.helpers.AdminAuthBodyHelper
import br.com.aseventos.data.net.helpers.AuthBodyHelper
import br.com.aseventos.data.net.helpers.RequestTokenHelper
import br.com.aseventos.data.net.v2.APIV2Services
import br.com.aseventos.data.net.v2.helpers.SaveUserPasswordHelper
import br.com.aseventos.data.net.v2.helpers.ValidateUserEmailHelper
import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.models.CustomTargetVideo
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.models.v2.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.Observable
import java.io.File
import javax.inject.Inject

class RemoteUserDataSource : UserDataSource {
    override fun searchUsersAndDirectories(query: String): Observable<List<SearchResult>> =
            this.httpServices.user.searchUsersAndDirectories(query)
                    .flatMap { Observable.just(it.data?.result) }



    private var httpServices: HttpServices
    private var apiV2Services : APIV2Services

    @Inject constructor(httpServices: HttpServices, apiV2Services : APIV2Services) {
        this.httpServices = httpServices
        this.apiV2Services = apiV2Services
    }

    override fun authenticateUserWithCredentials(email: String, password: String): Observable<User> =
            httpServices.user.authenticateUserWithCredentials(AuthBodyHelper(email, password))
                    .flatMap { Observable.just(it.data) }

    override fun validateUserEmailExistence(email: String, ignoreValidation : Boolean): Observable<Session> =
            httpServices.user.validateUserEmailExistence(AuthBodyHelper(email, null))
                    .flatMap { Observable.just(it.data) }

    override fun createPasswordForUser(token: String, password: String): Observable<User> =
            httpServices.user.createPasswordForUser(token, User(null, null, null, null, password, null, null))
                    .flatMap { Observable.just(it.data)  }

    override fun generateAdminToken(): Observable<User> =
            httpServices.user.generateAdminToken(AdminAuthBodyHelper()).flatMap { Observable.just(it.data) }

    override fun requestChangePasswordToken(email : String): Observable<User> =
            httpServices.user.requestChangePasswordToken(RequestTokenHelper(email)).flatMap { Observable.just(it.data) }

    override fun sendCustomTargetVideoToProvider(file: File): Observable<CustomTargetVideo> {
        val filepart = MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MediaType.parse("video/mp4"), file))
        return httpServices.user.sendCustomTargetVideoToProvider(filepart).flatMap { Observable.just(it.data) }
    }

    override fun sendCustomTargetVideoImages(file: File, videoUrl: String): Observable<Void> {
        val rqBody = RequestBody.create(MediaType.parse("image/jpeg"), file)
        val imgPart = MultipartBody.Part.createFormData("file", file.name, rqBody)
        val videoUrlPart = MultipartBody.create(okhttp3.MultipartBody.FORM, videoUrl)


        return httpServices.user.sendCustomTargetVideoImages(imgPart, videoUrlPart)
    }

    override fun findUserFiles(baseSystemId: String): Observable<List<AppMedia>> {
        return apiV2Services.user.findUserFiles(baseSystemId).flatMap { Observable.just(it.data) }
    }

    override fun findUserByQuery(query: String): Observable<AppUser> {
        return apiV2Services.user.findUserByQuery(query).flatMap { Observable.just(it.data) }
    }

    override fun saveUserPassword(password: String, passwordCreationToken: String): Observable<AppSession> {
        return apiV2Services.user.saveUserPassword(SaveUserPasswordHelper(passwordCreationToken, password)).flatMap { Observable.just(it.data) }
    }

    override fun validateUserEmail(email: String): Observable<AppCreationToken> {
        return apiV2Services.user.validateUserEmail(ValidateUserEmailHelper(email)).flatMap { Observable.just(it.data) }
    }

    override fun authorizeUserV2(email: String, password: String): Observable<AppSession> {
        return apiV2Services.user.authorizeUserV2(AuthorizeUserV2Helper(email, password)).flatMap { Observable.just(it.data) }
    }
}