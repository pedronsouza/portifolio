package br.com.aseventos.data.db.dao

import android.arch.persistence.room.*
import br.com.aseventos.data.db.entities.DemoEntity

@Dao
interface DemoDAO {
    @Query("SELECT * FROM demos")
    fun getAll() : List<DemoEntity>

    @Query("SELECT * FROM demos WHERE status = :status")
    fun getByStatus(status : String) : List<DemoEntity>

    @Query("SELECT * FROM demos WHERE id = :id")
    fun getById(id : Int) : DemoEntity

    @Query("SELECT * FROM demos WHERE baseSystemId = :id")
    fun getByBaseSystemId(id : String) : DemoEntity?

    @Query("SELECT * FROM demos LIMIT 1")
    fun getFirstDemo() : DemoEntity

    @Query("DELETE FROM demos WHERE id = :demoId")
    fun removeById(demoId : Int)

    @Query("UPDATE demos SET status=:status WHERE id = :demoId")
    fun setStatusForDemo(status: String, demoId: Int)

    @Insert
    fun insert(demo : DemoEntity) : Long

    @Delete
    fun remove(demo : DemoEntity)

    @Update
    fun update(demo: DemoEntity)

    @Query("SELECT * FROM demos WHERE baseSystemId = :searchItemId")
    fun findByBaseSystemId(searchItemId: String) : DemoEntity?

    @Query("SELECT * FROM demos LIMIT 1")
    fun getFirstOrNull() : DemoEntity?
}