package br.com.aseventos.data.net.helpers

import br.com.aseventos.domain.models.CustomTargetPhoto
import com.google.gson.annotations.Expose

data class PhotoUploadHelper(@Expose val photos : List<CustomTargetPhoto>)