package br.com.aseventos.data.net.helpers

import com.google.gson.annotations.Expose

data class AuthBodyHelper(@Expose val email : String, @Expose val password : String?)