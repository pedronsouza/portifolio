package br.com.aseventos.data.net.v2

import br.com.aseventos.data.net.v2.endpoints.UserV2Endpoints
import retrofit2.Retrofit

class APIV2Services(private val retrofit : Retrofit) {
    val user by lazy { retrofit.create(UserV2Endpoints::class.java) }
}