package br.com.aseventos.data.usecases.session

import br.com.aseventos.data.BuildConfig
import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class CheckUserEmailOnServerUseCase : BaseUseCase<Session> {
    var email : String? = null
    private var userRepository : UserRepository

    @Inject
    constructor(threadExecutor: ThreadExecutor,
                postExecutionThread: PostExecutionThread,
                userRepository : UserRepository) : super(threadExecutor, postExecutionThread) {
        this.userRepository = userRepository
    }

    override fun observableFor(): Observable<Session> =
            userRepository.createNewUserWithEmail(email!!, BuildConfig.IGNORE_EMAIL_VALIDATION)
}