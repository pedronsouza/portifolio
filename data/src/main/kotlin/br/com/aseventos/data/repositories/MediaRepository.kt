package br.com.aseventos.data.repositories


import br.com.aseventos.domain.datasources.media.MediaDataSource
import br.com.aseventos.domain.models.Directory
import br.com.aseventos.domain.models.Recon
import rx.Observable
import javax.inject.Inject

class MediaRepository {
    private val mediaDataSource: MediaDataSource

    @Inject constructor(mediaDataSource: MediaDataSource) {
        this.mediaDataSource = mediaDataSource
    }


    fun getAllMediasFromDirectory(id : String) : Observable<List<Recon>> =
            this.mediaDataSource.getAllMediasFromDirectory(id)

    fun getAllMediasFromUser(id : String) : Observable<List<Recon>> =
            this.mediaDataSource.getAllMediasFromUser(id)

}