package br.com.aseventos.data.datasources.media

import br.com.aseventos.data.net.HttpServices
import br.com.aseventos.domain.datasources.media.MediaDataSource
import br.com.aseventos.domain.models.Directory
import br.com.aseventos.domain.models.Recon
import rx.Observable
import javax.inject.Inject


class RemoteMediaDataSource : MediaDataSource {
    private val httpServices: HttpServices
    @Inject constructor(httpServices: HttpServices) {
        this.httpServices = httpServices
    }

    override fun getAllMediasFromDirectory(id: String): Observable<List<Recon>> =
            httpServices.media.getAllMediasFromCategory(id, null, null).flatMap { Observable.just(it.data?.result) }

    override fun getAllMediasFromUser(userId: String): Observable<List<Recon>> =
            httpServices.media.getAllMediasFromCategory(null, null, userId).flatMap { Observable.just(it.data?.result) }
}