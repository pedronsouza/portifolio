package br.com.aseventos.data.usecases.v2

import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.models.v2.AppCreationToken
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class ValidateUserEmailCase : BaseUserCase<AppCreationToken> {

    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        userDataSource : UserDataSource) : super(threadExecutor, postExecutionThread, userDataSource)

    var email : String? = null

    override fun observableFor(): Observable<AppCreationToken> = userDataSource.validateUserEmail(email!!)
}