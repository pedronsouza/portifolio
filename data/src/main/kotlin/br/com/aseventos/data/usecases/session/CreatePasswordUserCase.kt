package br.com.aseventos.data.usecases.session

import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class CreatePasswordUserCase : BaseUseCase<User> {
    private var userRepository : UserRepository
    var password : String? = null
    var token : String? = null

    @Inject constructor(userRepository: UserRepository, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : super(threadExecutor, postExecutionThread) {
        this.userRepository = userRepository
    }

    override fun observableFor(): Observable<User> =
            this.userRepository.createPasswordForUser(token!!, password!!)
}