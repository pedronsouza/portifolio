package br.com.aseventos.data.net.helpers

import com.google.gson.annotations.Expose

data class VideoUploadResponseHelper(@Expose val videoId : String, @Expose val url : String)