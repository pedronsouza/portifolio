package br.com.aseventos.data.net.helpers

import com.google.gson.annotations.Expose

data class RequestTokenHelper(@Expose val email : String)