package br.com.aseventos.data.net.helpers

import com.google.gson.annotations.Expose

data class DataResponse<T>(@Expose val data : T)