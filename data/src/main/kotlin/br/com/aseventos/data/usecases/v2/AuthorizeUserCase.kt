package br.com.aseventos.data.usecases.v2

import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.models.v2.AppSession
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class AuthorizeUserCase : BaseUserCase<AppSession> {

    @Inject
    constructor(threadExecutor: ThreadExecutor,
                postExecutionThread: PostExecutionThread,
                userDataSource : UserDataSource) : super(threadExecutor, postExecutionThread, userDataSource)

    var password : String? = null
    var email : String? = null

    override fun observableFor(): Observable<AppSession> =
            userDataSource.authorizeUserV2(email!!, password!!)
}