package br.com.aseventos.data.usecases.demonstrations

import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class FetchDemonstrationBySearchItemIdUseCase : BaseUseCase<DemoEntity?> {
    private val appDatabase: ReconDatabase?
    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        appDatabase: ReconDatabase?) : super(threadExecutor, postExecutionThread) {
        this.appDatabase = appDatabase
    }

    var searchItemId : String? = null

    override fun observableFor(): Observable<DemoEntity?> =
            Observable.fromCallable {
                appDatabase?.demoDao()?.findByBaseSystemId(searchItemId!!)
            }
}