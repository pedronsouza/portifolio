package br.com.aseventos.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import br.com.aseventos.data.db.dao.DemoDAO
import br.com.aseventos.data.db.dao.MediaDAO
import br.com.aseventos.data.db.dao.TargetDAO
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.MediaEntity
import br.com.aseventos.data.db.entities.TargetEntity

@Database(entities = [DemoEntity::class, MediaEntity::class, TargetEntity::class], version = 3)
abstract class ReconDatabase : RoomDatabase() {
    abstract fun demoDao()  : DemoDAO
    abstract fun mediaDao() : MediaDAO
    abstract fun targetDao() : TargetDAO

    companion object {
        private var INSTANCE : ReconDatabase? = null

        fun getInstance(context : Context) : ReconDatabase? {
            if (INSTANCE == null) {
                synchronized(ReconDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context, ReconDatabase::class.java, "as-recon.db")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }

            return INSTANCE
        }
    }

    fun destroyInstance() {
        INSTANCE = null
    }
}