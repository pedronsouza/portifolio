package br.com.aseventos.data.usecases.session

import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import javax.inject.Inject
import rx.Observable

class GenerateAdminTokenUseCase : BaseUseCase<User>  {

    private val userRepository: UserRepository
    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        userRepository: UserRepository) : super(threadExecutor, postExecutionThread) {
        this.userRepository = userRepository
    }


    override fun observableFor(): Observable<User> =
            userRepository.generateAdminToken()
}