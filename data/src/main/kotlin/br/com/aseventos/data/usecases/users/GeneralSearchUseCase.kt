package br.com.aseventos.data.usecases.users

import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject


class GeneralSearchUseCase : BaseUseCase<List<SearchResult>>{

    private val userRepository: UserRepository
    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        userRepository: UserRepository) : super(threadExecutor, postExecutionThread) {
       this.userRepository = userRepository
    }
    var query : String? = null

    override fun observableFor(): Observable<List<SearchResult>> =
        userRepository.searchUsersAndDirectories(query!!)

}