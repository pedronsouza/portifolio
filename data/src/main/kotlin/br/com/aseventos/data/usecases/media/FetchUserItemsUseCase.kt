package br.com.aseventos.data.usecases.media

import br.com.aseventos.data.repositories.MediaRepository
import br.com.aseventos.domain.models.Directory
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class FetchUserItemsUseCase: BaseUseCase<Directory> {
    private val mediaRepository : MediaRepository
    var itemId : String? = null

    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        mediaRepository : MediaRepository) : super(threadExecutor, postExecutionThread) {
        this.mediaRepository = mediaRepository
    }

    override fun observableFor(): Observable<Directory> {
        return this.mediaRepository.getAllMediasFromUser(itemId!!).flatMap { Observable.just(Directory(it.first().category.id, it.first().category.name, it)) }
    }
}