package br.com.aseventos.data.usecases.executors

import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.RejectedExecutionHandler
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BackgroundThreadExecutor : ThreadExecutor {
    @Inject constructor() {
        this.threadPoolExecutor = ThreadPoolExecutor(corePoolSize, maxPoolSize,
                keepAliveTime, timeUnit, workQueue)
    }

    private val corePoolSize = 3
    private val maxPoolSize = 5
    private val keepAliveTime = 120L
    private val timeUnit = TimeUnit.SECONDS
    private val workQueue = LinkedBlockingQueue<Runnable>()
    private val rejectedExecutionHandler = RejectedExecutionHandler { _, _ -> threadPoolExecutor!!.shutdown() }
    private var threadPoolExecutor: ThreadPoolExecutor? = null


    override fun execute(interactor: Runnable?) {
        if (interactor == null) {
            throw IllegalArgumentException("Interactor must not be null")
        }
        threadPoolExecutor?.rejectedExecutionHandler = rejectedExecutionHandler
        threadPoolExecutor?.submit(interactor)
    }
}