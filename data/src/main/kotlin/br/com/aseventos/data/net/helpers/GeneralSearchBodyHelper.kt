package br.com.aseventos.data.net.helpers

import br.com.aseventos.domain.models.SearchResult
import com.google.gson.annotations.Expose

data class GeneralSearchBodyHelper(@Expose val result : List<SearchResult>)