package br.com.aseventos.data.usecases.v2

import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.models.v2.AppMedia
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject


class FindUserFilesCase : BaseUserCase<List<AppMedia>>  {

    @Inject constructor(threadExecutor: ThreadExecutor,
                        postExecutionThread: PostExecutionThread,
                        userDataSource : UserDataSource) : super(threadExecutor, postExecutionThread, userDataSource)

    var baseSystemId : String? = null
    override fun observableFor(): Observable<List<AppMedia>> =
            userDataSource.findUserFiles(baseSystemId!!)
}