package br.com.aseventos.data.usecases.session

import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import javax.inject.Inject

class AuthenticateUserUseCase : BaseUseCase<User> {
    private var userRepository : UserRepository
    var email : String? = null
    var password : String? = null

    @Inject
    constructor(threadExecutor: ThreadExecutor,
                postExecutionThread: PostExecutionThread,
                userRepository : UserRepository) : super(threadExecutor, postExecutionThread) {
        this.userRepository = userRepository
    }

    override fun observableFor(): Observable<User> =
            this.userRepository.authenticateUserWithCredentials(email!!, password!!)
}