package br.com.aseventos.data.usecases.executors

import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class MainThreadExecutor : PostExecutionThread {
    @Inject constructor()
    override fun scheduler(): Scheduler = AndroidSchedulers.mainThread()
}