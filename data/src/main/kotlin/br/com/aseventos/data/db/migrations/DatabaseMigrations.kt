package br.com.aseventos.data.db.migrations

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.migration.Migration

object DatabaseMigrations {
    val MIGRATION_ADD_MEDIATYPE_TO_MEDIA_ENTITIY = object: Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE medias ADD COLUMN iSCustomTarget INTEGER NOT NULL DEFAULT 0")
        }
    }
}