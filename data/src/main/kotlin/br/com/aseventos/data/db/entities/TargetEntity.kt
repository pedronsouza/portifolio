package br.com.aseventos.data.db.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "targets",
        foreignKeys = [ForeignKey(entity = MediaEntity::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("mediaId"),
                onDelete = ForeignKey.CASCADE)])
data class TargetEntity(
        @PrimaryKey(autoGenerate = true)
        @NotNull
        var id : Int,

        @NotNull
        var mediaId : Int,

        @NotNull
        var fileBlob : String?) {
    constructor() : this (0,0, "")
}