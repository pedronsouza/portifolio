package br.com.aseventos.data.usecases.v2

import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.usecases.BaseUseCase
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor


abstract class BaseUserCase<T> : BaseUseCase<T> {
    protected var userDataSource : UserDataSource

    constructor(threadExecutor: ThreadExecutor,
                postExecutionThread: PostExecutionThread,
                userDataSource: UserDataSource) : super(threadExecutor, postExecutionThread) {
        this.userDataSource = userDataSource
    }
}