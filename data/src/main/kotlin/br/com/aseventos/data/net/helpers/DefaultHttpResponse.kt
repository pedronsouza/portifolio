package br.com.aseventos.data.net.helpers

import com.google.gson.annotations.Expose

data class DefaultHttpResponse<T>(@Expose val message : String?, @Expose val data : T?)