package br.com.aseventos.data.adapters

import android.content.Context
import br.com.aseventos.domain.adapters.TrackerAdapter
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker

class GATrackerAdapter : TrackerAdapter {
    private lateinit var context : Context
    private lateinit var trackerId : String
    private var gaClient : GoogleAnalytics? = null
    private var gaTracker : Tracker? = null

    constructor(context : Context, trackerId : String) {
        this.context = context
        this.trackerId = trackerId
    }

    override fun initTracker() : TrackerAdapter {
        this.gaClient = GoogleAnalytics.getInstance(context)
        this.gaTracker = gaClient?.newTracker(trackerId)
        return this
    }

    override fun trackPageView(pageName: String) {
        this.gaTracker?.setScreenName(pageName)
        this.gaTracker?.send(HitBuilders.ScreenViewBuilder().build())
    }

    override fun trackPageView(pageName: TrackerAdapter.PageName) {
        this.gaTracker?.setScreenName(pageName.text)
        this.gaTracker?.send(HitBuilders.ScreenViewBuilder().build())
    }

    override fun trackEvent(category: TrackerAdapter.EventCategory,
                            action : TrackerAdapter.EventName,
                            label : String?) {
        this.gaTracker?.send(HitBuilders.EventBuilder()
                .setCategory(category.text)
                .setLabel(label)
                .setAction(action.text)
                .build())
    }
}