package br.com.aseventos.domain.models.v2

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AppUser(
        @Expose @SerializedName("base_system_id") val baseSystemId : String,
        @Expose val name : String,
        @Expose val email : String,
        @Expose var course : String?,
        @Expose val college : String?,
        @Expose @SerializedName("photo_url") val photoUrl: String?,
        @Expose @SerializedName("free_trial_eligible") val freeTrialEligible : Boolean
) {
    init {
        if (this.college != null) {
            this.course = this.college
        }
    }
}