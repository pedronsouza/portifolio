package br.com.aseventos.domain.adapters


interface TrackerAdapter {
    fun trackPageView(pageName : PageName)
    fun trackPageView(pageName : String)
    fun trackEvent(category : EventCategory, action : EventName, label : String?)
    fun initTracker() : TrackerAdapter

    enum class PageName(val text : String) {
        SIGN_UP_EMAIL_VALIDATION("SIGN_UP_EMAIL_VALIDATION"),
        SIGN_UP_PWD_CREATION("SIGN_UP_PWD_CREATION"),
        SIGN_IN_FORM("SIGN_IN_FORM"),
        CHOOSE_SIGN_OPTION("CHOOSE_SIGN_OPTION"),
        SPLASH("SPLASH"),
        CAMERA_HOME("CAMERA_HOME"),
        MEDIA_LIST("MEDIA_LIST"),
        SEARCH_ITEMS("SEARCH_ITEMS"),
        SEARCH_RESULT_DETAIL("SEARCH_RESULT_DETAIL"),
        RECON_DETAIL("RECON_DETAIL"),
        DOWNLOAD("DOWNLOAD")
    }

    enum class EventCategory(val text : String) {
        ACTION("ACTION"),
        RESULT("RESULT"),
        NAVIGATION("NAVIGATION")
    }

    enum class EventName(val text : String) {
        NEW_ACCOUNT("NEW_ACCOUNT"),
        NEW_ACCOUNT_VALID_EMAIL_S("NEW_ACCOUNT_VALID_EMAIL_S"),
        NEW_ACCOUNT_VALID_EMAIL_E("NEW_ACCOUNT_VALID_EMAIL_E"),
        SIGN_IN("SIGN_IN"),
        ANON_SIGN_IN("ANON_SIGN_IN"),
        TARGET_RECON("TARGET_RECON"),
        TARGET_CREATED("TARGET_RECON"),
        USER_SEARCHED("USER_SEARCHED"),
        NEW_DEMO("NEW_DEMO"),
        NEW_DEMO_SYNC("NEW_DEMO_SYNC"),
        DEMO_LOAD("NEW_DEMO_LOAD"),
        DEMO_REMOVE("DEMO_REMOVE"),
        TEMPORARY_ACCESS("TEMPORARY_ACCESS"),
        VIDEO_PLAY("DETAIL_VIDEO_PLAY"),
        TARGET_VIEW("DETAIL_TARGET_VIEW"),
        TARGET_SHARED("TARGET_SHARED"),
        BOARDING_OPEN("BOARDING_OPEN"),
        BOARDING_DISMISS("BOARDING_DISMISS"),
        BOARDING_DONE("BOARDING_DONE")
    }
}