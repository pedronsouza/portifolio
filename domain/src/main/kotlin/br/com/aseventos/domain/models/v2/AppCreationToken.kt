package br.com.aseventos.domain.models.v2

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AppCreationToken(@Expose @SerializedName("password_creation_token") val passwordCreationToken: String)