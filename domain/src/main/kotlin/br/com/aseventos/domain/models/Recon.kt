package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Recon(@Expose @SerializedName("_id") val _id : String,
                 @Expose val category : Directory,
                 @Expose val video : MediaInfo,
                 @Expose val lastUpdated : String,
                 @Expose var stored : Boolean = false,
                 @Expose var file : String? = null,
                 @Expose val user : String?,
                 @Expose val photos : List<Target>)