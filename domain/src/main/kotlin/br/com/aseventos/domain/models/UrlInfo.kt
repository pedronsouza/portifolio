package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class UrlInfo(@Expose val uri : String) {
    fun extractId() : String
         = uri.split("/")[2]

}