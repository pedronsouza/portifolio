package br.com.aseventos.domain.execptions.validations

class PasswordNotMatchException : Throwable()