package br.com.aseventos.domain.di

import javax.inject.Scope

/**
 * Created by pedronsouza on 07/10/17.
 */
@Scope
@Retention
annotation class ActivityScope