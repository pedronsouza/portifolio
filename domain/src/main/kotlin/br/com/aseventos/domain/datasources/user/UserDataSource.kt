package br.com.aseventos.domain.datasources.user

import br.com.aseventos.domain.models.CustomTargetVideo
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.models.v2.AppCreationToken
import br.com.aseventos.domain.models.v2.AppMedia
import br.com.aseventos.domain.models.v2.AppSession
import br.com.aseventos.domain.models.v2.AppUser
import rx.Observable
import java.io.File

interface UserDataSource {
    fun authenticateUserWithCredentials(email : String, password : String) : Observable<User>
    fun validateUserEmailExistence(email: String, ignoreValidation : Boolean): Observable<Session>
    fun createPasswordForUser(token : String, password: String) : Observable<User>
    fun searchUsersAndDirectories(query : String) : Observable<List<SearchResult>>
    fun generateAdminToken() : Observable<User>
    fun requestChangePasswordToken(email : String) : Observable<User>
    fun sendCustomTargetVideoToProvider(file : File) : Observable<CustomTargetVideo>
    fun sendCustomTargetVideoImages(file : File, videoUrl : String) : Observable<Void>

    fun findUserFiles(baseSystemId : String) : Observable<List<AppMedia>>
    fun findUserByQuery(query : String) : Observable<AppUser>
    fun saveUserPassword(password: String, passwordCreationToken : String) : Observable<AppSession>
    fun validateUserEmail(email : String) : Observable<AppCreationToken>
    fun authorizeUserV2(email: String, password: String) : Observable<AppSession>
}