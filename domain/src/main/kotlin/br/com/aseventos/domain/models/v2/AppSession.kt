package br.com.aseventos.domain.models.v2

import com.google.gson.annotations.Expose

data class AppSession(@Expose val user: AppUser, @Expose val token : String)