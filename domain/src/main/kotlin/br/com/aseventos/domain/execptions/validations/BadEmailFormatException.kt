package br.com.aseventos.domain.execptions.validations

class BadEmailFormatException : Throwable()