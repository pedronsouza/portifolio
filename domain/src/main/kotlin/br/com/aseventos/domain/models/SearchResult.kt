package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchResult(@Expose val id : String,
                        @Expose @SerializedName("label") val title : String,
                        @Expose val description : String?,
                        @Expose val imgUrl : String?,
                        @Expose val meta : SearchResultInfo?,
                        @Expose val type : Type) {
    enum class Type {
        @SerializedName("user")
        USER,
        @SerializedName("directory")
        DIRECTORY
    }
}
