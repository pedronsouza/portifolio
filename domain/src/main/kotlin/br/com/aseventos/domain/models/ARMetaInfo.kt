package br.com.aseventos.domain.models

import com.google.gson.Gson
import com.google.gson.annotations.Expose

data class ARMetaInfo(@Expose val videoUrl : String) {
    fun toJson(gson: Gson): String =
            gson.toJson(this)
}