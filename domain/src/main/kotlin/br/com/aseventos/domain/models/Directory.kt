package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Directory(@Expose @SerializedName("_id") val id : String,
                     @Expose val name : String,
                     @Expose val recons : List<Recon>)