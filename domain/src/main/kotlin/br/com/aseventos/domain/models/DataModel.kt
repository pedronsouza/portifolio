package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class DataModel<T>(@Expose val data: T)