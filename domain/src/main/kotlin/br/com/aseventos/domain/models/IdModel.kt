package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class IdModel(@Expose val _id : String)