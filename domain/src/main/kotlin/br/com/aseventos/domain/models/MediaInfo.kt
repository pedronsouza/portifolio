package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MediaInfo(@Expose @SerializedName("_id") val _id : String,
                     @Expose @SerializedName("vimeo") val info : UrlInfo,
                     @Expose var isCustomTarget : Boolean = false,
                     @Expose var thumbnail : String)