package br.com.aseventos.domain.models.v2

import com.google.gson.annotations.Expose

class AppPhoto(@Expose val key : String, @Expose val url : String)