package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Target(@Expose @SerializedName("_id") val id : String)