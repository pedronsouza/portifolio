package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.sun.org.apache.bcel.internal.generic.FLOAD

data class ARTarget(@Expose val image : String,
                    @Expose val name : String,
                    @Expose val size : List<Float>,
                    @Expose val meta : String) {
}