package br.com.aseventos.domain.datasources.media

import br.com.aseventos.domain.models.Directory
import br.com.aseventos.domain.models.Recon
import rx.Observable

interface MediaDataSource {
    fun getAllMediasFromDirectory(id : String) : Observable<List<Recon>>
    fun getAllMediasFromUser(userId : String) : Observable<List<Recon>>
}