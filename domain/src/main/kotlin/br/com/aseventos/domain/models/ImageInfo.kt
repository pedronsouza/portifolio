package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class ImageInfo(@Expose val data : String,
                     @Expose val contentType : String,
                     @Expose var localPath : String? = null)