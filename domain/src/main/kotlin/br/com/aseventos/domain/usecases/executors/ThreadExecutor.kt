package br.com.aseventos.domain.usecases.executors

import java.util.concurrent.Executor

interface ThreadExecutor : Executor {
}