package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class ARConfiguration(@Expose val images : List<ARTarget>) {
}