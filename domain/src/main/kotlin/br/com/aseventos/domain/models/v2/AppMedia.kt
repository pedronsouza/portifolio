package br.com.aseventos.domain.models.v2

import com.google.gson.annotations.Expose

data class AppMedia(@Expose val key : String,
                    @Expose val url : String,
                    @Expose val photos : List<AppPhoto>)