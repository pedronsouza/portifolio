package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class Demonstration(@Expose val id : String,
                         @Expose val title : String,
                         @Expose var items : MutableList<Recon>,
                         @Expose var status : Status = Status.IDLE,
                         @Expose var configFileLocation : String? = null)  {
    enum class Status {
        IDLE,
        DOWNLOADING,
        STORED
    }
}