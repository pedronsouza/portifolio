package br.com.aseventos.domain.usecases.executors

import rx.Scheduler

interface PostExecutionThread {
    fun scheduler() : Scheduler
}