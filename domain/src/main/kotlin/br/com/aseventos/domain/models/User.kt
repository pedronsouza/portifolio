package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose


data class User(@Expose val id : String?,
                @Expose val name : String?,
                @Expose val course : String?,
                @Expose val email : String?,
                @Expose val password : String?,
                @Expose val token : String?,
                @Expose var photo : String?) {
}