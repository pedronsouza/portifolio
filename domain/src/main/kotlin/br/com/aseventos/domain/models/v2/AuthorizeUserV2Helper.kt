package br.com.aseventos.domain.models.v2

import com.google.gson.annotations.Expose

data class AuthorizeUserV2Helper(@Expose val email : String, @Expose val password : String)