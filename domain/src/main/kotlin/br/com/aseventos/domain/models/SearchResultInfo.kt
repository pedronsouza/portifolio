package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class SearchResultInfo(@Expose val email : String, @Expose val photo : String?, @Expose val haveAccess : Boolean = false)