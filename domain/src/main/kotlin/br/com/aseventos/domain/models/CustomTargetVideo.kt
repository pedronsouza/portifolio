package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose

data class CustomTargetVideo (@Expose val videoId : String, @Expose val url : String)