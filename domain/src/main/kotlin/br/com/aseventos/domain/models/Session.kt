package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Session(@Expose var user : User?,@Expose  var passwordCreationToken : String?, @Expose var scope : Scope) {
    companion object {
        @JvmStatic val SESSION_PREF_KEY = "SESSION_PREF_KEY"
    }

    enum class Scope {
        @SerializedName("Authorized")
        AUTHORIZED,
        @SerializedName("CreationToken")
        CREATION_TOKEN,
        @SerializedName("Unauthorized")
        UNAUTHORIZED,
        @SerializedName("Guest")
        GUEST
    }
}