package br.com.aseventos.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CustomTargetPhoto(@Expose @SerializedName("photoId") val id : String, val targetId : String)