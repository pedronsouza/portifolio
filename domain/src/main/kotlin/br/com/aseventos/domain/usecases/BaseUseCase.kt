package br.com.aseventos.domain.usecases

import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import rx.Observable
import rx.Subscriber
import rx.Subscription
import rx.schedulers.Schedulers

abstract class BaseUseCase<T>(private val threadExecutor: ThreadExecutor, private val postExecutionThread: PostExecutionThread) {
    abstract fun observableFor() : Observable<T>
    private var subscription : Subscription? = null

    fun execute(subscriber: Subscriber<T>) {
        this.subscription = observableFor()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler())
                .subscribe(subscriber)
    }

    fun unsubscribe() {
        subscription?.unsubscribe()
    }
}