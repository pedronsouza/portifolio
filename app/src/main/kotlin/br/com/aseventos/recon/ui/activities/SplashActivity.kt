package br.com.aseventos.recon.ui.activities

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.MediaEntity
import br.com.aseventos.data.db.entities.TargetEntity
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.v2.AppMedia
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.AsReconApplication.Companion.isInternalBuild
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.presenters.SplashPresenter
import br.com.aseventos.recon.ui.view.SplashView
import javax.inject.Inject

class SplashActivity : AppCompatActivity(), SplashView {
    @Inject lateinit var sessionManager : SessionManager
    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var presenter : SplashPresenter
    @Inject lateinit var appTracker : TrackerAdapter
    var appDatabase : ReconDatabase? = null
    var demosToFix = mutableListOf<DemoEntity>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        activityGraph?.inject(this)
        appDatabase = activityGraph?.database()
        appTracker.trackPageView(TrackerAdapter.PageName.SPLASH)
    }

    fun sanitizeDemos() {
        val demos = appDatabase?.demoDao()?.getAll()
        if (demos != null) {
            demos.forEach { demo ->
                val videos = appDatabase?.mediaDao()?.getByDemoId(demo.id)
                if (videos == null || videos.isEmpty()) {
                    demosToFix.plusAssign(demo)
                }
            }

            if (demosToFix.isNotEmpty()) {
                presenter.findAndFixFilesForDemo(demosToFix.first(), this)
            } else {
                validateAndSend()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isInternalBuild()) {
            sanitizeDemos()
        } else {
            validateAndSend()
        }
    }

    fun validateAndSend() {
        if (!sessionManager.isAuthorized()) {
            if (isInternalBuild() && !sessionManager.isAuthorized()) {
                appRouter.toCameraActionActivity(this)
            } else if (!isInternalBuild()) {
                appRouter.toAccessOptionsActivity(this)
            } else {
                appRouter.toCameraActionActivity(this)
            }
        } else {
            appRouter.toCameraActionActivity(this)
        }
    }

    override fun onStop() {
        presenter.unsubscribe()
        super.onStop()
    }

    override fun onAdminTokenGenerated(session: Session) {
        appRouter.toCameraActionActivity(this)
    }

    override fun onAdminTokenGenerationError(e: Throwable) {
        AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage("Ocorreu um erro ao inicializar o aplicativo, tente novamente")
                .setPositiveButton("Ok, Entendi") { dialogInterface: DialogInterface, i: Int ->
                    finish()
                }.show()
    }

    override fun onFilesFoundedForDemo(demo: DemoEntity, files: List<AppMedia>) {
        files.flatMap {recon ->
            val video = MediaEntity()
            synchronized(video) {
                video.demoId = demo.id
                video.sourceUri = recon.url
                video.status = DemoEntity.STATUS_IDLE
                video.id = appDatabase?.mediaDao()?.insert(video)?.toInt()!!
                recon.photos.forEach { t ->
                    val target = TargetEntity()
                    target.mediaId = video.id
                    target.fileBlob = t.url
                    target.id = appDatabase!!.targetDao().insert(target).toInt()
                }

                listOf(video)
            }
        }
        validateAndSend()
    }

    override fun onDemoHasNoFiles(demo: DemoEntity) {
        appDatabase?.demoDao()?.removeById(demo.id)
    }

}
