package br.com.aseventos.recon.ui.view

import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.models.Demonstration
import br.com.aseventos.domain.models.Recon
import br.com.aseventos.recon.ui.events.OnMediaListScrollStateChanged
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel

interface MediaListView : ProgressibleView {
    fun onMediaListItemsFetched(demo: DemoEntity, items : List<MediaListItemViewModel>)
    fun onMediaListItemsFetchError(e : Throwable)
    fun onMediaListScrollStateChanged(event : OnMediaListScrollStateChanged)
    fun onDemonstrationCreated(demonstration: DemoEntity)
    fun onDemonstrationRetrieved(demonstration: DemoEntity)
    fun onNoDemoCreatedForSearchItem()
    fun onDemonstrationCreationError(e : Throwable)
    fun onFilesCreatedAndDownloadServiceStarted()
}