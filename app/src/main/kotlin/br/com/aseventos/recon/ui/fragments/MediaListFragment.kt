package br.com.aseventos.recon.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.ui.adapters.MediaListAdapter
import br.com.aseventos.recon.ui.view.TitleView
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import javax.inject.Inject

class MediaListFragment : Fragment(), TitleView {
    companion object {
        val MEDIA_LIST_ITENS_FLAG = "MEDIA_LIST_ITENS"
        val MEDIA_LIST_TITLE_FLAG = "MEDIA_LIST_TITLE"

        fun newInstance(items : List<MediaListItemViewModel>,
                        title : String = "Videos") : MediaListFragment {
            val f = MediaListFragment()
            f.arguments = Bundle()
            f.arguments!!.putParcelableArray(MEDIA_LIST_ITENS_FLAG, items.toTypedArray())
            f.arguments!!.putString(MEDIA_LIST_TITLE_FLAG, title)
            return f
        }
    }

    private lateinit var viewGroup : View

    private val items by lazy { arguments?.getParcelableArray(MEDIA_LIST_ITENS_FLAG)!!.toList() as List<MediaListItemViewModel> }
    private val recycler by lazy { viewGroup.findViewById<RecyclerView>(R.id.media_list_recycler) }

    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var appFileLoader : FileLoader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appGraph?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewGroup = LayoutInflater.from(context).inflate(R.layout.fragment_media_list, container, false)
        return viewGroup
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler.adapter = MediaListAdapter(items, appRouter, appFileLoader)
    }

    override fun getViewTitle(): String = arguments?.getString(MEDIA_LIST_TITLE_FLAG)!!
}