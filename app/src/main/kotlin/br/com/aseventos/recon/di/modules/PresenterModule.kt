package br.com.aseventos.recon.di.modules

import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.usecases.media.FetchDirectoryItemsUseCase
import br.com.aseventos.data.usecases.users.GeneralSearchUseCase
import br.com.aseventos.data.local.DemonstrationManager
import br.com.aseventos.data.usecases.demonstrations.*
import br.com.aseventos.data.usecases.media.FetchUserItemsUseCase
import br.com.aseventos.data.usecases.session.*
import br.com.aseventos.data.usecases.users.UploadCustomTargetImageUseCase
import br.com.aseventos.data.usecases.users.UploadCustomTargetVideoUseCase
import br.com.aseventos.data.usecases.v2.*
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.presenters.*
import dagger.Module
import dagger.Provides

@Module
class PresenterModule {

    @Provides
    fun providesNewUserPresenter(checkUserEmailOnServerUseCase: CheckUserEmailOnServerUseCase,
                                 createPasswordUserCase: CreatePasswordUserCase,
                                 requestChangePasswordTokenUseCase : RequestChangePasswordTokenUseCase,
                                 validateUserEmailCase : ValidateUserEmailCase,
                                 saveUserPasswordCase : SaveUserPasswordCase) : NewUserPresenter =
            NewUserPresenter(checkUserEmailOnServerUseCase, createPasswordUserCase, requestChangePasswordTokenUseCase, validateUserEmailCase, saveUserPasswordCase)

    @Provides
    fun providesSignInPresenter(authorizeUserCase : AuthorizeUserCase) : SignInPresenter =
            SignInPresenter(authorizeUserCase)

    @Provides
    fun providesSearchUsersPresenter(generalSearchUseCase: GeneralSearchUseCase,
                                     findUserInRemoteCase : FindUserInRemoteCase) : SearchUsersPresenter =
            SearchUsersPresenter(generalSearchUseCase, findUserInRemoteCase)

    @Provides
    fun providesSplashPresenter(generateAdminTokenUseCase: GenerateAdminTokenUseCase,
                                sessionManager: SessionManager,
                                findUserFilesCase: FindUserFilesCase) : SplashPresenter =
            SplashPresenter(generateAdminTokenUseCase, sessionManager, findUserFilesCase)

    @Provides
    fun providesMediaListPresenter(appDatabase: ReconDatabase?,
                                   appFileLoader : FileLoader,
                                   fetchDirectoryItemsUseCase : FetchDirectoryItemsUseCase,
                                   fetchDemonstrationBySearchItemIdUseCase: FetchDemonstrationBySearchItemIdUseCase,
                                   createNewDemonstrationUseCase: CreateNewDemonstrationUseCase,
                                   fetchUserItemsUseCase : FetchUserItemsUseCase,
                                   uploadCustomTargetVideoUseCase: UploadCustomTargetVideoUseCase,
                                   uploadCustomTargetImageUseCase: UploadCustomTargetImageUseCase,
                                   findUserFilesCase : FindUserFilesCase) : MediaListPresenter =
            MediaListPresenter(appDatabase,
                    appFileLoader,
                    fetchDirectoryItemsUseCase,
                    fetchDemonstrationBySearchItemIdUseCase,
                    createNewDemonstrationUseCase,
                    fetchUserItemsUseCase,
                    uploadCustomTargetVideoUseCase,
                    uploadCustomTargetImageUseCase,
                    findUserFilesCase)

    @Provides
    fun providesSearchItemDetailPresenter(checkUserEmailOnServerUseCase: CheckUserEmailOnServerUseCase) : SearchItemDetailPresenter =
            SearchItemDetailPresenter(checkUserEmailOnServerUseCase)

    @Provides
    fun providesDemonstrationPresenter(fetchAllDemonstrationsUseCase : FetchAllDemonstrationsUseCase) : DemonstrationPresenter = DemonstrationPresenter(fetchAllDemonstrationsUseCase)
}