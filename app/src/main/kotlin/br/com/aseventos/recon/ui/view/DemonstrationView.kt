package br.com.aseventos.recon.ui.view

import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.recon.helpers.FileLoader

interface DemonstrationView {
    fun onDemonstrationsFetched(items : List<DemoEntity>)
    fun onDemonstrationsEmpty()
    fun onLoadDemonstration(demoId: Int)
    fun onShowDemonstrationContent(demoId: Int)
    fun onDemonstrationRemoved(demoId: Int)
    fun getFileLoader() : FileLoader
}