package br.com.aseventos.recon.ui.presenters

abstract class BasePresenter {
    abstract fun unsubscribe()
}