package br.com.aseventos.recon.ui.view

interface MainMenuListener {
    fun onDrawerClosedInvoked()
    fun onDisplayOnboarding()
}