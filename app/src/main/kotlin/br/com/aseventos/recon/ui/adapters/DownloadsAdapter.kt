package br.com.aseventos.recon.ui.adapters

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.domain.models.Demonstration
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.view.DownloadsView
import br.com.aseventos.recon.ui.viewmodels.DownloadItemViewModel
import com.facebook.drawee.view.SimpleDraweeView
import com.tonyodev.fetch2.Download
import com.tonyodev.fetch2.Status

class DownloadsAdapter(val items : MutableList<DownloadItemViewModel>, val view : DownloadsView) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = items.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_media_list_item_download, parent, false)
        return DownloadViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DownloadViewHolder) {
            holder.update(items[position], (position + 1))
        }
    }

    inner class DownloadViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val position    by lazy { itemView.findViewById<TextView>(R.id.item_position) }
        private val itemIcon    by lazy { itemView.findViewById<SimpleDraweeView>(R.id.item_image) }
        private val progress    by lazy { itemView.findViewById<TextView>(R.id.item_progress) }
        private val iconOverlay by lazy { itemView.findViewById<ImageView>(R.id.item_icon_overlay) }
        private val name        by lazy { itemView.findViewById<TextView>(R.id.item_name) }
        private val button      by lazy { itemView.findViewById<TextView>(R.id.item_button) }

        fun update(item : DownloadItemViewModel, order : Int) {
            if (order <= 9) {
                name.text = "Vídeo 0$order"
                position.text = "0$order"
            } else {
                position.text = order.toString()
                name.text = "Vídeo $order"
            }

            if (item.targetId != -1) {
                view.getFileLoader().createTargetImageRequestForTargetId(item.targetId) {
                    itemIcon.controller = it
                }
            }

            if (item.status == Status.COMPLETED) {
                iconOverlay.visibility = View.VISIBLE
            } else {
                iconOverlay.visibility = View.INVISIBLE
            }

            button.setOnClickListener {  } // reset
            when {
                item.status == Status.DOWNLOADING -> progress.text = "Progresso ${item.progress} %"
                item.status == Status.COMPLETED -> {
                    button.text = "Visualizar"
                    button.setOnClickListener {
                        view.onMediaItemClicked(item)
                    }
                    progress.text = "Finalizado"
                }
                else -> progress.text = "Aguardando na fila"
            }
        }
    }

    fun updateProgress(download : Download, progress : Int) {
        val item = items.firstOrNull { it.id.contentEquals(download.id.toString()) }
        if (item != null) {
            val indexOf = items.indexOf(item)
            item.progress = progress

            if (progress >= 100) {
                item.status = Status.COMPLETED
            } else {
                item.status = download.status
            }

            items[indexOf] = item
            items.sortByDescending { it.progress }
            notifyItemChanged(indexOf)

        }
    }

    fun setAsDone(download: Download) {
        val item = items.firstOrNull { it.id.contentEquals(download.id.toString()) }
        if (item != null) {
            val indexOf = items.indexOf(item)
            item.status = Status.COMPLETED
            item.progress = 100
            items.sortByDescending { it.status == Status.DOWNLOADING }
            notifyDataSetChanged()
        }
    }
}