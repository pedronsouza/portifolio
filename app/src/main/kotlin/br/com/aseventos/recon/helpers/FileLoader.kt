package br.com.aseventos.recon.helpers

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Base64
import android.util.Log
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.TargetEntity
import br.com.aseventos.domain.models.ARConfiguration
import br.com.aseventos.domain.models.ARMetaInfo
import br.com.aseventos.domain.models.ARTarget
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.BuildConfig
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.interfaces.DraweeController
import com.google.gson.Gson
import io.fabric.sdk.android.services.concurrency.AsyncTask
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import java.io.BufferedInputStream
import java.io.File
import java.net.HttpURLConnection
import java.net.URI
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class FileLoader {
    private var database : ReconDatabase? = null
    private lateinit var gson : Gson
    private val okHttpClient by lazy {
        val logging = HttpLoggingInterceptor()
        if(BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }
        OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build()
    }

    @Inject constructor(database: ReconDatabase?, gson: Gson) {
        this.database = database
        this.gson = gson
    }

    companion object {
        private var INSTANCE : FileLoader? = null
        fun getInstance(database: ReconDatabase?, gson : Gson) : FileLoader {
            if (INSTANCE == null) {
                INSTANCE = FileLoader(database, gson)
            }
            return INSTANCE!!
        }
    }

    fun getTargetImagePathByTargetId(targetId : Int) : String {
        return "${AsReconApplication.getSaveDir()}/${HashUtils.sha1(targetId.toString())}"
    }

    fun urlForImageHandlerWithTargetSourceId(targetId: String) : String {
        return "${BuildConfig.IMG_HANDLER_URL}$targetId"
    }

    fun createEasyArConfigurationFileFromDemonstration(demo : DemoEntity, afterFileCreated : (String) -> Unit) {
        val videos = database?.mediaDao()?.getByDemoId(demo.id)?.filter { it.status.contentEquals(DemoEntity.STATUS_STORED) }
        val arTargets = mutableListOf<ARTarget>()

        videos?.forEach { v ->
            val targets = database?.targetDao()?.getByMediaId(v.id)
            val metaInfo = ARMetaInfo(v.localPathUri!!)
            if (targets != null && targets.isNotEmpty()) {
                val parsedTargets = targets.flatMap { listOf(ARTarget(
                            image = DemoUtils.localPathForTargetStorage(it),
                            name = it.id.toString(),
                            size = listOf(20.0F),
                            meta = metaInfo.toJson(gson)
                    ))
                }
                arTargets.plusAssign(parsedTargets)
            }
        }
        val config = ARConfiguration(arTargets.toList())
        AsReconApplication.saveDemonstrationConfigLocal(config, demo.id) { filePath ->
            demo.localConfigPath = filePath
            database?.demoDao()?.update(demo)
            afterFileCreated(filePath)
        }
    }

    fun loadBitmapForTargetId(targetId : Int, onBitmapCreated: (Bitmap) -> Unit){
        var target : TargetEntity? = database?.targetDao()?.getTargetWithFileBlobById(targetId)

        val decodedBytes = Base64.decode(target?.fileBlob, Base64.DEFAULT)
        try {
            val img = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
            onBitmapCreated(img)
            target = null
        } catch(e : Throwable) {
            Log.e("FileLoader", Log.getStackTraceString(e))
        }
    }

    fun createTargetImageRequestForTargetId(targetId : Int, onImageRequestCreated: (DraweeController) -> Unit) {
        val target : TargetEntity? = database?.targetDao()?.getTargetWithFileBlobById(targetId)

        if(target != null) {
            val controller = Fresco.newDraweeControllerBuilder()
                    .setUri(Uri.parse(target.fileBlob))
                    .setTapToRetryEnabled(true)
                    .build()

            onImageRequestCreated(controller)
        }
    }

    fun removeFileWithLocalPath(localPath : String) {
        try {
            val uri = URI(localPath)
            val file = File(uri.path)
            if (file.exists()) {
                file.delete()
            }
        } catch (e : Throwable) {

        }
    }


    fun fetchImageAsBitmap(url: String, onCompletion: (Bitmap) -> Unit) {
        AsyncTask.execute {

            try {
                val request = Request.Builder().url(url).removeHeader("Authorization").addHeader("Content-Type", "image/*").build();
                val response = okHttpClient.newCall(request).execute()
                val inStream = response.body()?.byteStream()
                val buff = BufferedInputStream(inStream)
                val bmp = BitmapFactory.decodeStream(buff)


                onCompletion(bmp)
            } catch (e: Exception) {
                Log.e("Error", e.message)
                e.printStackTrace()
            }
        }
    }
}