package br.com.aseventos.recon.ui.adapters

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.R
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent.ACTION_VIEW
import android.graphics.Bitmap
import android.graphics.drawable.Animatable
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.util.Log
import br.com.aseventos.data.db.entities.TargetEntity
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.helpers.HashUtils
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.image.ImageInfo
import java.io.File
import java.net.URL


class VideoPhotosAdapter(private val items : List<TargetEntity>,val appFileLoader : FileLoader) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = items.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_media_item_photo, parent, false)
        return PhotoViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PhotoViewHolder) {
            holder.update(items[position], (position + 1))
        }
    }

    inner class PhotoViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val itemName by lazy { itemView.findViewById<TextView>(R.id.item_name) }
        private val icon by lazy { itemView.findViewById<ImageView>(R.id.item_thumb) }
        private val shareBt by lazy { itemView.findViewById<ImageView>(R.id.item_share_button) }
        private val showBt by lazy { itemView.findViewById<TextView>(R.id.item_show_button) }

        fun update(item : TargetEntity, counter : Int) {
            itemName.text = "Imagem ${counter}"
            icon.visibility = View.VISIBLE

            AsyncTask.execute {
                appFileLoader.fetchImageAsBitmap(item.fileBlob!!) { btmp ->
                    val ctx = itemView.context
                    if (ctx is AppCompatActivity) {
                        ctx.runOnUiThread {
                            shareBt.setOnClickListener {
                                //TODO native share
                                appGraph?.appTracker()?.trackEvent(TrackerAdapter.EventCategory.ACTION,
                                        TrackerAdapter.EventName.TARGET_SHARED,
                                        null)
                                val imgUrl = MediaStore.Images.Media.insertImage(itemView.context.contentResolver, btmp, HashUtils.sha1(item.id.toString()), null)
                                appGraph?.appRouter()?.toNativeShare(itemView.context, Uri.parse(imgUrl))
                            }

                            showBt.setOnClickListener{
                                appGraph?.appTracker()?.trackEvent(TrackerAdapter.EventCategory.ACTION,
                                        TrackerAdapter.EventName.TARGET_VIEW,
                                        null)
                                val imgUrl = MediaStore.Images.Media.insertImage(itemView.context.contentResolver, btmp, HashUtils.sha1(item.id.toString()), null)
                                appGraph?.appRouter()?.toShowOnGallery(itemView.context, imgUrl)
                            }

                            icon.setImageBitmap(btmp)
                        }
                    }
                }
            }
        }
    }
}