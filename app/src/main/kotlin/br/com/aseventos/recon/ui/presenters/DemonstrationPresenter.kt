package br.com.aseventos.recon.ui.presenters

import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.usecases.demonstrations.FetchAllDemonstrationsUseCase
import br.com.aseventos.domain.models.Demonstration
import br.com.aseventos.recon.helpers.BaseSubscriber
import br.com.aseventos.recon.ui.view.DemonstrationView
import javax.inject.Inject

class DemonstrationPresenter : BasePresenter {
    private val fetchAllDemonstrationsUseCase : FetchAllDemonstrationsUseCase

    @Inject constructor(fetchAllDemonstrationsUseCase : FetchAllDemonstrationsUseCase) {
        this.fetchAllDemonstrationsUseCase = fetchAllDemonstrationsUseCase
    }

    fun fetchAllDemos(view : DemonstrationView) {
        this.fetchAllDemonstrationsUseCase.execute(FetchDemosSubscriber(view))
    }

    override fun unsubscribe() {
        fetchAllDemonstrationsUseCase.unsubscribe()
    }

    private inner class FetchDemosSubscriber(val view : DemonstrationView) : BaseSubscriber<List<DemoEntity>>() {
        override fun onNext(item: List<DemoEntity>) {
            super.onNext(item)
            if (item.isNotEmpty()) {
                view.onDemonstrationsFetched(item)
            } else {
                view.onDemonstrationsEmpty()
            }
        }
    }
}