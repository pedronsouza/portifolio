package br.com.aseventos.recon.ui.fragments

import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import android.widget.VideoView
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.activities.HomeActivity
import org.greenrobot.eventbus.EventBus

class VideoPlayerFullScreenFragment : Fragment() {
    companion object {
        val VIDEO_PLAYER_SEEK_POSITION_FLAG = "VIDEO_PLAYER_SEEK_POSITION";
        val VIDEO_PLAYER_URL_FLAG = "VIDEO_PLAYER_URL";

        public fun newInstance(url : String, seekPosition : Int = 0) : VideoPlayerFullScreenFragment {
            val fragment = VideoPlayerFullScreenFragment()
            fragment.arguments = Bundle()
            fragment.arguments!!.putString(VIDEO_PLAYER_URL_FLAG, url);
            fragment.arguments!!.putInt(VIDEO_PLAYER_SEEK_POSITION_FLAG, seekPosition);
            return fragment;
        }
    }
    private var viewGroup: View? = null
    private val videoView by lazy { viewGroup?.findViewById(R.id.fullscreen_video_view) as VideoView? }
    private val sourceUrl by lazy { arguments?.getString(VIDEO_PLAYER_URL_FLAG, null) }
    private val seekPosition by lazy { arguments?.getInt(VIDEO_PLAYER_SEEK_POSITION_FLAG, -1) ?: -1 }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewGroup = inflater?.inflate(R.layout.fragment_video_player_fullscreen, container, false);
        return viewGroup
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        videoView?.setVideoURI(Uri.parse(sourceUrl))
        val controls = MediaController(activity)
        controls.setAnchorView(videoView)
        videoView?.setMediaController(controls)

        videoView?.setOnErrorListener { mediaPlayer, i, x ->
            AlertDialog.Builder(activity)
                    .setTitle("Alerta")
                    .setMessage("Erro ao tentar reproduzir o video em fullscreen")
                    .setNeutralButton("Ok, Entendi", { _, _ ->
                        EventBus.getDefault().post(HomeActivity.OnFullScreenVideoFinished())
                    }).show()

            false;
        }

        if (seekPosition > 0) {
            videoView?.seekTo(seekPosition)
        }

        videoView?.start()
    }
}