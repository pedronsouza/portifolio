package br.com.aseventos.recon.helpers

import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.MediaEntity
import br.com.aseventos.data.db.entities.TargetEntity
import br.com.aseventos.domain.models.Recon
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.BuildConfig
import br.com.aseventos.recon.ui.viewmodels.DemoViewModel
import br.com.aseventos.recon.ui.viewmodels.DownloadableItemViewModel
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import br.com.asrecon.recon.v2.HelloAR
import java.net.URL

object DemoUtils {
    fun buildDemoViewModel(demo : DemoEntity, db : ReconDatabase?) : DemoViewModel {
        val video = db?.mediaDao()?.getByDemoId(demo.id)?.first()
        if (video != null) {
            val firstTarget = db.targetDao().getFirstByMediaId(video.id)
            if (firstTarget != null) {
                return DemoViewModel(
                        id = demo.id,
                        firstTargetId = firstTarget.id,
                        name =  demo.name,
                        status =  demo.status
                )
            } else {
                throw IllegalArgumentException("target for demo ${demo.id} not founded in local database")
            }
        } else {
            throw IllegalArgumentException("video for demo ${demo.id} not founded in local database")
        }
    }

    fun buildRelationTreeForDemo(demo: DemoEntity, appDatabase: ReconDatabase?) : List<Map<MediaEntity, List<TargetEntity>>> {
        val videos = appDatabase?.mediaDao()?.getMediasForDemoId(demo.id)
        if (videos != null && videos.isNotEmpty()) {
            return videos.flatMap {v ->
                val targets = appDatabase.targetDao().getByMediaId(v.id)
                return@flatMap listOf(mapOf(Pair(v, targets)))
            }
        } else {
            return emptyList()
        }
    }

    fun buildRelationTreeForDemo(demoId: Int, appDatabase: ReconDatabase?) : List<Map<MediaEntity, List<TargetEntity>>> {
        val demo = appDatabase?.demoDao()?.getById(demoId)
        if (demo != null) {
            return DemoUtils.buildRelationTreeForDemo(demo, appDatabase)
        } else {
            throw java.lang.IllegalArgumentException("demoId ${demoId} not founded on database")
        }
    }

    fun localPathForVideoStorage(demoId : Int, videoId : Int) : String {
        return "${AsReconApplication.getSaveDir()}/recon_${HashUtils.sha1(demoId.toString())}_video_${HashUtils.sha1(videoId.toString())}.mp4"
    }

    fun buildDownloableListOfItensFromDemo(demo : DemoEntity, appDatabase: ReconDatabase?) : List<DownloadableItemViewModel> {
        val videos = appDatabase?.mediaDao()?.getByDemoId(demo.id)
        return if (videos != null) {
            videos.flatMap { v ->
                val targets = appDatabase.targetDao().getByMediaId(v.id)
                listOf(DownloadableItemViewModel(
                        demoId = v.demoId,
                        reconId = v.id,
                        remotePath = HelloAR.getFinalURL(URL(v.sourceUri)).toString(),
                        localPath = localPathForVideoStorage(v.demoId, v.id),
                        requestId = null,
                        targetId = targets.flatMap { listOf(it.id.toString()) },
                        type = MediaListItemViewModel.Type.VIDEO
                ))
            }

        } else {
            emptyList()
        }
    }

    fun buildDemoViewModel(demoId : Int, db : ReconDatabase?) : DemoViewModel {
        val demo = db?.demoDao()?.getById(demoId)
        if (demo != null) {
            return DemoUtils.buildDemoViewModel(demo, db)
        } else {
            throw IllegalArgumentException("demoId ${demoId} not founded in local database")
        }
    }

    fun localPathForTargetStorage(target : TargetEntity): String =
        "${AsReconApplication.getSaveDir()}/${HashUtils.sha1(target.id.toString())}.jpg"

    fun sourceUriForVideoWithRecon(reconId: String): String =
            "${BuildConfig.BASE_URL}videos/file/${reconId}/hd"

}