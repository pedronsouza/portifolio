package br.com.aseventos.recon.ui.presenters

import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.usecases.session.GenerateAdminTokenUseCase
import br.com.aseventos.data.usecases.v2.FindUserFilesCase
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.models.v2.AppMedia
import br.com.aseventos.recon.helpers.BaseSubscriber
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.view.SplashView
import javax.inject.Inject

class SplashPresenter : BasePresenter {
    private val generateAdminTokenUseCase: GenerateAdminTokenUseCase
    private val sessionManager: SessionManager
    private val findUserFilesCase: FindUserFilesCase

    @Inject constructor(generateAdminTokenUseCase: GenerateAdminTokenUseCase,
                        sessionManager: SessionManager,
                        findUserFilesCase: FindUserFilesCase) {
        this.generateAdminTokenUseCase = generateAdminTokenUseCase
        this.sessionManager = sessionManager
        this.findUserFilesCase = findUserFilesCase
    }

    fun generateAdminToken(view : SplashView) {
        this.generateAdminTokenUseCase.execute(GenerateTokenSubscriber(view))
    }

    fun findAndFixFilesForDemo(demoEntity: DemoEntity, view : SplashView) {
        this.findUserFilesCase.baseSystemId = demoEntity.baseSystemId
        this.findUserFilesCase.execute(FindAndFixFilesSubscribe(demoEntity, view))
    }

    override fun unsubscribe() =
            generateAdminTokenUseCase.unsubscribe()

    inner class FindAndFixFilesSubscribe(val demo: DemoEntity, val view : SplashView) : BaseSubscriber<List<AppMedia>>() {
        override fun onNext(item: List<AppMedia>) {
            super.onNext(item)
            if (item.isNotEmpty()) {
                view.onFilesFoundedForDemo(demo, item)
            } else {
                view.onDemoHasNoFiles(demo)
            }
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            view.onDemoHasNoFiles(demo)
        }
    }

    inner class GenerateTokenSubscriber(val view : SplashView) : BaseSubscriber<User>() {
        override fun onNext(item: User) {
            super.onNext(item)
            sessionManager.session = Session(item, null, Session.Scope.AUTHORIZED)
            sessionManager.store()
            view.onAdminTokenGenerated(sessionManager.session)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            view.onAdminTokenGenerationError(e)
        }
    }
}