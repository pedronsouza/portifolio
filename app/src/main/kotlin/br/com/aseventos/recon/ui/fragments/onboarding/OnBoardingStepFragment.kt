package br.com.aseventos.recon.ui.fragments.onboarding

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.recon.R

class OnBoardingStepFragment : Fragment() {
    lateinit var viewGroup : View
    companion object {
        private val TITLE_ARG = "TITLE_ARG"
        private val MSG_ARG = "MSG_ARG"
        private val IMG_RES_ARG = "IMG_RES_ARG"

        fun create(title : String,
                   msg : String,
                   imgRes : Int) : OnBoardingStepFragment {
            val fragment = OnBoardingStepFragment()
            fragment.arguments = Bundle()
            fragment.arguments!!.putString(TITLE_ARG, title)
            fragment.arguments!!.putString(MSG_ARG, msg)
            fragment.arguments!!.putInt(IMG_RES_ARG, imgRes)
            return fragment
        }
    }

    private val title by lazy { arguments?.getString(TITLE_ARG, null) }
    private val msg by lazy { arguments?.getString(MSG_ARG, null) }
    private val imgRes by lazy { arguments?.getInt(IMG_RES_ARG) }

    private val titleField by lazy { viewGroup.findViewById(R.id.on_boarding_step_title) as TextView?}
    private val msgField by lazy { viewGroup.findViewById(R.id.on_boarding_step_message) as TextView? }
    private val imgView by lazy { viewGroup.findViewById(R.id.on_boarding_step_icon) as ImageView? }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewGroup = LayoutInflater.from(activity).inflate(R.layout.fragment_on_boarding_step, container, false)
        return viewGroup
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleField?.text = title
        msgField?.text = msg
        imgView?.setImageResource(imgRes!!)
    }
}