package br.com.aseventos.recon.ui.view

interface ProgressibleView {
    fun startProgress()
    fun stopProgress()
}