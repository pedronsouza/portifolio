package br.com.aseventos.recon.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import br.com.aseventos.data.adapters.GATrackerAdapter
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.usecases.executors.BackgroundThreadExecutor
import br.com.aseventos.data.usecases.executors.MainThreadExecutor
import br.com.aseventos.data.utils.GsonFactory
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.helpers.AppMenu
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.data.local.DemonstrationManager
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.FileLoader
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton



@Module
class AppModule(private val context : Application) {
    @Singleton
    @Provides
    fun providesThreadExecutor() : ThreadExecutor = BackgroundThreadExecutor()

    @Singleton
    @Provides
    fun providesPostThreadExecutor() : PostExecutionThread = MainThreadExecutor()

    @Singleton
    @Provides
    fun providesGson() : Gson = GsonFactory.create()

    @Provides
    fun providesFileLoader(database : ReconDatabase?, gson : Gson) : FileLoader = FileLoader.getInstance(database, gson)

    @Provides
    fun providesSharedPreferences() : SharedPreferences =
            context.getSharedPreferences(AsReconApplication.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

    @Provides
    fun providesUserSession(preferences: SharedPreferences, gson : Gson) : Session {
        val raw = preferences.getString(SessionManager.SESSION_STORAGE_KEY, null)
        return if (raw != null) {
            gson.fromJson(raw, Session::class.java)
        } else {
            Session(null, null, Session.Scope.UNAUTHORIZED)
        }
    }

    @Singleton
    @Provides
    fun providesSessionManager(session: Session,
                               preferences: SharedPreferences,
                               gson: Gson) : SessionManager {
        val scope = if (session.user == null  && session.passwordCreationToken == null)
            Session.Scope.UNAUTHORIZED
        else if (session.user != null && session.passwordCreationToken == null && session.scope != Session.Scope.GUEST) {
            Session.Scope.AUTHORIZED
        } else if (session.scope != Session.Scope.GUEST){
            Session.Scope.CREATION_TOKEN
        } else {
            Session.Scope.GUEST
        }

        session.scope = scope

        return SessionManager(session, preferences, gson)
    }

    @Singleton
    @Provides
    fun providesAppRouter() : AppRouter =
            AppRouter()

    @Singleton
    @Provides
    fun providesAppMenu() : AppMenu =
            AppMenu()

    @Singleton
    @Provides
    fun providesDemonstrationManager(gson : Gson,
                                     sharedPreferences: SharedPreferences) : DemonstrationManager =
            DemonstrationManager(gson, sharedPreferences)

    @Provides
    @Singleton
    fun providesTrackerAdapter(context : Context) : TrackerAdapter =
            GATrackerAdapter(context, context.getString(R.string.ga_tracker_id))


}