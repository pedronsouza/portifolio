package br.com.aseventos.recon.ui.activities

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.MotionEventCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.domain.models.*
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.AsReconApplication.Companion.isInternalBuild
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.DemoUtils
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.services.UploadCustomTargetService
import br.com.aseventos.recon.ui.fragments.MainMenuFragment
import br.com.aseventos.recon.ui.fragments.OnBoardingFragment
import br.com.aseventos.recon.ui.fragments.VideoPlayerFullScreenFragment
import br.com.aseventos.recon.ui.view.HomeView
import br.com.asrecon.recon.v2.GLView
import br.com.asrecon.recon.v2.VideoPlayBackV2
import com.crashlytics.android.Crashlytics
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.lang.ref.WeakReference
import javax.inject.Inject

class HomeActivity : VideoPlayBackV2(), HomeView, DrawerLayout.DrawerListener {
    companion object{
        val camPermisionRequestCode = 432
        val DEFAULT_ANIMATION_DURATION = 500L
        val OFFLINE_MODE_KEY = "OFFLINE_MODE"
        val OFFLINE_MODE_JSONPATH_KEY = "OFFLINE_MODE_JSONPATH"
    }
    @Inject lateinit var sessionManager : SessionManager
    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var appTracker : TrackerAdapter

    private val toolbar by lazy { mainContentView.findViewById(R.id.main_toolbar) as Toolbar?}
    private val drawerLayout by lazy { mainContentView.findViewById(R.id.container) as DrawerLayout? }
    private val helpBt by lazy { mainContentView.findViewById(R.id.img_help_bt) as View? }
    private val content by lazy { mainContentView.findViewById(R.id.page_content) as View? }
    private val playerControls by lazy { mainContentView.findViewById(R.id.home_player_controls) as LinearLayout? }
    private val offLineBt by lazy { mainContentView.findViewById(R.id.offline_mode_bt) as TextView? }

    private val key by lazy { getString(R.string.easyar_app_key )}
    private val cloud_server_address by lazy { getString(R.string.easyar_cloud_server)}
    private val cloud_key by lazy { getString(R.string.easyar_cloud_key)}
    private val cloud_secret by lazy { getString(R.string.easyar_cloud_secret) }
    private val playerBtPlay by lazy { findViewById(R.id.home_bt_player_play) as ImageView? }
    private val playerBtFullscreen by lazy { findViewById(R.id.home_bt_player_fullscreen) as ImageView? }
    private val loadingContentIndicator by lazy { findViewById(R.id.loading) as LinearLayout? }
    private var offlineMode = false
    private var jsonPath : String? = null
    private val videoUriCustom          by lazy { intent.getStringExtra(UploadCustomTargetService.UPLOAD_CUSTOM_TGT_VIDEO_URI_FLAG) }
    private val imgTargetsCustom          by lazy { intent.getStringArrayListExtra(UploadCustomTargetService.UPLOAD_CUSTOM_TGT_IMAGES_FLAG) }
    private var offlineDialog : AlertDialog? = null
    private var currentTargets : MutableList<String> = mutableListOf()
    private var demoLoaded = false

    var appDatabase : ReconDatabase? = null
    @Inject lateinit var appFileLoader : FileLoader

    private var userId : String? = null

    private val handler = Handler()

    override fun onCloudReconInit() {
        val demo = if (!isInternalBuild()) appDatabase?.demoDao()?.getByBaseSystemId(sessionManager.session.user?.id ?: "") else null

        if (demo != null) {
            AsReconApplication.appGraph?.appFileLoader()?.createEasyArConfigurationFileFromDemonstration(demo) {
                if(glView != null) {
                    demoLoaded = true
                    glView.loadExtraTargets(it)
                }
            }
        }
    }


    override fun onCloudReconStartLoadTarget() {
        appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                TrackerAdapter.EventName.TARGET_RECON,
                null)
        if (!offlineMode || jsonPath == null) {
            runOnUiThread {
                loadingContentIndicator?.animate()
                        ?.setDuration(DEFAULT_ANIMATION_DURATION)
                        ?.alpha(0.0F)
                        ?.start()
            }
        }
    }

    override fun onCloudVideoStatusChanged(status : Int) {
        runOnUiThread {
            loadingContentIndicator?.animate()
                    ?.setDuration(DEFAULT_ANIMATION_DURATION)
                    ?.alpha(0.0F)
                    ?.start()
        }
    }

    override fun onCloudReconFinished(status: Int) {
        Log.d("HomeActivity", "onCloudReconFinished")
        runOnUiThread {
            handler.postDelayed({
                loadingContentIndicator?.animate()
                        ?.setDuration(DEFAULT_ANIMATION_DURATION)
                        ?.alpha(0.0F)
                        ?.start()
            }, 3000L)

            if (status == GLView.CloudReconStatus.READY.statusValue) {


            } else if (status == GLView.CloudReconStatus.FAIL.statusValue) {
                Crashlytics.log("Erro de serialização do metadata")
            } else {

            }
        }
    }



    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)

    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)

    }

    override fun onResume() {
        super.onResume()
        val demo = if (!isInternalBuild()) appDatabase?.demoDao()?.getByBaseSystemId(sessionManager.session.user?.id ?: "") else null

        if (demo != null) {
            AsReconApplication.appGraph?.appFileLoader()?.createEasyArConfigurationFileFromDemonstration(demo) {
                if(glView != null) {
                    demoLoaded = true
                    glView.loadExtraTargets(it)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isInternalBuild()) {
            offlineMode = intent.getBooleanExtra(OFFLINE_MODE_KEY, false)
            jsonPath = intent.getStringExtra(OFFLINE_MODE_JSONPATH_KEY)
        } else {
            offlineMode = false
            jsonPath = null
        }

        activityGraph?.inject(this)
        appDatabase = activityGraph?.database()
        userId = sessionManager.session.user?.id;

        if (videoUriCustom != null) {
            val i = Intent(this, UploadCustomTargetService::class.java)
            i.putExtra(UploadCustomTargetService.UPLOAD_CUSTOM_TGT_VIDEO_URI_FLAG, videoUriCustom)
            i.putExtra(UploadCustomTargetService.UPLOAD_CUSTOM_TGT_IMAGES_FLAG, imgTargetsCustom)
            startService(i)
            AlertDialog.Builder(this)
                    .setTitle("Seu Target está sendo processado")
                    .setMessage("Tudo pronto, já iniciamos o processamento dos arquivos e o envio para o servidor. Em breve o app irá te notificar sobre o resultado.")
                    .setPositiveButton("OK entendi") {i, _ ->
                        if (sessionManager.isSessionFirstAccess() && isInternalBuild()) {
                            onDisplayOnboarding()
                        } else {
                            onInitAppReconWithPermission()
                        }

                        i.dismiss()
                    }.setNegativeButton("Cancelar", null)
                    .show()
        } else {

            if (sessionManager.isSessionFirstAccess() && isInternalBuild()) {
                appTracker.trackEvent(TrackerAdapter.EventCategory.RESULT,
                        TrackerAdapter.EventName.BOARDING_OPEN,
                        null)
                onDisplayOnboarding()
            } else {
                onInitAppReconWithPermission()
            }
        }


    }

    override fun onReconInitError(e: Throwable?) {
        Crashlytics.logException(e)
    }

    fun onInitAppReconWithPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE), camPermisionRequestCode)
        } else {
            initAppRecon(cloud_server_address, cloud_key, cloud_secret, key, offlineMode, jsonPath, userId)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            camPermisionRequestCode -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onInitAppReconWithPermission()
                }
            }
        }
    }

    override fun onOnBoardFinished() {
        removeOnboarding()
        if (playerIsReadyToBeControledByUser()) {
            glView.play()
        } else if (this.glView == null) {
            onInitAppReconWithPermission()
        } else {
            toolbar?.setNavigationIcon(R.drawable.ic_drawer_icon_closed)
        }
    }


    override fun getMainContentView(): View =
            LayoutInflater.from(this).inflate(R.layout.activity_home, null)

    override fun onMainContendViewInserted() {
        appTracker.trackPageView(TrackerAdapter.PageName.CAMERA_HOME)

        setSupportActionBar(toolbar)
        toolbar?.setNavigationIcon(R.drawable.ic_drawer_icon)
        toolbar?.setNavigationOnClickListener {
            drawerLayout?.openDrawer(GravityCompat.START)
        }

        drawerLayout?.addDrawerListener(this)
        supportFragmentManager.beginTransaction()
                .add(R.id.drawer_content, MainMenuFragment.create(this))
                .commit()

        helpBt?.setOnClickListener {
            if (playerIsReadyToBeControledByUser() && isPlaying) {
                glView.pause()
            }

            appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                    TrackerAdapter.EventName.BOARDING_OPEN,
                    null)

            onDisplayOnboarding()
        }

        playerBtPlay?.setOnClickListener {
            if (playerIsReadyToBeControledByUser()) {
                if (isPlaying) {
                    playerBtPlay?.setImageResource(R.drawable.ic_play_arrow_white_24dp)
                    glView.pause()
                } else {
                    playerBtPlay?.setImageResource(R.drawable.ic_pause_white_24dp)
                    glView.play()
                }
            }
        }



        playerBtFullscreen?.setOnClickListener {
            if (playerIsReadyToBeControledByUser()) {
                val videoUrl = glView.currentVideoUrl()
                val seekTo = glView.currentPosition()

                if (isPlaying)
                    glView.pause()


                appRouter.toFullScreenPlayer(this, videoUrl, seekTo)
            }
        }

        content?.setOnTouchListener { view, motionEvent ->
            if (playerIsReadyToBeControledByUser()) {
                val action = MotionEventCompat.getActionMasked(motionEvent)
                if (action ==  MotionEvent.ACTION_DOWN) {
                    val finalAlpha = if (playerControls?.alpha == 0.0F) 1.0F else 0F
                    playerControls?.animate()?.setDuration(DEFAULT_ANIMATION_DURATION)?.alpha(finalAlpha)?.start()

                    if (finalAlpha == 1.0F) {
                        handler.postDelayed(task, 5000L);
                    }
                }
            }

            false
        }

        if(offlineMode && isInternalBuild() && offlineDialog == null) {
            offlineDialog = AlertDialog.Builder(this)
                    .setTitle("Alerta")
                    .setMessage("A demonstração foi carregada com sucesso")
                    .setPositiveButton("Ok, entendi") { x, d ->
                        x.dismiss()
                        offLineBt?.text = "Desativar modo offline"
                        offLineBt?.isSelected = true
                    }
                    .show()
        }



        if (offlineMode || (glView != null && glView.helloAR.offlineModeOn)) {
            loadingContentIndicator?.alpha = 0.0F
            offLineBt?.text = "Desativar modo offline"
            offLineBt?.isSelected = true
        } else {
            offLineBt?.text = "Ativar modo offline"
            offLineBt?.isSelected = false
        }


        offLineBt?.setOnClickListener {
            offLineBt?.isSelected = !offLineBt?.isSelected!!
            glView.helloAR.stop()
            glView.helloAR.dispose()
            if (offLineBt?.isSelected!!) {
                val demo = appDatabase?.demoDao()?.getFirstDemo()!!
                if (jsonPath == null) {
                    appFileLoader.createEasyArConfigurationFileFromDemonstration(demo) { filepath ->
                        appRouter.toCameraActionActivity(this, filepath)
                    }

                } else {
                    appRouter.toCameraActionActivity(this, demo.localConfigPath)
                }
            } else {
                val i = baseContext.packageManager.getLaunchIntentForPackage( baseContext.packageName);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i)
            }
        }

        offLineBt?.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (playerIsReadyToBeControledByUser()) {
            val target = supportFragmentManager.findFragmentByTag(VideoPlayerFullScreenFragment::class.java.canonicalName)
            if (target != null) {
                supportFragmentManager.beginTransaction().remove(target).commit()
                glView.visibility = View.VISIBLE;
                glView.play()
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    private val task = {
        if (playerControls?.alpha == 1.0F) {
            playerControls?.animate()?.setDuration(DEFAULT_ANIMATION_DURATION)?.alpha(0.0F)?.start()
        }
    }

    override fun onDrawerClosed(drawerView: View) {
        toolbar?.setNavigationIcon(R.drawable.ic_drawer_icon_closed)
    }
    override fun onDrawerOpened(drawerView: View) {
        toolbar?.navigationIcon = null
    }
    override fun onDrawerStateChanged(newState: Int) {}
    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}

    override fun onDrawerClosedInvoked() {
        drawerLayout?.closeDrawer(GravityCompat.START)
    }

    override fun onDisplayOnboarding() {

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, OnBoardingFragment.create(WeakReference(this)), OnBoardingFragment::class.java.canonicalName)
                .commit()
    }

    private fun removeOnboarding() {
        val fragment = supportFragmentManager.findFragmentByTag(OnBoardingFragment::class.java.canonicalName)
        if (fragment != null) {
            supportFragmentManager.beginTransaction().remove(fragment).commit()
        }

//        toolbar?.setNavigationIcon(R.drawable.ic_drawer_icon_closed)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onOnboardingFinishedEvent(event : OnOnboardingFinished) {
        onOnBoardFinished()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onFullScreenVideoFinished(event: OnFullScreenVideoFinished) {
        onBackPressed()
    }

    class OnOnboardingFinished
    class OnFullScreenVideoFinished
}
