package br.com.aseventos.recon.ui.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.MediaEntity
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.R
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.DemoUtils
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.ui.adapters.DemonstrationAdapter
import br.com.aseventos.recon.ui.presenters.DemonstrationPresenter
import br.com.aseventos.recon.ui.view.DemonstrationView
import br.com.aseventos.recon.ui.viewmodels.DemoViewModel
import com.tonyodev.fetch2.Download
import com.tonyodev.fetch2core.Func
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DemonstrationsActivity : DrawerActivity(), DemonstrationView {
    override fun getToolbarTitle(): String = "Demonstrações"
    override fun getToolbarNavigationIcon(): Int = R.drawable.ic_drawer_closed
    override fun layoutResourceId(): Int = R.layout.activity_demonstrations
    override fun onInject(graph: ActivityComponent?) = graph?.inject(this)!!

    @Inject lateinit var presenter : DemonstrationPresenter
    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var appFileLoader : FileLoader

    private val recycler by lazy { findViewById<RecyclerView>(R.id.demos_recycler) }
    private val emptyState by lazy { findViewById<ViewGroup>(R.id.frame_empty_state) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        presenter.fetchAllDemos(this)

    }

    override fun onUnsubscribe() {
        presenter.unsubscribe()
    }

    override fun onDemonstrationsFetched(items: List<DemoEntity>) {
        emptyState.visibility = View.GONE
        recycler.adapter = DemonstrationAdapter(items.flatMap {
            return@flatMap mutableListOf(DemoUtils.buildDemoViewModel(it.id, appDatabase))
        } as MutableList<DemoViewModel>, this)
        recycler.visibility = View.VISIBLE
    }

    override fun onDemonstrationsEmpty() {
        emptyState.visibility = View.VISIBLE
    }

    override fun onDemonstrationRemoved(demoId : Int) {
        appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                TrackerAdapter.EventName.DEMO_REMOVE,
                null)

        val videos = appDatabase?.mediaDao()?.getByDemoId(demoId) ?: emptyList()
        val targets = videos.flatMap { listOf(appDatabase?.targetDao()?.getByMediaId(it.id) ?: emptyList()) }

        videos.forEach {
            if (it.localPathUri != null && it.localPathUri!!.isNotEmpty()) {
                appFileLoader.removeFileWithLocalPath(it.localPathUri!!)
            }
        }
        targets.forEach {
            it.forEach { target ->
                if (target.fileBlob != null && target.fileBlob!!.isNotEmpty()) {
                    appFileLoader.removeFileWithLocalPath(target.fileBlob!!)
                }
            }
        }

        AsReconApplication.FETCH_INSTANCE.getDownloads(Func {
            it.forEach { download ->
                val extraDemoId = download.extras.getString(DownloadsActivity.DOWNLOAD_DEMO_ID_EXTRA, "-1")
                if(extraDemoId.contentEquals(demoId.toString())) {
                    AsReconApplication.FETCH_INSTANCE.remove(download.id)
                }
            }
        })

        appDatabase?.demoDao()?.removeById(demoId)
        val adapter = recycler.adapter
        if (adapter is DemonstrationAdapter) {
            adapter.remove(demoId)
            if (adapter.items.size == 0) {
                emptyState.visibility = View.VISIBLE
            }
        }
    }

    override fun onLoadDemonstration(demoId: Int) {
        appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                TrackerAdapter.EventName.DEMO_LOAD,
                "L")
        val demonstration = appDatabase?.demoDao()?.getById(demoId)
        if (demonstration != null) {
            if (demonstration.status.contentEquals(DemoEntity.STATUS_STORED)) {
                appFileLoader.createEasyArConfigurationFileFromDemonstration(demonstration) {filePath ->
                    appRouter.toCameraActionActivity(this@DemonstrationsActivity, filePath)
                }
            } else {
                appRouter.toCameraActionActivity(this)
            }
        }
    }

    override fun onShowDemonstrationContent(demoId: Int) {
        appRouter.toMediaList(this, demoId)
    }

    override fun getFileLoader(): FileLoader {
        return appFileLoader
    }
}