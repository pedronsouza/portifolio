package br.com.aseventos.recon.ui.presenters

import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.data.db.entities.MediaEntity
import br.com.aseventos.data.db.entities.TargetEntity
import br.com.aseventos.data.usecases.demonstrations.CreateNewDemonstrationUseCase
import br.com.aseventos.data.usecases.demonstrations.FetchDemonstrationBySearchItemIdUseCase
import br.com.aseventos.data.usecases.media.FetchDirectoryItemsUseCase
import br.com.aseventos.data.usecases.media.FetchUserItemsUseCase
import br.com.aseventos.data.usecases.users.UploadCustomTargetImageUseCase
import br.com.aseventos.data.usecases.users.UploadCustomTargetVideoUseCase
import br.com.aseventos.data.usecases.v2.FindUserFilesCase
import br.com.aseventos.domain.models.Directory
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.v2.AppMedia
import br.com.aseventos.recon.helpers.BaseSubscriber
import br.com.aseventos.recon.helpers.DemoUtils
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.services.DownloadService
import br.com.aseventos.recon.ui.view.MediaListView
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import br.com.aseventos.recon.ui.viewmodels.SearchItemViewModel
import br.com.aseventos.recon.workers.DownloadWorker
import javax.inject.Inject

class MediaListPresenter : BasePresenter {
    private val appDatabase : ReconDatabase?
    private val appFileLoader : FileLoader
    private val fetchDirectoryItemsUseCase : FetchDirectoryItemsUseCase
    private val fetchDemonstrationBySearchItemIdUseCase: FetchDemonstrationBySearchItemIdUseCase
    private val createNewDemonstrationUseCase: CreateNewDemonstrationUseCase
    private val fetchUserItemsUseCase: FetchUserItemsUseCase
    private val uploadCustomTargetVideoUseCase: UploadCustomTargetVideoUseCase
    private val uploadCustomTargetImageUseCase: UploadCustomTargetImageUseCase
    private val findUserFilesCase : FindUserFilesCase

    @Inject constructor(appDatabase: ReconDatabase?,
                        appFileLoader : FileLoader,
                        fetchDirectoryItemsUseCase: FetchDirectoryItemsUseCase,
                        fetchDemonstrationBySearchItemIdUseCase: FetchDemonstrationBySearchItemIdUseCase,
                        createNewDemonstrationUseCase: CreateNewDemonstrationUseCase,
                        fetchUserItemsUseCase: FetchUserItemsUseCase,
                        uploadCustomTargetVideoUseCase: UploadCustomTargetVideoUseCase,
                        uploadCustomTargetImageUseCase: UploadCustomTargetImageUseCase,
                        findUserFilesCase : FindUserFilesCase) {

        this.appDatabase = appDatabase
        this.appFileLoader = appFileLoader
        this.fetchDirectoryItemsUseCase = fetchDirectoryItemsUseCase
        this.fetchDemonstrationBySearchItemIdUseCase = fetchDemonstrationBySearchItemIdUseCase
        this.createNewDemonstrationUseCase = createNewDemonstrationUseCase
        this.fetchUserItemsUseCase = fetchUserItemsUseCase
        this.uploadCustomTargetVideoUseCase = uploadCustomTargetVideoUseCase
        this.uploadCustomTargetImageUseCase = uploadCustomTargetImageUseCase
        this.findUserFilesCase = findUserFilesCase
    }


    fun createFilesForDownloadAndSendToDownloadView(demoId : Int, view : MediaListView) {
        val downloadWorker = OneTimeWorkRequest.Builder(DownloadWorker::class.java)
        val data = Data.Builder()
        data.putInt(DownloadService.DEMO_ITEM_FLAG, demoId)
        downloadWorker.setInputData(data.build())
        WorkManager.getInstance().enqueue(downloadWorker.build())
        view.onFilesCreatedAndDownloadServiceStarted()
    }

    //FIX
    fun fetchItemsUseCaseBySearchIemWithDemonstration(searchItem: SearchItemViewModel,
                                                      demo: DemoEntity,
                                                      view : MediaListView) {
        view.startProgress()
        when(searchItem.type) {
            SearchResult.Type.USER -> {
                this.findUserFilesCase.baseSystemId = searchItem.id
                this.findUserFilesCase.execute(FetchUserFilesSubscriber(demo.id, view))
            }
            SearchResult.Type.DIRECTORY -> {
                this.fetchDirectoryItemsUseCase.itemId = searchItem.id
                this.fetchDirectoryItemsUseCase.execute(FetchDirectoryItemsSubscriber(demo.id, view))
            }
        }
    }

    fun checkAndCreateDemonstrationWithSearchItem(searchItem: SearchItemViewModel, view: MediaListView) {
        this.fetchDemonstrationBySearchItemIdUseCase.searchItemId = searchItem.id
        this.fetchDemonstrationBySearchItemIdUseCase.execute(FetchDemoBySearchItemIdSubscriber(searchItem, view))
    }

    fun createNewDemoWithSearchItem(searchItem: SearchItemViewModel, view: MediaListView) {
        this.createNewDemonstrationUseCase.searchItemId = searchItem.id
        this.createNewDemonstrationUseCase.title = searchItem.title
        this.createNewDemonstrationUseCase.execute(CreateNewDemonstrationSubscriber(view))
    }

    override fun unsubscribe() {
        this.fetchDirectoryItemsUseCase.unsubscribe()
        this.fetchDemonstrationBySearchItemIdUseCase.unsubscribe()
        this.createNewDemonstrationUseCase.unsubscribe()
        this.fetchUserItemsUseCase.unsubscribe()
        this.uploadCustomTargetImageUseCase.unsubscribe()
        this.uploadCustomTargetVideoUseCase.unsubscribe()
        this.appDatabase?.destroyInstance()
        this.findUserFilesCase.unsubscribe()
    }

    private inner class FetchUserFilesSubscriber(val demoId : Int, val view : MediaListView) : BaseSubscriber<List<AppMedia>>() {
        override fun onCompleted() {
            super.onCompleted()
            view.stopProgress()
        }

        override fun onNext(item: List<AppMedia>) {
            super.onNext(item)
            val demo = appDatabase?.demoDao()?.getById(demoId)
            if (demo != null) {
                if (item.isEmpty()) {
                    view.onMediaListItemsFetchError(IllegalStateException("Nenhum arquivo encontrado para esse usuário"))
                } else {
                    val items = item.flatMap { recon ->
                        val video = MediaEntity()
                        synchronized(video) {
                            video.demoId = demoId
                            video.sourceUri = recon.url
                            video.status = DemoEntity.STATUS_IDLE
                            video.id = appDatabase?.mediaDao()?.insert(video)?.toInt()!!
                            recon.photos.forEach { t ->
                                val target = TargetEntity()
                                target.mediaId = video.id
                                target.fileBlob = t.url
                                target.id = appDatabase.targetDao().insert(target).toInt()
                            }

                            listOf(video)
                        }
                    }

                    view.onMediaListItemsFetched(demo, buildMediaItemViewModelByRecons(items))
                }
            } else {
                view.onMediaListItemsFetchError(IllegalArgumentException("demoId ${demoId} not founded on local database"))
            }
        }
    }

    private inner class FetchDirectoryItemsSubscriber(val demoId: Int, val view : MediaListView) : BaseSubscriber<Directory>() {
        override fun onCompleted() {
            super.onCompleted()
            view.stopProgress()
        }

        override fun onNext(item: Directory) {
            super.onNext(item)
            val demo = appDatabase?.demoDao()?.getById(demoId)

            if (demo != null) {
                if (item.recons.isEmpty()) {
                    view.onMediaListItemsFetchError(IllegalStateException("Nenhum arquivo encontrado para esse usuário"))
                } else {
                    val items = item.recons.flatMap {recon ->
                        val video = MediaEntity()
                        synchronized(video) {
                            video.demoId = demoId
                            video.sourceUri = DemoUtils.sourceUriForVideoWithRecon(recon.video._id)
                            video.status = DemoEntity.STATUS_IDLE
                            video.id = appDatabase?.mediaDao()?.insert(video)?.toInt()!!
                            recon.photos.forEach { t ->
                                val target = TargetEntity()
                                target.mediaId = video.id
                                target.fileBlob = appFileLoader.urlForImageHandlerWithTargetSourceId(t.id)
                                target.id = appDatabase.targetDao().insert(target).toInt()
                            }

                            listOf(video)
                        }
                    }
                    view.onMediaListItemsFetched(demo, buildMediaItemViewModelByRecons(items))
                }

            } else {
                view.onMediaListItemsFetchError(IllegalArgumentException("demoId ${demoId} not founded on local database"))
            }
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            val demo = appDatabase?.demoDao()?.getById(demoId)

            if (demo != null) {
                appDatabase?.demoDao()?.removeById(demoId)
            }

            view.onMediaListItemsFetchError(e)
        }
    }

    private inner class FetchDemoBySearchItemIdSubscriber(val searchItem: SearchItemViewModel, val view : MediaListView) : BaseSubscriber<DemoEntity?>() {
        override fun onNext(item: DemoEntity?) {
            super.onNext(item)
            if (item == null) {
                view.onNoDemoCreatedForSearchItem()
            } else {
                view.onDemonstrationRetrieved(item)
            }
        }

        override fun onError(e: Throwable) {
            super.onError(e)
        }
    }

    private inner class CreateNewDemonstrationSubscriber(val view : MediaListView) : BaseSubscriber<DemoEntity>() {
        override fun onNext(item: DemoEntity) {
            super.onNext(item)
            view.onDemonstrationCreated(item)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            view.onDemonstrationCreationError(e)
        }
    }



    fun buildMediaItemViewModelByRecons(videos : List<MediaEntity>) : List<MediaListItemViewModel> {
        return videos.flatMap {
            val firstTarget = appDatabase?.targetDao()?.getFirstByMediaId(it.id)
            if (firstTarget != null) {
                listOf(MediaListItemViewModel(
                                    id = it.id,
                                    demoId = it.demoId,
                                    name = "Video 1",
                                    description = "",
                                    archived = false,
                                    targetId = firstTarget.id,
                                    sourceUrl = it.sourceUri,
                                    isCustomTarget =  false,
                                    type = MediaListItemViewModel.Type.VIDEO
                                )
                        )
            } else {
                emptyList()
            }
        }
    }

    fun isDemoReadyForSync(demonstration: DemoEntity): Boolean {
        val videos = appDatabase?.mediaDao()?.getByDemoId(demonstration.id)
        return (videos != null && videos.isNotEmpty())
    }
}