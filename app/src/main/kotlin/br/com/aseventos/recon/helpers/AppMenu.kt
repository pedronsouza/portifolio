package br.com.aseventos.recon.helpers

import android.content.Context
import br.com.aseventos.domain.models.Session
import br.com.aseventos.recon.AsReconApplication.Companion.isInternalBuild
import br.com.aseventos.recon.BuildConfig
import br.com.aseventos.recon.ui.viewmodels.MenuItemViewModel
import javax.inject.Inject

class AppMenu {
    @Inject constructor() {

    }

    fun createMenuByUserSession(session: Session, listener: Listener) : List<MenuItemViewModel> {
        val items = mutableListOf<MenuItemViewModel>()

        if (session.scope == Session.Scope.GUEST) {
            items.plusAssign(MenuItemViewModel( "Home",         { listener.onHomeItemMenuClicked() }))
        } else if (isInternalBuild()) {
            items.plusAssign(MenuItemViewModel( "Home",         { listener.onHomeItemMenuClicked() }))
            items.plusAssign(MenuItemViewModel( "Buscar",       { listener.onSearchItemMenuClicked() }))
            items.plusAssign(MenuItemViewModel( "Demonstração", { listener.onDemosScreenItemMenuClicked() }))
            items.plusAssign(MenuItemViewModel( "Downloads",    { listener.onDownloadScreenItemMenuClicked() }))
        } else {
            items.plusAssign(MenuItemViewModel( "Home",         { listener.onHomeItemMenuClicked() }))
            items.plusAssign(MenuItemViewModel( "Mídias",       { listener.onAppMediaItemMenuClicked() }))
            items.plusAssign(MenuItemViewModel( "Downloads",       { listener.onDownloadScreenItemMenuClicked() }))
        }

        return items.toList()
    }

    interface Listener {
        fun getCtx() : Context
        fun onHomeItemMenuClicked()
        fun onSearchItemMenuClicked()
        fun onAppMediaItemMenuClicked()
        fun onLogOutMediaItemMenuClicked()
        fun onDemosScreenItemMenuClicked()
        fun onDownloadScreenItemMenuClicked()
    }
}