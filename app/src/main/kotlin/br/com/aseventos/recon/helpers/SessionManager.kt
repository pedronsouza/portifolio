package br.com.aseventos.recon.helpers

import android.content.SharedPreferences
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import com.google.gson.Gson

class SessionManager(var session : Session, private val preferences: SharedPreferences, private val gson: Gson) {
    companion object {
        val DEFAULT_GUEST_USER_ID = "guestuserid"
        val SESSION_STORAGE_KEY = "SESSION_STORAGE_KEY"
        val SESSION_FIRST_ACCESS_KEY = "%s_FIRST_ACCESS_KEY"
    }

    fun isAuthorized() : Boolean =
            session.user != null || session.passwordCreationToken != null

    fun isGuestUser() : Boolean =
            session.user != null || session.scope == Session.Scope.GUEST

    fun createNewSessionForGuestUser() : Boolean {
        val user = User(DEFAULT_GUEST_USER_ID, null, null, null, null, null, null)
        val newSession = Session(user, null, Session.Scope.GUEST)
        this.session = newSession
        return store()
    }

    fun updateScopeWithUser(user : User, scope : Session.Scope) : Boolean {
        this.session.scope = scope
        this.session.user = user
        return store()
    }

    fun isSessionFirstAccess() : Boolean =
            preferences.getBoolean(String.format(SESSION_FIRST_ACCESS_KEY, session.user?.id), true)

    fun markAcess() : Boolean =
            preferences.edit().putBoolean(String.format(SESSION_FIRST_ACCESS_KEY, session.user?.id), false).commit()

    fun store() : Boolean {
        val raw = gson.toJson(this.session, Session::class.java)
        return preferences.edit().putString(SESSION_STORAGE_KEY, raw).commit()
    }

    fun getNetToken() : String? = session.user?.token

    fun logout() : Boolean =
        preferences.edit().remove(SESSION_STORAGE_KEY).commit()

}