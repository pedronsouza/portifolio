package br.com.aseventos.recon.ui.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.fragments.onboarding.OnBoardingStepFragment

class OnBoardingStepAdapter(fragmentManager: FragmentManager, val context : Context?) : FragmentStatePagerAdapter(fragmentManager) {
    companion object {
        val TOTAL_STEPS = 3
    }

    val titles = arrayOf(R.string.onboarding_step_one_title, R.string.onboarding_step_two_title, R.string.onboarding_step_three_title)
    val msgs = arrayOf(R.string.onboard_step_one_msg, R.string.onboard_step_two_msg, R.string.onboard_step_three_msg)
    val icons = arrayOf(R.drawable.ic_onboard_first_step, R.drawable.ic_onboard_second_step, R.drawable.ic_onboard_third_step)

    override fun getItem(position: Int): Fragment {
        return OnBoardingStepFragment.create(context?.getString(titles[position])!!, context.getString(msgs[position]), icons[position])
    }

    override fun getCount(): Int {
        return TOTAL_STEPS
    }
}