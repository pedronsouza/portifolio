package br.com.aseventos.recon.ui.view

import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.models.v2.AppCreationToken

interface NewUserView : ProgressibleView {
    fun onNewUserCreated(user : User)
    fun onNewUserCreationError(e : Throwable)
    fun onEmailValidatedOnServer(appCreationToken: AppCreationToken)
    fun onEmailValidatedOnServer(session: Session)
    fun onEmailValidationError(e : Throwable)
}