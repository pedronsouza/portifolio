package br.com.aseventos.recon.ui.activities

import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.view.ProgressibleView

abstract class ProgressibleActivity : AppCompatActivity(), ProgressibleView {
    private val progressBar by lazy { findViewById(R.id.main_progress_bar) as ProgressBar? }
    override fun startProgress() {
        progressBar?.isIndeterminate = true
        progressBar?.visibility = View.VISIBLE
    }

    override  fun stopProgress() {
        progressBar?.isIndeterminate = false
        progressBar?.visibility = View.INVISIBLE
    }
}