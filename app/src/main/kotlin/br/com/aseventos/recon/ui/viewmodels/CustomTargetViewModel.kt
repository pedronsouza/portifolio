package br.com.aseventos.recon.ui.viewmodels

import com.google.gson.annotations.Expose

data class CustomTargetViewModel(@Expose val videoUrl : String, @Expose val images : List<String>)