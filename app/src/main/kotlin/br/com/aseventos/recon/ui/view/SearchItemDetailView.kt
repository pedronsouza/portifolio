package br.com.aseventos.recon.ui.view

import br.com.aseventos.domain.models.Session

interface SearchItemDetailView {
    fun onTemporaryAccessCreated(session : Session)
    fun onTemporaryAccessCreationError(e: Throwable)
}