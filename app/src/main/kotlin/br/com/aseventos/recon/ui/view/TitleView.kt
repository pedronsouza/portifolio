package br.com.aseventos.recon.ui.view

interface TitleView {
    fun getViewTitle() : String
}