package br.com.aseventos.recon.ui.presenters

import br.com.aseventos.data.usecases.users.GeneralSearchUseCase
import br.com.aseventos.data.usecases.v2.FindUserInRemoteCase
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.SearchResultInfo
import br.com.aseventos.domain.models.v2.AppUser
import br.com.aseventos.recon.helpers.BaseSubscriber
import br.com.aseventos.recon.ui.view.SearchUserView
import javax.inject.Inject

class SearchUsersPresenter : BasePresenter {
    private val generalSearchUseCase : GeneralSearchUseCase
    private val findUserInRemoteCase: FindUserInRemoteCase

    @Inject constructor(generalSearchUseCase : GeneralSearchUseCase,
                        findUserInRemoteCase: FindUserInRemoteCase) {
        this.generalSearchUseCase = generalSearchUseCase
        this.findUserInRemoteCase = findUserInRemoteCase
    }

    fun searchUsersAndDirectory(query : String, view : SearchUserView) {
        view.showProgress()
        this.findUserInRemoteCase.query = query
        this.findUserInRemoteCase.execute(FindUserSubscriber(view))
    }

    inner class FindUserSubscriber(val view : SearchUserView) : BaseSubscriber<AppUser>() {
        override fun onCompleted() {
            super.onCompleted()
            view.hideProgress()
        }

        override fun onNext(item: AppUser) {
            super.onNext(item)
            view.onSearchResultsRetrieved(listOf(SearchResult(
                    id = item.baseSystemId,
                    title = item.name,
                    description = item.course,
                    imgUrl = item.photoUrl,
                    meta = SearchResultInfo(item.email, item.photoUrl, item.freeTrialEligible),
                    type = SearchResult.Type.USER)))
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            view.onSearchResultError(e)
        }
    }

    override fun unsubscribe() {
        this.generalSearchUseCase.unsubscribe()
        this.findUserInRemoteCase.unsubscribe()
    }
}