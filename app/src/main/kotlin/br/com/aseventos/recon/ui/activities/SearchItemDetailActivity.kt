package br.com.aseventos.recon.ui.activities

import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.Session
import br.com.aseventos.recon.R
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.ui.adapters.SearchMegaButtonsAdapter
import br.com.aseventos.recon.ui.presenters.SearchItemDetailPresenter
import br.com.aseventos.recon.ui.view.SearchItemDetailView
import br.com.aseventos.recon.ui.view.TrackableView
import br.com.aseventos.recon.ui.viewmodels.MegaButtonViewModel
import br.com.aseventos.recon.ui.viewmodels.SearchItemViewModel
import com.facebook.drawee.view.SimpleDraweeView
import javax.inject.Inject

class SearchItemDetailActivity : ToolbarActivity(), SearchItemDetailView, TrackableView {
    companion object {
        val INTENT_USER_PROFILE_ITEM_FLAG = "INTENT_USER_PROFILE_ITEM"
    }


    override fun layoutResourceId(): Int = R.layout.activity_user_profile
    override fun onInject(graph: ActivityComponent?) = graph?.inject(this)!!
    override fun getToolbarTitle(): String = ""
    override fun getToolbarNavigationIcon(): Int = R.drawable.ic_back_arrow_white

    private val item                by lazy { intent.getParcelableExtra<SearchItemViewModel>(INTENT_USER_PROFILE_ITEM_FLAG) }

    private val label               by lazy { findViewById<TextView>(R.id.search_item_detail_label) }
    private val description         by lazy { findViewById<TextView>(R.id.search_item_detail_description) }
    private val thumbDir            by lazy { findViewById<ImageView>(R.id.search_item_detail_thumb_dir) }
    private val thumbUsr            by lazy { findViewById<SimpleDraweeView>(R.id.search_item_detail_thumb) }
    private val buttonRecycler      by lazy { findViewById<RecyclerView>(R.id.search_detail_buttons_recycler) }

    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var presenter : SearchItemDetailPresenter

    override fun getTrackPageName(): TrackerAdapter.PageName = TrackerAdapter.PageName.SEARCH_RESULT_DETAIL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar?.setNavigationOnClickListener { onBackPressed() }
        buttonRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        val l = mutableListOf<MegaButtonViewModel>()
        l.plusAssign(MegaButtonViewModel(R.drawable.ic_demo_menu, "Demonstração") {
            appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                    TrackerAdapter.EventName.NEW_DEMO,
                    item.id)

            appRouter.toMediaList(this@SearchItemDetailActivity, item)
        })

        if(item.type == SearchResult.Type.USER && !item.temporaryAccessEnabled) {
            l.plusAssign(MegaButtonViewModel(R.drawable.ic_temporary_access, "Acesso Temporário") { presenter.enableTemporaryAcessForUser(item, this@SearchItemDetailActivity) })
        }

        val demo = appDatabase?.demoDao()?.findByBaseSystemId(item.id)
        if (demo != null && demo.status.contentEquals(DemoEntity.STATUS_DOWNLOADING)) {
            l.plusAssign(MegaButtonViewModel(R.drawable.ic_download_video_xxhdpi, "Ver downloads") {
                appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                        TrackerAdapter.EventName.TEMPORARY_ACCESS,
                        null)
                appRouter.toDownloads(this@SearchItemDetailActivity, null)
            })
        }


        buttonRecycler.adapter = SearchMegaButtonsAdapter(l)

        label.text = item.title
        description.text = item.description

        if (item.type == SearchResult.Type.USER) {
            if (item.imgUrl != null)
                thumbUsr.setImageURI(Uri.parse(item.imgUrl), null)
        } else {
            thumbDir.setImageResource(R.drawable.ic_directory_placeholder)
        }
    }

    override fun onUnsubscribe() {
        presenter.unsubscribe()
    }

    override fun onTemporaryAccessCreated(session : Session) {
        appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                TrackerAdapter.EventName.TEMPORARY_ACCESS,
                "S")
        AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage("Acesso Temporário liberado para o usuário ${item.title}")
                .setPositiveButton("OK") { d: DialogInterface, _: Int ->
                    d.dismiss()
                }.show()

        val adapter = buttonRecycler.adapter as SearchMegaButtonsAdapter
        val indexOf = adapter.itens.indexOfFirst { it.resId == R.drawable.ic_temporary_access }
        if (indexOf != -1) {
            adapter.itens.removeAt(indexOf)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onTemporaryAccessCreationError(e: Throwable) {
        appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                TrackerAdapter.EventName.TEMPORARY_ACCESS,
                "E")
        AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage("Erro ao liberar o acesso temporário: ${e.message}")
                .setPositiveButton("OK, entendi") { d: DialogInterface, _: Int ->
                    d.dismiss()
                }.show()
    }
}