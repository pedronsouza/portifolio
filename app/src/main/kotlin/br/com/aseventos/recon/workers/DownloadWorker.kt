package br.com.aseventos.recon.workers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.AsReconApplication.Companion.FETCH_INSTANCE
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.helpers.DemoUtils
import br.com.aseventos.recon.services.DownloadService
import br.com.aseventos.recon.ui.activities.DownloadsActivity
import br.com.aseventos.recon.ui.events.OnDataEnqueuedEvent
import br.com.aseventos.recon.ui.events.OnDownloadProgressUpdate
import br.com.aseventos.recon.ui.viewmodels.DownloadableItemViewModel
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.DownloadBlock
import com.tonyodev.fetch2core.Extras
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.io.FileOutputStream

class DownloadWorker(context : Context, params : WorkerParameters) : Worker(context, params) {
    private val appDatabase by lazy { appGraph?.database() }
    private val appFileLoader by lazy { appGraph?.appFileLoader() }

    companion object {
        public fun saveTargetImages(filename : String, data : Bitmap, onImageSaved: (String) -> Unit) {
            try {
                val out = FileOutputStream(filename)
                data.compress(Bitmap.CompressFormat.PNG, 100, out)
                out.flush()
                out.close()
                onImageSaved(filename)
            } catch (e: Exception) {

            }

        }
    }

    override fun doWork(): Result {
        val demoId = inputData.getInt(DownloadService.DEMO_ITEM_FLAG, -1)
        val demonstration = appDatabase?.demoDao()?.getById(demoId)

        if (demonstration != null) {
            val items = DemoUtils.buildDownloableListOfItensFromDemo(demonstration, appDatabase)
            if (items.isNotEmpty()) {
                AsReconApplication.FETCH_INSTANCE.enqueue(items.flatMap {
                    createFileIfNeed(it)
                    appDatabase?.mediaDao()?.setStatusById(DemoEntity.STATUS_DOWNLOADING, it.reconId)

                    val req = Request(it.remotePath, it.localPath)
                    it.requestId = req.id.toString()
                    req.extras = Extras(mapOf(Pair(DownloadsActivity.DOWNLOAD_DEMO_ID_EXTRA, demoId.toString()),
                            Pair(DownloadsActivity.DOWNLOAD_VIDEO_ID_EXTRA, it.reconId.toString())
                    ))

                    req.priority = Priority.NORMAL
                    req.networkType = NetworkType.ALL
                    mutableListOf(req)
                })

                appDatabase?.demoDao()?.setStatusForDemo(DemoEntity.STATUS_DOWNLOADING, demoId)
                demonstration.status = DemoEntity.STATUS_DOWNLOADING

                EventBus.getDefault().post(OnDataEnqueuedEvent())
            }
        }
        return Result.SUCCESS
    }

    private fun createFileIfNeed(downloadableItem: DownloadableItemViewModel) {
        try {
            val f = File(downloadableItem.localPath)
            f.createNewFile()
        } catch(e : Throwable) {
            Log.d("MediaListPresenter", "Error creating file ${Log.getStackTraceString(e)}")
        }
    }

    private val fetchImageListener = object: FetchListener {
        override fun onAdded(download: Download) {

        }

        override fun onCancelled(download: Download) {

        }

        override fun onCompleted(download: Download) {

        }

        override fun onDeleted(download: Download) {

        }

        override fun onDownloadBlockUpdated(download: Download, downloadBlock: DownloadBlock, totalBlocks: Int) {

        }

        override fun onError(download: Download, error: Error, throwable: Throwable?) {

        }

        override fun onPaused(download: Download) {

        }

        override fun onProgress(download: Download, etaInMilliSeconds: Long, downloadedBytesPerSecond: Long) {

        }

        override fun onQueued(download: Download, waitingOnNetwork: Boolean) {

        }

        override fun onRemoved(download: Download) {

        }

        override fun onResumed(download: Download) {

        }

        override fun onStarted(download: Download, downloadBlocks: List<DownloadBlock>, totalBlocks: Int) {

        }

        override fun onWaitingNetwork(download: Download) {

        }
    }

    private fun showReportNotification(demonstration: DemoEntity) {}



}