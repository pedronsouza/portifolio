package br.com.aseventos.recon.ui.presenters

import br.com.aseventos.data.usecases.session.CheckUserEmailOnServerUseCase
import br.com.aseventos.data.usecases.session.CreatePasswordUserCase
import br.com.aseventos.data.usecases.session.RequestChangePasswordTokenUseCase
import br.com.aseventos.data.usecases.v2.SaveUserPasswordCase
import br.com.aseventos.data.usecases.v2.ValidateUserEmailCase
import br.com.aseventos.domain.execptions.validations.BadEmailFormatException
import br.com.aseventos.domain.execptions.validations.PasswordNotMatchException
import br.com.aseventos.domain.execptions.validations.TemporaryAcesssException
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.models.v2.AppCreationToken
import br.com.aseventos.domain.models.v2.AppSession
import br.com.aseventos.recon.helpers.BaseSubscriber
import br.com.aseventos.recon.ui.view.NewUserView
import retrofit2.adapter.rxjava.HttpException
import java.lang.ref.WeakReference
import java.util.regex.Pattern


class NewUserPresenter : BasePresenter {
    companion object {
        private val VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)
        private val VALID_PWD_RANGE_REGEX = Pattern.compile("^.{4,8}\$", Pattern.CASE_INSENSITIVE)
    }
    private var checkUserEmailOnServerUseCase: CheckUserEmailOnServerUseCase
    private var validateUserEmailCase : ValidateUserEmailCase
    private var createPasswordUseCase : CreatePasswordUserCase
    private var requestChangePasswordTokenUseCase : RequestChangePasswordTokenUseCase
    private var saveUserPasswordCase : SaveUserPasswordCase

    constructor(checkUserEmailOnServerUseCase: CheckUserEmailOnServerUseCase,
                createPasswordUseCase : CreatePasswordUserCase,
                requestChangePasswordTokenUseCase : RequestChangePasswordTokenUseCase,
                validateUserEmailCase : ValidateUserEmailCase,
                saveUserPasswordCase : SaveUserPasswordCase) : super() {
        this.checkUserEmailOnServerUseCase = checkUserEmailOnServerUseCase
        this.createPasswordUseCase = createPasswordUseCase
        this.requestChangePasswordTokenUseCase = requestChangePasswordTokenUseCase
        this.validateUserEmailCase = validateUserEmailCase
        this.saveUserPasswordCase = saveUserPasswordCase
    }

    fun createUserWithEmail(email : String, view : WeakReference<NewUserView>) {
        val emailCheck = VALID_EMAIL_ADDRESS_REGEX.matcher(email).find()

        if (emailCheck) {
            validateUserEmailCase.email = email
            validateUserEmailCase.execute(CheckUserEmailSubscriber(view))
        } else {
            view.get()?.onNewUserCreationError(PasswordNotMatchException())
        }
    }

    fun createPasswordForUserWith(password : String, confirm : String, session: Session, view : WeakReference<NewUserView>) {
        if (VALID_PWD_RANGE_REGEX.matcher(password).find()) {
            if (password.contentEquals(confirm)) {
                saveUserPasswordCase.password = password
                saveUserPasswordCase.passwordCreationToken = session.passwordCreationToken
                saveUserPasswordCase.execute(CreatePasswordSubscriber(view))
            } else {
                view.get()?.onNewUserCreationError(PasswordNotMatchException())
            }
        } else {
            view.get()?.onNewUserCreationError(BadEmailFormatException())
        }
    }

    fun requestChangePasswordTokenForPwdCreation(email : String, view : NewUserView) {
        this.requestChangePasswordTokenUseCase.email = email
        this.requestChangePasswordTokenUseCase.execute(RequestChangePasswordTokenPwd(view))
    }

    override fun unsubscribe() {
        checkUserEmailOnServerUseCase.unsubscribe()
        createPasswordUseCase.unsubscribe()
        requestChangePasswordTokenUseCase.unsubscribe()
        validateUserEmailCase.unsubscribe()
        saveUserPasswordCase.unsubscribe()
    }

    inner class RequestChangePasswordTokenPwd(val view : NewUserView) : BaseSubscriber<User>() {
        override fun onNext(item: User) {
            super.onNext(item)
            view.onEmailValidatedOnServer(Session(item, item.token, Session.Scope.CREATION_TOKEN))
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            view.onEmailValidationError(e)
        }
    }

    inner class CreatePasswordSubscriber(val view : WeakReference<NewUserView>) : BaseSubscriber<AppSession>() {
        override fun onNext(item: AppSession) {
            super.onNext(item)
            val user = User(
                    id = item.user.baseSystemId,
                    name = item.user.name,
                    course = item.user.course,
                    email = item.user.email,
                    password = null,
                    token = item.token,
                    photo = item.user.photoUrl
            )

            view.get()?.onNewUserCreated(user)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            if (e is HttpException) {
                val c = e.code()
                when(c) {
                    422 -> { view.get()?.onNewUserCreationError(TemporaryAcesssException()) }
                    else -> { view.get()?.onNewUserCreationError(e) }
                }
            } else {
                view.get()?.onNewUserCreationError(e)
            }

        }
    }

    inner class CheckUserEmailSubscriber(val view : WeakReference<NewUserView>) : BaseSubscriber<AppCreationToken>() {
        override fun onNext(item: AppCreationToken) {
            view.get()?.onEmailValidatedOnServer(item)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            if (e is HttpException) {
                val c = e.code()
                when(c) {
                    422 -> { view.get()?.onEmailValidationError(TemporaryAcesssException()) }
                    else -> { view.get()?.onEmailValidationError(e) }
                }
            } else {
                view.get()?.onEmailValidationError(e)
            }
        }

    }
}