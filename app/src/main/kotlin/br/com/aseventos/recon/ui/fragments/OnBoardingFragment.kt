package br.com.aseventos.recon.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.activities.HomeActivity
import br.com.aseventos.recon.ui.adapters.OnBoardingStepAdapter
import br.com.aseventos.recon.ui.view.HomeView
import me.relex.circleindicator.CircleIndicator
import org.greenrobot.eventbus.EventBus
import java.lang.ref.WeakReference
import javax.inject.Inject


class OnBoardingFragment : Fragment(), ViewPager.OnPageChangeListener {
    companion object {
        fun create(parent : WeakReference<HomeView>) : OnBoardingFragment {
            val fragment = OnBoardingFragment()
            fragment.parent = parent
            return fragment
        }
    }

    lateinit var parent: WeakReference<HomeView>
    private lateinit var viewGroup : View

    private val viewPager by lazy { viewGroup.findViewById(R.id.on_boarding_viewpager) as ViewPager? }
    private val indicator by lazy { viewGroup.findViewById(R.id.on_boarding_viewpager_indicator) as CircleIndicator? }
    private val skipBt  by lazy { viewGroup.findViewById(R.id.skip_tutorial_br) as TextView? }
    private val nextStepBt  by lazy { viewGroup.findViewById(R.id.next_step_bt) as TextView? }

    var adapter : br.com.aseventos.recon.ui.adapters.OnBoardingStepAdapter? = null

    @Inject lateinit var sessionManager : SessionManager
    @Inject lateinit var appTracker : TrackerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appGraph?.inject(this)
        sessionManager.markAcess()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewGroup = LayoutInflater.from(context).inflate(R.layout.fragment_on_boarding, container, false)
        return viewGroup
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = OnBoardingStepAdapter(childFragmentManager, activity)

        appTracker.trackPageView("ONBOARDING_STEP_${viewPager!!.currentItem}")

        viewPager?.offscreenPageLimit = OnBoardingStepAdapter.TOTAL_STEPS
        viewPager?.adapter = adapter
        viewPager?.addOnPageChangeListener(this)
        indicator?.setViewPager(viewPager)

        skipBt?.setOnClickListener {
            if (viewPager!!.currentItem == (OnBoardingStepAdapter.TOTAL_STEPS - 1)) {
                appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                        TrackerAdapter.EventName.BOARDING_DONE,
                        null)
            } else {
                appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                        TrackerAdapter.EventName.BOARDING_DISMISS,
                        null)
            }

            EventBus.getDefault().post(HomeActivity.OnOnboardingFinished())
        }

        nextStepBt?.setOnClickListener {
            if (viewPager?.currentItem == 2) {
                EventBus.getDefault().post(HomeActivity.OnOnboardingFinished())
            } else {
                val nextItem = (viewPager?.currentItem ?: 0) + 1
                viewPager?.setCurrentItem(nextItem, true)
            }
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) {
        if (position == (OnBoardingStepAdapter.TOTAL_STEPS - 1)) {
            nextStepBt?.setText(R.string.ok_lets_go)
        } else {
            nextStepBt?.setText(R.string.next)
        }
    }
}