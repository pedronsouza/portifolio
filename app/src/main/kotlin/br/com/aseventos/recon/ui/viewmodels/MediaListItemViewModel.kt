package br.com.aseventos.recon.ui.viewmodels

import android.os.Parcel
import android.os.Parcelable

data class MediaListItemViewModel(val id : Int,
                                  val demoId : Int,
                                  val name : String,
                                  val description : String,
                                  val archived : Boolean = false,
                                  var targetId : Int,
                                  val sourceUrl : String,
                                  val isCustomTarget : Boolean,
                                  val type : Type) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt() == 1,
            parcel.readSerializable() as Type) {
    }

    enum class Type(val value : String) {
        VIDEO("video"),
        IMAGE("image")

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(demoId)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeByte(if (archived) 1 else 0)
        parcel.writeInt(targetId)
        parcel.writeString(sourceUrl)
        parcel.writeInt(if (isCustomTarget) 1 else 0)
        parcel.writeSerializable(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MediaListItemViewModel> {
        override fun createFromParcel(parcel: Parcel): MediaListItemViewModel {
            return MediaListItemViewModel(parcel)
        }

        override fun newArray(size: Int): Array<MediaListItemViewModel?> {
            return arrayOfNulls(size)
        }
    }
}