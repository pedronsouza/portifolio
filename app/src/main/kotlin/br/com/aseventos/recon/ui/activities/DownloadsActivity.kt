package br.com.aseventos.recon.ui.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.AsReconApplication.Companion.FETCH_INSTANCE
import br.com.aseventos.recon.R
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.ui.adapters.DownloadsAdapter
import br.com.aseventos.recon.ui.events.OnDownloadProgressUpdate
import br.com.aseventos.recon.ui.view.DownloadsView
import br.com.aseventos.recon.ui.view.TrackableView
import br.com.aseventos.recon.ui.viewmodels.DownloadItemViewModel
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import com.tonyodev.fetch2.Download
import com.tonyodev.fetch2.Error
import com.tonyodev.fetch2.FetchListener
import com.tonyodev.fetch2.Status
import com.tonyodev.fetch2core.DownloadBlock
import com.tonyodev.fetch2core.Func
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DownloadsActivity : ToolbarActivity(), DownloadsView, TrackableView {
    companion object {
        val DOWNLOADABLE_ITENS_KEY = "DOWNLOADABLE_ITENS"
        val DOWNLOAD_DEMO_ID_EXTRA = "DOWNLOAD_DEMO_ID_EXTRA"
        val DOWNLOAD_VIDEO_ID_EXTRA = "DOWNLOAD_VIDEO_ID_EXTRA"
        val DOWNLOAD_TYPE_EXTRA = "DOWNLOAD_TYPE_EXTRA"
    }

    override fun getToolbarTitle(): String = "Downloads"
    override fun getToolbarNavigationIcon(): Int = R.drawable.ic_back_dark
    override fun layoutResourceId(): Int = R.layout.activity_downloads
    override fun onInject(graph: ActivityComponent?) = graph?.inject(this)!!
    override fun onUnsubscribe() {}

    private val downloadsRecycler by lazy { findViewById<RecyclerView>(R.id.downloads_recycler) }
    private val emptyState        by lazy { findViewById<ViewGroup>(R.id.frame_empty_state) }

    lateinit var adapter : DownloadsAdapter
    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var appFileLoader : FileLoader


    override fun getTrackPageName(): TrackerAdapter.PageName = TrackerAdapter.PageName.DOWNLOAD

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (FETCH_INSTANCE.hasActiveDownloads) {
            menuInflater.inflate(R.menu.menu_download, menu)
            return true
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_clear_queue) {
            clearQueue()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun clearQueue() {
        FETCH_INSTANCE.cancelAll()
        FETCH_INSTANCE.removeAll()

        adapter.items.forEach { download ->
            val demo = appDatabase?.demoDao()?.getById(download.demoId)
            if (demo != null) {
                appDatabase?.demoDao()?.removeById(download.demoId)
            }
        }

        adapter.items.clear()
        adapter.notifyDataSetChanged()
        emptyState?.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar?.setNavigationOnClickListener { onBackPressed() }
        downloadsRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val scheduler = Executors.newSingleThreadScheduledExecutor()
        scheduler.scheduleAtFixedRate({
            FETCH_INSTANCE.getDownloads(Func { downloads ->
                if (downloads.isNotEmpty()) {
                    val failed = downloads.filter { it.status == Status.FAILED }
                    if (failed.isNotEmpty()) {
                        val requests = failed.flatMap { listOf(it.request) }
                        FETCH_INSTANCE.remove(failed.flatMap { listOf(it.id) })
                        FETCH_INSTANCE.enqueue(requests)
                    }
                }
            })
        }, 0, 1, TimeUnit.MINUTES)

        FETCH_INSTANCE.getDownloads(Func {downloads ->
            if (downloads.isEmpty()) {
                emptyState.visibility = View.VISIBLE
            } else {
                emptyState.visibility = View.GONE
                adapter = DownloadsAdapter(downloads.flatMap {
                    val demoId = it.extras.getString(DOWNLOAD_DEMO_ID_EXTRA, "-1").toInt()
                    val videoId = it.extras.getString(DOWNLOAD_VIDEO_ID_EXTRA, "-1").toInt()
                    var targetId = -1

                    val video = appDatabase?.mediaDao()?.getById(videoId)
                    if (video != null) {
                        val target = appDatabase?.targetDao()?.getFirstByMediaId(video.id)
                        targetId = target?.id ?: targetId
                    }

                    listOf(DownloadItemViewModel(
                            id = it.id.toString(),
                            demoId = demoId,
                            videoId = videoId,
                            name = it.url,
                            sourceUrl = it.url,
                            progress = it.progress,
                            targetId = targetId,
                            status = it.status)
                    )
                }.sortedByDescending { it.status == Status.DOWNLOADING }.toMutableList(), this@DownloadsActivity)

                downloadsRecycler.adapter = adapter
                FETCH_INSTANCE.addListener(fetchListener)

            }

        })
    }

    override fun onMediaItemClicked(item: DownloadItemViewModel) {
        val video = appDatabase?.mediaDao()?.getById(item.videoId)
        if (video != null) {
            appRouter.toMediaItemDetail(this, MediaListItemViewModel(
                    id = video.id,
                    demoId =  item.demoId,
                    name = item.name,
                    description = "",
                    archived = item.status == Status.COMPLETED,
                    targetId = item.targetId,
                    sourceUrl = item.sourceUrl,
                    isCustomTarget = false,
                    type = MediaListItemViewModel.Type.VIDEO
            ))
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDowloadProgressUpdate(event : OnDownloadProgressUpdate) {
        adapter.updateProgress(event.download, event.progress)
    }

    override fun getFileLoader(): FileLoader =
            appFileLoader

    val fetchListener = object: FetchListener {
        override fun onAdded(download: Download) {

        }

        override fun onCancelled(download: Download) {

        }

        override fun onCompleted(download: Download) {
            adapter.setAsDone(download)
            FETCH_INSTANCE.removeAllWithStatus(Status.COMPLETED)
        }

        override fun onDeleted(download: Download) {

        }

        override fun onDownloadBlockUpdated(download: Download, downloadBlock: DownloadBlock, totalBlocks: Int) {

        }

        override fun onError(download: Download, error: Error, throwable: Throwable?) {

        }

        override fun onPaused(download: Download) {

        }

        override fun onProgress(download: Download, etaInMilliSeconds: Long, downloadedBytesPerSecond: Long) {
            adapter.updateProgress(download, download.progress)
        }

        override fun onQueued(download: Download, waitingOnNetwork: Boolean) {

        }

        override fun onRemoved(download: Download) {

        }

        override fun onResumed(download: Download) {

        }

        override fun onStarted(download: Download, downloadBlocks: List<DownloadBlock>, totalBlocks: Int) {

        }

        override fun onWaitingNetwork(download: Download) {

        }

    }
}