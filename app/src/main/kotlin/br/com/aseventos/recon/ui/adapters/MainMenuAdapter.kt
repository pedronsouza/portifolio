package br.com.aseventos.recon.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.viewmodels.MenuItemViewModel

class MainMenuAdapter(val items : List<MenuItemViewModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = items.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_menu_item, parent, false)
        return MenuItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MenuItemViewHolder) {
            holder.update(items.get(position))
        }
    }

    class MenuItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val titleView by lazy { itemView.findViewById<TextView>(R.id.title) }

        fun update(model : MenuItemViewModel) {
            titleView.text = model.title
            itemView.setOnClickListener {
                model.onClickCompletionBlock()
            }
        }
    }
}