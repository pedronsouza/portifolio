package br.com.aseventos.recon

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.os.AsyncTask
import android.os.Environment
import android.os.StrictMode
import android.support.multidex.MultiDexApplication
import br.com.aseventos.domain.models.ARConfiguration
import br.com.aseventos.domain.models.Demonstration
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.di.components.ApplicationComponent
import br.com.aseventos.recon.di.components.DaggerActivityComponent
import br.com.aseventos.recon.di.components.DaggerApplicationComponent
import br.com.aseventos.recon.di.modules.AppModule
import br.com.aseventos.recon.di.modules.DataModule
import br.com.aseventos.recon.di.modules.NetModule
import br.com.aseventos.recon.di.modules.PresenterModule
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.facebook.drawee.backends.pipeline.Fresco
import com.tonyodev.fetch2core.Downloader
import io.fabric.sdk.android.Fabric
import java.io.File
import java.io.FileOutputStream
import io.fabric.sdk.android.services.settings.IconRequest.build
import android.os.StrictMode.setVmPolicy
import android.support.multidex.MultiDex
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.recon.helpers.DemoUtils
import br.com.aseventos.recon.helpers.HashUtils
import br.com.aseventos.recon.ui.activities.DownloadsActivity
import br.com.aseventos.recon.ui.events.OnDownloadProgressUpdate
import br.com.aseventos.recon.workers.DownloadWorker
import com.google.android.gms.analytics.Tracker
import com.google.android.gms.analytics.GoogleAnalytics
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.DownloadBlock
import com.tonyodev.fetch2core.Logger
import com.tonyodev.fetch2okhttp.OkHttpDownloader
import okhttp3.OkHttpClient
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.TimeUnit


class AsReconApplication : MultiDexApplication() {
    companion object {
        @JvmStatic var appGraph : ApplicationComponent? = null
        @JvmStatic var activityGraph : ActivityComponent? = null
        @JvmStatic val SHARED_PREFERENCES_NAME = "AS_RECON_PREFS"
        @JvmStatic val FLAVOR_BUILD_INTERNAL = "internal"
        @JvmStatic val FLAVOR_BUILD_INTERNAL_STORE = "internalStore"
        @JvmStatic val VIMEO_VIDEO_THUMB_FORMAT = "https://i.vimeocdn.com/video/%s_200x150.webp"
        @JvmStatic lateinit var FETCH_INSTANCE : Fetch
        @JvmStatic var isOnBackground = false
        @JvmStatic var tracker : Tracker? = null
        @JvmStatic var gaClient : GoogleAnalytics? = null

        fun isInternalBuild() : Boolean = (BuildConfig.FLAVOR.contentEquals(AsReconApplication.FLAVOR_BUILD_INTERNAL) || BuildConfig.FLAVOR.contentEquals(AsReconApplication.FLAVOR_BUILD_INTERNAL_STORE))

        @JvmStatic fun getSaveDir() : String {
            return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()
        }

        @JvmStatic fun getDemonstrationJsonPath(demoId : Int) : String  =
            "${getSaveDir()}/demo_${HashUtils.sha1(demoId.toString())}_targets.json"



        @JvmStatic fun saveDemonstrationConfigLocal(config : ARConfiguration?,
                                                    demoId: Int,
                                                    onFileSaved: (String) -> Unit) {
            val gson = appGraph?.gson()
            val rawConfig = if(config != null) gson?.toJson(config) else "{\"images\" : []}"
            AsyncTask.execute {
                try {
                    val f = File(getDemonstrationJsonPath(demoId))
                    if (!f.exists()) {
                        f.createNewFile()
                    }

                    val fos = FileOutputStream(f, false)
                    fos.write(rawConfig!!.toByteArray())
                    fos.close()
                    fos.flush()
                    onFileSaved(getDemonstrationJsonPath(demoId))
                }catch (e : Throwable) {

                }
            }
        }

        private val appDatabase by lazy { appGraph?.database() }
        private val appFileLoader by lazy { appGraph?.appFileLoader() }
        val globalFetchListener = object: FetchListener {
            override fun onAdded(download: Download) {

            }

            override fun onCancelled(download: Download) {

            }

            override fun onCompleted(download: Download) {
                AsReconApplication.FETCH_INSTANCE.removeAllWithStatus(Status.COMPLETED)

                val demoId = download.extras.getString(DownloadsActivity.DOWNLOAD_DEMO_ID_EXTRA, "-1").toInt()
                val demo = appDatabase?.demoDao()?.getById(demoId)
                val type = download.extras.getString(DownloadsActivity.DOWNLOAD_TYPE_EXTRA, "")

                if (demo != null) {
                    val videoId = download.extras.getString(DownloadsActivity.DOWNLOAD_VIDEO_ID_EXTRA, "-1").toInt()
                    val video = appDatabase?.mediaDao()?.getById(videoId)

                    if (video != null) {
                        video.localPathUri = DemoUtils.localPathForVideoStorage(demoId, videoId)
                        video.status = DemoEntity.STATUS_STORED
                        appDatabase?.mediaDao()?.update(video)

                        val targets = appDatabase?.targetDao()?.getByMediaId(videoId)

                        targets?.forEach { t ->
                            appFileLoader?.fetchImageAsBitmap(t.fileBlob!!) { btmp ->
                                DownloadWorker.saveTargetImages(
                                        filename = DemoUtils.localPathForTargetStorage(t),
                                        data = btmp
                                ) {}
                            }
                        }
                    }
                    val notStoredMedias = appDatabase?.mediaDao()?.getNotCompletedByDemoId(demoId)
                    if (notStoredMedias != null && notStoredMedias.isEmpty()) {
                        appDatabase?.demoDao()?.setStatusForDemo(DemoEntity.STATUS_STORED, demoId)
                    }
                }
            }

            override fun onDeleted(download: Download) {

            }

            override fun onDownloadBlockUpdated(download: Download, downloadBlock: DownloadBlock, totalBlocks: Int) {

            }

            override fun onError(download: Download, error: Error, throwable: Throwable?) {
                FETCH_INSTANCE.enqueue(download.request)
            }

            override fun onPaused(download: Download) {

            }

            override fun onProgress(download: Download, etaInMilliSeconds: Long, downloadedBytesPerSecond: Long) {
                EventBus.getDefault().post(OnDownloadProgressUpdate(download, download.progress))
            }

            override fun onQueued(download: Download, waitingOnNetwork: Boolean) {

            }

            override fun onRemoved(download: Download) {

            }

            override fun onResumed(download: Download) {

            }

            override fun onStarted(download: Download, downloadBlocks: List<DownloadBlock>, totalBlocks: Int) {

            }

            override fun onWaitingNetwork(download: Download) {

            }

        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        isOnBackground = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        isOnBackground = true
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }


    override fun onCreate() {
        super.onCreate()
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        Fabric.with(this, Crashlytics(), Answers())
        Fresco.initialize(this)

        val fetchOkHttpClient = OkHttpClient.Builder()
                .readTimeout(10L, TimeUnit.MINUTES)
                .writeTimeout(10L, TimeUnit.MINUTES)
                .connectTimeout(10L, TimeUnit.MINUTES)
                .build()

        val fetchConfiguration = FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(1)
                .setHttpDownloader(OkHttpDownloader(fetchOkHttpClient, Downloader.FileDownloaderType.SEQUENTIAL))
                .build()

        FETCH_INSTANCE = Fetch.Impl.getInstance(fetchConfiguration)
        FETCH_INSTANCE.addListener(globalFetchListener)

        appGraph = DaggerApplicationComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule())
                .dataModule(DataModule(this))
                .build()

        activityGraph = DaggerActivityComponent.builder()
                .applicationComponent(appGraph!!)
                .presenterModule(PresenterModule())
                .build()
    }


}