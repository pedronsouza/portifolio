package br.com.aseventos.recon.ui.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.v2.AppUser
import br.com.aseventos.recon.R
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.ui.adapters.GeneralSearchAdapter
import br.com.aseventos.recon.ui.presenters.SearchUsersPresenter
import br.com.aseventos.recon.ui.view.SearchUserView
import br.com.aseventos.recon.ui.view.TrackableView
import com.crashlytics.android.Crashlytics
import javax.inject.Inject

class SearchUsersActivity : DrawerActivity(), SearchUserView, TrackableView {
    override fun layoutResourceId(): Int = R.layout.activity_search_users
    override fun onInject(graph: ActivityComponent?) { graph?.inject(this) }
    override fun getToolbarTitle(): String = "Busca"
    override fun getToolbarNavigationIcon(): Int = R.drawable.ic_drawer_icon_closed

    private val searchField         by lazy { findViewById<EditText>(R.id.search_field_text_input) }
    private val progress            by lazy { findViewById<ViewGroup>(R.id.default_progress_content) }
    private val searchButton        by lazy { findViewById<TextView>(R.id.search_button) }
    private val mainMsg             by lazy { findViewById<TextView>(R.id.search_main_message) }
    private val searchReycler       by lazy { findViewById<RecyclerView>(R.id.search_recycler) }
    private val emptyState          by lazy { findViewById<ViewGroup>(R.id.frame_empty_state) }

    var currentSet = emptyList<SearchResult>()

    @Inject lateinit var presenter : SearchUsersPresenter
    @Inject lateinit var appRouter : AppRouter

    override fun getTrackPageName(): TrackerAdapter.PageName = TrackerAdapter.PageName.SEARCH_ITEMS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchButton.setOnClickListener {
            appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                    TrackerAdapter.EventName.USER_SEARCHED,
                    searchField.text.toString())

            presenter.searchUsersAndDirectory(searchField.text.toString(), this@SearchUsersActivity)
        }

        searchReycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        showSoftKeyboard(searchField)
    }

    override fun onSearchResultsRetrieved(itens: List<SearchResult>) {
        hideProgress()
        currentSet = itens
        searchReycler.adapter = GeneralSearchAdapter(itens, this)
        emptyState.visibility = View.INVISIBLE
    }

    override fun onSearchResultEmpty() {
        currentSet = emptyList()
        searchReycler.adapter = null
        emptyState.visibility = View.VISIBLE
        mainMsg.text = getString(R.string.empty_search_msg, searchField.text.toString())
    }

    override fun onSearchResultError(e: Throwable) {
        hideProgress()
        onSearchResultEmpty()
        Crashlytics.logException(e)
    }

    override fun showProgress() =
            progress.setVisibility(View.VISIBLE)

    override fun hideProgress() =
            progress.setVisibility(View.INVISIBLE)

    override fun onUnsubscribe() =
            presenter.unsubscribe()

    override fun onSearchItemSelected(item: SearchResult) {
        appRouter.toSearchItemDetail(this, item)
    }
}