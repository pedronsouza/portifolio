package br.com.aseventos.recon.ui.view

import br.com.aseventos.domain.adapters.TrackerAdapter

interface TrackableView {
    fun getTrackPageName() : TrackerAdapter.PageName
}