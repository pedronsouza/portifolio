package br.com.aseventos.recon.ui.presenters

import br.com.aseventos.data.usecases.session.CheckUserEmailOnServerUseCase
import br.com.aseventos.domain.models.Session
import br.com.aseventos.recon.helpers.BaseSubscriber
import br.com.aseventos.recon.ui.view.SearchItemDetailView
import br.com.aseventos.recon.ui.viewmodels.SearchItemViewModel
import javax.inject.Inject

class SearchItemDetailPresenter : BasePresenter {
    private var checkUserEmailOnServerUseCase: CheckUserEmailOnServerUseCase

    @Inject constructor(checkUserEmailOnServerUseCase: CheckUserEmailOnServerUseCase) {
        this.checkUserEmailOnServerUseCase = checkUserEmailOnServerUseCase
    }

    fun enableTemporaryAcessForUser(searchItem : SearchItemViewModel, view: SearchItemDetailView) {
        this.checkUserEmailOnServerUseCase.email = searchItem.email
        this.checkUserEmailOnServerUseCase.execute(CheckUserEmailSubscriber(view))
    }

    override fun unsubscribe() {
        checkUserEmailOnServerUseCase.unsubscribe()
    }

    private inner class CheckUserEmailSubscriber(val view : SearchItemDetailView) : BaseSubscriber<Session>() {
        override fun onNext(item: Session) {
            super.onNext(item)
            view.onTemporaryAccessCreated(item)
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            view.onTemporaryAccessCreationError(e)
        }
    }
}