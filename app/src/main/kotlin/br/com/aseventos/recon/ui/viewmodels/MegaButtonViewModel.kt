package br.com.aseventos.recon.ui.viewmodels

data class MegaButtonViewModel(val resId : Int, val title : String, val onMegaButtonClicked : () -> Unit)