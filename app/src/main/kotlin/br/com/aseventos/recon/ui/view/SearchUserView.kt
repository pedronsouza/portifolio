package br.com.aseventos.recon.ui.view

import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.v2.AppUser

interface SearchUserView {
    fun onSearchResultsRetrieved(itens : List<SearchResult>)
    fun onSearchResultEmpty()
    fun onSearchResultError(e : Throwable)
    fun onSearchItemSelected(item : SearchResult)
    fun showProgress()
    fun hideProgress()
}