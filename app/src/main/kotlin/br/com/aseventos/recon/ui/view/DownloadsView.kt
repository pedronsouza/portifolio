package br.com.aseventos.recon.ui.view

import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.ui.viewmodels.DownloadItemViewModel

interface DownloadsView {
    fun onMediaItemClicked(item : DownloadItemViewModel)
    fun getFileLoader(): FileLoader
}