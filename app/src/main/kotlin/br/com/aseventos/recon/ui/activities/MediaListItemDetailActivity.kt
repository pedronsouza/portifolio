package br.com.aseventos.recon.ui.activities

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.R
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.services.DownloadService
import br.com.aseventos.recon.ui.adapters.VideoPhotosAdapter
import br.com.aseventos.recon.ui.events.OnDataEnqueuedEvent
import br.com.aseventos.recon.ui.view.TrackableView
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import br.com.aseventos.recon.workers.DownloadWorker
import br.com.asrecon.recon.v2.HelloAR
import com.facebook.drawee.view.SimpleDraweeView
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.net.URL
import javax.inject.Inject

class MediaListItemDetailActivity : ToolbarActivity(), TrackableView {
    override fun getToolbarTitle(): String = ""
    override fun getToolbarNavigationIcon(): Int = R.drawable.ic_back_arrow_white
    override fun layoutResourceId(): Int = R.layout.activity_media_list_item_detail
    override fun onInject(graph: ActivityComponent?) = graph?.inject(this)!!


    companion object {
        val MEDIA_LIST_ITEM_FLAG = "MEDIA_LIST_ITEM"
        val MEDIA_LIST_PHOTOS_FLAG = "MEDIA_LIST_PHOTOS"

    }

    private val mediaListItem by lazy { intent.getParcelableExtra<MediaListItemViewModel>(MEDIA_LIST_ITEM_FLAG) }
    private val customTargetImages     by lazy { intent.getStringArrayListExtra(MEDIA_LIST_PHOTOS_FLAG) }

    private val videoPreview by lazy { findViewById<SimpleDraweeView>(R.id.video_view_preview_local) }
    private val mainButton   by lazy { findViewById<TextView>(R.id.main_button) }
    private val playerButton by lazy { findViewById<ImageView>(R.id.play_video_button) }
    private val recycler     by lazy { findViewById<RecyclerView>(R.id.video_photos_recycler) }

    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var appFileLoader : FileLoader

    lateinit var progresDialog : ProgressDialog

    override fun getTrackPageName(): TrackerAdapter.PageName = TrackerAdapter.PageName.RECON_DETAIL

    override fun onStart() {
        super.onStart()
    }

    override fun onUnsubscribe() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        recycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        toolbar?.setNavigationOnClickListener { onBackPressed() }

        val targets = appDatabase?.targetDao()?.getByMediaId(mediaListItem.id)
        val video = appDatabase?.mediaDao()?.getById(mediaListItem.id)
        val demonstration = appDatabase?.demoDao()?.getById(mediaListItem.demoId)

        if (targets != null && targets.isNotEmpty()) {
            recycler.adapter = VideoPhotosAdapter(targets, appFileLoader)
            appFileLoader.createTargetImageRequestForTargetId(targets.first().id) { controller ->
                videoPreview.controller = controller
            }
        }

        playerButton.setOnClickListener {
            appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                    TrackerAdapter.EventName.VIDEO_PLAY,
                    null)

            if(video != null ) {
                if (video.localPathUri != null) {
                    appRouter.toFullScreenPlayer(this, video.localPathUri!!)
                } else {
                    progresDialog = ProgressDialog.show(this, "Aguarde", "Carregando arquivo", true)
                    AsyncTask.execute {
                        val url = HelloAR.getFinalURL(URL(video.sourceUri)).toString()
                        runOnUiThread { progresDialog.dismiss() }
                        appRouter.toFullScreenPlayer(this, url)
                    }
                }
            }

        }

        if (video?.status?.contentEquals(DemoEntity.STATUS_STORED) ?: false) {
            mainButton.text = "Download Concluido"
            mainButton.setOnClickListener {  }
        } else {
            mainButton.text = "Iniciar Download"
            mainButton.setOnClickListener {
                appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                        TrackerAdapter.EventName.NEW_DEMO_SYNC,
                        null)

                if (demonstration != null) {
                    val downloadWorker = OneTimeWorkRequest.Builder(DownloadWorker::class.java)
                    val data = Data.Builder()
                    data.putInt(DownloadService.DEMO_ITEM_FLAG, demonstration.id)
                    downloadWorker.setInputData(data.build())
                    WorkManager.getInstance().enqueue(downloadWorker.build())
                    progresDialog = ProgressDialog.show(this, "Aguarde", "Preparando arquivos para download", true)
                }
            }
        }


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDataEnqueuedEvent(event : OnDataEnqueuedEvent) {
        progresDialog.dismiss()
        AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage("Download iniciado com sucesso")
                .setPositiveButton("Acompanhar Progresso") { _: DialogInterface, _: Int ->
                    appRouter.toDownloads(this, null)
                    finish()
                }.show()
    }
}