package br.com.aseventos.recon.ui.viewmodels

import android.os.Parcel
import android.os.Parcelable
import br.com.aseventos.domain.models.SearchResult

data class SearchItemViewModel(val id : String,
                               val title : String,
                               val description : String?,
                               val imgUrl : String?,
                               var email : String?,
                               var temporaryAccessEnabled : Boolean,
                               val type : SearchResult.Type) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt() == 1,
            parcel.readSerializable() as SearchResult.Type) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(imgUrl)
        parcel.writeString(email)
        parcel.writeInt(if (temporaryAccessEnabled) 1 else 0)
        parcel.writeSerializable(type)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchItemViewModel> {
        override fun createFromParcel(parcel: Parcel): SearchItemViewModel {
            return SearchItemViewModel(parcel)
        }

        override fun newArray(size: Int): Array<SearchItemViewModel?> {
            return arrayOfNulls(size)
        }
    }
}