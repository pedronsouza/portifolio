package br.com.aseventos.recon.di.modules

import android.content.SharedPreferences
import br.com.aseventos.data.net.HttpServices
import br.com.aseventos.data.net.v2.APIV2Services
import br.com.aseventos.data.utils.EnumRetrofitConverterFactory
import br.com.aseventos.data.utils.GsonFactory
import br.com.aseventos.domain.models.Session
import br.com.aseventos.recon.BuildConfig
import br.com.aseventos.recon.helpers.SessionManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetModule {
    @Provides
    @Singleton
    fun providesRetrofit(httpClient: OkHttpClient, gson : Gson) : Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(EnumRetrofitConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun providesHttpClient(sessionManager: SessionManager) : OkHttpClient {
        val logging = HttpLoggingInterceptor()
        if(BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }

        val httpClient = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor({
                    val request = it.request()
                            .newBuilder()
                            .addHeader("User-Agent", "Android|${BuildConfig.VERSION_NAME}|${BuildConfig.VERSION_CODE}")
                            .addHeader("Accept-Language", Locale.getDefault().language.toLowerCase())


                    val token = sessionManager.getNetToken()
                    if (token != null) {
                        request.addHeader("Authorization", "Bearer ${token}")
                    } else {
                        request.addHeader("Authorization", "Bearer ${BuildConfig.CLIENT_TOKEN}")
                    }

                    it.proceed(request.build())
                })
                .addInterceptor(logging)
                .build()

        return httpClient
    }

    @Provides @Singleton fun providesHttpServices(retrofit: Retrofit) : HttpServices = HttpServices(retrofit)
    @Provides @Singleton fun providesAPIV2Services(retrofit: Retrofit) : APIV2Services = APIV2Services(retrofit)
}