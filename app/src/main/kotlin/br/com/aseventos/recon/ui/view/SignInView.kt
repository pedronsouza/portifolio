package br.com.aseventos.recon.ui.view

import br.com.aseventos.domain.models.User

interface SignInView : ProgressibleView {
    fun onAuthenticationSuccessfully(user : User)
    fun onAuthenticationError(e: Throwable)
}