package br.com.aseventos.recon.di.modules


import android.content.Context
import br.com.aseventos.data.usecases.demonstrations.FetchDemonstrationBySearchItemIdUseCase
import br.com.aseventos.data.datasources.media.RemoteMediaDataSource
import br.com.aseventos.data.datasources.user.RemoteUserDataSource
import br.com.aseventos.data.local.DemonstrationManager
import br.com.aseventos.data.net.HttpServices
import br.com.aseventos.data.repositories.MediaRepository
import br.com.aseventos.data.repositories.UserRepository
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.data.net.v2.APIV2Services
import br.com.aseventos.data.usecases.demonstrations.CreateNewDemonstrationUseCase
import br.com.aseventos.data.usecases.demonstrations.FetchAllDemonstrationsUseCase
import br.com.aseventos.data.usecases.demonstrations.FetchStoredDemonstrationsUseCase
import br.com.aseventos.data.usecases.media.FetchDirectoryItemsUseCase
import br.com.aseventos.data.usecases.media.FetchUserItemsUseCase
import br.com.aseventos.data.usecases.session.AuthenticateUserUseCase
import br.com.aseventos.data.usecases.session.CreatePasswordUserCase
import br.com.aseventos.data.usecases.session.GenerateAdminTokenUseCase
import br.com.aseventos.data.usecases.session.RequestChangePasswordTokenUseCase
import br.com.aseventos.data.usecases.users.GeneralSearchUseCase
import br.com.aseventos.data.usecases.users.UploadCustomTargetImageUseCase
import br.com.aseventos.data.usecases.users.UploadCustomTargetVideoUseCase
import br.com.aseventos.data.usecases.v2.*
import br.com.aseventos.domain.datasources.media.MediaDataSource
import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import dagger.Module
import dagger.Provides

@Module(includes = arrayOf(NetModule::class, AppModule::class))
class DataModule(val context: Context) {
    @Provides fun providesContext() : Context = context

    @Provides
    fun provideLocalDatabase() : ReconDatabase? = ReconDatabase.getInstance(context)

    @Provides
    fun providesAuthenticateUserUseCase(threadExecutor: ThreadExecutor,
                                        postExecutionThread: PostExecutionThread,
                                        userRepository: UserRepository) : AuthenticateUserUseCase
        = AuthenticateUserUseCase(threadExecutor, postExecutionThread, userRepository)

    @Provides
    fun providesCreatePasswordUserCase(threadExecutor: ThreadExecutor,
                                       postExecutionThread: PostExecutionThread,
                                       userRepository: UserRepository) : CreatePasswordUserCase = CreatePasswordUserCase(userRepository,
                                                                                                                            threadExecutor,
                                                                                                                            postExecutionThread)

    @Provides fun providesGeneralSearchUseCase(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               userRepository: UserRepository) : GeneralSearchUseCase = GeneralSearchUseCase(threadExecutor,
                                                                                                                                postExecutionThread,
                                                                                                                                userRepository)

    @Provides fun providesUploadCustomTargetVideoUseCase(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               userRepository: UserRepository) : UploadCustomTargetVideoUseCase = UploadCustomTargetVideoUseCase(threadExecutor,
                                                                                                                                postExecutionThread,
                                                                                                                                userRepository)

    @Provides fun providesUploadCustomTargetImageUseCase(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               userRepository: UserRepository) : UploadCustomTargetImageUseCase = UploadCustomTargetImageUseCase(threadExecutor,
                                                                                                                                postExecutionThread,
                                                                                                                                userRepository)
    @Provides fun providesRequestChangePasswordTokenUseCase(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               userRepository: UserRepository) : RequestChangePasswordTokenUseCase = RequestChangePasswordTokenUseCase(threadExecutor,
                                                                                                                                postExecutionThread,
                                                                                                                                userRepository)

    @Provides fun providesGenerateAdminTokenUseCase(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               userRepository: UserRepository) : GenerateAdminTokenUseCase = GenerateAdminTokenUseCase(threadExecutor,
            postExecutionThread,
            userRepository)

    @Provides fun providesFetchDirectoryItemsUseCase(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               mediaRepository: MediaRepository) : FetchDirectoryItemsUseCase = FetchDirectoryItemsUseCase(threadExecutor,
                                                                                                                                            postExecutionThread,
                                                                                                                                            mediaRepository)
    @Provides fun providesFetchUserItemsUseCase(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               mediaRepository: MediaRepository) : FetchUserItemsUseCase = FetchUserItemsUseCase(threadExecutor,
                                                                                                                                            postExecutionThread,
                                                                                                                                            mediaRepository)

    @Provides
    fun providesUserDataSource(httpServices: HttpServices, apiV2Services: APIV2Services) : UserDataSource = RemoteUserDataSource(httpServices, apiV2Services)
    @Provides
    fun providesUserRepository(userDataSource: UserDataSource) : UserRepository = UserRepository(userDataSource)

    @Provides
    fun providesMediaDataSource(httpServices: HttpServices) : MediaDataSource = RemoteMediaDataSource(httpServices)

    @Provides
    fun providesMediaRepository(mediaDataSource: MediaDataSource) : MediaRepository = MediaRepository(mediaDataSource)


    @Provides
    fun providesFetchDemonstrationBySearchItemIdUseCase(threadExecutor: ThreadExecutor,
                                                        postExecutionThread: PostExecutionThread,
                                                        appDatabase: ReconDatabase?) : FetchDemonstrationBySearchItemIdUseCase = FetchDemonstrationBySearchItemIdUseCase(threadExecutor,
                                                                                                                                                                            postExecutionThread,
                                                                                                                                                                            appDatabase)
    @Provides
    fun providesCreateNewDemonstrationUseCase(threadExecutor: ThreadExecutor,
                                              postExecutionThread: PostExecutionThread,
                                              appDatabase: ReconDatabase?) : CreateNewDemonstrationUseCase = CreateNewDemonstrationUseCase(threadExecutor, postExecutionThread, appDatabase)

    @Provides
    fun providesFechStoredDemonstrationsUseCase(threadExecutor: ThreadExecutor,
                                              postExecutionThread: PostExecutionThread,
                                              appDatabase: ReconDatabase?) : FetchStoredDemonstrationsUseCase = FetchStoredDemonstrationsUseCase(threadExecutor, postExecutionThread, appDatabase)
    @Provides
    fun providesFetchAllDemonstrationsUseCase(threadExecutor: ThreadExecutor,
                                              postExecutionThread: PostExecutionThread,
                                              appDatabase: ReconDatabase?) : FetchAllDemonstrationsUseCase = FetchAllDemonstrationsUseCase(threadExecutor, postExecutionThread, appDatabase)

    @Provides
    fun providesValidateUserEmailCase(threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread,
                              userDataSource: UserDataSource) : ValidateUserEmailCase = ValidateUserEmailCase(threadExecutor, postExecutionThread, userDataSource)
    @Provides
    fun providesSaveUserPasswordCase(threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread,
                              userDataSource: UserDataSource) : SaveUserPasswordCase = SaveUserPasswordCase(threadExecutor, postExecutionThread, userDataSource)
    @Provides
    fun providesAuthorizeUserCase(threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread,
                              userDataSource: UserDataSource) : AuthorizeUserCase = AuthorizeUserCase(threadExecutor, postExecutionThread, userDataSource)
    @Provides
    fun providesFindUserFilesCase(threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread,
                              userDataSource: UserDataSource) : FindUserFilesCase = FindUserFilesCase(threadExecutor, postExecutionThread, userDataSource)

    @Provides
    fun providesFindUserInRemoteCase(threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread,
                              userDataSource: UserDataSource) : FindUserInRemoteCase = FindUserInRemoteCase(threadExecutor, postExecutionThread, userDataSource)
}
