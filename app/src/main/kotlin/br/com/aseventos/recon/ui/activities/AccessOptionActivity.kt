package br.com.aseventos.recon.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.SessionManager
import javax.inject.Inject

class AccessOptionActivity : AppCompatActivity() {

    private val accessOptionGuestBt by lazy { findViewById(R.id.access_option_guest) as TextView? }
    private val accessOptionUserBt by lazy { findViewById(R.id.access_option_user) as TextView? }

    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var sessionManager : SessionManager
    @Inject lateinit var appTracker : TrackerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access_option)
        activityGraph?.inject(this)
        appTracker.trackPageView(TrackerAdapter.PageName.CHOOSE_SIGN_OPTION)
    }

    fun checkPermissionForGuestAndRedirect() {
        sessionManager.createNewSessionForGuestUser()
        appRouter.toCameraActionActivity(this@AccessOptionActivity)
        finish()
    }

    override fun onResume() {
        super.onResume()
        accessOptionGuestBt?.setOnClickListener {
            checkPermissionForGuestAndRedirect()
            appTracker.trackEvent(
                    TrackerAdapter.EventCategory.ACTION,
                    TrackerAdapter.EventName.ANON_SIGN_IN,
                    null)
        }

        accessOptionUserBt?.setOnClickListener {
            appRouter.toAuthenticateUserActivity(this@AccessOptionActivity)
            appTracker.trackEvent(
                    TrackerAdapter.EventCategory.NAVIGATION,
                    TrackerAdapter.EventName.SIGN_IN,
                    null
            )
        }
    }
}
