package br.com.aseventos.recon.ui.view

import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.v2.AppMedia

interface SplashView {
    fun onAdminTokenGenerated(session : Session)
    fun onAdminTokenGenerationError(e : Throwable)
    fun onFilesFoundedForDemo(demo: DemoEntity, files: List<AppMedia>)
    fun onDemoHasNoFiles(demo : DemoEntity)
}