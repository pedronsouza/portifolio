package br.com.aseventos.recon.helpers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.AsReconApplication.Companion.isInternalBuild
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.activities.*
import br.com.aseventos.recon.ui.fragments.VideoPlayerFullScreenFragment
import br.com.aseventos.recon.ui.viewmodels.DownloadableItemViewModel
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import br.com.aseventos.recon.ui.viewmodels.SearchItemViewModel
import javax.inject.Inject


class AppRouter {

    @Inject constructor()
    private fun buildSimpleIntent(ctx : AppCompatActivity, clazz : Class<*>) =
            Intent(ctx, clazz)

    fun toCameraActionActivity(ctx : AppCompatActivity,
                               jsonPath : String? = null) {
        val intent = buildSimpleIntent(ctx, HomeActivity::class.java)
        intent.putExtra(HomeActivity.OFFLINE_MODE_KEY, jsonPath != null)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

        if (jsonPath != null)
            intent.putExtra(HomeActivity.OFFLINE_MODE_JSONPATH_KEY, jsonPath)
        else if (appGraph?.sessionManager()?.isGuestUser() == true) {
            intent.putExtra(HomeActivity.OFFLINE_MODE_KEY, false)
            ctx.startActivity(intent)
            return
        } else if (!isInternalBuild()) {
            val demoId = appGraph?.sessionManager()?.session?.user?.id ?: ""
            val demo = appGraph?.database()?.demoDao()?.getByBaseSystemId(demoId)
            if (demo != null && demo.baseSystemId.contentEquals(appGraph?.sessionManager()?.session?.user?.id ?: "")) {
                Handler().postDelayed({
                    appGraph?.appFileLoader()?.createEasyArConfigurationFileFromDemonstration(demo) {
                        intent.putExtra(HomeActivity.OFFLINE_MODE_JSONPATH_KEY, it)
                        intent.putExtra(HomeActivity.OFFLINE_MODE_KEY, true)

                        ctx.startActivity(intent)
                    }
                }, 200L)

                return
            } else {
                val defaultUri = "sample_db.json"
                intent.putExtra(HomeActivity.OFFLINE_MODE_JSONPATH_KEY, defaultUri)
                intent.putExtra(HomeActivity.OFFLINE_MODE_KEY, true)
                ctx.startActivity(intent)
            }

        }

        ctx.finish()
        ctx.startActivity(intent)

    }


    fun toAuthenticateUserActivity(ctx: AppCompatActivity) {
        ctx.startActivity(buildSimpleIntent(ctx, MainActivity::class.java))
        ctx.finish()
    }


    fun toAccessOptionsActivity(ctx: AppCompatActivity) =
            ctx.startActivity(buildSimpleIntent(ctx, AccessOptionActivity::class.java))

    fun toProfileActivity(ctx: AppCompatActivity) =
            ctx.startActivity(buildSimpleIntent(ctx, ProfileActivity::class.java))

    fun toNewUserActivity(ctx: AppCompatActivity, startWithCreation : Boolean) {
        val intent = buildSimpleIntent(ctx, NewUserActivity::class.java)
        intent.putExtra(NewUserActivity.NEW_USER_START_FROM_PASSWORD_KEY, startWithCreation)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        ctx.finish()
        ctx.startActivity(intent)

    }

    fun toFullScreenPlayer(ctx: AppCompatActivity, url : String, seekTo: Int = -1) {
        val intent = buildSimpleIntent(ctx, FullScreenVideoActivity::class.java)
        intent.putExtra(VideoPlayerFullScreenFragment.VIDEO_PLAYER_URL_FLAG, url)
        intent.putExtra(VideoPlayerFullScreenFragment.VIDEO_PLAYER_SEEK_POSITION_FLAG, seekTo)
        ctx.startActivity(intent)
    }

    fun toSearchUsersAction(activity: AppCompatActivity) {
        val intent = buildSimpleIntent(activity, SearchUsersActivity::class.java)
        activity.startActivity(intent)
    }

    fun toUserMediaAction(ctx: AppCompatActivity) {
        val intent = buildSimpleIntent(ctx, UserMediaActivity::class.java)
        ctx.startActivity(intent)
    }

    fun toSearchItemDetail(context: Context, searchResult: SearchResult) {
        val intent = buildSimpleIntent(context as AppCompatActivity, SearchItemDetailActivity::class.java)
        intent.putExtra(SearchItemDetailActivity.INTENT_USER_PROFILE_ITEM_FLAG,
                SearchItemViewModel(searchResult.id,
                        searchResult.title,
                        searchResult.description,
                        searchResult.imgUrl,
                        searchResult.meta?.email,
                        searchResult.meta?.haveAccess ?: false,
                        searchResult.type))

        context.startActivity(intent)
    }

    fun toMediaList(context: Context, searchItem: SearchItemViewModel) {
        val intent = buildSimpleIntent(context as AppCompatActivity, MediaListActivity::class.java)
        intent.putExtra(MediaListActivity.MEDIA_LIST_SEARCH_ITEM_FLAG, searchItem)
        context.startActivity(intent)
    }

    fun toMediaList(context : Context, demonstrationId : Int) {
        val intent = buildSimpleIntent(context as AppCompatActivity, MediaListActivity::class.java)
        intent.putExtra(MediaListActivity.MEDIA_LIST_DEMONSTRATION_ID_FLAG, demonstrationId)
        context.startActivity(intent)
    }

    fun toDownloads(context : Context, items: List<DownloadableItemViewModel>?) {
        val intent = buildSimpleIntent(context as AppCompatActivity, DownloadsActivity::class.java)
        context.startActivity(intent)
    }

    fun toDemonstration(context: Context) {
        val intent = buildSimpleIntent(context as AppCompatActivity, DemonstrationsActivity::class.java)
        context.startActivity(intent)
    }

    fun toMediaItemDetail(context: Context,
                          item: MediaListItemViewModel,
                          targetsImgs : List<String> = emptyList()) {
        if (context is AppCompatActivity) {
            val intent = buildSimpleIntent(context, MediaListItemDetailActivity::class.java)
            intent.putExtra(MediaListItemDetailActivity.MEDIA_LIST_ITEM_FLAG, item)

            if (targetsImgs.isNotEmpty()) {
                intent.putStringArrayListExtra(MediaListItemDetailActivity.MEDIA_LIST_PHOTOS_FLAG, ArrayList(targetsImgs))
            }

            context.startActivity(intent)
        }
    }

    fun toNativeShare(context : Context, uri : Uri) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.setType("image/jpeg")
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        context.startActivity(Intent.createChooser(intent, "Compartilhar a imagem"))
    }

    fun toShowOnGallery(context: Context, imgUrl : String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(Uri.parse(imgUrl), "image/jpeg")
        context.startActivity(intent)
    }

    fun resetApp(context : Context) {
        val mStartActivity = Intent(context, SplashActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId,  mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT)
        val mgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?

        if (mgr != null) {
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
            System.exit(0)
        }
    }
}

