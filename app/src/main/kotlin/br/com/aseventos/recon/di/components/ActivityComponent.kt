package br.com.aseventos.recon.di.components

import br.com.aseventos.domain.di.ActivityScope
import br.com.aseventos.recon.di.modules.PresenterModule
import br.com.aseventos.recon.ui.activities.*
import br.com.aseventos.recon.ui.fragments.MediaListFragment
import dagger.Component


@ActivityScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(PresenterModule::class))
interface ActivityComponent : ApplicationComponent {
    fun inject(activity : MainActivity)
    fun inject(activity : AccessOptionActivity)
    fun inject(activity : NewUserActivity)
    fun inject(activity : SplashActivity)
    fun inject(activity : HomeActivity)
    fun inject(activity : FullScreenVideoActivity)
    fun inject(activity : ProfileActivity)
    fun inject(activity : SearchUsersActivity)
    fun inject(activity : BaseActivity)
    fun inject(activity : SearchItemDetailActivity)
    fun inject(activity : MediaListActivity)
    fun inject(activity : DownloadsActivity)
    fun inject(activity : DemonstrationsActivity)
    fun inject(activity : MediaListItemDetailActivity)

}