package br.com.aseventos.recon.ui.activities

import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import br.com.aseventos.recon.R

abstract class ToolbarActivity : BaseActivity() {
    protected val toolbar by lazy { findViewById<Toolbar>(R.id.main_toolbar) }


    abstract fun getToolbarTitle() : String
    abstract fun getToolbarNavigationIcon() : Int


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        title = getToolbarTitle()
        toolbar.title = getToolbarTitle()
        toolbar?.setNavigationIcon(getToolbarNavigationIcon())
    }
}