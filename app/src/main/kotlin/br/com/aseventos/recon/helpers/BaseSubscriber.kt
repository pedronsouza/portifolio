package br.com.aseventos.recon.helpers

import android.util.Log
import br.com.aseventos.recon.BuildConfig
import com.crashlytics.android.Crashlytics
import rx.Subscriber

abstract class BaseSubscriber<T> : Subscriber<T>() {
    override fun onCompleted() {
        if (BuildConfig.DEBUG) {
            Log.d(this::class.java.simpleName, "onComplete Invoked")
        }
    }

    override fun onNext(item: T) {
        if (BuildConfig.DEBUG) {
            Log.d(this::class.java.simpleName, "onNext Invoked")
        }
    }

    override fun onError(e: Throwable) {
        if (BuildConfig.DEBUG) {
            Log.e(this::class.java.simpleName, Log.getStackTraceString(e))
        }
        Crashlytics.logException(e)
    }
}