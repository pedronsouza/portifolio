package br.com.aseventos.recon.ui.adapters

import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.view.DemonstrationView
import br.com.aseventos.recon.ui.viewmodels.DemoViewModel
import com.facebook.drawee.view.SimpleDraweeView

class DemonstrationAdapter(val items : MutableList<DemoViewModel>, val view : DemonstrationView) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val IDLE_VIEWTYPE = 0
    private val STORED_VIEWTYPE = 1
    private val DOWNLOADING_VIEWTYPE = 2

    override fun getItemCount(): Int = items.size
    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        var out = -1
        if(item.status.contentEquals(DemoEntity.STATUS_IDLE)) {
            out = IDLE_VIEWTYPE
        } else if (item.status.contentEquals(DemoEntity.STATUS_DOWNLOADING)) {
            val videos = appGraph?.database()?.mediaDao()?.getByDemoId(item.id);
            val stored = videos?.filter { it.status.contentEquals(DemoEntity.STATUS_STORED) } ?: emptyList()
            if (stored.isNotEmpty()) {
                out = STORED_VIEWTYPE
            } else {
                out = DOWNLOADING_VIEWTYPE
            }
        } else {
            out = STORED_VIEWTYPE
        }

        return out
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_media_list_item_video, parent, false)
        return when(viewType) {
            IDLE_VIEWTYPE -> IdleDemonstrationViewHolder(v)
            STORED_VIEWTYPE -> StoredDemonstrationViewHolder(v)
            else -> InProgressDemonstrationViewHolder(v)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is BaseDemonstrationViewHolder) {
            holder.update(items[position])
        }
    }

    fun remove(demoId : Int) {
        val item = items.first { it.id == demoId }
        remove(item)
    }

    fun remove(item : DemoViewModel) {
        val indexOf = items.indexOf(item)
        if (indexOf != -1) {
            items.remove(item)
            notifyItemRemoved(indexOf)
        }
    }

    abstract inner class BaseDemonstrationViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        protected val itemIcon by lazy { itemView.findViewById<ImageView>(R.id.item_icon_overlay) }
        protected val itemImage by lazy { itemView.findViewById<SimpleDraweeView>(R.id.item_icon) }
        protected val itemName by lazy { itemView.findViewById<TextView>(R.id.item_name) }
        private val iconOverlay by lazy { itemView.findViewById<ImageView>(R.id.item_icon_overlay) }
        protected val button  by lazy { itemView.findViewById<TextView>(R.id.item_button) }

        open fun update(item : DemoViewModel) {
            itemView.setOnClickListener { view.onShowDemonstrationContent(item.id) }
            itemName.text = item.name
            itemView.alpha = 1F

            itemView.setOnLongClickListener {
                AlertDialog.Builder(itemView.context)
                        .setTitle("Alerta")
                        .setMessage("Você deseja realmente remover está demonstração?")
                        .setNegativeButton("Cancelar", null)
                        .setPositiveButton("OK, Entendi") { dialog, which ->
                            view.onDemonstrationRemoved(item.id)
                        }.show()

                return@setOnLongClickListener true
            }

            view.getFileLoader().createTargetImageRequestForTargetId(item.firstTargetId) { controller ->
                itemImage.controller = controller
                if (item.status.contentEquals(DemoEntity.STATUS_STORED)) {
                    iconOverlay.visibility = View.VISIBLE
                } else {
                    iconOverlay.visibility = View.INVISIBLE
                }
            }
        }
    }

    inner class StoredDemonstrationViewHolder(itemView : View) : BaseDemonstrationViewHolder(itemView) {
        override fun update(item: DemoViewModel) {
            super.update(item)
            button.text = "Carregar\nApresentação"
            button.setOnClickListener { view.onLoadDemonstration(item.id) }

        }
    }

    inner class InProgressDemonstrationViewHolder(itemView: View) : BaseDemonstrationViewHolder(itemView) {
        override fun update(item: DemoViewModel) {
            super.update(item)
            button.text = "Acompanhar\ndownload"
            itemView.alpha = 0.5F
        }
    }

    inner class IdleDemonstrationViewHolder(itemView: View) : BaseDemonstrationViewHolder(itemView) {
        override fun update(item: DemoViewModel) {
            super.update(item)
            button.text = "Iniciar\nDownload"
        }
    }
}