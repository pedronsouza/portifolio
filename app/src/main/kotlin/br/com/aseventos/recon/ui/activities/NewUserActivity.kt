package br.com.aseventos.recon.ui.activities

import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.CardView
import android.view.View
import android.widget.TextView
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.domain.execptions.validations.BadEmailFormatException
import br.com.aseventos.domain.execptions.validations.InvalidEmalException
import br.com.aseventos.domain.execptions.validations.PasswordNotMatchException
import br.com.aseventos.domain.execptions.validations.TemporaryAcesssException
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.models.v2.AppCreationToken
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.presenters.NewUserPresenter
import br.com.aseventos.recon.ui.view.NewUserView
import br.com.aseventos.recon.ui.view.TrackableView
import java.lang.ref.WeakReference
import javax.inject.Inject

class NewUserActivity : ProgressibleActivity(), NewUserView {
    companion object {
        val NEW_USER_START_FROM_PASSWORD_KEY = "NEW_USER_START_FROM_PASSWORD"
    }
    private val emailField by lazy { findViewById(R.id.sign_up_email) as TextInputEditText? }
    private val emailInpLayout by lazy { findViewById(R.id.email_input_layout) as TextInputLayout? }
    private val signUpBt by lazy { findViewById(R.id.sign_up_bt) as TextView? }
    private val signInBt by lazy { findViewById(R.id.sign_in_bt) as TextView? }
    private val mainCardView by lazy { findViewById(R.id.main_card_view) as CardView? }
    private val pwdCardView by lazy { findViewById(R.id.pwd_creation_card_view) as CardView? }
    private val pwdBt by lazy { findViewById(R.id.sign_up_pwd_bt) as TextView? }

    private val passwordField by lazy { findViewById(R.id.sign_up_pwd) as TextInputEditText? }
    private val confirmField by lazy { findViewById(R.id.sign_up_confirm_pwd) as TextInputEditText? }

    private val startWithCreation by lazy { intent?.extras?.getBoolean(NEW_USER_START_FROM_PASSWORD_KEY) ?: false }

    @Inject lateinit var presenter : NewUserPresenter
    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var sessionManager : SessionManager
    @Inject lateinit var appTracker : TrackerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)
        activityGraph?.inject(this)
        if (startWithCreation) {
            setViewMode(Mode.PWD_CREATION)
        } else {
            setViewMode(Mode.EMAIL_CHECK)
        }


    }

    enum class Mode {
        EMAIL_CHECK,
        PWD_CREATION
    }

    fun setViewMode(mode : Mode) {
        when(mode) {
            NewUserActivity.Mode.EMAIL_CHECK -> {
                appTracker.trackPageView(TrackerAdapter.PageName.SIGN_UP_EMAIL_VALIDATION)
                mainCardView?.visibility = View.VISIBLE
                pwdCardView?.visibility = View.INVISIBLE
            }

            NewUserActivity.Mode.PWD_CREATION -> {
                appTracker.trackPageView(TrackerAdapter.PageName.SIGN_UP_PWD_CREATION)
                mainCardView?.visibility = View.INVISIBLE
                pwdCardView?.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()

        signUpBt?.setOnClickListener {
            emailInpLayout?.error = null
            startProgress()
            appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                    TrackerAdapter.EventName.NEW_ACCOUNT,
                    emailField?.text?.toString())

            presenter.createUserWithEmail(emailField?.text?.toString() ?: "", WeakReference(this))
        }

        signInBt?.setOnClickListener {
            appRouter.toAuthenticateUserActivity(this@NewUserActivity)
        }

        pwdBt?.setOnClickListener {
            startProgress()
            passwordField?.error = null
            passwordField?.error = null
            appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                    TrackerAdapter.EventName.NEW_ACCOUNT,
                    emailField?.text?.toString())

            presenter.createPasswordForUserWith(passwordField?.text?.toString() ?: "",
                    confirmField?.text?.toString() ?: "",
                    sessionManager.session,
                    WeakReference(this@NewUserActivity))
        }
    }

    override fun onStop() {
        presenter.unsubscribe()
        super.onStop()
    }

    fun goToCameraView() {
        appRouter.toCameraActionActivity(this)
        finish()
    }

    override fun onEmailValidatedOnServer(appCreationToken: AppCreationToken) {
        stopProgress()
        appTracker.trackEvent(TrackerAdapter.EventCategory.RESULT,
                TrackerAdapter.EventName.NEW_ACCOUNT_VALID_EMAIL_S,
                emailField?.text?.toString())

        sessionManager.session = Session(null, appCreationToken.passwordCreationToken, Session.Scope.CREATION_TOKEN)

        if (sessionManager.store()) {
            setViewMode(Mode.PWD_CREATION)
        }
    }

    override fun onEmailValidatedOnServer(session: Session) {
        stopProgress()
        appTracker.trackEvent(TrackerAdapter.EventCategory.RESULT,
                TrackerAdapter.EventName.NEW_ACCOUNT_VALID_EMAIL_S,
                emailField?.text?.toString())

        sessionManager.session = session
        sessionManager.session.scope = Session.Scope.CREATION_TOKEN

        if (sessionManager.store()) {
            setViewMode(Mode.PWD_CREATION)
        }
    }

    override fun onEmailValidationError(e: Throwable) {
        appTracker.trackEvent(TrackerAdapter.EventCategory.RESULT,
                TrackerAdapter.EventName.NEW_ACCOUNT_VALID_EMAIL_S,
                emailField?.text?.toString())

        when (e) {
            is BadEmailFormatException -> {
                stopProgress()
                emailInpLayout?.error = getString(R.string.bad_email_format)
            }
            is InvalidEmalException -> {
                stopProgress()
                emailInpLayout?.error = getString(R.string.invalid_email_error)
            }

            is TemporaryAcesssException -> {
                presenter.requestChangePasswordTokenForPwdCreation(emailField?.text?.toString()!!, this)
            }

            else -> {
                stopProgress()
                emailInpLayout?.error = getString(R.string.invalid_email_error)
            }
        }
    }

    override fun onNewUserCreationError(e: Throwable) {
        stopProgress()
        when(e) {
            is BadEmailFormatException -> {
                passwordField?.error = getString(R.string.password_wrong_format_error)
                confirmField?.error = getString(R.string.password_wrong_format_error)
            }

            is PasswordNotMatchException -> {
                passwordField?.error = getString(R.string.password_not_match)
                confirmField?.error = getString(R.string.password_not_match)
            }

            else -> {
                passwordField?.error = getString(R.string.password_creation_error)
            }
        }
    }

    override fun onNewUserCreated(user: User) {
        stopProgress()
        sessionManager.session.user = user
        sessionManager.session.passwordCreationToken = null
        sessionManager.session.scope = Session.Scope.AUTHORIZED

        if (sessionManager.store()) {
            appRouter.toCameraActionActivity(this, null)
            finish()
        }

    }
}
