package br.com.aseventos.recon.ui.activities

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.aseventos.data.db.entities.DemoEntity
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.recon.AsReconApplication.Companion.isInternalBuild
import br.com.aseventos.recon.R
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.ui.adapters.DefaultFragmentPagerAdapter
import br.com.aseventos.recon.ui.events.OnDataEnqueuedEvent
import br.com.aseventos.recon.ui.events.OnMediaListScrollStateChanged
import br.com.aseventos.recon.ui.fragments.MediaListFragment
import br.com.aseventos.recon.ui.presenters.MediaListPresenter
import br.com.aseventos.recon.ui.view.MediaListView
import br.com.aseventos.recon.ui.view.TrackableView
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import br.com.aseventos.recon.ui.viewmodels.SearchItemViewModel
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject


class MediaListActivity : DrawerActivity(), MediaListView, TrackableView {
    companion object {
        val MEDIA_LIST_SEARCH_ITEM_FLAG = "MEDIA_LIST_SEARCH_ITEM"
        val MEDIA_LIST_DEMONSTRATION_ID_FLAG = "MEDIA_LIST_DEMONSTRATION_ID"
    }

    override fun getToolbarNavigationIcon(): Int = R.drawable.ic_drawer_icon_closed
    override fun layoutResourceId(): Int = R.layout.activity_media_list
    override fun onInject(graph: ActivityComponent?) = graph?.inject(this)!!

    private val searchItem by lazy { intent.getParcelableExtra<SearchItemViewModel?>(MEDIA_LIST_SEARCH_ITEM_FLAG) }
    private val demonstrationId by lazy { intent.getIntExtra(MEDIA_LIST_DEMONSTRATION_ID_FLAG, 0) }
    private val demonstration by lazy { appDatabase?.demoDao()?.getById(demonstrationId) }
    private val downloadButton by lazy { findViewById<TextView>(R.id.download_button) }
    private val emptyMsg by lazy { findViewById<TextView>(R.id.search_main_message) }
    private val progressView by lazy { findViewById<ViewGroup>(R.id.default_progress_content) }
    private val emptyState   by lazy { findViewById<ViewGroup>(R.id.frame_empty_state) }
    private val fabCustomTgt by lazy { findViewById<FloatingActionButton>(R.id.fap_custom_target) }
    private val viewPager    by lazy { findViewById<ViewPager>(R.id.content_frame) }
    private val tabLayout    by lazy { findViewById<TabLayout>(R.id.main_tablayout) }

    private val FILE_CHOOSER_CODE = 123

    lateinit var progresDialog : ProgressDialog

    override fun getTrackPageName(): TrackerAdapter.PageName = TrackerAdapter.PageName.MEDIA_LIST

    @Inject lateinit var presenter: MediaListPresenter
    @Inject lateinit var appRouter: AppRouter

    var adapter : DefaultFragmentPagerAdapter? = null
    var mediaFiles = ArrayList<MediaFile>()

    override fun getToolbarTitle(): String {
        if (searchItem == null) {
            return demonstration?.name!!
        } else {
            return searchItem!!.title
        }
    }

    override fun onBackPressed() {
        if(!isInternalBuild()) {
            appRouter.toCameraActionActivity(this)
            finish()
        } else {
            super.onBackPressed()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (isInternalBuild()) {
            emptyMsg.setText(R.string.empty_media_list_internal)
        } else {
            emptyMsg.setText(R.string.empty_media_list_internal)
        }

        if (searchItem == null) {
            onDemonstrationRetrieved(demonstration!!)
        } else {
            presenter.checkAndCreateDemonstrationWithSearchItem(searchItem!!, this)
        }
        fabCustomTgt.visibility = View.GONE
    }

    private val FILE_VIDEO_REQUEST_ID = 123

    private fun startVideoChoose() {
        val intent =  Intent(this, FilePickerActivity::class.java)
        intent.putExtra(FilePickerActivity.CONFIGS, Configurations.Builder()
                .setCheckPermission(true)
                .setSelectedMediaFiles(mediaFiles)
                .enableVideoCapture(true)
                .setShowVideos(true)
                .setShowAudios(false)
                .setShowImages(false)
                .setSkipZeroSizeFiles(true)
                .setMaxSelection(1)
                .build())
        startActivityForResult(intent, FILE_CHOOSER_CODE);
    }

    private fun startVideoTargetsChoose() {
        val intent =  Intent(this, FilePickerActivity::class.java)
        intent.putExtra(FilePickerActivity.CONFIGS, Configurations.Builder()
                .setCheckPermission(true)
                .setSelectedMediaFiles(mediaFiles)
                .enableVideoCapture(true)
                .setShowVideos(false)
                .setShowAudios(false)
                .setShowImages(true)
                .setSkipZeroSizeFiles(true)
                .setMaxSelection(5)
                .build())
        startActivityForResult(intent, FILE_CHOOSER_CODE)

    }

    private fun setupFragmentWithItems(items : List<MediaListItemViewModel>) {
        val f = mutableListOf<Fragment>()
        f.plusAssign(MediaListFragment.newInstance(items.filter { !it.isCustomTarget }))

        if (!isInternalBuild()) {
            f.plusAssign(MediaListFragment.newInstance(items.filter { it.isCustomTarget }))
        }

        adapter = DefaultFragmentPagerAdapter(listOf(MediaListFragment.newInstance(items)), supportFragmentManager)
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)


    }

    override fun onStart() {
        super.onStart()
    }

    override fun onUnsubscribe() {
        presenter.unsubscribe()
    }

    override fun onNoDemoCreatedForSearchItem() {
        presenter.createNewDemoWithSearchItem(searchItem!!, this)
    }

    override fun onMediaListItemsFetched(demonstration: DemoEntity, items: List<MediaListItemViewModel>) {
        setupFragmentWithItems(items)
        setupButtonByDemo(demonstration)
    }

    override fun onMediaListScrollStateChanged(event : OnMediaListScrollStateChanged) {
        if (event.shouldDisplayButton) {
            downloadButton.animate().alpha(1.0F).setDuration(100L).start()
        } else {
            downloadButton.animate().alpha(0.0F).setDuration(100L).start()
        }
    }

    override fun onDemonstrationCreated(demonstration: DemoEntity) {
        if (isInternalBuild()) {
            AlertDialog.Builder(this)
                    .setTitle("Demonstração Criada")
                    .setMessage("Uma nova demonstração foi criada para o item procurado, clique em OK para iniciar a sincronização de arquivos")
                    .setPositiveButton("OK") { _, _ ->
                        presenter.fetchItemsUseCaseBySearchIemWithDemonstration(searchItem!!, demonstration, this)
                    }.show()
        } else {
            presenter.fetchItemsUseCaseBySearchIemWithDemonstration(searchItem!!, demonstration, this)
        }

        setupButtonByDemo(demonstration)
    }

    override fun onDemonstrationRetrieved(demonstration: DemoEntity) {
        val videos = appDatabase?.mediaDao()?.getByDemoId(demonstration.id)
        if (videos != null) {
            val items = presenter.buildMediaItemViewModelByRecons(videos)
            setupFragmentWithItems(items)
            downloadButton.animate().alpha(1.0F).setDuration(100L).start()
            setupButtonByDemo(demonstration)
        }
    }

    override fun onFilesCreatedAndDownloadServiceStarted() {
        appTracker.trackEvent(TrackerAdapter.EventCategory.ACTION,
                TrackerAdapter.EventName.NEW_DEMO_SYNC,
                null)

        progresDialog = ProgressDialog.show(this, "Aguarde", "Preparando arquivos para download", true)

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDataEnqueuedEvent(event : OnDataEnqueuedEvent) {
        progresDialog.dismiss()
        AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage("Download iniciado com sucesso")
                .setPositiveButton("Acompanhar Progresso") { _: DialogInterface, _: Int ->
                    appRouter.toDownloads(this@MediaListActivity, null)
                    finish()
                }.show()
    }

    private fun setupButtonByDemo(demonstration: DemoEntity) {
        if (viewPager.currentItem == 0) {
            downloadButton.text = if(demonstration.status.contentEquals(DemoEntity.STATUS_IDLE)) {
                "Iniciar Download"
            } else if(demonstration.status.contentEquals(DemoEntity.STATUS_DOWNLOADING)) {
                "Ver downloads"
            } else {
                "Carregar Demonstração"
            }

            downloadButton.visibility = View.VISIBLE
            downloadButton.setOnClickListener {
                if(demonstration.status.contentEquals(DemoEntity.STATUS_IDLE)) {
                    presenter.createFilesForDownloadAndSendToDownloadView(demonstration.id, this)
                } else if(demonstration.status.contentEquals(DemoEntity.STATUS_DOWNLOADING)) {
                    appRouter.toDownloads(this, null)
                } else {
                    appRouter.toCameraActionActivity(this, demonstration.localConfigPath)
                }
            }

            downloadButton.animate().alpha(1.0F).setDuration(100L).start()
            if (!isInternalBuild() && demonstration.status.contentEquals(DemoEntity.STATUS_STORED)) {
                downloadButton.visibility = View.GONE
            }

        } else {
            downloadButton.text = "Adicionar novo vídeo"
            downloadButton.setOnClickListener {
                AlertDialog.Builder(this@MediaListActivity)
                        .setTitle("Alerta")
                        .setMessage("Vamos iniciar o processo de upload de seus videos e suas imagens para que você possa criar sua própria experiência de realidade aumentada, primeiro iremos selecionar o video desejado e depois, todas as imagens")
                        .setPositiveButton("Escolher o Video") { _, _ ->
                            startVideoChoose()
                        }
                        .setNegativeButton("Cancelar", null)
                        .show()
            }
        }


        viewPager.clearOnPageChangeListeners()
        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                tabLayout.getTabAt(position)?.select()
                if (position == 0) {
                    downloadButton.text = if(demonstration.status.contentEquals(DemoEntity.STATUS_IDLE)) {
                        "Iniciar Download"
                    } else if(demonstration.status.contentEquals(DemoEntity.STATUS_DOWNLOADING)) {
                        "Ver downloads"
                    } else {
                        "Carregar Demonstração"
                    }

                    if (!isInternalBuild() && demonstration.status.contentEquals(DemoEntity.STATUS_STORED)) {
                        downloadButton.visibility = View.GONE
                    }
                } else {
                    downloadButton.visibility = View.VISIBLE
                    downloadButton.text = "Adicionar novo vídeo"
                    downloadButton.setOnClickListener {
                        AlertDialog.Builder(this@MediaListActivity)
                                .setTitle("Alerta")
                                .setMessage("Vamos iniciar o processo de upload de seus videos e suas imagens para que você possa criar sua própria experiência de realidade aumentada, primeiro iremos selecionar o video desejado e depois, todas as imagens")
                                .setPositiveButton("Escolher o Video") { _, _ ->
                                    startVideoChoose()
                                }
                                .setNegativeButton("Cancelar", null)
                                .show()
                    }

                }
            }
        })



    }

    override fun onDemonstrationCreationError(e: Throwable) {
        //TODO handle error creating demo
    }

    override fun onMediaListItemsFetchError(e: Throwable) {
        emptyState.visibility = View.VISIBLE
        progressView.visibility = View.GONE
        downloadButton.visibility = View.GONE
    }

    override fun startProgress() {
        emptyState.visibility = View.GONE
        progressView.visibility = View.VISIBLE
    }

    override fun stopProgress() {
        progressView.visibility = View.INVISIBLE
    }


}
