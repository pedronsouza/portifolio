package br.com.aseventos.recon.ui.activities

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.SessionManager
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequestBuilder
import javax.inject.Inject
import android.content.Intent
import android.util.Log


class ProfileActivity : AppCompatActivity() {
    companion object {
        val PICK_IMAGE = 321
    }
    private val toolbar by lazy { findViewById(R.id.main_toolbar) as Toolbar? }
    private val name by lazy { findViewById(R.id.profile_user_name) as TextView? }
    private val nameHeader by lazy { findViewById(R.id.user_name_header) as TextView? }
    private val clazz by lazy { findViewById(R.id.profile_user_room_layout) as TextView? }
    private val email by lazy { findViewById(R.id.profile_user_email) as TextView? }
    private val emailHeader by lazy { findViewById(R.id.user_email_header) as TextView? }
    private val editImg by lazy { findViewById(R.id.edit_image) as TextView? }
    private val profileImg by lazy { findViewById(R.id.user_profile_image) as SimpleDraweeView? }

    @Inject lateinit var sessionManager : SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        activityGraph?.inject(this)
        toolbar?.setNavigationIcon(R.drawable.ic_back_dark)
        toolbar?.setNavigationOnClickListener { onBackPressed() }

        name?.text = sessionManager.session.user?.name
        nameHeader?.text = sessionManager.session.user?.name

        email?.text = sessionManager.session.user?.email
        emailHeader?.text = sessionManager.session.user?.email

        editImg?.setOnClickListener { launchGalleryPicker() }

        if (sessionManager.session.user?.photo != null) {
            val request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(sessionManager.session.user?.photo)).setLocalThumbnailPreviewsEnabled(true).setProgressiveRenderingEnabled(true).build()
            profileImg?.controller = Fresco.newDraweeControllerBuilder().setImageRequest(request).setOldController(profileImg?.controller).build()
            profileImg?.visibility = View.VISIBLE
        } else {
            profileImg?.visibility = View.INVISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE) {
            //TODO save image
            Log.d("ProfileActivity", "Image Selected from gallery")
        }
    }

    private fun launchGalleryPicker() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Escolha uma imagem"), PICK_IMAGE)
    }
}