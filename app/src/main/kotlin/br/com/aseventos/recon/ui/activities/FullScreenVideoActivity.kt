package br.com.aseventos.recon.ui.activities

import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.widget.MediaController
import android.widget.VideoView
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.ui.fragments.VideoPlayerFullScreenFragment
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class FullScreenVideoActivity : AppCompatActivity() {
    private val videoView by lazy { findViewById(R.id.fullscreen_video_view) as VideoView? }
    private val sourceUrl by lazy { intent?.extras?.getString(VideoPlayerFullScreenFragment.VIDEO_PLAYER_URL_FLAG, null) }
    private val seekPosition by lazy { intent?.extras?.getInt(VideoPlayerFullScreenFragment.VIDEO_PLAYER_SEEK_POSITION_FLAG, -1) ?: -1 }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen_video)
        videoView?.setVideoURI(Uri.parse(sourceUrl))
        val controls = MediaController(this)
        controls.setAnchorView(videoView)
        videoView?.setMediaController(controls)

        videoView?.setOnErrorListener { mediaPlayer, i, x ->
            AlertDialog.Builder(this)
                    .setTitle("Alerta")
                    .setMessage("Erro ao tentar reproduzir o video em fullscreen")
                    .setNeutralButton("Ok, Entendi", { _, _ ->
                        EventBus.getDefault().post(HomeActivity.OnFullScreenVideoFinished())
                    }).show()

            false;
        }

        if (seekPosition > 0) {
            videoView?.seekTo(seekPosition)
        }

        videoView?.start()
    }
}