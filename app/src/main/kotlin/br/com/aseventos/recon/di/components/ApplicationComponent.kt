package br.com.aseventos.recon.di.components


import android.content.Context
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.domain.datasources.media.MediaDataSource
import br.com.aseventos.domain.datasources.user.UserDataSource
import br.com.aseventos.domain.usecases.executors.PostExecutionThread
import br.com.aseventos.domain.usecases.executors.ThreadExecutor
import br.com.aseventos.recon.di.modules.AppModule
import br.com.aseventos.recon.di.modules.DataModule
import br.com.aseventos.recon.di.modules.NetModule
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.data.local.DemonstrationManager
import br.com.aseventos.data.net.HttpServices
import br.com.aseventos.data.usecases.users.UploadCustomTargetImageUseCase
import br.com.aseventos.data.usecases.users.UploadCustomTargetVideoUseCase
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.fragments.MainMenuFragment
import br.com.aseventos.recon.ui.fragments.MediaListFragment
import br.com.aseventos.recon.ui.fragments.OnBoardingFragment
import com.google.gson.Gson
import com.tonyodev.fetch2.Fetch
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(NetModule::class, DataModule::class, AppModule::class))
interface ApplicationComponent {
    fun sessionManager() : SessionManager
    fun threadExecutor() : ThreadExecutor
    fun postExecutionThread() : PostExecutionThread
    fun userDataSource() : UserDataSource
    fun mediaDataSource() : MediaDataSource
    fun appRouter() : AppRouter
    fun demonstrationManager() : DemonstrationManager
    fun context() : Context
    fun gson() : Gson
    fun httpServices() : HttpServices
    fun appTracker() : TrackerAdapter
    fun database() : ReconDatabase?
    fun appFileLoader() : FileLoader

    fun uploadCustomTargetVideo() : UploadCustomTargetVideoUseCase
    fun uploadCUstomTargetImages() : UploadCustomTargetImageUseCase


    fun inject(item : OnBoardingFragment)
    fun inject(item : MainMenuFragment)
    fun inject(fragment : MediaListFragment)
}