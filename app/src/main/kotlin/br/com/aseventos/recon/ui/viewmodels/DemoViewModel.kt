package br.com.aseventos.recon.ui.viewmodels

data class DemoViewModel(
        val id : Int,
        val name : String,
        val firstTargetId : Int,
        val status : String)