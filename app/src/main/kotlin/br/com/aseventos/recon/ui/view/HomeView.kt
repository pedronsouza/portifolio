package br.com.aseventos.recon.ui.view

interface HomeView : MainMenuListener {
    fun onOnBoardFinished()
}