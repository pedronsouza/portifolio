package br.com.aseventos.recon.ui.events

data class OnMediaListScrollStateChanged(val shouldDisplayButton : Boolean)