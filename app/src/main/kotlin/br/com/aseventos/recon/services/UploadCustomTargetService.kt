package br.com.aseventos.recon.services

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.support.v4.app.JobIntentService
import android.util.Log
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import io.fabric.sdk.android.services.settings.IconRequest.build
import android.content.Context.NOTIFICATION_SERVICE
import br.com.aseventos.recon.R.mipmap.ic_launcher
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import br.com.aseventos.data.net.helpers.CustomTargetInfoHelper
import br.com.aseventos.domain.models.CustomTargetVideo
import br.com.aseventos.domain.models.Demonstration
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.activities.HomeActivity
import br.com.aseventos.recon.ui.activities.MediaListActivity
import br.com.aseventos.recon.ui.events.OnUploadCustomTargetErrorEvent
import br.com.aseventos.recon.ui.events.OnUploadCustomTargetFinishedEvent
import br.com.aseventos.recon.ui.viewmodels.SearchItemViewModel
import org.greenrobot.eventbus.EventBus
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream
import java.lang.RuntimeException


class UploadCustomTargetService : JobIntentService() {
    companion object {
        val UPLOAD_CUSTOM_TGT_VIDEO_URI_FLAG = "UPLOAD_CUSTOM_TGT_VIDEO_URI"
        val UPLOAD_CUSTOM_TGT_IMAGES_FLAG = "UPLOAD_CUSTOM_TGT_IMAGES"
    }

    val imagesForUpload = mutableListOf<String>()
    var videoUri = ""
    val not_id = "12313"

    override fun onHandleWork(intent: Intent) {
        try {
            showOngoingNotification()
            imagesForUpload.plusAssign(intent.getStringArrayListExtra(UPLOAD_CUSTOM_TGT_IMAGES_FLAG))
            videoUri = intent.getStringExtra(UPLOAD_CUSTOM_TGT_VIDEO_URI_FLAG)

            val endpoints = appGraph?.httpServices()?.user!!
            val videoInfo = if (videoUri.contains("http")) {
                CustomTargetVideo("", videoUri)
            } else {
                endpoints.sendCustomTargetVideoToProviderSync(createFilePart(File(videoUri), "video/mp4")).execute().body().data
            }

            if (videoInfo != null) {
                videoUri = videoInfo.videoId
                val videoUrlPart = MultipartBody.create(okhttp3.MultipartBody.FORM, videoUri)
                val photosId = mutableListOf<String>()
                val imgs = imagesForUpload.flatMap { listOf(createFilePart(File(it), "image/png")) }
                imgs.forEach { img ->
                    val data = endpoints.sendCustomTargetVideoImagesSync(img, videoUrlPart).execute().body()
                    if (data != null) {
                        photosId.plusAssign(data.data!!.photos.flatMap { listOf(it.id) })
                    }
                }
                if (photosId.size > 0) {
                    val respData = endpoints.saveCustomTargetInfo(CustomTargetInfoHelper(videoUri, photosId.toList())).execute().body()
                    if (respData != null) {
                        showReportNotification()
                    }
                } else {
                    showErrorReportNotification()
                }
            } else {
                showErrorReportNotification()
            }

        } catch (e : Throwable) {
            showErrorReportNotification()
        }
    }

    private fun createFilePart(file : File, mediaType : String) : MultipartBody.Part {
        val rqBody = RequestBody.create(MediaType.parse("image/jpeg"), file)
        return MultipartBody.Part.createFormData("file", file.name, rqBody)
    }


    private fun showReportNotification() {
        if (AsReconApplication.isOnBackground) {

            val intent = Intent(this, HomeActivity::class.java)
            val contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val b = NotificationCompat.Builder(this, not_id)

            b.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Upload de Targets")
                    .setContentText("Seu target foi criado com sucesso.")
                    .setContentIntent(contentIntent)

                    .setContentInfo("Info")


            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(1, b.build())
        } else {
            EventBus.getDefault().post(OnUploadCustomTargetFinishedEvent())
        }
    }

    private fun showOngoingNotification() {
        val intent = Intent(this, HomeActivity::class.java)
        val contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val b = NotificationCompat.Builder(this, not_id)

        b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Upload de Targets")
                .setContentText("Estamos processando seus arquivos. Em breve iremos te avisar sobre o envio")
                .setContentIntent(contentIntent)
                .setOngoing(true)

                .setContentInfo("Info")


        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(1, b.build())
    }

    private fun showErrorReportNotification() {

        if (AsReconApplication.isOnBackground) {
            val sessionManager = appGraph?.sessionManager()!!
            val user = sessionManager.session.user
            val intent = Intent(this, MediaListActivity::class.java)

            if (user != null) {
                val searchItem = SearchItemViewModel(user.id!!, user.name!!, user.course, user.photo, user.email, true, SearchResult.Type.USER)
                intent.putExtra(MediaListActivity.MEDIA_LIST_SEARCH_ITEM_FLAG, searchItem)
            }

            val contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val b = NotificationCompat.Builder(this, not_id)

            b.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Upload de Targets")
                    .setContentText("Ocorreu um erro no seu envio.")
                    .addAction(R.drawable.ic_download_video_xxhdpi, "Tentar Novamente", contentIntent)
                    .setContentIntent(contentIntent)
                    .setContentInfo("Info")


            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(1, b.build())
        } else {
            EventBus.getDefault().post(OnUploadCustomTargetErrorEvent(videoUri, ArrayList(imagesForUpload)))
        }

    }

    fun createFileImgAsPng(filePath : String) : File {
        try {
            val bitmap = BitmapFactory.decodeFile(filePath)
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            val bitmapdata = bos.toByteArray()

            val newFile = File(filePath.replace(".jpeg", ".png").replace(".jpg", ".png"))

            val fos =FileOutputStream(newFile)
            bos.write(bitmapdata)
            bos.flush()
            bos.close()
            return newFile
        } catch (e: Exception) {
            e.printStackTrace()
        }

        throw RuntimeException()
    }
}