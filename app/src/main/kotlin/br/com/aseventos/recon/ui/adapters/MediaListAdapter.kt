package br.com.aseventos.recon.ui.adapters

import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.FileLoader
import br.com.aseventos.recon.ui.viewmodels.MediaListItemViewModel
import com.facebook.drawee.view.SimpleDraweeView

class MediaListAdapter(private val items : List<MediaListItemViewModel>,
                       private val appRouter : AppRouter,
                       private val appFileLoader : FileLoader) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = items.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_media_list_item_video, parent, false)
        return MediaListViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MediaListViewHolder) {
            holder.update(items[position], position)
        }
    }

    inner class MediaListViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val positionLbl by lazy { itemView.findViewById<TextView>(R.id.item_position) }
        private val itemIcon    by lazy { itemView.findViewById<SimpleDraweeView>(R.id.item_icon) }
        private val iconOverlay by lazy { itemView.findViewById<ImageView>(R.id.item_icon_overlay) }
        private val itemName    by lazy { itemView.findViewById<TextView>(R.id.item_name) }
        private val button      by lazy { itemView.findViewById<TextView>(R.id.item_button) }

        fun update(item : MediaListItemViewModel, position: Int) {
            val counter = position + 1
            positionLbl.text = counter.toString()

            if (item.type == MediaListItemViewModel.Type.VIDEO) {
                itemView.setOnClickListener{
                    appRouter.toMediaItemDetail(itemView.context, item)
                }

                itemName.text = "Video ${counter}"
            } else {
                itemName.text = "Imagem ${counter}"
                iconOverlay.visibility = View.GONE
            }

            appFileLoader.createTargetImageRequestForTargetId(item.targetId) { controller ->
                itemIcon.controller = controller
            }
        }
    }
}