package br.com.aseventos.recon.ui.activities

import android.Manifest
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.TextView
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.domain.models.Session
import br.com.aseventos.domain.models.User
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.presenters.SignInPresenter
import br.com.aseventos.recon.ui.view.SignInView
import br.com.aseventos.recon.ui.view.TrackableView
import java.lang.ref.WeakReference
import javax.inject.Inject


class MainActivity : ProgressibleActivity(), SignInView, TrackableView {
    private val signUpBt by lazy { findViewById(R.id.sign_up_bt) as TextView? }
    private val signInBt by lazy { findViewById(R.id.sign_in_bt) as TextView? }

    private val emailLayout by lazy { findViewById(R.id.email_layout) as TextInputLayout? }
    private val emailField by lazy { findViewById(R.id.sign_in_email) as TextInputEditText? }

    private val pwdLayout by lazy { findViewById(R.id.pwd_layout) as TextInputLayout? }
    private val pwdField by lazy { findViewById(R.id.sign_in_pwd) as TextInputEditText? }

    private val errorLabel by lazy { findViewById(R.id.sign_in_error_label) as TextView? }

    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var sessionManager : SessionManager
    @Inject lateinit var presenter : SignInPresenter

    enum class Mode {
        NORMAL,
        BAD_CREDENTIALS,
        LOADING
    }

    override fun getTrackPageName(): TrackerAdapter.PageName = TrackerAdapter.PageName.SIGN_IN_FORM

    fun setViewMode(mode : Mode) {
        when(mode) {
            MainActivity.Mode.NORMAL -> {
                stopProgress()
                errorLabel?.visibility = View.INVISIBLE
            }

            MainActivity.Mode.BAD_CREDENTIALS -> {
                stopProgress()
                errorLabel?.visibility = View.VISIBLE
            }

            MainActivity.Mode.LOADING -> {
                startProgress()
                errorLabel?.visibility = View.INVISIBLE
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityGraph?.inject(this)

        signUpBt?.setOnClickListener {
            appRouter.toNewUserActivity(this, false)
        }

        signInBt?.setOnClickListener {
            setViewMode(Mode.LOADING)
            presenter.authenticateUserWithCredentials(emailField?.text?.toString() ?: "",
                                                                                    pwdField?.text?.toString() ?: "",
                                                                                    WeakReference(this@MainActivity)) }
    }

    override fun onStop() {
        presenter.unsubscribe()
        super.onStop()
    }

    fun goToCameraView() {
        appRouter.toCameraActionActivity(this@MainActivity)
        finish()
    }

    override fun onAuthenticationSuccessfully(user: User) {
        setViewMode(Mode.NORMAL)
        sessionManager.session.user = user
        sessionManager.session.scope = Session.Scope.AUTHORIZED

        if (sessionManager.store()) {
            appRouter.toCameraActionActivity(this, null)
            finish();
        }
    }

    override fun onAuthenticationError(e: Throwable) =
            setViewMode(Mode.BAD_CREDENTIALS)
}
