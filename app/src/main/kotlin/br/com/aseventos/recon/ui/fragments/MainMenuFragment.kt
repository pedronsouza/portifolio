package br.com.aseventos.recon.ui.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.domain.models.Session
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.AsReconApplication.Companion.isInternalBuild
import br.com.aseventos.recon.R
import br.com.aseventos.recon.helpers.AppMenu
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.helpers.SessionManager
import br.com.aseventos.recon.ui.activities.HomeActivity
import br.com.aseventos.recon.ui.adapters.MainMenuAdapter
import br.com.aseventos.recon.ui.view.MainMenuListener
import br.com.aseventos.recon.ui.viewmodels.MenuItemViewModel
import br.com.aseventos.recon.ui.viewmodels.SearchItemViewModel
import java.lang.ref.WeakReference
import javax.inject.Inject

class MainMenuFragment : Fragment(), AppMenu.Listener {
    companion object {
        fun create(parent : MainMenuListener) : MainMenuFragment {
            val fragment = MainMenuFragment()
            fragment.parent = WeakReference(parent)
            return fragment
        }
    }

    private lateinit var viewGroup : View
    private lateinit var parent : WeakReference<MainMenuListener>

    private val recycler by lazy { viewGroup.findViewById<RecyclerView?>(R.id.menu_recycler_view) }
    private val drawerBt by lazy { viewGroup.findViewById<ImageView>(R.id.drawer_control) }
    private val logOutBt by lazy { viewGroup.findViewById<TextView>(R.id.logout_bt) }

    @Inject lateinit var appRouter : AppRouter
    @Inject lateinit var sessionManager : SessionManager
    @Inject lateinit var appMenu : AppMenu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appGraph?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewGroup = inflater!!.inflate(R.layout.fragment_main_menu, null)
        return viewGroup
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        drawerBt?.setOnClickListener {
            parent.get()?.onDrawerClosedInvoked()
        }

        if (isInternalBuild()) {
            logOutBt?.visibility = View.INVISIBLE
        } else {
            logOutBt?.visibility = View.VISIBLE
            logOutBt?.setOnClickListener { onLogOutMediaItemMenuClicked() }
        }

        val adapter = MainMenuAdapter(appMenu.createMenuByUserSession(sessionManager.session, this))

        recycler?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler?.adapter = adapter
        recycler?.setHasFixedSize(true)

    }

    override fun getCtx(): Context = activity!!

    override fun onHomeItemMenuClicked() {
        if (activity is HomeActivity) {
            parent.get()?.onDrawerClosedInvoked()
        } else {
            activity?.finish()
        }
    }

    override fun onSearchItemMenuClicked() {
        appRouter.toSearchUsersAction(activity as AppCompatActivity)
        activity?.finish()
    }


    override fun onAppMediaItemMenuClicked() {
        val user = sessionManager.session.user
        if (user != null) {
            val searchItem = SearchItemViewModel(user.id  ?: "", user.name ?: "", user.course, user.photo, user.email, true, SearchResult.Type.USER)
            appRouter.toMediaList(activity as AppCompatActivity, searchItem)
        }
    }

    override fun onLogOutMediaItemMenuClicked() {
        if (sessionManager.logout()) {
            appRouter.toAccessOptionsActivity(activity as AppCompatActivity)
        }
    }

    override fun onDownloadScreenItemMenuClicked() {
        appRouter.toDownloads(activity!!, null)
    }

    override fun onDemosScreenItemMenuClicked() {
        appRouter.toDemonstration(activity!!)
        activity?.finish()
    }
}