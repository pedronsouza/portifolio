package br.com.aseventos.recon.helpers.ui

import `in`.workarounds.typography.TextView
import android.content.Context
import android.util.AttributeSet

class AnimatedButtonView : TextView {

    constructor(context: Context?) : super(context) { init(context, null) }
    constructor(context: Context?, attrs : AttributeSet) : super(context, attrs) { init(context, attrs) }
    constructor(context: Context?, attrs : AttributeSet, defStyle : Int) : super(context, attrs, defStyle) { init(context, attrs) }

    private fun init(context: Context?, attrs: AttributeSet?) {}
    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener { view ->
            view.alpha = 0.6F
            l?.onClick(view)
            view.postDelayed({ view.alpha = 1.0F }, 150)
        }
    }
}