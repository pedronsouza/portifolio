package br.com.aseventos.recon.ui.adapters

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.domain.models.SearchResult
import br.com.aseventos.recon.AsReconApplication.Companion.appGraph
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.view.SearchUserView
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.common.util.UriUtil



class GeneralSearchAdapter(val items : List<SearchResult>, val view : SearchUserView) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = items.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_search_result_item, parent, false)
        return SearchResultViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SearchResultViewHolder) {
            holder.update(items[position])
        }
    }

    inner class SearchResultViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val thumb       by lazy { itemView.findViewById<SimpleDraweeView>(R.id.search_result_thumb) }
        private val label       by lazy { itemView.findViewById<TextView>(R.id.search_result_label) }
        private val description by lazy { itemView.findViewById<TextView>(R.id.search_result_description) }
        private val button      by lazy { itemView.findViewById<ImageView>(R.id.search_result_button) }
        private val thumbDir    by lazy { itemView.findViewById<ImageView>(R.id.search_directory_thumb)}

        fun update(item : SearchResult) {
            if (item.type == SearchResult.Type.USER) {
                thumb.visibility = View.VISIBLE
                thumbDir.visibility = View.INVISIBLE
                if (item.meta?.photo != null)
                    thumb.setImageURI(Uri.parse(item.meta?.photo))

                description.text = item.description
            } else {
                thumb.visibility = View.INVISIBLE
                thumbDir.visibility = View.VISIBLE
                thumbDir.setImageResource(R.drawable.ic_directory_placeholder)
            }

            label.text = item.title
            button.visibility = View.GONE
            itemView.setOnClickListener {
                view.onSearchItemSelected(item)
            }
        }
    }
}