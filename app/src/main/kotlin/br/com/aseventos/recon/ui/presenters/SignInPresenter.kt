package br.com.aseventos.recon.ui.presenters

import br.com.aseventos.data.usecases.session.AuthenticateUserUseCase
import br.com.aseventos.data.usecases.v2.AuthorizeUserCase
import br.com.aseventos.domain.execptions.validations.BadCredentialsExceptions
import br.com.aseventos.domain.models.User
import br.com.aseventos.domain.models.v2.AppSession
import br.com.aseventos.recon.helpers.BaseSubscriber
import br.com.aseventos.recon.ui.view.SignInView
import java.lang.ref.WeakReference
import java.util.regex.Pattern
import javax.inject.Inject

class SignInPresenter : BasePresenter {
    companion object {
        private val VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)
        private val VALID_PWD_SIZE = Pattern.compile("^.{3,8}\$", Pattern.CASE_INSENSITIVE)
    }

    private var authorizeUserCase : AuthorizeUserCase

    @Inject constructor(authorizeUserCase : AuthorizeUserCase) {
        this.authorizeUserCase = authorizeUserCase
    }

    fun authenticateUserWithCredentials(email : String, password : String, view : WeakReference<SignInView>) {
        if (VALID_EMAIL_ADDRESS_REGEX.matcher(email).find() && VALID_PWD_SIZE.matcher(password).find()) {
            this.authorizeUserCase.email = email
            this.authorizeUserCase.password = password
            this.authorizeUserCase.execute(AuthenticateUserSubscriber(view))
        } else {
            view.get()?.onAuthenticationError(BadCredentialsExceptions())
        }
    }

    override fun unsubscribe() {
        authorizeUserCase.unsubscribe()
    }

    inner class AuthenticateUserSubscriber(val view : WeakReference<SignInView>) : BaseSubscriber<AppSession>() {
        override fun onError(e: Throwable) =
                view.get()?.onAuthenticationError(e) ?: Unit

        override fun onNext(item: AppSession){
            val map = User(
                    id = item.user.baseSystemId,
                    name = item.user.name,
                    course = item.user.course,
                    email = item.user.email,
                    password = null,
                    token = item.token,
                    photo = item.user.photoUrl
            )

            view.get()?.onAuthenticationSuccessfully(map)
        }


        override fun onCompleted() {}

    }
}