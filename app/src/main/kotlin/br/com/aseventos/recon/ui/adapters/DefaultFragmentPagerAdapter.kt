package br.com.aseventos.recon.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import br.com.aseventos.recon.ui.view.TitleView

class DefaultFragmentPagerAdapter(val frags : List<Fragment>, fm : FragmentManager) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment = frags[position]
    override fun getCount(): Int = frags.size
    override fun getPageTitle(position: Int): CharSequence? {
        val f = frags[position]
        return if (f is TitleView) {
            f.getViewTitle()
        } else {
            super.getPageTitle(position)
        }
    }
}