package br.com.aseventos.recon.ui.events

import com.tonyodev.fetch2.Download

data class OnDownloadProgressUpdate(val download : Download, val progress : Int)