package br.com.aseventos.recon.ui.viewmodels

data class MenuItemViewModel(val title : String, val onClickCompletionBlock : () -> Unit)