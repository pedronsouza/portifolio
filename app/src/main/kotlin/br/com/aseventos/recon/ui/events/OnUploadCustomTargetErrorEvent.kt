package br.com.aseventos.recon.ui.events

data class OnUploadCustomTargetErrorEvent(val videoUri : String, val images : ArrayList<String>)