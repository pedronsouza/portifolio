package br.com.aseventos.recon.ui.activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import br.com.aseventos.data.db.ReconDatabase
import br.com.aseventos.domain.adapters.TrackerAdapter
import br.com.aseventos.recon.AsReconApplication
import br.com.aseventos.recon.AsReconApplication.Companion.activityGraph
import br.com.aseventos.recon.di.components.ActivityComponent
import br.com.aseventos.recon.helpers.AppRouter
import br.com.aseventos.recon.services.UploadCustomTargetService
import br.com.aseventos.recon.ui.events.OnUploadCustomTargetErrorEvent
import br.com.aseventos.recon.ui.events.OnUploadCustomTargetFinishedEvent
import br.com.aseventos.recon.ui.observers.ASLifeCycleObserver
import br.com.aseventos.recon.ui.view.TrackableView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {
    abstract fun layoutResourceId() : Int
    abstract fun onInject(graph : ActivityComponent?)
    abstract fun onUnsubscribe()

    @Inject lateinit var baseAppRouter : AppRouter
    @Inject lateinit var appTracker : TrackerAdapter
    var appDatabase : ReconDatabase? = null

    private val lifecycleObserver = ASLifeCycleObserver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (layoutResourceId() != 0) {
            setContentView(layoutResourceId())
        }
        activityGraph?.inject(this)
        appDatabase = activityGraph?.database()
        onInject(activityGraph)

        if (this is TrackableView) {
            appTracker.trackPageView(this.getTrackPageName())
        }
    }

    protected fun showSoftKeyboard(view : View) {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view.requestFocus()
        inputMethodManager.showSoftInput(view, 0)
    }

    protected fun hideSoftKeyboard(view : View) {
        if (currentFocus != null) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUploadCustomTargetFinishedEvent(event : OnUploadCustomTargetFinishedEvent) {
        AlertDialog.Builder(this)
                .setTitle("Seu Target está sendo processado")
                .setMessage("Tudo pronto, já iniciamos o processamento dos arquivos e o envio para o servidor. Em breve o app irá te notificar sobre o resultado.")
                .setPositiveButton("OK entendi") {i, _ ->
                    baseAppRouter.toMediaList(this@BaseActivity, appDatabase?.demoDao()?.getFirstDemo()?.id ?: 0)
                }.setNegativeButton("Cancelar", null)
                .show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUploadCustomTargetErrorEvent(event : OnUploadCustomTargetErrorEvent) {
        AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage("Ocorreu um erro no seu envio.")
                .setPositiveButton("Tentar Novamente") {i, _ ->
                    val intent = Intent(this, UploadCustomTargetService::class.java)
                    intent.putExtra(UploadCustomTargetService.UPLOAD_CUSTOM_TGT_VIDEO_URI_FLAG, event.videoUri)
                    intent.putExtra(UploadCustomTargetService.UPLOAD_CUSTOM_TGT_IMAGES_FLAG, event.images)
                    startService(intent)

                    AlertDialog.Builder(this)
                            .setTitle("Seu Target está sendo processado")
                            .setMessage("Tudo pronto, já iniciamos o processamento dos arquivos e o envio para o servidor. Em breve o app irá te notificar sobre o resultado.")
                            .setPositiveButton("OK entendi") {i, _ ->
                                i.dismiss()
                            }.setNegativeButton("Cancelar", null)
                            .show()
                }.setNegativeButton("Cancelar", null)
                .show()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        lifecycle.addObserver(lifecycleObserver)
    }

    override fun onStop() {
        super.onStop()
        onUnsubscribe()
        EventBus.getDefault().unregister(this)
        lifecycle.removeObserver(lifecycleObserver)
    }
}