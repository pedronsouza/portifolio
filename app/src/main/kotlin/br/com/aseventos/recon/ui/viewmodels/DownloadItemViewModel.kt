package br.com.aseventos.recon.ui.viewmodels

import com.google.gson.annotations.Expose
import com.tonyodev.fetch2.Status

data class DownloadItemViewModel(@Expose val id: String,
                                 @Expose val demoId : Int,
                                 @Expose val videoId : Int,
                                 @Expose val name: String,
                                 @Expose val sourceUrl : String,
                                 @Expose var progress: Int,
                                 @Expose val targetId : Int,
                                 @Expose var status: Status)