package br.com.aseventos.recon.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.viewmodels.MegaButtonViewModel

class SearchMegaButtonsAdapter(val itens : MutableList<MegaButtonViewModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = itens.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_search_item_detail_button, parent, false)
        return MegaButtonViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MegaButtonViewHolder) {
            holder.bind(itens[position])
        }
    }

    inner class MegaButtonViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val thumb by lazy { itemView.findViewById<ImageView>(R.id.button_icon) }
        private val label by lazy { itemView.findViewById<TextView>(R.id.button_label) }

        fun bind(item : MegaButtonViewModel) {
            thumb.setImageResource(item.resId)
            label.text = item.title
            itemView.setOnClickListener {
                item.onMegaButtonClicked()
            }
        }
    }

}

