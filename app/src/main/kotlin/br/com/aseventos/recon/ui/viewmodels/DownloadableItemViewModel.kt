package br.com.aseventos.recon.ui.viewmodels

import android.os.Parcel
import android.os.Parcelable

data class DownloadableItemViewModel(val demoId : Int,
                                     val reconId : Int,
                                     val remotePath : String,
                                     val localPath : String,
                                     var requestId : String? = null,
                                     val targetId : List<String>,
                                     val type : MediaListItemViewModel.Type) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createStringArrayList(),
            parcel.readSerializable() as MediaListItemViewModel.Type) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(demoId)
        parcel.writeInt(reconId)
        parcel.writeString(remotePath)
        parcel.writeString(localPath)
        parcel.writeString(requestId)
        parcel.writeStringList(targetId)
        parcel.writeSerializable(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DownloadableItemViewModel> {
        override fun createFromParcel(parcel: Parcel): DownloadableItemViewModel {
            return DownloadableItemViewModel(parcel)
        }

        override fun newArray(size: Int): Array<DownloadableItemViewModel?> {
            return arrayOfNulls(size)
        }
    }
}