package br.com.aseventos.recon.ui.activities

import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.view.View
import br.com.aseventos.recon.R
import br.com.aseventos.recon.ui.fragments.MainMenuFragment
import br.com.aseventos.recon.ui.fragments.OnBoardingFragment
import br.com.aseventos.recon.ui.view.HomeView
import java.lang.ref.WeakReference

abstract class DrawerActivity : ToolbarActivity(), DrawerLayout.DrawerListener, HomeView {
    override fun onDrawerStateChanged(newState: Int) {}
    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
    protected val drawerLayout by lazy { findViewById<DrawerLayout>(R.id.container) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        drawerLayout.addDrawerListener(this)
        toolbar?.setNavigationOnClickListener {
            drawerLayout?.openDrawer(GravityCompat.START)
        }
        supportFragmentManager.beginTransaction()
                .add(R.id.drawer_content, MainMenuFragment.create(this))
                .commit()
    }

    override fun onDrawerClosed(drawerView: View) {
        toolbar?.setNavigationIcon(R.drawable.ic_drawer_icon_closed)
    }

    override fun onDrawerOpened(drawerView: View) {
        toolbar?.navigationIcon = null
    }

    override fun onDrawerClosedInvoked() {
        drawerLayout?.closeDrawer(GravityCompat.START)
    }

    override fun onDisplayOnboarding() {
        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, OnBoardingFragment.create(WeakReference(this)), OnBoardingFragment::class.java.canonicalName)
                .commit()
    }

    override fun onOnBoardFinished() {
        val fragment = supportFragmentManager.findFragmentByTag(OnBoardingFragment::class.java.canonicalName)
        if (fragment != null) {
            supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
    }
}