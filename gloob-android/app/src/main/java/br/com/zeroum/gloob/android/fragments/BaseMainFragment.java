package br.com.zeroum.gloob.android.fragments;

import android.os.Bundle;

import br.com.zeroum.gloob.android.activities.MainActivity;

/**
 * Created by zeroum on 7/16/15.
 */
public abstract class BaseMainFragment extends GloobFragment {
    protected MainActivity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }
}
