package br.com.zeroum.gloob.android.fragments.profile;

import android.os.Bundle;

import br.com.zeroum.gloob.android.activities.ProfileActivity;
import br.com.zeroum.gloob.android.fragments.GloobFragment;
import br.com.zeroum.gloob.android.models.Profile;

/**
 * Created by zeroum on 7/17/15.
 */
public abstract class BaseProfileFragment extends GloobFragment {
    protected ProfileActivity mActivity;
    protected Profile mCurrentProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ProfileActivity) getActivity();
        mCurrentProfile = Profile.getCurrentProfile();
    }
}
