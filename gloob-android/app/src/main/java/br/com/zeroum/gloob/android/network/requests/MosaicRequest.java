package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.objects.MosaicRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/15/15.
 */
public class MosaicRequest implements Response.Listener<JSONObject>, Response.ErrorListener {
    private BaseRequest request;
    private BroadcastManager broadcastManager;
    private MosaicRequestObject responseObject;

    public MosaicRequest(int pageIndex) {
        broadcastManager = new BroadcastManager();
        request = new JsonAuthenticatedRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getMosaicUrl(pageIndex), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_MOSAIC_ERROR);
    }

    @Override
    public void onResponse(JSONObject response) {
        MosaicRequestObject episodes = GsonProvider.getGson().fromJson(response.toString(), MosaicRequestObject.class);
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_MOSAIC_OK, episodes);
    }

    public BaseRequest getRequest() {
        return request;
    }
}
