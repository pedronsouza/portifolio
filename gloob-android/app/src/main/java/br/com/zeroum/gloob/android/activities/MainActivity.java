package br.com.zeroum.gloob.android.activities;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.CharactersFragment;
import br.com.zeroum.gloob.android.fragments.HomeFragment;
import br.com.zeroum.gloob.android.fragments.profile.NewProfileRegisterFragment;
import br.com.zeroum.gloob.android.fragments.profile.UserAlertsFragment;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.EventLog;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.animations.ResizeWidthAnimation;
import br.com.zeroum.gloob.android.support.ui.widget.GloobMenu;

public class MainActivity extends GloobActivity {
    public static final int ACTIVITY_CREATEPROFILE_REQUESTCODE = 101;
    public static final String START_AT_CHARACTERS_INTENT = "START_AT_CHARACTERS_INTENT";

    private boolean hasAvatarChanged;
    private boolean startAtCharacters;
    private GloobMenu mMenu;
    private View mOverlay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Profile.hasProfileInCurrentSession()) {
            startActivityForResult(new Intent(this, CreateProfileActivity.class), MainActivity.ACTIVITY_CREATEPROFILE_REQUESTCODE);
        }

        startAtCharacters = getIntent().getBooleanExtra(START_AT_CHARACTERS_INTENT, false);
        TrackManager.getInstance().trackPageView(trackablePageView);
        setupInterface();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTIVITY_CREATEPROFILE_REQUESTCODE) {
            hasAvatarChanged = (resultCode == NewProfileRegisterFragment.ACTIVITY_RESULT_NEW_PROFILE);
        }

        if (resultCode == UserAlertsFragment.ACTIVITY_RESULT_START_AT_CHARACTERS) {
            startAtCharacters = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Profile.hasProfileInCurrentSession()) {
            updateAvatar();
        } else {
            mMenu.setDefaultAvatar();
        }

        if (Session.isSignedIn()) {
            EventLog.getInstance().logAccessEvent();
            EventLog.getInstance().logDateEvent();
            EventLog.getInstance().logCarnivalRule();
            EventLog.getInstance().logMarkRule();
        }

        if (startAtCharacters) {
            startAtCharacters = false;
            mMenu.setCharactersSelected();
            changeToFragment(R.id.activity_main_frags_container, CharactersFragment.newInstance(), false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBroadcastManager.registerLocalBroadcastReceiver(startAtCharactersReceiver, BroadcastManager.Action.START_AT_CHARACTERS);
        mBroadcastManager.registerLocalBroadcastReceiver(changeAvatarReceiver, BroadcastManager.Action.CHANGE_AVATAR_RECEIVER);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBroadcastManager.unregisterLocalBroadcastReceiver(startAtCharactersReceiver);
        mBroadcastManager.unregisterLocalBroadcastReceiver(changeAvatarReceiver);
    }

    private void updateAvatar() {
        mMenu.setAvatar(Profile.getCurrentProfile().getCurrentAvatar());
    }

    private void setupInterface() {
        mMenu = Finder.findViewById(this, R.id.activity_main_buttons_holder);
        mOverlay = Finder.findViewById(this, R.id.activity_main_overlay);

        mOverlay.setOnClickListener(overlayClickListener);

        mMenu.setActivity(this);
        mMenu.setMenuExpandListener(menuExpandListener);

        if (startAtCharacters) {
            changeToFragment(R.id.activity_main_frags_container, CharactersFragment.newInstance(), false);
        } else {
            changeToFragment(R.id.activity_main_frags_container, HomeFragment.newInstance(), false);
        }


    }

    public int getContainerId() {
        return R.id.activity_main_frags_container;
    }

    private final GloobMenu.OnMenuExpandListener menuExpandListener = new GloobMenu.OnMenuExpandListener() {
        @Override
        public void onMenuExpand(boolean expanded) {
            if (expanded) {
                Animation partialFadeIn = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fadeout);
                partialFadeIn.setFillAfter(true);

                mOverlay.setVisibility(View.VISIBLE);
                mOverlay.startAnimation(partialFadeIn);
            } else {
                Animation partialFadeOut = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fadein);
                partialFadeOut.setFillAfter(false);
                mOverlay.startAnimation(partialFadeOut);
                mOverlay.setVisibility(View.GONE);
            }
        }
    };

    private final BroadcastReceiver startAtCharactersReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            changeToFragment(R.id.activity_main_frags_container, CharactersFragment.newInstance(), true);
        }
    };

    private final BroadcastReceiver changeAvatarReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mMenu.setAvatar(Profile.getCurrentProfile().getCurrentAvatar());
        }
    };

    private final View.OnClickListener overlayClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mMenu.menuExpandClickListener.onClick(v);
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/";
        }
    };
}
