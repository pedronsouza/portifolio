package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.Schedule;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/28/15.
 */
public class AlertView extends LinearLayout {
    private Program mProgram;
    private Schedule mSchedule;
    private SimpleDraweeView mImage;
    private GloobTextView mProgramTitle;
    private GloobTextView mEpisode;
    private GloobTextView mDate;
    private OnAlertRemoved onAlertRemoved;
    private Context mContext;

    public AlertView(Context context, Schedule schedule, Program program) {
        super(context);
        mSchedule = schedule;
        mProgram = program;
        init(context);
    }

    public AlertView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AlertView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AlertView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.alert_view, this);
        mImage = Finder.findViewById(this, R.id.alert_view_img);
        mProgramTitle = Finder.findViewById(this, R.id.alert_view_program);
        mEpisode = Finder.findViewById(this, R.id.alert_view_episode);
        mDate = Finder.findViewById(this, R.id.alert_view_date);

        findViewById(R.id.alert_view_bt_remove).setOnClickListener(removeAlert);

        mImage.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        int width = mImage.getMeasuredWidth();
        int height = mImage.getMeasuredHeight();

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(mProgram.getImage()))
                .setResizeOptions(new ResizeOptions(width, height))
                .setProgressiveRenderingEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(mImage.getController())
                .setImageRequest(request)
                .build();

        mImage.setController(controller);
        DateTime auxDate = new DateTime(mSchedule.getDate());

        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        mProgramTitle.setText(mProgram.getTitle());
        mEpisode.setText(mSchedule.getTitle());
        mDate.setText(sf.format(auxDate.toDate()));
    }

    public void setOnAlertRemoved(OnAlertRemoved onAlertRemoved) {
        this.onAlertRemoved = onAlertRemoved;
    }

    public interface OnAlertRemoved {
        void hasBeenRemoved();
    }

    private final OnClickListener removeAlert = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setMessage("Deseja mesmo remover este alerta?");
            dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    TrackManager.getInstance().trackEvent(removeAlertEvent);
                    mSchedule.unsetAlarm();

                    if (onAlertRemoved != null) {
                        onAlertRemoved.hasBeenRemoved();
                    }

                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    private final TrackManager.ITrackableEvent removeAlertEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Remover Alerta";
        }

        @Override
        public String getLabelForTrack() {
            return String.format(Urls.slugify(mSchedule.getProgramTitle()));
        }
    };
}
