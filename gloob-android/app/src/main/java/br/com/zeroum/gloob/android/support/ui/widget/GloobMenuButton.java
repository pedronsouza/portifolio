package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.GloobFragment;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/13/15.
 */
public class GloobMenuButton extends LinearLayout {
    private Context mContext;
    private String mText;
    private Drawable mIcon;
    private GloobFragment mFragment;

    public ImageButton mButton;
    private GloobTextView mLabel;

    public GloobMenuButton(Context context) {
        super(context);
        init(context, null);
    }

    public GloobMenuButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GloobMenuButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GloobMenuButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mContext = context;
        inflate(mContext, R.layout.gloob_menu_button, this);

        mButton = Finder.findViewById(this, R.id.gloob_menu_button_avatar);
        mLabel = Finder.findViewById(this, R.id.gloob_menu_button_avatar_title);

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.GloobMenuButton, 0, 0);
            mText = a.getString(R.styleable.GloobMenuButton_text);
            mIcon = a.getDrawable(R.styleable.GloobMenuButton_src);
        }

        if (mText != null) {
            mLabel.setText(mText);
        }

        if (mIcon != null) {
            mButton.setImageDrawable(mIcon);
        }
    }

    public void setExpanded(boolean expanded) {
        int visibility = (expanded) ? View.VISIBLE : View.GONE;
        mLabel.setVisibility(visibility);
    }

    public String getText() {
        return mText;
    }

    public void setText(String mText) {
        this.mText = mText;
        mLabel.setText(mText);
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public void setIcon(Drawable mIcon) {
        this.mIcon = mIcon;
        mButton.setImageDrawable(mIcon);
    }

    public void setFragment(GloobFragment fragment) {
        mFragment = fragment;
    }

    public GloobFragment getFragment() {
        return mFragment;
    }

    @Override
    public void setSelected(boolean selected) {
        mButton.setSelected(selected);
    }

    @Override
    public void setTag(Object tag) {
        super.setTag(tag);
        mButton.setTag(tag);
        mLabel.setTag(tag);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        mButton.setOnClickListener(l);
        mLabel.setOnClickListener(l);
    }
}
