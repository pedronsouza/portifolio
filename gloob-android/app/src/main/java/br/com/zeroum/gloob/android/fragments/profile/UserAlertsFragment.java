package br.com.zeroum.gloob.android.fragments.profile;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;
import java.util.Set;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.DispatcherActivity;
import br.com.zeroum.gloob.android.activities.MainActivity;
import br.com.zeroum.gloob.android.activities.PlayerActivity;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.Schedule;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.ui.widget.AlertView;

public class UserAlertsFragment extends BaseProfileFragment {
    public static final int ACTIVITY_RESULT_START_AT_CHARACTERS = 102;
    private LinearLayout mEmptyState;
    private LinearLayout mAlertsView;
    private LinearLayout mAlertsHolder;

    public static UserAlertsFragment newInstance() {
        UserAlertsFragment fragment = new UserAlertsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public UserAlertsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_user_alerts;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEmptyState = Finder.findViewById(mView, R.id.fragment_user_alerts_empty_state_holder);
        mAlertsView = Finder.findViewById(mView, R.id.fragment_user_alerts_view);
        mAlertsHolder = Finder.findViewById(mView, R.id.fragment_user_alerts_holder);

        mView.findViewById(R.id.fragment_user_alerts_see_all_chars).setOnClickListener(seeAllCharactersClickListener);
        setupAlerts();
    }

    private void setupAlerts() {
        List<Schedule> allAlerts = Schedule.getAllAlerts();

        if (allAlerts.size() > 0) {
            mEmptyState.setVisibility(View.GONE);
            mAlertsView.setVisibility(View.VISIBLE);

            mAlertsHolder.removeAllViews();

            for (Schedule alert : allAlerts) {
                Program program = findProgramByName(alert.getProgramTitle());
                if (program != null) {
                    AlertView alertView = new AlertView(getActivity(), alert, program);
                    alertView.setOnAlertRemoved(onAlertRemoved);
                    mAlertsHolder.addView(alertView);
                }
            }

        } else {
            mEmptyState.setVisibility(View.VISIBLE);
            mAlertsView.setVisibility(View.GONE);
        }
    }

    private Program findProgramByName(String programName) {
        List<Program> programs = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_PROGRAMS_KEY, Program.class);
        for (Program p : programs) {
            if (p.getTitle().equals(programName)) {
                return p;
            }
        }

        return null;
    }

    private final View.OnClickListener seeAllCharactersClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
            mainIntent.putExtra(MainActivity.START_AT_CHARACTERS_INTENT, true);
            startActivity(mainIntent);
        }
    };

    private final AlertView.OnAlertRemoved onAlertRemoved = new AlertView.OnAlertRemoved() {
        @Override
        public void hasBeenRemoved() {
            setupAlerts();
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/alerta";
        }
    };
}
