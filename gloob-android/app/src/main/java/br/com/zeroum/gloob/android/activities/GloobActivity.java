package br.com.zeroum.gloob.android.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import br.com.zeroum.gloob.android.fragments.achievements.BaseAchievementUnlockedFragment;
import br.com.zeroum.gloob.android.models.achievements.Achievement;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.managers.VolleyManager;
import br.com.zeroum.gloob.android.network.receivers.Command;

import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;

/**
 * Created by zeroum on 7/13/15.
 */
public abstract class GloobActivity extends AppCompatActivity {
    protected View mContentView;
    protected VolleyManager mVolleyManager;
    protected BroadcastManager mBroadcastManager;

    protected abstract int getLayoutResourceId();

    public GloobActivity() {
        mVolleyManager = new VolleyManager();
        mBroadcastManager = new BroadcastManager();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mContentView = getWindow().getDecorView().findViewById(android.R.id.content);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni == null) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Oops");
            dialog.setMessage("Falha ao carregar as informaçoes do servidor. Verifique sua conexão com a internet e tente novamente.");
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            dialog.show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBroadcastManager.registerLocalBroadcastReceiver(badRequestErrorReceiver, BroadcastManager.Action.BAD_REQUEST_ERROR);
        mBroadcastManager.registerLocalBroadcastReceiver(newAchievementUnlocked, BroadcastManager.Action.NEW_ACHIEVEMENT_UNLOCKED);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBroadcastManager.unregisterLocalBroadcastReceiver(badRequestErrorReceiver);
        mBroadcastManager.unregisterLocalBroadcastReceiver(newAchievementUnlocked);
    }

    protected void changeToFragment(int containerId, Fragment fragment, boolean addToBackstack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment);

        if (addToBackstack)
            ft.addToBackStack(fragment.getClass().toString());

        ft.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    protected void changeToFragment(int containerId, Fragment fragment, boolean addToBackstack, int transaction) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment);

        if (addToBackstack)
            ft.addToBackStack(fragment.getClass().toString());

        ft.setTransitionStyle(transaction);
        ft.commit();
    }

    private BroadcastReceiver badRequestErrorReceiver = newDataBroadcastReceiver(new Command<Parcelable>() {
        @Override
        public void execute(Context context, Parcelable data) {

        }
    });

    private BroadcastReceiver newAchievementUnlocked = newDataBroadcastReceiver(new Command<Achievement>() {
        @Override
        public void execute(Context context, Achievement data) {
            Fragment frag = BaseAchievementUnlockedFragment.newInstance(data);

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(mContentView.getId(), frag);

            ft.commitAllowingStateLoss();
        }
    });
}
