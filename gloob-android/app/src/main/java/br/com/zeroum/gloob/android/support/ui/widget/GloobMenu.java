package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.CreateProfileActivity;
import br.com.zeroum.gloob.android.activities.MainActivity;
import br.com.zeroum.gloob.android.activities.ProfileActivity;
import br.com.zeroum.gloob.android.fragments.CharactersFragment;
import br.com.zeroum.gloob.android.fragments.GamesFragment;
import br.com.zeroum.gloob.android.fragments.HomeFragment;
import br.com.zeroum.gloob.android.fragments.SearchFragment;
import br.com.zeroum.gloob.android.fragments.VideosFragment;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.animations.ResizeWidthAnimation;

/**
 * Created by zeroum on 7/14/15.
 */
public class GloobMenu extends RelativeLayout {
    private static final int MENU_ANIM_DURATION = 100;
    private MainActivity mActivity;
    private View mExpandButton;
    private ImageButton mExpandArrow;
    private View mMenuTouchArea;
    private Context mContext;
    private GloobTextView mAvatarName;
    private GloobMenuButton mAvatarButton;
    private int[] mButtons;
    private ImageButton mLogoButton;
    private OnMenuExpandListener menuExpandListener;

    public GloobMenu(Context context) {
        super(context);
        init(context);
    }

    public GloobMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GloobMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GloobMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.gloob_menu, this);

        mExpandButton = Finder.findViewById(this, R.id.gloob_menu_bt_menu_expand);
        mExpandArrow = Finder.findViewById(this, R.id.gloob_menu_bt_expand_arrow);
        mAvatarButton = Finder.findViewById(this, R.id.gloob_menu_bt_avatar);
        mLogoButton = Finder.findViewById(this, R.id.gloob_menu_bt_logo);

        mButtons = new int[] { R.id.gloob_menu_bt_home, R.id.gloob_menu_bt_videos, R.id.gloob_menu_bt_games, R.id.gloob_menu_bt_programs, R.id.gloob_menu_bt_search, R.id.gloob_menu_bt_avatar};
        mMenuTouchArea = Finder.findViewById(this, R.id.activity_menu_expand_touch_area);

        for (int resId : mButtons) {
            findViewById(resId).setOnClickListener(menuButtonClickListener);
            findViewById(resId).setTag(resId);
        }

        findViewById(R.id.gloob_menu_bt_home).setSelected(true);

        mExpandButton.setOnClickListener(menuExpandClickListener);
        mExpandArrow.setOnClickListener(menuExpandClickListener);
        mAvatarButton.setOnClickListener(avatarClickListener);
        mLogoButton.setOnClickListener(menuButtonClickListener);

        if (!isInEditMode()) {
            if (Profile.hasProfileInCurrentSession() && Session.isSignedIn()) {
                mAvatarButton.setIcon(mContext.getResources().getDrawable(Profile.getCurrentProfile().getCurrentAvatar().getImageForHomeResourceId()));
                mAvatarButton.setText(mContext.getString(R.string.main_activity_avatar_menu_with_profile));
            }
        }

    }

    public void setAvatar(Avatar avatar) {
        mAvatarButton.setIcon(mContext.getResources().getDrawable(avatar.getImageForHomeResourceId()));
        mAvatarButton.setText(mContext.getString(R.string.activity_main_bt_avatar));
        int size = (int) mContext.getResources().getDimension(R.dimen.home_avatar_size);
        int margin = (int) mContext.getResources().getDimension(R.dimen.default_margin);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        params.leftMargin = margin;
        mAvatarButton.mButton.setLayoutParams(params);
    }

    public void setDefaultAvatar() {
        mAvatarButton.setIcon(mContext.getResources().getDrawable(R.drawable.hm_monstros));
        mAvatarButton.setText(mContext.getString(R.string.new_profile_first_step_create_avatar));
    }

    public void setActivity(MainActivity mActivity) {
        this.mActivity = mActivity;
    }

    private void setLabelsVisibility(boolean visible) {
        for (int resId : mButtons) {
            GloobMenuButton button = Finder.findViewById(this, resId);
            button.setExpanded(visible);
        }

    }

    private void unselectAll() {
        for (int resId : mButtons) {
            findViewById(resId).setSelected(false);
        }
    }

    private final View.OnClickListener avatarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mExpandArrow.isSelected()) {
                Class clazz = CreateProfileActivity.class;
                if (Profile.hasAnyProfile()) {
                    clazz = ProfileActivity.class;
                }

                mActivity.startActivityForResult(new Intent(mActivity, clazz), 0);
            } else {
                menuButtonClickListener.onClick(v);
            }
        }
    };

    public void setButtonsInteraction(boolean enabled) {
        for(int resId : mButtons) {
            findViewById(resId).setEnabled(enabled);
        }
    }

    public final View.OnClickListener menuExpandClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final boolean isMenuExpanded = mExpandArrow.isSelected();
            int width = R.dimen.activity_main_menu_expand;

            if (isMenuExpanded) {
                width = R.dimen.activity_main_menu_hide;
            }

            ResizeWidthAnimation resizeAnim = new ResizeWidthAnimation(mExpandButton, getResources().getDimension(width));
            resizeAnim.setDuration(MENU_ANIM_DURATION);
            resizeAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    if (mExpandArrow.isSelected()) { // onHide
                        mMenuTouchArea.setVisibility(View.VISIBLE);
                        setLabelsVisibility(false);
                    } else { // onShow
                        setButtonsInteraction(false);
                        mMenuTouchArea.setVisibility(View.GONE);
                    }

                    mExpandArrow.setSelected(!mExpandArrow.isSelected());
                    menuExpandListener.onMenuExpand(mExpandArrow.isSelected());
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (mExpandArrow.isSelected()) {
                        setLabelsVisibility(true);
                    }

                    setButtonsInteraction(true);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            mExpandButton.startAnimation(resizeAnim);
        }
    };

    private View.OnClickListener menuButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mExpandArrow.isSelected()) {
                int tag = (int) v.getTag();
                Fragment frag = null;
                if (tag == R.id.gloob_menu_bt_games) {
                    frag = GamesFragment.newInstance();
                }

                if (tag == R.id.gloob_menu_bt_search) {
                    frag = SearchFragment.newInstance();
                }

                if (tag == R.id.gloob_menu_bt_home || tag == R.id.gloob_menu_bt_logo) {
                    frag = HomeFragment.newInstance();
                }

                if (tag ==  R.id.gloob_menu_bt_videos) {
                    frag = VideosFragment.newInstance();
                }

                if (tag == R.id.gloob_menu_bt_programs) {
                    frag = CharactersFragment.newInstance();
                }

                if (frag != null) {
                    FragmentManager fm = mActivity.getSupportFragmentManager();
                    int containerId = mActivity.getContainerId();

                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(containerId, frag);
                    ft.addToBackStack(frag.getClass().toString());
                    ft.commit();
                }

                unselectAll();
                findViewById(tag).setSelected(true);
                menuExpandClickListener.onClick(v);

            } else {
                menuExpandClickListener.onClick(v);
            }
        }
    };

    public void setCharactersSelected() {
        unselectAll();
        findViewById(R.id.gloob_menu_bt_programs).setSelected(true);
    }

    public interface OnMenuExpandListener {
        void onMenuExpand(boolean expanded);
    }

    public void setMenuExpandListener(OnMenuExpandListener menuExpandListener) {
        this.menuExpandListener = menuExpandListener;
    }
}
