package br.com.zeroum.gloob.android.models;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.MainActivity;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 8/3/15.
 */
public class GloobNotificationManager {
    private static final String NOTIFICATION_FORMAT = "Dentro de instantes, EP.%d de %s no canal GLOOB";
    private static final String NOTIFICATION_TITLE = "Gloob";
    public void registerNotification(Schedule schedule) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        NotificationManager notificationManager = (NotificationManager) application().getSystemService(Context.NOTIFICATION_SERVICE);

        try {
            Date fireDate = format.parse(schedule.getDate());

            NotificationCompat.Builder builder = new NotificationCompat.Builder(application());
            builder.setSmallIcon(R.mipmap.ic_launcher);
            builder.setPriority(Notification.PRIORITY_HIGH);
            builder.setContentTitle(NOTIFICATION_TITLE);
            builder.setOnlyAlertOnce(true);

            builder.setContentText(String.format(NOTIFICATION_FORMAT, schedule.getNumber(), schedule.getProgramTitle()));
            builder.setWhen(fireDate.getTime());// TODO: Fix after test

            Intent notificationIntent = new Intent(application(), MainActivity.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(application());
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);

            notificationManager.notify(schedule.getId(), builder.build());
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void cancelNotification(int notificationId) {
        NotificationManager notificationManager = (NotificationManager) application().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);
    }

    public static GloobNotificationManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        public static final GloobNotificationManager INSTANCE = new GloobNotificationManager();
    }
}
