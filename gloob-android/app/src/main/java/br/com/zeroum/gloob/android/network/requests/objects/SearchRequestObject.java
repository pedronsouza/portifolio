package br.com.zeroum.gloob.android.network.requests.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.SearchResult;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;

/**
 * Created by zeroum on 7/27/15.
 */
public class SearchRequestObject implements Parcelable {
    @Expose
    private int count;
    @Expose
    private String next;
    @Expose
    private String previous;

    private List<SearchResult> programs;
    private List<SearchResult> episodes;

    public int getCount() {
        return count;
    }

    public String getNext() {
        return next;
    }

    public String getPrevious() {
        return previous;
    }

    public List<SearchResult> getPrograms() {
        return programs;
    }

    public List<SearchResult> getEpisodes() {
        return episodes;
    }

    public SearchRequestObject(JSONObject response, int pageIndex) throws JSONException {
        JSONArray mixedResults = response.getJSONArray("results");

        if (pageIndex == 1) {
            if (mixedResults.length() > 0) {
                JSONObject programsResults = mixedResults.getJSONObject(0);
                JSONObject episodesResults = mixedResults.getJSONObject(1);

                SearchResult[] programsArray = GsonProvider.getGson().fromJson(programsResults.getJSONArray("results").toString(), SearchResult[].class);
                SearchResult[] episodesArray = GsonProvider.getGson().fromJson(episodesResults.getJSONArray("results").toString(), SearchResult[].class);

                programs = Arrays.asList(programsArray);
                episodes = Arrays.asList(episodesArray);

                count = episodesResults.getInt("count");
                next = episodesResults.getString("next");
                previous = episodesResults.getString("previous");
            }
        } else {
            SearchResult[] episodesArray = GsonProvider.getGson().fromJson(mixedResults.toString(), SearchResult[].class);

            episodes = Arrays.asList(episodesArray);

            count = response.getInt("count");
            next = response.getString("next");
            previous = response.getString("previous");
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.count);
        dest.writeString(this.next);
        dest.writeString(this.previous);
        dest.writeTypedList(programs);
        dest.writeTypedList(episodes);
    }

    protected SearchRequestObject(Parcel in) {
        this.count = in.readInt();
        this.next = in.readString();
        this.previous = in.readString();
        this.programs = in.createTypedArrayList(SearchResult.CREATOR);
        this.episodes = in.createTypedArrayList(SearchResult.CREATOR);
    }

    public static final Parcelable.Creator<SearchRequestObject> CREATOR = new Parcelable.Creator<SearchRequestObject>() {
        public SearchRequestObject createFromParcel(Parcel source) {
            return new SearchRequestObject(source);
        }

        public SearchRequestObject[] newArray(int size) {
            return new SearchRequestObject[size];
        }
    };
}
