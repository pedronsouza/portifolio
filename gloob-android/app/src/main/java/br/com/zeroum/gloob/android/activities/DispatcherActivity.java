package br.com.zeroum.gloob.android.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.MostViewed;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.achievements.AchievementsManager;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.models.achievements.repositories.RulesRepository;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.receivers.Command;
import br.com.zeroum.gloob.android.network.receivers.ListCommand;
import br.com.zeroum.gloob.android.network.requests.objects.MosaicRequestObject;
import br.com.zeroum.gloob.android.network.requests.objects.ProgramsRequestObject;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.ui.widget.GloobButton;

import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;
import static br.com.zeroum.gloob.android.network.receivers.ListBroadcastReceiver.newListBroadcastReceiver;

public class DispatcherActivity extends GloobActivity {
    public static final String CACHED_PROGRAMS_KEY = "DispatcherActivity.CACHED_PROGRAMS_KEY";
    public static final String CACHED_GAMES_KEY = "DispatcherActivity.CACHED_GAMES_KEY";
    public static final String CACHED_MOSAIC_KEY = "DispatcherActivity.CACHED_MOSAIC_KEY";
    public static final String CACHED_MOSTVIEWD_KEY = "DispatcherActivity.CACHED_MOSTVIEWD_KEY";

    private GloobButton mReconectButton;
    private ProgressBar mProgressBar;
    private List<Game> mGames;
    private List<Episode> mMostViewds;
    private List<Episode> mMosaicEpisodes;
    private List<Program> mPrograms;

    private int mMosaicPageIndex = 1;
    private int mProgramsPageIndex = 1;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_dispatcher;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReconectButton = Finder.findViewById(this, R.id.activity_dispatcher_reconect);
        mProgressBar = Finder.findViewById(this, R.id.activity_dispatcher_progress);
        mReconectButton.setOnClickListener(reconectButtonClickListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBroadcastManager.registerLocalBroadcastReceiver(gamesBroadcastReceiver, BroadcastManager.Action.GET_GAMES_OK);
        mBroadcastManager.registerLocalBroadcastReceiver(mostViewedBroadcastReceiver, BroadcastManager.Action.GET_MOST_VIEWED_OK);
        mBroadcastManager.registerLocalBroadcastReceiver(mosaicBroadcastReceiver, BroadcastManager.Action.GET_MOSAIC_OK);
        mBroadcastManager.registerLocalBroadcastReceiver(programsBroadcastReceiver, BroadcastManager.Action.GET_PROGRAMS_OK);

        mBroadcastManager.registerLocalBroadcastReceiver(errorBroadcastReceiver, BroadcastManager.Action.GET_MOSAIC_ERROR);
        mBroadcastManager.registerLocalBroadcastReceiver(errorBroadcastReceiver, BroadcastManager.Action.GET_MOST_VIEWED_ERROR);
        mBroadcastManager.registerLocalBroadcastReceiver(errorBroadcastReceiver, BroadcastManager.Action.GET_PROGRAMS_ERROR);
        mBroadcastManager.registerLocalBroadcastReceiver(errorBroadcastReceiver, BroadcastManager.Action.GET_GAMES_ERROR);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBroadcastManager.unregisterLocalBroadcastReceiver(gamesBroadcastReceiver);
        mBroadcastManager.unregisterLocalBroadcastReceiver(mostViewedBroadcastReceiver);
        mBroadcastManager.unregisterLocalBroadcastReceiver(mosaicBroadcastReceiver);
        mBroadcastManager.unregisterLocalBroadcastReceiver(programsBroadcastReceiver);
        mBroadcastManager.unregisterLocalBroadcastReceiver(errorBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startDataWarmUp();
    }

    private void startDataWarmUp() {
        mVolleyManager.getGames();
        mProgressBar.setVisibility(View.VISIBLE);
        mReconectButton.setVisibility(View.GONE);
    }

    private void invalidateRequests() {
        mVolleyManager.cancelAllRequests();
        mReconectButton.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Oops");
        dialog.setMessage("Falha ao carregar as informaçoes do servidor. Verifique sua conexão com a internet e tente novamente.");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialog.show();
    }

    private void prefetchImages() {
        Uri uri;
        ImagePipeline pipeline = Fresco.getImagePipeline();

        for (Game game : mGames) {
            if (game.getImage() != null) {
                uri = Uri.parse(game.getImage());
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                        .setProgressiveRenderingEnabled(true)
                        .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                        .build();
                pipeline.prefetchToDiskCache(request, this);
            }
        }

        for (Episode episode : mMosaicEpisodes) {
            if (episode.getImage() != null) {
                uri = Uri.parse(episode.getImage());
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                        .setProgressiveRenderingEnabled(true)
                        .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                        .build();
                pipeline.prefetchToDiskCache(request, this);
            }
        }

        for (Episode mostViewed : mMostViewds) {
            if (mostViewed.getImage() != null) {
                uri = Uri.parse(mostViewed.getImage());
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                        .setProgressiveRenderingEnabled(true)
                        .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                        .build();
                pipeline.prefetchToDiskCache(request, this);
            }
        }

        for (Program program : mPrograms) {
            if (program.getImage() != null) {
                uri = Uri.parse(program.getImage());
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                        .setProgressiveRenderingEnabled(true)
                        .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                        .build();
                pipeline.prefetchToDiskCache(request, this);
            }
        }
    }

    private void finishWarmup() {
        SharedCache.getInstance().storeList(CACHED_GAMES_KEY, mGames, Game.class);
        SharedCache.getInstance().storeList(CACHED_MOSAIC_KEY, mMosaicEpisodes, Episode.class);
        SharedCache.getInstance().storeList(CACHED_MOSTVIEWD_KEY, mMostViewds, Episode.class);
        SharedCache.getInstance().storeList(CACHED_PROGRAMS_KEY, mPrograms, Program.class);

        prefetchImages();

        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(R.anim.fadeout, R.anim.fadein);
    }

    private final View.OnClickListener reconectButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startDataWarmUp();
        }
    };

    private final BroadcastReceiver gamesBroadcastReceiver = newListBroadcastReceiver(new ListCommand<Game>() {
        @Override
        public void execute(Context context, List<Game> games) {
            if (games != null) {
                mGames = games;
                mVolleyManager.getMostViewedVideos();
            }
        }
    });


    private final BroadcastReceiver mostViewedBroadcastReceiver = newListBroadcastReceiver(new ListCommand<Episode>() {
        @Override
        public void execute(Context context, List<Episode> list) {
            if (list != null) {
                mMostViewds = list;
                mVolleyManager.getMosaicData(mMosaicPageIndex);
            }
        }
    });

    private final BroadcastReceiver mosaicBroadcastReceiver = newDataBroadcastReceiver(new Command<MosaicRequestObject>() {
        @Override
        public void execute(Context context, MosaicRequestObject data) {
            if (data != null && data.getEpisodes() != null) {
                if (mMosaicEpisodes == null) {
                    mMosaicEpisodes = data.getEpisodes();
                } else {
                    mMosaicEpisodes.addAll(data.getEpisodes());
                }

                if (data.getNext() != null && mMosaicPageIndex < 3) {
                    mMosaicPageIndex++;
                    mVolleyManager.getMosaicData(mMosaicPageIndex);
                } else {
                    mVolleyManager.getPrograms(mProgramsPageIndex);
                }
            } else {
                invalidateRequests();
            }
        }
    });



    private final BroadcastReceiver programsBroadcastReceiver = newDataBroadcastReceiver(new Command<ProgramsRequestObject>() {
        @Override
        public void execute(Context context, ProgramsRequestObject data) {
            if (data != null && data.getPrograms() != null) {
                if (mPrograms == null) {
                    mPrograms = data.getPrograms();
                } else {
                    mPrograms.addAll(data.getPrograms());
                }

                if (data.getNext() != null) {
                    mProgramsPageIndex++;
                    mVolleyManager.getPrograms(mProgramsPageIndex);
                } else {
                    finishWarmup();
                }
            } else {
                invalidateRequests();
            }
        }
    });

    private final BroadcastReceiver errorBroadcastReceiver = newDataBroadcastReceiver(new Command<Parcelable>() {
        @Override
        public void execute(Context context, Parcelable data) {
            invalidateRequests();
        }
    });
}
