package br.com.zeroum.gloob.android.fragments;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.adapters.GridGamesAdapter;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.ExpandableHeightGridView;

public class GamesShortcutFragment extends GloobFragment {
    private static final String GAMES_LIST_KEY = "GamesShortcutFragment.GAMES_LIST_KEY";
    private static final String PROGRAM_KEY = "GamesShortcutFragment.PROGRAM_KEY";
    private final BroadcastManager broadcastManager;
    private List<Game> mGames;
    private ExpandableHeightGridView mGridView;
    private GridGamesAdapter mGridAdapter;
    private Program mProgram;

    public static GamesShortcutFragment newInstance(ArrayList<Game> games, Program program) {
        GamesShortcutFragment fragment = new GamesShortcutFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(GAMES_LIST_KEY, games);
        args.putParcelable(PROGRAM_KEY, program);
        fragment.setArguments(args);
        return fragment;
    }

    public GamesShortcutFragment() {
        broadcastManager = new BroadcastManager();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGames = getArguments().getParcelableArrayList(GAMES_LIST_KEY);
            mProgram = getArguments().getParcelable(PROGRAM_KEY);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_games_shortcut;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGridView = Finder.findViewById(mView, R.id.fragment_games_shortcut_gridview);
        mView.findViewById(R.id.fragment_games_shortcut_back_video).setOnClickListener(keepPlayingClickListener);

        mGridAdapter = new GridGamesAdapter(getActivity(), mGames, mProgram);
        mGridView.setAdapter(mGridAdapter);
        mGridView.setExpanded(true);
    }

    private final View.OnClickListener keepPlayingClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            broadcastManager.sendLocalBroadcast(BroadcastManager.Action.START_PLAYING);
            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.beginTransaction().remove(GamesShortcutFragment.this).commit();
        }
    };
}
