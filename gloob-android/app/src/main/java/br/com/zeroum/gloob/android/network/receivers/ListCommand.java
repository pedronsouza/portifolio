package br.com.zeroum.gloob.android.network.receivers;

import android.content.Context;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by zeroum on 4/13/15.
 */
public interface ListCommand<T extends Parcelable> {

    void execute(Context context, List<T> list);

}
