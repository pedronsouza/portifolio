package br.com.zeroum.gloob.android.models.achievements.rules;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.zeroum.gloob.android.models.Profile;

/**
 * Created by zeroum on 4/20/15.
 */
public class BirthdayRule extends BaseRule implements IRule{
    @Override
    public boolean isValid() {
        Profile profile = Profile.getCurrentProfile();
        DateFormat df = new SimpleDateFormat("dd/MM");
        return df.format(profile.getBirthday()).equals(df.format(new Date()));
    }

    @Override
    public boolean shouldMark() {
        return isValid();
    }
}
