package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

/**
 * Created by zeroum on 8/4/15.
 */
public class GloobHorizontalScrollView extends HorizontalScrollView {
    private OnScrollChange onScrollChange;
    public GloobHorizontalScrollView(Context context) {
        super(context);
    }

    public GloobHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GloobHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GloobHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {

        if (onScrollChange != null) {
            onScrollChange.onScroll(l, t);
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }

    public void setOnScrollChange(OnScrollChange onScrollChange) {
        this.onScrollChange = onScrollChange;
    }

    public interface OnScrollChange {
        void onScroll(int x, int y);
    }
}
