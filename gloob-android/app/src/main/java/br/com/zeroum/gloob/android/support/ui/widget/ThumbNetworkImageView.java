package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/17/15.
 */
public class ThumbNetworkImageView extends RelativeLayout {
    private SimpleDraweeView mImageView;
    private GloobTextView mLabel;
    private View mDetailsHolder;

    private int mWidth;
    private int mHeight;

    public ThumbNetworkImageView(Context context) {
        super(context);
        init(context, null);
    }

    public ThumbNetworkImageView(Context context, int width, int height) {
        super(context);
        mWidth = width;
        mHeight = height;

        init(context, null);
    }

    public ThumbNetworkImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ThumbNetworkImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ThumbNetworkImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.thumb_network_imageview, this);

        mImageView = Finder.findViewById(this, R.id.gloob_network_imageview_img);
        mLabel = Finder.findViewById(this, R.id.video_thumb_imgview_label);
        mDetailsHolder = Finder.findViewById(this, R.id.video_thumb_imgview_details_holder);
    }

    public void setLabel(String lbl) {
        mLabel.setText(lbl);
        setDetailsVisibility(View.VISIBLE);
    }

    public void setDetailsVisibility(int visibility) {
        mDetailsHolder.setVisibility(visibility);
    }

    public void setImageUrl(String url) {
        Uri uri = Uri.parse(url);
        mImageView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int width = (mWidth == 0) ? mImageView.getMeasuredWidth() : mWidth;
        int height = (mHeight == 0) ? mImageView.getMeasuredHeight() : mHeight;

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions(width, height))
                .setProgressiveRenderingEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(mImageView.getController())
                .setImageRequest(request)
                .build();

        mImageView.setController(controller);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        mImageView.setOnClickListener(l);
    }

    @Override
    public void setTag(Object tag) {
        super.setTag(tag);
        mImageView.setTag(tag);
    }
}
