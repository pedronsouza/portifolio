package br.com.zeroum.gloob.android.fragments.profile;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.SelectAvatarView;

public class NewProfileRegisterFragment extends BaseProfileCreationFragment {
    public static int ACTIVITY_RESULT_NEW_PROFILE = 201;

    private LinearLayout mAvatarsHolder;
    private Avatar mCurrentAvatar;
    private DateTime mBirthdateValue;
    private EditText mBirthdate;
    private EditText mName;

    public static NewProfileRegisterFragment newInstance() {
        NewProfileRegisterFragment fragment = new NewProfileRegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public NewProfileRegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_new_profile_register;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<Avatar> defaultsAvatar = Avatar.getDefaulAvatars();
        mAvatarsHolder = Finder.findViewById(mView, R.id.new_profile_register_avatar_holder);
        mBirthdate = Finder.findViewById(mView, R.id.new_profile_register_birthday);
        mName = Finder.findViewById(mView, R.id.new_profile_register_name);
        int increment = 0;
        for (Avatar avatar: defaultsAvatar) {


            SelectAvatarView selectAvatarView = new SelectAvatarView(getActivity(), avatar);
            selectAvatarView.setOnSelectAvatarListener(selectAvatarListener);

            if (increment == 0) {
                selectAvatarView.setSelected(true);
                mCurrentAvatar = avatar;
            }

            mAvatarsHolder.addView(selectAvatarView);



            increment++;
        }

        mView.findViewById(R.id.new_profile_register_bt_create).setOnClickListener(registerClickListener);
        mBirthdate.setOnClickListener(birthdayDialogClickListenner);
    }


    private final SelectAvatarView.OnSelectAvatarListener selectAvatarListener = new SelectAvatarView.OnSelectAvatarListener() {
        @Override
        public void onSelect(Avatar avatar) {
            mCurrentAvatar = avatar;
        }
    };

    private final View.OnClickListener registerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean allValid = true;
            String name = mName.getText().toString();
            allValid = (name.length() > 1) && allValid;
            allValid = (mBirthdateValue != null && mCurrentAvatar != null);

            if (allValid) {
                Profile profile = new Profile();
                profile.setName(name);
                profile.setBirthDay(mBirthdateValue.toDate());
                profile.setCurrentAvatar(mCurrentAvatar);

                Profile.newProfile(profile);
                Profile.setCurrentProfile(profile);

                mActivity.setResult(ACTIVITY_RESULT_NEW_PROFILE);
                mActivity.finish();
            } else {
                Snackbar.make(mView, mActivity.getString(R.string.new_profile_register_validation_message), Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    private final View.OnClickListener birthdayDialogClickListenner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar c = Calendar.getInstance();

            int mYear = c.get(Calendar.YEAR) - 1;
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(mActivity, datePickerListener, mYear, mMonth, mDay);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            dialog.show();
        }
    };


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            DateTime dateTime = new DateTime(year, (month + 1), day, 0, 0);
            mBirthdateValue = dateTime;
            DateTimeFormatter df = DateTimeFormat.forPattern("dd/MM/yyyy");
            mBirthdate.setText(df.print(dateTime));
        }
    };
}
