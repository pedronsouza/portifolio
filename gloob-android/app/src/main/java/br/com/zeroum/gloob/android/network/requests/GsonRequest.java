package br.com.zeroum.gloob.android.network.requests;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by philipesteiff on 21/10/14.
 */
public class GsonRequest<T> extends BaseRequest<T> {

    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";

    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    private final Class<T> clazz;
    private final Map<String, String> headers;
    private final Map<String, String> params;
    private final Response.Listener<T> listener;
    private final Request.Priority priority;
    private final String url;

    private int SET_SOCKET_TIMEOUT = 20 * 1000;

    /**
     * * Make a request and return a parsed object from JSON.
     *
     * @param requestMethod {@link com.android.volley.Request.Method} Define o tipo da request
     * @param priority      {@link com.android.volley.Request.Priority} Seta a prioridade da request. Pode ser passado 'null'.
     * @param url           URL of the request to make
     * @param responseClazz Relevant class object, for Gson's reflection
     * @param params;       Parameters to be passed on request body
     * @param listener      Listener onde a resposta serÃ¡ entregue
     * @param errorListener Listener onde o erro serÃ¡ entregue
     */
    public GsonRequest(int requestMethod, Request.Priority priority, String url, Class<T> responseClazz, Map<String, String> params, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(requestMethod, url, errorListener);

        this.url = url; // Utilizado apenas para log
        this.priority = priority;
        this.clazz = responseClazz;
        this.params = params;
        this.listener = listener;
        this.headers = null;

        policySetup();

        Log.d("New -> url: ", this.url);
    }

    @Override
    public Request.Priority getPriority() {
        if (priority == null)
            return Request.Priority.NORMAL;
        else
            return priority;
    }

    @Override
    protected void deliverResponse(final T response) {
        super.deliverResponse(response);
        listener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data);
            return Response.success(GsonProvider.getGson().fromJson(jsonString, clazz), HttpHeaderParser.parseCacheHeaders(response));

        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("User-Agent", "Android native app");
        headers.put("Accept-Encoding", "gzip,deflate,sdch");
        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

    public void policySetup() {
        setRetryPolicy(new DefaultRetryPolicy(SET_SOCKET_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

}
