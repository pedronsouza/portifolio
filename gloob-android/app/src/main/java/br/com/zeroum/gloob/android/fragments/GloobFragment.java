package br.com.zeroum.gloob.android.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.managers.VolleyManager;

/**
 * Created by zeroum on 7/14/15.
 */
public abstract class GloobFragment extends Fragment {
    protected View mView;
    protected BroadcastManager mBroadcastManager;
    protected VolleyManager mVolleyManager;
    protected abstract int getLayoutResourceId();

    public GloobFragment() {
        mBroadcastManager = new BroadcastManager();
        mVolleyManager = new VolleyManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(getLayoutResourceId(), container, false);
        return mView;
    }

    protected void changeToFragment(int containerId, Fragment fragment, boolean addToBackstack) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment);

        if (addToBackstack)
            ft.addToBackStack(fragment.getClass().toString());

        ft.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    protected void changeToFragment(int containerId, Fragment fragment, boolean addToBackstack, int transaction) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment);

        if (addToBackstack)
            ft.addToBackStack(fragment.getClass().toString());

        ft.setTransitionStyle(transaction);
        ft.commit();
    }
}
