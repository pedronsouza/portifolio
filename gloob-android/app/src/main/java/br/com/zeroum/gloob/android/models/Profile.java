package br.com.zeroum.gloob.android.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import br.com.zeroum.gloob.android.GloobApplication;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;
import br.com.zeroum.gloob.android.support.GloobPreferences;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/20/15.
 */
public class Profile implements Parcelable {
    public static final Integer MAX_PROFILES = 3;
    private static final String ALLPROFILES_KEY = "Profile.ALLPROFILES_KEY_%s";
    private static final String CURRENTPROFILE_KEY = "Profile.CURRENTPROFILE_KEY_%s";
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("active")
    @Expose
    private boolean active;
    @SerializedName("avatar")
    @Expose
    private br.com.zeroum.gloob.android.models.achievements.Avatar avatar;
    @SerializedName("birthday")
    @Expose
    private Date birthDay;

    private static SharedPreferences mPrefs = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, Context.MODE_PRIVATE);
    private static SharedPreferences.Editor editor = mPrefs.edit();

    public static Profile getCurrentProfile() {
        String profileRaw = mPrefs.getString(getFormattedCurrentProfileKey(), null);
        if (profileRaw != null) {
            return GsonProvider.getGson().fromJson(profileRaw, Profile.class);
        }

        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthDay;
    }

    public void setBirthDay(Date date) {
        birthDay = date;
    }

    public Avatar getCurrentAvatar() {
        getCurrentProfile();
        return avatar;
    }

    public void setCurrentName(String name) {
        this.name = name;
    }

    public void save() {
        mPrefs.edit().putString(getFormattedCurrentProfileKey(), GsonProvider.getGson().toJson(this)).apply();
        List<Profile> profiles = getAllProfiles();
        Set<String> updatedProfiles = new HashSet<>();

        for (Profile p : profiles) {
            if (this.id.equals(p.getId())) {
                p = this;
            }

            updatedProfiles.add(GsonProvider.getGson().toJson(p));
        }
        mPrefs.edit().putStringSet(getFormattedAllProfilesKey(), updatedProfiles).commit();

    }

    public static List<Profile> getAllProfiles() {
        List<Profile> profiles = new ArrayList();
        Set<String> profilesRaw = new HashSet();

        profilesRaw = mPrefs.getStringSet(getFormattedAllProfilesKey(), profilesRaw);

        for (String profileRaw : profilesRaw) {
            Profile profile = GsonProvider.getGson().fromJson(profileRaw, Profile.class);
            profiles.add(profile);
        }

        return profiles;
    }

    public void setCurrentAvatar(Avatar currentAvatar) {
        this.avatar = currentAvatar;
    }

    public static boolean newProfile(Profile newProfile) {
        SharedPreferences.Editor editor = mPrefs.edit();
        Set<String> profiles = new HashSet();
        profiles = mPrefs.getStringSet(getFormattedAllProfilesKey(), profiles);

        if (profiles.size() >= 3) {
            return false;
        } else {
            newProfile.setId(UUID.randomUUID().toString());
            String profileJSON = GsonProvider.getGson().toJson(newProfile);
            profiles.add(profileJSON);
            setCurrentProfile(newProfile);
            return editor.putStringSet(getFormattedAllProfilesKey(), profiles).commit();
        }
    }

    public static boolean setCurrentProfile(Profile currentProfile) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(getFormattedCurrentProfileKey(), GsonProvider.getGson().toJson(currentProfile));
        return editor.commit();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeByte(active ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.avatar, 0);
        dest.writeLong(birthDay != null ? birthDay.getTime() : -1);
    }

    public Profile() {
    }

    protected Profile(Parcel in) {
        this.id =  in.readString();
        this.name = in.readString();
        this.active = in.readByte() != 0;
        this.avatar = in.readParcelable(Avatar.class.getClassLoader());
        long tmpBirthDay = in.readLong();
        this.birthDay = tmpBirthDay == -1 ? null : new Date(tmpBirthDay);
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    public static final void deleteProfile(Profile profile) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(getFormattedCurrentProfileKey());

        Set<String> profilesRaw = mPrefs.getStringSet(getFormattedAllProfilesKey(), null);

        if (profilesRaw != null) {
            for (int i = 0; i < profilesRaw.size(); i++) {
                String p = (String) profilesRaw.toArray()[i];
                Profile pro = GsonProvider.getGson().fromJson(p, Profile.class);

                if (pro.getId().equals(profile.getId())) {
                    profilesRaw.remove(p);
                }
            }

            if (profilesRaw.size() > 0) {
                String p = (String) profilesRaw.toArray()[0];
                Profile newProfile = GsonProvider.getGson().fromJson(p, Profile.class);
                setCurrentProfile(newProfile);
            }
        }
    }

    private static String getFormattedCurrentProfileKey() {
        Session currentSession = Session.getCurrentSession();
        if (Session.isSignedIn() && currentSession != null) {
            return String.format(CURRENTPROFILE_KEY, currentSession.getAccessToken());
        }
        return CURRENTPROFILE_KEY;
    }

    private static String getFormattedAllProfilesKey() {
        Session currentSession = Session.getCurrentSession();
        if (Session.isSignedIn() && currentSession != null) {
            return String.format(ALLPROFILES_KEY, currentSession.getAccessToken());
        }
        return ALLPROFILES_KEY;
    }

    public static boolean hasAnyProfile() {
        return getAllProfiles().size() > 0;
    }

    public static boolean hasProfileInCurrentSession() {
        return getCurrentProfile() != null;
    }

    public static void removeProfile(Profile profile) {
        List<Profile> allProfiles = getAllProfiles();
        Set<String> profilesRaw = new HashSet();

        for (Profile p : allProfiles) {
            if (!p.getId().equals(profile.getId())) {
                profilesRaw.add(GsonProvider.getGson().toJson(p, Profile.class));
            }
        }

        mPrefs.edit().putStringSet(getFormattedAllProfilesKey(), profilesRaw).commit();
    }

    public static void removeCurrentProfile() {
        mPrefs.edit().remove(getFormattedCurrentProfileKey()).apply();
    }
}
