package br.com.zeroum.gloob.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.activities.DispatcherActivity;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.ui.widget.GenericGloobList;

/**
 * Created by zeroum on 4/8/15.
 */
public class Program implements Parcelable, GenericGloobList.IGenericListItem {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("alternative_poster_image")
    @Expose
    private String image;
    @SerializedName("content_rating")
    @Expose
    private String contentRating;
    @SerializedName("linear")
    @Expose
    private Boolean linear;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("seasonsCount")
    @Expose
    private Integer seasonsCount;
    @SerializedName("episodesCount")
    @Expose
    private Integer episodesCount;
    @SerializedName("description")
    @Expose
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContentRating() {
        return contentRating;
    }

    public void setContentRating(String contentRating) {
        this.contentRating = contentRating;
    }

    public Boolean getLinear() {
        return linear;
    }

    public void setLinear(Boolean linear) {
        this.linear = linear;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSeasonsCount() {
        return seasonsCount;
    }

    public void setSeasonsCount(Integer seasonsCount) {
        this.seasonsCount = seasonsCount;
    }

    public Integer getEpisodesCount() {
        return episodesCount;
    }

    public void setEpisodesCount(Integer episodesCount) {
        this.episodesCount = episodesCount;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Program getCachedProgramById(int id) {
        List<Program> programs = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_PROGRAMS_KEY, Program.class);
        Program program = null;

        if (programs != null) {
            for (Program p : programs) {
                if (p.getId() == id) {
                    program = p;
                    break;
                }
            }
        }

        return program;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.image);
        dest.writeString(this.contentRating);
        dest.writeValue(this.linear);
        dest.writeString(this.title);
        dest.writeValue(this.seasonsCount);
        dest.writeValue(this.episodesCount);
        dest.writeString(this.description);
    }

    protected Program(Parcel in) {
        this.id = in.readInt();
        this.image = in.readString();
        this.contentRating = in.readString();
        this.linear = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.title = in.readString();
        this.seasonsCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.episodesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.description = in.readString();
    }

    public static final Creator<Program> CREATOR = new Creator<Program>() {
        public Program createFromParcel(Parcel source) {
            return new Program(source);
        }

        public Program[] newArray(int size) {
            return new Program[size];
        }
    };

    @Override
    public String getImageUrl() {
        return this.image;
    }

    @Override
    public String getLabel() {
        return this.title;
    }

    @Override
    public Object getValueForTag() {
        return this.id;
    }
}
