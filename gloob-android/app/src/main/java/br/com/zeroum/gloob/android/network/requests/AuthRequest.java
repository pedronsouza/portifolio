package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.objects.AuthRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/15/15.
 */
public class AuthRequest implements Response.Listener<JSONObject>, Response.ErrorListener {
    private BaseRequest request;
    private final BroadcastManager broadcastManager;

    public AuthRequest(AuthRequestObject requestObject) {
        final String params = GsonProvider.getGson().toJson(requestObject);
        broadcastManager = new BroadcastManager();
        try {
            request = new JsonAuthenticatedRequest(Request.Method.POST, Request.Priority.IMMEDIATE, Urls.getTokenIssuerUrl(), GsonProvider.toMap(new JSONObject(params)), this, this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.BAD_REQUEST_ERROR);
    }

    @Override
    public void onResponse(JSONObject response) {
        Session session = GsonProvider.getGson().fromJson(response.toString(), Session.class);

        try {
            session.getUser().setProvider(response.getJSONObject("autorizador").getString("app"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_TOKEN_OK, session);
    }

    public BaseRequest getRequest() {
        return request;
    }
}
