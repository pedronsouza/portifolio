package br.com.zeroum.gloob.android.models.achievements.rules;

/**
 * Created by zeroum on 4/17/15.
 */
public interface IRule {
    boolean isValid();
    boolean shouldMark();
}
