package br.com.zeroum.gloob.android.network.requests.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.zeroum.gloob.android.models.Episode;

/**
 * Created by zeroum on 7/15/15.
 */
public class MosaicRequestObject implements Parcelable {
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("next")
    @Expose
    private String next;
    @SerializedName("results")
    @Expose
    private List<Episode> episodes;

    public int getCount() {
        return count;
    }
    public String getNext() {
        return this.next;
    }
    public List<Episode> getEpisodes() {
        return episodes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.count);
        dest.writeString(this.next);
        dest.writeTypedList(episodes);
    }

    public MosaicRequestObject() {
    }

    protected MosaicRequestObject(Parcel in) {
        this.count = in.readInt();
        this.next = in.readString();
        this.episodes = in.createTypedArrayList(Episode.CREATOR);
    }

    public static final Parcelable.Creator<MosaicRequestObject> CREATOR = new Parcelable.Creator<MosaicRequestObject>() {
        public MosaicRequestObject createFromParcel(Parcel source) {
            return new MosaicRequestObject(source);
        }

        public MosaicRequestObject[] newArray(int size) {
            return new MosaicRequestObject[size];
        }
    };
}
