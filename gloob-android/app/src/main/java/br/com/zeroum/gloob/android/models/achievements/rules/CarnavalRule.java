package br.com.zeroum.gloob.android.models.achievements.rules;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

/**
 * Created by zeroum on 4/20/15.
 */
public class CarnavalRule extends BaseRule implements IRule{
    private final LocalDateTime[] validDates;

    public CarnavalRule() {
        DateTimeFormatter pattern = DateTimeFormat.forPattern("dd/MM/yyyy");
        validDates = new LocalDateTime[] { LocalDateTime.parse("09/02/2016", pattern),
                LocalDateTime.parse("28/02/2017", pattern), LocalDateTime.parse("13/02/2018", pattern),
                LocalDateTime.parse("05/03/2019", pattern), LocalDateTime.parse("25/02/2020", pattern),
                LocalDateTime.parse("25/02/2021", pattern), LocalDateTime.parse("25/02/2022", pattern)
        };
    }

    @Override
    public boolean isValid() {
        LocalDateTime now = new LocalDateTime(new Date());
        DateTimeFormatter pattern = DateTimeFormat.forPattern("dd/MM/yyyy");

        for (LocalDateTime carnaval : validDates) {
            return (carnaval.toString(pattern).equals(now.toString(pattern)));
        }

        return false;
    }

    @Override
    public boolean shouldMark() {
        return isValid();
    }
}
