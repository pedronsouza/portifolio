package br.com.zeroum.gloob.android.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.adapters.ScheduleAdapter;
import br.com.zeroum.gloob.android.models.Schedule;
import br.com.zeroum.gloob.android.support.Finder;

public class ScheduleActivity extends GloobActivity {
    public static final String SCHEDULE_KEY = "ScheduleActivity.";
    private List<Schedule> mSchedule;
    private ListView mScheduleList;
    private ScheduleAdapter mAdapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_schedule;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSchedule = getIntent().getParcelableArrayListExtra(SCHEDULE_KEY);
        mScheduleList = Finder.findViewById(this, R.id.activity_schedule_list);

        findViewById(R.id.activity_schedule_close).setOnClickListener(closeClickListener);
        mAdapter = new ScheduleAdapter(this, mSchedule);
        mScheduleList.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
