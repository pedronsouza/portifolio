package br.com.zeroum.gloob.android.network.requests;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.zeroum.gloob.android.activities.ProgramActivity;
import br.com.zeroum.gloob.android.models.Schedule;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.managers.VolleyManager;
import br.com.zeroum.gloob.android.network.requests.objects.ScheduleRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 4/29/15.
 */
public class ScheduleRequest implements Response.Listener<String>, Response.ErrorListener {

    private Request request;
    private String query;
    private int fiveDaysRequest = 0;
    private BroadcastManager localBroadcastManager;
    private VolleyManager mVolleyManager;
    private List<Schedule> fullSchedules = new ArrayList();

    public ScheduleRequest(int extraDay, String program) {

        query = program;

        fiveDaysRequest = extraDay;

        Date today = new Date();
        Date requestDay;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, extraDay);
        requestDay = calendar.getTime();

        localBroadcastManager = new BroadcastManager();
        mVolleyManager = new VolleyManager();
        request = new XMLAuthenticatedRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getScheduleUrl(requestDay), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {

    }

    @Override
    public void onResponse(String response) {

        JSONObject jsonObj = null;
        try {
            jsonObj = XML.toJSONObject(response);
        } catch (JSONException e) {
            Log.e("JSON exception", e.getMessage());
            e.printStackTrace();
        }

        ScheduleRequestObject scheduleObj = new ScheduleRequestObject();
        scheduleObj = scheduleObj.parseResponse(jsonObj, query);

        ProgramActivity.fullSchedules.addAll(scheduleObj.getSchedule());

        if(fiveDaysRequest<5){
            fiveDaysRequest++;
            mVolleyManager.getSchedule(fiveDaysRequest, query);
        }else{

            ArrayList<Schedule> filteredArray = new ArrayList<>();

            for(Schedule sch : ProgramActivity.fullSchedules){
                boolean duplicate = false;

                for(int s = 0 ; s<filteredArray.size() ; s++){
                    if(filteredArray.get(s).getDate().equals(sch.getDate())) duplicate = true;
                }

                if(!duplicate) {
                    filteredArray.add(sch);
                }
            }

            ProgramActivity.fullSchedules = new ArrayList<>();
            ProgramActivity.fullSchedules.addAll(filteredArray);

            localBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_SCHEDULE_OK, ProgramActivity.fullSchedules);
        }
    }

    public Request getRequest() {
        return request;
    }
}
