package br.com.zeroum.gloob.android.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import br.com.zeroum.gloob.android.fragments.GloobFragment;
import br.com.zeroum.gloob.android.fragments.profile.SelfieListFragment;

/**
 * Created by zeroum on 7/21/15.
 */
public class FragmentViewPagerAdapter extends FragmentPagerAdapter {
    private List<GloobFragment> selfies;

    public FragmentViewPagerAdapter(FragmentManager fm, List<GloobFragment> fragments) {
        super(fm);
        this.selfies = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return selfies.get(position);
    }

    @Override
    public int getCount() {
        return selfies.size();
    }
}
