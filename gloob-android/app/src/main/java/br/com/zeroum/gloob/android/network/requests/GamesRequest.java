package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.objects.GamesRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/15/15.
 */
public class GamesRequest implements Response.Listener<JSONObject>, Response.ErrorListener {

    private Request request;
    private BroadcastManager localBroadcastManager;

    public GamesRequest() {
        localBroadcastManager = new BroadcastManager();
        request = new JsonAuthenticatedRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getGamesUrl(), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        localBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_GAMES_ERROR);
    }

    @Override
    public void onResponse(JSONObject json) {
        List<Game> games = new ArrayList<>();
        try {
            Game[] results = GsonProvider.getGson().fromJson(json.getJSONArray("results").toString(), Game[].class);

            for (Game g : results)
                games.add(g);

            localBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_GAMES_OK, games);
        } catch (JSONException e) {
            localBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_GAMES_ERROR);
        }

    }

    public Request getRequest() {
        return request;
    }
}
