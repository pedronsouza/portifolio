package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/21/15.
 */
public class SelfiesPagerControl extends LinearLayout {
    private int mTotalPages = 3;
    private Context mContext;
    private LinearLayout mBulletsHolder;
    private ImageButton mPreviousButton;
    private ImageButton mNextButton;
    private OnPagination mOnPagination;
    private int mCurrentPage;

    public SelfiesPagerControl(Context context, int totalPages) {
        super(context);
        mTotalPages = totalPages;
        init(context);
    }

    public SelfiesPagerControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SelfiesPagerControl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SelfiesPagerControl(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.pf_selfies_pager, this);
        mBulletsHolder = Finder.findViewById(this, R.id.pf_selfies_pager_bullets_holder);
        mPreviousButton = Finder.findViewById(this, R.id.pf_selfies_pager_previous);
        mNextButton = Finder.findViewById(this, R.id.pf_selfies_pager_next);
        mCurrentPage = 0;

        for (int i = 0; i < mTotalPages; i++) {
            ImageButton bullet = new ImageButton(mContext);
            bullet.setImageResource(R.drawable.pf_selfies_bullet);
            bullet.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));

            LinearLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 5, 0);
            params.gravity = Gravity.CENTER_VERTICAL;

            bullet.setTag(i);

            if (i == mCurrentPage) {
                bullet.setSelected(true);
            }

            mBulletsHolder.addView(bullet);
        }

        mPreviousButton.setOnClickListener(pagerButtonsClickListener);
        mNextButton.setOnClickListener(pagerButtonsClickListener);
    }

    public void setOnPagination(OnPagination mOnPagination) {
        this.mOnPagination = mOnPagination;
    }

    public void setTotalPages(int pages) {
        mTotalPages = pages;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public interface OnPagination {
        void onPage(int position);
    }

    public void setCurrentPage(int position) {
        mCurrentPage = position;
        setupBulletState();
    }

    private void setupBulletState() {
        for (int i = 0; i < mTotalPages; i++) {
            ImageView bullet = (ImageView) findViewWithTag(i);
            if (i == mCurrentPage) {
                bullet.setSelected(true);
            } else {
                bullet.setSelected(false);
            }
        }
    }

    private final OnClickListener pagerButtonsClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.pf_selfies_pager_previous) {
                if (mCurrentPage != 0) {
                    mCurrentPage--;
                }
            } else {
                if (mCurrentPage < (mTotalPages - 1)) {
                    mCurrentPage++;
                }
            }

            setupBulletState();
            mOnPagination.onPage(mCurrentPage);
        }
    };
}
