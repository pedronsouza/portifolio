package br.com.zeroum.gloob.android.support;

import android.content.SharedPreferences;

import br.com.zeroum.gloob.android.GloobApplication;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;

import static android.content.Context.MODE_PRIVATE;
import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 7/15/15.
 */
public class GloobPreferences {
    private final SharedPreferences mPreferences;

    private GloobPreferences() {
        mPreferences = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
    }

    public void putEntity(String key, Object entity) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(key, GsonProvider.getGson().toJson(entity));
        editor.apply();
    }

    public <T> T getEntity(String key, Class clazz) {
        String entityRaw = mPreferences.getString(key, null);

        if (entityRaw != null) {
            return (T) GsonProvider.getGson().fromJson(entityRaw, clazz);
        }

        return null;
    }

    public void removeEntity(String key) {
        mPreferences.edit().remove(key).apply();
    }

    private static class SingletonHolder {
        public static final GloobPreferences INSTANCE = new GloobPreferences();
    }

    public static GloobPreferences getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
