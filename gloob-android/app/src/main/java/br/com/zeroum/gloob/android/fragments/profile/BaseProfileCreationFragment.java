package br.com.zeroum.gloob.android.fragments.profile;

import android.os.Bundle;

import br.com.zeroum.gloob.android.activities.CreateProfileActivity;
import br.com.zeroum.gloob.android.fragments.GloobFragment;

/**
 * Created by zeroum on 7/14/15.
 */
public abstract class BaseProfileCreationFragment extends GloobFragment {
    protected CreateProfileActivity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (CreateProfileActivity) getActivity();
    }
}
