package br.com.zeroum.gloob.android.fragments.profile;


import android.os.Bundle;
import android.app.Fragment;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.GloobFragment;
import br.com.zeroum.gloob.android.fragments.SelfieDetailsFragment;
import br.com.zeroum.gloob.android.models.achievements.Selfie;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;

import static br.com.zeroum.gloob.android.GloobApplication.application;

public class SelfieListFragment extends GloobFragment {
    public static final String SELFIELIST_KEY = "SelfieListFragment.SELFIELIST_KEY";

    private List<Selfie> mSelfies;

    public static SelfieListFragment newInstance(Selfie[] selfieList) {
        SelfieListFragment fragment = new SelfieListFragment();
        Bundle args = new Bundle();
        args.putParcelableArray(SELFIELIST_KEY, selfieList);
        fragment.setArguments(args);
        return fragment;
    }

    public SelfieListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Selfie[] array = (Selfie[]) getArguments().getParcelableArray(SELFIELIST_KEY);
            mSelfies = Arrays.asList(array);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_selfie_list;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        for (int i = 1; i <= mSelfies.size(); i++) {
            String imgButtonResource = String.format("fragment_selfie_list_item_%d", i);
            String lblResouce = String.format("fragment_selfie_list_name_%d", i);
            String holderResource = String.format("fragment_selfie_list_item_holder_%d", i);

            int imgBtResId = getActivity().getResources().getIdentifier(imgButtonResource, "id", application().getPackageName());
            int lblResId = getActivity().getResources().getIdentifier(lblResouce, "id", application().getPackageName());
            int holderResId = getActivity().getResources().getIdentifier(holderResource, "id", application().getPackageName());

            ImageButton imgButton = Finder.findViewById(mView, imgBtResId);
            GloobTextView lbl = Finder.findViewById(mView, lblResId);
            LinearLayout holder = Finder.findViewById(mView, holderResId);

            int selfieIndex = (i - 1);
            Selfie selfie = mSelfies.get(selfieIndex);

            if (selfie.isUnlocked()) {
                selfie = AchievementsRepository.getInstance().getUnlockedSelfieById(selfie.getId());
                imgButton.setImageDrawable(selfie.getImageForUnlock());
            }

            lbl.setText(selfie.getTitle());
            imgButton.setOnClickListener(seeSelfieDetails);
            holder.setVisibility(View.VISIBLE);

            lbl.setTag(selfieIndex);
            imgButton.setTag(selfieIndex);
            holder.setTag(selfieIndex);
        }
    }

    private final View.OnClickListener seeSelfieDetails = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = (int) v.getTag();
            Selfie selfie = mSelfies.get(index);

            android.support.v4.app.Fragment frag = SelfieDetailsFragment.newInstance(selfie);
            View contentView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
            FragmentManager fm = getActivity().getSupportFragmentManager();

            fm.beginTransaction()
                    .add(contentView.getId(), frag)
                    .addToBackStack(frag.getClass().toString())
                    .commit();
        }
    };
}
