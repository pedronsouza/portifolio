package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/14/15.
 */
public class GloobPreviousArrowButton extends LinearLayout {
    private ImageButton mBackButton;

    public GloobPreviousArrowButton(Context context) {
        super(context);
        init(context);
    }

    public GloobPreviousArrowButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GloobPreviousArrowButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GloobPreviousArrowButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.bt_arrow_previous, this);
        mBackButton = Finder.findViewById(this, R.id.bt_arrow_previous_bt);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        mBackButton.setOnClickListener(l);
    }
}
