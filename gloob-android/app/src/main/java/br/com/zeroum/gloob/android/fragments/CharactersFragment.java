package br.com.zeroum.gloob.android.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.DispatcherActivity;
import br.com.zeroum.gloob.android.activities.ProgramActivity;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.ui.widget.GenericGloobList;
import br.com.zeroum.gloob.android.support.ui.widget.GenericListItem;

public class CharactersFragment extends GloobFragment {
    private LinearLayout mCharactersHolder;
    private List<GenericGloobList.IGenericListItem> mPrograms;

    public static CharactersFragment newInstance() {
        CharactersFragment fragment = new CharactersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_characters;
    }

    public CharactersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrograms = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_PROGRAMS_KEY, Program.class);

        Collections.sort(mPrograms, new Comparator<GenericGloobList.IGenericListItem>() {
            @Override
            public int compare(GenericGloobList.IGenericListItem obj1, GenericGloobList.IGenericListItem obj2) {
                return obj1.getLabel().compareTo(obj2.getLabel());
            }
        });

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCharactersHolder = Finder.findViewById(mView, R.id.fragment_characters_list_holder);
        GenericGloobList list = new GenericGloobList(getActivity(), mPrograms, itensClickListener, R.layout.generic_list_item_characters, new int []{ R.drawable.ps_eng0, R.drawable.ps_eng1, R.drawable.ps_eng2 });
        mCharactersHolder.addView(list);
    }

    private final View.OnClickListener itensClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int programId = (int) v.getTag();
            for (GenericGloobList.IGenericListItem p : mPrograms) {
                int tagVal = (int) p.getValueForTag();

                if (tagVal == programId) {
                    Intent programIntent = new Intent(getActivity(), ProgramActivity.class);
                    programIntent.putExtra(ProgramActivity.PROGRAM_KEY, (Program) p);

                    startActivity(programIntent);
                    break;
                }
            }
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/personagens";
        }
    };
}
