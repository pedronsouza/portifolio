package br.com.zeroum.gloob.android.models.achievements.factories;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by zeroum on 4/17/15.
 */
public class EventKeyFactory {
    private static final String KEY_FOR_VIEW = "EventKeyFactory.KEY_FOR_VIEW_%s_%s";
    private static final String KEY_FOR_ACCESS = "EventKeyFactory.KEY_FOR_ACCESS_%s";
    private static final String KEY_FOR_BIRTHDAY = "EventKeyFactory.KEY_FOR_BIRTHDAY_%s_%s";
    private static final String KEY_FOR_DATE = "EventKeyFactory.KEY_FOR_DATE_%s_%s";
    private static final String KEY_FOR_PLAY = "EventKeyFactory.KEY_FOR_PLAY_%s";
    private static final String KEY_FOR_VIDEO = "EventKeyFactory.KEY_FOR_VIDEO_%s_%s";
    private static final String KEY_FOR_CARNAVAL = "EventKeyFactory.KEY_FOR_CARNAVAL_%s_%s";
    private static final String KEY_FOR_SEARCH = "EventKeyFactory.KEY_FOR_SEARCH_%s";
    private static final String KEY_FOR_MARK = "EventKeyFactory.KEY_FOR_MARK_%s";
    private static final String KEY_FOR_BOOKMARK = "EventKeyFactory.KEY_FOR_BOOKMARK_%s";
    private static final String KEY_FOR_PLAYUNIQUE = "EventKeyFactory.KEY_FOR_PLAYUNIQUE_%s_%s";

    public String getKeyByRuleCondition(String ruleCondition, String... args) {
        String methodName = String.format("keyFor_%s", ruleCondition);
        Method method;
        try {

            method = this.getClass().getDeclaredMethod(methodName, String[].class);
            return (String)method.invoke(this, new Object[] { args });
        } catch (NoSuchMethodException e) {
            e.printStackTrace();

        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
    @SafeVarargs
    private final String keyFor_views(String... args) {
        return String.format(KEY_FOR_VIEW, args);
    }
    @SafeVarargs
    private final String keyFor_access(String... args) {
        return String.format(KEY_FOR_ACCESS, args);
    }
    @SafeVarargs
    private final String keyFor_birthday(String... args) {
        return String.format(KEY_FOR_BIRTHDAY, args);
    }
    @SafeVarargs
    private final String keyFor_date(String... args) {
        return String.format(KEY_FOR_DATE, args);
    }
    @SafeVarargs
    private final String keyFor_play(String... args) {
        return String.format(KEY_FOR_PLAY, args);
    }
    @SafeVarargs
    private final String keyFor_video(String... args) {
        return String.format(KEY_FOR_VIDEO, args);
    }
    @SafeVarargs
    private final String keyFor_carnaval(String... args) {
        return String.format(KEY_FOR_CARNAVAL, args);
    }
    @SafeVarargs
    private final String keyFor_mark(String... args) {
        return String.format(KEY_FOR_MARK, args);
    }
    @SafeVarargs
    private final String keyFor_bookmark(String... args) {
        return String.format(KEY_FOR_BOOKMARK, args);
    }
    @SafeVarargs
    private final String keyFor_search(String... args) { return String.format(KEY_FOR_SEARCH, args); }
    @SafeVarargs
    private final String keyFor_play_unique(String... args) {
        return String.format(KEY_FOR_PLAYUNIQUE, args);
    }

    private EventKeyFactory() {

    }

    private static class SingletonHolder {
        public static final EventKeyFactory INSTANCE = new EventKeyFactory();
    }

    public static EventKeyFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
