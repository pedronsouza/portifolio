package br.com.zeroum.gloob.android.models.achievements.rules;


import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.achievements.Event;
import br.com.zeroum.gloob.android.models.achievements.factories.EventKeyFactory;
import br.com.zeroum.gloob.android.models.achievements.repositories.EventsRepository;

/**
 * Created by zeroum on 4/17/15.
 */
public class MarkRule extends BaseRule implements IRule {
    @Override
    public boolean isValid() {
        String key = EventKeyFactory.getInstance().getKeyByRuleCondition(Condition.MARK.toString(), Profile.getCurrentProfile().getId());
        Event event = EventsRepository.getInstance().getEventByKey(key);
        if (event.getValue() == null) {
            return false;
        }

        double totalHits = (double) event.getValue();
        return totalHits >= eventAmount;
    }

    @Override
    public boolean shouldMark() {
        return !isValid();
    }
}
