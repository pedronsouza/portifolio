package br.com.zeroum.gloob.android.fragments.achievements;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.ProfileActivity;
import br.com.zeroum.gloob.android.models.achievements.Selfie;

public class NewSelfieUnlockedFragment extends BaseAchievementUnlockedFragment {
    public final String TAG = "NewSelfieUnlockedFragment";

    public NewSelfieUnlockedFragment() {
        // Required empty public constructor
    }

    public static NewSelfieUnlockedFragment newInstance(Selfie selfie) {
        NewSelfieUnlockedFragment fragment = new NewSelfieUnlockedFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_ACHIEVEMENT, selfie);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    protected int getLayoutResourceId() {
        return super.getLayoutResourceId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected Drawable imageForAchievement() {
        return mAchievement.toSelfie().getImageForUnlock();
    }

    @Override
    protected String textForMainButton() {
        return "VEJA SUA SELFIE";
    }

    @Override
    protected String textForMessage() {
        return getActivity().getString(R.string.new_selfie_message);
    }

    @Override
    protected void mainButtonClick() {
        close();
        startActivity(new Intent(getActivity(), ProfileActivity.class));
    }
}
