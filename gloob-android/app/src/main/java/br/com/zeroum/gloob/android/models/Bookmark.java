package br.com.zeroum.gloob.android.models;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.zeroum.gloob.android.GloobApplication;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;
import br.com.zeroum.gloob.android.support.GloobPreferences;

import static android.content.Context.MODE_PRIVATE;
import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 7/22/15.
 */
public class Bookmark implements Parcelable {
    private static final String ALL_BOOKMARKS_KEY = "ALL_BOOKMAKRS_KEY_%s";
    private static final String BOOKMARK_ENTITY_KEY = "BOOKMARK_ENTITY_KEY_%s_%d";

    @SerializedName("episode")
    @Expose
    private Episode episode;

    public Episode getEpisode() {
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

    public static boolean addEpisodeToBookmark(Episode episode) {
        if (Profile.hasAnyProfile() && Profile.hasProfileInCurrentSession()) {
            Bookmark bookmark = new Bookmark();
            bookmark.episode = episode;

            GloobPreferences.getInstance().putEntity(bookmark.getBookmarkFormatKey(), bookmark);
            addToAllBookmarks(bookmark);
            return true;
        }

        return false;
    }

    public static boolean removeFromBookmarks(Episode ep) {
        Bookmark bookmark = getBookmark(ep);
        GloobPreferences.getInstance().removeEntity(getBookmarkEntityKey(ep));
        removeFromAllBookmakrs(bookmark);
        return true;
    }

    public static boolean removeFromBookmarks(Bookmark bookmark) {
        GloobPreferences.getInstance().removeEntity(getBookmarkEntityKey(bookmark.getEpisode()));
        removeFromAllBookmakrs(bookmark);
        return true;
    }

    public static Bookmark getBookmark(Episode episode) {
        Bookmark bookmark = GloobPreferences.getInstance().getEntity(getBookmarkEntityKey(episode), Bookmark.class);
        return bookmark;
    }

    public String getBookmarkFormatKey() {
        return String.format(BOOKMARK_ENTITY_KEY, Profile.getCurrentProfile().getId(), (episode.getId() + episode.getProgram().getId()));
    }

    private static String getBookmarkEntityKey(Episode ep) {
        return String.format(BOOKMARK_ENTITY_KEY, Profile.getCurrentProfile().getId(), (ep.getId() + ep.getProgram().getId()));
    }

    private static void addToAllBookmarks(Bookmark bookmark) {
        SharedPreferences prefs = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
        Set<String> bookmarksSet = prefs.getStringSet(getAllBookmarksKeyFormatted(), new HashSet<String>());

        bookmarksSet.add(GsonProvider.getGson().toJson(bookmark));
        prefs.edit().putStringSet(getAllBookmarksKeyFormatted(), bookmarksSet).apply();
    }

    public static List<Bookmark> getAllBookmakrs() {
        SharedPreferences prefs = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
        List<Bookmark> bookmarks = new ArrayList();
        Set<String> bookmarksSet = prefs.getStringSet(getAllBookmarksKeyFormatted(), new HashSet<String>());

        for (String bookmarkRaw : bookmarksSet) {
            Bookmark b = GsonProvider.getGson().fromJson(bookmarkRaw, Bookmark.class);
            bookmarks.add(b);
        }

        return bookmarks;
    }

    private static void removeFromAllBookmakrs(Bookmark bookmark) {
        SharedPreferences prefs = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
        Set<String> bookmarksSet = prefs.getStringSet(getAllBookmarksKeyFormatted(), new HashSet<String>());

        for (String raw : bookmarksSet) {
            Bookmark b = GsonProvider.getGson().fromJson(raw, Bookmark.class);
            if (b.getEpisode().getId() == bookmark.getEpisode().getId()) {
                bookmarksSet.remove(raw);
                break;
            }
        }

        prefs.edit().putStringSet(getAllBookmarksKeyFormatted(), bookmarksSet).apply();
    }

    private static String getAllBookmarksKeyFormatted() {
        return String.format(ALL_BOOKMARKS_KEY, Profile.getCurrentProfile().getId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.episode, 0);
    }

    public Bookmark() {
    }

    protected Bookmark(Parcel in) {
        this.episode = in.readParcelable(Episode.class.getClassLoader());
    }

    public static final Parcelable.Creator<Bookmark> CREATOR = new Parcelable.Creator<Bookmark>() {
        public Bookmark createFromParcel(Parcel source) {
            return new Bookmark(source);
        }

        public Bookmark[] newArray(int size) {
            return new Bookmark[size];
        }
    };
}
