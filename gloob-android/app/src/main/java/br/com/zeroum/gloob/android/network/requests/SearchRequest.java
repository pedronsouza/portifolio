    package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.objects.SearchRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/27/15.
 */
public class SearchRequest implements Response.Listener<JSONObject>, Response.ErrorListener {
    private BroadcastManager broadcastManager;
    private BaseRequest request;
    private int pageIndex;

    public SearchRequest(String query, int pageIndex) {
        broadcastManager = new BroadcastManager();
        this.pageIndex = pageIndex;
        request = new JsonAuthenticatedRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getSearchUrl(pageIndex, query), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.BAD_REQUEST_ERROR);
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            SearchRequestObject searchRequestObject = new SearchRequestObject(response, pageIndex);
            broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_SEARCH_OK, searchRequestObject);
        } catch (JSONException e) {
            broadcastManager.sendLocalBroadcast(BroadcastManager.Action.BAD_REQUEST_ERROR);
            e.printStackTrace();
        }
    }

    public BaseRequest getRequest() {
        return request;
    }
}
