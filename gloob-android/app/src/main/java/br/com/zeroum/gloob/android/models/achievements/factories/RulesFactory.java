package br.com.zeroum.gloob.android.models.achievements.factories;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import br.com.zeroum.gloob.android.models.achievements.rules.AccessRule;
import br.com.zeroum.gloob.android.models.achievements.rules.BaseRule;
import br.com.zeroum.gloob.android.models.achievements.rules.BirthdayRule;
import br.com.zeroum.gloob.android.models.achievements.rules.BookmarkRule;
import br.com.zeroum.gloob.android.models.achievements.rules.CarnavalRule;
import br.com.zeroum.gloob.android.models.achievements.rules.DateRule;
import br.com.zeroum.gloob.android.models.achievements.rules.MarkRule;
import br.com.zeroum.gloob.android.models.achievements.rules.PlayRule;
import br.com.zeroum.gloob.android.models.achievements.rules.SearchRule;
import br.com.zeroum.gloob.android.models.achievements.rules.VideoRule;
import br.com.zeroum.gloob.android.models.achievements.rules.ViewsRule;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;


/**
 * Created by zeroum on 4/20/15.
 */
public class RulesFactory {
    private JSONObject ruleRaw;

    public BaseRule createRuleByCondition(String condition, JSONObject ruleRaw) {
        BaseRule rule = null;

        try {
            Method method = this.getClass().getDeclaredMethod(String.format("createRuleFor%s", condition));
            this.ruleRaw = ruleRaw;
            rule = (BaseRule) method.invoke(this);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return rule;
    }

    public BaseRule createRuleByCondition(String condition, String ruleRaw) {
        BaseRule rule = null;

        try {
            Method method = this.getClass().getDeclaredMethod(String.format("createRuleFor%s", condition));
            this.ruleRaw = new JSONObject(ruleRaw);
            rule = (BaseRule) method.invoke(this);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rule;
    }

    private MarkRule createRuleFormark() {
        return (MarkRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), MarkRule.class);
    }

    private AccessRule createRuleForaccess() {
        return (AccessRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), AccessRule.class);
    }

    private SearchRule createRuleForsearch() {
        return (SearchRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), SearchRule.class);
    }

    private DateRule createRuleFordate() {
        return (DateRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), DateRule.class);
    }

    private CarnavalRule createRuleForcarnaval() {
        return (CarnavalRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), CarnavalRule.class);
    }

    private BirthdayRule createRuleForbirthday() {
        return (BirthdayRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), BirthdayRule.class);
    }

    private BookmarkRule createRuleForbookmark() {
        return (BookmarkRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), BookmarkRule.class);
    }

    private ViewsRule createRuleForviews() {
        return (ViewsRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), ViewsRule.class);
    }

    private PlayRule createRuleForplay() {
        return (PlayRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), PlayRule.class);
    }

    private VideoRule createRuleForvideo() {
        return (VideoRule) GsonProvider.getGson().fromJson(ruleRaw.toString(), VideoRule.class);
    }

    private static class SingletonHolder {
        public static final RulesFactory INSTANCE = new RulesFactory();
    }

    public static RulesFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
