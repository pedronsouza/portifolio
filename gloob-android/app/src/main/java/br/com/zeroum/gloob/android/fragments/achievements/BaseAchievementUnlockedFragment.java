package br.com.zeroum.gloob.android.fragments.achievements;

import android.content.BroadcastReceiver;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.GloobFragment;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.Achievement;
import br.com.zeroum.gloob.android.models.achievements.AchievementsManager;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;


public abstract class BaseAchievementUnlockedFragment extends GloobFragment {
    protected BroadcastManager mBroadcastManager;
    private static BaseAchievementUnlockedFragment mInstance;
    public final String TAG = "BaseAchievementUnlockedFragment";
    public static final String BUNDLE_ACHIEVEMENT = "NewSelfieUnlockedFragment.BUNDLE_ACHIEVEMENT";
    protected Achievement mAchievement;
    protected View mView;
    private ImageView mImage;
    private GloobTextView mTitle;
    private GloobTextView mSubtitle;
    private GloobTextView mDescription;
    private Button mMainButton;
    protected ImageButton mCloseButton;
    private GloobTextView mMessage;

    public static BaseAchievementUnlockedFragment newInstance(Achievement achievement) {
        BaseAchievementUnlockedFragment fragment = (achievement.isKindAvatar()) ? NewAvatarUnlockedFragment.newInstance(achievement.toAvatar()): NewSelfieUnlockedFragment.newInstance(achievement.toSelfie());

        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_ACHIEVEMENT, achievement);
        fragment.setArguments(args);
        mInstance = fragment;
        return fragment;
    }

    public BaseAchievementUnlockedFragment() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_new_achievement_unlocked;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBroadcastManager = new BroadcastManager();
        if (getArguments() != null) {
            mAchievement = getArguments().getParcelable(BUNDLE_ACHIEVEMENT);

            if (mAchievement.isKindSelfie()) {
                mAchievement = AchievementsRepository.getInstance().getUnlockedSelfieById(mAchievement.getId());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_new_achievement_unlocked, container, false);
        setupInterface();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        AchievementsManager.getInstance().removeFromQueue(mAchievement);
        TrackManager.getInstance().trackEvent(newAchievementEvent);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void setupInterface() {
        mImage = Finder.findViewById(mView, R.id.new_achievement_image);
        mTitle = Finder.findViewById(mView, R.id.new_achievement_title);
        mSubtitle = Finder.findViewById(mView, R.id.new_achievement_subtitle);
        mDescription = Finder.findViewById(mView, R.id.new_achievement_description);
        mMessage = Finder.findViewById(mView, R.id.fragment_achievement_message);
        mMainButton = Finder.findViewById(mView, R.id.new_achievement_main_button);
        mCloseButton = Finder.findViewById(mView, R.id.fragment_achievement_close);
        mCloseButton.setOnClickListener(closeButtonClickListener);

        mMainButton.setOnClickListener(openProfileListener);
        mMainButton.setText(textForMainButton());

        mImage.setImageDrawable(this.imageForAchievement());
        mTitle.setText(mAchievement.getTitle());
        mSubtitle.setText(mAchievement.getSubtitle());
        mDescription.setText(mAchievement.getDescriptionOn());
        mMessage.setText(textForMessage());
    }

    protected abstract String textForMainButton();
    protected abstract String textForMessage();
    protected abstract Drawable imageForAchievement();
    protected abstract void mainButtonClick();

    private View.OnClickListener openProfileListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mainButtonClick();
        }
    };

    protected View.OnClickListener closeButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            close();
        }
    };

    protected void close() {
        mBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.ACHIEVEMENT_UNLOCKED_DISMISSED, mAchievement);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.remove(BaseAchievementUnlockedFragment.this);
        ft.commit();
    }

    private TrackManager.ITrackableEvent newAchievementEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Prêmio - Tipo Premio";
        }

        @Override
        public String getLabelForTrack() {
            String avatarKind = (mAchievement.isKindAvatar()) ? "avatar" : "selfie";
            String event = String.format("%s|%s", avatarKind, Urls.slugify(mAchievement.getTitle()));
            return event;
        }
    };
}
