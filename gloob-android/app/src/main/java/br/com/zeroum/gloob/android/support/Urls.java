package br.com.zeroum.gloob.android.support;

import com.github.slugify.Slugify;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by zeroum on 7/13/15.
 */
public class Urls {
    private final static String BASE_URL = "https://api.vod.globosat.tv/globosatplay/%s.json";
    public final static String BASE_GAMES_URL = "http://api.apps.globosat.tv/gloobplay/%s";
    private final static String BASE_EPISODE_URL = "https://api.vod.globosat.tv/globosatplay/%s/%d?channel=gloob";
    private final static String BASE_PROGRAMS_URL = "https://api.vod.globosat.tv/globosatplay/%s.json?channel_id=1%s";
    private final static String LOGIN_URL = "https://auth.globosat.tv/oauth/token/";
    private final static String BASE_SCHEDULE_URL = "http://gradephp.canaisglobosat.globo.com/uploads/gradesdoscanais/grade_canal=GLOB_dia=%s";

    public static String getTokenIssuerUrl() {
        return LOGIN_URL;
    }

    public static String getGamesUrl() {
        return String.format(BASE_GAMES_URL, "games.json");
    }

    public static String getTop5() {
        return String.format(BASE_GAMES_URL, "top.json");
    }

    public static String getMosaicUrl(int pageIndex) {
        return String.format(BASE_PROGRAMS_URL, "episodes",String.format("&ordering=-first_publish_date&page=%d", pageIndex));
    }

    public static String getProgramsUrl(int pageIndex) {
        return String.format(BASE_PROGRAMS_URL, "programs",String.format("&page=%d", pageIndex));
    }

    public static String getSearchUrl(int pageIndex, String query) {
        if (pageIndex == 1) {
            String url = String.format(BASE_URL, "search");
            return String.format("%s?q=%s&channel=gloob", url, query);
        } else {
            String url = String.format(BASE_URL, "episodes");
            return String.format("%s?q=%s&page=%d&channel=gloob", url, query, pageIndex);
        }
    }

    public static String getProgramEpisodes(int programId, int pageIndex) {
        String baseUrl = String.format(BASE_URL, "episodes");
        return String.format("%s?program_id=%d&page=%d", baseUrl, programId, pageIndex);
    }

    public static String getScheduleUrl(Date request_day) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(request_day);

        String day = (calendar.get(Calendar.DAY_OF_MONTH) < 10) ? "0"+calendar.get(Calendar.DAY_OF_MONTH) : ""+calendar.get(Calendar.DAY_OF_MONTH);
        String month = (calendar.get(Calendar.MONTH) < 10) ? "0"+(calendar.get(Calendar.MONTH)+1) : ""+(calendar.get(Calendar.MONTH)+1);
        String year = ""+calendar.get(Calendar.YEAR);

        String baseUrl = String.format(BASE_SCHEDULE_URL, String.format("%s-%s-%s", day, month, year));
        return baseUrl;
    }

    public static String getEpisodeByIdUrl(int episodeId) {
        return String.format(BASE_EPISODE_URL, "episodes", episodeId);
    }

    public static String slugify(String toSlugify) {
        Slugify slg = null;
        try {
            slg = new Slugify();
            return slg.slugify(toSlugify);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return toSlugify;
    }
}
