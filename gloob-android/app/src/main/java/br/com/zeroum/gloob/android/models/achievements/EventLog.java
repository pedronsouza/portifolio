package br.com.zeroum.gloob.android.models.achievements;

import android.util.Log;

import java.util.Date;
import java.util.List;

import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.achievements.factories.EventKeyFactory;
import br.com.zeroum.gloob.android.models.achievements.repositories.EventsRepository;
import br.com.zeroum.gloob.android.models.achievements.repositories.RulesRepository;
import br.com.zeroum.gloob.android.models.achievements.rules.BaseRule;
import br.com.zeroum.gloob.android.models.achievements.rules.BookmarkRule;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;

/**
 * Created by zeroum on 4/17/15.
 */
public class EventLog {
    private static final String TAG = "EventLog:";
    private BroadcastManager broadcastManager;

    private EventLog() {
        broadcastManager = new BroadcastManager();
    }

    private static class SingletonHolder {
        public static final EventLog INSTANCE = new EventLog();
    }

    public static EventLog getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private void logIncrementalEvents(List<BaseRule> rules, String... args) {
        if (shouldMark()) {
            for (BaseRule rule : rules) {
                if (rule.shouldMark()) {
                    if (rule.getCondition().equals(BaseRule.Condition.VIEW.toString())) {
                        if (!rule.getProgramId().equals(args[1]))
                            continue;
                    }

                    String key = EventKeyFactory.getInstance().getKeyByRuleCondition(rule.getCondition(), args);
                    Event event = EventsRepository.getInstance().getEventByKey(key);

                    if (event.getValue() == null) {
                        event.setValue(new Double(1));
                        event.setCreatedAt(new Date());
                    } else {
                        double value = (double) event.getValue();
                        value += 1;
                        event.setValue(value);
                    }

                    event.setUpdatedAt(new Date());
                    event.setCondition(rule.getCondition());

                    if (EventsRepository.getInstance().saveEvent(key, event)) {
                        Log.d("EventLog:", String.format("Registering event for rule id%d and condition %s", rule.getId(), rule.getCondition()));
                        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.NEW_EVENT_REGISTERED, event);
                    }
                }
            }
        }
    }

    public void logVideoRule() {
        if (shouldMark()) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.VIDEO.toString());
            logIncrementalEvents(rules, String.valueOf(Profile.getCurrentProfile().getId()));
        }
    }

    public void logSearchRule() {
        if (shouldMark()) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.SEARCH.toString());
            logIncrementalEvents(rules, String.valueOf(Profile.getCurrentProfile().getId()));
        }
    }

    public void logProgramViews(Episode ep) {
        if (shouldMark()) {
            RulesRepository.getInstance().setupAchievementsForEachRules();
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.VIEW.toString());
            logIncrementalEvents(rules, String.valueOf(Profile.getCurrentProfile().getId()), String.valueOf(ep.getProgram().getId()));
        }
    }

    public void logCarnivalRule() {
        if (shouldMark()) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.CARNAVAL.toString());
            logIncrementalEvents(rules, String.valueOf(Profile.getCurrentProfile().getId()));
        }
    }

    public void logPlayEvent() {
        if (shouldMark()) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.PLAY.toString());
            logIncrementalEvents(rules, String.valueOf(Profile.getCurrentProfile().getId()));
        }
    }

    public void logMarkRule() {
        if (shouldMark()) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.MARK.toString());
            logIncrementalEvents(rules, String.valueOf(Profile.getCurrentProfile().getId()));
        }
    }

    public void logBookmarkRule() {
        if (shouldMark()) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.BOOKMARK.toString());
            for (BaseRule base : rules) {
                BookmarkRule rule = (BookmarkRule) base;
                if (rule.shouldMark()) {

                    String key = EventKeyFactory.getInstance().getKeyByRuleCondition(rule.getCondition(), String.valueOf(Profile.getCurrentProfile().getId()));
                    Event event = EventsRepository.getInstance().getEventByKey(key);
                    if (event.getValue() == null) {
                        event.setCreatedAt(new Date());
                    }

                    event.setValue(true);
                    event.setUpdatedAt(new Date());
                    event.setCondition(rule.getCondition());

                    if (EventsRepository.getInstance().saveEvent(key, event)) {
                        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.NEW_EVENT_REGISTERED, event);
                    }
                }
            }
        }
    }

    public void logAccessEvent() {
        if (shouldMark()) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.ACCESS.toString());
            logIncrementalEvents(rules, String.valueOf(Profile.getCurrentProfile().getId()));
        }
    }

    public void logDateEvent() {
        if ((shouldMark())) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.DATE.toString());
            rules.addAll(RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.CARNAVAL.toString()));
            rules.addAll(RulesRepository.getInstance().getRulesByCondition(BaseRule.Condition.BIRTHDAY.toString()));

            for (BaseRule rule : rules) {
                if (rule.shouldMark()) {
                    String key = EventKeyFactory.getInstance().getKeyByRuleCondition(rule.getCondition(), String.valueOf(Profile.getCurrentProfile().getId()), String.valueOf(rule.getId()));
                    Event event = EventsRepository.getInstance().getEventByKey(key);

                    if (event.getValue() == null) {
                        event.setCreatedAt(new Date());
                    }

                    event.setValue(true);
                    event.setUpdatedAt(new Date());
                    event.setCondition(rule.getCondition());

                    if (EventsRepository.getInstance().saveEvent(key, event)) {
                        Log.d(TAG, String.format("Registering event for rule id %d and condition %s", rule.getId(), rule.getCondition()));
                        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.NEW_EVENT_REGISTERED, event);
                    }
                }
            }
        }
    }

    private boolean shouldMark() {
        return (Profile.hasProfileInCurrentSession());
    }

}
