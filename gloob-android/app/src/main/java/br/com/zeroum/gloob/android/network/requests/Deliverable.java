package br.com.zeroum.gloob.android.network.requests;
public interface Deliverable {
    boolean hasBeenDelivered();
}