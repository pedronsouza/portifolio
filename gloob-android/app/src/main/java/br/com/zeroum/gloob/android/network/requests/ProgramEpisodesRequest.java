package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.objects.ProgramEpisodesRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/28/15.
 */
public class ProgramEpisodesRequest implements Response.Listener<JSONObject>, Response.ErrorListener {
    private BaseRequest request;
    private BroadcastManager broadcastManager;

    public ProgramEpisodesRequest(int programId, int pageIndex) {
        broadcastManager = new BroadcastManager();
        request = new JsonAuthenticatedRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getProgramEpisodes(programId, pageIndex), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.BAD_REQUEST_ERROR);
    }

    @Override
    public void onResponse(JSONObject response) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_PROGRAM_EPISODES_OK, GsonProvider.getGson().fromJson(response.toString(), ProgramEpisodesRequestObject.class));
    }

    public BaseRequest getRequest() {
        return request;
    }
}
