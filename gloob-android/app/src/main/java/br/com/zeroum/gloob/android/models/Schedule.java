package br.com.zeroum.gloob.android.models;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import br.com.zeroum.gloob.android.GloobApplication;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;
import br.com.zeroum.gloob.android.support.GloobPreferences;

import static android.content.Context.MODE_PRIVATE;
import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/8/15.
 */
public class Schedule implements Parcelable {
    private static final String ALL_ALERTS_KEY = "Schedule.ALL_ALERTS_KEY_%s";
    @Expose
    private int id;
    @Expose
    public String title = null;
    @Expose
    public String programTitle = null;
    @Expose
    public String sinopse = null;
    @Expose
    public String date = null;
    @Expose
    public int epNumber = 0;

    public String getTitle() {
        return this.title;
    }

    public String getProgramTitle() {
        return this.programTitle;
    }

    public String getSinopse() {
        return this.sinopse;
    }

    public int getNumber() {
        return this.epNumber;
    }

    public String getDate() {
        return date;
    }

    public Schedule(JSONObject episode, String epDate) throws JSONException {
        try {
            Random randomGenerator = new Random();
            this.id = randomGenerator.nextInt(1000);
            this.title = episode.getJSONObject("episodio").getString("nome");
            this.programTitle = episode.getJSONObject("programa").getString("nome");
            this.sinopse = episode.getJSONObject("episodio").getString("sinopse");
            this.epNumber = episode.getJSONObject("episodio").getInt("numeroEpisodio");
            this.date = epDate;

        } catch (JSONException e) {
        }
    }

    public void setAlarm() {
        Set<String> alarms = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE).getStringSet(getAllAlarmsKeyFormated(), new HashSet<String>());

        boolean shouldAdd = true;

        for (String schedulRaw : alarms) {
            Schedule s = GsonProvider.getGson().fromJson(schedulRaw, Schedule.class);
            if (s.getDate().equals(date)) {
                shouldAdd = false;
            }
        }

        if (shouldAdd) {
            alarms.add(GsonProvider.getGson().toJson(this));
            application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE).edit().putStringSet(getAllAlarmsKeyFormated(), alarms).apply();
        }

        GloobNotificationManager.getInstance().registerNotification(this);
    }

    public void unsetAlarm() {
        Set<String> alarms = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE).getStringSet(getAllAlarmsKeyFormated(), new HashSet<String>());

        for (String scheduleRaw : alarms) {

            Schedule s = GsonProvider.getGson().fromJson(scheduleRaw, Schedule.class);

            if (s.getDate().equals(date)) {
                alarms.remove(scheduleRaw);
                application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE).edit().putStringSet(getAllAlarmsKeyFormated(), alarms).apply();
                int notificationId = this.getId();
                GloobNotificationManager.getInstance().cancelNotification(notificationId);
                break;
            }
        }
    }

    private static String getAllAlarmsKeyFormated() {
        return String.format(ALL_ALERTS_KEY, Profile.getCurrentProfile().getId());
    }

    public boolean hasAlarmSet(String programName) {
        Set<String> alarms = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE).getStringSet(getAllAlarmsKeyFormated(), new HashSet<String>());
        boolean hasAlarm = false;

        for (String scheduleRaw : alarms) {
            Schedule s = GsonProvider.getGson().fromJson(scheduleRaw, Schedule.class);
            if (s.getDate().equals(date) && s.getProgramTitle().equals(programName)) {
                hasAlarm = true;
                break;
            }
        }

        return hasAlarm;
    }

    private String getAlarmKey() {
        return String.format("%s_%s", this.programTitle, Profile.getCurrentProfile().getId());
    }

    public static List<Schedule> getAllAlerts() {
        Set<String> alarmsRaw = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE).getStringSet(getAllAlarmsKeyFormated(), new HashSet<String>());
        List<Schedule> alarms = new ArrayList();

        for (String alarmRaw : alarmsRaw) {
            alarms.add(GsonProvider.getGson().fromJson(alarmRaw, Schedule.class));
        }

        return alarms;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.programTitle);
        dest.writeString(this.sinopse);
        dest.writeString(this.date);
        dest.writeInt(this.epNumber);
    }

    protected Schedule(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.programTitle = in.readString();
        this.sinopse = in.readString();
        this.date = in.readString();
        this.epNumber = in.readInt();
    }

    public static final Creator<Schedule> CREATOR = new Creator<Schedule>() {
        public Schedule createFromParcel(Parcel source) {
            return new Schedule(source);
        }

        public Schedule[] newArray(int size) {
            return new Schedule[size];
        }
    };
}
