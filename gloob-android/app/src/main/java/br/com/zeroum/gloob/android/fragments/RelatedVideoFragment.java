package br.com.zeroum.gloob.android.fragments;


import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.ThumbNetworkImageView;

public class RelatedVideoFragment extends GloobFragment {
    public static final String VIDEO_EXTRA_KEY = "RelatedVideoFragment.VIDEO_EXTRA_KEY";

    private Episode mEpisode;
    private SimpleDraweeView mEpisodeThumb;
    private OnRelatedVideoSelected onRelatedVideoSelected;


    public static RelatedVideoFragment newInstance(Episode video) {
        RelatedVideoFragment fragment = new RelatedVideoFragment();
        Bundle args = new Bundle();
        args.putParcelable(VIDEO_EXTRA_KEY, video);
        fragment.setArguments(args);
        return fragment;
    }

    public RelatedVideoFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEpisode = getArguments().getParcelable(VIDEO_EXTRA_KEY);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_related_video;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEpisodeThumb = Finder.findViewById(mView, R.id.fragment_related_video_thumb);

        mEpisodeThumb.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int width = mEpisodeThumb.getMeasuredWidth();
        int height = mEpisodeThumb.getMeasuredHeight();
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(mEpisode.getImage()))
                .setResizeOptions(new ResizeOptions(width, height))
                .setProgressiveRenderingEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(mEpisodeThumb.getController())
                .setImageRequest(request)
                .build();

        mEpisodeThumb.setController(controller);
        mEpisodeThumb.setOnClickListener(relatedVideoClickListener);
    }

    public void setOnRelatedVideoSelected(OnRelatedVideoSelected onRelatedVideoSelected) {
        this.onRelatedVideoSelected = onRelatedVideoSelected;
    }

    public interface OnRelatedVideoSelected {
        void onSelection(Episode episode);
    }

    private final View.OnClickListener relatedVideoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TrackManager.getInstance().trackEvent(programEpisodeEvent);
            if (onRelatedVideoSelected != null) {
                onRelatedVideoSelected.onSelection(mEpisode);
            }
        }
    };

    private TrackManager.ITrackableEvent programEpisodeEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Play";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("videos|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };
}
