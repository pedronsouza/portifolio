package br.com.zeroum.gloob.android.fragments.profile;


import android.os.Bundle;
import android.view.View;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Devices;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.GloobButton;

public class NewProfileFirstStep extends BaseProfileCreationFragment {

    private GloobButton mCreateAvatar;
    private GloobButton mCreateLater;
    private GloobButton mSignIn;
    private View mLoginHolder;

    public static NewProfileFirstStep newInstance() {
        NewProfileFirstStep fragment = new NewProfileFirstStep();
        return fragment;
    }

    public NewProfileFirstStep() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_new_profile_first_step;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCreateAvatar = Finder.findViewById(mView, R.id.new_profile_first_step_create_avatar);
        mCreateLater = Finder.findViewById(mView, R.id.new_profile_first_step_create_later);
        mSignIn = Finder.findViewById(mView, R.id.new_profile_first_step_sigin);
        mLoginHolder = Finder.findViewById(mView, R.id.new_profile_first_step_login_holder);

        mCreateAvatar.setOnClickListener(createAvatarClickListener);
        mCreateLater.setOnClickListener(createLaterClickListener);
        mSignIn.setOnClickListener(signInClickListener);

        if (Session.isSignedIn()) {
            mLoginHolder.setVisibility(View.GONE);
        }
    }

    private final View.OnClickListener createAvatarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Profile.hasAnyProfile() && Session.isSignedIn()) {
                mActivity.changeRegistrationStep(SelectProfileFragment.newInstance());
            } else {
                mActivity.changeRegistrationStep(NewProfileSecondStep.newInstance());
            }
        }
    };

    private final View.OnClickListener createLaterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mActivity.finish();
        }
    };

    private final View.OnClickListener signInClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TrackManager.getInstance().trackEvent(signInEvent);
            mActivity.showAuthWebView();
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/bem-vindo/crie-seu-avatar";
        }
    };

    private final TrackManager.ITrackableEvent signInEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Entrar";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("inferior/%s/android-%s", Urls.slugify(Devices.getDeviceName()), String.valueOf(android.os.Build.VERSION.SDK_INT));
        }
    };
}
