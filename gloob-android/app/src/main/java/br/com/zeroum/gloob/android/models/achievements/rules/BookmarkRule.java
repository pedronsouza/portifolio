package br.com.zeroum.gloob.android.models.achievements.rules;

import java.util.List;

import br.com.zeroum.gloob.android.models.Bookmark;
import br.com.zeroum.gloob.android.models.Episode;

/**
 * Created by zeroum on 4/20/15.
 */
public class BookmarkRule extends BaseRule implements IRule{
    @Override
    public boolean isValid() {
        List<Bookmark> bookmarks = Bookmark.getAllBookmakrs();

        if (bookmarks != null) {
            return bookmarks.size() >= this.eventAmount;
        }

        return false;
    }

    @Override
    public boolean shouldMark() {
        return isValid();
    }
}
