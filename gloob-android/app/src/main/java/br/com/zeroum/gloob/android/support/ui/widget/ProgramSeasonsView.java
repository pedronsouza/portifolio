package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.adapters.GridVideosAdapter;
import br.com.zeroum.gloob.android.models.Season;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/28/15.
 */
public class ProgramSeasonsView extends LinearLayout {
    private static final String TITLE_FORMAT = "%d. Temporada";
    private Season mSeason;

    private GloobTextView mTitle;
    private ExpandableHeightGridView mGrid;

    public ProgramSeasonsView(Context context, Season season) {
        super(context);
        mSeason = season;
        init(context);
    }

    public ProgramSeasonsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ProgramSeasonsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ProgramSeasonsView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.program_season_episodes, this);

        mTitle = Finder.findViewById(this, R.id.program_season_title);
        mGrid = Finder.findViewById(this, R.id.program_season_grid);

        mTitle.setText(String.format(TITLE_FORMAT, mSeason.getNumber()));

        GridVideosAdapter videosAdapter = new GridVideosAdapter(context, mSeason.getEpisodes());
        videosAdapter.setRelatedVideosTitle(String.format("%s %s (%d)",mSeason.getEpisodes().get(0).getProgram().getTitle(), String.format("TEMP %d", mSeason.getNumber()), mSeason.getEpisodes().size()));
        mGrid.setAdapter(videosAdapter);
        mGrid.setExpanded(true);
    }
}
