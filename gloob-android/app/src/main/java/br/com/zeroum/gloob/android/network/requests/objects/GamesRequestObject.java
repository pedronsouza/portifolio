package br.com.zeroum.gloob.android.network.requests.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;

/**
 * Created by zeroum on 4/13/15.
 */
public class GamesRequestObject implements IRequestObject<GamesRequestObject>, Parcelable {

    public int getCount() {
        return count;
    }

    public String getPrevious() {
        return previous;
    }

    public String getNext() {
        return this.next;
    }

    public List<Game> getGames() {
        return games;
    }

    @Expose
    @SerializedName("count")
    private int count;
    @Expose
    @SerializedName("previous")
    private String previous;
    @Expose
    @SerializedName("next")
    private String next;
    @Expose
    @SerializedName("results")
    private List<Game> games;

    public GamesRequestObject(JSONObject response) {
        try {
            parseResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public GamesRequestObject parseResponse(JSONObject json) throws JSONException {
        this.games = Arrays.asList(GsonProvider.getGson().fromJson(json.getJSONArray("results").toString(), Game[].class));

        this.count = json.getInt("count");
        this.previous = json.getString("previous");
        this.next = json.getString("next");

        return this;
    }

    public GamesRequestObject() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.count);
        dest.writeString(this.previous);
        dest.writeString(this.next);
        dest.writeTypedList(games);
    }

    private GamesRequestObject(Parcel in) {
        this.count = in.readInt();
        this.previous = in.readString();
        this.next = in.readString();
        in.readTypedList(games, Game.CREATOR);
    }

    public static final Creator<GamesRequestObject> CREATOR = new Creator<GamesRequestObject>() {
        public GamesRequestObject createFromParcel(Parcel source) {
            return new GamesRequestObject(source);
        }

        public GamesRequestObject[] newArray(int size) {
            return new GamesRequestObject[size];
        }
    };
}
