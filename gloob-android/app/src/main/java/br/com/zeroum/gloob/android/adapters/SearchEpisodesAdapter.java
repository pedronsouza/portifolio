package br.com.zeroum.gloob.android.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.SearchResult;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.ThumbNetworkImageView;

/**
 * Created by zeroum on 7/27/15.
 */
public class SearchEpisodesAdapter extends BaseAdapter {
    private static final String SEASON_FORMAT = "TEMP.%d";
    private List<SearchResult> mEpisodes;
    private Context mContext;

    public SearchEpisodesAdapter(Context context,List<SearchResult> episodes) {
        mEpisodes = episodes;
        mContext = context;
    }

    public void setEpisodes(List<SearchResult> episodes) {
        this.mEpisodes = episodes;
    }

    @Override
    public int getCount() {
        return mEpisodes.size();
    }

    @Override
    public Object getItem(int position) {
        return mEpisodes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mEpisodes.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (convertView == null) {
            v = View.inflate(mContext, R.layout.search_episodes_list_item, null);
        }

        SearchResult episode = mEpisodes.get(position);

        ThumbNetworkImageView image = Finder.findViewById(v, R.id.search_episodes_list_item_image);
        GloobTextView programName = Finder.findViewById(v, R.id.search_episodes_list_item_program_name);
        GloobTextView episodeName = Finder.findViewById(v, R.id.search_episodes_list_item_episode_title);
        GloobTextView seasonDetails = Finder.findViewById(v, R.id.search_episodes_list_item_season);

        image.setImageUrl(episode.getImage());
        programName.setText(episode.getProgram());
        episodeName.setText(episode.getTitle());
        seasonDetails.setText(String.format(SEASON_FORMAT, episode.getSeasonCount()));

        return v;
    }
}
