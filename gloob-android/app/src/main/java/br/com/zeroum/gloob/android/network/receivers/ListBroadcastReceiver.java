package br.com.zeroum.gloob.android.network.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import java.util.List;

import static br.com.zeroum.gloob.android.network.managers.BroadcastManager.getPayloadAsList;

/**
 * Created by zeroum on 4/13/15.
 */
public final class ListBroadcastReceiver<T extends Parcelable> extends BroadcastReceiver {

    private final ListCommand<T> command;

    public ListBroadcastReceiver(ListCommand<T> command) {
        this.command = command;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        List<T> list = getPayloadAsList(intent);
        this.command.execute(context, list);
    }

    public static <T extends Parcelable> ListBroadcastReceiver<T> newListBroadcastReceiver(ListCommand<T> command) {
        return new ListBroadcastReceiver<T>(command);
    }

}
