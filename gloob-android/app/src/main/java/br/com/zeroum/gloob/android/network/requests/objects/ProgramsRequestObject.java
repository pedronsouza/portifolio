package br.com.zeroum.gloob.android.network.requests.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.zeroum.gloob.android.models.Program;

/**
 * Created by zeroum on 7/15/15.
 */
public class ProgramsRequestObject implements Parcelable {
    public int getCount() {
        return count;
    }

    public String getPrevious() {
        return previous;
    }

    public String getNext() {
        return next;
    }

    public List<Program> getPrograms() {
        return programs;
    }

    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("previous")
    @Expose
    private String previous;
    @SerializedName("next")
    @Expose
    private String next;
    @SerializedName("results")
    @Expose
    private List<Program> programs;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.count);
        dest.writeString(this.previous);
        dest.writeString(this.next);
        dest.writeTypedList(programs);
    }

    public ProgramsRequestObject() {
    }

    protected ProgramsRequestObject(Parcel in) {
        this.count = in.readInt();
        this.previous = in.readString();
        this.next = in.readString();
        this.programs = in.createTypedArrayList(Program.CREATOR);
    }

    public static final Parcelable.Creator<ProgramsRequestObject> CREATOR = new Parcelable.Creator<ProgramsRequestObject>() {
        public ProgramsRequestObject createFromParcel(Parcel source) {
            return new ProgramsRequestObject(source);
        }

        public ProgramsRequestObject[] newArray(int size) {
            return new ProgramsRequestObject[size];
        }
    };
}
