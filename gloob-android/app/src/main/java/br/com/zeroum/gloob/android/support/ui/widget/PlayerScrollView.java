package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Created by zeroum on 7/30/15.
 */
public class PlayerScrollView extends ScrollView {
    private OnScrollChangesListener scrollAtTopListener;


    public PlayerScrollView(Context context) {
        super(context);
    }

    public PlayerScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlayerScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PlayerScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (this.scrollAtTopListener != null) {
            scrollAtTopListener.onScrollChange(getScrollX(), getScrollY());
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }

    public void setScrollChangesListener(OnScrollChangesListener scrollAtTopListener) {
        this.scrollAtTopListener = scrollAtTopListener;
    }

    public interface OnScrollChangesListener {
        void onScrollChange(int x, int y);
    }
}
