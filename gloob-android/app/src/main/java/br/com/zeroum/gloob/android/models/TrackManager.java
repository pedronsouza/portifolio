package br.com.zeroum.gloob.android.models;

import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.support.Devices;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 8/3/15.
 */
public class TrackManager {
    public static final int DISPATCH_PERIOD_IN_SECONDS = 900;// 15 minutes
    public static final String TRACKMANAGER_DEFAUL_EVENT_ACTION = "GLOOB_PLAY";
    public static final String TRACK_MANAGER_TAG = "TrackManager";

    private Tracker mTracker;

    public void trackPageView(ITrackablePageView trackablePageView) {
        try {
            mTracker.setScreenName(trackablePageView.getScreenName());
            mTracker.send(getScreenBuilderWithCustomDimentions().build());
        } catch (Exception e) {
            Log.e(TRACK_MANAGER_TAG, "Erro ao fazer o track do pageview");
        }
    }

    public void trackEvent(ITrackableEvent trackableEvent) {
        try {
            mTracker.send(getEventBuilderWithCustomDimentions()
                    .setCategory(trackableEvent.getCategoryForTrack())
                    .setAction(TRACKMANAGER_DEFAUL_EVENT_ACTION)
                    .setLabel(trackableEvent.getLabelForTrack())
                    .build());
        } catch (Exception e) {
            Log.e(TRACK_MANAGER_TAG, String.format("Erro ao fazer o track do evento %s", trackableEvent.getCategoryForTrack()));
        }
    }

    private void setupGoogleAnalyticsSdk() {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(application());
        analytics.setLocalDispatchPeriod(DISPATCH_PERIOD_IN_SECONDS);

        mTracker = analytics.newTracker(application().getResources().getString(R.string.ga_track_ua));
    }

    private HitBuilders.ScreenViewBuilder getScreenBuilderWithCustomDimentions() {
        String provider = "";
        String cliendId = "";
        String isLogged = "não logado";
        String deviceInfo = String.format("%s|%d", Devices.getDeviceName(), android.os.Build.VERSION.SDK_INT);

        if (Session.isSignedIn()) {
            provider = Session.getCurrentSession().getUser().getProvider();
            cliendId = String.format("%d_%s", Session.getCurrentSession().getUser().getCliendId(), provider);
            isLogged = "logado";
        }

        return new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(1, provider)
                .setCustomDimension(1, cliendId)
                .setCustomDimension(1, isLogged)
                .setCustomDimension(1, deviceInfo);
    }

    private HitBuilders.EventBuilder getEventBuilderWithCustomDimentions() {
        String provider = "";
        String cliendId = "";
        String isLogged = "não logado";
        String deviceInfo = String.format("%s|%d", Devices.getDeviceName(), android.os.Build.VERSION.SDK_INT);

        if (Session.isSignedIn()) {
            provider = Session.getCurrentSession().getUser().getProvider();
            cliendId = String.format("%d_%s", Session.getCurrentSession().getUser().getCliendId(), provider);
            isLogged = "logado";
        }

        return new HitBuilders.EventBuilder()
                .setCustomDimension(1, provider)
                .setCustomDimension(1, cliendId)
                .setCustomDimension(1, isLogged)
                .setCustomDimension(1, deviceInfo);
    }

    private TrackManager() {
        setupGoogleAnalyticsSdk();
    }

    public static TrackManager getInstance() {
        return SingletonHolder.INSTANCE_HOLDER;
    }

    private static class SingletonHolder {
        public static final TrackManager INSTANCE_HOLDER = new TrackManager();
    }

    public interface ITrackablePageView {
        String getScreenName();
    }

    public interface ITrackableEvent {
        String getCategoryForTrack();
        String getLabelForTrack();
    }
}
