package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/21/15.
 */
public class GenericListItem extends LinearLayout {
    private int mItemLayoutId;
    private SimpleDraweeView mImageView;
    private GloobTextView mLabel;
    private GenericGloobList.IGenericListItem mItem;
    private ImageButton mClickArea;
    private OnClickListener mItemClickListener;

    public GenericListItem(Context context, GenericGloobList.IGenericListItem item, OnClickListener itemClickListener, int itemLayoutId) {
        super(context);
        mItem = item;
        mItemClickListener = itemClickListener;
        mItemLayoutId = itemLayoutId;
        init(context);
    }

    public GenericListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GenericListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GenericListItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, mItemLayoutId, this);

        mImageView = Finder.findViewById(this, R.id.generic_item_image);
        mLabel = Finder.findViewById(this, R.id.generic_item_label);
        mClickArea = Finder.findViewById(this, R.id.generic_item_click_area);

        mImageView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int width = mImageView.getMeasuredWidth();
        int height =mImageView.getMeasuredHeight();

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(mItem.getImageUrl()))
                .setResizeOptions(new ResizeOptions(width, height))
                .setProgressiveRenderingEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(mImageView.getController())
                .setImageRequest(request)
                .build();

        mImageView.setController(controller);

        if (mItem.getLabel() != null) {
            String text = mItem.getLabel();
            if (text.length() > 13) {
                text = String.format("%s...", text.substring(0, 13));
            }

            mLabel.setText(text);
            mLabel.setVisibility(View.VISIBLE);
        }


        setGravity(Gravity.CENTER_HORIZONTAL);

        if (mItemClickListener != null)
            setOnClickListener(mItemClickListener);
    }

    public void setItem(GenericGloobList.IGenericListItem item) {
        mItem = item;
    }

    public void hideLabel() {
        mLabel.setVisibility(GONE);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        setClickable(true);
        mImageView.setOnClickListener(l);
        mClickArea.setOnClickListener(l);
    }

    @Override
    public void setTag(Object tag) {
        super.setTag(tag);
        mImageView.setTag(tag);
        mClickArea.setTag(tag);
    }
}
