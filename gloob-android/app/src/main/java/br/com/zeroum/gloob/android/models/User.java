package br.com.zeroum.gloob.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zeroum on 7/14/15.
 */
public class User implements Parcelable {
    @SerializedName("nome")
    @Expose
    private String userName;
    @SerializedName("email")
    @Expose
    private String email;
    @Expose
    private String provider;
    @Expose
    @SerializedName("uid")
    private int cliendId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userName);
        dest.writeString(this.email);
        dest.writeString(this.provider);
    }

    public static void signIn(String result) {
    }

    public User() {
    }

    protected User(Parcel in) {
        this.userName = in.readString();
        this.email = in.readString();
        this.provider = in.readString();
    }



    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public static boolean isSignedIn() {
        return false;
    }

    public int getCliendId() {
        return cliendId;
    }

    public void setCliendId(int cliendId) {
        this.cliendId = cliendId;
    }
}
