package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

/**
 * Created by zeroum on 7/27/15.
 */
public class EndlessScrollView extends ScrollView {
    private OnScrollToEnd onScrollToEndListener;
    public EndlessScrollView(Context context) {
        super(context);
    }

    public EndlessScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EndlessScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EndlessScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setOnScrollToEndListener(OnScrollToEnd onScrollToEndListener) {
        this.onScrollToEndListener = onScrollToEndListener;
    }

    public interface OnScrollToEnd {
        void hasHeachedEnd();
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        View view = getChildAt(getChildCount()-1);
        int diff = (view.getBottom()-(getHeight()+getScrollY()));

        if(diff == 0) {
            if (onScrollToEndListener != null) {
                onScrollToEndListener.hasHeachedEnd();
            }
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }
}
