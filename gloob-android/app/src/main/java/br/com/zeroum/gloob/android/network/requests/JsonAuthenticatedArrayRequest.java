package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.support.Urls;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/13/15.
 */
public class JsonAuthenticatedArrayRequest extends JsonArrayRequest {
    private final String mUrl;

    public JsonAuthenticatedArrayRequest(int requestMethod, Request.Priority priority, String url, Map<String, String> params, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        super(requestMethod, priority, url, params, listener, errorListener);
        mUrl = url;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        headers.putAll(super.getHeaders());
        headers.put("AUTHORIZATION", String.format("%s %s", "Token", tokenByURL()));
        return headers;
    }

    private String tokenByURL() {
        if (mUrl.contains(String.format(Urls.BASE_GAMES_URL, "")))
            return application().getString(R.string.http_auth_token_2);
        else
            return application().getString(R.string.http_auth_token);
    }
}
