package br.com.zeroum.gloob.android.network.volley;

import android.os.Handler;
import android.util.Log;

import com.android.volley.Request;

import java.util.HashSet;
import java.util.Set;

import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.Deliverable;

/**
 * Created by zeroum on 4/8/15.
 */
public class ObserverRequests {
    static private ObserverRequests instance = null;
    
    private final Handler handler = new Handler();

    private final Set<Request<?>> currentRequests = new HashSet<Request<?>>();
    private boolean isObserverRunning = false;
    boolean hasRequestPending = false;

    public static ObserverRequests getInstance() {
        if (instance == null)
            instance = new ObserverRequests();
        return instance;
    }


    public void addToObserver(final Request request) {

        /* inicializa o Verificador Furioso */
        if (!isObserverRunning) {
            isObserverRunning = true;
            Log.d("ObserverRequests", "Iniciando verificador Furioso");
            initObserver();
        }

        synchronized (currentRequests) {
            currentRequests.add(request);
        }

    }

    private void initObserver() {
        synchronized (currentRequests) {
            handler.post(runnableScene);
        }
    }

    private Runnable runnableScene = new Runnable() {
        @Override
        public void run() {

            synchronized (currentRequests) {
                //Log.d("ObserverRequest", "Observer rodando!"); // TODO Remover log.

                hasRequestPending = false;

                for (Request request : currentRequests) {
                    if (!request.isCanceled()) {
                        if (!((Deliverable) request).hasBeenDelivered()) {
                            hasRequestPending = true;
                        }
                        hasRequestPending = true;
                    }
                }

                if (hasRequestPending) {
                    handler.postDelayed(runnableScene, 2000);
                } else {
                    isObserverRunning = false;
                    new BroadcastManager().sendLocalBroadcast(BroadcastManager.Action.ALL_REQUESTS_FINISHED);
                }
            }
        }
    };
}
