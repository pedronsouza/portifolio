package br.com.zeroum.gloob.android.network.requests.objects;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.models.Schedule;

/**
 * Created by zeroum on 4/13/15.
 */
public class ScheduleRequestObject implements IRequestObjectQuery<ScheduleRequestObject>, Parcelable {

    private int count;

    private ArrayList<Schedule> episodes = new ArrayList<>();

    public int getCount() {
        return count;
    }

    public List<Schedule> getSchedule() {
        return episodes;
    }

    @Override
    public ScheduleRequestObject parseResponse(JSONObject xpp, String query) {

        try {
            JSONObject grade = xpp.getJSONObject("gradePorDiaResponse");
            JSONObject returns = grade.getJSONObject("return");
            JSONObject grid = returns.getJSONObject("grid");
            JSONObject data = grid.getJSONObject("data");

            JSONArray shows = data.getJSONArray("show");

            count = shows.length();

            for(int i = 0 ; i<count ; i++){
                if(shows.getJSONObject(i).getJSONObject("programa").getString("nome").toLowerCase().replaceAll(" ", "").equals(query.toLowerCase().replaceAll(" ", ""))){

                    boolean duplicate = false;

                    for(Schedule sch : episodes) {
                        if(sch.getTitle().toLowerCase().replaceAll(" ","").equals(shows.getJSONObject(i).getJSONObject("episodio").getString("nome").toLowerCase().replaceAll(" ",""))) {
                            duplicate = true;
                        }
                    }

                    if(!duplicate) {

                        if(shows.getJSONObject(i).getJSONObject("exibicoes").optJSONArray("exibicao") != null){
                            for(int d = 0 ; d<shows.getJSONObject(i).getJSONObject("exibicoes").optJSONArray("exibicao").length() ; d++){
                                Schedule scheduleEpisode = new Schedule(shows.getJSONObject(i),shows.getJSONObject(i).getJSONObject("exibicoes").getJSONArray("exibicao").getString(d));
                                episodes.add(scheduleEpisode);
                            }
                        }else{
                            Schedule scheduleEpisode = new Schedule(shows.getJSONObject(i),shows.getJSONObject(i).getJSONObject("exibicoes").getString("exibicao"));
                            episodes.add(scheduleEpisode);
                        }
                    }
                }
            }

        } catch (JSONException e) {
        }

        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public ScheduleRequestObject() {
    }

    private ScheduleRequestObject(Parcel in) {
    }

    public static final Creator<ScheduleRequestObject> CREATOR = new Creator<ScheduleRequestObject>() {
        public ScheduleRequestObject createFromParcel(Parcel source) {
            return new ScheduleRequestObject(source);
        }

        public ScheduleRequestObject[] newArray(int size) {
            return new ScheduleRequestObject[size];
        }
    };
}
