package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.PlayerActivity;
import br.com.zeroum.gloob.android.models.Bookmark;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/28/15.
 */
public class BookmarkThumbView extends LinearLayout {
    private Bookmark mBookmark;
    private ThumbNetworkImageView mImage;
    private GloobTextView mProgram;
    private GloobTextView mEpisode;
    private GloobTextView mSeason;
    private OnBookmarkRemovedListener onBookmarkRemovedListener;
    private Context mContext;

    public BookmarkThumbView(Context context, Bookmark bookmark) {
        super(context);
        mBookmark = bookmark;
        init(context);
    }

    public BookmarkThumbView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BookmarkThumbView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BookmarkThumbView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.bookmark_thumb_view, this);

        mImage = Finder.findViewById(this, R.id.bookmark_thumb_img);
        mProgram = Finder.findViewById(this, R.id.bookmark_thumb_program);
        mEpisode = Finder.findViewById(this, R.id.bookmark_thumb_episode);
        mSeason = Finder.findViewById(this, R.id.bookmark_thumb_season);

        mImage.setImageUrl(mBookmark.getEpisode().getImage());
        mProgram.setText(mBookmark.getEpisode().getProgram().getTitle());
        mEpisode.setText(mBookmark.getEpisode().getTitle());
        mSeason.setText(String.format("TEMP.%d - EP.%d", mBookmark.getEpisode().getSeason().getNumber(), mBookmark.getEpisode().getNumber()));

        mImage.setOnClickListener(openVideoClickListener);
        setClickable(true);
        setOnClickListener(openVideoClickListener);

        findViewById(R.id.bookmark_thumb_close).setOnClickListener(removeClickListener);
    }

    public interface OnBookmarkRemovedListener {
        void onRemoved();
    }

    public void setOnBookmarkRemovedListener(OnBookmarkRemovedListener onBookmarkRemovedListener) {
        this.onBookmarkRemovedListener = onBookmarkRemovedListener;
    }

    private final OnClickListener removeClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setMessage("Deseja remover este favorito?");
            dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            dialog.setPositiveButton("Remover", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Bookmark.removeFromBookmarks(mBookmark);
                    if (onBookmarkRemovedListener != null) {
                        onBookmarkRemovedListener.onRemoved();
                    }
                }
            });

            dialog.show();
        }
    };

    private final OnClickListener openVideoClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            List<Bookmark> bookmarks = Bookmark.getAllBookmakrs();
            ArrayList<Episode> relatedEpisodes = new ArrayList();

            for (Bookmark b : bookmarks) {
                relatedEpisodes.add(b.getEpisode());
            }

            Intent playerIntent = new Intent(mContext, PlayerActivity.class);
            playerIntent.putExtra(PlayerActivity.EPISODE_EXTRA_KEY, mBookmark.getEpisode());
            playerIntent.putParcelableArrayListExtra(PlayerActivity.EPISODES_LIST_EXTRA_KEY, relatedEpisodes);
            playerIntent.putExtra(PlayerActivity.EPISODE_RELATED_VIDOS_TITLE_KEY, String.format("VÍDEOS FAVORITOS (%d)", relatedEpisodes.size()));
            TrackManager.getInstance().trackEvent(playVideoEvent);
            mContext.startActivity(playerIntent);
        }
    };

    private final TrackManager.ITrackableEvent playVideoEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Play";
        }

        @Override
        public String getLabelForTrack() {
            Episode episode = mBookmark.getEpisode();
            String event = String.format("seu-perfil|vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(episode.getId()),
                    Urls.slugify(episode.getCategory()),
                    Urls.slugify(episode.getProgram().getTitle()),
                    (episode.getSeason() == null) ? "" : String.valueOf(episode.getSeason().getNumber()),
                    (episode.getNumber() == 0) ? "" : String.valueOf(episode.getNumber()),
                    Urls.slugify(episode.getTitle()));

            return event;
        }
    };
}
