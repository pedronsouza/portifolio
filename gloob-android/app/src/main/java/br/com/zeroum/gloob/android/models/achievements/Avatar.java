package br.com.zeroum.gloob.android.models.achievements;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.List;

import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.models.achievements.repositories.RulesRepository;
import br.com.zeroum.gloob.android.models.achievements.rules.BaseRule;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/15/15.
 */
public class Avatar extends Achievement {

    public Avatar() {

    }

    public Avatar(Achievement achievement) {
        this.id = achievement.getId();
        this.title = achievement.getTitle();
        this.subtitle = achievement.getSubtitle();
        this.descriptionOn = achievement.getDescriptionOn();
        this.descriptionOff = achievement.getDescriptionOff();
        this.icon = achievement.getIcon();
        this.rulesRaw = achievement.getRulesRaw();
        this.type = achievement.getType();
        this.relatedRules = achievement.getRelatedRules();
    }

    @Override
    public Drawable getImageForUnlock() {
        Context context = application();
        String imageName = String.format("avatar_novoperfil_%s", this.getIcon());
        int id = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
        return application().getResources().getDrawable(id);
    }

    public Drawable getImageForCreation() {
        String imageName = String.format("avatar_novoperfil_%s", this.getIcon());
        int id = application().getResources().getIdentifier(imageName, "drawable", application().getPackageName());
        return application().getResources().getDrawable(id);
    }

    public int getImageForHomeResourceId() {
        String imageName = String.format("avatar_home_%s", this.getIcon());
        int id = application().getResources().getIdentifier(imageName, "drawable", application().getPackageName());
        return id;
    }

    public int getImageForProfileId() {
        String imageName = String.format("avatar_criar_selecionado_%s", this.getIcon());
        int id = application().getResources().getIdentifier(imageName, "drawable", application().getPackageName());
        return id;
    }

    public int getDefaultImageId() {
        String imageName = String.format("avatar_criar_%s", this.getIcon());
        int id = application().getResources().getIdentifier(imageName, "drawable", application().getPackageName());
        return id;
    }
    public static List<Avatar> getDefaulAvatars() {
        return AchievementsRepository.getInstance().getAvatars().subList(0, 5);
    }

    public boolean checkifAvatarIsValid() {
        BaseRule rule = RulesRepository.getInstance().getRuleById(this.rulesRaw[0]);

        if(rule.isValid()) {
            return true;
        }else{
            return false;
        }
    }
}
