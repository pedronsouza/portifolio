package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by zeroum on 4/13/15.
 */
public class JsonRequest extends BaseRequest<JSONObject> {
    private final Priority priority;
    private Response.Listener<JSONObject> listener;
    private Map<String, String> params;
    private int SET_SOCKET_TIMEOUT = 20 * 1000;

    public JsonRequest(int method, Priority priority, String url, Map<String, String> params, Response.Listener<JSONObject> reponseListener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
        this.priority = priority;
        policySetup();
    }

    @Override
    public Priority getPriority() {
        if (priority == null) {
            return Priority.NORMAL;
        }

        return priority;
    }

    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        return params;
    };

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,"UTF-8");
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        super.deliverResponse(response);
        listener.onResponse(response);
    }

    public void policySetup() {
        setRetryPolicy(new DefaultRetryPolicy(SET_SOCKET_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
