package br.com.zeroum.gloob.android.network.requests;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

public abstract class BaseRequest<T> extends Request<T> implements Deliverable {

    private boolean delivered = false; // Status do envio da resposta

    public BaseRequest(int method, String url, Response.ErrorListener listener) {
        super(method, url, listener);
    }

    @Override
    protected abstract Response<T> parseNetworkResponse(NetworkResponse response);

    @Override
    protected void deliverResponse(T response) {
        delivered = true;
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
        delivered = true;
    }

    /**
     * Se a Response || Error foi enviada, retorna verdadeiro.
     *
     * @return retorna o status da request
     */
    @Override
    public boolean hasBeenDelivered() {
        return delivered;
    }
}