package br.com.zeroum.gloob.android.network.managers;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.zeroum.gloob.android.fragments.CharactersFragment;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.MostViewed;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.achievements.Achievement;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.models.achievements.Event;
import br.com.zeroum.gloob.android.network.requests.objects.MosaicRequestObject;
import br.com.zeroum.gloob.android.network.requests.objects.SearchRequestObject;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/8/15.
 */
public class BroadcastManager {
    public static final String LIST = "list";
    public static final String DATA = "data";
    public static final String PAGE = "page";
    public static final int NO_DATA = -1;

    public static final String DATE = "date";

    private final LocalBroadcastManager localBroadcastManager;

    public BroadcastManager() {
        localBroadcastManager = LocalBroadcastManager.getInstance(application());
    }

    public enum Action {

        /* Requests */
        ALL_REQUESTS_FINISHED(Void.class, "all_requests_finished"),
        NEW_ACHIEVEMENT_UNLOCKED(Void.class, "new_achievement_unlocked"),
        BAD_REQUEST_ERROR(Void.class, "bad_request_error"),
        TOGGLE_AVATAR_SELECTION(Avatar.class, "toggle_avatar_selection"),
        GET_TOKEN_OK(Session.class, "get_token_ok"),
        GET_GAMES_ERROR(Game.class, "get_games_error"),
        GET_GAMES_OK(Game.class, "get_games_ok"),
        GET_MOST_VIEWED_ERROR(MostViewed.class, "get_most_viewed_error"),
        GET_MOST_VIEWED_OK(MostViewed.class, "get_most_viewed_ok"),
        GET_MOSAIC_ERROR(MosaicRequestObject.class, "get_mosaic_error"),
        GET_MOSAIC_OK(MosaicRequestObject.class, "get_mosaic_ok"),
        GET_PROGRAMS_OK(Program.class, "get_programs_ok"),
        GET_PROGRAMS_ERROR(Program.class, "get_programs_error"),
        NEW_EVENT_REGISTERED(Event.class, "new_event_registered"),
        ACHIEVEMENT_UNLOCKED_DISMISSED(Achievement.class, "achievement_unlocked_dismissed"),
        CHANGE_AVATAR(Avatar.class, "change_avatar"),
        REBUILD_PROFILE_LIST(Profile.class, "rebuild_profile_list"),
        CHANGE_AVATAR_RECEIVER(Avatar.class, "change_avatar_receiver"),
        START_PLAYING(Episode.class, "start_playing"),
        GET_SEARCH_OK(SearchRequestObject.class, "get_search_ok"),
        GET_PROGRAM_EPISODES_OK(Episode.class, "get_program_episodes_ok"),
        GET_SCHEDULE_OK(Void.class, "get_schedule_ok"),
        START_AT_CHARACTERS(CharactersFragment.class, "start_at_characters"), GET_EPISODE_BY_ID_OK(Episode.class, "get_episode_by_id_ok");


        private Class<?> clazz;
        private final String action;

        private Action(final Class<?> clazz, final String action) {
            this.clazz = clazz;
            this.action = action;
        }

        public Class<?> getClazz() {
            return clazz;
        }

        public String getAction() {
            return action;
        }

        public static Action valueOf(final Class<? extends Parcelable> clazz) {
            if (clazz == null) {
                throw new IllegalArgumentException("Valor desconhecido " + clazz);
            }
            for (final Action action : values()) {
                if (action.getClazz().equals(clazz)) {
                    return action;
                }
            }
            throw new IllegalArgumentException("Valor desconhecido " + clazz);
        }

    }


    /**
     * Registers a {@link BroadcastReceiver} to a given <b>action</b>.
     *
     * @param broadcastReceiver
     * @param action
     */
    public void registerLocalBroadcastReceiver(final BroadcastReceiver broadcastReceiver, final Action action) {
        localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(action.getAction()));
    }

    /**
     * Unregisters a {@link BroadcastReceiver} from the
     * {@link BroadcastManager}.
     *
     * @param broadcastReceiver
     */
    public void unregisterLocalBroadcastReceiver(final BroadcastReceiver broadcastReceiver) {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
    }

    /**
     * Sends a broadcast {@link Intent}
     * {@link BroadcastReceiver}s registered to <b>action</b>.
     *
     * @param action
     */
    public <T extends Parcelable> void sendLocalBroadcast(final Action action) {
        final Intent intent = new Intent(action.getAction());
        localBroadcastManager.sendBroadcast(intent);
    }

    /**
     * Sends a broadcast {@link Intent} containing a <b>data</b> to all
     * {@link BroadcastReceiver}s registered to <b>action</b>.
     *
     * @param action
     * @param data
     */
    public <T extends Parcelable> void sendLocalBroadcast(final Action action, final List<T> data) {
        final Intent intent = new Intent(action.getAction());
        intent.putParcelableArrayListExtra(LIST, (ArrayList<T>) data);
        localBroadcastManager.sendBroadcast(intent);
    }

    /**
     * Sends a broadcast {@link Intent} containing a <b>data</b> to all
     * {@link BroadcastReceiver}s registered to <b>action</b>.
     *
     * @param action
     * @param data
     */
    public <T extends Parcelable> void sendLocalBroadcast(final Action action, final ArrayList<XmlPullParser> data) {
        final Intent intent = new Intent(action.getAction());
        intent.putExtra(LIST, data);
        localBroadcastManager.sendBroadcast(intent);
    }

    /**
     * Sends a broadcast {@link Intent} containing a <b>data</b> to all
     * {@link BroadcastReceiver}s registered to <b>action</b>.
     *
     * @param action
     * @param data
     */
    public <T extends Parcelable> void sendLocalBroadcast(final Action action, final T data) {
        final Intent intent = new Intent(action.getAction());
        intent.putExtra(DATA, data);
        localBroadcastManager.sendBroadcast(intent);
    }

    public <T extends Parcelable> void sendLocalBroadcast(final Action action, final T data, int pageIndex) {
        final Intent intent = new Intent(action.getAction());
        intent.putExtra(DATA, data);
        intent.putExtra(PAGE, pageIndex);
        localBroadcastManager.sendBroadcast(intent);
    }

    /**
     * Sends a broadcast {@link Intent} containing a <b>data</b> to all
     * {@link BroadcastReceiver}s registered to <b>action</b>.
     *
     * @param action
     * @param data
     */
    public void sendLocalBroadcast(final Action action, final int data) {
        final Intent intent = new Intent(action.getAction());
        intent.putExtra(DATA, data);
        localBroadcastManager.sendBroadcast(intent);
    }

    /**
     * Sends a broadcast {@link Intent} containing a <b>data</b> to all
     * {@link BroadcastReceiver}s registered to <b>action</b>.
     *
     * @param action
     * @param data
     */
    public void sendLocalBroadcast(final Action action, final boolean data) {
        final Intent intent = new Intent(action.getAction());
        intent.putExtra(DATA, data);
        localBroadcastManager.sendBroadcast(intent);
    }

    /**
     * Creates a new {@link Bundle} with the given {@link List} as an extra.
     *
     * @param data
     * @return the {@link Bundle}
     */
    public static <T extends Parcelable> Bundle newPayloadBundle(final List<T> data) {
        final Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(LIST, (ArrayList<? extends Parcelable>) data);
        return bundle;
    }

    public static <T extends Parcelable> Bundle newPayloadBundle(final T data) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(DATA, data);
        return bundle;
    }

    public static <T1 extends Parcelable, T2 extends Parcelable> Bundle newPayloadBundle(final T1 data, final List<T2> list) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(DATA, data);
        bundle.putParcelableArrayList(LIST, (ArrayList<? extends Parcelable>) list);
        return bundle;
    }

    /**
     * Creates a new {@link Intent} with the given {@link List} as an extra.
     *
     * @param data
     * @return the {@link Intent}
     */
    public static <T extends Parcelable> Intent newPayloadIntent(final Intent intent, final List<T> data) {
        intent.putParcelableArrayListExtra(LIST, (ArrayList<? extends Parcelable>) data);
        return intent;
    }

    public static <T extends Parcelable> Intent newPayloadIntent(final Intent intent, final T data) {
        intent.putExtra(DATA, data);
        return intent;
    }

    /**
     * Receives an {@link ArrayList} of {@link Parcelable} from <b>bundle</b>.
     *
     * @param bundle
     * @return the {@link ArrayList}
     */
    public static <T extends Parcelable> ArrayList<T> getPayloadAsList(final Bundle bundle) {
        return bundle.getParcelableArrayList(LIST);
    }

    /**
     * Receives an {@link ArrayList} of {@link Parcelable} from <b>intent</b>.
     *
     * @param intent
     * @return the {@link ArrayList}
     */
    public static <T extends Parcelable> ArrayList<T> getPayloadAsList(final Intent intent) {
        return intent.getParcelableArrayListExtra(LIST);
    }

    /**
     * Receives an {@link String} from <b>intent</b>.
     *
     * @param intent
     * @return the {@link String}
     */
    public static String getPayloadAsString(final Intent intent) {
        return intent.getStringExtra(DATA);
    }

    /**
     * Receives an <b>int</b> from <b>intent</b>.
     *
     * @param intent
     * @return the {@link ArrayList}
     */
    public static int getPayloadAsInt(final Intent intent) {
        return intent.getIntExtra(DATA, NO_DATA);
    }

    /**
     * Receives an {@link ArrayList} of {@link Parcelable} from <b>intent</b>.
     *
     * @param intent
     * @return the {@link ArrayList}
     */
    public static <T extends Parcelable> T getPayload(final Intent intent) {
        return intent.getParcelableExtra(DATA);
    }

    public static <T extends Parcelable> T getPayload(final Bundle bundle) {
        return bundle.getParcelable(DATA);
    }

    /**
     * Custom methods
     */

    public static Intent newDatePayloadIntent(final Intent intent, final Date date) {
        intent.putExtra(DATE, date);
        return intent;
    }

    public static Date getPayloadDate(final Intent intent) {
        return (Date) intent.getSerializableExtra(DATE);
    }

    public static <T extends Parcelable> Bundle newDatePayload(final Bundle bundle, final Date date) {
        bundle.putSerializable(DATE, date);
        return bundle;
    }

    public static Date getPayloadDate(final Bundle bundle) {
        return (Date) bundle.getSerializable(DATE);
    }
}
