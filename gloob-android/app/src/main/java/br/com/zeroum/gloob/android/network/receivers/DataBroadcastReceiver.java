package br.com.zeroum.gloob.android.network.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import static br.com.zeroum.gloob.android.network.managers.BroadcastManager.getPayload;

/**
 * Created by zeroum on 4/13/15.
 */
public final class DataBroadcastReceiver<T extends Parcelable> extends BroadcastReceiver {

    private final Command<T> command;

    public DataBroadcastReceiver(Command<T> command) {
        this.command = command;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        T payload = getPayload(intent);
        this.command.execute(context, payload);
    }

    public static <T extends Parcelable> DataBroadcastReceiver<T> newDataBroadcastReceiver(Command<T> command) {
        return new DataBroadcastReceiver<T>(command);
    }

}
