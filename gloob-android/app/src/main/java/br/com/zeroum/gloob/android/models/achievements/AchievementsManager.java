package br.com.zeroum.gloob.android.models.achievements;

import android.content.BroadcastReceiver;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.models.achievements.repositories.RulesRepository;
import br.com.zeroum.gloob.android.models.achievements.rules.BaseRule;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.receivers.Command;

import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;

/**
 * Created by zeroum on 4/17/15.
 */
public class AchievementsManager {
    private final BroadcastManager broadcastManager;
    private List<Achievement> unlockedAchievementQueue;

    public static boolean queueInExecution = false;

    private AchievementsManager() {
        broadcastManager = new BroadcastManager();
        unlockedAchievementQueue = new ArrayList<>();

        broadcastManager.registerLocalBroadcastReceiver(newEventRegisteredReceiver, BroadcastManager.Action.NEW_EVENT_REGISTERED);
        broadcastManager.registerLocalBroadcastReceiver(achievementQueueNextAchievementReceveier, BroadcastManager.Action.ACHIEVEMENT_UNLOCKED_DISMISSED);
    }

    private void processQueue() {
        if (unlockedAchievementQueue.size() > 0 && !queueInExecution) {
            Achievement achievement = unlockedAchievementQueue.get(0);
            achievement.setAvatarUsed(Profile.getCurrentProfile().getCurrentAvatar().getIcon());
            broadcastManager.sendLocalBroadcast(BroadcastManager.Action.NEW_ACHIEVEMENT_UNLOCKED, (achievement.isKindAvatar() ? achievement.toAvatar() : achievement.toSelfie()));
            queueInExecution = true;
        }
    }

    public void removeFromQueue(Achievement achievement) {
        for (Achievement unlocked : unlockedAchievementQueue) {
            if (unlocked.getId() == achievement.getId()) {
                unlockedAchievementQueue.remove(unlocked);
                break;
            }
        }
    }

    private static class SingletonHolder {
        public static final AchievementsManager INSTANCE = new AchievementsManager();
    }

    public static AchievementsManager getInstance() { return SingletonHolder.INSTANCE; }

    private BroadcastReceiver newEventRegisteredReceiver = newDataBroadcastReceiver(new Command<Event>() {
        @Override
        public void execute(Context context, Event event) {
            List<BaseRule> rules = RulesRepository.getInstance().getRulesByCondition(event.getCondition());

            for(BaseRule rule : rules) {
                if (rule.isValid()) {
                    for (Achievement achievement : rule.getAchievements()) {
                        if (!achievement.isUnlocked()) {
                            if (achievement.isKindAvatar()) {
                                unlockedAchievementQueue.add(achievement.toAvatar());
                            } else {
                                unlockedAchievementQueue.add(achievement.toSelfie());
                            }

                            AchievementsRepository.getInstance().saveAchievementAsUnlocked(achievement);
                        }
                    }
                }
            }

            processQueue();
        }
    });

    private BroadcastReceiver achievementQueueNextAchievementReceveier = newDataBroadcastReceiver(new Command<Achievement>() {
        @Override
        public void execute(Context context, Achievement data) {
            queueInExecution = false;
            processQueue();
        }
    });
}
