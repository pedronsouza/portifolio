package br.com.zeroum.gloob.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zeroum on 4/8/15.
 */
public class Season implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("program")
    @Expose
    private int programId;
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("number")
    @Expose
    private int number;

    private List<Episode> episodes;

    public int getCmsId() {
        return cmsId;
    }

    public void setCmsId(int cmsId) {
        this.cmsId = cmsId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @SerializedName("id_cms")
    private int cmsId;

    public Season() {
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeInt(this.programId);
        dest.writeInt(this.year);
        dest.writeString(this.slug);
        dest.writeInt(this.number);
        dest.writeInt(this.cmsId);
    }

    protected Season(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.programId = in.readInt();
        this.year = in.readInt();
        this.slug = in.readString();
        this.number = in.readInt();
        this.cmsId = in.readInt();
    }

    public static final Creator<Season> CREATOR = new Creator<Season>() {
        public Season createFromParcel(Parcel source) {
            return new Season(source);
        }

        public Season[] newArray(int size) {
            return new Season[size];
        }
    };
}
