package br.com.zeroum.gloob.android.fragments.achievements;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.support.Urls;

public class NewAvatarUnlockedFragment extends BaseAchievementUnlockedFragment {
    public final String TAG = "NewAvatarUnlockedFragment";

    @Override
    protected int getLayoutResourceId() {
        return super.getLayoutResourceId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public static NewAvatarUnlockedFragment newInstance(Avatar avatar) {
        NewAvatarUnlockedFragment fragment = new NewAvatarUnlockedFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_ACHIEVEMENT, avatar);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected Drawable imageForAchievement() {
        return mAchievement.toAvatar().getImageForUnlock();
    }

    @Override
    protected String textForMainButton() {
        return "MUDAR AVATAR";
    }

    @Override
    protected String textForMessage() {
        return getActivity().getString(R.string.new_avatar_message);
    }

    @Override
    protected void mainButtonClick() {
        Profile profile = Profile.getCurrentProfile();

        if (profile != null) {
            TrackManager.getInstance().trackEvent(newAvatarEvent);
            profile.setCurrentAvatar(mAchievement.toAvatar());
            profile.save();
            Profile.setCurrentProfile(profile);
            mBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.CHANGE_AVATAR_RECEIVER);
            close();
        }
    }

    private TrackManager.ITrackableEvent newAvatarEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Mudar Avatar";
        }

        @Override
        public String getLabelForTrack() {
            String oldAvatar = Urls.slugify(Profile.getCurrentProfile().getCurrentAvatar().getName());
            String newAvatar = Urls.slugify(mAchievement.getName());

            return String.format("%s|%s", oldAvatar, newAvatar);
        }
    };
}
