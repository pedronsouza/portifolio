package br.com.zeroum.gloob.android.network.managers;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageRequest;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.network.requests.AuthRequest;
import br.com.zeroum.gloob.android.network.requests.EpisodeRequest;
import br.com.zeroum.gloob.android.network.requests.GamesRequest;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;
import br.com.zeroum.gloob.android.network.requests.MosaicRequest;
import br.com.zeroum.gloob.android.network.requests.MostViewedRequest;
import br.com.zeroum.gloob.android.network.requests.ProgramEpisodesRequest;
import br.com.zeroum.gloob.android.network.requests.ProgramsRequest;
import br.com.zeroum.gloob.android.network.requests.ScheduleRequest;
import br.com.zeroum.gloob.android.network.requests.SearchRequest;
import br.com.zeroum.gloob.android.network.requests.objects.AuthRequestObject;
import br.com.zeroum.gloob.android.network.volley.ObserverRequests;
import br.com.zeroum.gloob.android.network.volley.VolleySingleton;


/**
 * Created by zeroum on 4/8/15.
 */
public class VolleyManager {
    final private List<String> nonCancellableRequests = new ArrayList<String>() {{
        /* Adicione as requests que serão ignoradas nas chamadas de cancelmento
         * ex: add(VolleyTags.REGISTRATION);
         */
    }};

    final private ObserverRequests observerRequests = ObserverRequests.getInstance();

    // Variavel utilizada para adicionar a fila de requests no Volley
    final private RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();

    /**
    * ALL REQUEST SHOULD BE DECLARED OVER HERE!
    * */

    public void getAuthToken(AuthRequestObject requestObject) {
        AuthRequest request = new AuthRequest(requestObject);
        addToRequestQueue(request.getRequest());
    }

    public void getGames() {
        GamesRequest request = new GamesRequest();
        addToRequestQueue(request.getRequest());
    }

    public void getSearchResults(String query, int pageIndex) {
        SearchRequest request = new SearchRequest(query, pageIndex);
        addToRequestQueue(request.getRequest());
    }

    public void getMostViewedVideos() {
        MostViewedRequest request = new MostViewedRequest();
        addToRequestQueue(request.getRequest());
    }

    public void getMosaicData(int pageIndex) {
        MosaicRequest request = new MosaicRequest(pageIndex);
        addToRequestQueue(request.getRequest());
    }

    public void getPrograms(int pageIndex) {
        ProgramsRequest request = new ProgramsRequest(pageIndex);
        addToRequestQueue(request.getRequest());
    }

    public void getProgramsEpisodes(int programId, int pageIndex) {
        ProgramEpisodesRequest request = new ProgramEpisodesRequest(programId, pageIndex);
        addToRequestQueue(request.getRequest());
    }

    public void getEpisodeById(int episodeId) {
        EpisodeRequest request = new EpisodeRequest(episodeId);
        addToRequestQueue(request.getRequest());
    }

    public void getSchedule(int fiveDaysRequest, String program) {
        final ScheduleRequest scheduleRequest = new ScheduleRequest(fiveDaysRequest, program);
        addToRequestQueue(scheduleRequest.getRequest());
    }

    private void addToRequestQueue(final Request request) {
        observerRequests.addToObserver(request);
        requestQueue.add(request);
    }

    private <T> String preparePostParams(T params) {
        return GsonProvider.getGson().toJson(params);
    }

    /**
     * Cancela todas as Requests
     */
    public void cancelAllRequests() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return cancellableRequestFilter((String) request.getTag());
            }
        });
    }

    /**
     * Cancela todas as requests que não sejam Imagens
     */
    public void cancelAllNonImageRequests() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                if (request instanceof ImageRequest)
                    return false;
                return cancellableRequestFilter((String) request.getTag());
            }
        });
    }

    /**
     * Previne que requests especificas não sejam canceladas
     *
     * @param requestTag
     * @return
     */
    private boolean cancellableRequestFilter(final String requestTag) {
        if (requestTag != null) {
            for (String nonCancellableRequest : nonCancellableRequests) {
                if (requestTag.equals(nonCancellableRequest))
                    return false;
            }
        }
        return true;
    }
}
