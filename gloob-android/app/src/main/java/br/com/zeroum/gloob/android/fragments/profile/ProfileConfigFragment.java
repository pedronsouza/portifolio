package br.com.zeroum.gloob.android.fragments.profile;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;

import com.adobe.mediacore.info.Track;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.CreateProfileActivity;
import br.com.zeroum.gloob.android.activities.ProfileActivity;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.ProfileAvatarThumb;

public class ProfileConfigFragment extends BaseProfileFragment {
    private static final String PROFILES_TITLE_FORMAT = "PERFIS DA CONTA (%d/3)";
    private GloobTextView mName;
    private GloobTextView mEmail;
    private GloobTextView mProvider;
    private GloobTextView mProfilesTitle;
    private LinearLayout mProfilesHolder;
    private LinearLayout mAddProfileHolder;

    public static ProfileConfigFragment newInstance() {
        ProfileConfigFragment fragment = new ProfileConfigFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileConfigFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_profile_config;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mName = Finder.findViewById(mView, R.id.fragment_profile_config_user_name);
        mEmail = Finder.findViewById(mView, R.id.fragment_profile_config_user_email);
        mProvider = Finder.findViewById(mView, R.id.fragment_profile_config_user_provider);
        mProfilesTitle = Finder.findViewById(mView, R.id.fragment_profile_config_profiles_title);
        mProfilesHolder = Finder.findViewById(mView, R.id.fragment_profile_config_profiles_holder);
        mAddProfileHolder = Finder.findViewById(mView, R.id.fragment_select_profile_bt_add_holder);

        mView.findViewById(R.id.fragment_select_profile_bt_add_profile).setOnClickListener(addProfileClickListener);
        mView.findViewById(R.id.fragment_profile_config_logout).setOnClickListener(logoutClickListener);

        Session currentSession = Session.getCurrentSession();
        mName.setText(currentSession.getUser().getUserName());
        mEmail.setText(currentSession.getUser().getEmail());
        mProvider.setText(currentSession.getUser().getProvider());
        setupProfiles();
    }

    private void setupProfiles() {
        List<Profile> allProfiles = Profile.getAllProfiles();
        mProfilesTitle.setText(String.format(PROFILES_TITLE_FORMAT, allProfiles.size()));

        mProfilesHolder.removeAllViews();

        if (allProfiles.size() == Profile.MAX_PROFILES) {
            mAddProfileHolder.setVisibility(View.GONE);
        } else {
            mAddProfileHolder.setVisibility(View.VISIBLE);
        }

        for (Profile profile : allProfiles) {
            ProfileAvatarThumb thumb = new ProfileAvatarThumb(mActivity, profile);
            thumb.setOnProfileChangesListener(onProfileChanges);
            mProfilesHolder.addView(thumb);
        }
    }

    private final View.OnClickListener addProfileClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent newProfileIntent = new Intent(mActivity, CreateProfileActivity.class);
            newProfileIntent.putExtra(CreateProfileActivity.START_ON_CREATION_KEY, true);
            startActivityForResult(newProfileIntent, ProfileActivity.REQUEST_CODE_CREATE_NEW_PROFILE);
        }
    };

    private final ProfileAvatarThumb.OnProfileChanges onProfileChanges = new ProfileAvatarThumb.OnProfileChanges() {
        @Override
        public void hasChanged() {
            setupProfiles();
        }
    };

    private final View.OnClickListener logoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
            dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Session.signOut();
                    mActivity.finish();
                }
            });

            dialog.setTitle("Deseja sair da conta?");

            dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/configuracoes";
        }
    };
}
