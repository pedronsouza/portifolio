package br.com.zeroum.gloob.android.network.requests.objects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zeroum on 4/13/15.
 */
public interface IRequestObject<T> {
    T parseResponse(JSONObject json) throws JSONException;
}
