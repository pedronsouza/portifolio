package br.com.zeroum.gloob.android.network.volley;

import android.graphics.Bitmap;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import br.com.zeroum.gloob.android.network.volley.cache.ImageCacheManager;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/8/15.
 */
public class VolleySingleton {
    private static int DISK_IMAGECACHE_SIZE = 1024 * 1024 * 10;
    private static Bitmap.CompressFormat DISK_IMAGECACHE_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;
    private static int DISK_IMAGECACHE_QUALITY = 50;  //PNG is lossless so quality is ignored but must be provided

    static private VolleySingleton mInstance = null;
    protected RequestQueue requestQueue;
    protected ImageLoader imageLoader;

    protected VolleySingleton() {

        requestQueue = Volley.newRequestQueue(application());
        createImageCache(requestQueue);
        imageLoader = ImageCacheManager.getInstance().getImageLoader();
    }

    public static VolleySingleton getInstance() {
        if (mInstance == null)
            mInstance = new VolleySingleton();

        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        return this.requestQueue;
    }

    public ImageLoader getImageLoader() {
        return this.imageLoader;
    }

    /**
     * Create the image cache. Uses Memory Cache by default. Change to Disk for a Disk based LRU implementation.
     *
     * @param requestQueue
     */
    private void createImageCache(final RequestQueue requestQueue) {
        ImageCacheManager.getInstance().init(
                requestQueue,
                application().getApplicationContext(),
                application().getPackageCodePath()
                , DISK_IMAGECACHE_SIZE
                , DISK_IMAGECACHE_COMPRESS_FORMAT
                , DISK_IMAGECACHE_QUALITY
                , ImageCacheManager.CacheType.MEMORY);
    }
}
