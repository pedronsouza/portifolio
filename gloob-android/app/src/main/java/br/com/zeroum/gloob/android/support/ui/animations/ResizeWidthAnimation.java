package br.com.zeroum.gloob.android.support.ui.animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;

/**
 * Created by zeroum on 7/14/15.
 */
public class ResizeWidthAnimation extends Animation {
    private float mWidth;
    private float mStartWidth;
    private View mView;

    public ResizeWidthAnimation(View view, float width)
    {
        mView = view;
        mWidth = width;
        setInterpolator(new LinearInterpolator());
        mStartWidth = view.getWidth();
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t)
    {
        float newWidth = mStartWidth + (int) ((mWidth - mStartWidth) * interpolatedTime);
        mView.getLayoutParams().width = Math.round(newWidth);
        mView.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight)
    {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds()
    {
        return true;
    }
}
