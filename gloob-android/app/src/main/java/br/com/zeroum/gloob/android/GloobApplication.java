package br.com.zeroum.gloob.android;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.newrelic.agent.android.NewRelic;

import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.AchievementsManager;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.models.achievements.repositories.RulesRepository;

/**
 * Created by zeroum on 7/13/15.
 */
public class GloobApplication extends Application {
    public static final String SHAREDPREFERENCS_APP = "GloobApplication.SHAREDPREFERENCS_APP";
    private static GloobApplication mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        Crashlytics.start(this);
        NewRelic.withApplicationToken("AA687991fe55d164aaaee288be3be954fdf899f83e").start(this);
        mContext = this;

        RulesRepository.getInstance();
        AchievementsRepository.getInstance();
        RulesRepository.getInstance().setupAchievementsForEachRules();
        AchievementsManager.getInstance();
        TrackManager.getInstance();
    }

    public static GloobApplication application() {
        return mContext;
    }
}
