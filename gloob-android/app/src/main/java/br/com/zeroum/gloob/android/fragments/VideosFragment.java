package br.com.zeroum.gloob.android.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.CreateProfileActivity;
import br.com.zeroum.gloob.android.activities.DispatcherActivity;
import br.com.zeroum.gloob.android.activities.GloobActivity;
import br.com.zeroum.gloob.android.activities.MainActivity;
import br.com.zeroum.gloob.android.activities.PlayerActivity;
import br.com.zeroum.gloob.android.adapters.GridVideosAdapter;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.ui.widget.ExpandableHeightGridView;
import br.com.zeroum.gloob.android.support.ui.widget.ThumbNetworkImageView;

import static br.com.zeroum.gloob.android.GloobApplication.application;

public class VideosFragment extends GloobFragment {

    private List<Episode> mRecentEpisodes;
    private List<Episode> mTopEpisodes;
    private ExpandableHeightGridView mGridView;
    private GridVideosAdapter mAdapter;

    public static VideosFragment newInstance() {
        VideosFragment fragment = new VideosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_videos;
    }

    public VideosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecentEpisodes = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_MOSAIC_KEY, Episode.class);
        mTopEpisodes = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_MOSTVIEWD_KEY, Episode.class);

        mGridView = Finder.findViewById(mView, R.id.fragment_videos_gridview_most_recent);

        mView.findViewById(R.id.framgnet_videos_see_all_programs).setOnClickListener(seeAllCharactersClickListener);

        setupPodium();
        setupGridView();
    }

    private void setupGridView() {
        mAdapter = new GridVideosAdapter(getActivity(), mRecentEpisodes);
        mGridView.setAdapter(mAdapter);
        mGridView.setExpanded(true);
    }

    private void setupPodium() {
        for (int i = 1; i <= mTopEpisodes.size(); i++) {
            Episode ep = mTopEpisodes.get((i - 1));
            String imageThumbResource = String.format("fragment_videos_gridview_most_recent_podium_%d", i);
            int imgResId = getActivity().getResources().getIdentifier(imageThumbResource, "id", application().getPackageName());

            ThumbNetworkImageView podiumImage = Finder.findViewById(mView, imgResId);

            if (podiumImage != null) {
                podiumImage.setImageUrl(ep.getImage());
                podiumImage.setTag((i - 1));
                podiumImage.setOnClickListener(playerClickListener);
                podiumImage.setLabel(String.format("Ep. %d", ep.getNumber()));
            }
        }
    }

    private final View.OnClickListener playerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Session.isSignedIn() && Profile.hasProfileInCurrentSession()) {
                int index = (int) v.getTag();
                Episode episode = mTopEpisodes.get(index);
                Intent player = new Intent(getActivity(), PlayerActivity.class);
                ArrayList<Episode> episodesList = new ArrayList();

                for (Episode e : mTopEpisodes) {
                    episodesList.add(e);
                }

                player.putExtra(PlayerActivity.EPISODE_EXTRA_KEY, episode);
                player.putParcelableArrayListExtra(PlayerActivity.EPISODES_LIST_EXTRA_KEY, episodesList);
                player.putExtra(PlayerActivity.EPISODE_RELATED_VIDOS_TITLE_KEY, String.format("VÍDEOS MAIS VISTOS (%d)", episodesList.size()));

                getActivity().startActivity(player);
            } else {
                getActivity().startActivity(new Intent(getActivity(), CreateProfileActivity.class));
            }
        }
    };

    private final View.OnClickListener seeAllCharactersClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            changeToFragment(R.id.activity_main_frags_container, CharactersFragment.newInstance(), true);
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/videos";
        }
    };
}
