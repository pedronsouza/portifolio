package br.com.zeroum.gloob.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import br.com.zeroum.gloob.android.network.requests.GsonProvider;
import br.com.zeroum.gloob.android.network.requests.Provider;
import br.com.zeroum.gloob.android.support.GloobPreferences;

/**
 * Created by zeroum on 7/15/15.
 */
public class Session implements Parcelable {
    private static final String CURRENT_SESSSION_KEY = "Session.CURRENT_SESSSION_KEY";

    @SerializedName("usuario")
    @Expose
    private User user;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("expires_in")
    @Expose
    private Integer expiresIn;
    @SerializedName("autorizador")
    @Expose
    private Provider provider;
    private Date expiresAt;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Date getExpiresAt() {
        if (expiresAt == null) {
            expiresAt = new Date(expiresIn);
        }

        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public void startNewSession() {
        expiresAt = new Date(expiresIn);
        GloobPreferences.getInstance().putEntity(CURRENT_SESSSION_KEY, this);
    }

    public static Session getCurrentSession() {
        Session currentSession = new Session();
        currentSession = GloobPreferences.getInstance().getEntity(CURRENT_SESSSION_KEY, Session.class);
        return currentSession;
    }

    public Session() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.user, 0);
        dest.writeString(this.accessToken);
        dest.writeValue(this.expiresIn);
        dest.writeParcelable(this.provider, flags);
        dest.writeLong(expiresAt != null ? expiresAt.getTime() : -1);
    }

    protected Session(Parcel in) {
        this.user = in.readParcelable(User.class.getClassLoader());
        this.accessToken = in.readString();
        this.expiresIn = (Integer) in.readValue(Integer.class.getClassLoader());
        this.provider = in.readParcelable(Provider.class.getClassLoader());
        long tmpExpiresAt = in.readLong();
        this.expiresAt = tmpExpiresAt == -1 ? null : new Date(tmpExpiresAt);
    }

    public static final Creator<Session> CREATOR = new Creator<Session>() {
        public Session createFromParcel(Parcel source) {
            return new Session(source);
        }

        public Session[] newArray(int size) {
            return new Session[size];
        }
    };

    public static boolean isSignedIn() {
        return (GloobPreferences.getInstance().getEntity(CURRENT_SESSSION_KEY, Session.class) != null);
    }

    public static void signOut() {
        GloobPreferences.getInstance().removeEntity(CURRENT_SESSSION_KEY);
    }
}
