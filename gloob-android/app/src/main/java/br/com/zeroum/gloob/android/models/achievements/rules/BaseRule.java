package br.com.zeroum.gloob.android.models.achievements.rules;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.models.achievements.Achievement;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.models.achievements.Selfie;

/**
 * Created by zeroum on 4/15/15.
 */
public class BaseRule implements IRule, Parcelable {
    @SerializedName("id")
    @Expose
    protected Integer id;
    @SerializedName("condition")
    @Expose
    protected String condition;
    @SerializedName("description")
    @Expose
    protected String description;
    @SerializedName("interval")
    @Expose
    protected Integer interval;
    @SerializedName("event_amount")
    @Expose
    protected Integer eventAmount;
    @SerializedName("avatars")
    @Expose
    protected List<Integer> avatarsRaw;
    @SerializedName("selfies")
    @Expose
    protected List<Integer> selfiesRaw;
    @SerializedName("start_date")
    protected String start_date;
    @SerializedName("program_id")
    @Expose
    protected String program_id;
    @SerializedName("end_date")
    protected String end_date;

    protected List<Selfie> relatedSelfies;
    protected List<Avatar> relatedAvatars;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public String getProgramId() {
        return program_id;
    }

    public String getStartDate() { return start_date; }

    public String getEndDate() { return end_date; }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getEventAmount() {
        return eventAmount;
    }

    public void setEventAmount(Integer eventAmount) {
        this.eventAmount = eventAmount;
    }

    public List<Avatar> getAvatars() {
        return relatedAvatars;
    }

    public List<Achievement> getAchievements() {
        List<Achievement> achievementList = new ArrayList<>();

        if(this.relatedAvatars == null) this.relatedAvatars = new ArrayList<>();
        if(this.relatedSelfies == null) this.relatedSelfies = new ArrayList<>();

        achievementList.addAll(this.relatedAvatars);
        achievementList.addAll(this.relatedSelfies);
        return achievementList;
    }

    public void setAvatars(List<Avatar> avatars) {
        this.relatedAvatars = avatars;
    }

    public List<Selfie> getSelfies() {
        return relatedSelfies;
    }

    public void setSelfies(List<Selfie> selfies) {
        this.relatedSelfies = selfies;
    }

    public List<Integer> getAvatarsRaw() {
        return avatarsRaw;
    }

    public List<Integer> getSelfiesRaw() {
        return selfiesRaw;
    }

    @Override
    public boolean isValid() {
        return this.isValid();
    }

    @Override
    public boolean shouldMark() {
        return this.shouldMark();
    }

    public enum Condition {
        DATE("date"),
        CARNAVAL("carnaval"),
        BIRTHDAY("birthday"),
        BOOKMARK("bookmark"),
        ACCESS("access"),
        MARK("mark"),
        PLAY("play"),
        VIEW("views"),
        SEARCH("search"),
        VIDEO("video");

        private String value;

        private Condition(String _value) {
            value = _value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.condition);
        dest.writeString(this.description);
        dest.writeValue(this.interval);
        dest.writeValue(this.eventAmount);
        dest.writeList(this.avatarsRaw);
        dest.writeList(this.selfiesRaw);
        dest.writeString(this.start_date);
        dest.writeString(this.program_id);
        dest.writeString(this.end_date);
    }

    public BaseRule() {
    }

    protected BaseRule(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.condition = in.readString();
        this.description = in.readString();
        this.interval = (Integer) in.readValue(Integer.class.getClassLoader());
        this.eventAmount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.avatarsRaw = new ArrayList<Integer>();
        in.readList(this.avatarsRaw, List.class.getClassLoader());
        this.selfiesRaw = new ArrayList<Integer>();
        in.readList(this.selfiesRaw, List.class.getClassLoader());
        this.start_date = in.readString();
        this.program_id = in.readString();
        this.end_date = in.readString();
    }

    public static final Parcelable.Creator<BaseRule> CREATOR = new Parcelable.Creator<BaseRule>() {
        public BaseRule createFromParcel(Parcel source) {
            return new BaseRule(source);
        }

        public BaseRule[] newArray(int size) {
            return new BaseRule[size];
        }
    };
}