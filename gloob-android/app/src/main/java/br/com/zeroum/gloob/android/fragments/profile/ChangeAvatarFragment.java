package br.com.zeroum.gloob.android.fragments.profile;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.SelectAvatarView;

public class ChangeAvatarFragment extends BaseProfileFragment {
    private final BroadcastManager broadcastManager = new BroadcastManager();
    private LinearLayout mAvatarHolders;
    private ImageView mCurrentAvatar;
    private EditText mProfileName;
    private Avatar mSelectedAvatar;

    public static ChangeAvatarFragment newInstance() {
        ChangeAvatarFragment fragment = new ChangeAvatarFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public ChangeAvatarFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        mSelectedAvatar = mCurrentProfile.getCurrentAvatar();
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_change_avatar;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAvatarHolders = Finder.findViewById(mView, R.id.fragment_change_avatar_bt_avatar_holder);
        mCurrentAvatar = Finder.findViewById(mView, R.id.fragment_change_avatar_current_avatar);
        mProfileName = Finder.findViewById(mView, R.id.fragment_change_avatar_profile_name);

        mView.findViewById(R.id.fragment_change_avatar_bt_change).setOnClickListener(changeOnClickListener);
        mView.findViewById(R.id.fragment_change_avatar_bt_close).setOnClickListener(closeClickListener);

        List<Avatar> unlockedAvatars = AchievementsRepository.getInstance().getUnlockedAvatars();

        for (Avatar avatar : unlockedAvatars) {
            SelectAvatarView selectAvatarView = new SelectAvatarView(mActivity, avatar, true);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            selectAvatarView.setLayoutParams(params);

            if (mCurrentProfile.getCurrentAvatar().getIcon().equals(avatar.getIcon())) {
                selectAvatarView.setSelected(true);
            }

            mAvatarHolders.addView(selectAvatarView);
            selectAvatarView.setOnSelectAvatarListener(onSelectAvatarListener);
        }

        mSelectedAvatar = mCurrentProfile.getCurrentAvatar();
    }

    @Override
    public void onResume() {
        super.onResume();

        mCurrentAvatar.setImageResource(mSelectedAvatar.getImageForProfileId());
        mProfileName.setText(mCurrentProfile.getName());
    }

    private final SelectAvatarView.OnSelectAvatarListener onSelectAvatarListener = new SelectAvatarView.OnSelectAvatarListener() {
        @Override
        public void onSelect(Avatar avatar) {
            mSelectedAvatar = avatar;
            mCurrentAvatar.setImageResource(mSelectedAvatar.getImageForProfileId());
        }
    };

    private final View.OnClickListener changeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean allValid = true;
            String name = mProfileName.getText().toString();
            allValid = (name.length() > 1) && allValid;
            allValid = (mSelectedAvatar != null);

            if (allValid) {
                TrackManager.getInstance().trackEvent(changeAvatarEvent);
                mCurrentProfile.setCurrentAvatar(mSelectedAvatar);
                mCurrentProfile.setName(name);
                mCurrentProfile.save();
                broadcastManager.sendLocalBroadcast(BroadcastManager.Action.CHANGE_AVATAR);
                mActivity.getSupportFragmentManager().popBackStack();
            } else {
                Snackbar.make(mView, mActivity.getString(R.string.new_profile_register_validation_message), Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mActivity.getSupportFragmentManager().popBackStack();
        }
    };

    private final TrackManager.ITrackableEvent changeAvatarEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Mudar Avatar";
        }

        @Override
        public String getLabelForTrack() {
            String oldAvatar = Urls.slugify(Profile.getCurrentProfile().getCurrentAvatar().getName());
            String newAvatar = Urls.slugify(mSelectedAvatar.getName());

            return String.format("%s|%s", oldAvatar, newAvatar);
        }
    };
}
