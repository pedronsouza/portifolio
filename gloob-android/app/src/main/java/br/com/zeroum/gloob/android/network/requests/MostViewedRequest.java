package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.MostViewed;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/15/15.
 */
public class MostViewedRequest implements Response.Listener<JSONArray>, Response.ErrorListener {
    private Request request;
    private BroadcastManager localBroadcastManager;

    public MostViewedRequest() {
        localBroadcastManager = new BroadcastManager();
        request = new JsonAuthenticatedArrayRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getTop5(), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        localBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_MOST_VIEWED_ERROR);
    }

    @Override
    public void onResponse(JSONArray jsonArray) {
        List<Episode> results = new ArrayList<>();
        try {
            Episode[] data = GsonProvider.getGson().fromJson(String.valueOf(jsonArray.getJSONObject(0).getJSONArray("episodes")), Episode[].class);

            for (Episode d : data) {
                results.add(d);
            }

            localBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_MOST_VIEWED_OK, results);
        } catch (JSONException e) {
            localBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_MOST_VIEWED_ERROR);
        }

    }

    public Request getRequest() {
        return request;
    }
}
