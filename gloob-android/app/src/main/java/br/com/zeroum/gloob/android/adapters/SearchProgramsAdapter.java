package br.com.zeroum.gloob.android.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.SearchResult;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.ThumbNetworkImageView;

/**
 * Created by zeroum on 7/27/15.
 */
public class SearchProgramsAdapter extends BaseAdapter {
    private List<SearchResult> mPrograms;
    private Context mContext;

    public SearchProgramsAdapter(Context context, List<SearchResult> programs) {
        mPrograms = programs;
        mContext = context;
    }

    public void setPrograms(List<SearchResult> programs) {
        mPrograms = programs;
    }

    @Override
    public int getCount() {
        return mPrograms.size();
    }

    @Override
    public Object getItem(int position) {
        return mPrograms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mPrograms.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (convertView == null) {
            v = View.inflate(mContext, R.layout.search_programs_list_item, null);
        }

        SearchResult program = mPrograms.get(position);
        Program p = Program.getCachedProgramById(program.getId());

        SimpleDraweeView image = Finder.findViewById(v, R.id.search_programs_list_item_image);
        GloobTextView title = Finder.findViewById(v, R.id.search_programs_list_item_name);

        image.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int width = image.getMeasuredWidth();
        int height = image.getMeasuredHeight();

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(p.getImage()))
                .setResizeOptions(new ResizeOptions(width, height))
                .setProgressiveRenderingEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(image.getController())
                .setImageRequest(request)
                .build();

        image.setController(controller);
        title.setText(program.getTitle());

        return v;
    }
}
