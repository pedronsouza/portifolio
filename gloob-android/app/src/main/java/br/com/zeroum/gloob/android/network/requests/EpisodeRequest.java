package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.objects.EpisodeRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/29/15.
 */
public class EpisodeRequest implements Response.Listener<JSONObject>, Response.ErrorListener {
    private BroadcastManager broadcastManager;
    private BaseRequest request;

    public EpisodeRequest(int episodeId) {
        broadcastManager = new BroadcastManager();
        request = new JsonAuthenticatedRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getEpisodeByIdUrl(episodeId), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.BAD_REQUEST_ERROR);
    }

    @Override
    public void onResponse(JSONObject jsonObject) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_EPISODE_BY_ID_OK, GsonProvider.getGson().fromJson(jsonObject.toString(), Episode.class));
    }

    public BaseRequest getRequest() {
        return request;
    }
}
