package br.com.zeroum.gloob.android.network.volley;

/**
 * Created by zeroum on 4/8/15.
 */

public enum Authorization {
    Basic("Basic"),
    Social("Social"),
    Token("Token");

    final public static String AUTHORIZATION = "Authorization";

    private final String authorizationType;
    private String authorizationValue;

    Authorization(String type) {
        authorizationType = type;
    }

    public void setAuthorizationValue(final String authorizationValue) {
        this.authorizationValue = authorizationValue;
    }

    public String getAuthorizationType() {
        return authorizationType;
    }

    public String getAuthorizationValue() {
        return authorizationValue;
    }

}
