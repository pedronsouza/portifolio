package br.com.zeroum.gloob.android.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.DispatcherActivity;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.ui.widget.MosaicView;
import br.com.zeroum.gloob.android.support.ui.widget.TwoDimensionScrollView;

public class HomeFragment extends GloobFragment {
    private LinearLayout mContainer;
    private TwoDimensionScrollView mMainScroll;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public HomeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContainer = Finder.findViewById(mView, R.id.fragment_home_mosaic_container);
        mMainScroll = Finder.findViewById(mView, R.id.fragment_home_main_scroll);

        addMosaicThumbs();
        mMainScroll.setOffsetAtCenter();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_home;
    }

    private void addMosaicThumbs() {
        final List<Game> allGames = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_GAMES_KEY, Game.class);
        final List<Episode> allVideos = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_MOSAIC_KEY, Episode.class);

        for (int i = 0; i < MosaicView.MOSAIC_TOTAL_REPETITION; i++) {
            List<Game> mosaicGames = new ArrayList<>();
            List<Episode> mosaicVideos = new ArrayList<>();

            for (int indexGames = 0; indexGames < MosaicView.MOSAIC_TOTAL_GAMES_THUMB; indexGames++) {
                mosaicGames.add(allGames.get(indexGames));
            }

            for (int indexVideos = 0; indexVideos < MosaicView.MOSAIC_TOTAL_VIDEOS_THUMB; indexVideos++) {
                mosaicVideos.add(allVideos.get(indexVideos));
            }

            MosaicView mosaicView = new MosaicView(getActivity(), mosaicGames, mosaicVideos);
            mContainer.addView(mosaicView);

            allGames.removeAll(mosaicGames);
            allVideos.removeAll(mosaicVideos);
        }
    }
}
