package br.com.zeroum.gloob.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.profile.BaseProfileFragment;
import br.com.zeroum.gloob.android.fragments.profile.NewProfileRegisterFragment;
import br.com.zeroum.gloob.android.fragments.profile.ProfileConfigFragment;
import br.com.zeroum.gloob.android.fragments.profile.ProfileFragment;
import br.com.zeroum.gloob.android.fragments.profile.UserAlertsFragment;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.achievements.EventLog;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.GloobPreviousArrowButton;

public class ProfileActivity extends GloobActivity {
    public static final int REQUEST_CODE_CREATE_NEW_PROFILE = 300;
    public static final String START_AT_ALERTS = "START_AT_ALERTS";

    private ImageButton mProfileTabButton;
    private ImageButton mAlertsTabButton;
    private ImageButton mConfigTabButton;
    private GloobPreviousArrowButton mBackButton;
    private boolean shouldRebuilsProfileList;
    private boolean shouldStartAtAlerts;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_profile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        shouldRebuilsProfileList = (resultCode == NewProfileRegisterFragment.ACTIVITY_RESULT_NEW_PROFILE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProfileTabButton = Finder.findViewById(this, R.id.activity_profile_tab_profile);
        mAlertsTabButton = Finder.findViewById(this, R.id.activity_profile_tab_alerts);
        mConfigTabButton = Finder.findViewById(this, R.id.activity_profile_tab_config);
        mBackButton = Finder.findViewById(this, R.id.new_profile_second_step_back);

        mProfileTabButton.setOnClickListener(profileTabClickListener);
        mAlertsTabButton.setOnClickListener(alertsTabClickListener);
        mConfigTabButton.setOnClickListener(configTabClickListener);
        mBackButton.setOnClickListener(backButtonClickListener);

        shouldStartAtAlerts = getIntent().getBooleanExtra(START_AT_ALERTS, false);

        if (shouldStartAtAlerts) {
            mAlertsTabButton.setSelected(true);
            changeToFragment(getFragmentContainerId(), UserAlertsFragment.newInstance(), false);
        } else {
            mProfileTabButton.setSelected(true);
            changeToFragment(getFragmentContainerId(), ProfileFragment.newInstance(), false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (shouldRebuilsProfileList) {
            changeToFragment(getFragmentContainerId(), ProfileConfigFragment.newInstance(), false);

            if (Session.isSignedIn()) {
                EventLog.getInstance().logAccessEvent();
                EventLog.getInstance().logDateEvent();
                EventLog.getInstance().logCarnivalRule();
                EventLog.getInstance().logMarkRule();
            }
        }
    }

    public void changeToFragment(BaseProfileFragment fragment) {
        super.changeToFragment(getFragmentContainerId(), fragment, true);
    }

    public int getFragmentContainerId() {
        return R.id.activity_profile_frags_container;
    }

    private void unselectAllTabs() {
        mProfileTabButton.setSelected(false);
        mAlertsTabButton.setSelected(false);
        mConfigTabButton.setSelected(false);
    }

    private View.OnClickListener profileTabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            unselectAllTabs();
            v.setSelected(true);
            changeToFragment(getFragmentContainerId(), ProfileFragment.newInstance(), true);
        }
    };

    private View.OnClickListener alertsTabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            unselectAllTabs();
            v.setSelected(true);
            changeToFragment(getFragmentContainerId(), UserAlertsFragment.newInstance(), true);
        }
    };

    private View.OnClickListener configTabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            unselectAllTabs();
            v.setSelected(true);
            changeToFragment(getFragmentContainerId(), ProfileConfigFragment.newInstance(), true);
        }
    };

    private View.OnClickListener backButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
