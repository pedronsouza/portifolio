package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.auditude.ads.model.smil.Par;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.CreateProfileActivity;
import br.com.zeroum.gloob.android.activities.GameStageActivity;
import br.com.zeroum.gloob.android.activities.PlayerActivity;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 7/16/15.
 */
public class MosaicView extends RelativeLayout {
    public static final int MOSAIC_MAX_VIDEOS = 45;
    public static final int MOSAIC_TOTAL_VIDEOS_THUMB = 15;
    public static final int MOSAIC_TOTAL_GAMES_THUMB = 3;
    public static final int MOSAIC_TOTAL_REPETITION = 3;

    private List<Game> mGames;
    private List<Episode> mVideos;
    private Context mContext;

    public MosaicView(Context context, List<Game> games, List<Episode> videos) {
        super(context);
        mGames = games;
        mVideos = videos;

        init(context, null);
    }

    public MosaicView(Context context) {
        super(context);
        init(context, null);
    }

    public MosaicView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MosaicView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MosaicView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }


    public List<Game> getGames() {
        return mGames;
    }
    public void setGames(List<Game> mGames) { this.mGames = mGames; }
    public List<Episode> getVideos() {
        return mVideos;
    }
    public void setVideos(List<Episode> mVideos) {
        this.mVideos = mVideos;
    }

    private void init(Context context, AttributeSet attrs) {
        mContext = context;
        inflate(context, R.layout.mosaic_page, this);

        if (mGames.size() == MOSAIC_TOTAL_GAMES_THUMB || mVideos.size() == MOSAIC_TOTAL_VIDEOS_THUMB) {
            for (int i = 0; i < mGames.size(); i++) {
                int resIdIndex = i + 1;

                Game game = mGames.get(i);
                String imageThumbResource = String.format("game_frame_%d_img", resIdIndex);
                String gamesHolderResource = String.format("game_frame_%d", resIdIndex);
                String thumbLabels = String.format("game_frame_%d_txt", resIdIndex);

                int imgResId = context.getResources().getIdentifier(imageThumbResource, "id", application().getPackageName());
                int lblResId = context.getResources().getIdentifier(thumbLabels, "id", application().getPackageName());
                int gamesHolderid = context.getResources().getIdentifier(gamesHolderResource, "id", application().getPackageName());

                ThumbNetworkImageView imgView = Finder.findViewById(this, imgResId);
                GloobTextView lblView = Finder.findViewById(this, lblResId);
                RelativeLayout gamesHolder = Finder.findViewById(this, gamesHolderid);

                lblView.setText(game.getName());
                imgView.setImageUrl(game.getImage());
                imgView.setDetailsVisibility(View.GONE);
                imgView.setOnClickListener(gamesOnClickListener);
                gamesHolder.setOnClickListener(gamesOnClickListener);

                imgView.setTag(i);
                gamesHolder.setTag(i);
            }

            for (int x = 0; x < mVideos.size(); x++) {
                int restIdIndex = x + 1;
                Episode episode = mVideos.get(x);

                String imageThumbResource = String.format("ep_frame_%d", restIdIndex);

                int imgResId = context.getResources().getIdentifier(imageThumbResource, "id", application().getPackageName());
                ThumbNetworkImageView imgView = Finder.findViewById(this, imgResId);

                imgView.setImageUrl(episode.getImage());
                imgView.setLabel(String.format("Ep. %d", episode.getNumber()));
                imgView.setOnClickListener(videosOnClickListener);

                imgView.setTag(x);
            }

        }
    }

    private Game mCurrentGame;
    private final OnClickListener gamesOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = (int) v.getTag();
            Game game = mGames.get(index);
            mCurrentGame = game;
            TrackManager.getInstance().trackEvent(playGameEvent);
            Intent gameStageIntent = new Intent(mContext, GameStageActivity.class);
            gameStageIntent.putExtra(GameStageActivity.BUNDLE_GAME_KEY, game);

            mContext.startActivity(gameStageIntent);
        }
    };

    private Episode mCurrentEpisode;
    private final OnClickListener videosOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Session.isSignedIn() && Profile.hasProfileInCurrentSession()) {
                int index = (int) v.getTag();
                Episode episode = mVideos.get(index);
                mCurrentEpisode = episode;
                TrackManager.getInstance().trackEvent(playEpisodeEvent);
                Intent playerIntent = new Intent(mContext, PlayerActivity.class);

                playerIntent.putExtra(PlayerActivity.EPISODE_EXTRA_KEY, episode);
                playerIntent.putParcelableArrayListExtra(PlayerActivity.EPISODES_LIST_EXTRA_KEY, listToParcelableList(mVideos));
                playerIntent.putExtra(PlayerActivity.EPISODE_RELATED_VIDOS_TITLE_KEY, String.format("VÍDEOS MAIS NOVOS (%d)", mVideos.size()));
                mContext.startActivity(playerIntent);

            } else {
                mContext.startActivity(new Intent(mContext, CreateProfileActivity.class));
            }
        }
    };

    private ArrayList<Parcelable> listToParcelableList(List<Episode> list) {
        ArrayList<Parcelable> parcelableList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            parcelableList.add(list.get(i));
        }

        return parcelableList;
    }

    private final TrackManager.ITrackableEvent playEpisodeEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Play";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("home|vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mCurrentEpisode.getId()),
                    Urls.slugify(mCurrentEpisode.getCategory()),
                    Urls.slugify(mCurrentEpisode.getProgram().getTitle()),
                    (mCurrentEpisode.getSeason() == null) ? "" : String.valueOf(mCurrentEpisode.getSeason().getNumber()),
                    (mCurrentEpisode.getNumber() == 0) ? "" : String.valueOf(mCurrentEpisode.getNumber()),
                    Urls.slugify(mCurrentEpisode.getTitle()));

            return event;
        }
    };

    private final TrackManager.ITrackableEvent playGameEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Jogar";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("home|%s", Urls.slugify(mCurrentGame.getName()));
        }
    };
}
