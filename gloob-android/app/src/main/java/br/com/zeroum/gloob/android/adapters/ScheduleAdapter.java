package br.com.zeroum.gloob.android.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Schedule;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;

/**
 * Created by zeroum on 7/28/15.
 */
public class ScheduleAdapter extends BaseAdapter {
    private final Context mContext;
    private List<Schedule> mSchedules;


    public ScheduleAdapter(Context context, List<Schedule> schedules) {
        mSchedules = schedules;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mSchedules.size();
    }

    @Override
    public Object getItem(int position) {
        return mSchedules.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mSchedules.get(position).getNumber();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            v = View.inflate(mContext, R.layout.schedule_item_cell, null);
        }

        Schedule item = mSchedules.get(position);

        GloobTextView episodeName = Finder.findViewById(v, R.id.episode_name);
        GloobTextView episodeDate = Finder.findViewById(v, R.id.episode_date);
        GloobTextView episodeDescription = Finder.findViewById(v, R.id.episode_about);
        ImageButton toggleAlarm = Finder.findViewById(v, R.id.bt_toggle_alarm);

        if (item.hasAlarmSet(item.getProgramTitle())) {
            toggleAlarm.setBackgroundResource(R.drawable.remove_alarm);
        }

        toggleAlarm.setOnClickListener(toggleAlarmClickListener);
        toggleAlarm.setTag(position);

        episodeDescription.setText(item.getSinopse());
        episodeName.setText(String.format("EP.%d - %s", item.getNumber(), item.getTitle()));

        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date_obj = format.parse(item.getDate());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date_obj);

            String day = (cal.get(Calendar.DAY_OF_MONTH) < 10) ? "0"+cal.get(Calendar.DAY_OF_MONTH) : ""+cal.get(Calendar.DAY_OF_MONTH);
            String year = ""+cal.get(Calendar.YEAR);

            String hour = (cal.get(Calendar.HOUR_OF_DAY) < 10) ? "0"+cal.get(Calendar.HOUR_OF_DAY) : ""+cal.get(Calendar.HOUR_OF_DAY);
            String minute = (cal.get(Calendar.MINUTE) < 10) ? "0"+cal.get(Calendar.MINUTE) : ""+cal.get(Calendar.MINUTE);

            String date_pattern = "%s, %s de %s de %s, as %s:%s";

            String [] week_days= mContext.getResources().getStringArray(R.array.week_days);
            String [] months= mContext.getResources().getStringArray(R.array.months);

            date_pattern = String.format(date_pattern, week_days[cal.get(Calendar.DAY_OF_WEEK)-1], day, months[cal.get(Calendar.MONTH)], year, hour, minute);
            episodeDate.setText(date_pattern);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return v;
    }

    private Schedule mCurrentSchedule;
    private View.OnClickListener toggleAlarmClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            Schedule item = mSchedules.get(position);
            mCurrentSchedule = item;

            if (item.hasAlarmSet(item.getProgramTitle())) {
                item.unsetAlarm();
                TrackManager.getInstance().trackEvent(removeAlarmEvent);
                        ((ImageButton) v).setBackgroundResource(R.drawable.bt_add_profile);
            } else {
                item.setAlarm();
                TrackManager.getInstance().trackEvent(addAlarmEvent);
                ((ImageButton) v).setBackgroundResource(R.drawable.remove_alarm);
            }
        }
    };

    private final TrackManager.ITrackableEvent removeAlarmEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Remover Alerta";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("remover|%s", Urls.slugify(mCurrentSchedule.getProgramTitle()));
        }
    };

    private final TrackManager.ITrackableEvent addAlarmEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Ativar Alerta";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("ativar|%s", Urls.slugify(mCurrentSchedule.getProgramTitle()));
        }
    };
}
