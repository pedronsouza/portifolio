package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import br.com.zeroum.gloob.android.models.Session;

/**
 * Created by zeroum on 4/29/15.
 */
public class XMLAuthenticatedRequest extends XMLRequest {

    public XMLAuthenticatedRequest(int requestMethod, Request.Priority priority, String url, Map<String, String> params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(requestMethod, priority, url, params, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.putAll(super.getHeaders());
        headers.put("Authorization", String.format("%s %s", "Token", Session.getCurrentSession().getAccessToken()));
        return headers;
    }
}
