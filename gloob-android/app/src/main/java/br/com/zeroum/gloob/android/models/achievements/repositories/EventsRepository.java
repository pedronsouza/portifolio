package br.com.zeroum.gloob.android.models.achievements.repositories;

import android.content.SharedPreferences;

import br.com.zeroum.gloob.android.GloobApplication;
import br.com.zeroum.gloob.android.models.achievements.Event;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;

import static android.content.Context.MODE_PRIVATE;
import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/17/15.
 */
public class EventsRepository {

    public Event getEventByKey(String key) {
        SharedPreferences preferences = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
        String eventJson = preferences.getString(key, null);

        if (eventJson != null) {
            return GsonProvider.getGson().fromJson(eventJson, Event.class);
        }

        return new Event();
    }

    public boolean saveEvent(String key, Event event) {
        SharedPreferences preferences = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(key, GsonProvider.getGson().toJson(event));
        return editor.commit();
    }


    private EventsRepository() {

    }

    private static class SingletonHolder {
        public static final EventsRepository INSTANCE = new EventsRepository();
    }

    public static EventsRepository getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
