package br.com.zeroum.gloob.android.network.requests.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zeroum on 7/15/15.
 */
public class AuthRequestObject {
    @SerializedName("grant_type")
    @Expose
    private final String grantType = "authorization_code";
    @SerializedName("redirect_uri")
    @Expose
    private final String redirectUri = "http://gloob/token/";
    @SerializedName("code")
    @Expose
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
