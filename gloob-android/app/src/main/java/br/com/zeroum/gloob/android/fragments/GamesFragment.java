package br.com.zeroum.gloob.android.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.DispatcherActivity;
import br.com.zeroum.gloob.android.activities.GameStageActivity;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.GenericGloobList;

public class GamesFragment extends GloobFragment {
    private LinearLayout mGameListHolder;
    private List<GenericGloobList.IGenericListItem> mGames;
    private Game mClickedGame;

    public static GamesFragment newInstance() {
        GamesFragment fragment = new GamesFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public GamesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_games;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGameListHolder = Finder.findViewById(mView, R.id.fragment_games_holder);

        mGames = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_GAMES_KEY, Game.class);
        GenericGloobList list = new GenericGloobList(getActivity(), mGames, thumbsClickListener, R.layout.generic_list_item_games, new int []{ R.drawable.jg_eng0, R.drawable.jg_eng1, R.drawable.jg_eng2 });

        mGameListHolder.addView(list);
    }


    private final View.OnClickListener thumbsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent gameStage = new Intent(getActivity(), GameStageActivity.class);
            int gameId = (int) v.getTag();

            for (GenericGloobList.IGenericListItem g : mGames) {
                if (g.getValueForTag() == gameId) {
                    mClickedGame = (Game)g;
                    gameStage.putExtra(GameStageActivity.BUNDLE_GAME_KEY, (Game)g);
                    TrackManager.getInstance().trackEvent(playGameEvent);
                    break;
                }
            }

            startActivity(gameStage);
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/jogos";
        }
    };

    private final TrackManager.ITrackableEvent playGameEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Jogar";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("jogos|%s", Urls.slugify(mClickedGame.getName()));
        }
    };
}
