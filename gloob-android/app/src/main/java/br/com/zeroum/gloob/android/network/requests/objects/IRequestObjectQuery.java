package br.com.zeroum.gloob.android.network.requests.objects;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

/**
 * Created by zeroum on 4/13/15.
 */
public interface IRequestObjectQuery<T> {
    T parseResponse(JSONObject obj, String query) throws XmlPullParserException;
}
