package br.com.zeroum.gloob.android.activities;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.globo.player.Player;
import com.globo.player.listeners.EventListener;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.GamesShortcutFragment;
import br.com.zeroum.gloob.android.models.Bookmark;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.EventLog;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.receivers.Command;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.SharedCache;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.PlayerScrollView;
import br.com.zeroum.gloob.android.support.ui.widget.RelatedVideoThumb;

import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;

public class PlayerActivity extends GloobActivity {
    public final static String EPISODE_EXTRA_KEY = "PlayerActivity.EPISODE_EXTRA_KEY";
    public final static String EPISODES_LIST_EXTRA_KEY = "PlayerActivity.EPISODES_LIST_EXTRA_KEY";
    public final static String EPISODE_RELATED_VIDOS_TITLE_KEY = "PlayerActivity.EPISODE_RELATED_VIDOS_TITLE_KEY";
    public static final String EPISODE_SHOULD_LOAD_KEY = "EPISODE_SHOULD_LOAD_KEY";
    public static final String EPISODE_ID_EXTRA_KEY = "EPISODE_ID_EXTRA_KEY";
    private static final String SEASON_INFO_FORMAT = "TEMP.%d - EP.%d";
    private static final String SAVED_INSTANCE_ISPAUSED_KEY = "PlayerActivity.SAVED_INSTANCE_ISPAUSED_KEY";

    private Episode mEpisode;
    private List<Episode> mEpisodes;
    private RelativeLayout mPlayerHolder;
    private SimpleDraweeView mProgramThumb;
    private GloobTextView mProgramName;
    private GloobTextView mEpisodeName;
    private GloobTextView mProgramDescription;
    private GloobTextView mOtherVideosTitle;
    private Player mPlayer;
    private ImageButton mBookmarkButton;
    private ImageButton mAboutButton;
    private ImageButton mPlayButton;
    private View mButtonsHolder;
    private PlayerScrollView mScrollView;
    private boolean alreadyMarked;
    private boolean shouldLoadEpisode;
    private ProgressDialog mDialog;
    private ImageButton mBackButton;
    private String mRelatedVideosTitle;
    private LinearLayout mRelatedVideosHolder;
    private HorizontalScrollView mRelatedVideosScroll;
    private boolean activityIsPaused = false;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_player;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            activityIsPaused = savedInstanceState.getBoolean(SAVED_INSTANCE_ISPAUSED_KEY, false);
        }

        Intent extras = getIntent();
        shouldLoadEpisode = extras.getBooleanExtra(EPISODE_SHOULD_LOAD_KEY, false);
        if (!shouldLoadEpisode) {
            mEpisode = extras.getParcelableExtra(EPISODE_EXTRA_KEY);
            mEpisodes = extras.getParcelableArrayListExtra(EPISODES_LIST_EXTRA_KEY);
            mRelatedVideosTitle = extras.getStringExtra(EPISODE_RELATED_VIDOS_TITLE_KEY);
            setupInterface();
        } else {
            int episodeId = extras.getIntExtra(EPISODE_ID_EXTRA_KEY, 0);
            mDialog = new ProgressDialog(this, R.style.AppTheme_SearchDialog);
            mDialog.show();
            mVolleyManager.getEpisodeById(episodeId);
        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(SAVED_INSTANCE_ISPAUSED_KEY, activityIsPaused);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityIsPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPlayer != null && !activityIsPaused)
            mPlayer.play();

        activityIsPaused = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBroadcastManager.registerLocalBroadcastReceiver(startPlayingReceiever, BroadcastManager.Action.START_PLAYING);
        mBroadcastManager.registerLocalBroadcastReceiver(episodeByIdReceiver, BroadcastManager.Action.GET_EPISODE_BY_ID_OK);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer = null;
        }

        mBroadcastManager.unregisterLocalBroadcastReceiver(startPlayingReceiever);
        mBroadcastManager.unregisterLocalBroadcastReceiver(episodeByIdReceiver);
    }

    private void setupInterface() {
        mPlayerHolder = Finder.findViewById(this, R.id.activity_player_player_holder);
        mProgramThumb = Finder.findViewById(this, R.id.activity_player_program_img);
        mProgramName = Finder.findViewById(this, R.id.activity_player_details_program_info_name);
        mProgramDescription = Finder.findViewById(this, R.id.activity_player_details_program_info_description);
        mEpisodeName = Finder.findViewById(this, R.id.activity_player_details_program_info_ep);
        mOtherVideosTitle = Finder.findViewById(this, R.id.activity_player_other_videos_title);
        mRelatedVideosHolder = Finder.findViewById(this, R.id.activity_player_videos_holder);
        mRelatedVideosScroll = Finder.findViewById(this, R.id.activity_player_related_videos_scroll);
        mScrollView = Finder.findViewById(this, R.id.activity_player_scroll_view);
        mScrollView.setScrollChangesListener(scrollListener);

        mButtonsHolder = Finder.findViewById(this, R.id.activity_player_buttons_holder);
        mBackButton = Finder.findViewById(this, R.id.activity_player_bt_back);
        mBookmarkButton = Finder.findViewById(this, R.id.activity_player_bt_bookmark);
        mAboutButton = Finder.findViewById(this, R.id.activity_player_bt_about);
        mPlayButton = Finder.findViewById(this, R.id.activity_player_bt_play);

        mBookmarkButton.setOnClickListener(bookmarkClickListener);
        mAboutButton.setOnClickListener(aboutClickListener);
        mPlayButton.setOnClickListener(playClickListener);

        mBookmarkButton.setSelected(mEpisode.isBookmarked());
        mBackButton.setOnClickListener(backButtonClickListener);

        findViewById(R.id.activity_player_details_see_characters).setOnClickListener(programClickListener);

        Program program = mEpisode.getProgram();

        if (program != null) {
            int width = (int) getResources().getDimension(R.dimen.player_image_thumb_width);
            int height = (int) getResources().getDimension(R.dimen.player_image_thumb_height);

            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(program.getImage()))
                    .setResizeOptions(new ResizeOptions(width, height))
                    .setProgressiveRenderingEnabled(true)
                    .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(mProgramThumb.getController())
                    .setImageRequest(request)
                    .build();

            mProgramThumb.setController(controller);
            mProgramName.setText(program.getTitle());
            mProgramDescription.setText(program.getDescription());
        }

        mEpisodeName.setText(mEpisode.getTitle());
        setupPlayer();
        setupRelatedVideos();

        mPlayerHolder.postDelayed(new Runnable() {
            @Override
            public void run() {
                TrackManager.getInstance().trackEvent(videoTimeEvent);
            }
        }, 1000);
    }

    private void setupRelatedVideos() {
        if (mEpisodes != null && mEpisodes.size() > 0) {
            if(mRelatedVideosTitle != null)
                mOtherVideosTitle.setText(mRelatedVideosTitle);

            int currentVideoThumbPostion = 0;
            boolean shouldStopIncrement = false;
            for (int i = 0; i < mEpisodes.size(); i++) {
                Episode ep = mEpisodes.get(i);
                RelatedVideoThumb thumb = new RelatedVideoThumb(this, ep);
                thumb.setTag(i);
                thumb.setOnClickListener(relatedVideosClickListener);
                mRelatedVideosHolder.addView(thumb);

                if (ep.getId() != mEpisode.getId() && !shouldStopIncrement) {
                    thumb.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    currentVideoThumbPostion += thumb.getMeasuredWidth();
                } else {
                    shouldStopIncrement = true;
                }
            }

            final int scrollToValue = currentVideoThumbPostion;
            mRelatedVideosScroll.post(new Runnable() {
                @Override
                public void run() {
                    mRelatedVideosScroll.scrollTo(scrollToValue, 0);
                }
            });

        } else {
            mOtherVideosTitle.setVisibility(View.GONE);
            mRelatedVideosHolder.setVisibility(View.GONE);
        }
    }

    private void setupPlayer() {
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        int screenHeight = size.y;
        int screenWidth = size.x;

        mPlayerHolder.setLayoutParams(new RelativeLayout.LayoutParams(screenWidth, screenHeight));

        RelativeLayout playerView = new RelativeLayout(this);
        playerView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mPlayer = new Player(mEpisode.getVideoId());
        mPlayer.setAuthenticationToken(Session.getCurrentSession().getAccessToken());
        mPlayer.attachTo(playerView, this);
        mPlayer.setEventListener(playerEventListener);

        playerView.setOnTouchListener(buttonsTouchListener);
        mPlayerHolder.addView(playerView);
    }

    private void markViewEvent() {
        float durationInMinutes = (mPlayer.getDuration() / 1000) / 60;
        float currentPositionInMinutes = (mPlayer.getCurrentPosition() / 1000) / 60;
        float targetDurationInMinutes = durationInMinutes / 2;

        if (currentPositionInMinutes >= targetDurationInMinutes && !alreadyMarked) {
            EventLog.getInstance().logProgramViews(mEpisode);
            EventLog.getInstance().logVideoRule();
            alreadyMarked = true;
            TrackManager.getInstance().trackEvent(completionEvent);
        }
    }

    private final View.OnClickListener programClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent programIntent = new Intent(PlayerActivity.this, ProgramActivity.class);
            programIntent.putExtra(ProgramActivity.PROGRAM_KEY, mEpisode.getProgram());
            startActivity(programIntent);
        }
    };

    private final View.OnClickListener bookmarkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mEpisode.isBookmarked()) {
                if (Bookmark.removeFromBookmarks(mEpisode)) {
                    mBookmarkButton.setSelected(false);
                } else {
                    Snackbar.make(mContentView, "Erro ao remover dos favoritos, tente novamente", Snackbar.LENGTH_SHORT);
                }
            } else {
                if (Bookmark.addEpisodeToBookmark(mEpisode)) {
                    EventLog.getInstance().logBookmarkRule();
                    TrackManager.getInstance().trackEvent(bookmarkEvent);
                    mBookmarkButton.setSelected(true);
                } else {
                    Snackbar.make(mContentView, "Erro ao adicionar dos favoritos, tente novamente", Snackbar.LENGTH_SHORT);
                }
            }


        }
    };

    private final View.OnClickListener aboutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int screenHeight = size.y;
            TrackManager.getInstance().trackEvent(aboutEvent);
            ObjectAnimator objectAnimator = ObjectAnimator.ofInt(mScrollView, "scrollY", 0, screenHeight).setDuration(400);
            objectAnimator.start();
        }
    };

    private final View.OnClickListener playClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPlayer.pause();
            List<Game> games = SharedCache.getInstance().retriveList(DispatcherActivity.CACHED_GAMES_KEY, Game.class);
            FragmentManager fm = getSupportFragmentManager();
            Fragment fragment = GamesShortcutFragment.newInstance((ArrayList<Game>) games, mEpisode.getProgram());

            fm.beginTransaction()
                    .add(mContentView.getId(), fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();


        }
    };

    private final View.OnTouchListener buttonsTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (mButtonsHolder.getVisibility() == View.GONE) {
                    mButtonsHolder.setVisibility(View.VISIBLE);
                    mBackButton.setVisibility(View.VISIBLE);
                } else {
                    mButtonsHolder.setVisibility(View.GONE);
                    mBackButton.setVisibility(View.GONE);
                }
            }

            return false;
        }
    };

    private final View.OnClickListener backButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private final BroadcastReceiver startPlayingReceiever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mPlayer != null)
                mPlayer.play();
        }
    };

    private final BroadcastReceiver episodeByIdReceiver = newDataBroadcastReceiver(new Command<Episode>() {
        @Override
        public void execute(Context context, Episode episode) {
            mDialog.dismiss();
            mEpisode = episode;
            TrackManager.getInstance().trackEvent(searchEpisodeEvent);
            setupInterface();
        }
    });

    private final EventListener playerEventListener = new EventListener() {

        @Override
        public void onStop() {
            super.onStop();
            markViewEvent();
        }

        @Override
        public void onPause() {
            super.onPause();
            markViewEvent();
        }

        @Override
        public void onError(com.globo.player.Error error, Throwable throwable) {
            super.onError(error, throwable);
        }

        @Override
        public void onCompletion() {
            super.onCompletion();
            markViewEvent();
            TrackManager.getInstance().trackEvent(completionEvent);
        }

        @Override
        public void onPlayReady() {
            super.onPlayReady();

            TrackManager.getInstance().trackEvent(contentEvent);
            TrackManager.getInstance().trackEvent(providerEvent);
            TrackManager.getInstance().trackEvent(firstPlayEvent);
            TrackManager.getInstance().trackEvent(playEvent);
        }

        @Override
        public void onPlay() {
            super.onPlay();
            markViewEvent();
        }
    };

    private final PlayerScrollView.OnScrollChangesListener scrollListener = new PlayerScrollView.OnScrollChangesListener() {

        @Override
        public void onScrollChange(int x, int y) {
            if (y == 0) {
                mPlayerHolder.getChildAt(0).setVisibility(View.VISIBLE);
                mPlayer.play();
            } else {
                mPlayer.showMediaControls(false);
                mPlayerHolder.getChildAt(0).setVisibility(View.INVISIBLE);
                mPlayer.pause();
            }


        }
    };

    private final View.OnClickListener relatedVideosClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = (int)v.getTag();
            Episode episode = mEpisodes.get(index);
            Intent playerIntent = new Intent(PlayerActivity.this, PlayerActivity.class);
            playerIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            playerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            playerIntent.putExtra(EPISODE_EXTRA_KEY, episode);
            playerIntent.putParcelableArrayListExtra(EPISODE_RELATED_VIDOS_TITLE_KEY, (ArrayList<Episode>) mEpisodes);
            playerIntent.putExtra(EPISODE_RELATED_VIDOS_TITLE_KEY, mRelatedVideosTitle);
            startActivity(playerIntent);
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return String.format("/videos/%s/%s", Urls.slugify(mEpisode.getProgram().getTitle()), Urls.slugify(mEpisode.getTitle()));
        }
    };

    private final TrackManager.ITrackableEvent videoTimeEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Video - Tempo";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };

    private final TrackManager.ITrackableEvent firstPlayEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Video - Primeiro Play";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };

    private final TrackManager.ITrackableEvent contentEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "RT - Conteudo";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("%s|%s", Urls.slugify(mEpisode.getProgram().getTitle()), Urls.slugify(mEpisode.getTitle()));
        }
    };

    private final TrackManager.ITrackableEvent providerEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "RT - Operadora";
        }

        @Override
        public String getLabelForTrack() {
            return Urls.slugify(Session.getCurrentSession().getUser().getProvider());
        }
    };

    private final TrackManager.ITrackableEvent playEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Video - Play";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };


    private final TrackManager.ITrackableEvent completionEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Video - Finalizado";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };

    private final TrackManager.ITrackableEvent bookmarkEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Favorito";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };

    private final TrackManager.ITrackableEvent aboutEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Sobre o vídeo";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("vod|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };

    private TrackManager.ITrackableEvent searchEpisodeEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Play";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("busca|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mEpisode.getId()),
                    Urls.slugify(mEpisode.getCategory()),
                    Urls.slugify(mEpisode.getProgram().getTitle()),
                    (mEpisode.getSeason() == null) ? "" : String.valueOf(mEpisode.getSeason().getNumber()),
                    (mEpisode.getNumber() == 0) ? "" : String.valueOf(mEpisode.getNumber()),
                    Urls.slugify(mEpisode.getTitle()));

            return event;
        }
    };


}
