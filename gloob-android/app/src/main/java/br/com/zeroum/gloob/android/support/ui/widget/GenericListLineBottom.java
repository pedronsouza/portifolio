package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/21/15.
 */
public class GenericListLineBottom extends LinearLayout {
    private int mItemLayoutId;
    private boolean mHideLabel;
    private Context mContext;
    private List<GenericGloobList.IGenericListItem> mGenericItens;
    private LinearLayout mItensHolder;
    private OnClickListener itensClickListener;
    private ImageView mBackground;

    public GenericListLineBottom(Context context, List<GenericGloobList.IGenericListItem> genericItens, boolean hideLabel, OnClickListener itemClickListener, int itemLayoutId) {
        super(context);
        mGenericItens = genericItens;
        mHideLabel = hideLabel;
        mItemLayoutId = itemLayoutId;
        this.itensClickListener= itemClickListener;
        init(context);
    }

    public GenericListLineBottom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GenericListLineBottom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GenericListLineBottom(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.generic_list_line_bottom, this);

        mItensHolder = Finder.findViewById(this, R.id.generic_list_line_itens_holder);
        mBackground = Finder.findViewById(this, R.id.generic_list_line_bg);

        for (GenericGloobList.IGenericListItem item : mGenericItens) {
            GenericListItem listItem = new GenericListItem(context, item, itensClickListener, mItemLayoutId);
            listItem.setTag(item.getValueForTag());

            if (itensClickListener != null)
                listItem.setOnClickListener(itensClickListener);


            mItensHolder.addView(listItem);
        }
    }

    public void setItens(List<GenericGloobList.IGenericListItem> genericItens) {
        mGenericItens = genericItens;
    }

    public void setItensClickListener(OnClickListener itensClickListener) {
        this.itensClickListener = itensClickListener;
    }

    public void setBackgroundLine(int resId) {
        mBackground.setImageResource(resId);
    }
}
