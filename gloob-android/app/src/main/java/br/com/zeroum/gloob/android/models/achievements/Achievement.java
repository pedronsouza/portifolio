package br.com.zeroum.gloob.android.models.achievements;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.models.achievements.rules.BaseRule;

/**
 * Created by zeroum on 4/15/15.
 */
public class Achievement implements Parcelable {
    @SerializedName("id")
    @Expose
    protected int id;
    @SerializedName("title")
    @Expose
    protected String title;
    @Expose
    @SerializedName("avatar_used")
    protected String avatar_used;
    @SerializedName("subtitle")
    @Expose
    protected String subtitle;
    @SerializedName("description_on")
    @Expose
    protected String descriptionOn;
    @SerializedName("description_off")
    @Expose
    protected String descriptionOff;
    @SerializedName("icon")
    @Expose
    protected String icon;
    @SerializedName("rules")
    @Expose
    protected Integer[] rulesRaw;
    @SerializedName("achievementType")
    @Expose
    protected String type;
    @SerializedName("achievement")
    @Expose
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected transient ArrayList<BaseRule> relatedRules;

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAvatarUsed(String avatar) {
        this.avatar_used = avatar;
    }

    public String getAvatarUsed() {
        return avatar_used;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescriptionOn() {
        return descriptionOn;
    }

    public void setDescriptionOn(String descriptionOn) {
        this.descriptionOn = descriptionOn;
    }

    public String getDescriptionOff() {
        return descriptionOff;
    }

    public void setDescriptionOff(String descriptionOff) {
        this.descriptionOff = descriptionOff;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<BaseRule> getRules() {
        return relatedRules;
    }

    public void setRules(ArrayList<BaseRule> rules) {
        this.relatedRules = rules;
    }


    public Integer[] getRulesRaw() {
        return rulesRaw;
    }

    public boolean isUnlocked() {
        List<Achievement> achievements = AchievementsRepository.getInstance().getUnlockedAchievements();
        for (Achievement a : achievements) {
            if (a.getIcon().equals(this.getIcon()) && a.getType().equals(this.getType())) {
                return true;
            }
        }

        return false;
    }

    public boolean isKindAvatar() {
        return (this.type.equals("avatar"));
    }

    public boolean isKindSelfie() {
        return (this.type.equals("selfie"));
    }

    public Achievement() {
    }

    public Drawable getImageForUnlock() {
        return null; // Must be implemented by the childrens
    }

    public Avatar toAvatar() {
        return new Avatar(this);
    }

    public Selfie toSelfie() {
        return new Selfie(this);
    }

    public String getType() {
        return type;
    }

    public ArrayList<BaseRule> getRelatedRules() {
        return relatedRules;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.avatar_used);
        dest.writeString(this.subtitle);
        dest.writeString(this.descriptionOn);
        dest.writeString(this.descriptionOff);
        dest.writeString(this.icon);
//        dest.writeArray(this.rulesRaw);
        dest.writeString(this.type);
        dest.writeList(this.relatedRules);
        dest.writeString(this.name);
    }

    protected Achievement(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.avatar_used = in.readString();
        this.subtitle = in.readString();
        this.descriptionOn = in.readString();
        this.descriptionOff = in.readString();
        this.icon = in.readString();
        this.name = in.readString();
//        this.rulesRaw = in.readArray(Integer[].class.getClassLoader());
        this.type = in.readString();
        this.relatedRules = new ArrayList<BaseRule>();
        in.readList(this.relatedRules, List.class.getClassLoader());
    }

    public static final Creator<Achievement> CREATOR = new Creator<Achievement>() {
        public Achievement createFromParcel(Parcel source) {
            return new Achievement(source);
        }

        public Achievement[] newArray(int size) {
            return new Achievement[size];
        }
    };
}
