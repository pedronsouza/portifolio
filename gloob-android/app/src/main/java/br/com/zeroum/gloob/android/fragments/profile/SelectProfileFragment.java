package br.com.zeroum.gloob.android.fragments.profile;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.GloobButton;
import br.com.zeroum.gloob.android.support.ui.widget.SelectAvatarView;

public class SelectProfileFragment extends BaseProfileCreationFragment {

    private LinearLayout mProfilesHolder;
    private Profile mSelectedProfile;
    private ImageButton mAddProfileButton;
    private View mAddProfileHolder;

    public static SelectProfileFragment newInstance() {
        SelectProfileFragment fragment = new SelectProfileFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public SelectProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }

        Profile.removeCurrentProfile();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_select_profile;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProfilesHolder = Finder.findViewById(mView, R.id.fragment_select_profile_holder);
        mView.findViewById(R.id.fragment_select_profile_bt_main).setOnClickListener(selectAvatarClickListener);
        mAddProfileHolder = Finder.findViewById(mView, R.id.fragment_select_profile_bt_add_holder);
        mAddProfileButton = Finder.findViewById(mView, R.id.fragment_select_profile_bt_add_profile);

        List<Profile> allProfiles = Profile.getAllProfiles();

        for (Profile profile : allProfiles) {
            SelectAvatarView avatarView = new SelectAvatarView(mActivity, profile.getCurrentAvatar());
            avatarView.setName(profile.getName());
            avatarView.setOnSelectAvatarListener(selectAvatarListener);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            avatarView.setLayoutParams(params);

            mProfilesHolder.addView(avatarView);
        }

        if (allProfiles.size() < Profile.MAX_PROFILES) {
            mAddProfileButton.setOnClickListener(addNewProfileClickListener);
            mAddProfileHolder.setVisibility(View.VISIBLE);
        }
    }


    private SelectAvatarView.OnSelectAvatarListener selectAvatarListener = new SelectAvatarView.OnSelectAvatarListener() {
        @Override
        public void onSelect(Avatar avatar) {
            List<Profile> allProfiles = Profile.getAllProfiles();
            for (Profile profile : allProfiles) {
                if (profile.getCurrentAvatar().getIcon().equals(avatar.getIcon())) {
                    mSelectedProfile = profile;
                }
            }
        }
    };

    private final View.OnClickListener selectAvatarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mSelectedProfile == null) {
                Snackbar.make(mView, mActivity.getString(R.string.fragment_select_profile_must_select), Snackbar.LENGTH_SHORT).show();
            } else {
                Profile.setCurrentProfile(mSelectedProfile);
                mActivity.setResult(NewProfileRegisterFragment.ACTIVITY_RESULT_NEW_PROFILE);
                mActivity.finish();
            }
        }
    };

    private final View.OnClickListener addNewProfileClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mActivity.changeRegistrationStep(NewProfileRegisterFragment.newInstance());
        }
    };
}
