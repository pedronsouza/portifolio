package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.receivers.Command;
import br.com.zeroum.gloob.android.support.Finder;

import static br.com.zeroum.gloob.android.GloobApplication.application;
import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;

/**
 * Created by zeroum on 7/14/15.
 */
public class SelectAvatarView extends RelativeLayout {
    private boolean isHorizontal;
    private ImageButton mAvatarImageButton;
    private ImageView mAvatarIndicatorImageView;
    private Avatar mAvatar;
    private BroadcastManager mBroadcastManager;
    private OnSelectAvatarListener onSelectAvatarListener;
    private GloobTextView mAvatarName;

    public SelectAvatarView(Context context) {
        super(context);
    }

    public SelectAvatarView(Context context, Avatar avatar) {
        super(context);
        mAvatar = avatar;
        init(context);
    }

    public SelectAvatarView(Context context, Avatar avatar, boolean isHorizontal) {
        super(context);
        mAvatar = avatar;
        this.isHorizontal = isHorizontal;
        init(context);
    }

    public SelectAvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectAvatarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SelectAvatarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mBroadcastManager.registerLocalBroadcastReceiver(toggleSelectionReceiver, BroadcastManager.Action.TOGGLE_AVATAR_SELECTION);
    }

    @Override
    protected void onDetachedFromWindow() {
        mBroadcastManager.unregisterLocalBroadcastReceiver(toggleSelectionReceiver);
    }

    private void init(Context context) {
        mBroadcastManager = new BroadcastManager();
        int layoutId = R.layout.select_avatar;

        if (isHorizontal) {
            layoutId = R.layout.select_avatar_horizontal;
        }

        inflate(context, layoutId, this);

        mAvatarImageButton = Finder.findViewById(this, R.id.select_avatar_image);
        mAvatarIndicatorImageView = Finder.findViewById(this, R.id.select_avatar_selection_icon);
        mAvatarName = Finder.findViewById(this, R.id.select_avatar_name);

        String imageName = String.format("avatar_novoperfil_%s", mAvatar.getIcon());
        int id = application().getResources().getIdentifier(imageName, "drawable", application().getPackageName());
        mAvatarImageButton.setBackgroundResource(id);

        mAvatarImageButton.setOnClickListener(selectClickListener);

        setClickable(true);
        setOnClickListener(selectClickListener);
    }

    public void setName(String name) {
        mAvatarName.setText(name);
        mAvatarName.setVisibility(View.VISIBLE);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        int visibility = (selected) ? View.VISIBLE : View.GONE;

        if (isHorizontal)
            visibility = (selected) ? View.VISIBLE : View.INVISIBLE;

        mAvatarIndicatorImageView.setVisibility(visibility);
    }

    public interface OnSelectAvatarListener {
        void onSelect(Avatar avatar);
    }

    public OnSelectAvatarListener getOnSelectAvatarListener() {
        return onSelectAvatarListener;
    }

    public void setOnSelectAvatarListener(OnSelectAvatarListener onSelectAvatarListener) {
        this.onSelectAvatarListener = onSelectAvatarListener;
    }

    private final BroadcastReceiver toggleSelectionReceiver = newDataBroadcastReceiver(new Command<Avatar>() {
        @Override
        public void execute(Context context, Avatar data) {
            if (mAvatar.getId() != data.getId()) {
                mAvatarIndicatorImageView.setVisibility(View.GONE);
            }
        }
    });
    private final View.OnClickListener selectClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (onSelectAvatarListener != null) {
                mBroadcastManager.sendLocalBroadcast(BroadcastManager.Action.TOGGLE_AVATAR_SELECTION, mAvatar);
                onSelectAvatarListener.onSelect(mAvatar);
                mAvatarIndicatorImageView.setVisibility(VISIBLE);
            }
        }
    };
}
