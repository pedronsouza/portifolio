package br.com.zeroum.gloob.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.zeroum.gloob.android.support.ui.widget.GenericGloobList;

/**
 * Created by zeroum on 4/13/15.
 */
public class Game implements Parcelable, GenericGloobList.IGenericListItem
{
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getGameFile() {
        return game_file;
    }

    public String getDescription() {
        return description;
    }

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("game_file")
    @Expose
    private String game_file;

    public Game() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeString(this.game_file);
        dest.writeString(this.description);
    }

    private Game(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.image = in.readString();
        this.game_file = in.readString();
        this.description = in.readString();
    }

    public static final Creator<Game> CREATOR = new Creator<Game>() {
        public Game createFromParcel(Parcel source) {
            return new Game(source);
        }

        public Game[] newArray(int size) {
            return new Game[size];
        }
    };

    @Override
    public String getImageUrl() {
        return this.image;
    }

    @Override
    public String getLabel() {
        return null;
    }

    @Override
    public Object getValueForTag() {
        return this.id;
    }
}
