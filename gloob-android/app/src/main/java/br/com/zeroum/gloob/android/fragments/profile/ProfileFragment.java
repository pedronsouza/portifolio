package br.com.zeroum.gloob.android.fragments.profile;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.adapters.FragmentViewPagerAdapter;
import br.com.zeroum.gloob.android.fragments.GloobFragment;
import br.com.zeroum.gloob.android.fragments.SelfieDetailsFragment;
import br.com.zeroum.gloob.android.models.Bookmark;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.Selfie;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.BookmarkThumbView;
import br.com.zeroum.gloob.android.support.ui.widget.GloobButton;
import br.com.zeroum.gloob.android.support.ui.widget.GloobHorizontalScrollView;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.SelfieImageButton;
import br.com.zeroum.gloob.android.support.ui.widget.SelfiesPagerControl;

public class ProfileFragment extends BaseProfileFragment {
    private final static String SELFIES_TITLE_FORMAT = "SUAS SELFIES (%d)";
    private final static String BOOKMARKS_TITLE_FORMAT = "VÍDEOS FAVORITOS (%S)";
    private final int TOTAL_SELFIES_PER_FRAGMENT = 4;

    private ImageView mAvatarImage;
    private final BroadcastManager broadcastManager = new BroadcastManager();
    private GloobTextView mAvatarName;
    private GloobTextView mSelfiesTitle;
    private GloobTextView mBookmarksTitle;
    private GloobButton mChangeAvatarButton;
    private LinearLayout mBookmarksHolder;
    private List<Selfie> mSelfies;
    private SelfiesPagerControl mPageControl;
    private LinearLayout mSelfiesHolder;
    private GloobHorizontalScrollView mSelfiesScroll;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAvatarImage = Finder.findViewById(mView, R.id.fragment_profile_current_avatar_img);
        mAvatarName = Finder.findViewById(mView, R.id.fragment_profile_current_avatar_name);
        mChangeAvatarButton = Finder.findViewById(mView, R.id.fragment_profile_current_avatar_bt_change_avatar);
        mSelfiesTitle = Finder.findViewById(mView, R.id.fragment_profile_selfies_title);
        mBookmarksTitle = Finder.findViewById(mView, R.id.fragment_profile_bookmarks_title);
        mSelfiesHolder = Finder.findViewById(mView, R.id.fragment_profile_selfies_holder);
        mSelfiesScroll = Finder.findViewById(mView, R.id.fragment_profile_selfies_scroll);
        mBookmarksHolder = Finder.findViewById(mView, R.id.fragment_profile_bookmarks_container);
        mPageControl = Finder.findViewById(mView, R.id.fragment_profile_page_control);

        setupInterface();
    }

    @Override
    public void onStart() {
        super.onStart();
        broadcastManager.registerLocalBroadcastReceiver(changeAvatarBroadcastReceiver, BroadcastManager.Action.CHANGE_AVATAR);
    }

    @Override
    public void onStop() {
        super.onStop();
        broadcastManager.unregisterLocalBroadcastReceiver(changeAvatarBroadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAvatarImage.setImageResource(mCurrentProfile.getCurrentAvatar().getImageForProfileId());
        mAvatarName.setText(mCurrentProfile.getName());
    }

    private void setupInterface() {
        mChangeAvatarButton.setOnClickListener(changeAvatarClickListener);
        setupSelfiesList();
        setupBookmarks();
    }

    private void setupBookmarks() {
        List<Bookmark> bookmarks = Bookmark.getAllBookmakrs();
        mBookmarksHolder.removeAllViews();

        if (bookmarks.size() > 0) {
            for (Bookmark b : bookmarks) {
                BookmarkThumbView thumbView = new BookmarkThumbView(getActivity(), b);
                thumbView.setOnBookmarkRemovedListener(onBookmarkRemovedListener);
                mBookmarksHolder.addView(thumbView);
            }

            mBookmarksTitle.setText(String.format(BOOKMARKS_TITLE_FORMAT, bookmarks.size()));
        } else {
            GloobTextView message = new GloobTextView(getActivity());
            message.setTextAppearance(getActivity(), R.style.AppTheme_GloobTextView);
            message.setText(mActivity.getString(R.string.fragment_profile_empty_bookmarks));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            message.setLayoutParams(params);
            message.setGravity(Gravity.CENTER_HORIZONTAL);

            mBookmarksHolder.addView(message);
        }
    }

    private void setupSelfiesList() {
        mSelfies = new ArrayList();
        mSelfies.addAll(AchievementsRepository.getInstance().getSelfies());

        int[] placeholders = new int[] { R.drawable.pf_avatar_placeholder01, R.drawable.pf_avatar_placeholder02, R.drawable.pf_avatar_placeholder03 };
        int index = 0;

        for (int i =0; i < mSelfies.size(); i++) {
            Selfie selfie = mSelfies.get(i);
            SelfieImageButton imgSelfie = new SelfieImageButton(getActivity());

            if (selfie.isUnlocked()) {
                selfie = AchievementsRepository.getInstance().getUnlockedSelfieById(selfie.getId());
                imgSelfie.setImage(selfie.getImageForUnlock());
            } else {
                imgSelfie.setImage(getActivity().getResources().getDrawable(placeholders[index]));
            }

            imgSelfie.setTitle(selfie.getTitle());

            index ++;

            if (index > 2) {
                index = 0;
            }

            imgSelfie.setTag(i);
            imgSelfie.setOnClickListener(seeSelfieDetails);
            mSelfiesHolder.addView(imgSelfie);
        }

        int totalItens = (int) Math.ceil(((double)mSelfies.size() / (double)TOTAL_SELFIES_PER_FRAGMENT));

        mPageControl.setTotalPages(totalItens);
        mPageControl.setOnPagination(onPaginate);
        mSelfiesScroll.setOnScrollChange(onScrollChange);

        mSelfiesTitle.setText(String.format(SELFIES_TITLE_FORMAT, AchievementsRepository.getInstance().getUnlockedSelfies().size()));
    }

    private Selfie[] objectArrayToSelfieArray(Object[] array) {
        Selfie[] selfies = new Selfie[array.length];

        for (int i = 0; i < array.length; i++) {
            selfies[i] = (Selfie) array[i];
        }

        return selfies;
    }

    private final View.OnClickListener changeAvatarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment frag = ChangeAvatarFragment.newInstance();
            View contentView = mActivity.getWindow().getDecorView().findViewById(android.R.id.content);
            FragmentManager fm = mActivity.getSupportFragmentManager();

            fm.beginTransaction()
                    .add(contentView.getId(), frag)
                    .addToBackStack(frag.getClass().toString())
                    .commit();
        }
    };

    private final BroadcastReceiver changeAvatarBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mCurrentProfile = Profile.getCurrentProfile();
            mAvatarImage.setImageResource(mCurrentProfile.getCurrentAvatar().getImageForProfileId());
            mAvatarName.setText(mCurrentProfile.getName());
        }
    };

    private final BookmarkThumbView.OnBookmarkRemovedListener onBookmarkRemovedListener = new BookmarkThumbView.OnBookmarkRemovedListener() {
        @Override
        public void onRemoved() {
            setupBookmarks();
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/seu-perfil";
        }
    };

    private final View.OnClickListener seeSelfieDetails = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = (int) v.getTag();
            Selfie selfie = mSelfies.get(index);

            SelfieDetailsFragment frag = SelfieDetailsFragment.newInstance(selfie);
            View contentView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
            FragmentManager fm = getActivity().getSupportFragmentManager();

            fm.beginTransaction()
                    .add(contentView.getId(), frag)
                    .addToBackStack(frag.getClass().toString())
                    .commit();
        }
    };

    private final SelfiesPagerControl.OnPagination onPaginate = new SelfiesPagerControl.OnPagination() {
        @Override
        public void onPage(int position) {
            mSelfiesScroll.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int width = mSelfiesScroll.getMeasuredWidth();

            int totalPages = (int) Math.ceil(((double)mSelfies.size() / (double)TOTAL_SELFIES_PER_FRAGMENT));
            int pageSize = Math.round((width / totalPages));
            int sizeToScroll = pageSize * position;

            mSelfiesScroll.scrollTo(sizeToScroll, 0);
        }
    };

    private final GloobHorizontalScrollView.OnScrollChange onScrollChange = new GloobHorizontalScrollView.OnScrollChange() {
        @Override
        public void onScroll(int x, int y) {
            mSelfiesScroll.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int width = mSelfiesScroll.getMeasuredWidth();
            int totalPages = (int) Math.ceil(((double)mSelfies.size() / (double)TOTAL_SELFIES_PER_FRAGMENT));
            int pageSize = Math.round((width / totalPages));

            int page = (int)Math.ceil((double) x / (double) pageSize);

            mPageControl.setCurrentPage(page);
        }
    };
}
