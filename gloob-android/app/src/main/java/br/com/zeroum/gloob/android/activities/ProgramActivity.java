package br.com.zeroum.gloob.android.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.Schedule;
import br.com.zeroum.gloob.android.models.Season;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.receivers.Command;
import br.com.zeroum.gloob.android.network.receivers.ListCommand;
import br.com.zeroum.gloob.android.network.requests.objects.ProgramEpisodesRequestObject;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.ProgramSeasonsView;

import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;
import static br.com.zeroum.gloob.android.network.receivers.ListBroadcastReceiver.newListBroadcastReceiver;

public class ProgramActivity extends GloobActivity {
    public static String PROGRAM_KEY = "ProgramActivity.PROGRAM_KEY";
    public static ArrayList<Schedule> fullSchedules = new ArrayList();

    private GloobTextView mProgramTitle;
    private GloobTextView mProgramDescription;
    private SimpleDraweeView mProgramImage;
    private Program mProgram;
    private List<Episode> mAllEpisodes = new ArrayList();
    private List<Season> mAllSeasons = new ArrayList();
    private ProgressDialog mDialog;
    private ScrollView mViewContent;
    private int pageIndex = 1;
    private LinearLayout mSeasonsHolder;
    private LinearLayout mAlertsButtonsHolder;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_program;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialog = new ProgressDialog(this, R.style.AppTheme_SearchDialog);
        mDialog.show();

        mProgram = getIntent().getParcelableExtra(PROGRAM_KEY);

        mViewContent = Finder.findViewById(this, R.id.activity_program_content);
        mProgramTitle = Finder.findViewById(this, R.id.activity_program_title);
        mProgramDescription = Finder.findViewById(this, R.id.activity_program_description);
        mProgramImage = Finder.findViewById(this, R.id.activity_program_image);
        mSeasonsHolder = Finder.findViewById(this, R.id.activity_program_seasons_holder);
        mAlertsButtonsHolder = Finder.findViewById(this, R.id.activity_program_alerts_buttons_holder);

        findViewById(R.id.activity_program_close).setOnClickListener(closeClickListener);
        findViewById(R.id.activity_program_add_alert).setOnClickListener(addAlertClickListener);
        findViewById(R.id.activity_program_all_alerts).setOnClickListener(allAlertsClickListener);

        mProgramTitle.setText(mProgram.getTitle());
        mProgramDescription.setText(mProgram.getDescription());


        mProgramImage.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int width = mProgramImage.getMeasuredWidth();
        int height = mProgramImage.getMeasuredHeight();

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(mProgram.getImage()))
                .setResizeOptions(new ResizeOptions(width, height))
                .setProgressiveRenderingEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(mProgramImage.getController())
                .setImageRequest(request)
                .build();

        mProgramImage.setController(controller);
        mVolleyManager.getProgramsEpisodes(mProgram.getId(), pageIndex);

        if (!Session.isSignedIn() || !Profile.hasProfileInCurrentSession()) {
            mAlertsButtonsHolder.setVisibility(View.GONE);
        }

        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBroadcastManager.registerLocalBroadcastReceiver(programsEpisodesReceiver, BroadcastManager.Action.GET_PROGRAM_EPISODES_OK);
        mBroadcastManager.registerLocalBroadcastReceiver(scheduleReceiver, BroadcastManager.Action.GET_SCHEDULE_OK);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBroadcastManager.unregisterLocalBroadcastReceiver(programsEpisodesReceiver);
        mBroadcastManager.unregisterLocalBroadcastReceiver(scheduleReceiver);
    }

    private void setupSeasonsGrids() {
        for (Episode ep : mAllEpisodes) {
            Season season = getSeasonIfContains(ep.getSeason().getNumber());

            if (season == null) {
                season = ep.getSeason();
                mAllSeasons.add(season);
            }

            if (season.getEpisodes() == null) {
                season.setEpisodes(new ArrayList<Episode>());
            }

            season.getEpisodes().add(ep);
        }

        for (Season s : mAllSeasons) {
            Collections.sort(s.getEpisodes(), new Comparator<Episode>() {
                @Override
                public int compare(Episode ep1, Episode ep2) {
                    return ep2.getNumber() - ep1.getNumber();
                }
            });

            ProgramSeasonsView seasonsView = new ProgramSeasonsView(this, s);
            mSeasonsHolder.addView(seasonsView);
        }

        mDialog.dismiss();
        mViewContent.setVisibility(View.VISIBLE);
    }

    private Season getSeasonIfContains(int seasonNumber) {
        for (Season s : mAllSeasons) {
            if (s.getNumber() == seasonNumber){
                return s;
            }
        }

        return null;
    }

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private final View.OnClickListener addAlertClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDialog.show();
            mVolleyManager.getSchedule(0, mProgram.getTitle());
        }
    };

    private final View.OnClickListener allAlertsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent profileIntent = new Intent(ProgramActivity.this, ProfileActivity.class);
            profileIntent.putExtra(ProfileActivity.START_AT_ALERTS, true);
            startActivity(profileIntent);
        }
    };


    private final BroadcastReceiver programsEpisodesReceiver = newDataBroadcastReceiver(new Command<ProgramEpisodesRequestObject>() {
        @Override
        public void execute(Context context, ProgramEpisodesRequestObject response) {
            if (response != null) {
                if (response.getResults() != null)
                    mAllEpisodes.addAll(response.getResults());

                if (response.getNext() != null && !response.getNext().equals("null")) {
                    pageIndex++;
                    mVolleyManager.getProgramsEpisodes(mProgram.getId(), pageIndex);
                } else {
                    setupSeasonsGrids();
                }
            }
        }
    });

    private BroadcastReceiver scheduleReceiver = newListBroadcastReceiver(new ListCommand<Schedule>() {
        @Override
        public void execute(Context context, List<Schedule> response) {
            if (response == null || response.isEmpty()) {
                mDialog.dismiss();
                AlertDialog.Builder dialog = new AlertDialog.Builder(ProgramActivity.this);
                dialog.setMessage(String.format("Nenhuma exibição encontrada para %s nos próximos 5 dias", mProgram.getTitle()));
                dialog.show();
            } else {
                Intent scheduleIntent = new Intent(ProgramActivity.this, ScheduleActivity.class);
                scheduleIntent.putParcelableArrayListExtra(ScheduleActivity.SCHEDULE_KEY, fullSchedules);
                mDialog.dismiss();
                startActivity(scheduleIntent);
            }
        }
    });

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return String.format("/personagens/%s", Urls.slugify(mProgram.getTitle()));
        }
    };
}
