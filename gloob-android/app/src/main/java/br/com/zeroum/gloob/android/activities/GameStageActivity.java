package br.com.zeroum.gloob.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.EventLog;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;

public class GameStageActivity extends GloobActivity {
    public static final String BUNDLE_GAME_KEY = "GameStageActivity.BUNDLE_GAME_KEY";
    private Game mGame;
    private WebView mWebView;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_game_stage;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent extras = getIntent();
        
        if (extras != null) {
            mGame = extras.getParcelableExtra(BUNDLE_GAME_KEY);
        }

        TrackManager.getInstance().trackPageView(trackablePageView);
        setupInterface();
    }

    private void setupInterface() {
        mWebView = Finder.findViewById(this, R.id.activity_game_stage_webview);
        findViewById(R.id.activity_game_stage_close).setOnClickListener(closeOnClickListener);

        if (mGame != null) {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.setWebViewClient(client);
            mWebView.loadUrl(mGame.getGameFile());
        }
    }

    private final View.OnClickListener closeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mWebView.loadUrl("about:blank");
            finish();
        }
    };

    private final WebViewClient client = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            EventLog.getInstance().logPlayEvent();
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return String.format("/jogos/%s", Urls.slugify(mGame.getName()));
        }
    };
}
