package br.com.zeroum.gloob.android.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.managers.VolleyManager;
import br.com.zeroum.gloob.android.network.receivers.Command;
import br.com.zeroum.gloob.android.network.requests.objects.AuthRequestObject;
import br.com.zeroum.gloob.android.support.Finder;

import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;

/**
 * Created by zeroum on 7/14/15.
 */
public class WebAuthActivity extends AppCompatActivity {
    public static final int AUTH_OK_RESULT_CODE = 200;
    private WebView mWebView;
    private ProgressDialog mProgressBar;
    protected VolleyManager mVolleyManager;
    protected BroadcastManager mBroadcastManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_auth);
        mVolleyManager = new VolleyManager();
        mBroadcastManager = new BroadcastManager();
        mProgressBar = new ProgressDialog(this, R.style.AppTheme_SearchDialog);
        mWebView = Finder.findViewById(this, R.id.activity_web_auth_webview);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(client);

        mWebView.loadUrl(getString(R.string.auth_url));

        TrackManager.getInstance().trackPageView(trackablePageView);
        findViewById(R.id.activity_web_auth_bt_close).setOnClickListener(closeClickListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBroadcastManager.registerLocalBroadcastReceiver(authTokenOkReceiver, BroadcastManager.Action.GET_TOKEN_OK);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBroadcastManager.unregisterLocalBroadcastReceiver(authTokenOkReceiver);
    }

    private boolean hasAuthenticaded(String url) {
        return url.contains("?code=");
    }

    private final WebViewClient client = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            mWebView.loadUrl(url);
            return true;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgressBar.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                if (mProgressBar!=null) {
                    if (mProgressBar.isShowing()) {
                        mProgressBar.dismiss();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if(hasAuthenticaded(url)){
                Uri uri= Uri.parse(url);
                String code = uri.getQueryParameter("code");

                if (code != null) {
                    AuthRequestObject authRequestObject = new AuthRequestObject();
                    authRequestObject.setCode(code);
                    mVolleyManager.getAuthToken(authRequestObject);
                }
            }
        }
    };

    private final BroadcastReceiver authTokenOkReceiver = newDataBroadcastReceiver(new Command<Session>() {
        @Override
        public void execute(Context context, Session session) {
            session.startNewSession();
            setResult(AUTH_OK_RESULT_CODE);
            finish();
        }
    });

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/login";
        }
    };
}
