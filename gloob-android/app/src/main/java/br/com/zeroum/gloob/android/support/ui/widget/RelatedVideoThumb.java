package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 8/4/15.
 */
public class RelatedVideoThumb extends LinearLayout {
    private Episode mEpisode;
    private ThumbNetworkImageView mImage;
    private GloobTextView mProgramName;
    private GloobTextView mEpisodeName;
    private GloobTextView mSeasonInfo;

    public RelatedVideoThumb(Context context, Episode episode) {
        super(context);
        mEpisode = episode;
        init(context);
    }

    public RelatedVideoThumb(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RelatedVideoThumb(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RelatedVideoThumb(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.related_video_thumb, this);

        mImage = Finder.findViewById(this, R.id.related_video_thumb);
        mProgramName = Finder.findViewById(this, R.id.related_video_program);
        mEpisodeName = Finder.findViewById(this, R.id.related_video_episode);
        mSeasonInfo = Finder.findViewById(this, R.id.related_video_season);

        if (mEpisode != null) {
            mProgramName.setText(mEpisode.getProgram().getTitle());
            mEpisodeName.setText(mEpisode.getTitle());
            mSeasonInfo.setText(String.format("TEMP.%d EP.%d", mEpisode.getSeason().getNumber(), mEpisode.getNumber()));
            mImage.setImageUrl(mEpisode.getImage());
        }
    }
}
