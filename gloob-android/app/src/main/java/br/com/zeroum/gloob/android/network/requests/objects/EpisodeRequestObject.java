package br.com.zeroum.gloob.android.network.requests.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.List;

import br.com.zeroum.gloob.android.models.Episode;

/**
 * Created by zeroum on 7/29/15.
 */
public class EpisodeRequestObject implements Parcelable {
    @Expose
    private int count;
    @Expose
    private String next;
    @Expose
    private String previous;
    @Expose
    private List<Episode> results;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<Episode> getResults() {
        return results;
    }

    public void setResults(List<Episode> results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.count);
        dest.writeString(this.next);
        dest.writeString(this.previous);
        dest.writeTypedList(results);
    }

    public EpisodeRequestObject() {
    }

    protected EpisodeRequestObject(Parcel in) {
        this.count = in.readInt();
        this.next = in.readString();
        this.previous = in.readString();
        this.results = in.createTypedArrayList(Episode.CREATOR);
    }

    public static final Parcelable.Creator<EpisodeRequestObject> CREATOR = new Parcelable.Creator<EpisodeRequestObject>() {
        public EpisodeRequestObject createFromParcel(Parcel source) {
            return new EpisodeRequestObject(source);
        }

        public EpisodeRequestObject[] newArray(int size) {
            return new EpisodeRequestObject[size];
        }
    };
}
