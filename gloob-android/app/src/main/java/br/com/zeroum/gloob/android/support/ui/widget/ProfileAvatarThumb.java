package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.text.SimpleDateFormat;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/20/15.
 */
public class ProfileAvatarThumb extends LinearLayout {
    private Profile mProfile;
    private Context mContext;
    private ImageButton mAvatarThumb;
    private GloobTextView mProfileName;
    private GloobTextView mProfileBirthday;
    private GloobTextView mProfileActivie;
    private GloobTextView mProfileChange;
    private ImageButton mRemoveButton;
    private OnProfileChanges mOnProfileChanges;

    public ProfileAvatarThumb(Context context, Profile profile) {
        super(context);
        mProfile = profile;
        init(context);
    }

    public ProfileAvatarThumb(Context context) {
        super(context);
        init(context);
    }

    public ProfileAvatarThumb(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ProfileAvatarThumb(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ProfileAvatarThumb(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.profile_avatar_thumb, this);

        mAvatarThumb = Finder.findViewById(this, R.id.profile_avatar_thumb_img);
        mProfileName = Finder.findViewById(this, R.id.profile_avatar_thumb_name);
        mProfileBirthday = Finder.findViewById(this, R.id.profile_avatar_thumb_birthday);
        mProfileActivie = Finder.findViewById(this, R.id.profile_avatar_thumb_active_profile);
        mProfileChange = Finder.findViewById(this, R.id.profile_avatar_thumb_change_profile);
        mRemoveButton = Finder.findViewById(this, R.id.profile_avatar_thumb_remove);

        mAvatarThumb.setImageResource(mProfile.getCurrentAvatar().getImageForProfileId());
        mProfileName.setText(mProfile.getName());

        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
        mProfileBirthday.setText(sf.format(mProfile.getBirthday()));

        setClickable(true);

        if (mProfile.getId().equals(Profile.getCurrentProfile().getId())) {
            mProfileActivie.setVisibility(View.VISIBLE);
            mProfileChange.setVisibility(View.GONE);
            mRemoveButton.setVisibility(View.GONE);
        } else {
            mProfileActivie.setVisibility(View.GONE);
            mProfileChange.setVisibility(View.VISIBLE);
            mRemoveButton.setVisibility(View.VISIBLE);

            setOnClickListener(changeProfileOnClickListener);
            mAvatarThumb.setOnClickListener(changeProfileOnClickListener);
        }

        mRemoveButton.setOnClickListener(removeProfileListener);
    }

    public void setOnProfileChangesListener(OnProfileChanges listener) {
        mOnProfileChanges = listener;
    }

    public interface OnProfileChanges {
        void hasChanged();
    }


    private final OnClickListener changeProfileOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Profile.setCurrentProfile(mProfile);
                    mProfile.save();

                    if (mOnProfileChanges != null) {
                        mOnProfileChanges.hasChanged();
                    }
                }
            });

            dialog.setTitle("Deseja trocar de perfil?");

            dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    private final OnClickListener removeProfileListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Profile.removeProfile(mProfile);

                    if (mOnProfileChanges != null) {
                        mOnProfileChanges.hasChanged();
                    }
                }
            });

            dialog.setTitle("Deseja remover este perfil?");

            dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };
}
