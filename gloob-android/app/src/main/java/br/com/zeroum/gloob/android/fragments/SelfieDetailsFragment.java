package br.com.zeroum.gloob.android.fragments;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.profile.BaseProfileFragment;
import br.com.zeroum.gloob.android.models.achievements.Selfie;
import br.com.zeroum.gloob.android.models.achievements.repositories.AchievementsRepository;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;

public class SelfieDetailsFragment extends BaseProfileFragment {
    public static final String BUNDLE_SELFIE_KEY = "SelfieDetailsFragment.BUNDLE_SELFIE_KEY";
    private Selfie mSelfie;
    private ImageView mSelfieImg;
    private GloobTextView mTitle;
    private GloobTextView mDescription;
    private GloobTextView mFullDescription;

    public static SelfieDetailsFragment newInstance(Selfie selfie) {
        SelfieDetailsFragment fragment = new SelfieDetailsFragment();

        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_SELFIE_KEY, selfie);
        fragment.setArguments(args);

        return fragment;
    }

    public SelfieDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSelfie = getArguments().getParcelable(BUNDLE_SELFIE_KEY);
            if (mSelfie.isUnlocked())
                mSelfie = AchievementsRepository.getInstance().getUnlockedSelfieById(mSelfie.getId());
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_selfie_details;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSelfieImg = Finder.findViewById(mView, R.id.fragment_selfie_details_selfie);
        mTitle = Finder.findViewById(mView, R.id.fragment_selfie_details_title);
        mDescription = Finder.findViewById(mView, R.id.fragment_selfie_details_description);
        mFullDescription = Finder.findViewById(mView, R.id.fragment_selfie_details_full_description);

        mView.findViewById(R.id.fragment_selfie_details_close).setOnClickListener(closeClickListener);

        if (mSelfie != null) {
            mFullDescription.setText(mSelfie.getDescriptionOff());
            if (mSelfie.isUnlocked()) {
                mSelfieImg.setImageDrawable(mSelfie.getImageForUnlock());
                mFullDescription.setText(mSelfie.getDescriptionOn());
            }

            mTitle.setText(mSelfie.getTitle());
            mDescription.setText(mSelfie.getSubtitle());
        }
    }

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mActivity.getSupportFragmentManager().popBackStack();
        }
    };
}
