package br.com.zeroum.gloob.android.models.achievements.rules;

import org.joda.time.DateTime;
import org.joda.time.Hours;

import java.util.Date;

import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.achievements.Event;
import br.com.zeroum.gloob.android.models.achievements.factories.EventKeyFactory;
import br.com.zeroum.gloob.android.models.achievements.repositories.EventsRepository;

/**
 * Created by zeroum on 4/20/15.
 */
public class AccessRule extends BaseRule implements IRule {
    @Override
    public boolean isValid() {
        String key = EventKeyFactory.getInstance().getKeyByRuleCondition(this.condition, String.valueOf(Profile.getCurrentProfile().getId()));
        Event event = EventsRepository.getInstance().getEventByKey(key);
        Double totalHits = (Double) event.getValue();

        return (totalHits >= this.eventAmount);
    }

    @Override
    public boolean shouldMark() {
        DateTime currentDate = new DateTime(new Date());
        String key = EventKeyFactory.getInstance().getKeyByRuleCondition(this.condition, String.valueOf(Profile.getCurrentProfile().getId()));
        Event event = EventsRepository.getInstance().getEventByKey(key);

        if (event.getValue() == null)
            return true;

        DateTime lastEventDate = new DateTime(event.getUpdatedAt());
        Hours hoursInterval = Hours.hoursBetween(currentDate.toLocalDate(), lastEventDate.toLocalDate());

        return (hoursInterval.getHours() >= this.interval);
    }
}
