package br.com.zeroum.gloob.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zeroum on 4/13/15.
 */
public class Episode implements Parcelable
{
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public int getNumber() {
        return number;
    }

    public String getCroppedImage() {
        return image_cropped;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContentRating() {
        return contentRating;
    }

    public void setContentRating(String contentRating) {
        this.contentRating = contentRating;
    }

    public Program getProgram() {
        return program;
    }

    public String getProgramName() {
        return program_name;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public int getVideoId() {
        return videoId;
    }

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_globo_videos")
    @Expose
    private int videoId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("number")
    @Expose
    private int number;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("image_cropped")
    @Expose
    private String image_cropped;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("content_rating")
    @Expose
    private String contentRating;
    @SerializedName("program")
    @Expose
    private Program program;
    @SerializedName("program_name")
    private String program_name;
    @SerializedName("season")
    @Expose
    private Season season;
    @SerializedName("categories")
    @Expose
    private String[] category;

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Episode() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isBookmarked() {
        return (Bookmark.getBookmark(this) != null);
    }

    public String getCategory() {
        if (category != null && category.length > 0)
            return category[0];
        else
            return "";
    }

    public void setCategory(String[] category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.videoId);
        dest.writeString(this.title);
        dest.writeInt(this.number);
        dest.writeString(this.image);
        dest.writeString(this.image_cropped);
        dest.writeString(this.description);
        dest.writeString(this.contentRating);
        dest.writeParcelable(this.program, 0);
        dest.writeString(this.program_name);
        dest.writeParcelable(this.season, 0);
        dest.writeStringArray(this.category);
    }

    protected Episode(Parcel in) {
        this.id = in.readInt();
        this.videoId = in.readInt();
        this.title = in.readString();
        this.number = in.readInt();
        this.image = in.readString();
        this.image_cropped = in.readString();
        this.description = in.readString();
        this.contentRating = in.readString();
        this.program = in.readParcelable(Program.class.getClassLoader());
        this.program_name = in.readString();
        this.season = in.readParcelable(Season.class.getClassLoader());
        this.category = in.createStringArray();
    }

    public static final Creator<Episode> CREATOR = new Creator<Episode>() {
        public Episode createFromParcel(Parcel source) {
            return new Episode(source);
        }

        public Episode[] newArray(int size) {
            return new Episode[size];
        }
    };
}
