package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by zeroum on 7/14/15.
 */
public class GloobTextView extends TextView {
    public GloobTextView(Context context) {
        super(context);
        init(context);
    }

    public GloobTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GloobTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GloobTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context) {
        Typeface font = this.getTypeface();

        if (font != null) {
            int fontStyle = font.getStyle();

            switch (fontStyle) {
                case Typeface.BOLD:
                    font = Typeface.createFromAsset(getContext().getAssets(), "mavenblack.ttf");
                    break;
                case Typeface.ITALIC:
                    font = Typeface.createFromAsset(getContext().getAssets(), "mavenregular.ttf");
                    break;
                case Typeface.NORMAL:
                    font = Typeface.createFromAsset(getContext().getAssets(), "mavenregular.ttf");
                    break;
                default:
                    font = Typeface.createFromAsset(getContext().getAssets(), "mavenregular.ttf");
                    break;
            }
        }

        this.setTypeface(font);
    }
}
