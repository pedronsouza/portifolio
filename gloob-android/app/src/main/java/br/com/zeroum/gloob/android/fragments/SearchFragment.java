package br.com.zeroum.gloob.android.fragments;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.adobe.mediacore.info.Track;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.activities.CreateProfileActivity;
import br.com.zeroum.gloob.android.activities.MainActivity;
import br.com.zeroum.gloob.android.activities.PlayerActivity;
import br.com.zeroum.gloob.android.activities.ProgramActivity;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.SearchResult;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.models.achievements.EventLog;
import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.receivers.Command;
import br.com.zeroum.gloob.android.network.requests.objects.SearchRequestObject;
import br.com.zeroum.gloob.android.support.Finder;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.EndlessScrollView;
import br.com.zeroum.gloob.android.support.ui.widget.GloobTextView;
import br.com.zeroum.gloob.android.support.ui.widget.ThumbNetworkImageView;

import static br.com.zeroum.gloob.android.network.receivers.DataBroadcastReceiver.newDataBroadcastReceiver;

public class SearchFragment extends GloobFragment {
    private static final String SEASON_FORMAT = "TEMP.%d";
    private static final String SEARCH_EMPTY_MESSAGE_FORMAT = "Nenhum resultado encontrado para \"%s\"";
    private static final String SEARCH_RESULTS_MESSAGE_FORMAT = "Exibindo %d de %d resultados encontrados para \"%s\"";

    private int pageIndex = 1;
    private int mTotalResults;
    private String searchTerm;

    private EditText mSearchEditText;
    private List<SearchResult> mEpisodes;
    private List<SearchResult> mPrograms;
    private boolean mHasMore;
    private ProgressDialog mDialog;
    private LinearLayout mSearchWithResultsHolder;
    private LinearLayout mSearchEmptyHolder;
    private GloobTextView mSearchEmptyMessage;
    private GloobTextView mSearchWithResultsMessage;
    private ImageView mSearchAvatar;
    private LinearLayout mSearchEpisodesHolder;
    private LinearLayout mSearchProgramsHolder;
    private EndlessScrollView mSearchScrollView;
    private boolean requestInProgress;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        mDialog = new ProgressDialog(getActivity(), R.style.AppTheme_SearchDialog);
        TrackManager.getInstance().trackPageView(trackablePageView);
    }

    @Override
    public void onStart() {
        super.onStart();
        mBroadcastManager.registerLocalBroadcastReceiver(searchResultsReceiver, BroadcastManager.Action.GET_SEARCH_OK);
    }

    @Override
    public void onStop() {
        super.onStop();
        mBroadcastManager.unregisterLocalBroadcastReceiver(searchResultsReceiver);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_search;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSearchEditText = Finder.findViewById(mView, R.id.fragment_search_editext);
        mSearchScrollView = Finder.findViewById(mView, R.id.fragment_search_scroll);
        mSearchWithResultsHolder = Finder.findViewById(mView, R.id.fragment_search_results);
        mSearchAvatar = Finder.findViewById(mView, R.id.fragment_search_avatar);
        mSearchEpisodesHolder = Finder.findViewById(mView, R.id.fragment_search_episodes_list);
        mSearchProgramsHolder = Finder.findViewById(mView, R.id.fragment_search_characters_list);
        mSearchWithResultsMessage = Finder.findViewById(mView, R.id.fragment_search_results_message);
        mSearchEmptyHolder = Finder.findViewById(mView, R.id.fragment_search_empty_results);
        mSearchEmptyMessage = Finder.findViewById(mView, R.id.fragment_search_empty_results_message);
        mSearchEditText.setImeActionLabel("Buscar", KeyEvent.KEYCODE_ENTER);
        mSearchEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mSearchEditText.getWindowToken(), 0);
                    doSearchClickListener.onClick(mSearchEditText);
                    return true;
                }

                return false;
            }
        });

        mView.findViewById(R.id.fragment_search_bt).setOnClickListener(doSearchClickListener);
        mView.findViewById(R.id.fragment_search_see_programs).setOnClickListener(seeProgramsClickListener);

        mSearchScrollView.setOnScrollToEndListener(scrollListener);
    }

    private void doSearch() {
        mVolleyManager.getSearchResults(searchTerm, pageIndex);
        mDialog.show();
    }

    private void setStateAsEmptyResults() {
        mSearchEmptyHolder.setVisibility(View.VISIBLE);
        mSearchAvatar.setVisibility(View.GONE);
        mSearchWithResultsHolder.setVisibility(View.GONE);
        mSearchEmptyMessage.setText(String.format(SEARCH_EMPTY_MESSAGE_FORMAT, searchTerm));
        mDialog.dismiss();
    }

    private void addProgramsToList(List<SearchResult> programs) {

        for (SearchResult item : programs) {
            View v = getActivity().getLayoutInflater().inflate(R.layout.search_programs_list_item, null);
            v.setTag(item.getId());
            v.setClickable(true);
            v.setOnClickListener(programsClickListener);

            Program p = Program.getCachedProgramById(item.getId());

            SimpleDraweeView image = Finder.findViewById(v, R.id.search_programs_list_item_image);
            GloobTextView title = Finder.findViewById(v, R.id.search_programs_list_item_name);

            image.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int width = image.getMeasuredWidth();
            int height = image.getMeasuredHeight();

            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(p.getImage()))
                    .setResizeOptions(new ResizeOptions(width, height))
                    .setProgressiveRenderingEnabled(true)
                    .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(image.getController())
                    .setImageRequest(request)
                    .build();

            image.setController(controller);
            title.setText(item.getTitle());

            mSearchProgramsHolder.addView(v);
        }
    }

    private void addEpisodesToList(List<SearchResult> episodes) {

        for (SearchResult item : episodes) {
            View v = getActivity().getLayoutInflater().inflate(R.layout.search_episodes_list_item, null);
            v.setTag(item.getId());
            v.setClickable(true);
            v.setOnClickListener(episodeClickListener);


            ThumbNetworkImageView image = Finder.findViewById(v, R.id.search_episodes_list_item_image);
            GloobTextView programName = Finder.findViewById(v, R.id.search_episodes_list_item_program_name);
            GloobTextView episodeName = Finder.findViewById(v, R.id.search_episodes_list_item_episode_title);
            GloobTextView seasonDetails = Finder.findViewById(v, R.id.search_episodes_list_item_season);

            image.setImageUrl(item.getImage());
            programName.setText(item.getProgram());
            episodeName.setText(item.getTitle());
            seasonDetails.setText(String.format(SEASON_FORMAT, item.getSeasonCount()));

            mSearchEpisodesHolder.addView(v);
        }

        mSearchEmptyHolder.setVisibility(View.GONE);
        mSearchAvatar.setVisibility(View.GONE);
        mSearchWithResultsHolder.setVisibility(View.VISIBLE);
        mSearchWithResultsMessage.setText(String.format(SEARCH_RESULTS_MESSAGE_FORMAT, mEpisodes.size(), mTotalResults, searchTerm));
    }

    private final BroadcastReceiver searchResultsReceiver = newDataBroadcastReceiver(new Command<SearchRequestObject>() {
        @Override
        public void execute(Context context, SearchRequestObject response) {
            requestInProgress = false;
            if (response != null) {
                mTotalResults = response.getCount();
                if (response.getEpisodes() == null || response.getEpisodes().size() == 0) {
                    setStateAsEmptyResults();
                } else {

                    if (response.getEpisodes() != null) {
                        if (mEpisodes == null) {
                            mEpisodes = response.getEpisodes();
                        } else {
                            ArrayList<SearchResult> results = new ArrayList<SearchResult>();
                            results.addAll(mEpisodes);
                            results.addAll(response.getEpisodes());

                            mEpisodes = results;
                        }

                        addEpisodesToList(response.getEpisodes());
                    }

                    if (response.getPrograms() != null) {
                        if (mPrograms == null) {
                            mPrograms = response.getPrograms();
                        } else {
                            ArrayList<SearchResult> results = new ArrayList<SearchResult>();
                            results.addAll(mPrograms);
                            results.addAll(response.getPrograms());
                            mPrograms = results;
                        }

                        addProgramsToList(response.getPrograms());
                    }

                }

                mHasMore = (response.getNext() != null && !response.getNext().equals("null"));

                mDialog.dismiss();
            }

            TrackManager.getInstance().trackEvent(searchEvent);
        }
    });

    private final View.OnClickListener doSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            searchTerm = mSearchEditText.getText().toString();
            mEpisodes = new ArrayList();
            mPrograms = new ArrayList();
            mTotalResults = 0;

            EventLog.getInstance().logSearchRule();

            if (pageIndex > 1) {
                pageIndex = 1;
            }

            mSearchEpisodesHolder.removeAllViews();
            mSearchProgramsHolder.removeAllViews();

            doSearch();
        }
    };

    private EndlessScrollView.OnScrollToEnd scrollListener = new EndlessScrollView.OnScrollToEnd() {
        @Override
        public void hasHeachedEnd() {
            if (mHasMore && !requestInProgress) {
                pageIndex++;
                mVolleyManager.getSearchResults(searchTerm, pageIndex);
                mDialog.show();
                requestInProgress = true;
            }
        }
    };

    private Program mCurrentProgram;
    private final View.OnClickListener programsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = (int)v.getTag();

            Program program = Program.getCachedProgramById(id);
            mCurrentProgram = program;
            TrackManager.getInstance().trackEvent(programEvent);

            Intent programIntent = new Intent(getActivity(), ProgramActivity.class);
            programIntent.putExtra(ProgramActivity.PROGRAM_KEY, program);
            startActivity(programIntent);
        }
    };

    private SearchResult mCurrentEpisode;
    private final View.OnClickListener episodeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Session.isSignedIn() && Profile.hasAnyProfile()) {
                int episodeId = (int)v.getTag();


                for (SearchResult result : mEpisodes) {
                    if (result.getId() == episodeId) {
                        mCurrentEpisode = result;
                        TrackManager.getInstance().trackEvent(episodeEvent);
                        break;
                    }
                }

                Intent playerIntent = new Intent(getActivity(), PlayerActivity.class);
                playerIntent.putExtra(PlayerActivity.EPISODE_SHOULD_LOAD_KEY, true);
                playerIntent.putExtra(PlayerActivity.EPISODE_ID_EXTRA_KEY, episodeId);
                startActivity(playerIntent);
            } else {
                startActivity(new Intent(getActivity(), CreateProfileActivity.class));
            }
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return "/busca";
        }
    };

    private final TrackManager.ITrackableEvent searchEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Geral - Busca";
        }

        @Override
        public String getLabelForTrack() {
            String label;

            if (mTotalResults == 0) {
                label = "%s|nenhum-resultado";
            } else {
                label = "%s";
            }

            return String.format(label, Urls.slugify(searchTerm));
        }
    };

    private final TrackManager.ITrackableEvent programEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Resultado de Busca";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("%s|personagem|%s", Urls.slugify(searchTerm), Urls.slugify(mCurrentProgram.getTitle()));
        }
    };

    private final TrackManager.ITrackableEvent episodeEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Resultado de Busca";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("%s|videos|%s", Urls.slugify(searchTerm), Urls.slugify(mCurrentEpisode.getTitle()));
        }
    };

    private final View.OnClickListener seeProgramsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
            mainIntent.putExtra(MainActivity.START_AT_CHARACTERS_INTENT, true);
            startActivity(mainIntent);
        }
    };
}
