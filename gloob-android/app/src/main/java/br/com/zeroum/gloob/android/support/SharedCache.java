package br.com.zeroum.gloob.android.support;

import android.content.SharedPreferences;
import android.os.Parcelable;

import com.auditude.ads.model.smil.Par;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.zeroum.gloob.android.GloobApplication;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;

import static android.content.Context.MODE_PRIVATE;
import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 7/16/15.
 */
public class SharedCache<T> {
    private static final String CACHEKEYS = "SharedCache.CACHEKEYS";
    private final SharedPreferences mPrefs;
    private Set<String> mKeys;

    private SharedCache() {
        mPrefs = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
        mKeys = new HashSet<>();
        mPrefs.getStringSet(CACHEKEYS, mKeys);
    }

    private void addKeyToCacheManagement(String key) {
        mKeys.add(key);
        mPrefs.edit().putStringSet(CACHEKEYS, mKeys);
    }

    public void storeList(String key, List<T> list, Class<T> clazz) {
        Set<String> storeSet = new HashSet();
        for (T object : list) {
            storeSet.add(GsonProvider.getGson().toJson(object, clazz));
        }

        mPrefs.edit().putStringSet(key, storeSet).apply();
        addKeyToCacheManagement(key);
    }

    public List<T> retriveList(String key, Class<T> clazz) {
        List<T> returnList = new ArrayList();
        Set<String> storedSet = mPrefs.getStringSet(key, null);

        if (storedSet != null) {
            for (String objectRaw : storedSet) {
                returnList.add(GsonProvider.getGson().fromJson(objectRaw, clazz));
            }
        }

        return returnList;
    }

    public void flushCache() {
        for (String key : mKeys) {
            mPrefs.edit().remove(key).apply();
        }
    }

    public static SharedCache getInstance() {
        return SingletonHolder.INSTANCE;
    }

    static class SingletonHolder {
        private static final SharedCache INSTANCE = new SharedCache();
    }
}
