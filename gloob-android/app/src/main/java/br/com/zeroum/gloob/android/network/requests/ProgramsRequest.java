package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import br.com.zeroum.gloob.android.network.managers.BroadcastManager;
import br.com.zeroum.gloob.android.network.requests.objects.ProgramsRequestObject;
import br.com.zeroum.gloob.android.support.Urls;

/**
 * Created by zeroum on 7/15/15.
 */
public class ProgramsRequest implements Response.Listener<JSONObject>, Response.ErrorListener {
    private BaseRequest request;
    private BroadcastManager broadcastManager;

    public ProgramsRequest(int pageIndex) {
        broadcastManager = new BroadcastManager();
        request = new JsonAuthenticatedRequest(Request.Method.GET, Request.Priority.IMMEDIATE, Urls.getProgramsUrl(pageIndex), null, this, this);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_PROGRAMS_ERROR);
    }

    @Override
    public void onResponse(JSONObject json) {
        ProgramsRequestObject programsRequestObject = GsonProvider.getGson().fromJson(String.valueOf(json), ProgramsRequestObject.class);
        broadcastManager.sendLocalBroadcast(BroadcastManager.Action.GET_PROGRAMS_OK, programsRequestObject);
    }

    public Request getRequest() {
        return request;
    }
}
