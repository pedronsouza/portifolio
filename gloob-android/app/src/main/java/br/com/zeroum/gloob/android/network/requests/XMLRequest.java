package br.com.zeroum.gloob.android.network.requests;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by zeroum on 4/29/15.
 */
public class XMLRequest extends StringRequest implements Deliverable {
    private final Priority priority;
    private Response.Listener<String> listener;
    private Map<String, String> params;
    private boolean delivered;

    public XMLRequest(int method, Priority priority, String url, Map<String, String> params, Response.Listener<String> responseListener, Response.ErrorListener errorListener) {
        super(method, url, responseListener, errorListener);
        this.listener = responseListener;
        this.params = params;
        this.priority = priority;
    }

    @Override
    public Priority getPriority() {
        if (priority == null) {
            return Priority.NORMAL;
        }

        return priority;
    }

    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        return params;
    };

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            String responseString = new String(response.data, "UTF-8");
            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(String response) {
        // TODO Auto-generated method stub
        delivered = true;
        listener.onResponse(response);
    }

    @Override
    public boolean hasBeenDelivered() {
        return delivered;
    }
}
