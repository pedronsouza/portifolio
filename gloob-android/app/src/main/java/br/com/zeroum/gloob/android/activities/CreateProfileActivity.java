package br.com.zeroum.gloob.android.activities;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.fragments.profile.BaseProfileCreationFragment;
import br.com.zeroum.gloob.android.fragments.profile.NewProfileFirstStep;
import br.com.zeroum.gloob.android.fragments.profile.NewProfileRegisterFragment;
import br.com.zeroum.gloob.android.fragments.profile.SelectProfileFragment;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.support.Finder;

public class CreateProfileActivity extends GloobActivity {
    public static final int AUTH_WEBVIEW_REQUEST_CODE = 100;
    public static final String START_ON_CREATION_KEY = "CreateProfileActivity.START_ON_CREATION_KEY";
    private final int fragmentContainer = R.id.activity_create_profile_container;
    private ImageButton mCloseButton;
    private boolean hasSignedIn;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_create_profile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCloseButton = Finder.findViewById(this, R.id.activity_create_profile_bt_close);
        mCloseButton.setOnClickListener(closeButton);

        Intent intent = getIntent();
        if (intent.getBooleanExtra(START_ON_CREATION_KEY, false)) {
            changeToFragment(fragmentContainer, NewProfileRegisterFragment.newInstance(), false);
        } else {
            changeToFragment(fragmentContainer, NewProfileFirstStep.newInstance(), false);
        }
    }

    public void changeRegistrationStep(BaseProfileCreationFragment fragment) {
        changeToFragment(fragmentContainer, fragment, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTH_WEBVIEW_REQUEST_CODE) {
            hasSignedIn = (resultCode == WebAuthActivity.AUTH_OK_RESULT_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (hasSignedIn) {
            if (Profile.hasAnyProfile() && Session.isSignedIn()) {
                changeRegistrationStep(SelectProfileFragment.newInstance());
            } else {
                changeRegistrationStep(NewProfileRegisterFragment.newInstance());
            }
        }
    }

    public void showAuthWebView() {
        startActivityForResult(new Intent(this, WebAuthActivity.class), AUTH_WEBVIEW_REQUEST_CODE);
    }

    private final View.OnClickListener closeButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
