package br.com.zeroum.gloob.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zeroum on 7/15/15.
 */
public class MostViewed implements Parcelable {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image_cropped")
    @Expose
    private String image;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.image);
    }

    public MostViewed() {
    }

    protected MostViewed(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.image = in.readString();
    }

    public static final Parcelable.Creator<MostViewed> CREATOR = new Parcelable.Creator<MostViewed>() {
        public MostViewed createFromParcel(Parcel source) {
            return new MostViewed(source);
        }

        public MostViewed[] newArray(int size) {
            return new MostViewed[size];
        }
    };
}
