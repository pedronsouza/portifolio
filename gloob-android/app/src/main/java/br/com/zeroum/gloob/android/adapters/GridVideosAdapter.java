package br.com.zeroum.gloob.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.activities.CreateProfileActivity;
import br.com.zeroum.gloob.android.activities.PlayerActivity;
import br.com.zeroum.gloob.android.models.Episode;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.ThumbNetworkImageView;

/**
 * Created by zeroum on 7/28/15.
 */
public class GridVideosAdapter extends BaseAdapter {
    private List<Episode> episodes;
    private Context mContext;
    private String mRelatedVideosTitle;

    public void setRelatedVideosTitle(String mRelatedVideosTitle) {
        this.mRelatedVideosTitle = mRelatedVideosTitle;
    }

    public GridVideosAdapter(Context context, List<Episode> episodeList) {
        episodes = episodeList;
        mContext = context;
    }

    @Override
    public int getCount() {
        return episodes.size();
    }

    @Override
    public Object getItem(int position) {
        return episodes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ThumbNetworkImageView thumbView;
        Episode episode = episodes.get(position);

        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();

        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140, metrics);
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 72, metrics);

        if (convertView == null) {
            thumbView = new ThumbNetworkImageView(mContext, width, height);
        } else {
            thumbView = (ThumbNetworkImageView) convertView;
        }
        thumbView.setLayoutParams(new AbsListView.LayoutParams(width, height));
        thumbView.setTag(position);
        thumbView.setImageUrl(episode.getImage());
        thumbView.setOnClickListener(gameClickListener);
        thumbView.setLabel(String.format("Ep. %d", episode.getNumber()));

        return thumbView;
    }

    private boolean cameFromProgramList() {
        return (mRelatedVideosTitle != null);
    }

    private Episode mCurrentEpisode;
    private View.OnClickListener gameClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Session.isSignedIn() && Profile.hasProfileInCurrentSession()) {
                int index = (int) v.getTag();
                Episode episode = episodes.get(index);
                mCurrentEpisode = episode;
                TrackManager.getInstance().trackEvent(programEpisodeEvent);
                Intent player = new Intent(mContext, PlayerActivity.class);
                ArrayList<Episode> episodesList = new ArrayList();

                for (Episode e : episodes) {
                    episodesList.add(e);
                }

                player.putExtra(PlayerActivity.EPISODE_EXTRA_KEY, episode);
                player.putParcelableArrayListExtra(PlayerActivity.EPISODES_LIST_EXTRA_KEY, episodesList);

                if (mRelatedVideosTitle == null)
                    player.putExtra(PlayerActivity.EPISODE_RELATED_VIDOS_TITLE_KEY, String.format("VÍDEOS MAIS NOVOS (%d)", episodesList.size()));
                else
                    player.putExtra(PlayerActivity.EPISODE_RELATED_VIDOS_TITLE_KEY, mRelatedVideosTitle);

                mContext.startActivity(player);
            } else {
                mContext.startActivity(new Intent(mContext, CreateProfileActivity.class));
            }
        }
    };

    private TrackManager.ITrackableEvent programEpisodeEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Play";
        }

        @Override
        public String getLabelForTrack() {
            String event = String.format("personagem|%s||fechado|gloob|%s|%s|%s|%s|%s",
                    String.valueOf(mCurrentEpisode.getId()),
                    Urls.slugify(mCurrentEpisode.getCategory()),
                    Urls.slugify(mCurrentEpisode.getProgram().getTitle()),
                    (mCurrentEpisode.getSeason() == null) ? "" : String.valueOf(mCurrentEpisode.getSeason().getNumber()),
                    (mCurrentEpisode.getNumber() == 0) ? "" : String.valueOf(mCurrentEpisode.getNumber()),
                    Urls.slugify(mCurrentEpisode.getTitle()));

            return event;
        }
    };
}
