package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import br.com.zeroum.gloob.android.R;

/**
 * Created by zeroum on 7/21/15.
 */
public class GenericGloobList extends LinearLayout {
    public static final int MAX_ITENS_PER_LINE = 4;
    private  int[] mLinesDrawables;
    private List<IGenericListItem> mGenericItens;
    private OnClickListener mItensClickListener;
    private int mItemLayoutId;

    public GenericGloobList(Context context, List<IGenericListItem> itens, OnClickListener itensClickListener, int itemLayoutId, int[] linesDrawables) {
        super(context);
        mGenericItens = itens;
        mItensClickListener = itensClickListener;
        mItemLayoutId = itemLayoutId;
        mLinesDrawables = linesDrawables;

        init(context);
    }

    public GenericGloobList(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GenericGloobList(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GenericGloobList(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        setOrientation(VERTICAL);

        float thumbWidth = context.getResources().getDimension(R.dimen.generic_list_item_bottom_width) + context.getResources().getDimension(R.dimen.thumb_item_margin);

        LinearLayout holder = new LinearLayout(context);
        holder.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        holder.setOrientation(VERTICAL);

        int itensPerLine = (int) Math.ceil((metrics.widthPixels / metrics.density) / thumbWidth);

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP || Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            itensPerLine += 1;
        }

        if (itensPerLine >= MAX_ITENS_PER_LINE) { // This is bad, and i should feel bad about that... But there's no other way (for now) so... ¯\_(ツ)_/¯
            itensPerLine = MAX_ITENS_PER_LINE - 1;
        }

        double listSize = mGenericItens.size();
        double totalItens = itensPerLine;

        int totalLines = (int) Math.ceil((listSize/ totalItens));

        int lineBgIndex = 0;
        for (int i = 0; i < totalLines; i++) {
            if (lineBgIndex > 2)
                lineBgIndex = 0;

            int startIndex = itensPerLine * i;
            int endIndex = itensPerLine + startIndex;

            if (endIndex >= mGenericItens.size()) {
                endIndex = mGenericItens.size();
            }

            List<IGenericListItem> itensForLine = mGenericItens.subList(startIndex, endIndex);
            GenericListLineBottom line = new GenericListLineBottom(context, itensForLine, true, mItensClickListener, mItemLayoutId);

            line.setBackgroundLine(mLinesDrawables[lineBgIndex]);

            lineBgIndex++;

            holder.addView(line);
        }

        addView(holder);
    }

    public interface IGenericListItem {
        String getImageUrl();
        String getLabel();
        Object getValueForTag();
    }
}
