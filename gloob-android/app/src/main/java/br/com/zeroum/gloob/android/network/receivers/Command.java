package br.com.zeroum.gloob.android.network.receivers;

import android.content.Context;
import android.os.Parcelable;

/**
 * Created by zeroum on 4/13/15.
 */
public interface Command<T extends Parcelable> {

    void execute(Context context, T data);

}
