package br.com.zeroum.gloob.android.fragments.profile;


import android.os.Bundle;
import android.view.View;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Session;
import br.com.zeroum.gloob.android.models.User;
import br.com.zeroum.gloob.android.support.ui.widget.GloobButton;

public class NewProfileSecondStep extends BaseProfileCreationFragment {
    private GloobButton mContinue;

    public static NewProfileSecondStep newInstance() {
        NewProfileSecondStep fragment = new NewProfileSecondStep();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public NewProfileSecondStep() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_new_profile_second_step;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView.findViewById(R.id.new_profile_second_step_continue).setOnClickListener(continueClickListener);
        mView.findViewById(R.id.new_profile_second_step_back).setOnClickListener(backClickListener);

    }

    private final View.OnClickListener continueClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Session.isSignedIn()) {
                mActivity.changeRegistrationStep(NewProfileRegisterFragment.newInstance());
            } else {
                mActivity.showAuthWebView();
            }
        }
    };

    private final View.OnClickListener backClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mActivity.onBackPressed();
        }
    };
}
