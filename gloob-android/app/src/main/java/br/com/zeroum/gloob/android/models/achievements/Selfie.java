package br.com.zeroum.gloob.android.models.achievements;

import android.content.Context;
import android.graphics.drawable.Drawable;
import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/15/15.
 */
public class Selfie extends Achievement {

    public Selfie() {

    }

    public Selfie(Achievement achievement) {
        this.id = achievement.getId();
        this.title = achievement.getTitle();
        this.subtitle = achievement.getSubtitle();
        this.descriptionOn = achievement.getDescriptionOn();
        this.descriptionOff = achievement.getDescriptionOff();
        this.icon = achievement.getIcon();
        this.rulesRaw = achievement.getRulesRaw();
        this.type = achievement.getType();
        this.relatedRules = achievement.getRelatedRules();
        this.avatar_used = achievement.getAvatarUsed();
    }

    public String getTitle() { return this.title; };

    public String getIcon() { return this.icon; };

    @Override
    public Drawable getImageForUnlock() {
        Context context = application();
        String avatar = this.avatar_used;
        String imageName = String.format("selfie_%s_%s", avatar, this.getIcon());

        int id = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
        return application().getResources().getDrawable(id);
    }


}
