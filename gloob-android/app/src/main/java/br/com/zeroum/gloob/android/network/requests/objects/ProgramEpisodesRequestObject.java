package br.com.zeroum.gloob.android.network.requests.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.List;

import br.com.zeroum.gloob.android.models.Episode;

/**
 * Created by zeroum on 7/28/15.
 */
public class ProgramEpisodesRequestObject implements Parcelable {
    @Expose
    private String next;
    @Expose
    private String previous;
    @Expose
    private int count;
    @Expose
    private List<Episode> results;

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Episode> getResults() {
        return results;
    }

    public void setResults(List<Episode> results) {
        this.results = results;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.next);
        dest.writeString(this.previous);
        dest.writeInt(this.count);
        dest.writeTypedList(results);
    }

    public ProgramEpisodesRequestObject() {
    }

    protected ProgramEpisodesRequestObject(Parcel in) {
        this.next = in.readString();
        this.previous = in.readString();
        this.count = in.readInt();
        this.results = in.createTypedArrayList(Episode.CREATOR);
    }

    public static final Parcelable.Creator<ProgramEpisodesRequestObject> CREATOR = new Parcelable.Creator<ProgramEpisodesRequestObject>() {
        public ProgramEpisodesRequestObject createFromParcel(Parcel source) {
            return new ProgramEpisodesRequestObject(source);
        }

        public ProgramEpisodesRequestObject[] newArray(int size) {
            return new ProgramEpisodesRequestObject[size];
        }
    };
}
