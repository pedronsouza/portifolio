package br.com.zeroum.gloob.android.support.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.achievements.Selfie;
import br.com.zeroum.gloob.android.support.Finder;

/**
 * Created by zeroum on 7/17/15.
 */
public class SelfieImageButton extends RelativeLayout {
    private ImageView mImageView;
    private GloobTextView mTitle;

    public SelfieImageButton(Context context) {
        super(context);
        init(context);
    }

    public SelfieImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SelfieImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SelfieImageButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context) {
        inflate(context, R.layout.selfies_image_button, this);
        mImageView = Finder.findViewById(this, R.id.selfies_image_button_image);
        mTitle = Finder.findViewById(this, R.id.selfies_image_button_title);
    }

    public void setImage(Drawable image) {
        mImageView.setImageDrawable(image);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setAsUnlocked(boolean unlocked) {
        if (unlocked) {
            mTitle.setTypeface(mTitle.getTypeface(), Typeface.BOLD);
        }
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        setClickable(true);
    }
}
