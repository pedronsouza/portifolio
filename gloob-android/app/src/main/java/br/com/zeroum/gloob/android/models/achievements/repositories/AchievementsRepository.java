package br.com.zeroum.gloob.android.models.achievements.repositories;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.zeroum.gloob.android.BuildConfig;
import br.com.zeroum.gloob.android.GloobApplication;
import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.Profile;
import br.com.zeroum.gloob.android.models.achievements.Achievement;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.models.achievements.Selfie;
import br.com.zeroum.gloob.android.models.achievements.rules.BaseRule;
import br.com.zeroum.gloob.android.network.requests.GsonProvider;
import br.com.zeroum.gloob.android.support.FileUtils;

import static android.content.Context.MODE_PRIVATE;
import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/16/15.
 */
public class AchievementsRepository {
    // TODO: By now, whe are not generating a unique ID for profiles, them whe are using here Profile's name to variate the achievements set. But it must be a unique key.
        private static final String SHAREDEPREFERENCES_UNLOCKEDACHIEVEMENTS_KEY = "AchievementsRepository.SHAREDEPREFERENCES_UNLOCKEDACHIEVEMENTS_KEY_%s";

    private List<Avatar> avatars;
    private List<Selfie> selfies;

    private AchievementsRepository() {
        String avatarsRaw = (BuildConfig.DEBUG) ? FileUtils.getFileContentByResId(R.raw.avatars)  : FileUtils.getFileContentByResId(R.raw.avatars);
        String selfiesRaw = (BuildConfig.DEBUG) ? FileUtils.getFileContentByResId(R.raw.selfies) : FileUtils.getFileContentByResId(R.raw.selfies);

        Avatar[] auxAvatar;
        Selfie[] auxSelfie;
        try {

            auxAvatar = GsonProvider.getGson().fromJson(new JSONArray(avatarsRaw).toString(), Avatar[].class);
            auxSelfie = GsonProvider.getGson().fromJson(new JSONArray(selfiesRaw).toString(), Selfie[].class);

            avatars = Arrays.asList(auxAvatar);
            selfies = Arrays.asList(auxSelfie);

            setupRulesForAchievements();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupRulesForAchievements() {
        List<Avatar> avatars = new ArrayList<>();
        List<Selfie> selfies = new ArrayList<>();
        for (Avatar avatar : this.avatars) {
            ArrayList<BaseRule> rules = new ArrayList<>();

            for (Integer i : avatar.getRulesRaw()) {
                rules.add(RulesRepository.getInstance().getRuleById(i));
            }

            avatar.setRules(rules);
            avatar.setType("avatar");
            avatars.add(avatar);
        }

        for (Selfie selfie : this.selfies) {
            ArrayList<BaseRule> rules = new ArrayList<>();

            for (Integer i : selfie.getRulesRaw()) {
                rules.add(RulesRepository.getInstance().getRuleById(i));
            }
            selfie.setType("selfie");
            selfie.setRules(rules);
            selfies.add(selfie);
        }

        this.avatars = avatars;
        this.selfies = selfies;
    }

    public List<Avatar> getAvatars() {
        return avatars;
    }

    public List<Selfie> getSelfies() {
        setupRulesForAchievements();
        return selfies;
    }

    public Avatar getAvatarById(int id) {
        Avatar obj = null;
        for(Avatar avatar : this.avatars) {
            if (avatar.getId() == id) {
                obj = avatar;
                break;
            }
        }

        return obj;
    }

    public Selfie getSelfieById(int id) {
        Selfie obj = null;
        for(Selfie selfie : this.selfies) {
            if (selfie.getId() == id) {
                obj = selfie;
                break;
            }
        }

        return obj;
    }

    public void saveAchievementAsUnlocked(Achievement achievement) {
        SharedPreferences preferences = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);
        Set<String> unlockedAchievements = new HashSet<>();
        SharedPreferences.Editor editor = preferences.edit();

        unlockedAchievements.addAll(preferences.getStringSet(getUnlockedAchievementsKeyFormatted(), unlockedAchievements));

        if (achievement.isKindSelfie()) {
            achievement.setAvatarUsed(Profile.getCurrentProfile().getCurrentAvatar().getIcon());
        }

        try {
            JSONObject achievJSON = new JSONObject(achievementToJson(achievement));
            unlockedAchievements.add(achievJSON.toString());

            editor.putStringSet(getUnlockedAchievementsKeyFormatted(), unlockedAchievements);
            editor.apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<Achievement> getUnlockedAchievements() {
        SharedPreferences preferences = application().getSharedPreferences(GloobApplication.SHAREDPREFERENCS_APP, MODE_PRIVATE);

        Set<String> unlockedAchievements = new HashSet<>();
        unlockedAchievements.addAll(preferences.getStringSet(getUnlockedAchievementsKeyFormatted(), new HashSet()));

        List<Achievement> achievementList = new ArrayList<>();
        achievementList.addAll(Avatar.getDefaulAvatars());
        for (String jsonAchievement : unlockedAchievements) {
            Achievement achievement = jsonToAchievement(jsonAchievement);
            achievement.setRules((ArrayList<BaseRule>) RulesRepository.getInstance().getRulesByAchievement(achievement));
            achievementList.add(achievement);
        }

        return achievementList;
    }

    public List<Avatar> getUnlockedAvatars() {
        List<Avatar> achievementList = new ArrayList<>();

        for (Achievement achievement : getUnlockedAchievements()) {

            if (achievement.isKindAvatar()) {
                achievementList.add(new Avatar(achievement));
            }
        }

        return achievementList;
    }

    public List<Selfie> getUnlockedSelfies() {
        List<Selfie> achievementList = new ArrayList<>();

        for (Achievement achievement : getUnlockedAchievements()) {

            if (achievement.isKindSelfie()) {
                achievementList.add(new Selfie(achievement));
            }
        }

        return achievementList;
    }

    private String achievementToJson(Achievement achievement) {
        return GsonProvider.getGson().toJson(achievement);
    }

    private Achievement jsonToAchievement(String json) {
        return GsonProvider.getGson().fromJson(json, Achievement.class);
    }

    public Selfie getUnlockedSelfieById(int id) {
        List<Selfie> unlockedSelfies = getUnlockedSelfies();
        for (Selfie unlockedSelfie : unlockedSelfies ) {
            if (unlockedSelfie.getId() == id) {
                return unlockedSelfie;
            }
        }

        return null;
    }

    private static class SingletonHolder {
        public static final AchievementsRepository INSTANCE = new AchievementsRepository();
    }

    public static AchievementsRepository getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private String getUnlockedAchievementsKeyFormatted() {
        if (Profile.getCurrentProfile() != null) {
            return String.format(SHAREDEPREFERENCES_UNLOCKEDACHIEVEMENTS_KEY, Profile.getCurrentProfile().getId());
        }

        return SHAREDEPREFERENCES_UNLOCKEDACHIEVEMENTS_KEY;
    }
}
