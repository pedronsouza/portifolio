package br.com.zeroum.gloob.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import com.adobe.mediacore.info.Track;

import java.util.List;

import br.com.zeroum.gloob.android.activities.GameStageActivity;
import br.com.zeroum.gloob.android.models.Game;
import br.com.zeroum.gloob.android.models.Program;
import br.com.zeroum.gloob.android.models.TrackManager;
import br.com.zeroum.gloob.android.support.Urls;
import br.com.zeroum.gloob.android.support.ui.widget.ThumbNetworkImageView;

/**
 * Created by zeroum on 7/22/15.
 */
public class GridGamesAdapter extends BaseAdapter {
    private List<Game> games;
    private Program mProgram;
    private Context mContext;
    private int mTouchedGameIndex;

    public GridGamesAdapter(Context context, List<Game> gameList, Program program) {
        games = gameList;
        mContext = context;
        mProgram = program;
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ThumbNetworkImageView thumbView;
        Game game = games.get(position);

        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();

        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 160, metrics);
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 92, metrics);

        if (convertView == null) {
            thumbView = new ThumbNetworkImageView(mContext, width, height);
        } else {
            thumbView = (ThumbNetworkImageView) convertView;
        }
        thumbView.setLayoutParams(new AbsListView.LayoutParams(width, height));
        thumbView.setTag(position);
        thumbView.setImageUrl(game.getImage());
        thumbView.setOnClickListener(gameClickListener);

        return thumbView;
    }

    private View.OnClickListener gameClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = (int) v.getTag();
            mTouchedGameIndex = index;

            TrackManager.getInstance().trackPageView(trackablePageView);
            TrackManager.getInstance().trackEvent(playGameEvent);

            Game game = games.get(index);
            Intent gameStage = new Intent(mContext, GameStageActivity.class);
            gameStage.putExtra(GameStageActivity.BUNDLE_GAME_KEY, game);

            mContext.startActivity(gameStage);
        }
    };

    private final TrackManager.ITrackablePageView trackablePageView = new TrackManager.ITrackablePageView() {
        @Override
        public String getScreenName() {
            return String.format("/videos/%s/%s/atalho-jogos", Urls.slugify(mProgram.getTitle()), Urls.slugify(games.get(mTouchedGameIndex).getName()));
        }
    };

    private final TrackManager.ITrackableEvent playGameEvent = new TrackManager.ITrackableEvent() {
        @Override
        public String getCategoryForTrack() {
            return "Clique - Jogar";
        }

        @Override
        public String getLabelForTrack() {
            return String.format("videos|%s", Urls.slugify(games.get(mTouchedGameIndex).getName()));
        }
    };
}
