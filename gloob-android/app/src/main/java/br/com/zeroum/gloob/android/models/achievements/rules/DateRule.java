package br.com.zeroum.gloob.android.models.achievements.rules;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by zeroum on 4/17/15.
 */

public class DateRule extends BaseRule implements  IRule, Parcelable {
    @SerializedName("start_date")
    @Expose
    private Date startDate;
    @SerializedName("end_date")
    @Expose
    private Date endDate;

    private void setupDates() {
        int year = new DateTime().getYear();
        Calendar c = Calendar.getInstance();

        c.setTime(startDate);
        c.set(Calendar.YEAR, year);
        startDate = c.getTime();

        c.setTime(endDate);
        c.set(Calendar.YEAR, year);
        endDate = c.getTime();
    }

    @Override
    public boolean isValid() {
        setupDates();
        DateTime start = new DateTime(startDate);
        DateTime end = new DateTime(endDate);
        DateTime now = new DateTime(new Date());

        return (now.isAfter(start) && now.isBefore(end) || now.isEqual(end));
    }

    @Override
    public boolean shouldMark() {
        return isValid();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(startDate != null ? startDate.getTime() : -1);
        dest.writeLong(endDate != null ? endDate.getTime() : -1);
    }

    public DateRule() {
    }

    protected DateRule(Parcel in) {
        long tmpStartDate = in.readLong();
        this.startDate = tmpStartDate == -1 ? null : new Date(tmpStartDate);
        long tmpEndDate = in.readLong();
        this.endDate = tmpEndDate == -1 ? null : new Date(tmpEndDate);
    }

    public static final Parcelable.Creator<DateRule> CREATOR = new Parcelable.Creator<DateRule>() {
        public DateRule createFromParcel(Parcel source) {
            return new DateRule(source);
        }

        public DateRule[] newArray(int size) {
            return new DateRule[size];
        }
    };
}
