package br.com.zeroum.gloob.android.support;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static br.com.zeroum.gloob.android.GloobApplication.application;

/**
 * Created by zeroum on 4/16/15.
 */
public class FileUtils {
    public static String getFileContentByResId(int resId) {
        String fileContent = null;
        try {
            InputStream inputStream = application().getResources().openRawResource(resId);;

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                fileContent = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("FileUtils", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("FileUtils", "Can not read file: " + e.toString());
        }

        return fileContent;
    }
}
