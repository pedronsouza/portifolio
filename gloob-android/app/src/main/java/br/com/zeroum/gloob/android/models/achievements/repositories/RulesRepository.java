package br.com.zeroum.gloob.android.models.achievements.repositories;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.zeroum.gloob.android.BuildConfig;
import br.com.zeroum.gloob.android.R;
import br.com.zeroum.gloob.android.models.achievements.Achievement;
import br.com.zeroum.gloob.android.models.achievements.Avatar;
import br.com.zeroum.gloob.android.models.achievements.Selfie;
import br.com.zeroum.gloob.android.models.achievements.factories.RulesFactory;
import br.com.zeroum.gloob.android.models.achievements.rules.BaseRule;
import br.com.zeroum.gloob.android.support.FileUtils;

/**
 * Created by zeroum on 4/16/15.
 */

public class RulesRepository {
    private List<BaseRule> rules;
    private static RulesRepository mInstance = null;

    private RulesRepository() {
        String rulesRaw = (BuildConfig.DEBUG) ? FileUtils.getFileContentByResId(R.raw.rules) :  FileUtils.getFileContentByResId(R.raw.rules);

        BaseRule[] data = null;
        rules = new ArrayList<>();

        try {
            JSONArray raw = new JSONArray(rulesRaw);
            BaseRule rule;
            for (int i = 0; i < raw.length(); i++) {
                JSONObject obj = raw.getJSONObject(i);
                rule = RulesFactory.getInstance().createRuleByCondition(obj.getString("condition"), obj);

                rules.add(rule);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<BaseRule> getRules() {
        return rules;
    }

    public BaseRule getRuleById(int id) {
        BaseRule obj = null;
        for(BaseRule rule : this.rules) {

            if(rule == null) continue;

            try {
                if (rule.getId() == id) {
                    obj = rule;
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return obj;
    }

    public List<BaseRule> getRulesByCondition(String condition) {
        List<BaseRule> ruleList = new ArrayList<>();
        BaseRule rule;
        for(int i = 0; i < this.rules.size(); i++) {
            rule = this.rules.get(i);

            if(rule == null) continue;

            if (rule.getCondition().equals(condition)) {
                ruleList.add(rule);
            }
        }

        return ruleList;
    }

    public void setupAchievementsForEachRules() {
        List<BaseRule> baseRules = new ArrayList<>();

        try {
            for (BaseRule rule : this.rules) {

                List<Avatar> avatars = new ArrayList<>();
                List<Selfie> selfies = new ArrayList<>();

                if(rule == null) continue;

                for (int i = 0; i < rule.getAvatarsRaw().size(); i++) {
                    avatars.add(AchievementsRepository.getInstance().getAvatarById(rule.getAvatarsRaw().get(i)));
                }

                for (int x = 0; x < rule.getSelfiesRaw().size(); x++) {
                    selfies.add(AchievementsRepository.getInstance().getSelfieById(rule.getSelfiesRaw().get(x)));
                }

                rule.setAvatars(avatars);
                rule.setSelfies(selfies);
                baseRules.add(rule);
            }

            this.rules = baseRules;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<BaseRule> getRulesByAchievement(Achievement achievement) {
        List<BaseRule> rules = new ArrayList<>();

        for (int i = 0; i < achievement.getRulesRaw().length; i++) {
            rules.add(getRuleById(achievement.getRulesRaw()[i]));
        }

        return rules;
    }

    public static RulesRepository getInstance() {
        if(mInstance == null) {
            mInstance = new RulesRepository();
        }
        return mInstance;
    }
}
