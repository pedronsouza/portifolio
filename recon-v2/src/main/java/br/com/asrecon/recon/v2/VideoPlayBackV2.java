//================================================================================================================================
//
//  Copyright (c) 2015-2017 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

package br.com.asrecon.recon.v2;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.HashMap;

import cn.easyar.Engine;


abstract public class VideoPlayBackV2 extends AppCompatActivity implements GLView.CloudReconListener
{

    protected GLView glView;
    protected View mainContentView;

    abstract protected void onReconInitError(Throwable e);
    abstract protected View getMainContentView();
    abstract protected void onMainContendViewInserted();

    protected ViewGroup getRootViewGroup() {
        return ((ViewGroup) findViewById(R.id.activity_video_playback_v2_preview));
    }


    @Override
    public void onCloudReconIsReady() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onMainContendViewInserted();
            }
        });
    }

    @Override
    public void onCloudAccessDenied() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(VideoPlayBackV2.this, "Desculpe, mas você não possui acesso a este conteúdo", Toast.LENGTH_LONG).show();
            }
        });
    }

    protected boolean playerIsReadyToBeControledByUser() {
        return glView != null && glView.ready();
    }

    protected boolean isPlaying() {
        return glView.playing();
    }

    protected void loadExtraTargets(String path) {
        glView.loadExtraTargets(path);
    }

    protected void initAppRecon(String cloudServerAddress, String cloudKey, String cloudSecret, String key, Boolean offlineMode, String jsonPath, String userId) {
        if (!Engine.initialize(this, key)) {
            Log.e("HelloAR", "Initialization Failed.");
        }

        ViewGroup root = ((ViewGroup) findViewById(R.id.activity_video_playback_v2_preview));

        if (glView != null) {
            root.removeView(glView);
            glView = new GLView(this, cloudServerAddress, cloudKey, cloudSecret, this, offlineMode, jsonPath, userId);
            root.addView(glView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        } else {
            mainContentView = getMainContentView();
            glView = new GLView(this, cloudServerAddress, cloudKey, cloudSecret, this, offlineMode, jsonPath, userId);
            root.addView(glView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            root.addView(mainContentView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_playback_v2);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private interface PermissionCallback
    {
        void onSuccess();
        void onFailure();
    }
    private HashMap<Integer, PermissionCallback> permissionCallbacks = new HashMap<Integer, PermissionCallback>();
    private int permissionRequestCodeSerial = 0;
    @TargetApi(23)
    private void requestCameraPermission(PermissionCallback callback)
    {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                int requestCode = permissionRequestCodeSerial;
                permissionRequestCodeSerial += 1f;
                permissionCallbacks.put(requestCode, callback);
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, requestCode);
            } else {
                callback.onSuccess();
            }
        } else {
            callback.onSuccess();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (permissionCallbacks.containsKey(requestCode)) {
            PermissionCallback callback = permissionCallbacks.get(requestCode);
            permissionCallbacks.remove(requestCode);
            boolean executed = false;
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    executed = true;
                    callback.onFailure();
                }
            }
            if (!executed) {
                callback.onSuccess();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        try {
            if (glView != null) { glView.onResume(); }
        } catch (Throwable e) {
            //xD
        }
    }

    @Override
    protected void onPause()
    {
        try {
            if (glView != null) { glView.onPause(); }
        } catch (Throwable e) {
            //xD
        }
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
