//================================================================================================================================
//
//  Copyright (c) 2015-2017 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

package br.com.asrecon.recon.v2;

import android.opengl.GLES20;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import cn.easyar.CameraCalibration;
import cn.easyar.CameraDevice;
import cn.easyar.CameraDeviceFocusMode;
import cn.easyar.CameraDeviceType;
import cn.easyar.CameraFrameStreamer;
import cn.easyar.CloudRecognizer;
import cn.easyar.CloudStatus;
import cn.easyar.Frame;
import cn.easyar.FunctorOfVoidFromCloudStatus;
import cn.easyar.FunctorOfVoidFromCloudStatusAndListOfPointerOfTarget;
import cn.easyar.FunctorOfVoidFromPointerOfTargetAndBool;
import cn.easyar.ImageTarget;
import cn.easyar.ImageTracker;
import cn.easyar.Renderer;
import cn.easyar.StorageType;
import cn.easyar.Target;
import cn.easyar.TargetInstance;
import cn.easyar.TargetStatus;
import cn.easyar.Vec2I;
import cn.easyar.Vec4I;

public class HelloAR
{
    private CameraDevice camera;
    private CameraFrameStreamer streamer;
    private ArrayList<ImageTracker> trackers;
    private Renderer videobg_renderer;
    private ArrayList<VideoRenderer> video_renderers;
    private VideoRenderer current_video_renderer;
    private int tracked_target = 0;
    private int active_target = 0;
    private CloudRecognizer cloud_recognizer;
    public ARVideo video = null;
    private boolean viewport_changed = false;
    private String currentVideoUrl = null;
    private Vec2I view_size = new Vec2I(0, 0);
    public boolean offlineModeOn = false;
    private int rotation = 0;
    private Vec4I viewport = new Vec4I(0, 0, 1280, 720);
    private GLView.CloudReconListener listener;
    private String userId;
    private ArrayList<String> loadedExtraTargetsId = new ArrayList<>();

    public HelloAR(GLView.CloudReconListener listener)
    {
        trackers = new ArrayList<ImageTracker>();
        this.listener = listener;
    }

    public String getCurrentVideoUrl() { return currentVideoUrl; }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    private void loadFromImage(ImageTracker tracker, String path)
    {
        ImageTarget target = new ImageTarget();
        String jstr = "{\n"
            + "  \"images\" :\n"
            + "  [\n"
            + "    {\n"
            + "      \"image\" : \"" + path + "\",\n"
            + "      \"name\" : \"" + path.substring(0, path.indexOf(".")) + "\"\n"
            + "    }\n"
            + "  ]\n"
            + "}";
        target.setup(jstr, StorageType.Assets | StorageType.Json, "");
        tracker.loadTarget(target, new FunctorOfVoidFromPointerOfTargetAndBool() {
            @Override
            public void invoke(Target target, boolean status) {
                Log.i("HelloAR", String.format("load target (%b): %s (%d)", status, target.name(), target.runtimeID()));
            }
        });
    }

    public void loadExtraTargets(String path) {
        this.loadAllFromJsonFile(trackers.get(0), path);
    }

    private void loadAllFromJsonFile(ImageTracker tracker, String path)
    {

        for (ImageTarget target : ImageTarget.setupAll(path, StorageType.Assets)) {
            final String key = target.meta() + target.name();
            if (!loadedExtraTargetsId.contains(key)) {
                tracker.loadTarget(target, new FunctorOfVoidFromPointerOfTargetAndBool() {
                    @Override
                    public void invoke(Target target, boolean status) {
                        try {
                            loadedExtraTargetsId.add(key);
                            Log.i("HelloAR", String.format("load target (%b): %s (%d)", status, target.name(), target.runtimeID()));
                        } catch (Throwable ex) {
                        }
                    }
                });
            }

        }
    }

    public boolean initialize(String cloud_server_address,
                              String cloud_key,
                              String cloud_secret,
                              Boolean offlineMode,
                              String jsonPath,
                              String userId)
    {
        this.userId = userId;
        if (offlineMode) {
            camera = new CameraDevice();
            streamer = new CameraFrameStreamer();
            streamer.attachCamera(camera);

            boolean status = true;
            offlineModeOn = true;
            status &= camera.open(CameraDeviceType.Default);
            camera.setSize(new Vec2I(1280, 720));

            if (!status) { return status; }
            ImageTracker tracker = new ImageTracker();
            tracker.attachStreamer(streamer);
            loadAllFromJsonFile(tracker, jsonPath);
            trackers.add(tracker);

            return status;
        } else {
            if (camera == null)
                camera = new CameraDevice();

            streamer = new CameraFrameStreamer();
            streamer.attachCamera(camera);
            cloud_recognizer = new CloudRecognizer();
            cloud_recognizer.attachStreamer(streamer);

            boolean status = true;
            status &= camera.open(CameraDeviceType.Default);
            camera.setSize(new Vec2I(1280, 720));

            cloud_recognizer.open(cloud_server_address, cloud_key, cloud_secret, new FunctorOfVoidFromCloudStatus() {
                @Override
                public void invoke(int status) {
                    listener.onCloudReconInit();
                    if (status == CloudStatus.Success) {
                        Log.i("HelloAR", "CloudRecognizerInitCallBack: Success");
                    } else if (status == CloudStatus.Reconnecting) {
                        Log.i("HelloAR", "CloudRecognizerInitCallBack: Reconnecting");
                    } else if (status == CloudStatus.Fail) {
                        Log.i("HelloAR", "CloudRecognizerInitCallBack: Fail");
                    } else {
                        Log.i("HelloAR", "CloudRecognizerInitCallBack: " + Integer.toString(status));
                    }
                }
            }, new FunctorOfVoidFromCloudStatusAndListOfPointerOfTarget() {
                private HashSet<String> uids = new HashSet<String>();
                @Override
                public void invoke(int status, ArrayList<Target> targets) {
                    if (status == CloudStatus.Success) {

                        Log.i("HelloAR", "CloudRecognizerCallBack: Success");
                    } else if (status == CloudStatus.Reconnecting) {
                        Log.i("HelloAR", "CloudRecognizerCallBack: Reconnecting");
                    } else if (status == CloudStatus.Fail) {
                        Log.i("HelloAR", "CloudRecognizerCallBack: Fail");
                    } else {
                        Log.i("HelloAR", "CloudRecognizerCallBack: " + Integer.toString(status));

                    }
                    synchronized (uids) {
                        for (Target t : targets) {
                            if (!uids.contains(t.uid())) {
                                Log.i("HelloAR", "add cloud target: " + t.uid());
                                uids.add(t.uid());
                                trackers.get(0).loadTarget(t, new FunctorOfVoidFromPointerOfTargetAndBool() {
                                    @Override
                                    public void invoke(Target target, boolean status) {
                                        listener.onCloudReconFinished(GLView.CloudReconStatus.READY.statusValue);
                                        Log.i("HelloAR", String.format("load target (%b): %s (%s)", status, target.name(), target.meta()));
                                    }
                                });
                            }
                        }
                    }
                }
            });

            if (!status) { return status; }
            ImageTracker tracker = new ImageTracker();
            tracker.attachStreamer(streamer);
            trackers.add(tracker);

            return status;


        }
    }

    public void dispose()
    {
        if (video != null) {
            video.dispose();
            video = null;
        }
        tracked_target = 0;
        active_target = 0;

        for (ImageTracker tracker : trackers) {
            tracker.dispose();
        }
        trackers.clear();
        video_renderers.clear();
        current_video_renderer = null;
        if (videobg_renderer != null) {
            videobg_renderer.dispose();
            videobg_renderer = null;
        }
        if (streamer != null) {
            streamer.dispose();
            streamer = null;
        }
        if (camera != null) {
            camera.dispose();
            camera = null;
        }
    }

    public boolean start()
    {
        boolean status = true;
        status &= (camera != null) && camera.start();
        status &= (streamer != null) && streamer.start();
        status &= (cloud_recognizer != null) && cloud_recognizer.start();
        camera.setFocusMode(CameraDeviceFocusMode.Continousauto);
        for (ImageTracker tracker : trackers) {
            status &= tracker.start();
        }

        listener.onCloudReconIsReady();

        if (offlineModeOn)
            listener.onCloudReconInit();
        return status;
    }

    public boolean stop()
    {
        boolean status = true;
        for (ImageTracker tracker : trackers) {
            status &= tracker.stop();
        }
        status &= (streamer != null) && streamer.stop();
        status &= (camera != null) && camera.stop();
        status &= (cloud_recognizer != null) && cloud_recognizer.stop();
        return status;
    }

    public void initGL()
    {
        if (active_target != 0) {
            video.onLost();
            video.dispose();
            video  = null;
            tracked_target = 0;
            active_target = 0;
        }
        if (videobg_renderer != null) {
            videobg_renderer.dispose();
        }
        videobg_renderer = new Renderer();
        video_renderers = new ArrayList<VideoRenderer>();
        for (int k = 0; k < 1; k += 1) {
            VideoRenderer video_renderer = new VideoRenderer();
            video_renderer.init();
            video_renderers.add(video_renderer);
        }
        current_video_renderer = null;
    }

    public void resizeGL(int width, int height)
    {
        view_size = new Vec2I(width, height);
        viewport_changed = true;
    }

    private void updateViewport()
    {
        CameraCalibration calib = camera != null ? camera.cameraCalibration() : null;
        int rotation = calib != null ? calib.rotation() : 0;
        if (rotation != this.rotation) {
            this.rotation = rotation;
            viewport_changed = true;
        }
        if (viewport_changed) {
            Vec2I size = new Vec2I(1, 1);
            if ((camera != null) && camera.isOpened()) {
                size = camera.size();
            }
            if (rotation == 90 || rotation == 270) {
                size = new Vec2I(size.data[1], size.data[0]);
            }
            float scaleRatio = Math.max((float) view_size.data[0] / (float) size.data[0], (float) view_size.data[1] / (float) size.data[1]);
            Vec2I viewport_size = new Vec2I(Math.round(size.data[0] * scaleRatio), Math.round(size.data[1] * scaleRatio));
            viewport = new Vec4I((view_size.data[0] - viewport_size.data[0]) / 2, (view_size.data[1] - viewport_size.data[1]) / 2, viewport_size.data[0], viewport_size.data[1]);

            if ((camera != null) && camera.isOpened())
                viewport_changed = false;
        }
    }

    public static URL getFinalURL(URL url) {
        try {
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setInstanceFollowRedirects(false);
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
            con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
            con.addRequestProperty("Referer", "https://www.google.com/");
            con.connect();
            //con.getInputStream();
            int resCode = con.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_SEE_OTHER
                    || resCode == HttpURLConnection.HTTP_MOVED_PERM
                    || resCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                String Location = con.getHeaderField("Location");
                if (Location.startsWith("/")) {
                    Location = url.getProtocol() + "://" + url.getHost() + Location;
                }
                return getFinalURL(new URL(Location));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return url;
    }

    public void render()
    {
        GLES20.glClearColor(1.f, 1.f, 1.f, 1.f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (videobg_renderer != null) {
            Vec4I default_viewport = new Vec4I(0, 0, view_size.data[0], view_size.data[1]);
            GLES20.glViewport(default_viewport.data[0], default_viewport.data[1], default_viewport.data[2], default_viewport.data[3]);
            if (videobg_renderer.renderErrorMessage(default_viewport)) {
                return;
            }
        }

        if (streamer == null) { return; }
        Frame frame = streamer.peek();
        try {
            updateViewport();
            GLES20.glViewport(viewport.data[0], viewport.data[1], viewport.data[2], viewport.data[3]);

            if (videobg_renderer != null) {
                videobg_renderer.render(frame, viewport);
            }

            ArrayList<TargetInstance> targetInstances = frame.targetInstances();
            if (targetInstances.size() > 0) {
                TargetInstance targetInstance = targetInstances.get(0);
                Target target = targetInstance.target();
                int status = targetInstance.status();
                if (status == TargetStatus.Tracked) {
                    int id = target.runtimeID();
                    if (active_target != 0 && active_target != id) {
                        video.onLost();
                        video.dispose();
                        video  = null;
                        tracked_target = 0;
                        active_target = 0;
                    }
                    if (tracked_target == 0) {
                        if (video == null && video_renderers.size() > 0) {
                            listener.onCloudReconStartLoadTarget();
                            String target_name = target.name();
                            String target_meta = target.meta();
                            if (target_meta != null) {
                                try {

                                    if (target_meta.startsWith("{")) {
                                        JSONObject obj = new JSONObject(target_meta);
                                        String videoUrl = obj.getString("videoUrl");
                                        currentVideoUrl = videoUrl;
                                        video = new ARVideo();
                                        video.openVideoFile(videoUrl, video_renderers.get(0).texId(), StorageType.Absolute);
                                        current_video_renderer = video_renderers.get(0);
                                    } else {
                                        byte[] metaData = Base64.decode(target_meta, Base64.NO_WRAP | Base64.URL_SAFE);
                                        JSONObject obj = new JSONObject(new String(metaData, Base64.DEFAULT));
                                        String videoUrl = obj.getString("videoUrl");
                                        URL redirectUrl;

                                        video = new ARVideo();
                                        currentVideoUrl = videoUrl;
                                        if (obj.has("userId")) {
                                            String userId = obj.getString("userId");

                                            if (this.userId != null && userId != null) {
                                                if (this.userId.contentEquals(userId)) {
                                                    redirectUrl = getFinalURL(new URL(videoUrl));
                                                } else {
                                                    throw new VideoAccessDeniedException();

                                                }
                                            } else {
                                                redirectUrl = getFinalURL(new URL(videoUrl));
                                            }
                                        } else {
                                            redirectUrl = getFinalURL(new URL(videoUrl));
                                        }


                                        video.openStreamingVideo(redirectUrl.toString(), video_renderers.get(0).texId());
                                        current_video_renderer = video_renderers.get(0);
                                    }

                                } catch(VideoAccessDeniedException e) {
                                    listener.onCloudAccessDenied();
                                } catch (Throwable e) {
                                    e.printStackTrace();
                                    listener.onCloudReconFinished(GLView.CloudReconStatus.FAIL.statusValue);
                                }
                            }


                        }
                        if (video != null) {
                            video.onFound();
                            tracked_target = id;
                            active_target = id;
                        }
                    }
                    ImageTarget imagetarget = target instanceof ImageTarget ? (ImageTarget)(target) : null;
                    if (imagetarget != null) {
                        if (current_video_renderer != null) {
                            video.update();
                            if (video.isRenderTextureAvailable()) {
                                current_video_renderer.render(camera.projectionGL(0.2f, 500.f), targetInstance.poseGL(), imagetarget.size());
                            }
                        }
                    }
                }
            } else {
                if (tracked_target != 0) {
                    video.onLost();
                    tracked_target = 0;
                }
            }
        }
        finally {
            frame.dispose();
        }
    }

    class VideoAccessDeniedException extends Throwable {

    }
}
