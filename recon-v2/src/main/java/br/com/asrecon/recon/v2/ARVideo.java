//================================================================================================================================
//
//  Copyright (c) 2015-2017 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

package br.com.asrecon.recon.v2;

import android.util.Log;

import cn.easyar.FunctorOfVoidFromVideoStatus;
import cn.easyar.StorageType;
import cn.easyar.VideoPlayer;
import cn.easyar.VideoStatus;
import cn.easyar.VideoType;

public class ARVideo
{
    private VideoPlayer player;
    private boolean prepared;
    private boolean found;
    private String path;
    private int videoStatus;
    public boolean isPlaying = false;

    public int getVideoStatus() { return videoStatus; }

    public int currentPosition() {  return player.currentPosition();}

    public void play() {
        isPlaying = true;
        player.play();
    }

    public void pause() {
        isPlaying = false;
        player.pause();
    }

    public ARVideo()
    {
        player = new VideoPlayer();
        prepared = false;
        found = false;
    }
    public void dispose()
    {
        isPlaying = false;
        player.close();
    }

    public void openVideoFile(String path, int texid)
    {
        this.path = path;
        player.setRenderTexture(texid);
        player.setVideoType(VideoType.Normal);
        player.open(path, StorageType.Assets, new FunctorOfVoidFromVideoStatus() {
            @Override
            public void invoke(int status) {
                setVideoStatus(status);
            }
        });
    }

    public void openVideoFile(String path, int texid, int storageType)
    {
        this.path = path;
        player.setRenderTexture(texid);
        player.setVideoType(VideoType.Normal);
        player.open(path, storageType, new FunctorOfVoidFromVideoStatus() {
            @Override
            public void invoke(int status) {
                setVideoStatus(status);
            }
        });
    }

    public void openVideoFile(String path, int textid, int storageType, final GLView.CloudReconListener listener) {
        this.path = path;
        player.setRenderTexture(textid);
        player.setVideoType(VideoType.Normal);
        player.open(path, storageType, new FunctorOfVoidFromVideoStatus() {
            @Override
            public void invoke(final int status) {
                setVideoStatus(status);
                listener.onCloudVideoStatusChanged(status);
            }
        });


    }

    public void openTransparentVideoFile(String path, int texid)
    {
        this.path = path;
        player.setRenderTexture(texid);
        player.setVideoType(VideoType.TransparentSideBySide);
        player.open(path, StorageType.Assets, new FunctorOfVoidFromVideoStatus() {
            @Override
            public void invoke(int status) {
                setVideoStatus(status);
            }
        });
    }
    public void openStreamingVideo(String url, int texid)
    {
        isPlaying = false;
        this.path = url;
        player.setRenderTexture(texid);
        player.setVideoType(VideoType.Normal);
        player.open(url, StorageType.Absolute, new FunctorOfVoidFromVideoStatus() {
            @Override
            public void invoke(int status) {
                setVideoStatus(status);
            }
        });
    }

    public void setVideoStatus(int status)
    {
        videoStatus = status;
        Log.i("HelloAR", "video: " + path + " (" + Integer.toString(status) + ")");
        if (status == VideoStatus.Ready) {
            prepared = true;
            if (found) {
                isPlaying = true;
                player.play();
            }
        } else if (status == VideoStatus.Completed) {
            if (found) {
                isPlaying = true;
                player.play();
            }
        }
    }

    public void onFound()
    {
        found = true;
        if (prepared) {
            isPlaying = true;
            player.play();
        }
    }
    public void onLost()
    {
        found = false;
        if (prepared) {
            isPlaying = false;
            player.pause();
        }
    }
    public boolean isRenderTextureAvailable()
    {
        return player.isRenderTextureAvailable();
    }
    public void update()
    {
        player.updateFrame();
    }
}
