(function(){
	var updateOrders = function() {
		if ($('#url-polling').length > 0){
			var url = $('#url-polling').val();
			var last_order_time = $('.order-holder:first').attr('data-time');
			$.ajax({
				url: url,
				data: { after: last_order_time },
				success: function(response) {
					$('#all-orders').prepend(response);
				}
			});	
		}

		setTimeout(updateOrders, 10000);
	}

	updateOrders();
})();