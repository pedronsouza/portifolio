class Api::V1::OrderItemsController < ApiController

	api :GET, "/orders/:order_id/order_items"
	param :order_id, :number, required: true
	description "Retorna todos os itens de um pedido"
	def index
		begin
			render json: current_order.items	
		rescue Exception => e
			render json: {error: 'Order not found'}, status: 404
		end
		
	end

	api :GET, "/orders/:order_id/order_items/:id"
	param :user_id, :number, required: true
	param :id, :number, required: true
	description "Retorna um item do pedido"
	def show
		begin
			render json: current_item	
		rescue Exception => e
			render json: {error: 'Item was not found'}, status: 404
		end
	end

	api :POST, "/orders/:order_id/order_items/"
	param :order_id, :number, required: true
	param "item[food_id]", :number, required: true
	param "item[observations]", String, required: true
	param "item[amount]", :number, required: true
	description "Adiciona um novo item ao pedido"
	def create
		@item = current_order.items.create(params[:item])
		if @item.save
			render json: @item
		else
			render json: {error: @item.errors}, status: 500
		end
	end

	api :PUT, "/orders/:order_id/order_items/:id"
	param :order_id, :number, required: true
	param :id, :number, required: true
	param "item[food_id]", :number, required: true
	param "item[observations]", String, required: true
	param "item[amount]", :number, required: true
	description "Adiciona um novo item ao pedido"
	def update
		@item = current_item
		@item.update_attributes!(params[:item])

		if @item.save
			render json: @item
		else
			render json: {error: @item.errors}, status: 500
		end
	end

	api :DELETE, "/orders/:order_id/order_items/:id"
	param :id, :number, required: true
	def destroy
		begin
			current_item.destroy
			render json: ''
		rescue Exception => e
			render json: 'Unable to remove the given item'
		end
	end

	private
	def current_order
		@order = Order.find(params[:order_id])
	end

	def current_item
		@item = OrderItem.find(params[:id])
	end
end
