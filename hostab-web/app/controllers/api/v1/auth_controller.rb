# -*- encoding : utf-8 -*-
class Api::V1::AuthController < ApiController
	respond_to :json

	api :POST, '/auth'
	param :user, Hash, :desc => "User info" do
		param :password, String, required: true
	end
	description "Cria um novo pedido para o usuário"
	def create
		@user = get_user
		if @user
			render status: 200, json: @user
		else
			render status: 404, json: {error: 'No user found with given password' }
		end
	end

	private
	def get_user
		@user = current_company.users.first(conditions: { password: params[:user][:password] })
	end
end
