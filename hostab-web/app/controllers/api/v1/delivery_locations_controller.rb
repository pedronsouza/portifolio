class Api::V1::DeliveryLocationsController < ApiController
  
  api :GET, "/delivery_locations"
  description "Retorna todos os destinos de entrega cadastradas para o hotel"
  def index
   render json: current_company.delivery_locations.all.to_json
  end
end
