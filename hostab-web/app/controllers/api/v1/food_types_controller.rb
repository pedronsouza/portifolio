# -*- encoding : utf-8 -*-
class Api::V1::FoodTypesController < ApiController

	api :GET, '/food_types/'
	description "Retorna as categorias existentes no cardápio"
	def index
		render json: FoodType.order(:position)
	end

end
