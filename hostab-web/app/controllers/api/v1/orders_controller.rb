# -*- encoding : utf-8 -*-
class Api::V1::OrdersController < ApiController
	api :GET, "/users/:user_id/orders"
	param :user_id, :number, required: true
	description "Retorna todos os pedidos do cliente"
	def index
		render json: current_user.orders
	end

	api :POST, '/users/:user_id/orders'
	param "order[user_id]", :number, required: true
	
	description "Cria um novo pedido para o usuário"
	def create
		params[:order][:company_id] = current_user.company_id
		@order = current_user.orders.create(params[:order])
		render json: @order 
	end

	api :PUT, '/users/:user_id/orders/:id'
	param :order, Hash, :required => true do
    param :status, Integer, '0 => requested, 1 => processing, 2 => delivered, 3 => canceled'
    param :delivery_time, Time, 'Horário de entrega do pedido'
    param :delivery_location, Time, 'Local de entrega'
  end
	def update
		@order = Order.find(params[:id])
		@order.update_attributes(params[:order])
		render json: @order if @order.save
	end

	def show
	end

	private
	def current_user
		@user = User.find(params[:user_id])
	end
end
