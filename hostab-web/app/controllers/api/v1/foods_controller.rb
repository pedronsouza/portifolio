# -*- encoding : utf-8 -*-
class Api::V1::FoodsController < ApiController

	api :GET, '/food_types/:food_type_id/foods'
	param :food_type_id, :number
	description 'Retorna todos os alimentos de uma determinada categoria'
	def index
		render json: current_food_type.foods(params[:food_type]).order(:position)
	end

	api :GET, '/food_types/:food_type_id/foods/:id'
	param :food_type_id, :number
	param :food_id, :number
	description 'Retorna um determinado alimento de uma determinada categoria'
	def show
		render json: get_food
	end

	private
	def get_food
		@food = Food.find(params[:id])
	end

	def current_food_type
		@food_type = FoodType.find(params[:food_type_id])
	end
end
