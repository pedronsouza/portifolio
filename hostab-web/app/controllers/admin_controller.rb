class AdminController < ActionController::Base
	protect_from_forgery

	layout 'application'
	before_filter :restrict_access
	helper_method :current_user, :user_signed_in?

	private
	def restrict_access
		redirect_to admin_sign_in_path if session[:admin_user_id].nil?
	end

	def current_user
    @current_user ||= ::Admin::User.find(session[:admin_user_id]) if session[:admin_user_id]
  	rescue ActiveRecord::RecordNotFound
    	session.delete(:admin_user_id)
    nil
  end
end