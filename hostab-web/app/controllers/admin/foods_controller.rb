class Admin::FoodsController < AdminController
	helper_method :current_food_type
	def index
		@foods = current_food_type.foods
	end

	def new
		@food = Food.new
	end

	def create
		@food = current_food_type.foods.create(params[:food])
		if @food.save
			redirect_to admin_food_type_foods_path(@food.food_type_id)
		else
			flash[:notice] = "Preencha os campos corretamente"
			redirect_to new_admin_food_type_food_path(params[:food_type_id])
		end
	end

	def edit
		@food = current_food
		@food_image = FoodImage.new
	end

	def update
		@food = current_food
		@food.update_attributes(params[:food])
		if @food.save
			redirect_to admin_food_type_foods_path(@food.food_type_id)
		else
			flash[:notice] = "Preencha os campos corretamente"
			redirect_to edit_admin_food_type_food_path(@food.food_type_id, @food.id)
		end
	end

	def destroy
		current_food.destroy
		redirect_to admin_food_type_foods_path(params[:food_type_id])
	end

	def add_image
		@food = current_food
		@food.food_images.create(params[:food_image])
		redirect_to edit_admin_food_type_food_path(@food.food_type_id, @food)
	end

	def remove_image
		FoodImage.destroy(params[:image_id])
		redirect_to edit_admin_food_type_food_path(current_food.food_type_id, current_food)
	end

	private
	def current_food_type
		@food_type = FoodType.unscoped.find(params[:food_type_id])
	end

	def current_food
		@food = Food.find(params[:id]) if params[:id]
	end
end
