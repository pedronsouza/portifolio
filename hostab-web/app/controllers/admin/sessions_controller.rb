class Admin::SessionsController < AdminController
	skip_before_filter :restrict_access
	layout false

  def new
  end

  def create
  	@user = Admin::User.authenticate(params[:email], params[:password])
  	unless @user.nil?
  		session[:admin_user_id] = @user.id
  		redirect_to admin_home_index_path
  	else
  		flash[:notice] = 'Email ou Senha invalidos'
  		redirect_to admin_sign_in_path
  	end
  end

  def destroy
  	session[:admin_user_id] = nil
  	redirect_to admin_sign_in_path
  end

end
