class Admin::HomeController < AdminController
  def index
  	@orders = Order.unclosed.order('created_at DESC').limit(10)
  	@users = current_user.company.users.current_at_hotel
  end
end
