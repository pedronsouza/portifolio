class Admin::DeliveryLocationsController < AdminController
  def index
    @delivery_locations = DeliveryLocation.all
  end

  def new
    @delivery_location = DeliveryLocation.new
  end

  def create
    @delivery_location = current_user.company.delivery_locations.create(params[:delivery_location])
    if @delivery_location.save
      redirect_to admin_delivery_locations_path
    else
      render :new
    end
  end

  def edit
    @delivery_location = DeliveryLocation.find(params[:id])
  end

  def update
    @delivery_location = DeliveryLocation.find(params[:id])
    @delivery_location.update_attributes(params[:delivery_location])

    if @delivery_location.save
      redirect_to admin_delivery_locations_path
    else
      render :edit
    end
  end

  def destroy
    DeliveryLocation.destroy(params[:id])
    redirect_to admin_delivery_locations_path
  end
end
