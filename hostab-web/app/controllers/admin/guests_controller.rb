class Admin::GuestsController < AdminController
	def index
		@users = ::User.by_company(current_user.company_id) if current_user
		@company = ::Company.find(current_user.company_id) if current_user
	end

	def new
		@user = ::User.new
	end

	def create
		@user = current_user.company.users.create(params[:user])
		if @user.save
			redirect_to admin_guests_path
		else
			flash[:notice] = 'Preencha os campos corretamente'
			redirect_to new_admin_guest_path
		end
	end

	def edit
		@user = ::User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])
		@user.update_attributes(params[:user])
		if @user.save
			redirect_to admin_guests_path
		else
			flash[:notice] = 'Preencha os campos corretamente'
			redirect_to new_admin_guest_path
		end
	end

	def destroy
		User.destroy(params[:id])
		redirect_to admin_guests_path
	end

	def search

	end
end
