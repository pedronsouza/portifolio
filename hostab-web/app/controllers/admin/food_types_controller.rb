
class Admin::FoodTypesController < AdminController

  def index
    @food_types = FoodType.unscoped.all
  end

  def new
    @food_type = FoodType.new
  end

  def create
    @food_type = current_user.company.food_types.create(params[:food_type])
    if @food_type.save
      redirect_to admin_menus_path
    else
      render :new
    end
  end

  def edit
    @food_type = FoodType.unscoped.find(params[:id])
  end

  def update
    @food_type = FoodType.unscoped.find(params[:id])
    @food_type.update_attributes(params[:food_type])
    if @food_type.save
      redirect_to admin_menus_path
    else
      render :edit
    end
  end

  def destroy
    FoodType.unscoped.destroy(params[:id])
    redirect_to admin_menus_path
  end
end
