class Kitchen::HomeController < AdminController

  layout 'kitchen'
  def index
    @orders = current_user.company.orders.where('status != 2').order('created_at DESC')
  end

  def polling_orders
    @orders = current_user.company.orders.where('status != ? AND created_at > ?', 2, Time.at(params[:after].to_i + 1)).order('created_at DESC')
    render partial: 'list'
  end

  def set_as_ready
    @order = Order.find(params[:order_id])
    @order.set_status :delivered
    @order.save
    redirect_to kitchen_home_index_path
  end

  def destroy
    Order.destroy(params[:id])
    redirect_to kitchen_home_index_path
  end

end
