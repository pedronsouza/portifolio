# -*- encoding : utf-8 -*-
class ApiController < ActionController::Base
	before_filter :restrict_access
	before_filter :set_locale
	respond_to :json
	
  private
  def set_locale
  	I18n.locale = request.headers["X-API-LOCALE"]
  end

  def current_company
  	authenticate_or_request_with_http_token do |token, options|
	    AccessToken.find_by_token(token).company
	  end
  end

  def restrict_access
	 	authenticate_or_request_with_http_token do |token, options|
	    AccessToken.exists?(token: token)
	  end
  end
end
