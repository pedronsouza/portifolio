# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  belongs_to :company
  has_many :orders
  attr_accessible :name, :room, :password, :selected_locale, :checkin_at, :checkout_at

  validates :name, presence: true
  validates :room, presence: true
  validates :company, presence: true
  validates :checkin_at, presence: true

  before_save :generate_password
  scope :by_company, ->(company_id) { User.all(conditions: { company_id: company_id }) }
  scope :current_at_hotel, where(checkout_at: nil).order('checkin_at DESC')


  def formated_checkin_date
    self.checkin_at.strftime('%d/%m/%Y %H:%M')
  end

  private
  def generate_password
  	self.password = rand(Time.now.strftime('%H%s').to_i).to_s[0,5]
  end
end
