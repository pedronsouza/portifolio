# -*- encoding : utf-8 -*-
require 'yaml'
class Company < ActiveRecord::Base
  has_many :access_tokens
  has_many :delivery_locations
  has_many :food_types
  has_many :users
  has_many :orders
  
  attr_accessible :application_id, :weather_id, :application_secret, :name
  attr_accessor :menu_yaml_cache

  validates :name, presence: true
  validates :application_id, presence: true
  validates :application_secret, presence: true

  after_validation :generate_application_credentials

  def menu_path
     "#{Rails.root}/config/menus/#{application_id}.yml"
  end

  def menu_configuration
    if File.exist?(self.menu_path)
      if menu_yaml_cache.nil?
        menu_yaml_cache = YAML.load_file(self.menu_path)
      end
      
      menu_yaml_cache
    end
  end

  private
  def generate_application_credentials
  	self.application_id = 10.times.map{ 20 + Random.rand(11) }.join('')
  	self.application_secret = Digest::SHA1.hexdigest(Time.now.to_s)
  end
end
