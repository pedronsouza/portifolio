# -*- encoding : utf-8 -*-
class FoodType < ActiveRecord::Base
	default_scope lambda { where(locale: I18n.locale) }
	belongs_to :company
	has_many :foods, dependent: :delete_all
  attr_accessible :name, :locale, :position

  validates :name, presence: true
  validates :locale, presence: true
  validates :company, presence: true
  scope :by_locale, where(locale: I18n.locale)
end
