class OrderItem < ActiveRecord::Base
  belongs_to :food
  belongs_to :order

  attr_accessible :observations, :food_id, :amount

  validates :food, 	presence: true
  validates :order, presence: true
  validates :amount, presence: true
end
