class FoodImage < ActiveRecord::Base
  belongs_to :food
  has_attached_file :plate
  attr_accessible :plate

  def plate_image_url
  	self.plate.url
  end

  def as_json(*options)
  	super({:except => [:id, :food_id, :plate_updated_at, :updated_at], methods: [:plate_image_url]})
  end
end
