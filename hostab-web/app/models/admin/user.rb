class Admin::User < ActiveRecord::Base
	
	belongs_to :company
  attr_accessible :email, :name, :password, :salt, :company_id

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true, format: /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i
  validates :password, presence: true
  validates :salt, presence: true
  validates :company, presence: true

  before_create :encrypt_password
  def self.table_name 
    'admin_users'
  end
  def self.authenticate(email, password)
  	@user = Admin::User.find_by_email(email)
  	if @user
  		@user = Admin::User.find(:first, conditions: { email: email, password: Digest::SHA1.hexdigest(password + @user.salt) })
  	end
  	return @user
  end

  private
  def encrypt_password
  	self.salt = Digest::SHA1.hexdigest(Time.now.to_s)
  	self.password = Digest::SHA1.hexdigest(self.password + self.salt)
  end

end
