# -*- encoding : utf-8 -*-
class AccessToken < ActiveRecord::Base
	belongs_to :company
  attr_accessible :token

  validates :token, presence: true

  after_validation :generate_token

  private
  def generate_token
  	self.token = Digest::SHA1.hexdigest(Time.now.to_s)
  end

end
