# -*- encoding : utf-8 -*-
class Order < ActiveRecord::Base
  attr_accessible :status, :food_id,  :delivery_time, :company_id, :user_id
  
  belongs_to :user
  belongs_to :company
  belongs_to :delivery_location
  
  validates :user, presence: true
  validates :status, presence: true

  has_many :items, :foreign_key => 'order_id', :class_name => "OrderItem"
  scope :unclosed, where('status != 2')

  def set_status(status)
    order_status = {requested: 0, processing: 1, delivered: 2}
  	self.status = order_status[status]
  end

  def get_status
    order_status = {requested: 0, processing: 1, delivered: 2}
    o_status = order_status.invert[self.status]
    o_status.to_s
  end

end
