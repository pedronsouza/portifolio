# -*- encoding : utf-8 -*-
class Food < ActiveRecord::Base
  belongs_to :food_type
  has_many :orders
  has_many :food_images
  attr_accessible :description, :price, :title, :position

  validates :title, presence: true
  validates :price, presence: true, numericality: { greater_than:  0.0}
  validates :description, presence: true

  def as_json(*options)
  	super({
        :include => { food_images: {:except => [:id, :food_id, :plate_updated_at, :updated_at], methods: [:plate_image_url]} } 
    })
  end
end
