class DeliveryLocation < ActiveRecord::Base
   attr_accessible :name
   belongs_to :company
   has_many :orders

   validates :name, presence: true
end
