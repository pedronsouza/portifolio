# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130806000302) do

  create_table "access_tokens", :force => true do |t|
    t.string   "token",      :null => false
    t.integer  "company_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "access_tokens", ["company_id"], :name => "index_access_tokens_on_company_id", :unique => true

  create_table "admin_users", :force => true do |t|
    t.integer  "company_id"
    t.string   "email",      :null => false
    t.string   "password",   :null => false
    t.string   "salt",       :null => false
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.string   "application_id",                         :null => false
    t.string   "application_secret",                     :null => false
    t.integer  "weather_id",         :default => 455827, :null => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  create_table "delivery_locations", :force => true do |t|
    t.string   "name",       :null => false
    t.integer  "company_id", :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "food_images", :force => true do |t|
    t.integer  "food_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "plate_file_name"
    t.string   "plate_content_type"
    t.integer  "plate_file_size"
    t.datetime "plate_updated_at"
  end

  add_index "food_images", ["food_id"], :name => "index_food_images_on_food_id"

  create_table "food_types", :force => true do |t|
    t.integer  "company_id"
    t.string   "name",                            :null => false
    t.string   "locale",     :default => "pt-BR", :null => false
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "position",   :default => 0
  end

  create_table "foods", :force => true do |t|
    t.integer  "food_type_id",                      :null => false
    t.string   "title",                             :null => false
    t.string   "description",                       :null => false
    t.float    "price",                             :null => false
    t.string   "locale",       :default => "pt-BR"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "position",     :default => 0
  end

  add_index "foods", ["food_type_id"], :name => "index_foods_on_food_type_id"

  create_table "order_items", :force => true do |t|
    t.integer  "food_id",                     :null => false
    t.integer  "order_id",                    :null => false
    t.string   "observations"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.integer  "amount",       :default => 1
  end

  add_index "order_items", ["food_id"], :name => "index_order_items_on_food_id"
  add_index "order_items", ["order_id"], :name => "index_order_items_on_order_id"

  create_table "orders", :force => true do |t|
    t.integer  "user_id",                                          :null => false
    t.integer  "status",        :default => 0,                     :null => false
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.datetime "delivery_time", :default => '2013-11-24 16:23:10', :null => false
    t.integer  "company_id"
  end

  add_index "orders", ["user_id"], :name => "index_orders_on_user_id"

  create_table "users", :force => true do |t|
    t.integer  "company_id",                           :null => false
    t.string   "name",                                 :null => false
    t.string   "room",                                 :null => false
    t.string   "password",                             :null => false
    t.string   "selected_locale", :default => "pt-BR", :null => false
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.datetime "checkin_at"
    t.datetime "checkout_at"
  end

  add_index "users", ["company_id"], :name => "index_users_on_company_id"

end
