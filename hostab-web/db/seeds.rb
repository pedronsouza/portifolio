# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Criando a Compania
@company = Company.create(name: "Hotel Leleklek")
@company.save
puts "--> Criando a empresa #{@company.name}"
@company.access_tokens.create.save
puts "----> Access Token para a empresa #{@company.name} é: #{@company.access_tokens.first.token}"

# Criando um usuário para utilizar a api
@company.users.create(name: 'Pedro Nicholas de Souza', room: "666", selected_locale: "pt-BR")
puts "--> Criando o usuário #{@company.users.first.name}"

# Categorias de Alimentos
puts "--> Criando as categorias de alimentos"
["Lanches", "Pratos Quentes", "Sobremesas"].each do |group|
	@company.food_types.create(name: group, locale: 'pt-BR').save
	puts "-----> Categoria #{group} criada com sucesso"
end

["Snacks", "Dinner", "Desserts"].each do |group|
	@company.food_types.create(name: group, locale: 'en').save
	puts "-----> Categoria #{group} criada com sucesso"
end


puts "--> Adicionando alimentos a categoria -> Lanches"
@food_type = FoodType.find(:first, conditions: { name: "Lanches" })
@food_type.foods.create(
	description: "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equalização das condições financeiras e administrativas exigidas. Pensando mais a longo prazo",
	price: 29.99,
	title: "Hamburgão LeLek"
).save


puts "-----> Adicionando alimento Hamburgão LeLek à categoria #{@food_type.name}"
puts "--> Adicionando alimentos a categoria -> Lanches"

@food_type = FoodType.find(:first, conditions: { name: "Pratos Quentes" })
@food_type.foods.create(
	description: "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equalização das condições financeiras e administrativas exigidas. Pensando mais a longo prazo",
	price: 29.99,
	title: "Macarronada AHHH"
).save

puts "-----> Adicionando alimento Hamburgão LeLek à categoria #{@food_type.name}"
puts "--> Adicionando alimentos a categoria -> Lanches"

@food_type = FoodType.find(:first, conditions: { name: "Sobremesas" })
@food_type.foods.create(
	description: "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equalização das condições financeiras e administrativas exigidas. Pensando mais a longo prazo",
	price: 29.99,
	title: "Shernobyl Cola"
).save

puts "-----> Adicionando alimento Hamburgão LeLek à categoria #{@food_type.name}"
puts "--> Adicionando alimentos a categoria -> Lanches"


@food_type = FoodType.find(:first, conditions: { name: "Snacks" })
@food_type.foods.create(
	description: "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equalização das condições financeiras e administrativas exigidas. Pensando mais a longo prazo",
	price: 29.99,
	title: "Shernobyl Cola"
).save

puts "-----> Adicionando alimento Hamburgão LeLek à categoria #{@food_type.name}"
puts "--> Adicionando alimentos a categoria -> Lanches"

@food_type = FoodType.find(:first, conditions: { name: "Dinner" })
@food_type.foods.create(
	description: "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equalização das condições financeiras e administrativas exigidas. Pensando mais a longo prazo",
	price: 29.99,
	title: "Macaroni AHHH"
).save

puts "-----> Adicionando alimento Hamburgão LeLek à categoria #{@food_type.name}"
puts "--> Adicionando alimentos a categoria -> Lanches"

@food_type = FoodType.find(:first, conditions: { name: "Desserts" })
@food_type.foods.create(
	description: "Caros amigos, a complexidade dos estudos efetuados desafia a capacidade de equalização das condições financeiras e administrativas exigidas. Pensando mais a longo prazo",
	price: 29.99,
	title: "Sernobyl Cola"
).save

puts "-----> Adicionando alimento Hamburgão LeLek à categoria #{@food_type.name}"
puts "--> Adicionando alimentos a categoria -> Lanches"


# Criar usuario para o admin
puts "--> Criando usuario do admin"
@admin_user = Admin::User.create(name: 'Administrador', email: 'admin@example.com', password: 'password', company_id: @company.id, salt: Time.now.to_s)
@admin_user.save