class CreateDeliveryLocations < ActiveRecord::Migration
  def change
    create_table :delivery_locations do |t|
      t.string :name, length: 50, null: false
      t.references :company, null: false
      t.timestamps
    end
  end
end
