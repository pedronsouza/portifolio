# -*- encoding : utf-8 -*-
class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.belongs_to :company, null: false
      t.string :name, null: false, length: 100
      t.string :room, null: false, length: 100
      t.string :password, length: 40, null: false
      t.string :selected_locale, length: 10, null: false, default: 'pt-BR'

      t.timestamps
    end
    add_index :users, :company_id
  end
end
