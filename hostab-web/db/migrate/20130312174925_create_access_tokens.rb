# -*- encoding : utf-8 -*-
class CreateAccessTokens < ActiveRecord::Migration
  def change
    create_table :access_tokens do |t|
      t.string :token, null: false, length: 40
      t.belongs_to :company
      t.timestamps
    end

    add_index(:access_tokens, :company_id, :unique => true)
  end
end
