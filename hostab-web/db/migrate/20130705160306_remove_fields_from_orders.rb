class RemoveFieldsFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :delivery_location
  end
end
