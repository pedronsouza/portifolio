class AddCheckinCheckoutFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :checkin_at, :datetime
    add_column :users, :checkout_at, :datetime
  end
end
