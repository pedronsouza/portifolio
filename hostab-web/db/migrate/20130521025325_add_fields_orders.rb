class AddFieldsOrders < ActiveRecord::Migration
 def change
 	add_column :orders, :delivery_time, :datetime, null: false, default: (Time.now + 30.minutes)
 	add_column :orders, :delivery_location, :string, null: false, default: (Time.now + 30.minutes)
 end
end
