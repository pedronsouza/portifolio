class CreateAdminUsers < ActiveRecord::Migration
  def change
    create_table :admin_users do |t|
    	t.belongs_to :company
      t.string :email, null: false, size:200
      t.string :password, null: false, size: 40
      t.string :salt, null: false, size: 40
      t.string :name, null: false, size: 100

      t.timestamps
    end
  end
end
