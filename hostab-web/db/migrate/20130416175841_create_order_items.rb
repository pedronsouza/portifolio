class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.belongs_to :food, null: false
      t.belongs_to :order, null: false
      t.string :observations, length: 200

      t.timestamps
    end
    add_index :order_items, :food_id
    add_index :order_items, :order_id
  end
end
