# -*- encoding : utf-8 -*-
class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name, length: 200
      t.string :application_id, length: 50, null: false
      t.string :application_secret, length: 40, null: false
      t.integer :weather_id, null: false, default: 455827

      t.timestamps
    end
  end
end
