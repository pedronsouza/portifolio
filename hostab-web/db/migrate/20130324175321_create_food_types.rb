# -*- encoding : utf-8 -*-
class CreateFoodTypes < ActiveRecord::Migration
  def change
    create_table :food_types do |t|
    	t.belongs_to :company
      t.string :name, null: false
      t.string :locale, null: false, default: 'pt-BR'

      t.timestamps
    end
  end
end
