class CreateFoodImages < ActiveRecord::Migration
  def change
    create_table :food_images do |t|
      t.belongs_to :food
      t.timestamps
    end

    add_attachment :food_images, :plate
    add_index :food_images, :food_id
  end
end
