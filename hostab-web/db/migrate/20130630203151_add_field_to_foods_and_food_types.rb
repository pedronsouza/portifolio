class AddFieldToFoodsAndFoodTypes < ActiveRecord::Migration
  def change
    add_column :foods, :position, :integer, default: 0
    add_column :food_types, :position, :integer, default: 0
  end
end
