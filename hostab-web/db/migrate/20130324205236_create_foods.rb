# -*- encoding : utf-8 -*-
class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.belongs_to :food_type, null: false
      t.string :title, null: false, length: 200
      t.string :description, null: false, length: 100
      t.float :price, null: false
      t.string :locale, default: 'pt-BR'

      t.timestamps
    end
    add_index :foods, :food_type_id
  end
end
