class AddAmountToOrderItens < ActiveRecord::Migration
  def change
    add_column :order_items, :amount, :integer, default: 1
  end
end
