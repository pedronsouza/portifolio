# -*- encoding : utf-8 -*-
class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :user, null: false
      t.integer :status, null: false, default: 0

      t.timestamps
    end
    add_index :orders, :user_id
  end
end
