# -*- encoding : utf-8 -*-
HostabWeb::Application.routes.draw do
  apipie

  namespace :api do
    namespace :v1 do
      resources :delivery_locations
    	scope "/users/:user_id" do
    		resources :orders, only: [:create, :update, :show, :index]
    	end

      scope "/orders/:order_id" do
        resources :order_items, only: [:create, :index, :destroy, :show, :update]
      end

      resources :auth, only: [:create]
      resources :food_types, only: [:index] do
        resources :foods, only: [:index, :show]
      end
    end
  end
  namespace :kitchen do
    get   '/sign_in',   to: 'sessions#new'
    post  '/sign_in',   to: 'sessions#create'
    get   '/sign_out',  to: 'sessions#destroy'
    get   '/polling_orders', to: 'home#polling_orders'
    put '/set_as_ready/:order_id', to: 'home#set_as_ready', as: 'kitchen_ready'

    resources :home, only: [:index, :destroy]
  end
  
  namespace :admin do
    get   '/sign_in',   to: 'sessions#new'
    post  '/sign_in',   to: 'sessions#create'
    get   '/sign_out',  to: 'sessions#destroy'

    resources :home, only: [:index]
    resources :delivery_locations
    resources :menus
    resources :food_types do
      resources :foods
    end

    scope 'food/:id/' do
      post '/image',    to: 'foods#add_image'
      delete '/image/:image_id',  to: 'foods#remove_image', as: 'remove_image'
    end

    resources :promotions, only: [:index]
    resources :room_services, only: [:index]
    resources :users, only: [:edit, :update]

    get '/guests/search', to: 'guests#search'
    resources :guests

    root to: "home#index"
  end
  

  root to: "apipie/apipies#index"
end
