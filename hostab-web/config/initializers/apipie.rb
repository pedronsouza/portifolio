# -*- encoding : utf-8 -*-
Apipie.configure do |config|
  config.app_name                = "Hostab API"
  config.api_base_url            = "/api/v1"
  config.doc_base_url            = "/apipie"
  config.validate								 = false
  # were is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/api/v1/*.rb"
end
