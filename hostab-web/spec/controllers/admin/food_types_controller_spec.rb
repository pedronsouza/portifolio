require 'spec_helper'

describe Admin::FoodTypesController do
  before(:each) { controller.stub restrict_access: true }
  describe '#index' do
    before(:each) { get :index }
    it { response.should be_success }
    it { response.should render_template('admin/food_types/index') }
    it { assigns(:food_types).should eql FoodType.all }
  end

  describe '#new' do
    before(:each) { get :new }
    it { response.should be_success }
    it { response.should render_template('admin/food_types/new') }
    it { assigns(:food_type).should_not be_nil }
  end

  describe '#create' do
    before(:each) do
      controller.stub current_user: FactoryGirl.create(:user)
      post :create, food_type: { name: 'Test FoodType', locale: 'en' }
    end

    it { response.should be_redirect }
    it { assigns(:food_type).should be_valid }

    context 'when data is invalid' do
      before(:each) { post :create, food_type: { name: nil } }
      it { response.should render_template('new') }
      it { assigns(:food_type).should_not be_valid }
    end
  end

  describe '#edit' do
    before(:each) do
      @current_food_type = FactoryGirl.create(:food_type)
      get :edit, id: @current_food_type.id, food_type: { name: 'Edited FoodType', locale: 'en' }
    end

    it { response.should be_success }
    it { response.should render_template('edit') }
    it { assigns(:food_type).should eql @current_food_type }
  end

  describe '#update' do
    before(:each) do
      @current_food_type = FactoryGirl.create(:food_type)
      p @current_food_type.save
      put :update, id: @current_food_type.id, food_type: { name: 'Edited food_type', locale: 'en' }
    end

    it { response.should be_redirect }
    it { assigns(:food_type).should eql @current_food_type }

    context 'when is invalid' do
      before(:each) { put :update, id: @current_food_type.id, food_type: { name: nil, locale: 'en' } }
      it { response.should render_template('edit') }
      it { assigns(:food_type).should_not be_valid }
    end
  end

  describe '#destroy' do
    before(:each) do
      @current_food_type = FactoryGirl.create(:food_type)
      delete :destroy, id: @current_food_type.id
    end

    it { response.should be_redirect }
  end
end
