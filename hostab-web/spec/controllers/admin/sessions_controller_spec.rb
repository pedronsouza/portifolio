require 'spec_helper'

describe Admin::SessionsController do
	before(:each) do
		controller.stub(:restrict_access => true)
	end

  describe "GET 'new'" do
    it "Should be sucess" do
      get 'new'
      response.should be_success
    end

    it "Should render new.html template" do
    	get 'new'
    	response.should render_template("new")
    end
  end

  describe "POST 'create'" do
  	it "Should redirect to login page if credentials are invalid" do
  		post 'create', email: 'invalid', password: 'invalid'
  		response.should be_redirect
  	end

  	it "Should flash a message if credentials are invalid" do
  		post 'create', email: 'invalid', password: 'invalid'
  		flash[:notice].should eql 'Email ou Senha invalidos'
  	end

  	it "Should set the session admin_user_id" do
  		@user = FactoryGirl.create(:admin_user, password: '123123')

  		post 'create', email: @user.email, password: '123123'
  		request.session[:admin_user_id].should eql @user.id
  	end
  end

  describe "GET 'destroy'" do
  	before(:each) do
  		request.session[:admin_user_id] = 1
  	end

  	it 'Should remove the session of user' do
  		get 'destroy'
  		request.session[:admin_user_id].should be_nil
  	end

  	it 'Should redirect the user to the login page' do
  		get 'destroy'
  		response.should be_redirect
  	end
  end

end
