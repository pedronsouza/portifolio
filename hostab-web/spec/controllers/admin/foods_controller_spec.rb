require 'spec_helper'

describe Admin::FoodsController do
	before(:each) do
		controller.stub(:restrict_access => true)
		@food_type = FactoryGirl.create(:food_type)
	end

	describe "GET 'index'" do
		it "Should be success" do
			get 'index', food_type_id: @food_type.id
			response.should be_success
		end

		it "Should render the index template" do
			get 'index', food_type_id: @food_type.id
			response.should render_template('admin/foods/index')
		end
	end

	describe "GET 'new'" do
		it "Should be success" do
			get 'new', food_type_id: @food_type.id
			response.should be_success
		end

		it "Should render the edit template" do
			get 'new', food_type_id: @food_type.id
			response.should render_template('admin/foods/new')
		end
	end

	describe "POST 'create'" do
		before(:each) do
			@food = FactoryGirl.build(:food)
		end

		it "If successfull, should redirect to food list" do
			post 'create', food_type_id: @food_type.id, food: { title: @food.title, description: @food.description, price: @food.price }
			response.should be_redirect
		end

		it "If has any problem, should redirect to new food view, with a notice message" do
			post 'create', food_type_id: @food_type.id, food: Hash.new
			response.should be_redirect
			flash[:notice].should_not be_nil
		end
	end

	describe "GET 'edit'" do
		before(:each) do
			@food = FactoryGirl.create(:food, food_type: @food_type) 
		end

		it "Should be success" do
			get 'edit', food_type_id: @food_type.id, id: @food.id
			response.should be_success
		end

		it "should render the edit template" do
			get 'edit', food_type_id: @food_type.id, id: @food.id
			response.should render_template('admin/foods/edit')
		end
	end

	describe "PUT 'update'" do
		before(:each) do
			@food = FactoryGirl.create(:food, food_type: @food_type)
		end

		it "Should be success and redirect" do
			put 'update', food_type_id: @food.food_type_id, id: @food.id, food: { title: @food.title, description: @food.description, price: @food.price }
			response.should be_redirect
		end

		it "Should redirect with a notice if have any errors" do
			put 'update', food_type_id: @food.food_type_id, id: @food.id, food: {title: nil}
			response.should be_redirect
			flash[:notice].should_not be_nil
		end
	end

	describe "DELETE 'destroy'" do
		before(:each) do
			@food = FactoryGirl.create(:food, food_type: @food_type)
		end

		it "Should exclude food and redirect to the list" do
			delete 'destroy', food_type_id: @food_type.id, id: @food.id
			expect { Food.find(@food.id)}.to raise_error
			response.should be_redirect
		end

		it "Should raise error if try to exclude with an invalid id" do
			expect { delete 'destroy', food_type_id: @food_type.id, id: 0 }.to raise_error
		end
	end

	describe "POST 'add_image'" do
		before(:each) do
			@food = FactoryGirl.create(:food, food_type_id: @food_type)
		end

		it "Should be success and redirect" do
			@food_image = FactoryGirl.create(:food_image)
			post 'add_image', id: @food.id
			response.should be_redirect
		end
	end

	describe "DELETE 'remove_image'" do
		before(:each) do
			@food = FactoryGirl.create(:food)
			@food_image = FactoryGirl.create(:food_image, food: @food)
		end

		it "Should be success and redirect" do
			delete 'remove_image', id: @food.id, image_id: @food_image.id
			response.should be_redirect
		end
	end
end
