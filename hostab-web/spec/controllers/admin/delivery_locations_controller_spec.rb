require 'spec_helper'

describe Admin::DeliveryLocationsController do
  before(:each) do
    user = FactoryGirl.create(:admin_user)
    controller.stub(current_user: user)
    controller.stub(restrict_access: false)
  end

  describe '#index' do
    before(:each) { get :index }
    it { response.should be_success   }
    it { response.should render_template('admin/delivery_locations/index') }
  end

  describe '#new' do
    before(:each) { get :new }
    it { response.should be_success }
    it { response.should render_template('admin/delivery_locations/new') }
    it { assigns(:delivery_location).should_not be nil }
  end

  describe '#create' do
    before(:each) { post :create, delivery_location: { name: 'Test location' } }
    it { response.should be_redirect }
    it { assigns(:delivery_location).should be_valid }

    context 'if invalid data' do
      before(:each) { post :create, delivery_location: { name: nil } }

      it { response.should render_template('admin/delivery_locations/new') }
      it { assigns(:delivery_location).should_not be_valid }
    end
  end

  describe '#edit' do
    before(:each) { get :edit, id: FactoryGirl.create(:delivery_location).id }

    it { response.should be_success }
    it { response.should render_template('admin/delivery_locations/edit') }
    it { assigns(:delivery_location).id.should_not be_nil }

    context 'when invalid id is passed' do
      it { expect { get :edit, id: 0 }.to raise_error }
    end
  end

  describe '#update' do
    before(:each) { put :update, id: FactoryGirl.create(:delivery_location).id, delivery_location: { name: 'Updated Name' } }

    it { response.should be_redirect }
    it { assigns(:delivery_location).should be_valid }

    context 'if invalid data' do
      before(:each) { put :update, id: FactoryGirl.create(:delivery_location).id, delivery_location: { name: nil } }
      it { response.should render_template('admin/delivery_locations/edit') }
    end

    context 'if not existent location is given' do
      it {  expect { put :edit, id: 0, delivery_location: { name: 'Updated Name' } }.to raise_error }
    end
  end

  describe '#destroy' do
    before(:each) { delete :destroy, id: FactoryGirl.create(:delivery_location).id }
    it { response.should be_redirect }
    context 'if locationd not exists' do
      it { expect { delete :destroy, id: 0 }.to raise_error }
    end
  end
end
