require 'spec_helper'

describe Admin::GuestsController do
	before(:each) do
		@user = FactoryGirl.create(:user)
		controller.stub(:current_user => @user)
		controller.stub(:restrict_access => true)
	end

	describe "GET 'index'" do
		it "Should be success" do
			get 'index'
			response.should be_success
		end

		it "Should render the index template" do
			get 'index'
			response.should render_template('admin/guests/index')
		end
	end

	describe "GET 'new'" do
		it "Should be success" do
			get 'new'
			response.should be_success
		end

		it 'Should render the new template' do
			get 'new'
			response.should render_template('admin/guests/new')
		end
	end

	describe "POST 'create'" do
		it "Should be success and redirect to users lists" do
			post 'create', user: { name: 'Fiction User', room: '123AB', checkin_at: Time.now }
			response.should be_redirect
			flash[:notice].should be_nil
		end

		it "Should be failed, redirect to new template and show the error message" do
			post 'create', user: { name: 'Fiction User', room: nil }
			response.should be_redirect
			flash[:notice].should_not be_nil
		end
	end

	describe "GET 'edit'" do
		before(:each) do
			@user = FactoryGirl.create(:user)
		end
		it "Should be success" do
			get 'edit', id: @user.id
			response.should be_success
		end

		it "Should render the edit template" do
			get 'edit', id: @user.id
			response.should render_template('admin/guests/edit')
		end
	end



end
