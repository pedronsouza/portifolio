# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Api::V1::FoodsController do
	before(:each) do
		@access_token = FactoryGirl.create(:access_token)
		@food_type = FactoryGirl.create(:food_type, locale: I18n.locale)
		@food = FactoryGirl.create(:food, food_type: @food_type)

		controller.stub(:restrict_access => true)
	end

	describe "GET 'index'" do
		it "Should be success" do
			get 'index', food_type_id: @food.food_type.id
			response.should be_success
		end

		it "Should return a list of all foods on system" do
			get 'index', food_type_id: @food.food_type.id
			response.body.should eql Food.all.to_json
		end
	end

	describe "GET 'show' " do
		it "Should be sucess" do
			get 'show', food_type_id: @food.food_type.id, id: @food.id
			response.should be_success
		end

		it "Should return the given food" do
			get 'show', food_type_id: @food.food_type.id, id: @food.id
			response.body.should eql @food.to_json
		end
	end
end
