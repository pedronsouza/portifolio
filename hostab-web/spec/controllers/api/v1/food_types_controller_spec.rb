# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Api::V1::FoodTypesController do
	before(:each) do
		@access_token = FactoryGirl.create(:access_token)
		request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(@access_token.token)
	end

	describe "GET 'index'" do
		it "Shoud return an list with all food types on system" do
			get 'index'
			response.should be_success
			response.body.should eql FoodType.all.to_json
		end
	end	
end
