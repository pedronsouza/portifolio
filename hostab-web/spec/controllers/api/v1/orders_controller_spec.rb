# -*- encoding : utf-8 -*-
require 'spec_helper'
describe Api::V1::OrdersController do
	before(:each) do
		@user = FactoryGirl.create(:user)
		@order = FactoryGirl.create(:order, user: @user)

		controller.stub(:restrict_access => true)
		controller.stub(:current_user => @user)
	end

	describe "GET 'index'" do
		it "Should be success" do
			get 'index', user_id: @user.id
			response.should be_success
		end

		it "Should return a list or all orders of an user" do
			get 'index', user_id: @user.id
			response.body.should eql @user.orders.to_json
		end
	end

	describe "PUT 'update'" do
		before(:each) do
			@order = FactoryGirl.create(:order)
		end
		it 'should be success' do
			put 'update', user_id: @user.id, id: @order.id, order: { delivery_time: Time.now }
			response.should be_success
		end
	end	

	describe "POST 'create'" do
		context "Success" do
			before(:each) do
				@food = FactoryGirl.create(:food)
				@do_action = lambda { post 'create', user_id: @user.id }
			end

			it "Should create an order" do
				@do_action
				response.should be_success
			end

			it "Should return the created order" do
				@do_action
				response.body.should_not be_nil 
			end
		end
	end
end
