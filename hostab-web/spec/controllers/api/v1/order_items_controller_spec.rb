require 'spec_helper'

describe Api::V1::OrderItemsController do
	before(:each) do
		@order = FactoryGirl.create(:order)
		@order_item = FactoryGirl.create(:order_item, order: @order)
		controller.stub(:restrict_access => true)
	end

	describe "GET 'index'" do
		it "Should be successful" do
			get 'index', order_id: @order.id
			response.should be_success
		end

		it "Should return an list with all itens on an order" do
			@order_item = FactoryGirl.create(:order_item, order: @order)
			
			get 'index', order_id: @order.id
			response.body.should eql @order.items.to_json
		end
	end

	describe "GET 'show'" do
		it "Should be successful" do
			get 'show', order_id: @order.id, id: @order_item.id
			response.should be_success
		end

		it "Should return status 404 if the item doesn't exist" do
			get 'show', order_id: @order.id, id: 0
			response.response_code.should eql 404
		end

		it "Should return the given item as json" do
			get 'show', order_id: @order.id, id: @order_item.id
			response.body.should eql @order_item.to_json
		end
	end

	describe "POST 'create'" do
		before(:each) do
			@food = FactoryGirl.create(:food)
		end

		it "Should be successful" do
			post 'create', order_id: @order.id, item: { food_id: @food.id, observations: nil, amount: 1 }
			response.should be_success
		end

		it "Should return status 500 with the validation errors if has anyone" do
			post 'create', order_id: @order.id, item: nil
			response.response_code.should eql 500
			response.body.should eql({error: @order.items.create.errors}.to_json)
		end

		it "Should create a new item for order and return the item details" do
			post 'create', order_id: @order.id, item: { food_id: @food.id, observations: '...' }
			response.should be_success
			response.body.should eql @order.items.last.to_json
		end
	end

	describe "PUT 'update'" do
		before(:each) do
			@food = FactoryGirl.create(:food)
		end
		
		it "Should be successful" do
			put 'update', order_id: @order.id, id: @order_item.id, item: { food_id: @food.id }
			response.should be_success
		end
	end
end
