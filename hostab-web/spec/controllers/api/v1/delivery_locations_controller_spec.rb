require 'spec_helper'

describe Api::V1::DeliveryLocationsController do
  before(:each)  do
    controller.stub(restrict_access: true)
    controller.stub(current_company: FactoryGirl.create(:company))
  end

  describe '#index' do
    before(:each) { get :index }
    it { response.should be_success }
    it { response.body.should eql DeliveryLocation.all.to_json }
  end
end
