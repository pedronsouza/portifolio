# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Api::V1::AuthController do
	before(:each) do
		@company = FactoryGirl.create(:company)
		@user = FactoryGirl.create(:user, company: @company)

		controller.stub(restrict_access: true)
		controller.stub(current_company: @company)
	end
	
	describe "POST 'create'" do
		it "Should return an user if password is valid" do
			post 'create', user: { password: @user.password}
			response.should be_success
		end

		it "Should return user as a json if password is valid" do
			post 'create', user: { password: @user.password}
			response.body.should eql @user.to_json
		end

		it "Should return error 404 if was unable to find user with given password" do
			post 'create', user: { }
			response.response_code.should eql 404
		end
	end
end
