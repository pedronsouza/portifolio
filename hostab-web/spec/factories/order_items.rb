# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order_item do
    association :food_id, factory: :food
    association :order_id, factory: :order
    amount 1
    observations "MyString"
  end
end
