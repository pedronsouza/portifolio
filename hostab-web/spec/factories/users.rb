# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    association :company_id, factory: :company
    name "MyString"
    password 'xDxD'
    room "MyString"
    selected_locale "MyString"
    checkin_at Time.now
    checkout_at Time.now
  end
end
