# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :admin_user, :class => 'Admin::User' do
  	association :company_id, factory: :company
    sequence(:email) { |n| "pedro#{n}@email.com" }
    password "123123"
    salt "salt"
    name "Pedro Nicholas de Souza"
  end
end
