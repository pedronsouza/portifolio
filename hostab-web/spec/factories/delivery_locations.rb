# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :delivery_location do
    association :company_id, factory: :company
    name 'Yooow'
  end
end
