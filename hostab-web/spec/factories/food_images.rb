# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :food_image do
    association :food_id, factory: :food
  end
end
