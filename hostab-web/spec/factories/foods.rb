# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :food do
    association :food_type, factory: :food_type
    title "MyString"
    description "MyString"
    price 1.5
  end
end
