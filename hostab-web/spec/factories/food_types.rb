# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :food_type do
  	association :company_id, factory: :company
    name "MyString"
    locale "xD"
  end
end
