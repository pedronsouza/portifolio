# -*- encoding : utf-8 -*-
require 'spec_helper'

describe User do
	before(:each) do
		@user = FactoryGirl.build(:user)
	end

  context "Validations" do
  	it "Should have a valid factory" do
  		@user.should be_valid
  	end

    it { FactoryGirl.build(:user, checkin_at: nil).should_not be_valid }

  	it "Should be invalid if try to save without name" do
  		@user.name = nil
  		@user.should_not be_valid
  	end

  	it "Should be invalid if try to save without room" do
  		@user.room = nil
  		@user.should_not be_valid
  	end

  	it "Should be invalid if try to save without company" do
  		@user.company = nil
  		@user.should_not be_valid
  	end
  end

  it "Should generate an password for user, so them he can connect to the app" do
  	old_password = "xD" 
		@user.password = old_password

		@user.save
		@user.password.should_not eql old_password
  end

  it 'should format date to the brazilian style' do
    @user = FactoryGirl.create(:user)
    @user.save
    @user.checkin_at.strftime('%d/%m/%Y %H:%M').should eql @user.formated_checkin_date
  end
end
