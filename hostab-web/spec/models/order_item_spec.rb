require 'spec_helper'

describe OrderItem do
  context "Validations" do
  	before(:each) do
  		@order_item = FactoryGirl.build(:order_item)
  	end

  	it "Should be a valid factory" do
  		@order_item.should be_valid
  	end

  	it "Should be invalid if try to save without any food associated" do
  		@order_item.food = nil
  		@order_item.should_not be_valid
  	end

  	it "Should be invalid if try to save without any order associated" do
  		@order_item.order = nil
  		@order_item.should_not be_valid
  	end
  	
    it 'Should be invalid if try to save without any amount' do 
      @order_item.amount = nil
      @order_item.should_not be_valid
    end
  end
end
