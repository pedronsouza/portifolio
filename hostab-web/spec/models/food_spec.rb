# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Food do
  before(:each) do
  	@food = FactoryGirl.build(:food)
  end

  context 'Validations' do
  	it "Should have a valid factory" do
  		@food.should be_valid
  	end

  	it "Should be invalid without title" do
  		@food.title = nil
  		@food.should_not be_valid
  	end

  	it "Should be invalid without price" do
  		@food.price = nil
  		@food.should_not be_valid
  	end

  	it "Should be invalid without description" do
  		@food.description = nil
  		@food.should_not be_valid
  	end

  	it "Should have a price higher than zero" do
  		@food.price = 0.0
  		@food.should_not be_valid
  	end
  end
end
