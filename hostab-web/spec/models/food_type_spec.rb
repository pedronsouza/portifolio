# -*- encoding : utf-8 -*-
require 'spec_helper'

describe FoodType do
	before(:each) do
		@food_type = FactoryGirl.build(:food_type)
	end

	context "Validations" do
		it "Should have a valid factory" do
			@food_type.should be_valid
		end

		it "Should be invalid without name" do
			@food_type.name = nil
			@food_type.should_not be_valid
		end

		it "Should be invalid without locale" do
			@food_type.locale = nil
			@food_type.should_not be_valid
		end

		it "Should be invalid without company" do
			@food_type.company = nil
			@food_type.should_not be_valid
		end
	end  
end
