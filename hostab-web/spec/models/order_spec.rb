# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Order do
	before(:each) do
		@order = FactoryGirl.create(:order)
	end

  context "Validations" do
  	it "Should has a valid factory" do
  		@order.should be_valid
  	end

  	it "Should be invalid if has no user associated" do
  		@order.user = nil
  		@order.should_not be_valid
  	end

  	it "Should be invalid if has no status" do
  		@order.status = nil
  		@order.should_not be_valid
  	end
  end

  context "Status Declaration" do
  	it "Setting status as :requested should return index 0" do
  		@order.set_status(:requested)
  		@order.status.should eql 0
  	end

  	it "Setting status as :processing should return index 1" do
  		@order.set_status(:processing)
  		@order.status.should eql 1
  	end

  	it "Setting status as :delivered should return index 2" do
  		@order.set_status(:delivered)
  		@order.status.should eql 2
  	end
  end
end
