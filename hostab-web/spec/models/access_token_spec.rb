# -*- encoding : utf-8 -*-
require 'spec_helper'

describe AccessToken do
  context "Validations" do
  	it "Should have a valid factory" do
  		@factory = FactoryGirl.create(:access_token)
  		@factory.should be_valid
  	end

  	it "Should not be valid if the token property is nil" do
  		@factory = FactoryGirl.build(:access_token, token: nil)
  		@factory.should_not be_valid
  	end
  end

  it "Should generate a token before save on database" do
  	old_value = "OldValue"
  	@factory = FactoryGirl.build(:access_token, token: old_value)
  	@factory.save
  	
  	@factory.token.should_not eql old_value
  end
end
