require 'spec_helper'

describe Admin::User do
  context "Validations" do
  	before(:each) do
  		@user = FactoryGirl.build(:admin_user)
  	end

  	it "Should be a valid factory" do
  		@user.should be_valid
  	end

  	it "Should be invalid if try to save without name" do
  		@user.name = nil
  		@user.should_not be_valid
  	end

  	context 'Email' do
	  	it "Should be invalid if try to save without email" do
	  		@user.email = nil
	  		@user.should_not be_valid
	  	end

	  	it "Should be invalid if try to save a duplicate email" do
	  		@user.email = 'same@mail.com'
	  		@user.save

	  		FactoryGirl.build(:admin_user, email: 'same@mail.com').should_not be_valid
	  	end

	  	it "Should be invalid if try to save with wrong formated email" do
	  		FactoryGirl.build(:admin_user, email:'wront@format').should_not be_valid
	  	end	
  	end

  	it "Should be invalid if try to save without password" do
  		@user.password = nil
  		@user.should_not be_valid
  	end

  	it "Should be invalid if try to save without salt" do
  		@user.salt = nil
  		@user.should_not be_valid
  	end

  	it "Should be invalid if has no company associated" do
  		@user.company = nil
  		@user.should_not be_valid
  	end
  end

  context "Password encryptation" do
  	before(:each) do
  		@user = FactoryGirl.build(:admin_user)
  	end

  	it "Should generate an password salt before save" do
  		old_salt = @user.salt
  		@user.save
  		@user.salt.should_not eql old_salt
  	end

  	it "Should encrypt the password with generated salt before_save" do
  		old_password = @user.password
  		@user.save
  		@user.password.should_not eql old_password
  	end
  end

  context "User authentication" do
  	before(:each) do
  		@user = FactoryGirl.create(:admin_user, password: '123123')
  	end

  	it 'Should return an user if the credentials are valid' do
  		@authenticated_user = Admin::User.authenticate(@user.email, '123123')
  		@authenticated_user.should_not be_nil
  	end
  end

end
