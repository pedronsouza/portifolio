# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Company do
  context "Validations" do
  	it "Should has an valid factory" do
  		@factory = FactoryGirl.create(:company)
  		@factory.should be_valid
  	end

  	it "Should give an error if try to save without an application_id" do
  		@factory = FactoryGirl.build(:company, application_id: nil)
  		@factory.should_not be_valid
  	end

  	it "Should give an error if try to save without an application_secret" do
  		@factory = FactoryGirl.build(:company, application_secret: nil)
  		@factory.should_not be_valid
  	end

  	it "Should give an error if try to save a name" do
  		@factory = FactoryGirl.build(:company, name: nil)
  		@factory.should_not be_valid
  	end
  end

  it "Should generate a random application_id on model creation" do
  	old_value = "OldValue"
  	@factory = FactoryGirl.build(:company, application_id: old_value)
  	@factory.save

  	@factory.application_id.should_not eql old_value
  end

  it "Should generate a random application_secret on model creation" do
  	old_value = "OldValue"
  	@factory = FactoryGirl.build(:company, application_secret: old_value)
  	@factory.save

  	@factory.application_secret.should_not eql old_value
  end
end
