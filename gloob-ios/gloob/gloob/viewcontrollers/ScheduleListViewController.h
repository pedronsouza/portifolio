//
//  ScheduleListViewController.h
//  gloob
//
//  Created by zeroum on 04/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"

@interface ScheduleListViewController : GloobViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *listOfExibition;

@property (nonatomic, assign) id delegate;

@end

@protocol ScheduleListViewControllerDelegate <NSObject>

- (void)dismissScheduleList;

@end
