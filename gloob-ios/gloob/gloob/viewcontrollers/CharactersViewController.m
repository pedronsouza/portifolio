//
//  CharactersViewController.m
//  gloob
//
//  Created by zeroum on 08/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "CharactersViewController.h"
#import "ProgramManager.h"
#import "Program.h"
#import "ParentViewController.h"

@interface CharactersViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrViewCharacters;
@property (weak, nonatomic) IBOutlet UIView *viewCharactersList;

- (void)setupInterface;

- (void)loadCharactersListIpad;
- (void)loadCharactersListIphone;

@end

@implementation CharactersViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    [self setupInterface];
}

- (void)dealloc {
}

#pragma mark - Private

- (void)setupInterface {
    
    [self.viewCharactersList setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ps_bg"]]];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {

        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:32.0f]];
        
        [self loadCharactersListIpad];
        
    } else {
        
        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:26.0f]];
        
        [self loadCharactersListIphone];
    }
}

- (void)loadCharactersListIpad {
    
    CGFloat x = 115.0f;
    CGFloat y = 50.0f;
    CGFloat width = 140.0f;
    CGFloat height = 180.0f;
    NSInteger i = 0;
    
    for (Program *program in [[ProgramManager sharedManager] objects]) {
        
        if (i % 5 == 0 && i > 0) {
            x = 115.0f;
            y += height;
        } else if (i > 0) {
            x += (width + 20.0f);
        }
        
        CGRect frame = CGRectMake(x, y, width, height);
        
        CharacterView *charView = [[CharacterView alloc] initWithFrame:frame
                                                               program:program
                                                              delegate:self];
        
        [self.viewCharactersList addSubview:charView];
        
        i++;
    }
    
    [self.scrViewCharacters setContentSize:CGSizeMake(self.view.frame.size.width,
                                                      y + height + 45.0f)];
    
    [self.viewCharactersList setFrame:CGRectMake(self.viewCharactersList.frame.origin.x,
                                                 self.viewCharactersList.frame.origin.y,
                                                 self.view.frame.size.width,
                                                 y + height)];
}

- (void)loadCharactersListIphone {
    
    CGFloat x = 80.0f;
    CGFloat y = 20.0f;
    CGFloat width = 77.0f;
    CGFloat height = 107.0f;
    NSInteger i = 0;
    NSInteger numberOfColumns = 3;
    
    if ([UIHelper isIPhone5]) {
        numberOfColumns = 4;
    }
    
    for (Program *program in [[ProgramManager sharedManager] objects]) {
        
        if (i % numberOfColumns == 0 && i > 0) {
            x = 80.0f;
            y += height;
        } else if (i > 0) {
            x += (width + 30.0f);
        }
        
        CGRect frame = CGRectMake(x, y, width, height);
        
        CharacterView *charView = [[CharacterView alloc] initWithFrame:frame
                                                               program:program
                                                              delegate:self];
        
        [self.viewCharactersList addSubview:charView];
        
        i++;
    }
    
    [self.scrViewCharacters setContentSize:CGSizeMake(self.view.frame.size.width,
                                                      y + height + 55.0f)];
    
    [self.viewCharactersList setFrame:CGRectMake(self.viewCharactersList.frame.origin.x,
                                                 self.viewCharactersList.frame.origin.y,
                                                 self.view.frame.size.width + 88.0f,
                                                 y + height)];
}

#pragma mark - CharacterView delegate

- (void)displayCharacterInPopUp:(Program *)character {
    
    ParentViewController *controller = (ParentViewController *)self.parentViewController;
    
    [controller displaySelectedCharacterPopUp:character];
    NSString *label = [NSString stringWithFormat:@"personagens|%@", [character.title slugalize]];
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Personagem" andLabel:label];
}

#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return [NSString stringWithFormat:@"/personagens"];
}
@end
