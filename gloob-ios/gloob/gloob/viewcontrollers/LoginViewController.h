//
//  LoginViewController.h
//  gloob
//
//  Created by zeroum on 16/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAHelper.h"
#import "GloobViewController.h"

@class Episode;

@interface LoginViewController : GloobViewController <UIWebViewDelegate, UIAlertViewDelegate, GATrackableScreen>

@property (nonatomic, assign) BOOL willPlayNow;

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) Episode *episode;

@property (nonatomic, strong) NSMutableArray *arrayVideosSource;
@property (nonatomic, assign) NSInteger videoAtIndex;
@property (nonatomic, strong) NSString *openingFrom;
@property (nonatomic, copy) void (^afterCloseBlock)();
@end

@protocol LoginViewControllerDelegate <NSObject>

@optional

- (void)playEpisode:(Episode *)episode;
- (void)playEpisode:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from;

@end
