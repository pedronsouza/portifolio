//
//  SelfiesListViewController.m
//  gloob
//
//  Created by Pedro on 3/27/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SelfiesListViewController.h"
#import "Selfie.h"
#import "ParentViewController.h"
#import "AchievementsFactory.h"

@interface SelfiesListViewController ()
@property (nonatomic, strong) NSArray *selfies;
- (void) updateInterfaceByNotification:(NSNotification *) notification;
@end

@implementation SelfiesListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andSelfies:(NSArray *)selfies {
    
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
        self.selfies = selfies;
        if (selfies.count > 6) {
            [NSException raise:@"Selfies set must have a maximum of 6 selfies" format:nil];
            
        }
    }
    
    return self;
}

- (instancetype)initWithSelfies:(NSArray *)selfies {
    self = [super initWithNibName:[UIHelper deviceViewName:@"SelfiesListView"] bundle:nil];
    
    if (self) {
        self.selfies = selfies;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            if (selfies.count > 6) {
                [NSException raise:@"Selfies set must have a maximum of 6 selfies" format:nil];
                
            }
        }
    }
    
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInterfaceByNotification:) name:kAvatarChange object:nil];
    [self setupInterface];
}

- (void) setupInterface {
    NSInteger interatorSize = self.selfies.count;
    
    for (int i = 0; i < interatorSize; i++) {
        UIView *selfieHolder = [self.view viewWithTag:(10 + i)];
        [selfieHolder setHidden:NO];
        UIButton *selfieImage = (UIButton *) [selfieHolder viewWithTag:100];
        UILabel *selfieNameLabel =  (UILabel *) [selfieHolder viewWithTag:101];
        Selfie *currentSelfie = self.selfies[i];
        
        if ([currentSelfie hasAlreadyUnlocked]) {
            [selfieImage setImage:[currentSelfie iconForSelfieList] forState:UIControlStateNormal];
            [selfieNameLabel setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:17.0f]];
            [selfieNameLabel setTextColor:[UIColor colorWithRed:(102.0 / 255.0) green:(102.0 / 255.0) blue:(102.0 / 255.0) alpha:1]];
        }
        
        selfieImage.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        selfieImage.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        
        selfieHolder.tag = currentSelfie.achievementId;
        selfieImage.tag = 100 + currentSelfie.achievementId;
        selfieNameLabel.tag = 1000 + currentSelfie.achievementId;
        selfieNameLabel.text = currentSelfie.name;
        
    }
}

- (void) updateInterfaceByNotification:(NSNotification *) notification {
    
    for (int i = 0; i < self.selfies.count; i++) {
        Selfie * currentSelfie = (Selfie *) self.selfies[i];
        
        if ([currentSelfie hasAlreadyUnlocked]) {
            NSInteger imageId = (100 + currentSelfie.achievementId);
            NSInteger labelId = (1000 + currentSelfie.achievementId);
            
            UIButton *image = (UIButton *)[self.view viewWithTag:imageId];
            UILabel *label = (UILabel *) [self.view viewWithTag:labelId];
            
            [image setImage:[currentSelfie iconForSelfieList] forState:UIControlStateNormal];
            [label setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:label.font.pointSize]];
        }
    }
}

- (void)didClickShowSelfieDetails:(id)sender {
    Selfie *selfie = (Selfie *)[[AchievementsFactory sharedInstance].selfies objectAtIndex:((UIButton *)sender).tag - 100];

    ParentViewController *parent = (ParentViewController *)self.parentViewController.parentViewController.parentViewController;
    [parent displaySelfieDetails:selfie];
}
@end
