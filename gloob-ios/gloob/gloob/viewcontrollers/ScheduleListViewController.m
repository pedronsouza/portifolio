//
//  ScheduleListViewController.m
//  gloob
//
//  Created by zeroum on 04/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ScheduleListViewController.h"
#import "ScheduleAlert.h"
#import "Program.h"

@interface ScheduleListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tbViewList;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

- (IBAction)closeWindow:(UIButton *)sender;

- (void)setupInterface;
- (void)didTouchUpInside:(UIButton *)sender;

@end

@implementation ScheduleListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInterface];
}

#pragma mark - Private

- (void)setupInterface {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK
                                               size:28.0f]];
    } else {
        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK
                                               size:25.0f]];
    }
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    ScheduleAlert *schedule = [self.listOfExibition objectAtIndex:sender.tag - 100];
    
    if (sender.selected == YES) {
        
        [sender setSelected:NO];
        
        [schedule disableScheduleAlertNotification];
        NSString *label = [NSString stringWithFormat:@"remover|%@", [schedule.program.title slugalize]];
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Remover Alerta" andLabel:label];
        
    } else {
        
        [sender setSelected:YES];
        
        [schedule enableScheduleAlertNotification];
        NSString *label = [NSString stringWithFormat:@"ativar|%@", [schedule.program.title slugalize]];
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Ativar Alerta" andLabel:label];
    }
}

#pragma mark - UITableView DataSource / Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.listOfExibition count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tbViewList registerNib:[UINib nibWithNibName:[UIHelper deviceViewName:@"ScheduleListCell"] bundle:nil]
          forCellReuseIdentifier:[NSString stringWithFormat:@"cell_%li", (long)indexPath.row]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"cell_%li", (long)indexPath.row]
                                                            forIndexPath:indexPath];
    
    UILabel *lblDate = (UILabel *)[cell viewWithTag:1];
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:2];
    UILabel *lblSinopse = (UILabel *)[cell viewWithTag:3];
    
    ScheduleAlert *schedule = [self.listOfExibition objectAtIndex:indexPath.row];
    
    [lblDate setText:schedule.data];
    [lblDate setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:13.0f]];
    
    [lblTitle setText:[NSString stringWithFormat:@"EP.%li - %@", (long)schedule.numeroEpisodio, schedule.nome]];
    [lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:20.0f]];
    
    [lblSinopse setText:schedule.sinopse];
    [lblSinopse setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:14.0f]];
    
    UIButton *btn = (UIButton *)[cell viewWithTag:4];
    
    [btn setTag:100 + indexPath.row];
    
    [btn addTarget:self
            action:@selector(didTouchUpInside:)
  forControlEvents:UIControlEventTouchUpInside];
    
    [btn setSelected:[schedule isScheduleAlertEnabled]];
    
    if ([UIHelper isIPhone5]) {
        
        [lblTitle setFrame:CGRectMake(lblTitle.frame.origin.x,
                                      lblTitle.frame.origin.y,
                                      500.0f,
                                      lblTitle.frame.size.height)];
        
        [lblSinopse setFrame:CGRectMake(lblSinopse.frame.origin.x,
                                        lblSinopse.frame.origin.y,
                                        500.0f,
                                        lblSinopse.frame.size.height)];
        
        [btn setCenter:CGPointMake(540.0f,
                                   btn.center.y)];
        
    }
    
    return cell;
}

#pragma mark - IBActions

- (IBAction)closeWindow:(UIButton *)sender {

    if ([self.delegate respondsToSelector:@selector(dismissScheduleList)]) {
        [self.delegate dismissScheduleList];
    }
}

@end
     
    
