//
//  PanelViewController.m
//  gloob
//
//  Created by zeroum on 12/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "PanelViewController.h"
#import "ProgramManager.h"
#import "GameManager.h"
#import "Episode.h"
#import "Login.h"
#import "LoginManager.h"
#import "LoginViewController.h"
#import "POCViewController.h"
#import "NSMutableArray+QueueAdditions.h"
#import "FeaturedHome.h"
#import "ImageCache.h"
#import "EventLog.h"
#import "FeaturedHome.h"

#import <QuartzCore/QuartzCore.h>

@interface PanelViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewEsteira;

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewPanel;

@property (weak, nonatomic) IBOutlet UIView *viewPanels;
@property (weak, nonatomic) IBOutlet UIView *viewFooter;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewTrack;

@property (nonatomic, strong) NSMutableArray *arrayEpisodes;
@property (nonatomic, strong) NSMutableArray *arrayGames;
@property (nonatomic, strong) NSMutableArray *arrayFeaturedHome;
@property (nonatomic, strong) NSMutableArray *arrayEsteira;

- (void)setupInterface;
- (void)setupEsteira;
- (void)setupEpisodes;
- (void)setupEpisodesInViewForIpad:(UIView *)view;
- (void)setupEpisodesInViewForIphone:(UIView *)view;
- (void)setupGames;

@end

@implementation PanelViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.arrayEpisodes = [[NSMutableArray alloc] initWithArray:[[ProgramManager sharedManager] objectsFeaturedEpisode]];
    
    self.arrayGames = [[NSMutableArray alloc] initWithArray:[[GameManager sharedManager] objects]];
    NSArray *games = [[GameManager sharedManager] objects];
    if (games.count > 0) {
        while (self.arrayGames.count < 9) {
            [self.arrayGames addObjectsFromArray:games];
        }
    }

    
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    [self setupInterface];
    [self setupEpisodes];
    [self setupGames];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        ProgramManager *manager = [ProgramManager sharedManager];
        
        [manager loadFeaturedHomeWithCompletionHandler:^(BOOL success, NSError *error) {
            
            if (success) {
                self.arrayFeaturedHome = [[NSMutableArray alloc] initWithArray:[[ProgramManager sharedManager] objectsFeaturedHome]];
                
                [self setupEsteira];
            }
        }];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kShouldMarkAccessEvent]) {
        [[EventLog sharedInstance] logAccessEvent];
        [[EventLog sharedInstance] verifyAndLogDateEvents];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldMarkAccessEvent];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - UITouch 

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.viewEsteira];
    
    for (EsteiraItemView *view in self.viewEsteira.subviews)
    {
        if ([view isKindOfClass:[EsteiraItemView class]]) {
            if ([view.layer.presentationLayer hitTest:touchLocation])
            {
                // This button was hit whilst moving - do something with it here
                if (self.isEsteiraPaused == NO) {
                    [self pauseAnimations];
                } else {
                    [self resumeAnimations];
                }
                
                [view openSelectedCharacter];
                
                break;
            }
        }
    }
}

- (void)didSelectFeaturedHomeItem:(FeaturedHome *)program {
    
    Program *prog = [[ProgramManager sharedManager] findProgramByID:program.id_featuredHome];
    
    NSString *label = [NSString stringWithFormat:@"home|%@", [prog.title slugalize]];
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Personagem" andLabel:label];
    
    if ([self.delegate respondsToSelector:@selector(displaySelectedCharacterPopUp:)]) {
        [self.delegate displaySelectedCharacterPopUp:prog];
    }
}

#pragma mark - Public

- (void)retryConnection {
    
    for (UIView *view in self.scrViewPanel.subviews) {
        
        if ([view isKindOfClass:[EpisodeView class]]) {
            
            EpisodeView *episodeView = (EpisodeView *)view;
            
            [episodeView removeFromSuperview];
            
            episodeView = nil;
        }
        
        if ([view isKindOfClass:[PanelGameView class]]) {
            
            PanelGameView *gameView = (PanelGameView *)view;
            
            [gameView removeFromSuperview];
            
            gameView = nil;
        }
    }
}

#pragma mark - Private

- (void)setupInterface {
    
    CGSize contentSize;
    CGPoint contentOffset;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        contentSize = CGSizeMake(2510.0f, 614.0f);
        contentOffset = CGPointMake(743.0f, 47.0f);

    } else {
        
        contentSize = CGSizeMake(1907.0f, 437.0f);
        
        if ([UIHelper isIPhone5]) {
            contentOffset = CGPointMake(720.0f, 60.0f);
        } else {
            contentOffset = CGPointMake(840.0f, 60.0f);
        }
    }
    
    [self.scrViewPanel setContentSize:contentSize];
    [self.scrViewPanel setContentOffset:contentOffset];
}

- (void)setupEsteira {
    
    self.arrayEsteira = [[NSMutableArray alloc] init];
    
    // inital config
    int spacing = 0;
    int initial_degrees = 0;
    int right_margin = 0;
    
    int total_itens = 12;
    
    int item_width = 160;
    int item_heigth = 160;
    
    int degrees_variation = 360 / total_itens;
    
    int first_item_x = [[UIScreen mainScreen] bounds].size.width - (total_itens * (item_width+right_margin));
    
    for (int i = 0; i < total_itens; i++) {
        
        int initial_y;
        
        if (first_item_x + spacing < 50) {
            initial_y = self.viewEsteira.frame.size.height;
        } else {
            initial_y = 0;
        }
        
        FeaturedHome *program = [self.arrayFeaturedHome objectAtIndex:i];
        
        EsteiraItemView *item = [[EsteiraItemView alloc] initWithFrame:CGRectMake(first_item_x+spacing,
                                                                                  initial_y,
                                                                                  item_width,
                                                                                  item_heigth)
                                                               program:program
                                                              delegate:self];
        
        [item setViewID:i];
        [item setDegrees:initial_degrees];
        [item setInital_x:first_item_x];
        
        [self.viewEsteira addSubview:item];
        [self.viewEsteira sendSubviewToBack:item];
        
        [self.arrayEsteira addObject:item];
        
        // prepare for the next loop
        spacing += item_width + right_margin;
        initial_degrees += degrees_variation;
    }
}

- (void)resumeAnimations {
    
    self.isEsteiraPaused = NO;
    
    for (EsteiraItemView *item in self.arrayEsteira) {
        [item resume];
    }
}

- (void)pauseAnimations {
    
    self.isEsteiraPaused = YES;
    
    for (EsteiraItemView *item in self.arrayEsteira) {
        [item pause];
    }
}

- (void)setupEpisodes {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        for (NSInteger tag = 1; tag <= 9; tag++) {
            [self setupEpisodesInViewForIpad:[self.viewPanels viewWithTag:tag]];
        }
        
    } else {
        
        for (NSInteger tag = 1; tag <= 9; tag++) {
            [self setupEpisodesInViewForIphone:[self.viewPanels viewWithTag:tag]];
        }
    }
}

- (void)setupEpisodesInViewForIpad:(UIView *)view {
    
    for (NSInteger episode = 0; episode < 5; episode++) {
        
        CGRect frame;
        
        if (episode == 0) {
            
            if (view.tag == 1 || view.tag == 4 || view.tag == 8) {
                
                frame = CGRectMake(166.0f, 0.0f, 328.0f, 186.0f);
                
            } else {
                
                frame = CGRectMake(332.0f, 0.0f, 328.0f, 186.0f);
            }
            
        } else {
            
            CGFloat x;
            CGFloat y = 0.0f;
            
            if (view.tag == 1 || view.tag == 4 || view.tag == 8) {
                
                x = 0.0f;
                
                if (episode == 3 || episode == 4) {
                    x = 502.0f;
                }
                
                if (episode == 2 || episode == 4) {
                    y = 97.0f;
                }
                
            } else {
                
                x = 166.0f;
                
                if (episode == 3 || episode == 4) {
                    x = 668.0f;
                }
                
                if (episode == 2 || episode == 4) {
                    y = 97.0f;
                }
            }
            
            frame = CGRectMake(x, y, 158.0f, 89.0f);
        }
        
        [self addEpisodeInView:view withFrame:frame];
    }
}

- (void)setupEpisodesInViewForIphone:(UIView *)view {
    
    for (NSInteger episode = 0; episode < 5; episode++) {
        
        CGRect frame;
        
        if (episode == 0) {
            
            if (view.tag == 1 || view.tag == 4 || view.tag == 8) {
                
                frame = CGRectMake(125.0f, 0.0f, 247.0f, 139.0f);
                
            } else {
                
                frame = CGRectMake(250.0f, 0.0f, 247.0f, 139.0f);
            }
            
        } else {
            
            CGFloat x;
            CGFloat y = 0.0f;
            
            if (view.tag == 1 || view.tag == 4 || view.tag == 8) {
                
                x = 0.0f;
                
                if (episode == 3 || episode == 4) {
                    x = 377.0f;
                }
                
                if (episode == 2 || episode == 4) {
                    y = 72.0f;
                }
                
            } else {
                
                x = 125.0f;
                
                if (episode == 3 || episode == 4) {
                    x = 502.0f;
                }
                
                if (episode == 2 || episode == 4) {
                    y = 72.0f;
                }
            }
            
            frame = CGRectMake(x, y, 120.0f, 67.0f);
        }
        
        [self addEpisodeInView:view withFrame:frame];
    }
}

- (void)addEpisodeInView:(UIView *)view withFrame:(CGRect)frame {
    
    Episode *episode = [self.arrayEpisodes objectAtIndex:0];

    EpisodeView *episodeView = [[EpisodeView alloc] initWithFrame:frame
                                                          episode:episode
                                                         delegate:self];

    [view addSubview:episodeView];
    
    [self.arrayEpisodes dequeue];
}

- (void)setupGames {

    for (NSInteger tag = 1; tag <= 9; tag++) {
        
        CGRect frame;
        
        UIView *view = [self.viewPanels viewWithTag:tag];
        
        if (view.tag == 1 || view.tag == 4 || view.tag == 8) {
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                
                frame = CGRectMake(668.0f, 0.0f, 158.0f, 186.0f);
                
            } else {
                
                frame = CGRectMake(502.0f, 0.0f, 120.0f, 139.0f);
                
            }
            
        } else {
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                
                frame = CGRectMake(0.0f, 0.0f, 158.0f, 186.0f);
                
            } else {
                
                frame = CGRectMake(0.0f, 0.0f, 120.0f, 139.0f);
                
            }
        }
        if (self.arrayGames.count > 0) {
            Game *game = [self.arrayGames objectAtIndex:0];
            
            PanelGameView *gameView = [[PanelGameView alloc] initWithFrame:frame
                                                                      game:game
                                                                  delegate:self];
            [view addSubview:gameView];
            [self.arrayGames dequeue];
        }
    }
}

#pragma mark - EpisodeView Delegate

- (void)playEpisode:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from {
    
    Login *login = [LoginManager getSavedLogin];
    
    if (login.access_token && [LoginManager isTokenActive]) {
        
        POCViewController *controller = [[POCViewController alloc] initWithNibName:[UIHelper deviceViewName:@"POCView"]
                                                                            bundle:nil];
        
        [controller setEpisode:episode];
        [controller setAccess_token:login.access_token];
        [controller setArrayVideosSource:[[ProgramManager sharedManager] objectsFeaturedEpisode]];
        [controller setCurrentIndex:0];
        [controller setOpeningFrom:@"VÍDEOS MAIS NOVOS"];
        
        [self presentViewController:controller animated:YES completion:nil];
        
        NSString *label = [NSString stringWithFormat:@"home|vod|%@||fechado|gloob|%@|%@|%@|%@|%@", @(episode.id_globo_videos),
                           [episode.category slugalize],
                           [episode.program.title slugalize],
                           (episode.season > 0 ? @(episode.season) : @""),
                           (episode.number > 0 ? @(episode.number) : @""),
                           [episode.title slugalize]];
        
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Play" andLabel:label];
    } else {
        
        LoginViewController *controller = [[LoginViewController alloc] initWithNibName:[UIHelper deviceViewName:@"LoginView"]
                                                                                bundle:nil];
        
        [controller setDelegate:self];
        [controller setEpisode:episode];
        [controller setWillPlayNow:YES];
        
        [controller setArrayVideosSource:[[ProgramManager sharedManager] objectsFeaturedEpisode]];
        [controller setVideoAtIndex:0];
        [controller setOpeningFrom:@"VÍDEOS MAIS NOVOS"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - PanelGameView Delegate

- (void)playGame:(Game *)game {
    
    if ([self.delegate respondsToSelector:@selector(displayGamesViewControllerWithGame:)]) {
        [self.delegate displayGamesViewControllerWithGame:game];
    }
}

#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return @"/";
}
@end
