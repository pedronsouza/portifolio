//
//  MyAlertsViewController.h
//  gloob
//
//  Created by zeroum on 15/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "ScheduleAlertView.h"
#import "GAHelper.h"

@interface MyAlertsViewController : GloobViewController <ScheduleAlertViewDelegate, UIAlertViewDelegate, GATrackableScreen>

@property (nonatomic, assign) BOOL isOpeningFromCharView;

@property (nonatomic, assign) id delegate;

@end

@protocol MyAlertsViewControllerDelegate <NSObject>

@optional

- (void)showAllCharacters;

@end
