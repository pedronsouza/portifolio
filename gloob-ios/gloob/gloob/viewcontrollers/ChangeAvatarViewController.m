//
//  ChangeAvatarViewController.m
//  gloob
//
//  Created by Pedro on 3/26/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ChangeAvatarViewController.h"
#import "AchievementRepository.h"
#import "ProfileManager.h"
#import "ParentViewController.h"

#define kChangeAvatarOriginX 0
#define kChangeAvatarOriginY 20
#define kChangeAvatarHeight [self heightForAvatarIcon]
@interface ChangeAvatarViewController ()
@property (nonatomic, strong) NSArray *unlockedAvatars;
@property (nonatomic, strong) Avatar *currentAvatar;
@property (nonatomic, strong) Avatar *selectedAvatar;

@end

@implementation ChangeAvatarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(onKeyboardHide:)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
    
    if ([UIHelper isIPhone5]) {
        [self.btnClose setCenter:CGPointMake(self.btnClose.center.x + 50.0f, self.btnClose.center.y)];
    }
    
    self.currentAvatar = [[ProfileManager sharedManager] currentProfile].currentAvatar;
    self.selectedAvatar = self.currentAvatar;
    self.unlockedAvatars = [[AchievementRepository sharedInstance] getUnlockedAvatars];
    self.mainAvatar.image = [self.currentAvatar iconForProfile];
    self.lblNomeAvatar.text = [[ProfileManager sharedManager] currentProfile].name;
        
    int avatarOriginX = kChangeAvatarOriginX;
    int contentSize = 0;
    int avatarOriginY = 0;
    for (int i = 0; i < self.unlockedAvatars.count; i++) {
        Avatar *avatar = self.unlockedAvatars[i];
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            contentSize += (kChangeAvatarOriginY + kChangeAvatarHeight) + 20;
            avatarOriginY = (kChangeAvatarOriginY + kChangeAvatarHeight) + 20;
        } else {
            contentSize += (kChangeAvatarOriginY + kChangeAvatarHeight);
            avatarOriginY = (kChangeAvatarOriginY + kChangeAvatarHeight);
        }
        
        CGRect avatarView = CGRectMake(avatarOriginX,
                                       (avatarOriginY * i),
                                       self.scrollView.frame.size.width,
                                       kChangeAvatarHeight);
        
        AvatarView *view = [[AvatarView alloc]
                            initVerticalScrollingAvatarWithFrame:avatarView avatar:self.unlockedAvatars[i] delegate:self];

        if (avatar.achievementId == self.currentAvatar.achievementId) {
            [view setSelected:YES];
        }
        
        [self.scrollView addSubview:view];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, contentSize + 20);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)moveViewUp {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                             
                             [self.view setFrame:CGRectMake(0.0f,
                                                            -200.0f,
                                                            [UIHelper screenSize].width,
                                                            [UIHelper screenSize].height)];
                         } else {
                             [self.view setFrame:CGRectMake(0.0f,
                                                            -155.0f,
                                                            [UIHelper screenSize].width,
                                                            [UIHelper screenSize].height)];
                         }
                         
                     }];
}

- (void)moveViewDown {
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         
                         [self.view setFrame:CGRectMake(0.0f,
                                                        0.0f,
                                                        [UIHelper screenSize].width,
                                                        [UIHelper screenSize].height)];
                     }];
}

- (void)onKeyboardHide:(NSNotification *)notification {
    
    [self moveViewDown];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [textField clearsOnBeginEditing];
    
    if (self.lblNomeAvatar) {
        [self moveViewUp];
    }
    
    if (self.lblNomeAvatar) {
        [self moveViewUp];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self moveViewDown];
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)didClickChangeAvatar:(id)sender {
    Profile *profile = [[ProfileManager sharedManager] currentProfile];
    NSString *oldAvatar = profile.currentAvatar.name;
    profile.currentAvatar = self.selectedAvatar;
    profile.name = self.lblNomeAvatar.text;
    [[ProfileManager sharedManager] setCurrentProfile:profile];
    [[ProfileManager sharedManager] saveLatestUsedProfile];
    [[ProfileManager sharedManager] saveProfilesToUserDefaults];
    
    NSString *label = [NSString stringWithFormat:@"%@|%@", [oldAvatar slugalize], [self.selectedAvatar.name slugalize]];
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Mudar Avatar" andLabel:label];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange object:nil];
    
    ParentViewController *parent = (ParentViewController *)self.parentViewController;
    [parent dismissPopUp];
}

- (IBAction)didClickClose:(id)sender {
    ParentViewController *parent = (ParentViewController *)self.parentViewController;
    [parent dismissPopUp];
}

- (void)didSelectedAvatar:(Avatar *)avatar {
    
    self.selectedAvatar = avatar;
    self.mainAvatar.image = [self.selectedAvatar iconForProfile];
    for (UIView *view in self.scrollView.subviews) {
        
        if ([view isKindOfClass:[AvatarView class]]) {
            
            AvatarView *avatarView = (AvatarView *)view;
            
            if (avatarView.avatar.achievementId == avatar.achievementId) {
                [avatarView setSelected:YES];
            } else {
                [avatarView setSelected:NO];
            }
        }
    }
}

- (CGFloat) heightForAvatarIcon {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 83.0f;
    } else {
        return 57.0f;
    }
}

- (void)deleteProfile:(Profile *)avatar {

}
@end
