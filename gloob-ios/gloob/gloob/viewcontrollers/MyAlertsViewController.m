//
//  MyAlertsViewController.m
//  gloob
//
//  Created by zeroum on 15/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "MyAlertsViewController.h"
#import "ScheduleAlert.h"
#import "ScheduleAlertManager.h"
#import "Program.h"
#import "ProgramManager.h"
#import "ColoredButton.h"

@interface MyAlertsViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewAlerts;
@property (weak, nonatomic) IBOutlet UIView *viewNoAlerts;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (weak, nonatomic) IBOutlet UILabel *lblNoAlert1;
@property (weak, nonatomic) IBOutlet UILabel *lblNoAlert2;
@property (weak, nonatomic) IBOutlet UILabel *lblNoAlert3;

@property (weak, nonatomic) IBOutlet ColoredButton *btnShowAllChars;

@property (nonatomic, strong) ScheduleAlert *selectedSchedule;

- (IBAction)showAllCharacters:(UIButton *)sender;

- (void)setupInterface;
- (void)setupScrViewAlertsIpad;
- (void)setupScrViewAlertsIphone;
- (void)removeAllViewsInScrollView;

@end

@implementation MyAlertsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupInterfaceByNotification:) name:kAvatarChange object:nil];
    [self setupInterface];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
}

#pragma mark - Private

- (void)setupInterface {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self setupScrViewAlertsIpad];
        
        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:32.0f]];
        [self.lblNoAlert1 setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:30.0f]];
        [self.lblNoAlert2 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:18.0f]];
        [self.lblNoAlert3 setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:24.0f]];
        [self.lblDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:16.0f]];

    } else {
        
        [self setupScrViewAlertsIphone];

        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:26.0f]];
        [self.lblNoAlert1 setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:18.0f]];
        [self.lblNoAlert2 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:14.0f]];
        [self.lblNoAlert3 setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:15.0f]];
        [self.lblDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:14.0f]];
    }

    [self setupVisibleView];
}
- (void) setupInterfaceByNotification:(NSNotification *)notification {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self setupScrViewAlertsIpad];
    } else {
        [self setupScrViewAlertsIphone];
    }
}
- (void)setupVisibleView {
    
    if ([[[ScheduleAlertManager sharedManager] getAllValidAlerts] count] == 0) {
        [self.viewNoAlerts setHidden:NO];
    } else {
        
        NSString *title = [NSString stringWithFormat:@"SEUS ALERTAS(%li)",(long)[[ScheduleAlertManager sharedManager] getAllValidAlerts].count];
        
        [self.lblTitle setText:title];
        
        [self.viewNoAlerts setHidden:YES];
    }
}

- (void)setupScrViewAlertsIpad {
    
    CGSize viewSize = CGSizeMake(160.0f, 270.0f);
    CGFloat space = 20.0f;
    CGFloat x = 72.0f;
    CGFloat y = 140.0f;
    
    NSInteger i = 0;
    
    for (ScheduleAlert *schedule in [[ScheduleAlertManager sharedManager] getAllValidAlerts]) {
        
        if (i % 5 == 0 && i > 0) {
            
            x = 72.0f;
            y += (viewSize.height + space);
            
        } else if (i > 0) {
            x += (viewSize.width + space);
        }
        
        CGRect frame = CGRectMake(x, y, viewSize.width, viewSize.height);
        
        ScheduleAlertView *view = [[ScheduleAlertView alloc] initWithFrame:frame
                                                                  schedule:schedule
                                                                  delegate:self];
        
        [self.scrViewAlerts addSubview:view];
        
        i++;
    }
    
    [self.scrViewAlerts setContentSize:CGSizeMake(self.scrViewAlerts.frame.size.width, y + viewSize.height + space)];
    
    [self setupVisibleView];
}

- (void)setupScrViewAlertsIphone {
    
    CGSize viewSize = CGSizeMake(160.0f, 270.0f);
    
    CGFloat margin = 0.0f;
    CGFloat space = 0.0f;
    CGFloat space2 = 0.0f;
    NSInteger numberOfColumns;
    
    if ([UIHelper isIPhone5]) {
        
        margin = 14.0f;
        space = 30.0f;
        space2 = 30.0f;
        numberOfColumns = 3;
        
    } else {
        
        margin = 60.0f;
        space = 40.0f;
        space2 = 40.0f;
        numberOfColumns = 2;
    }
    
    CGFloat x = margin;
    CGFloat y = 70.0f;
    
    NSInteger i = 0;

    for (ScheduleAlert *schedule in [[ScheduleAlertManager sharedManager] getAllValidAlerts]) {
        
        if (i % numberOfColumns == 0 && i > 0) {
            
            x = margin;
            y += (viewSize.height + space2);
            
        } else if (i > 0) {
            x += (viewSize.width + space);
        }
        
        CGRect frame = CGRectMake(x, y, viewSize.width, viewSize.height);
        
        ScheduleAlertView *view = [[ScheduleAlertView alloc] initWithFrame:frame
                                                                  schedule:schedule
                                                                  delegate:self];
        
        [self.scrViewAlerts addSubview:view];

        i++;
    }
    
    [self.scrViewAlerts setContentSize:CGSizeMake(self.scrViewAlerts.frame.size.width, y + viewSize.height + space2)];
    
    [self setupVisibleView];
}

- (void)removeAllViewsInScrollView {
    
    for (UIView *view in self.scrViewAlerts.subviews) {
        
        if ([view isKindOfClass:[ScheduleAlertView class]]) {
            
            [view removeFromSuperview];
        }
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self setupScrViewAlertsIpad];
    } else {
        [self setupScrViewAlertsIphone];
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 1) {
            NSString *label = [NSString stringWithFormat:@"remover|%@", [self.selectedSchedule.program.title slugalize]];
            [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Remover Alerta" andLabel:label];
            
            [self.selectedSchedule disableScheduleAlertNotification];
            [self removeAllViewsInScrollView];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                [self setupScrViewAlertsIpad];
            } else {
                [self setupScrViewAlertsIphone];
            }
        }
    }
}

#pragma mark - ScheduleAlertView Delegate

- (void)removeScheduleFromList:(ScheduleAlert *)schedule {
    
    self.selectedSchedule = schedule;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Deseja realmente remover este alerta?"
                                                   delegate:nil
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Remover", nil];
    
    [alert setTag:1];
    [alert setDelegate:self];
    
    [alert show];
}

#pragma mark - IBActions

- (IBAction)showAllCharacters:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(showAllCharacters)]) {
        [self.delegate showAllCharacters];
    }
}
#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return @"/alerta";
}
@end
