//
//  ProfileCreatorViewController.m
//  gloob
//
//  Created by zeroum on 13/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ProfileCreatorViewController.h"
#import "ColoredButton.h"
#import "GloobTextField.h"
#import "LoginViewController.h"
#import "Avatar.h"
#import "AchievementRepository.h"
#import "Profile.h"
#import "ProfileManager.h"


@interface ProfileCreatorViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewTouch;

@property (weak, nonatomic) IBOutlet UIView *view1;

@property (weak, nonatomic) IBOutlet UILabel *lblWelcome1;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome2;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome3;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome4;

@property (weak, nonatomic) IBOutlet ColoredButton *btnStartCreation;
@property (weak, nonatomic) IBOutlet ColoredButton *btnCreateLater;

@property (weak, nonatomic) IBOutlet UIView *view2;

@property (weak, nonatomic) IBOutlet UILabel *lblVoltar1;
@property (weak, nonatomic) IBOutlet UILabel *lblMission1;
@property (weak, nonatomic) IBOutlet UILabel *lblMission2;

@property (weak, nonatomic) IBOutlet ColoredButton *btnContinue;

@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIScrollView *scrViewAvatars;

@property (weak, nonatomic) IBOutlet UILabel *lblVoltar2;
@property (weak, nonatomic) IBOutlet UILabel *lblCreation1;
@property (weak, nonatomic) IBOutlet UILabel *lblCreation2;
@property (weak, nonatomic) IBOutlet UILabel *lblCreation3;

@property (weak, nonatomic) IBOutlet GloobTextField *txtFieldName;
@property (weak, nonatomic) IBOutlet GloobTextField *txtFieldBirthday;

@property (weak, nonatomic) IBOutlet ColoredButton *btnFinishCreation;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIToolbar *toolBar;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *birthday;
@property (weak, nonatomic) IBOutlet UIButton *btnVoltar2;
@property (weak, nonatomic) IBOutlet UIButton *btnVoltar1;

@property (weak, nonatomic) IBOutlet UILabel *lblLoginMessage1;
@property (weak, nonatomic) IBOutlet UILabel *lblLoginMessage2;
@property (weak, nonatomic) IBOutlet ColoredButton *btLogin;

@property (weak, nonatomic) IBOutlet UIImageView *overlayFrame;
@property (nonatomic, strong) Avatar *selectedAvatar;

- (void)setupInterface;
- (void)setupDatePicker;
- (void)loadAvatars;
- (void)displayView:(UIView *)view1 hideView:(UIView *)view2 andView:(UIView *)view3;
- (void)moveViewUp;
- (void)moveViewDown;
- (void)close:(id)sender;
- (void)onKeyboardHide:(NSNotification *)notification;

- (IBAction)startProfileCreation:(UIButton *)sender;
- (IBAction)createLater:(UIButton *)sender;
- (IBAction)goBack:(UIButton *)sender;

- (IBAction)finishCreation:(UIButton *)sender;
- (IBAction)closeWindow:(UIButton *)sender;
- (IBAction)didClickLogin:(id)sender;
@end

@implementation ProfileCreatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(onKeyboardHide:)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
    
    [self setupInterface];
    [self setupDatePicker];
    [self loadAvatars];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    
    if ([[ProfileManager sharedManager] currentProfile]) {
        [self.view3 setHidden:NO];
        [self.view3 setAlpha:1.0];
        [self.view2 setHidden:YES];
        [self.view1 setHidden:YES];
        [self.btnVoltar1 setHidden:YES];
        [self.btnVoltar2 setHidden:YES];
        [self.lblVoltar2 setHidden:YES];
        [self.lblCreation1 setText:@"NOVO PERFIL"];
        [self.lblCreation1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:26.0f]];
    }
    
    if ([LoginManager isAuthenticated]) {
        [self hideLoginInfo];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private
- (void)setupInterface {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.lblVoltar1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:11.0f]];
        [self.lblVoltar2 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:11.0f]];
        
        [self.lblWelcome1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:45.0f]];
        [self.lblWelcome2 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:30.0f]];
        [self.lblWelcome3 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:20.0f]];
        [self.lblWelcome4 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:20.0f]];
        
        [self.lblMission1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:32.0f]];
        [self.lblMission2 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:25.0f]];
        
        [self.lblCreation1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:32.0f]];
        [self.lblCreation2 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.lblCreation3 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
        [self.btnStartCreation.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:28.0f]];
        [self.btnCreateLater.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:23.0f]];
        [self.btnContinue.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:23.0f]];
        [self.btnFinishCreation.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:23.0f]];
        
        [self.txtFieldName setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:25.0f]];
        [self.txtFieldBirthday setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:25.0f]];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(close:)];
        
        [self.viewTouch addGestureRecognizer:tap];
        
    } else {
        
        [self.lblVoltar1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:11.0f]];
        [self.lblVoltar2 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:11.0f]];
        
        [self.lblWelcome1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:30.0f]];
        [self.lblWelcome2 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:18.0f]];
        [self.lblWelcome3 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.lblWelcome4 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:14.0f]];
        
        [self.lblMission1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:21.0f]];
        [self.lblMission2 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:18.0f]];
        
        [self.lblCreation1 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:18.0f]];
        [self.lblCreation2 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:12.0f]];
        [self.lblCreation3 setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:12.0f]];
        
        [self.btnStartCreation.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:19.0f]];
        [self.btnCreateLater.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:15.0f]];
        [self.btnContinue.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:19.0f]];
        [self.btnFinishCreation.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:19.0f]];
        
        [self.txtFieldName setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:17.0f]];
        [self.txtFieldBirthday setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:17.0f]];
        
        if ([UIHelper isIPhone5]) {
            [self.lblMission1 setCenter:CGPointMake(self.view.center.x, self.lblMission1.center.y)];
        }
    }
}

- (void)setupDatePicker {
    
    if (self.toolBar == nil) {
        
        self.toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 44.0f)];
        
        [self.toolBar setBarStyle:UIBarStyleDefault];
        
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:nil
                                                                               action:nil];
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"OK"
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self
                                                                action:@selector(closeDatePicker:)];
        
        [self.toolBar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    }
    
    self.txtFieldBirthday.inputAccessoryView = self.toolBar;
    
    self.datePicker = [[UIDatePicker alloc] init];
    
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    [self.datePicker setMaximumDate:[NSDate date]];
    
    // Subtracting one year from current date to display in datepicker
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:now];
    [comps setYear:[comps year] - 1];
    NSDate *oneYearAgo = [gregorian dateFromComponents:comps];
    
    [self.datePicker setDate:oneYearAgo];
    
    [self.datePicker addTarget:self
                        action:@selector(updateValue:)
              forControlEvents:UIControlEventValueChanged];
    
    self.txtFieldBirthday.inputView = self.datePicker;
}

- (void)closeDatePicker:(id)sender {
    
    [self.txtFieldBirthday resignFirstResponder];
    
    [self moveViewDown];
}

- (void)updateValue:(id)sender {
    
    self.birthday = self.datePicker.date;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"dd/MM/YYYY"];
    
    [self.txtFieldBirthday setText:[formatter stringFromDate:self.birthday]];
}

- (void)loadAvatars {
    
    CGSize viewSize;
    CGFloat x = 0.0f;
    CGFloat y = 0.0f;
    CGFloat space = 0.0f;
    NSInteger i = 0;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        viewSize = CGSizeMake(145.0f, 180.0f);
        space = 15.0f;
        x = space;
        
    } else {
        
        viewSize = CGSizeMake(100.0f, 125.0f);
        space = 10.0f;
        x = space;
    }
    
    for (Avatar *avatar in [Avatar createDefaultAchievements]) {

        CGRect frame = CGRectMake(x, y, viewSize.width, viewSize.height);
        
        AvatarView *view = [[AvatarView alloc] initHorizontalScrollingAvatarWithFrame:frame
                                                                               avatar:avatar
                                                                             delegate:self];
        
        if (i == 0) {
            [view setSelected:YES];
            self.selectedAvatar = avatar;
        }
        
        [self.scrViewAvatars addSubview:view];
        
        x += (viewSize.width + space);
        
        i++;
    }
    
    [self.scrViewAvatars setContentSize:CGSizeMake(x + space, self.scrViewAvatars.frame.size.height)];
}

- (void)displayView:(UIView *)view1 hideView:(UIView *)view2 andView:(UIView *)view3 {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         
                         [view1 setAlpha:1.0f];
                         [view2 setAlpha:0.0f];
                         [view3 setAlpha:0.0f];
                     }];
}

- (void)moveViewUp {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.view setFrame:CGRectMake(0.0f,
                                                        -155.0f,
                                                        [UIHelper screenSize].width,
                                                        [UIHelper screenSize].height)];
                         
                     }];
}

- (void)moveViewDown {
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         
                         [self.view setFrame:CGRectMake(0.0f,
                                                        0.0f,
                                                        [UIHelper screenSize].width,
                                                        [UIHelper screenSize].height)];
                     }];
}

- (void)close:(id)sender {
    
    [self.txtFieldName resignFirstResponder];
    [self.txtFieldBirthday resignFirstResponder];
    
    if ([self.delegate respondsToSelector:@selector(didFinishProfileCreation)]) {
        [self.delegate didFinishProfileCreation];
    }
}

- (void)onKeyboardHide:(NSNotification *)notification {
    
    [self moveViewDown];
}

#pragma mark - AvatarView Delegate

- (void)didSelectedAvatar:(Avatar *)avatar {
    
    self.selectedAvatar = avatar;
    
    for (UIView *view in self.scrViewAvatars.subviews) {
        
        if ([view isKindOfClass:[AvatarView class]]) {
            
            AvatarView *avatarView = (AvatarView *)view;
            
            if ([avatarView.avatar.icon isEqualToString:avatar.icon]) {
                [avatarView setSelected:YES];
            } else {
                [avatarView setSelected:NO];
            }
        }
    }
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 0) {
            
            [self.txtFieldName setText:nil];
            [self.txtFieldName becomeFirstResponder];
        }
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [textField clearsOnBeginEditing];
    
    if (self.txtFieldName) {
        [self moveViewUp];
    }
    
    if (self.txtFieldBirthday) {
        [self moveViewUp];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self moveViewDown];
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - IBActions

- (IBAction)startProfileCreation:(UIButton *)sender {
    __block ProfileCreatorViewController *self2 = self;
    switch (sender.tag) {
        case 1: {
            [self displayView:self.view2 hideView:self.view1 andView:self.view3];
            break;
        }
        case 2: {
            if (![LoginManager isAuthenticated]) {
                [self showLoginWithCompletionBlock:^{
                    [self2 displayView:self.view3 hideView:self.view1 andView:self.view2];
                }];
            } else {
                [self displayView:self.view3 hideView:self.view1 andView:self.view2];
            }
            
            break;
        }
        default:
            break;
    }
}

- (void) showLoginWithCompletionBlock:(void(^)())completionBlock {
    LoginViewController *controller = [[LoginViewController alloc] initWithNibName:[UIHelper deviceViewName:@"LoginView"]
                                                                            bundle:nil];
    [controller setDelegate:self];
    [controller setWillPlayNow:YES];
    [controller setAfterCloseBlock:completionBlock];
    [controller setArrayVideosSource:[[[ProfileManager sharedManager] currentProfile] favorites]];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)createLater:(UIButton *)sender {
    
    [self close:nil];
}

- (IBAction)goBack:(UIButton *)sender {
    
    switch (sender.tag) {
        case 3:
            [self displayView:self.view1 hideView:self.view2 andView:self.view3];
            break;
            
        case 4:
            [self displayView:self.view2 hideView:self.view1 andView:self.view3];
            break;
            
        default:
            break;
    }
}

- (IBAction)finishCreation:(UIButton *)sender {
    
    if (self.txtFieldName.text.length > 15) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ops!"
                                                        message:@"O nome do seu avatar deve conter no máximo 15 caracteres"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        
        [alert setTag:1];
        
        [alert show];
        
    } else {
        
        [self.txtFieldName resignFirstResponder];
        [self.txtFieldBirthday resignFirstResponder];
        
        if (self.txtFieldName.text.length == 0 || self.txtFieldBirthday.text.length == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ops!"
                                                            message:@"Preencha todos os campos corretamente para continuar"
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
            
            [alert show];
            
            [self moveViewDown];
            
        } else {
            
            Profile *profile = [[Profile alloc] initNewProfileWithName:self.txtFieldName.text
                                                              birthday:self.birthday
                                                                avatar:self.selectedAvatar];
            
            [[ProfileManager sharedManager] addProfileToList:profile];
            
            if ([self.delegate respondsToSelector:@selector(didFinishProfileCreation)]) {
                [self.delegate didFinishProfileCreation];
            }
        }
    }
}

- (IBAction)closeWindow:(UIButton *)sender {
    
    [self close:nil];
}

#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return @"/bem-vindo/crie-seu-avatar";
}

- (void)didClickLogin:(id)sender {
    __block ProfileCreatorViewController *self2 = self;
    [self showLoginWithCompletionBlock:^{
        [self2 displayView:self.view2 hideView:self.view1 andView:self.view3];
    }];
    
    NSString *label = [NSString stringWithFormat:@"inferior|%@/ios-%@", [[[UIDevice currentDevice] model] slugalize], [[[UIDevice currentDevice] systemVersion] slugalize]];
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Entrar" andLabel:label];
}

- (void) hideLoginInfo {
    [self.lblLoginMessage1 setHidden:YES];
    [self.lblLoginMessage2 setHidden:YES];
    [self.btLogin setHidden:YES];
}
@end
