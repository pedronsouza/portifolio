//
//  POCViewController.h
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "PlayerViewController.h"
#import "SelectedCharacterViewController.h"
#import "iCarousel.h"
#import "GameView.h"
#import "GameInteractionViewController.h"
#import "GAHelper.h"

@class Episode;
@class Game;

@interface POCViewController : GloobViewController <iCarouselDataSource, iCarouselDelegate, PlayerViewControllerDelegate, GameViewDelegate, SelectedCharacterViewControllerDelegate, GameInteractionViewControllerDelegate, GATrackableScreen>

@property (weak, nonatomic) IBOutlet UIScrollView *scrView;

@property (nonatomic, strong) NSString *access_token;

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) Episode *episode;

@property (nonatomic, strong) NSMutableArray *arrayVideosSource;
//@property (nonatomic, assign) NSInteger videoAtIndex;
@property (nonatomic, strong) NSString *openingFrom;

- (void)hideCarouselView;

@end



