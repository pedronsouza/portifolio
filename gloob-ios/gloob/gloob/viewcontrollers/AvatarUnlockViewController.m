//
//  AvatarUnlockViewController.m
//  gloob
//
//  Created by Pedro on 3/20/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "AvatarUnlockViewController.h"
#define kAvatarUnlockMainButtonText @"MUDAR AVATAR"
#define kBackgroundImageName @"pf_surpresa_bg"
#import "Profile.h"
#import "ProfileManager.h"

@interface AvatarUnlockViewController ()

@end

@implementation AvatarUnlockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (NSString *)textForMainButton {
    return kAvatarUnlockMainButtonText;
}

- (void)mainButtonClickAction:(id)sender {
    NSString *oldAvatar = [ProfileManager sharedManager].currentProfile.currentAvatar.name;
    [Profile currentProfile].currentAvatar = (Avatar *)self.achievement;
    [[ProfileManager sharedManager] saveLatestUsedProfile];
    [[ProfileManager sharedManager] saveProfilesToUserDefaults];
    [[ProfileManager sharedManager] recoverLatestUsedProfile];
    
    NSString *label = [NSString stringWithFormat:@"%@|%@", [oldAvatar slugalize], [self.achievement.name slugalize]];
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Mudar Avatar" andLabel:label];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange object:nil];
    [super didClickClose:sender];
}

- (void)didClickClose:(id)sender {
    [super didClickClose:sender];
}

- (void)didClickMainButton:(id)sender {
    [super didClickMainButton:sender];
}

- (UIImage *)imageForBackground {
    UIImage *image = [UIImage imageNamed:kBackgroundImageName];
    
    if ([UIHelper isIPhone5]) {
        image = [UIImage imageNamed:@"pf_surpresa_bg_wide"];
    }
    
    return image;
}

- (NSString *)textForAchievementMessage {
    return @"um novo avatar";
}

@end
