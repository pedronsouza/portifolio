//
//  ProfileCreatorViewController.h
//  gloob
//
//  Created by zeroum on 13/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "AvatarView.h"
#import "LoginManager.h"
#import "GAHelper.h"

@interface ProfileCreatorViewController : GloobViewController <AvatarViewDelegate, UITextFieldDelegate, UIAlertViewDelegate, GATrackableScreen>

@property (nonatomic, assign) id delegate;

@end

@protocol ProfileCreatorViewControllerDelegate <NSObject>

@optional

- (void)dismissPopUp;
- (void)didFinishProfileCreation;
@end
