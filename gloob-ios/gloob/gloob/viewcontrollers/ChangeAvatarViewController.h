//
//  ChangeAvatarViewController.h
//  gloob
//
//  Created by Pedro on 3/26/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobViewController.h"
#import "AvatarView.h"

@interface ChangeAvatarViewController : GloobViewController<AvatarViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *mainAvatar;
@property (weak, nonatomic) IBOutlet UITextField *lblNomeAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;


- (IBAction)didClickChangeAvatar:(id)sender;
- (IBAction)didClickClose:(id)sender;

@end
