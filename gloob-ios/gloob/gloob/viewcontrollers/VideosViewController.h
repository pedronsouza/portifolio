//
//  VideosViewController.h
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "EpisodeView.h"
#import "EpisodeButton.h"
#import "GAHelper.h"

@interface VideosViewController : GloobViewController <UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, EpisodeViewDelegate, EpisodeButtonDelegate, GATrackableScreen>

@property (nonatomic, assign) id delegate;

@end

@protocol VideosViewControllerDelegate <NSObject>

@optional

- (void)openCharactersViewController;

@end
