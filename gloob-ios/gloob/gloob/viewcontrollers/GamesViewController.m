//
//  GamesViewController.m
//  gloob
//
//  Created by zeroum on 03/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GamesViewController.h"
#import "Game.h"
#import "GameManager.h"
#import "GameInteractionViewController.h"
#import "NSMutableArray+QueueAdditions.h"

@interface GamesViewController ()

@property (nonatomic, strong) NSMutableArray *objects;

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewGames;
@property (weak, nonatomic) IBOutlet UIView *viewGame;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, assign) BOOL isWebViewGameVisible;

@property (nonatomic, strong) GameInteractionViewController *gamesController;

- (void)setupInterface;
- (void)setupContainers;
- (void)setupGamesInContainer:(UIView *)container;
- (void)lowerAllViews;
- (void)setupGamesInIphone;

@end

@implementation GamesViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.objects = [[NSMutableArray alloc] initWithArray:[[GameManager sharedManager] objects]];;
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    [self setupInterface];
}

#pragma mark - Private

- (void)setupInterface {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:32.0f]];
        
        [self setupContainers];
        
        self.gamesController = [[GameInteractionViewController alloc] initWithNibName:@"GameInteractionView"
                                                                               bundle:nil];
        
    } else {
        
        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:26.0f]];
        
        [self addBackgroundIphone];
        [self setupGamesInIphone];
        
    }
}

- (void)setupContainers {
    
    CGSize containerSize = CGSizeMake(1024.0f, 450.0f);
    
    // Set the number of Containers to create (each container can have 5 games)
    NSInteger numberOfContainers = ceilf((float)[self.objects count] / 5.0f);

    CGFloat containerY = 90.0f;
    
    for (NSInteger i = 0; i < numberOfContainers; i++) {
        
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                     containerY,
                                                                     containerSize.width,
                                                                     containerSize.height)];
        
        [container setTag:i];
        
        [self.scrViewGames addSubview:container];

        [self setupGamesInContainer:container];
        
        containerY += containerSize.height;
    }
    
    [self.scrViewGames setContentSize:CGSizeMake(self.view.frame.size.width, 90.0f + containerY)];
}

- (void)setupGamesInContainer:(UIView *)container {
    
    CGSize gameViewSize1 = CGSizeMake(330.0f, 186.0f);
    CGSize gameViewSize2 = CGSizeMake(245.0f, 138.0f);
    
    for (NSInteger i = 0; i < 5; i++) {
        
        if (self.objects.count > 0) {
            
            CGRect gearFrame;
            CGRect gameFrame;
            
            if (i % 5 == 0) {
                
                gearFrame = CGRectMake(48.0f, 337.0f, 143.0f, 200.0f);
                gameFrame = CGRectMake(10.0f, 87.0f, gameViewSize1.width, gameViewSize1.height);
                
            } else if (i % 5 == 1) {
                
                gearFrame = CGRectMake(123.0f, 10.0f, 381.0f, 100.0f);
                gameFrame = CGRectMake(347.0f, 108.0f, gameViewSize2.width, gameViewSize2.height);
                
            } else if (i % 5 == 2) {
                
                gearFrame = CGRectMake(458.0f, 9.0f, 311.0f, 294.0f);
                gameFrame = CGRectMake(768.0f, 0.0f, gameViewSize2.width, gameViewSize2.height);
                
            } else if (i % 5 == 3) {
                
                gearFrame = CGRectMake(894.0f, 138.0f, 4.0f, 76.0f);
                gameFrame = CGRectMake(687.0f, 214.0f, gameViewSize1.width, gameViewSize1.height);
                
            } else {
                
                gearFrame = CGRectMake(423.0f, 290.0f, 264.0f, 85.0f);
                gameFrame = CGRectMake(179.0f, 304.0f, gameViewSize2.width, gameViewSize2.height);
            }
            
            if (i > 0) {
                
                UIImageView *imgView = [[UIImageView alloc] initWithFrame:gearFrame];
                
                [imgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"jg_eng_%li", (long)i]]];
                
                [container addSubview:imgView];
            }
            
            Game *game = [self.objects objectAtIndex:0];

            GameView *view = [[GameView alloc] initWithFrame:gameFrame
                                                        game:game
                                                    delegate:self];
            
            [container addSubview:view];
            
            [self.objects dequeue];
        }
    }
}

- (void)lowerAllViews {
    
    for (UIView *view in self.scrViewGames.subviews) {
        
        [UIView animateWithDuration:0.5f
                         animations:^{
                             [view setCenter:CGPointMake(view.center.x, view.center.y + 590.0f)];
                         }];
    }
    
    [self.scrViewGames setContentSize:CGSizeMake(self.view.frame.size.width,
                                                 self.scrViewGames.contentSize.height + 590.0f)];
}

- (void)setupGamesInIphone {
    
    CGSize thumb = CGSizeMake(160.0f, 90.0f);
    CGFloat x = 60.0f;
    CGFloat y = 60.0f;
    CGFloat space = 0.0f;
    NSInteger i = 0;
    
    if ([UIHelper isIPhone5]) {
        
        space = 22.0f;
        x += space;
        
    }
    
    for (Game *game in self.objects) {
    
        if (i % 2 == 0 && i > 0) {
            
            x = 60.0f + space;
            y += (thumb.height + 20.0f);
            
        } else if (i > 0) {
            x += (thumb.width + 20.0f + space);
        }
        
        CGRect frame = CGRectMake(x, y, thumb.width, thumb.height);

        GameView *view = [[GameView alloc] initWithFrame:frame
                                                    game:game
                                                delegate:self];
        
        [self.scrViewGames addSubview:view];
        
        i++;
    }
    
    [self.scrViewGames setContentSize:CGSizeMake(self.scrViewGames.frame.size.width, y + thumb.height + 20.0f)];
}

- (void)addBackgroundIphone {
    
    CGFloat y = 60.0f;

    NSInteger numberOfBackgrounds = ceilf((float)[self.objects count] / 2.0f);
    
    for (NSInteger i = 0; i < numberOfBackgrounds; i++) {
        
        NSInteger filenumber = i % 3;
        
        NSString *filename = [NSString stringWithFormat:@"jg_eng%li", (long)filenumber];
        
        UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, y, 511.0f, 90.0f)];
        
        [background setImage:[UIImage imageNamed:filename]];
        
        [self.scrViewGames addSubview:background];
        
        y += 110.0f;
    }
}

#pragma mark - Public

- (void)openViewControllerWithGame:(Game *)game {
    [self openGame:game];
}

#pragma mark - GameView Delegate

- (void)openGame:(Game *)game {
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:game.game_file]];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        if (self.isWebViewGameVisible == YES) {
            
            for (UIView *view in self.viewGame.subviews) {
                [view removeFromSuperview];
            }
            
        } else {
            
            self.isWebViewGameVisible = YES;
            
            [self lowerAllViews];
        }
        
        [self.gamesController.view setFrame:CGRectMake(0.0f, 0.0f, 838.0f, 559.0f)];
        [self.gamesController willMoveToParentViewController:self];
        
        [self.viewGame addSubview:self.gamesController.view];
        [self addChildViewController:self.gamesController];
        
        [self.gamesController didMoveToParentViewController:self];
        
        [self.scrViewGames setContentOffset:CGPointZero animated:YES];
        
        [self.gamesController loadRequest:request];
        [self.gamesController setGame:game];
        
    } else {
        
        GameInteractionViewController *controller = [[GameInteractionViewController alloc] initWithNibName:@"GameInteractionViewSD"
                                                                                                    bundle:nil];
        
        [controller setRequest:request];
        [controller setGame:game];
        
        [self presentViewController:controller
                           animated:YES
                         completion:nil];
        
    }
    
    NSString *label = [NSString stringWithFormat:@"jogos|%@", [game.name slugalize]];
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Jogar" andLabel:label];
}


#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return @"/jogos";
}
@end
