//
//  SelfiesListViewController.h
//  gloob
//
//  Created by Pedro on 3/27/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobViewController.h"

@interface SelfiesListViewController : GloobViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andSelfies:(NSArray *)selfies;

- (instancetype) initWithSelfies:(NSArray *) selfies;
- (IBAction)didClickShowSelfieDetails:(id)sender;

@end
