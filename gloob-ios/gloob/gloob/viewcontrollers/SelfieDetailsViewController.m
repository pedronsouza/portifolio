//
//  SelfieDetailsViewController.m
//  gloob
//
//  Created by Pedro on 3/31/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SelfieDetailsViewController.h"
#import "ParentViewController.h"

@interface SelfieDetailsViewController ()
@property (nonatomic, strong) Selfie *selfie;
@end

@implementation SelfieDetailsViewController

- (instancetype)initWithSelfie:(Selfie *)selfie {
    self = [super initWithNibName:[UIHelper deviceViewName:@"SelfieDetailsView"] bundle:nil];
    
    if (self) {
        self.selfie = selfie;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickClose:)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
    
    if ([self.selfie hasAlreadyUnlocked]) {
        self.lblDescripton.text = self.selfie.descriptionOn;
        self.imgSelfie.image = [self.selfie iconForSelfieList];
    } else {
        self.lblDescripton.text = self.selfie.descriptionOff;
        self.imgSelfie.image = [UIImage imageNamed:@"pf_avatar_placeholder01"];
    }
    
    self.lblSubtitle.text = self.selfie.subtitle;
    self.lblTitle.text = self.selfie.title;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.lblDescripton setNumberOfLines:0];
        [self.lblDescripton sizeToFit];
        self.lblSubtitle.numberOfLines = 0;
        [self.lblSubtitle sizeToFit];
    } else {
        if (self.lblSubtitle.text.length >= 32) {
            self.lblSubtitle.numberOfLines = 2;
            CGRect subtitleFrame = self.lblSubtitle.frame;
            subtitleFrame.size.height *= 2;
            subtitleFrame.origin.y -= 5;
            self.lblSubtitle.frame = subtitleFrame;
        }
    }
}

- (void)didClickClose:(id)sender {
    ParentViewController *parent = (ParentViewController *) self.parentViewController;
    [parent dismissPopUp];
}
@end
