//
//  GamesViewController.h
//  gloob
//
//  Created by zeroum on 03/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "GameView.h"
#import "GAHelper.h"

@class Game;

@interface GamesViewController : GloobViewController <UIScrollViewDelegate, GameViewDelegate, GATrackableScreen>

- (void)openViewControllerWithGame:(Game *)game;

@end
