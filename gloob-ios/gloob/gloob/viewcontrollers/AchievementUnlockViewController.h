//
//  AchievementUnlockViewController.h
//  gloob
//
//  Created by Pedro on 3/20/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GloobViewController.h"
#import "Achievement.h"
#import "GamingManager.h"
#import "ParentViewController.h"

@interface AchievementUnlockViewController : GloobViewController
@property (nonatomic, strong) Achievement * achievement;
- (instancetype) initWithAchievement:(Achievement *) achievement;

@property (nonatomic, assign) id delegate;

@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIButton *mainButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtilteLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *lblAchievementMessage;




- (IBAction)didClickClose:(id)sender;
- (IBAction)didClickMainButton:(id)sender;

#pragma mark Abstract Methods
- (NSString *) textForMainButton;
- (void) mainButtonClickAction: (id)sender;
- (UIImage *) imageForBackground;
- (NSString *) textForAchievementMessage;

@end
