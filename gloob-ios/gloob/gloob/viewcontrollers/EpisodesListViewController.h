//
//  EpisodesListViewController.h
//  gloob
//
//  Created by zeroum on 27/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"

@class Program;
@class Episode;

@interface EpisodesListViewController : GloobViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) id delegate;

@property (nonatomic, weak) Program *program;

@end

@protocol EpisodesListViewControllerDelegate <NSObject>

@optional

- (void)playEpisode:(Episode *)episode;

@end
