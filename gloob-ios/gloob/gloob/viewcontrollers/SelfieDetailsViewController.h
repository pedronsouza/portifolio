//
//  SelfieDetailsViewController.h
//  gloob
//
//  Created by Pedro on 3/31/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobViewController.h"
#import "Selfie.h"

@interface SelfieDetailsViewController : GloobViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgSelfie;
@property (weak, nonatomic) IBOutlet UILabel *lblDescripton;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
-(IBAction)didClickClose:(id)sender;
- (instancetype) initWithSelfie:(Selfie *)selfie;

@end
