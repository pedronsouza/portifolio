//
//  ProfileViewController.h
//  gloob
//
//  Created by zeroum on 15/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "ConfigurationsViewController.h"
#import "MyAlertsViewController.h"
#import "ProfileCreatorViewController.h"

@interface ProfileViewController : GloobViewController <ConfigurationsViewControllerDelegate, MyAlertsViewControllerDelegate, ProfileCreatorViewControllerDelegate>

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) BOOL isOpeningFromCharView;

- (void)loadCurrentProfileView;
- (void)loadAlertsView;
- (void)openProfileCreator;
@end

@protocol ProfileViewControllerDelegate <NSObject>

@optional

- (void)dismissProfile;
- (void)showAllCharacters;

@end