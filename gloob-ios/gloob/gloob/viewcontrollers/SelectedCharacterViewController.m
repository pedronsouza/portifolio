//
//  SelectedCharacterViewController.m
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SelectedCharacterViewController.h"
#import "Program.h"
#import "Episode.h"
#import "ColoredButton.h"
#import "UIColor+HexColor.h"
#import "LoadingView.h"
#import "Login.h"
#import "LoginManager.h"
#import "POCViewController.h"
#import "ScheduleAlertManager.h"
#import "ProfileManager.h"
#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"

@interface SelectedCharacterViewController ()

@property (nonatomic, strong) NSMutableArray *objects;

@property (weak, nonatomic) IBOutlet UIView *viewLineSeparator;
@property (nonatomic, weak) UIView *viewAvatarIphone;
@property (nonatomic, weak) UILabel *lblHeaderIphone;

@property (weak, nonatomic) IBOutlet UIView *viewTouch;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAvatar;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (weak, nonatomic) IBOutlet ColoredButton *btnAlert;
@property (weak, nonatomic) IBOutlet UIButton *btnAllAlerts;

@property (weak, nonatomic) IBOutlet UICollectionView *colViewEpisodes;

@property (nonatomic, weak) UINib *cellnib;

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet LoadingView *loadingView;

@property (nonatomic, strong) NSMutableArray *episodesBySeason;

@property (nonatomic, strong) ProfileViewController *profileController;

- (IBAction)closeWindow:(UIButton *)sender;
- (IBAction)showAlertList:(UIButton *)sender;
- (IBAction)showAllAlerts:(UIButton *)sender;

- (void)setupInterface;
- (void)showViewContent;
- (void)dismissView;
- (void)setupAlertButton;

@end

@implementation SelectedCharacterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInterface];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    if (self.program.episodes.count == 0 || self.program.episodes == nil) {
        
        [self.program loadEpisodesWithCompletionHandler:^(BOOL success, NSError *error) {
            
            if (success) {
                
                [self.colViewEpisodes reloadData];
                
                [self.colViewEpisodes setHidden:NO];
                
                [self.loadingView hideLoadingViewWithCompletion:^(BOOL finished) {
                    
                    if (finished) {
                        [self showViewContent];
                    }
                }];
            }
            
            if (error) {
                
                [UIHelper alertViewWithMessage:@"Houve falha ao carregar os dados do servidor. Verifique sua conexão com a internet e tente novamente."
                                      andTitle:@"Ops!"];
                
                [self dismissView];
            }
        }];
        
    } else {
        
        [self.colViewEpisodes setHidden:NO];
        
        [self.loadingView hideLoadingViewWithCompletion:^(BOOL finished) {
            
            if (finished) {
                [self showViewContent];
            }
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setupAlertButton];
}

- (void)dealloc {
    [self.program.episodes removeAllObjects];
}

#pragma mark - Private

- (void)setupInterface {
    UINib *cellnib = [UINib nibWithNibName:[UIHelper deviceViewName:@"EpisodeCell"] bundle:nil];
    
    [self.colViewEpisodes registerNib:cellnib forCellWithReuseIdentifier:@"EpisodeCellId"];
    
    UINib *headerNib = [UINib nibWithNibName:@"EpisodeHeader" bundle:nil];
    
    [self.colViewEpisodes registerNib:headerNib
           forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                  withReuseIdentifier:@"cellHeader"];
    
    [self setupAlertButton];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(dismissView)];
        
        [self.viewTouch addGestureRecognizer:gesture];
        
        // Title
        [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
        [self.lblTitle setText:self.program.title];
        
        // Description
        [self.lblDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:16.0f]];
        [self.lblDescription setText:self.program.prog_description];
        
        // Avatar
        if (self.program.alternative_poster_image) {
            [self.imgViewAvatar sd_setImageWithURL:[NSURL URLWithString:self.program.alternative_poster_image]];
        }
        
        // All Alerts
        [self.btnAllAlerts.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:11.0f]];
        
        [self.btnAlert.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
    } else {
        
        [self.btnAllAlerts.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [self.btnAllAlerts.titleLabel setTextAlignment:NSTextAlignmentCenter];
        
        UINib *descriptionNib = [UINib nibWithNibName:@"DescriptionHeaderSD" bundle:nil];
        
        [self.colViewEpisodes registerNib:descriptionNib
               forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                      withReuseIdentifier:@"descriptionCell"];
    }
}

- (void)showViewContent {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.viewContent setAlpha:1.0f];
                     }];
}

- (void)playEpisode:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from {
    
    Login *login = [LoginManager getSavedLogin];
    
    if (login.access_token && [LoginManager isTokenActive]) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.colViewEpisodes reloadData];
        }
        
        POCViewController *controller = [[POCViewController alloc] initWithNibName:[UIHelper deviceViewName:@"POCView"]
                                                                            bundle:nil];
        
        [controller setEpisode:episode];
        [controller setAccess_token:login.access_token];
        [controller setOpeningFrom:from];
        [controller setArrayVideosSource:list];
        [controller setCurrentIndex:index];
        
        NSString *label = [NSString stringWithFormat:@"personagem|vod|%@||fechado|gloob|%@|%@|%@|%@|%@", @(episode.id_globo_videos),
                           [episode.category slugalize],
                           [episode.program.title slugalize],
                           (episode.season > 0 ? @(episode.season) : @""),
                           (episode.number > 0 ? @(episode.number) : @""),
                           [episode.title slugalize]];
        
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Play" andLabel:label];
        
        [self presentViewController:controller animated:YES completion:nil];
        
        
    } else {
        
        LoginViewController *controller = [[LoginViewController alloc] initWithNibName:[UIHelper deviceViewName:@"LoginView"]
                                                                                bundle:nil];
        
        [controller setDelegate:self];
        [controller setEpisode:episode];
        [controller setWillPlayNow:YES];
        [controller setOpeningFrom:from];
        [controller setArrayVideosSource:list];
        [controller setVideoAtIndex:index];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)dismissView {
    
    if ([self.delegate respondsToSelector:@selector(dismissPopUp)]) {
        [self.delegate dismissPopUp];
    }
}

- (void)setupAlertButton {
    
    if ([[ProfileManager sharedManager] objects].count == 0 && [LoginManager isAuthenticated] == NO) {
        
        [self.btnAlert setHidden:YES];
        [self.btnAllAlerts setHidden:YES];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            [self.imgViewAvatar setFrame:CGRectMake(15.0f, 0.0f, 140.0f, 140.0f)];
            [self.viewLineSeparator setFrame:CGRectMake(1.0f, 135.0f, 872.0f, 1.0f)];
            [self.colViewEpisodes setFrame:CGRectMake(17.0f, 136.0f, 840.0f, 504.0f)];
        } else {
            [self.viewAvatarIphone setFrame:CGRectMake(8.0f, 35.0f, 100.0f, 94.0f)];
            [self.viewLineSeparator setFrame:CGRectMake(0.0f, 129.0f, [UIHelper screenSize].width, 1.0f)];
            [self.lblHeaderIphone setFrame:CGRectMake(8.0f, 130.0f, 464.0f, 36.0f)];
        }
        
    } else {
        
        [self.btnAlert setHidden:NO];
        [self.btnAllAlerts setHidden:NO];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            [self.imgViewAvatar setFrame:CGRectMake(15.0f, 40.0f, 140.0f, 140.0f)];
            [self.viewLineSeparator setFrame:CGRectMake(1.0f, 171.0f, 872.0f, 1.0f)];
            [self.colViewEpisodes setFrame:CGRectMake(17.0f, 172.0f, 840.0f, 469.0f)];
        } else {
            [self.viewAvatarIphone setFrame:CGRectMake(8.0f, 70.0f, 100.0f, 94.0f)];
            [self.viewLineSeparator setFrame:CGRectMake(0.0f, 164.0f, [UIHelper screenSize].width, 1.0f)];
            [self.lblHeaderIphone setFrame:CGRectMake(8.0f, 164.0f, 464.0f, 36.0f)];
        }
        
        if ([ScheduleAlertManager checkIfProgramHaveAtLeastOneAlert:self.program]) {
            
            [self.btnAlert setCustomStyle:@"orange"];
            
            [self.btnAlert setImage:[UIImage imageNamed:@"pf_ico_alarme"]
                           forState:UIControlStateNormal];
            
            [self.btnAlert setImage:[UIImage imageNamed:@"pf_ico_alarme"]
                           forState:UIControlStateHighlighted];
            
            [self.btnAlert setImage:[UIImage imageNamed:@"pf_ico_alarme"]
                           forState:UIControlStateSelected];
            
            [self.btnAlert setTitle:@" REMOVER ALERTA" forState:UIControlStateNormal];
            
            [self.btnAlert awakeFromNib];
            
        } else {
            
            [self.btnAlert setCustomStyle:@"yellow"];
            
            [self.btnAlert setImage:[UIImage imageNamed:@"pf_ico_alarme_roxo"]
                           forState:UIControlStateNormal];
            
            [self.btnAlert setImage:[UIImage imageNamed:@"pf_ico_alarme_roxo"]
                           forState:UIControlStateHighlighted];
            
            [self.btnAlert setImage:[UIImage imageNamed:@"pf_ico_alarme_roxo"]
                           forState:UIControlStateSelected];
            
            [self.btnAlert setTitle:@" ADICIONAR ALERTA" forState:UIControlStateNormal];
            
            [self.btnAlert awakeFromNib];
        }
    }
}

#pragma mark - ProfileViewController Delegate

- (void)dismissProfile {
    [self.profileController willMoveToParentViewController:nil];
    [self.profileController.view removeFromSuperview];
    [self.profileController removeFromParentViewController];
    
    self.profileController = nil;
}

- (void)showAllCharacters {
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
    if ([self.delegate respondsToSelector:@selector(openCharactersView)]) {
        [self.delegate openCharactersView];
    }
}

#pragma mark - ScheduleListViewController Delegate

- (void)dismissScheduleList {
    
    [self setupAlertButton];
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

#pragma mark - EpisodeButton Delegate

- (void)didSelectEpisode:(Episode *)episode atIndexPath:(NSInteger)indexPath inSection:(NSInteger)section {
    
    NSInteger season = [self.program.seasons count] - section;
    
    NSMutableArray *temp_array = [[NSMutableArray alloc] init];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"number"
                                                                   ascending:YES];

    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    temp_array = (NSMutableArray *)[[self.program episodesFromSeasonNumber:season] sortedArrayUsingDescriptors:sortDescriptors];
    
    NSInteger indexOfObject = [temp_array indexOfObject:episode];
    
    if (self.isInsidePlayer) {
        
        if ([self.delegate respondsToSelector:@selector(playSelectedEpisodeInCurrentPlayer:fromList:index:openingFrom:)]) {
            [self.delegate playSelectedEpisodeInCurrentPlayer:episode
                                                     fromList:temp_array
                                                        index:indexOfObject
                                                  openingFrom:[NSString stringWithFormat:@"%@ - TEMP.%li", self.program.title, (long)season]];
        }
        
    } else {
        [self playEpisode:episode
                 fromList:temp_array
                    index:indexOfObject
              openingFrom:[NSString stringWithFormat:@"%@ - TEMP.%li", self.program.title, (long)season]];
    }
}

#pragma mark - UICollectionView DataSource / Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.program.seasons count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    NSInteger index = [self.program.seasons count] - section;
    
    return [[self.program episodesFromSeasonNumber:index] count];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *headerView;
    
    NSInteger season = ([self.program.seasons count] - indexPath.section);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && indexPath.section == 0) {
        
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                        withReuseIdentifier:@"descriptionCell"
                                                               forIndexPath:indexPath];
        
        self.viewAvatarIphone = (UIView *)[headerView viewWithTag:10];
        self.viewLineSeparator = (UIView *)[headerView viewWithTag:11];
        
        UILabel *lblTitle = (UILabel *)[headerView viewWithTag:1];
        
        [lblTitle setText:self.program.title];
        [lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
        [lblTitle setAdjustsFontSizeToFitWidth:YES];
        [lblTitle setMinimumScaleFactor:0.5f];
        
        UILabel *lblDescription = (UILabel *)[headerView viewWithTag:2];
        
        [lblDescription setText:self.program.prog_description];
        [lblDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:16.0f]];
        
        self.lblHeaderIphone = (UILabel *)[headerView viewWithTag:3];
        
        [self.lblHeaderIphone setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:15.0f]];
        [self.lblHeaderIphone setText:[NSString stringWithFormat:@" %liº TEMPORADA", (long)season]];
        [self.lblHeaderIphone setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
        
        UIImageView *icon = (UIImageView *)[headerView viewWithTag:4];

        if (self.program.alternative_poster_image) {
            [icon sd_setImageWithURL:[NSURL URLWithString:self.program.alternative_poster_image]];
        }
        
        UIButton *btnClose = (UIButton *)[headerView viewWithTag:5];
        
        [btnClose addTarget:self
                     action:@selector(closeWindow:)
           forControlEvents:UIControlEventTouchUpInside];
        
        self.btnAlert = (ColoredButton *)[headerView viewWithTag:6];
        
        [self.btnAlert addTarget:self
                               action:@selector(showAlertList:)
                     forControlEvents:UIControlEventTouchUpInside];
        
        self.btnAllAlerts = (UIButton *)[headerView viewWithTag:7];
        
        [self.btnAllAlerts addTarget:self
                              action:@selector(showAllAlerts:)
                    forControlEvents:UIControlEventTouchUpInside];
        
        [self.btnAllAlerts.titleLabel setText:@"VER TODOS OS\nSEUS ALERTAS"];
        [self.btnAllAlerts.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [self.btnAllAlerts.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.btnAllAlerts.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:14.0f]];
        
        [self setupAlertButton];
        
        if ([UIHelper isIPhone5]) {
            [self.btnAllAlerts setFrame:CGRectMake(375.0f,
                                                   self.btnAllAlerts.frame.origin.y,
                                                   self.btnAllAlerts.frame.size.width,
                                                   self.btnAllAlerts.frame.size.height)];
        }
  
    } else {
        
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                  withReuseIdentifier:@"cellHeader"
                                                                                         forIndexPath:indexPath];
        
        UILabel *title = (UILabel *)[headerView viewWithTag:1];
        
        [title setFrame:CGRectMake(0.0f, 0.0f, headerView.frame.size.width, 35.0f)];
        [title setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:15.0f]];
        
        [title setText:[NSString stringWithFormat:@" %liº TEMPORADA", (long)season]];
        [title setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    }

    return headerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGSize size;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        size = CGSizeMake(840.0f, 35.0f);
    } else {
        
        if (section == 0) {
            
            if ([[ProfileManager sharedManager] objects].count == 0 && [LoginManager isAuthenticated] == NO) {
                size = CGSizeMake(self.view.frame.size.width, 168.0f);
            } else {
                size = CGSizeMake(self.view.frame.size.width, 200.0f);
            }
            
            
        } else {
            size = CGSizeMake(self.view.frame.size.width, 35.0f);
        }
    }
    
    return size;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idCell = @"EpisodeCellId";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:idCell
                                                     forIndexPath:indexPath];
    
    NSInteger season = [self.program.seasons count] - indexPath.section;
    
    Episode *episode = [[self.program episodesFromSeasonNumber:season] objectAtIndex:indexPath.row];
    
    EpisodeButton *icon = (EpisodeButton *)[cell viewWithTag:1];
    
    [icon setDelegate:self];
    [icon setEpisode:episode];
    [icon setIndexPath:indexPath.row];
    [icon setSection:indexPath.section];
    [icon.imageView setImage:nil];
    [icon setImage:nil forState:UIControlStateNormal];
    [icon setImage:nil forState:UIControlStateHighlighted];
    
    if (episode.image_cropped == nil) {
        [icon setImage:[UIImage imageNamed:@"placeholder_medium"] forState:UIControlStateNormal];
    } else {
        [icon sd_setImageWithURL:[NSURL URLWithString:episode.image_cropped] forState:UIControlStateNormal];
    }
    
    [icon addTarget];

    UILabel *epNumber = (UILabel *)[cell viewWithTag:3];
    
    [epNumber setText:[NSString stringWithFormat:@"EP.%li", (long)episode.number]];
    [epNumber setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:12.0f]];
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    CGFloat space;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        space = 0.0f;
    } else {
        space = 13.0f;
    }
    
    return space;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    CGFloat space;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        space = 13.0f;
    } else {
        space = 13.0f;
    }
    
    return space;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {

    UIEdgeInsets insets; // top, left, bottom, right
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        insets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        
    } else {
        
        if ([UIHelper isIPhone5]) {
            insets = UIEdgeInsetsMake(0.0f, 20.0f, 20.0f, 20.0f);
        } else {
            insets = UIEdgeInsetsMake(0.0f, 30.0f, 20.0f, 30.0f);
        }
    }

    return insets;
}

#pragma mark - IBActions

- (IBAction)closeWindow:(UIButton *)sender {
    
    [self dismissView];
}

- (IBAction)showAlertList:(UIButton *)sender {
    
    [self.loadingView showLoadingViewWithCompletion:^(BOOL finish) {
        
        if (finish) {
            
            ScheduleAlertManager *manager = [ScheduleAlertManager sharedManager];
            
            [manager loadNextFiveDaysSchedule:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                    [self.loadingView hideLoadingViewWithCompletion:^(BOOL finish) {
                        
                        NSArray *list = (NSArray *)[manager getAllSchedulesForProgramNamed:self.program.title];
                        
                        if (list.count == 0) {
                            
                            NSString *message = [NSString stringWithFormat:@"Nenhuma exibição encontrada para %@ nos próximos 5 dias", self.program.title];
                            
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                            message:message
                                                                           delegate:self
                                                                  cancelButtonTitle:nil
                                                                  otherButtonTitles:@"OK", nil];
                            
                            [alert show];
                            
                        } else {
                            
                            ScheduleListViewController *controller = [[ScheduleListViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ScheduleListView"]
                                                                                                                  bundle:nil];
                            
                            [controller setModalPresentationStyle:UIModalPresentationFormSheet];
                            
                            [controller setDelegate:self];
                            [controller setListOfExibition:list];
                            
                            [self presentViewController:controller
                                               animated:YES
                                             completion:nil];
                            
                        }
                    }];
                }
                
            }];
        }
    }];
}

- (IBAction)showAllAlerts:(UIButton *)sender {
    
    self.profileController = [[ProfileViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ProfileView"]
                                                                     bundle:nil];
    
    [self.profileController setDelegate:self];
    
    [self.profileController setIsOpeningFromCharView:YES];
    
    [self.profileController.view setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self.profileController willMoveToParentViewController:self];
    
    [self.view addSubview:self.profileController.view];
    [self addChildViewController:self.profileController];
    
    [self.profileController didMoveToParentViewController:self];
    
    [self.profileController loadAlertsView];
}

#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return [NSString stringWithFormat:@"/personagens/%@", [self.program.title slugalize]];
}

@end
