    //
//  ParentViewController.m
//  gloob
//
//  Created by zeroum on 07/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ParentViewController.h"
#import "Program.h"
#import "ProgramManager.h"
#import "GameManager.h"
#import "ColoredButton.h"
#import "Login.h"
#import "LoginManager.h"
#import "GameInteractionViewController.h"
#import "AchievementUnlockViewController.h"
#import "Game.h"
#import "GamingManager.h"
#import "EventLog.h"
#import "Profile.h"
#import "ProfileManager.h"
#import "ChangeAvatarViewController.h"
#import "SelfieDetailsViewController.h"
#import "AchievementsFactory.h"
#import "AchievementRepository.h"
#import "SelectProfileViewController.h"

#define kAVCAppID 55
#define kAVCPlatform 5
#define kAVCAppleID @"998350311"

@interface ParentViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewSideMenu;
@property (weak, nonatomic) IBOutlet UIView *viewDark;

@property (weak, nonatomic) IBOutlet UIView *viewLoading;

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *viewMenuButtons;

@property (weak, nonatomic) IBOutlet UIWebView *webViewBanner;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewLoadingBg;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSideMenuBg;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnVideos;
@property (weak, nonatomic) IBOutlet UIButton *btnGames;
@property (weak, nonatomic) IBOutlet UIButton *btnCharacters;
@property (weak, nonatomic) IBOutlet UIButton *btnBusca;

@property (weak, nonatomic) IBOutlet UIButton *btnHome2;
@property (weak, nonatomic) IBOutlet UIButton *btnVideos2;
@property (weak, nonatomic) IBOutlet UIButton *btnGames2;
@property (weak, nonatomic) IBOutlet UIButton *btnCharacters2;
@property (weak, nonatomic) IBOutlet UIButton *btnBusca2;

@property (weak, nonatomic) IBOutlet UIButton *btnProfileAvatar;

@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;

@property (weak, nonatomic) UIViewController *childViewController;
@property (weak, nonatomic) UIViewController *currentPopUp;

@property (nonatomic, strong) PanelViewController *panelController;
@property (nonatomic, strong) GamesViewController *gamesController;
@property (nonatomic, strong) ProfileViewController *profileController;
@property (nonatomic, strong) ProfileCreatorViewController *creatorController;

@property (nonatomic, strong) NSMutableArray *lightboxQueue;
@property (nonatomic, assign) BOOL isQueueInExecution;
@property (nonatomic, assign) BOOL isMenuOpen;

- (void)setupInterface;
- (void)removeChildViewController;
- (void)setMenuButtonAsSelected:(NSInteger)buttonTag;
- (void)didReceivedErrorMessage:(NSError *)error;
- (void)dismissViewLoading;
- (void)showSideMenu;
- (void)hideSideMenu;
- (void)loadBannerView;
- (void)removeBannerView;
- (void)updateScreenWithProfile;
- (void)checkAppUpdate;

@property (weak, nonatomic) IBOutlet UIButton *menuTouchArea;

- (IBAction)retryConnection:(UIButton *)sender;

- (IBAction)openMainView:(UIButton *)sender;
- (IBAction)openVideosView:(UIButton *)sender;
- (IBAction)openGamesView:(UIButton *)sender;
- (IBAction)openCharactersView:(UIButton *)sender;
- (IBAction)openSearchView:(UIButton *)sender;
- (IBAction)toggleShowHideMenu:(UIButton *)sender;

@end

@implementation ParentViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupInterface];
    
    [self checkAppUpdate];
    
    [self loadData];
    
    // Gaming Notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(displayAchievementUnlockPopUp:)
                                                 name:kGammmingShouldExhibitAchievement
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(displayProfileSelectionPopUp:)
                                                name:kLoginWithProfile
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateScreenWithProfile)
                                                 name:kAvatarChange
                                               object:nil];
    
    self.lightboxQueue = [NSMutableArray new];
}

- (void) removeItemFromLightboxQueue:(UIViewController *)object {
    NSMutableArray *lightboxMirror = [[NSMutableArray alloc] initWithArray:[self.lightboxQueue copy]];
    for (UIViewController *controller in self.lightboxQueue) {
        if ([controller isKindOfClass:[object class]]) {
            [lightboxMirror removeObject:controller];
            self.isQueueInExecution = false;
        }
    }
    
    self.lightboxQueue = [[NSMutableArray alloc] initWithArray:lightboxMirror];
    [self processLightboxQueue];
}

- (void) processLightboxQueue {
    if (self.lightboxQueue.count > 0 && !self.isQueueInExecution) {
        self.isQueueInExecution = true;
        [self displayLightboxWithViewController:self.lightboxQueue[0]];
    }
}

- (void) displayLightboxWithViewController:(UIViewController *)controller {
    
    if ([controller respondsToSelector:@selector(setDelegate:)]) {
        [controller performSelector:@selector(setDelegate:) withObject:self];
    }
    
    [self setCurrentPopUp:controller];
    [controller willMoveToParentViewController:self];
    
    if ([UIHelper isIPhone5]) {
        [controller.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, [UIHelper screenSize].height)];
    }
    
    [self.view addSubview:controller.view];
    [self addChildViewController:controller];
    
    [controller didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [controller.view setAlpha:1.0f];
                     }];
}

#pragma mark - AVC

- (void)checkAppUpdate {
    AVC *avc = [[AVC alloc] init];
    [avc setDelegate:self];
    
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    [avc verificaAtualizacaoVersao:[build intValue]
                        plataforma:kAVCPlatform
                         aplicacao:kAVCAppID
                    delegateSender:self];
}

- (void)abreViewAtualizacaoCritica:(NSString*) titulo texto:(NSString*) texto {
    UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:titulo
                                                     message:texto
                                                    delegate:self
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"Atualizar",nil];
    [alerta setTag:1337];
    [alerta show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1337 && buttonIndex != [alertView cancelButtonIndex]) {
        
        NSString *urlAppStore = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@?mt=8", kAVCAppleID];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlAppStore]];
    }
}

#pragma mark - Public

- (void)loadData {
    
    [self.viewLoading setAlpha:1.0f];
    
    ProgramManager *programManager = [ProgramManager sharedManager];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        
        
    } else {
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(hideSideMenu)];
        
        
        [self.viewDark addGestureRecognizer:gesture];
        
        [self.btnHome.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.btnVideos.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.btnGames.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.btnCharacters.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.btnBusca.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.lblProfileName setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:10.0f]];
    }

    [programManager loadProgramsWithCompletionHandler:^(BOOL success, NSError *error) {
        
        if (success) {
            
            [programManager loadFeaturedEpisodesWithCompletionHandler:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                    GameManager *gameManager = [GameManager sharedManager];
                    
                    [gameManager loadGamesWithCompletionHandler:^(BOOL success, NSError *error) {
                        
                        if (success) {
                            
                            [self loadPanelViewController];
                            
                            [self loadBannerView];
                        }
                        
                        if (error) {
                            
                            [self didReceivedErrorMessage:error];

                        }
                    }];
                }
                
                if (error) {
                    
                    [self didReceivedErrorMessage:error];
                }
            }];
        }
        
        if (error) {
            
            [self didReceivedErrorMessage:error];
        }
    }];
}

#pragma mark - Private

- (void)setupInterface {
    CGRect rectForAvatar;
    
    if ([[ProfileManager sharedManager] isSignedInAnyProfile]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            rectForAvatar = CGRectMake(913, 5, 69, 66);
        } else {
            rectForAvatar = CGRectMake(self.btnGames.frame.origin.x, 259, 69, 66);
        }
    } else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            rectForAvatar = CGRectMake(881, 12, 133, 53);
        } else {
            rectForAvatar = CGRectMake(-32, 269, 133, 53);
        }
    }
    
    BOOL shouldChangeText = !([[ProfileManager sharedManager] canCreateMoreProfiles] == YES &&
                              [[ProfileManager sharedManager] isSignedInAnyProfile] == NO);
    Profile *profile = [[ProfileManager sharedManager] currentProfile];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.lblProfileName setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:13.0f]];
    } else {
        if ([UIHelper isIPhone5]) {
            
            [self.imgViewLoadingBg setImage:[UIImage imageNamed:@"bg_loading-568h"]];
            [self.viewContainer setFrame:CGRectMake(self.viewContainer.frame.origin.x,
                                                    self.viewContainer.frame.origin.y,
                                                    self.viewContainer.frame.size.width + 88.0f,
                                                    self.viewContainer.frame.size.height)];
        }
    }
    
    if (shouldChangeText)  {
//        [self.lblProfileName setText:profile.name];
        [self.lblProfileName setText:@"SEU PERFIL"];
        [self.btnProfileAvatar setImage:[profile.currentAvatar iconForHome] forState:UIControlStateNormal];
    } else {
        [self.lblProfileName setText:@"CRIAR PERFIL"];
    }

    [self.btnProfileAvatar setFrame:rectForAvatar];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProfileView:)];
    tap.numberOfTapsRequired = 1;
    
    [self.lblProfileName addGestureRecognizer:tap];
    [self.lblProfileName setUserInteractionEnabled:YES];
    
}

- (void)loadPanelViewController {
    
    self.panelController = [[PanelViewController alloc] initWithNibName:[UIHelper deviceViewName:@"PanelView"]
                                                                 bundle:nil];
    
    [self.panelController setDelegate:self];
    
    [self setChildViewController:self.panelController];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.panelController.view setFrame:CGRectMake(0.0f,
                                                       0.0f,
                                                       [UIHelper screenSize].width,
                                                       675.0f)];
        
    } else {
        
        if ([UIHelper isIPhone5]) {
            
            [self.panelController.view setFrame:CGRectMake(0.0f,
                                                           0.0f,
                                                           self.panelController.view.frame.size.width + 88.0f,
                                                           self.panelController.view.frame.size.height)];
        }
    }
    
    [self.panelController willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:self.panelController.view];
    [self addChildViewController:self.panelController];
    
    [self.panelController didMoveToParentViewController:self];
    
    [self.view sendSubviewToBack:self.viewContainer];
    
    [self setMenuButtonAsSelected:1];
}

- (void)removeChildViewController {
    
    [self.childViewController willMoveToParentViewController:nil];
    [self.childViewController.view removeFromSuperview];
    [self.childViewController removeFromParentViewController];
    
    self.childViewController = nil;
}

- (void)setMenuButtonAsSelected:(NSInteger)buttonTag {
    
    for (UIView *subview in self.viewMenuButtons.subviews) {
        
        if ([subview isKindOfClass:[UIButton class]]) {
            
            UIButton *button = (UIButton *)subview;
            
            if ([button tag] == buttonTag) {
                
                [button setSelected:YES];
                
                if (button.tag != 3) {
                    [button setUserInteractionEnabled:NO];
                }
                
            } else {
                
                [button setSelected:NO];
                [button setUserInteractionEnabled:YES];
                
            }
        }
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self hideSideMenu];
    }
}

- (void)didReceivedErrorMessage:(NSError *)error {
    
    [UIHelper alertViewWithMessage:@"Falha ao carregar as informações do servidor. Verifique sua conexão com a internet e tente novamente."
                          andTitle:@"Ops!"];
    
    UIActivityIndicatorView *spinner = (UIActivityIndicatorView *)[self.viewLoading viewWithTag:1];
    UILabel *label = (UILabel *)[self.viewLoading viewWithTag:2];
    ColoredButton *button = (ColoredButton *)[self.viewLoading viewWithTag:3];
    
    [spinner setHidden:YES];
    [label setHidden:YES];
    [button setHidden:NO];
}

- (void)dismissViewLoading {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.viewLoading setAlpha:0.0f];
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(removeBannerView)
                                    withObject:nil
                                    afterDelay:3.0f];
                     }];
}

- (void)showSideMenu {
    
    self.isMenuOpen = YES;
    
    [self.imgViewSideMenuBg setImage:[UIImage imageNamed:@"hm_bgnav2"]];
    
    [self.viewMenuButtons setFrame:CGRectMake(self.viewMenuButtons.frame.origin.x,
                                              self.viewMenuButtons.frame.origin.y,
                                              123.0f,
                                              self.viewMenuButtons.frame.size.height)];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.viewSideMenu setFrame:CGRectMake(0.0f,
                                                                self.viewSideMenu.frame.origin.y,
                                                                self.viewSideMenu.frame.size.width,
                                                                self.viewSideMenu.frame.size.height)];
                         
                         [self.viewDark setAlpha:1.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.25f
                                          animations:^{
                                              
                                              [self.btnHome2 setAlpha:1.0f];
                                              [self.btnVideos2 setAlpha:1.0f];
                                              [self.btnGames2 setAlpha:1.0f];
                                              [self.btnCharacters2 setAlpha:1.0f];
                                              [self.btnBusca2 setAlpha:1.0f];
                                              
                                              if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                                  [self.lblProfileName setAlpha:1.0f];
                                              }
                                              
                                          }];
                     }];
    
    [self.menuTouchArea setHidden:YES];
}

- (void)hideSideMenu {
    
    self.isMenuOpen = NO;
    
    [self.imgViewSideMenuBg setImage:[UIImage imageNamed:@"hm_bgnav1"]];
    
    [self.viewMenuButtons setFrame:CGRectMake(self.viewMenuButtons.frame.origin.x,
                                              self.viewMenuButtons.frame.origin.y,
                                              65.0f,
                                              self.viewMenuButtons.frame.size.height)];
    
    [UIView animateWithDuration:0.15
                     animations:^{
                         [self.btnHome2 setAlpha:0.0f];
                         [self.btnVideos2 setAlpha:0.0f];
                         [self.btnGames2 setAlpha:0.0f];
                         [self.btnCharacters2 setAlpha:0.0f];
                         [self.btnBusca2 setAlpha:0.0f];
                         [self.viewDark setAlpha:0.0f];
                         
                         if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                             [self.lblProfileName setAlpha:0.0f];
                         }
                     }];
    
    [UIView animateWithDuration:0.25f
                     animations:^{;

                         [self.viewSideMenu setFrame:CGRectMake(-70.0f,
                                                                self.viewSideMenu.frame.origin.y,
                                                                self.viewSideMenu.frame.size.width,
                                                                self.viewSideMenu.frame.size.height)];
                         
                     }];
    
    [self.menuTouchArea setHidden:NO];
}

- (void)loadBannerView {
    
    NSString *url = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        url = @"http://ads.globo.com/RealMedia/ads/adstream_sx.ads/oggsatmobilegloob/ios/gloobplay@x02!x02";
    } else {
        url = @"http://ads.globo.com/RealMedia/ads/adstream_sx.ads/oggsatmobilegloob/ios/smart/gloobplay@x02!x02";
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    [self.webViewBanner loadRequest:request];
}

- (void)removeBannerView {
    [UIView animateWithDuration:2.0f animations:^{
         [self.webViewBanner setAlpha:0.0f];
    } completion:^(BOOL finished) {
        if (finished) {
            [[EventLog sharedInstance] logAccessEvent];
            [[EventLog sharedInstance] verifyAndLogDateEvents];
            
            if ([[ProfileManager sharedManager] isSignedInAnyProfile] == NO) {
                
                [self openProfileCreatorWizard];
            }
        }
    }];
}

- (void)openProfileCreatorWizard {
    
    self.creatorController = [[ProfileCreatorViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ProfileCreatorView"]
                                                                            bundle:nil];
    [self.creatorController.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, [UIHelper screenSize].height)];
    [self.lightboxQueue addObject:self.creatorController];
    [self processLightboxQueue];
}

- (void)updateScreenWithProfile {
    Profile *profile = [[ProfileManager sharedManager] currentProfile];
    CGRect rectForAvatar;
    
    if ([[ProfileManager sharedManager] isSignedInAnyProfile]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            rectForAvatar = CGRectMake(913, 5, 69, 66);
        } else {
            rectForAvatar = CGRectMake(-1, 259, 69, 66);
        }
    } else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            rectForAvatar = CGRectMake(881, 12, 133, 53);
        } else {
            rectForAvatar = CGRectMake(-32, 269, 133, 53);
        }
    }
    
    
    if (profile) {
        [self.btnProfileAvatar setImage:[profile.currentAvatar iconForHome] forState:UIControlStateNormal];
//        [self.lblProfileName setText:profile.name];
        [self.lblProfileName setText:@"SEU PERFIL"];
    } else {
        [self.btnProfileAvatar setImage:[UIImage imageNamed:@"hm_monstros"] forState:UIControlStateNormal];
        [self.lblProfileName setText:@"CRIAR PERFIL"];
    }
    
    [self.btnProfileAvatar setFrame:rectForAvatar];

}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (!webView.isLoading) {
        [self dismissViewLoading];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self dismissViewLoading];
}

#pragma mark - PanelViewController Delegate

- (void)displaySelectedCharacterPopUp:(Program *)character {
    SelectedCharacterViewController *controller = [[SelectedCharacterViewController alloc] initWithNibName:[UIHelper deviceViewName:@"SelectedCharacterView"]
                                                                                                    bundle:nil];
    [controller setDelegate:self];
    [controller setProgram:character];
    [self.lightboxQueue addObject:controller];
    
    [self processLightboxQueue];
    
}

- (void)displayAchievementUnlockPopUp:(NSNotification *) notification {
    Achievement *achievement = (Achievement *) notification.object;
    
    if (![achievement hasAlreadyUnlocked]) {
        AchievementUnlockViewController *controller = (AchievementUnlockViewController *) [achievement viewControllerForAchievementUnlocked];
        
        [self.lightboxQueue addObject:controller];
        [self processLightboxQueue];
    }
}

- (void)displayProfileSelectionPopUp:(NSNotification *) notification {
    void (^completionBlock)() = notification.object;
    
    if (self.currentPopUp) {
        [self removeItemFromLightboxQueue:self.currentPopUp];
        [self.currentPopUp willMoveToParentViewController:nil];
        [self.currentPopUp.view removeFromSuperview];
        [self.currentPopUp removeFromParentViewController];
    }
    
    SelectProfileViewController *controller = [[SelectProfileViewController alloc] init];
    
    if (completionBlock) {
        [controller setCompletionBlock:completionBlock];
    }
    
    [self.lightboxQueue addObject:controller];
    [self processLightboxQueue];
}




- (void)displayGamesViewControllerWithGame:(Game *)game {

    UIButton *button = (UIButton *)[self.viewMenuButtons viewWithTag:3];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self openGamesView:button];
        
        [self.gamesController openViewControllerWithGame:game];
        
    } else {
        
        GameInteractionViewController *controller = [[GameInteractionViewController alloc] initWithNibName:[UIHelper deviceViewName:@"GameInteractionView"]
                                                                                                    bundle:nil];
        [controller setGame:game];
        
        NSString *label = [NSString stringWithFormat:@"home|%@", [game.name slugalize]];
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Jogar" andLabel:label];
        
        [self presentViewController:controller
                           animated:YES
                         completion:^{
                             [controller loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:game.game_file]]];
                         }];
    }
}

#pragma mark - ProfileCreatorViewController Delegate

- (void)didFinishProfileCreation {

    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.creatorController.view setAlpha:0.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             
                             [self.creatorController willMoveToParentViewController:nil];
                             [self.creatorController.view removeFromSuperview];
                             [self.creatorController removeFromParentViewController];
                             [self removeItemFromLightboxQueue:self.creatorController];
                             self.creatorController = nil;
                             Selfie *selfie = [Selfie selfieForProfileCreation];
                             
                             if (![selfie hasAlreadyUnlocked] && [[ProfileManager sharedManager] isSignedInAnyProfile]) {
                                 [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingShouldExhibitAchievement object:selfie];
                                 [[AchievementRepository sharedInstance] addUnlockedAchievementWithAchievement:selfie];
                                 [[EventLog sharedInstance] logAccessEvent];
                                 [[EventLog sharedInstance] verifyAndLogDateEvents];
                             }
                         }
                         
                     }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange
                                                        object:nil];
}

#pragma mark - SelectedCharacterViewController Delegate

- (void)dismissPopUp {
    
    if (self.panelController.isEsteiraPaused == YES) {
        [self.panelController resumeAnimations];
    }
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.currentPopUp.view setAlpha:0.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             if (self.currentPopUp == nil) {
                                 self.currentPopUp = self.lightboxQueue[0];
                             }
                             
                             [self.currentPopUp willMoveToParentViewController:nil];
                             [self.currentPopUp.view removeFromSuperview];
                             [self.currentPopUp removeFromParentViewController];
                             [self removeItemFromLightboxQueue:self.currentPopUp];
                             self.currentPopUp = nil;
                         }
                         
                     }];
}


- (void)dismissPopUpWithCompletionBlock:(void(^)())completionBlock {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                            
                         [self.currentPopUp.view setAlpha:0.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             
                             [self.currentPopUp willMoveToParentViewController:nil];
                             [self.currentPopUp.view removeFromSuperview];
                             [self.currentPopUp removeFromParentViewController];
                             [self removeItemFromLightboxQueue:self.currentPopUp];
                             self.currentPopUp = nil;
                             completionBlock();
                         }
                         
                     }];
}

- (void) displayChangeAvatar {
    ChangeAvatarViewController *controller = [[ChangeAvatarViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ChangeAvatarView"]
                                                                                          bundle:nil];
    [self.lightboxQueue addObject:controller];
    [self processLightboxQueue];
}

- (void) displaySelfieDetails:(Selfie *)selfie {
    SelfieDetailsViewController *controller = [[SelfieDetailsViewController alloc] initWithSelfie:selfie];
    
    [self.lightboxQueue addObject:controller];
    [self processLightboxQueue];
}

- (void)showAllCharacters {
    
    [self dismissProfile];
    
    [self openCharactersView:nil];
}

#pragma mark - VideosViewController & SearchViewController delegate

- (void)openCharactersViewController {
    
    UIButton *button = [[UIButton alloc] init];
    
    [button setTag:4];
    
    [self openCharactersView:button];
}

#pragma mark - ProfileViewController Delegate

- (void)dismissProfile {
    [self.profileController willMoveToParentViewController:nil];
    [self.profileController.view removeFromSuperview];
    [self.profileController removeFromParentViewController];
    
    self.profileController = nil;
}

#pragma mark - IBActions

- (IBAction)retryConnection:(UIButton *)sender {
    
    UIActivityIndicatorView *spinner = (UIActivityIndicatorView *)[self.viewLoading viewWithTag:1];
    UILabel *label = (UILabel *)[self.viewLoading viewWithTag:2];
    ColoredButton *button = (ColoredButton *)[self.viewLoading viewWithTag:3];
    
    [spinner setHidden:NO];
    [label setHidden:NO];
    [button setHidden:YES];
    
    [self.panelController retryConnection];
}

- (IBAction)openMainView:(UIButton *)sender {
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }
    
    [self loadPanelViewController];
}

- (IBAction)openVideosView:(UIButton *)sender {
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }

    [self setMenuButtonAsSelected:sender.tag];
    
    VideosViewController *controller = [[VideosViewController alloc] initWithNibName:[UIHelper deviceViewName:@"VideosView"]
                                                                              bundle:nil];
    
    [controller setDelegate:self];
    
    [self setChildViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [controller.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, self.viewContainer.frame.size.height)];
        
    } else {
        
        if ([UIHelper isIPhone5]) {
            
            [controller.view setFrame:CGRectMake(0.0f, 0.0f, controller.view.frame.size.width + 88.0f, self.viewContainer.frame.size.height)];
            
        }
    }
    
    [controller willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:controller.view];
    [self addChildViewController:controller];
    
    [controller didMoveToParentViewController:self];
}

- (IBAction)openGamesView:(UIButton *)sender {
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }
    
    [self setMenuButtonAsSelected:3];
    
    self.gamesController = [[GamesViewController alloc] initWithNibName:[UIHelper deviceViewName:@"GamesView"]
                                                                 bundle:nil];
    
    [self setChildViewController:self.gamesController];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.gamesController.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, self.viewContainer.frame.size.height)];
        
    } else {
        
        if ([UIHelper isIPhone5]) {
            
            [self.gamesController.view setFrame:CGRectMake(0.0f, 0.0f, self.gamesController.view.frame.size.width + 88.0f, self.viewContainer.frame.size.height)];
            
        }
    }
    
    [self.gamesController willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:self.gamesController.view];
    [self addChildViewController:self.gamesController];
    
    [self.gamesController didMoveToParentViewController:self];
}

- (IBAction)openCharactersView:(UIButton *)sender {
    
    [self setMenuButtonAsSelected:4];
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }
    
    CharactersViewController *controller = [[CharactersViewController alloc] initWithNibName:[UIHelper deviceViewName:@"CharactersView"]
                                                                                      bundle:nil];
    
    [controller setDelegate:self];
    
    [self setChildViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [controller.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, self.viewContainer.frame.size.height)];
        
    } else {
        
        if ([UIHelper isIPhone5]) {
            
            [controller.view setFrame:CGRectMake(0.0f, 0.0f, controller.view.frame.size.width + 88.0f, self.viewContainer.frame.size.height)];
            
        }
    }
    
    [controller willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:controller.view];
    [self addChildViewController:controller];
    
    [controller didMoveToParentViewController:self];
}

- (IBAction)openSearchView:(UIButton *)sender {
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }

    [self setMenuButtonAsSelected:sender.tag];
    
    SearchViewController *controller = [[SearchViewController alloc] initWithNibName:[UIHelper deviceViewName:@"SearchView"]
                                                                              bundle:nil];
    
    [controller setDelegate:self];
    
    [self setChildViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [controller.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, self.viewContainer.frame.size.height)];
        
    } else {
        
        if ([UIHelper isIPhone5]) {
            
            [controller.view setFrame:CGRectMake(0.0f, 0.0f, controller.view.frame.size.width + 88.0f, self.viewContainer.frame.size.height)];
            
        }
    }

    [controller willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:controller.view];
    [self addChildViewController:controller];
    
    [controller didMoveToParentViewController:self];
}

- (IBAction)openProfileView:(UIButton *)sender {
    NSArray *childs = [[NSArray alloc] initWithArray:self.childViewControllers];
    bool isProfileAlreadyInStack = false;
    for (int i =0; i < childs.count; i++) {
        UIViewController *controller = childs[i];
        if ([controller isKindOfClass:[ProfileViewController class]]) {
            isProfileAlreadyInStack = true;
            break;
        }
    }
    
    if (isProfileAlreadyInStack) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowProfileSelfies object:nil];
        return;
    }
    
    if ([[ProfileManager sharedManager] isSignedInAnyProfile] == YES) {
        // Se o game estiver ativo, fecha o game ao abrir o menu
        if ([self.childViewController isKindOfClass:[GamesViewController class]]) {
            [self openGamesView:nil];
        }
        
        self.profileController = [[ProfileViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ProfileView"]
                                                                         bundle:nil];
        
        [self.profileController setDelegate:self];
        
        [self.profileController.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, [UIHelper screenSize].height)];
        
        [self.profileController willMoveToParentViewController:self];
        
        [self addChildViewController:self.profileController];
        [self.view addSubview:self.profileController.view];
        [self.profileController didMoveToParentViewController:self];
        
        [self.profileController loadCurrentProfileView];
        
    } else if ([[ProfileManager sharedManager] isSignedInAnyProfile] == NO ) {
        
        [self openProfileCreatorWizard];
    }
}

- (IBAction)toggleShowHideMenu:(UIButton *)sender {
    
    if (self.isMenuOpen == YES) {
        [self hideSideMenu];
    } else {
        [self showSideMenu];
    }
}
@end
