//
//  CharactersViewController.h
//  gloob
//
//  Created by zeroum on 08/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "CharacterView.h"
#import "GAHelper.h"

@interface CharactersViewController : GloobViewController <CharacterViewDelegate, GATrackableScreen>

@property (nonatomic, assign) id delegate;

@end
