//
//  PlayerViewController.h
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "GAHelper.h"
#import "GloobViewController.h"
#import "AuditudeView.h"
#import "AuditudeViewDelegateProtocol.h"
#import "AuditudeAdPlaybackSession.h"
#import "AUDBanner.h"
#import "AUDExternalViewSource.h"

@class Episode;

@interface PlayerViewController : GloobViewController <WebmediaPlayerEventListener, UIAlertViewDelegate, GATrackableScreen, AuditudeViewDelegate>

@property (nonatomic, strong) NSString *access_token;

@property (nonatomic, weak) Episode *episode;

@property (nonatomic, strong) id delegate;

@property (nonatomic, assign) BOOL isPlayingFavorites;

@property (nonatomic, assign) BOOL isSizeCorrectionEnabled;

@property (nonatomic, assign) BOOL shouldSkipAuditude;

@property (nonatomic, strong) WebmediaPlayer *player;

// Auditude
@property (nonatomic, strong) AuditudeView *auditudeView;
@property (nonatomic, strong) AuditudeAdPlaybackSession *session;
@property (nonatomic, strong) AUDExternalViewSource *viewSource;
@property (nonatomic, strong) UIView *bannerView;
@property (nonatomic, strong) UIButton *clickButton;

@property (nonatomic, weak) NSTimer *playHeadTimer;
@property (nonatomic, assign) NSTimeInterval playHeadTime;

@property (nonatomic, assign) BOOL useButtonForAdClick;
@property (nonatomic, assign) BOOL listenersAdded;
@property (nonatomic, assign) BOOL isMainVideoPlaying;
@property (nonatomic, assign) BOOL isPostRoll;
@property (nonatomic, assign) BOOL skipBreak;

@property (nonatomic, strong) UIViewController *viewController;

- (void)startPlayback;
- (void)removeAllObservers;

@end

@protocol PlayerViewControllerDelegate <NSObject>

@optional

- (void)playNextVideoInPlaylist;
- (void)dismissVideoPlayer;
- (void)showViewButtons;

@end
