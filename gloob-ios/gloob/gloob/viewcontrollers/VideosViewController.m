//
//  VideosViewController.m
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "VideosViewController.h"
#import "Episode.h"
#import "ProgramManager.h"
#import "Login.h"
#import "LoginManager.h"
#import "LoginViewController.h"
#import "POCViewController.h"
#import "ColoredButton.h"
#import "LoadingView.h"
#import "UIButton+WebCache.h"

@interface VideosViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrView;
@property (weak, nonatomic) IBOutlet UICollectionView *colViewVideos;
@property (weak, nonatomic) IBOutlet UIView *viewPodium;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPodium;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCharacters;

@property (weak, nonatomic) IBOutlet LoadingView *viewLoading;

@property (weak, nonatomic) IBOutlet UILabel *lblText1;
    
@property (weak, nonatomic) IBOutlet UILabel *lblMostSeen;
@property (weak, nonatomic) IBOutlet UILabel *lblNewest;

@property (weak, nonatomic) IBOutlet ColoredButton *btnShowCharacters;

- (IBAction)showCharacters:(UIButton *)sender;

- (void)setupInterface;
- (void)setupIphone5;
- (void)setupTop5;
- (void)showInterface;

@end

@implementation VideosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInterface];
    
    ProgramManager *manager = [ProgramManager sharedManager];
    
    [manager loadTopFiveWithCompletionHandler:^(BOOL success, NSError *error) {
        
        if (success) {
            
            [self setupTop5];
            
            [self.viewLoading hideLoadingViewWithCompletion:nil];
            
            [self showInterface];
        }
        
        if (error) {
            
            [UIHelper alertViewWithMessage:@"Falha ao carregar as informações do servidor. Verifique sua conexão com a internet e tente novamente."
                                  andTitle:@"Ops!"];
        }
        
    }];
    
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
}

#pragma mark - Private

- (void)setupInterface {
    
    CGFloat fontSize;
    CGSize scrSize;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        fontSize = 32.0f;
        scrSize = CGSizeMake(1024.0f, 1850.0f);
        
    } else {
        
        fontSize = 26.0f;
        
        if ([UIHelper isIPhone5]) {
            
            [self setupIphone5];
            
            scrSize = CGSizeMake(self.scrView.frame.size.width, 2000.0f);
            
        } else {
            
            scrSize = CGSizeMake(self.scrView.frame.size.width, 1725.0f);
        }
    }
    
    // Label Titles
    [self.lblMostSeen setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:fontSize]];
    [self.lblNewest setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:fontSize]];
    
    // Other labels
    [self.lblText1 setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
    
    // Button
    [self.btnShowCharacters.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
    
    // CollectionView
    UINib *cellNib = [UINib nibWithNibName:[UIHelper deviceViewName:@"EpisodeCell"] bundle:nil];
    
    [self.colViewVideos registerNib:cellNib
         forCellWithReuseIdentifier:@"EpisodeCellId"];

    [self.scrView setContentSize:scrSize];
}

- (void)setupIphone5 {
    
    [self.imgViewCharacters setCenter:CGPointMake(self.imgViewCharacters.center.x + 40.0f, self.imgViewCharacters.center.y)];
    
    [self.btnShowCharacters setCenter:CGPointMake(self.btnShowCharacters.center.x + 70.0f, self.btnShowCharacters.center.y)];
    
    [self.colViewVideos setFrame:CGRectMake(self.colViewVideos.frame.origin.x,
                                            self.colViewVideos.frame.origin.y,
                                            self.colViewVideos.frame.size.width,
                                            self.colViewVideos.frame.size.height + 600.0f)];
    
    [self.viewPodium setFrame:CGRectMake(-20.0f,
                                         self.viewPodium.frame.origin.y,
                                         450.0f,
                                         self.viewPodium.frame.size.height)];
    
    [self.imgViewPodium setImage:[UIImage imageNamed:@"vd_podium_wide"]];
    
    [self.imgViewPodium setFrame:CGRectMake(self.imgViewPodium.frame.origin.x,
                                            self.imgViewPodium.frame.origin.y,
                                            450.0f,
                                            69.0f)];
    
    UIView *view1 = (UIView *)[self.viewPodium viewWithTag:1];
    
    [view1 setCenter:CGPointMake(view1.center.x + 22.0f, view1.center.y - 5.0f)];
    
    UIView *view2 = (UIView *)[self.viewPodium viewWithTag:2];
    
    [view2 setCenter:CGPointMake(view2.center.x + 11.0f, view2.center.y - 2.0f)];
    
    UIView *view3 = (UIView *)[self.viewPodium viewWithTag:3];
    
    [view3 setCenter:CGPointMake(view3.center.x + 35.0f, view3.center.y - 2.0f)];
    
    UIView *view4 = (UIView *)[self.viewPodium viewWithTag:4];
    
    [view4 setCenter:CGPointMake(view4.center.x, view4.center.y)];
    
    UIView *view5 = (UIView *)[self.viewPodium viewWithTag:5];
    
    [view5 setCenter:CGPointMake(view5.center.x + 46.0f, view5.center.y + 1.0f)];
}

- (void)setupTop5 {
    
    NSInteger i = 1;
    
    for (Episode *episode in [[ProgramManager sharedManager] objectsTopFive]) {

        CGRect frame;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            frame = CGRectMake(0.0f, 0.0f, 160.0f, 90.0f);
        } else {
            
            if ([UIHelper isIPhone5]) {
                frame = CGRectMake(0.0f, 0.0f, 84.0f, 47.0f);
            } else {
                frame = CGRectMake(0.0f, 0.0f, 72.0f, 41.0f);
            }
        }

        UIView *view = (UIView *)[self.viewPodium viewWithTag:i];
        
        EpisodeView *episodeView = [[EpisodeView alloc] initWithFrame:frame
                                                              episode:episode
                                                             delegate:self];
        
        [episodeView setVideosList:(NSMutableArray *)[[ProgramManager sharedManager] objectsTopFive]];
        [episodeView setVideoIndex:i - 1];
        [episodeView setOpeningFrom:@"VIDEOS MAIS VISTOS"];
        
        [view addSubview:episodeView];
        
        i++;
    }
}

- (void)showInterface {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.scrView setAlpha:1.0f];
                     }];
}

- (void)playEpisode:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from {
    
    Login *login = [LoginManager getSavedLogin];
    
    if (login.access_token && [LoginManager isTokenActive]) {
        
        POCViewController *controller = [[POCViewController alloc] initWithNibName:[UIHelper deviceViewName:@"POCView"]
                                                                            bundle:nil];
        
        [controller setEpisode:episode];
        [controller setAccess_token:login.access_token];
        [controller setArrayVideosSource:list];
        [controller setCurrentIndex:index];
        [controller setOpeningFrom:from];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    } else {
        
        LoginViewController *controller = [[LoginViewController alloc] initWithNibName:[UIHelper deviceViewName:@"LoginView"]
                                                                                bundle:nil];
        
        [controller setDelegate:self];
        [controller setEpisode:episode];
        [controller setWillPlayNow:YES];
        [controller setArrayVideosSource:list];
        [controller setVideoAtIndex:index];
        [controller setOpeningFrom:from];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - EpisodeButton Delegate

- (void)didSelectEpisode:(Episode *)episode atIndexPath:(NSInteger)indexPath inSection:(NSInteger)section {
    
    [self playEpisode:episode
             fromList:[[ProgramManager sharedManager] objectsFeaturedEpisode]
                index:indexPath
          openingFrom:@"VIDEOS MAIS NOVOS"];
    
    NSString *label = [NSString stringWithFormat:@"videos|vod|%@||fechado|gloob|%@|%@|%@|%@|%@", @(episode.id_globo_videos),
                       [episode.category slugalize],
                       [episode.program.title slugalize],
                       (episode.season > 0 ? @(episode.season) : @""),
                       (episode.number > 0 ? @(episode.number) : @""),
                       [episode.title slugalize]];
    
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Play" andLabel:label];
}

#pragma mark - UICollectionView DataSource / Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[[ProgramManager sharedManager] objectsFeaturedEpisode] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EpisodeCellId"
                                                                           forIndexPath:indexPath];
    
    Episode *episode = [[[ProgramManager sharedManager] objectsFeaturedEpisode] objectAtIndex:indexPath.row];
    
    EpisodeButton *icon = (EpisodeButton *)[cell viewWithTag:1];
    
    [icon setDelegate:self];
    [icon setEpisode:episode];
    [icon setIndexPath:indexPath.row];
    [icon setSection:indexPath.section];
    
    [icon addTarget];
    
    UIImageView *play = (UIImageView *)[cell viewWithTag:2];
    UILabel *epNumber = (UILabel *)[cell viewWithTag:3];
    
    [epNumber setText:[NSString stringWithFormat:@"EP.%li", (long)episode.number]];
    [epNumber setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:12.0f]];
    [play setHidden:NO];
    [epNumber setHidden:NO];
    
    if (episode.image_cropped == nil) {
        [icon setImage:[UIImage imageNamed:@"placeholder_medium"] forState:UIControlStateNormal];
    } else {
        [icon sd_setImageWithURL:[NSURL URLWithString:episode.image_cropped] forState:UIControlStateNormal];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    CGSize size;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        size = CGSizeMake(160.0f, 90.0f);
    } else {
        
        if ([UIHelper isIPhone5]) {
            size = CGSizeMake(144.0f, 81.0f);
        } else {
            size = CGSizeMake(120.0f, 67.0f);
        }
    }
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    CGFloat space;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        space = 0.0f;
    } else {
        
        if ([UIHelper isIPhone5]) {
            space = 5.0f;
        } else {
            space = 5.0f;
        }
        
    }
    
    return space;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    CGFloat space;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        space = 13.0f;
    } else {
        
        if ([UIHelper isIPhone5]) {
            space = 5.0f;
        } else {
            space = 5.0f;
        }
    }
    
    return space;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    UIEdgeInsets insets; // top, left, bottom, right
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        insets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        
    } else {
        
        if ([UIHelper isIPhone5]) {
            insets = UIEdgeInsetsMake(0.0f, 20.0f, 5.0f, 20.0f);
        } else {
            insets = UIEdgeInsetsMake(0.0f, 15.0f, 5.0f, 15.0f);
        }
    }
    
    return insets;
}

#pragma mark - IBActions

- (IBAction)showCharacters:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(openCharactersViewController)]) {
        [self.delegate openCharactersViewController];
    }
}

#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return @"/videos";
}

@end
