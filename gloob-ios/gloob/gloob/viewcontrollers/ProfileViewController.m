//
//  ProfileViewController.m
//  gloob
//
//  Created by zeroum on 15/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ProfileViewController.h"
#import "CurrentProfileViewController.h"
#import "Selfie.h"
#import "ProfileManager.h"
#import "GamingManager.h"
#import "EventLog.h"
#import "ParentViewController.h"

@interface ProfileViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *viewPopUp;

@property (weak, nonatomic) IBOutlet UILabel *lblBack;

@property (weak, nonatomic) IBOutlet UIButton *btnProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnAlerts;
@property (weak, nonatomic) IBOutlet UIButton *btnConfiguration;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeProfile;

@property (nonatomic, weak) UIViewController *childViewController;
@property (weak, nonatomic) UIViewController *currentPopUp;

@property (nonatomic, strong) ProfileCreatorViewController *creatorController;
@property (nonatomic, strong) ConfigurationsViewController *configsController;

- (IBAction)closeWindow:(UIButton *)sender;
- (IBAction)showCurrentProfile:(UIButton *)sender;
- (IBAction)showAlerts:(UIButton *)sender;
- (IBAction)showConfigurations:(UIButton *)sender;


- (void)setupInterface;
- (void)setButtonAsSelected:(UIButton *)sender;
- (void)removeChildViewController;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSelfies:) name:kShowProfileSelfies object:nil];
    [self setupInterface];
}

#pragma mark - Private

- (void) showSelfies:(NSNotification *) notification {
    [self showCurrentProfile:self.btnProfile];
}

- (void)setupInterface {
    
    // Label Voltar
    [self.lblBack setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:11.0f]];
}

- (void)setButtonAsSelected:(UIButton *)sender {
    
    for (UIView *view in self.viewHeader.subviews) {
        
        if ([view isKindOfClass:[UIButton class]]) {
            
            UIButton *button = (UIButton *)view;
            
            if (button.tag == sender.tag) {
                
                [button setSelected:YES];
                [button setUserInteractionEnabled:NO];
                
            } else {
                
                [button setSelected:NO];
                [button setUserInteractionEnabled:YES];
                
            }
        }
    }
}

- (void)removeChildViewController {
    
    [self.childViewController willMoveToParentViewController:nil];
    [self.childViewController.view removeFromSuperview];
    [self.childViewController removeFromParentViewController];
}

#pragma mark - Public

- (void)loadCurrentProfileView {
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }
    
    CurrentProfileViewController *controller = [[CurrentProfileViewController alloc] initWithNibName:[UIHelper deviceViewName:@"CurrentProfileView"]
                                                                                              bundle:nil];
    
    [self setChildViewController:controller];
    
    [controller.view setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.viewContainer.frame.size.height)];
    
    [controller willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:controller.view];
    [self addChildViewController:controller];
    
    [controller didMoveToParentViewController:self];
}

- (void)loadAlertsView {
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }
    
    UIButton *sender = [[UIButton alloc] init];
    
    [sender setTag:2];
    
    [self setButtonAsSelected:sender];
    
    MyAlertsViewController *controller = [[MyAlertsViewController alloc] initWithNibName:[UIHelper deviceViewName:@"MyAlertsView"]
                                                                                  bundle:nil];
    
    [self setChildViewController:controller];
    
    [controller setDelegate:self];
    
    if (self.isOpeningFromCharView == YES) {
        [controller setIsOpeningFromCharView:YES];
    }
    
    [controller.view setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.viewContainer.frame.size.height)];
    
    [controller willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:controller.view];
    [self addChildViewController:controller];
    
    [controller didMoveToParentViewController:self];
}

#pragma mark - MyAlertsViewController Delegate

- (void)showAllCharacters {
    
    if ([self.delegate respondsToSelector:@selector(showAllCharacters)]) {
        [self.delegate showAllCharacters];
    }
}

#pragma mark - NewProfileViewController delegate

- (void)dismissPopUp {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.viewPopUp setAlpha:0.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             
                             [self.currentPopUp willMoveToParentViewController:nil];
                             [self.currentPopUp.view removeFromSuperview];
                             [self.currentPopUp removeFromParentViewController];
                             
                             self.currentPopUp = nil;
                         }
                         
                     }];
}

#pragma mark - ProfileCreatorViewController Delegate

- (void)didFinishProfileCreation {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.creatorController.view setAlpha:0.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             
                             [self.creatorController willMoveToParentViewController:nil];
                             [self.creatorController.view removeFromSuperview];
                             [self.creatorController removeFromParentViewController];
                             
                             self.creatorController = nil;
                             [self.configsController updateProfiles];
                             Selfie *selfie = [Selfie selfieForProfileCreation];
                             
                             if (![selfie hasAlreadyUnlocked] && [[ProfileManager sharedManager] isSignedInAnyProfile]) {
                                 [[EventLog sharedInstance] verifyAndLogDateEvents];
                                 [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingShouldExhibitAchievement object:selfie];
                             }
                         }
                         
                     }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange
                                                        object:nil];
}

#pragma mark - ConfigurationsViewController Delegate

- (void)openProfileCreator {
    
    self.creatorController = [[ProfileCreatorViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ProfileCreatorView"]
                                                                            bundle:nil];
    
    [self.creatorController setDelegate:self];
    
    [self.creatorController.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, [UIHelper screenSize].height)];
    
    [self.creatorController willMoveToParentViewController:self];
    
    [self.view addSubview:self.creatorController.view];
    [self addChildViewController:self.creatorController];
    
    [self.creatorController didMoveToParentViewController:self];
}

#pragma mark - IBActions

- (IBAction)closeWindow:(UIButton *)sender {
    ParentViewController *parent = (ParentViewController *) self.parentViewController;
    if ([parent respondsToSelector:@selector(dismissProfile)]) {
        [self willMoveToParentViewController:self.parentViewController];
        [self.view removeFromSuperview];
        [self removeChildViewController];
        [self removeFromParentViewController];
        [parent dismissProfile];
    }
}

- (IBAction)showCurrentProfile:(UIButton *)sender {

    [self setButtonAsSelected:sender];
    
    [self loadCurrentProfileView];
}

- (IBAction)showAlerts:(UIButton *)sender {
    
    [self loadAlertsView];
}

- (IBAction)showConfigurations:(UIButton *)sender {
    
    if (self.childViewController != nil) {
        [self removeChildViewController];
    }
    
    [self setButtonAsSelected:sender];
    
    self.configsController = [[ConfigurationsViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ConfigurationView"]
                                                                       bundle:nil];
    
    [self.configsController setDelegate:self];
    
    [self setChildViewController:self.configsController];
    
    [self.configsController.view setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.viewContainer.frame.size.height)];
    
    [self.configsController willMoveToParentViewController:self];
    
    [self.viewContainer addSubview:self.configsController.view];
    [self addChildViewController:self.configsController];
    
    [self.configsController didMoveToParentViewController:self];
}

@end
