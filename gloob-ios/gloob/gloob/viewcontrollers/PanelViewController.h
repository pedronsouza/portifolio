//
//  PanelViewController.h
//  gloob
//
//  Created by zeroum on 12/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "EpisodeView.h"
#import "PanelGameView.h"
#import "GAHelper.h"
#import "EsteiraItemView.h"

@class Program;

@interface PanelViewController : GloobViewController <EpisodeViewDelegate, PanelGameViewDelegate, UIScrollViewDelegate, GATrackableScreen, EsteiraItemViewDelegate>

@property (nonatomic, assign) id delegate;

@property (nonatomic, assign) BOOL isEsteiraPaused;

- (void)retryConnection;
- (void)resumeAnimations;
- (void)pauseAnimations;

@end

@protocol PanelViewControllerDelegate <NSObject>

@optional

- (void)displaySelectedCharacterPopUp:(Program *)character;
- (void)displayGamesViewControllerWithGame:(Game *)game;

@end

