//
//  POCViewController.m
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "POCViewController.h"
#import "PlayerViewController.h"
#import "Episode.h"
#import "Game.h"
#import "GameManager.h"
#import "EpisodeView.h"
#import "ColoredButton.h"
#import "UIImageView+WebCache.h"

@interface POCViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewGames;
@property (weak, nonatomic) IBOutlet UIView *viewCarousel;

@property (strong, nonatomic) iCarousel *carouselFavorites;

@property (weak, nonatomic) IBOutlet UIView *viewButtons;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UIView *viewGames;
@property (weak, nonatomic) IBOutlet UIView *viewFavoriteVideos;

@property (weak, nonatomic) IBOutlet UIButton *btnBookmark;
@property (weak, nonatomic) IBOutlet UIButton *btnAbout;
@property (weak, nonatomic) IBOutlet UIButton *btnGames;
@property (weak, nonatomic) IBOutlet UIButton *btnVoltar;

@property (weak, nonatomic) IBOutlet UILabel *lblEpisodeNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblEpisodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblEpisodeDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGames;
@property (weak, nonatomic) IBOutlet UILabel *lblFavorites;

@property (weak, nonatomic) IBOutlet UILabel *lblFavoriteProgram;
@property (weak, nonatomic) IBOutlet UILabel *lblFavoriteTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFavoriteInfo;

@property (weak, nonatomic) IBOutlet UILabel *lblNoFavorites;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewCharacter;

@property (nonatomic, assign) BOOL isSizeCorrectionEnabled;

@property (nonatomic, assign) BOOL shouldSkipAuditude;

@property (nonatomic, weak) NSTimer *timerViewButtons;

@property (nonatomic, weak) Program *currentProgram;

@property (nonatomic, strong) PlayerViewController *playerController;
@property (nonatomic, strong) SelectedCharacterViewController *characterController;
@property (nonatomic, strong) GameInteractionViewController *gameController;

- (IBAction)favoriteThisVideo:(UIButton *)sender;
- (IBAction)showVideoInfo:(UIButton *)sender;
- (IBAction)showGames:(UIButton *)sender;
- (IBAction)showCharacterEpisodes:(UIButton *)sender;
- (IBAction)closeWindow:(UIButton *)sender;
- (IBAction)closeGamesView:(UIButton *)sender;

- (void)setupInterface;
- (void)setupEpisodeInfo;
- (void)setupFavoritesCarousel;
- (void)setupVisibleInformationInViewFavorites;

- (void)setupNotifications;
- (void)setupPlayerWithEpisode:(Episode *)episode;
- (void)setupViewGames;

- (void)showViewButtons;
- (void)hideViewButtons;

- (void)showViewGames;
- (void)hideViewGames;
- (void)lowerAllViews;
- (void)setFavoriteButtonState;

- (void)notificationShowButtonsMenuPlayer:(NSNotification*)notificationName;
- (void)notificationShowButtonsMenuPlayerWithTime:(NSNotification*)notificationName;

@end

@implementation POCViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupInterface];
    [self setupEpisodeInfo];
    [self setupFavoritesCarousel];
    [self setupNotifications];
    [self setupPlayerWithEpisode:self.episode];
    [self setupViewGames];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.btnVoltar.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:17.0f]];
    
    [self.scrView setContentSize:CGSizeMake(self.scrView.frame.size.width,
                                            self.scrView.frame.size.height + self.viewBottom.frame.size.height)];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"ShowButtonsMenuPlayer"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"ShowButtonsMenuPlayerWithTime"
                                                  object:nil];
}

#pragma mark - Public

- (void)hideCarouselView {
    [self.viewFavoriteVideos setHidden:YES];
    [self.lblFavorites setHidden:YES];
    [self.lblNoFavorites setHidden:YES];
}

#pragma mark - Private

- (void)setupInterface {
    
    [self.lblFavorites setText:[NSString stringWithFormat:@"%@(%li)", self.openingFrom, (long)self.arrayVideosSource.count]];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.lblEpisodeNumber setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:12.0f]];
        [self.lblEpisodeTitle setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
        [self.lblEpisodeDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:16.0f]];
        [self.lblProgramTitle setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
        
        [self.lblFavorites setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:21.0f]];
        [self.lblFavoriteProgram setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:15.0f]];
        [self.lblFavoriteTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:17.0f]];
        [self.lblFavoriteInfo setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:15.0f]];
        [self.lblNoFavorites setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:18.0f]];
        
        [self.lblGames setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:25.0f]];
        
    } else {
        
        [self.lblProgramTitle setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:14.0f]];
        [self.lblEpisodeTitle setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:16.0f]];
        [self.lblEpisodeDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:14.0f]];
        
        [self.lblFavorites setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:18.0f]];
        [self.lblFavoriteProgram setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:15.0f]];
        [self.lblFavoriteTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:17.0f]];
        [self.lblFavoriteInfo setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:15.0f]];
        [self.lblNoFavorites setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:16.0f]];
        
        [self.lblGames setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:23.0f]];
    }
}

- (void)setupEpisodeInfo {
    
    if (self.episode.program.alternative_poster_image) {
        [self.imgViewCharacter sd_setImageWithURL:[NSURL URLWithString:self.episode.program.alternative_poster_image]];
    }
    
    [self.lblEpisodeNumber setText:[NSString stringWithFormat:@"TEMP.%li - EP.%li", (long)self.episode.season, (long)self.episode.number]];
    [self.lblEpisodeTitle setText:self.episode.title];

    [self.lblEpisodeDescription setText:self.episode.ep_description];
    [self.lblProgramTitle setText:self.episode.program.title];
}

- (void)setupFavoritesCarousel {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        self.carouselFavorites = [[iCarousel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 374.0f, 146.0f)];
        
        [self.carouselFavorites setType:iCarouselTypeRotary];
        
        [self.carouselFavorites setScrollSpeed:0.5f];
        [self.carouselFavorites setPerspective:-1.0f/800.0f];
        
    } else {
        
        CGRect frame;
        
        if ([UIHelper isIPhone5]) {
            frame = CGRectMake(0.0f, 0.0f, 568.0f, 90.0f);
        } else {
            frame = CGRectMake(0.0f, 0.0f, 480.0f, 90.0f);
        }
        
        self.carouselFavorites = [[iCarousel alloc] initWithFrame:frame];
        
        [self.carouselFavorites setType:iCarouselTypeLinear];
    }

    [self.carouselFavorites setDelegate:self];
    [self.carouselFavorites setDataSource:self];
    [self.carouselFavorites setPagingEnabled:YES];
    [self.carouselFavorites setCenterItemWhenSelected:YES];
    
    [self.viewCarousel addSubview:self.carouselFavorites];
    
    [self setupVisibleInformationInViewFavorites];
    int index = 0;
    for (int i = 0; i < self.arrayVideosSource.count; i++) {
        Episode *ep = self.arrayVideosSource[i];
        if (self.episode.episode_id == ep.episode_id) {
            index = i;
            break;
        }
    }
    [self.carouselFavorites setCurrentItemIndex:index];
}

- (void)setupVisibleInformationInViewFavorites {
    
    if (self.arrayVideosSource.count == 0) {
        
        [self.lblNoFavorites setHidden:NO];
        [self.lblFavoriteProgram setHidden:YES];
        [self.lblFavoriteTitle setHidden:YES];
        [self.lblFavoriteInfo setHidden:YES];
        
        [self.viewCarousel setHidden:YES];
        
        [self.viewFavoriteVideos setHidden:YES];
        
    } else {
        
        [self.lblNoFavorites setHidden:YES];
        [self.lblFavoriteProgram setHidden:NO];
        [self.lblFavoriteTitle setHidden:NO];
        [self.lblFavoriteInfo setHidden:NO];
        
        [self.viewCarousel setHidden:NO];
        
        [self.viewFavoriteVideos setHidden:NO];
    }
}

- (void)setupNotifications {
    
    //Adicionando listeners do player que exibe os botoes por cima, caso o usuário tenha tocado na tela
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationShowButtonsMenuPlayer:)
                                                 name:@"ShowButtonsMenuPlayer"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationShowButtonsMenuPlayerWithTime:)
                                                 name:@"ShowButtonsMenuPlayerWithTime"
                                               object:nil];
}

- (void)setupPlayerWithEpisode:(Episode *)episode {
    
    if (self.playerController) {
        
        if ([self.playerController.player playing]) {
            self.playerController.player = nil;
        }
        
        [self.playerController.view removeFromSuperview];
        
        [self.playerController.auditudeView endBreak];
        
        [self.playerController.auditudeView removeFromSuperview];
        
        self.playerController = nil;
        
        self.shouldSkipAuditude = YES;
    }
    
    self.playerController = [[PlayerViewController alloc] initWithNibName:[UIHelper deviceViewName:@"PlayerView"]
                                                                   bundle:nil];

    self.currentProgram = episode.program;
    
    [self.playerController setDelegate:self];
    [self.playerController setAccess_token:self.access_token];
    [self.playerController setEpisode:episode];
    [self.playerController setIsSizeCorrectionEnabled:self.isSizeCorrectionEnabled];
    [self.playerController setShouldSkipAuditude:self.shouldSkipAuditude];
    
    [self.scrView addSubview:self.playerController.view];
    
    self.isSizeCorrectionEnabled = YES;
}

- (void)setupViewGames {
    
    [self.lblGames setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:25.0f]];

    CGSize viewSize;
    CGFloat x = 0.0f;
    CGFloat y = 0.0f;
    NSInteger numberOfColumns;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        viewSize = CGSizeMake(245.0f, 140.0f);
        x = 10.0f;
        y = 100.0f;
        numberOfColumns = 4;
        
    } else {
    
        viewSize = CGSizeMake(144.0f, 81.0f);
        x = 10.0f;
        y = 0.0f;
        numberOfColumns = 3;
    }
    
    for (NSInteger i = 0; i < [[[GameManager sharedManager] objects] count]; i++) {
        
        if (i % numberOfColumns == 0 && i > 0) {
            x = 10.0f;
            y += (viewSize.height + 10.0f);
        } else if (i > 0) {
            x += (viewSize.width + 10.0f);
        }
        
        CGRect frame = CGRectMake(x, y, viewSize.width, viewSize.height);
        
        Game *game = [[[GameManager sharedManager] objects] objectAtIndex:i];
        
        GameView *view = [[GameView alloc] initWithFrame:frame
                                                    game:game
                                                delegate:self];
        
        [self.scrViewGames addSubview:view];
    }
    
    [self.scrViewGames setContentSize:CGSizeMake(self.view.frame.size.width - 16.0f, y + viewSize.height + 30.0f)];
}

- (void)showViewButtons {
    
    [self.btnBookmark setSelected:[self.episode isFavorite]];
    
    [self.viewButtons setHidden:NO];
    [self.btnVoltar setHidden:NO];
    
    [self.scrView bringSubviewToFront:self.btnVoltar];
    [self.scrView bringSubviewToFront:self.viewButtons];
}

- (void)hideViewButtons {
    
    [self.viewButtons setHidden:YES];
    [self.btnVoltar setHidden:YES];
    
    if (self.timerViewButtons) {
        
        [self.timerViewButtons invalidate];
        
        self.timerViewButtons = nil;
    }
}

- (void)showViewGames {
    
    [self.view bringSubviewToFront:self.viewGames];
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.viewGames setAlpha:1.0f];
                     }];
}

- (void)hideViewGames {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.viewGames setAlpha:0.0f];
                     }];
}

- (void)lowerAllViews {
    
    for (UIView *view in self.scrViewGames.subviews) {
        
        [UIView animateWithDuration:0.5f
                         animations:^{
                             [view setCenter:CGPointMake(view.center.x, view.center.y + 600.0f)];
                         }];
    }
    
    [self.scrViewGames setContentSize:CGSizeMake(self.view.frame.size.width,
                                                 self.scrViewGames.contentSize.height + 600.0f)];
}

- (void)setFavoriteButtonState {

    if ([self.episode isFavorite] == YES) {

        [self.btnBookmark setSelected:NO];
        
    } else {

        [self.btnBookmark setSelected:YES];
    }
}

#pragma mark - iCarousel DataSource / Delegate

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return self.arrayVideosSource.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view {

    UIImageView *thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 90.0f)];
    
    Episode *episode;
    
    if (self.arrayVideosSource.count > 0) {
        
        episode = [self.arrayVideosSource objectAtIndex:index];
        
        if (episode.image_cropped == nil) {
            [thumbnail setImage:[UIImage imageNamed:@"placeholder_medium"]];
        } else {
            [thumbnail sd_setImageWithURL:[NSURL URLWithString:episode.image_cropped]];
        }
        
    }

    return thumbnail;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    
    Episode *episode = [self.arrayVideosSource objectAtIndex:index];
    
    self.episode = episode;
    self.currentIndex = carousel.currentItemIndex;
    
    [self setupEpisodeInfo];
    [self setupPlayerWithEpisode:self.episode];
    
    [self.scrView setContentOffset:CGPointZero animated:YES];
    
    [self setFavoriteButtonState];
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel {
    
    if (self.arrayVideosSource.count != 0) {
        
        Episode *episode = [self.arrayVideosSource objectAtIndex:carousel.currentItemIndex];
        
        [self.lblFavoriteProgram setText:episode.program.title];
        [self.lblFavoriteTitle setText:episode.title];
        [self.lblFavoriteInfo setText:[NSString stringWithFormat:@"TEMP.%li - EP.%li", (long)episode.season, (long)episode.number]];
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    
    switch (option) {

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            
            case iCarouselOptionSpacing:
                return 1.05f;
            
        } else {
            
            case iCarouselOptionFadeMin:
                return -2.0f;
            case iCarouselOptionFadeMax:
                return 0.0f;
            case iCarouselOptionFadeRange:
                return 2.0f;
            case iCarouselOptionFadeMinAlpha:
                return 2.0f;
            case iCarouselOptionArc:
                return 2.0f * M_PI;
            case iCarouselOptionCount:
                return 5.0f;
            
        }
            
        default:
            return value;
    }
}

#pragma mark - PlayerInfoViewController Delegate

- (void)playNextVideoInPlaylist {
    
    self.currentIndex++;
    
    if (self.currentIndex >= [self.arrayVideosSource count]) {
        [self dismissVideoPlayer];
        
        return;
    }
    
    [self.scrView setContentOffset:CGPointZero animated:YES];
    
    [self.carouselFavorites scrollToItemAtIndex:self.currentIndex animated:YES];
    
    Episode *episode = [self.arrayVideosSource objectAtIndex:self.currentIndex];
    
    self.episode = episode;
    
    [self setupEpisodeInfo];
    [self setupPlayerWithEpisode:self.episode];
}

- (void)dismissVideoPlayer {
    
    [self.playerController removeAllObservers];
    
    if (self.playerController.player.duration > 0) {
        double videoProgress = 100 * self.playerController.player.currentPosition / self.playerController.player.duration;
        
        // 90% do vídeo reproduzido
        if (videoProgress >= 90) {
            NSString *label = [NSString stringWithFormat:@"vod|%@||fechado|gloob|%@|%@|%@|%@|%@", @(self.episode.id_globo_videos),
                               [self.episode.category slugalize],
                               [self.episode.program.title slugalize],
                               (self.episode.season > 0 ? @(self.episode.season) : @""),
                               (self.episode.number > 0 ? @(self.episode.number) : @""),
                               [self.episode.title slugalize]];
            
            [[GAHelper sharedInstance] trackEventWithCategory:@"Video - Finalizado" andLabel:label];
        }
    }
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

#pragma mark - SelectedCharacterViewController Delegate

- (void)dismissPopUp {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.characterController.view setAlpha:0.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             
                             [self.characterController willMoveToParentViewController:nil];
                             [self.characterController.view removeFromSuperview];
                             [self.characterController removeFromParentViewController];
                             
                             self.characterController = nil;
                         }
                         
                     }];
}

- (void)playSelectedEpisodeInCurrentPlayer:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from {
    
    [self dismissPopUp];
    
    self.episode = episode;
    
    [self.playerController setEpisode:self.episode];
    
    [self setupPlayerWithEpisode:self.episode];
    
    self.arrayVideosSource = list;
    
    self.currentIndex = index;
    
    [self setupEpisodeInfo];
    
    [self.lblFavorites setText:[NSString stringWithFormat:@"%@(%li)", from, (long)self.arrayVideosSource.count]];
    
    [self.scrView setContentOffset:CGPointZero animated:YES];
    
    [self.carouselFavorites reloadData];
}

#pragma mark - GameView Delegate

- (void)openGame:(Game *)game {
    
    NSString *nibName = nil;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        nibName = @"PlayerGameView";
        
        
        
    } else {
        
        nibName = @"GameInteractionViewSD";
        
//        GameInteractionViewController *controller = [[GameInteractionViewController alloc] initWithNibName:@"GameInteractionViewSD"
//                                                                                                    bundle:nil];
//        [controller setGame:game];
//        [self presentViewController:controller
//                           animated:YES
//                         completion:^{
//                             [controller loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:game.game_file]]];
//                         }];
    }
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:game.game_file]];
    
    self.gameController = [[GameInteractionViewController alloc] initWithNibName:nibName
                                                                          bundle:nil];
    
    [self.gameController setGame:game];
    [self.gameController setRequest:request];
    [self.gameController setDelegate:self];
    
    [self.gameController.view setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, [UIHelper screenSize].height)];
    [self.gameController willMoveToParentViewController:self];
    
    [self.view addSubview:self.gameController.view];
    [self addChildViewController:self.gameController];
    
    [self.gameController didMoveToParentViewController:self];
    
    NSString *label = [NSString stringWithFormat:@"videos|%@", [game.name slugalize]];
    [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Jogar" andLabel:label];
}

#pragma mark - GameInteractionViewController delegate

- (void)dismissGame {
    
    [self.gameController willMoveToParentViewController:nil];
    [self.gameController.view removeFromSuperview];
    [self.gameController removeFromParentViewController];
    
    self.gameController = nil;
}

#pragma mark - NSNotification methods

- (void)notificationShowButtonsMenuPlayer:(NSNotification *)notificationName {
    
//    NSLog(@"%@", notificationName.name);
    
    if ([self.viewButtons isHidden] == YES) {
        [self showViewButtons];
    } else {
        [self hideViewButtons];
    }
}

- (void)notificationShowButtonsMenuPlayerWithTime:(NSNotification *)notificationName {
    
//    NSLog(@"%@", notificationName.name);
    
    if ([self.viewButtons isHidden] == YES) {
        
        [self showViewButtons];
        
        //Agenda para daqui a 5 segundos esconder os botões
        self.timerViewButtons = [NSTimer scheduledTimerWithTimeInterval:7.0f
                                                                 target:self
                                                               selector:@selector(hideViewButtons)
                                                               userInfo:nil
                                                                repeats:NO];
    } else {
        
        [self hideViewButtons];
    }
}

#pragma mark - IBActions

- (IBAction)favoriteThisVideo:(UIButton *)sender {

    if ([self.episode isFavorite] == YES) {
        
        [self.episode removeFromFavorites];
        
        [self.btnBookmark setSelected:NO];
        
    } else {
        
        [self.episode favoriteThisEpisode];
        
        [self.btnBookmark setSelected:YES];
        
        NSString *label = [NSString stringWithFormat:@"vod|%@||fechado|gloob|%@|%@|%@|%@|%@",
                           @(self.episode.id_globo_videos),
                           [self.episode.category slugalize],
                           [self.episode.program.title slugalize],
                           (self.episode.season > 0 ? @(self.episode.season) : @""),
                           (self.episode.number > 0 ? @(self.episode.number) : @""),
                           [self.episode.title slugalize]];
        
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Favorito" andLabel:label];
    }
    
    [self.carouselFavorites reloadData];
    [self setupVisibleInformationInViewFavorites];
    
    [self.lblFavorites setText:[NSString stringWithFormat:@"%@(%li)", self.openingFrom, (long)self.arrayVideosSource.count]];
}

- (IBAction)showVideoInfo:(UIButton *)sender {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         
                         [self.scrView setContentOffset:CGPointMake(0.0f,
                                                                    self.viewBottom.frame.size.height)];
                         
                         NSString *label = [NSString stringWithFormat:@"vod|%@||fechado|gloob|%@|%@|%@|%@|%@",
                                            @(self.episode.id_globo_videos),
                                            [self.episode.category slugalize],
                                            [self.episode.program.title slugalize],
                                            (self.episode.season > 0 ? @(self.episode.season) : @""),
                                            (self.episode.number > 0 ? @(self.episode.number) : @""),
                                            [self.episode.title slugalize]];
                         
                         [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Sobre o vídeo" andLabel:label];
                     }];
}

- (IBAction)showGames:(UIButton *)sender {
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    
    //Envia notification para que o play pause, por este estar em background
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayerBackground"
                                                        object:nil
                                                      userInfo:nil];
    
    [self showViewGames];
}

- (IBAction)showCharacterEpisodes:(UIButton *)sender {
    
    [self.playerController.player pause];

    self.characterController = [[SelectedCharacterViewController alloc] initWithNibName:[UIHelper deviceViewName:@"SelectedCharacterView"]
                                                                                 bundle:nil];
    
    [self.characterController setIsInsidePlayer:YES];
    [self.characterController setDelegate:self];
    [self.characterController setProgram:self.currentProgram];
    [self.characterController willMoveToParentViewController:self];
    
    if ([UIHelper isIPhone5]) {
        [self.characterController.view setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    }
    
    [self.view addSubview:self.characterController.view];
    [self addChildViewController:self.characterController];
    
    [self.characterController didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [self.characterController.view setAlpha:1.0f];
                     }];
}

- (IBAction)closeWindow:(UIButton *)sender {
    [self dismissVideoPlayer];
}

- (IBAction)closeGamesView:(UIButton *)sender {
    
    [self.playerController.player play];
    
    [self hideViewGames];
}

#pragma mark GATrackableScreen

- (NSString *)screenNameForScreenEvent {
    return [NSString stringWithFormat:@"/videos/%@/%@/atalho-jogos",
            [self.episode.program.title slugalize],
            [self.episode.title slugalize]];
}
@end
