//
//  GameInteractionViewController.m
//  gloob
//
//  Created by zeroum on 11/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GameInteractionViewController.h"
#import "EventLog.h"

@interface GameInteractionViewController ()

- (IBAction)closeWindow:(UIButton *)sender;

@end

@implementation GameInteractionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.webView setDelegate:self];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if ([UIHelper isIPhone5]) {
            [self.webView setFrame:CGRectMake(20.0f, 0.0f, 481.0f, 320.0f)];
        }
    }
    
    [self.webView loadRequest:self.request];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    [[EventLog sharedInstance] logPlayEventWithGameId:self.game.game_id];
}

- (void)loadRequest:(NSURLRequest *)request {
    [self.webView loadRequest:request];
}

- (IBAction)closeWindow:(UIButton *)sender {
    
    if (self.delegate) {
        
        if ([self.delegate respondsToSelector:@selector(dismissGame)]) {
            [self.delegate dismissGame];
        }
        
    } else {
        self.webView = nil;
        
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {

}

- (void)webViewDidFinishLoad:(UIWebView *)webView {

}

#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return [NSString stringWithFormat:@"/jogos/%@", [self.game.name slugalize]];
}
@end
