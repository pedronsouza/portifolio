//
//  PlayerViewController.m
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "PlayerViewController.h"
#import "POCViewController.h"
#import "Episode.h"
#import "EventLog.h"
#import "Login.h"
#import "LoginManager.h"

#define kMovieDurationAvaiable @"movieDurationAvaiable"
#define kMoviePlaybackDidFinish @"moviePlaybackDidFinish"
#define kMovieLoadStateDidChange @"movieLoadStateDidChange"
#define kMoviePlaybackStateDidChange @"moviePlaybackStateDidChange"
#define kMovieHasAlreadyBeenMarked @"kMovieHasAlreadyBeenMarked"

@interface PlayerViewController ()

@property (nonatomic, assign) NSTimeInterval playerInterval;
@property (nonatomic, assign) NSTimeInterval videoDuration;

@property (weak, nonatomic) IBOutlet UIView *viewPlayer;
@property (weak, nonatomic) IBOutlet UIView *viewPlayer568;

@property (weak, nonatomic) IBOutlet UIView *viewLoading;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, strong) UIView *viewAuditudeSkipLayer;
@property (nonatomic, strong) UILabel *lblAuditudeTimer;
@property (nonatomic, strong) UIButton *btnSkipAuditude;
@property (nonatomic, strong) NSTimer *auditudeSkipTimer;
@property (nonatomic, assign) NSInteger auditudeLengthInSeconds;
@property (nonatomic, assign) NSInteger auditudeSkipCounter;

@property (nonatomic, assign) BOOL isTimerSet;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) int seconds;

- (void)startTimer;
- (void)invalidateTimer;
- (void)setupNotifications;
- (void)showViewLoading;
- (void)hideViewLoading;

- (void)notificationDidEnterBackground:(NSNotification *)notificationName;
- (void)notificationDidEnterForeground:(NSNotification *)notificationName;

- (void)setupAuditude;
- (void)addAuditudeObservers;
- (void)removeAuditudeObservers;

@end

@implementation PlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNotifications];
    
    [self startPlayback];
    
    if (self.shouldSkipAuditude == NO) {
        [self setupAuditude];
        [self startAuditude];
    }
    
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kMovieHasAlreadyBeenMarked]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kMovieHasAlreadyBeenMarked];
    }
    
    [self invalidateTimer];
}

#pragma mark - Private

- (void)startTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(onTick:)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)invalidateTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)onTick:(NSTimer *)timer {
    
    if (self.seconds >= 60) {
        self.seconds = 0;
        
        NSString *label = [NSString stringWithFormat:@"vod|%@||fechado|gloob|%@|%@|%@|%@|%@",
                           @(self.episode.id_globo_videos),
                           [self.episode.category slugalize],
                           [self.episode.program.title slugalize],
                           (self.episode.season > 0 ? @(self.episode.season) : @""),
                           (self.episode.number > 0 ? @(self.episode.number) : @""),
                           [self.episode.title slugalize]];
        
        [[GAHelper sharedInstance] trackEventWithCategory:@"Video - Tempo" andLabel:label];

    } else {
        self.seconds++;
    }
}

- (void)setupAuditude {
    
    // display and use button to handle the click (YES) or use internal click, click on videoAd (NO)
    self.useButtonForAdClick = YES;
    
    self.auditudeView = [[AuditudeView alloc] init];
    
    [self.auditudeView setDelegate:self];
    
    //-- should the player open browser on videoAd click. (default is NO)
    [self.auditudeView setAuditudeHandlesClick:!self.useButtonForAdClick];
    
    // network ad prefetch
    [self.auditudeView setPrefetchNetworkAds:NO];
    
    // turn off logging
    NSLog(@"Auditude Version: %@", [AuditudeView auditudeSDKVersion]);
    [AuditudeView enableDebugLog:NO];
    
    [self.auditudeView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

    [self setupSkipAuditudeLayer];
    
    // set the dimensions
    if ([UIHelper isIPhone5]) {

        [self.auditudeView setFrame:CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, [UIHelper screenSize].height)];

        [self.viewPlayer568 addSubview:self.auditudeView];
        
        [self.viewPlayer568 addSubview:self.viewAuditudeSkipLayer];
        [self.viewPlayer568 bringSubviewToFront:self.viewAuditudeSkipLayer];
        
        [self.viewPlayer setHidden:YES];
        [self.viewPlayer568 setHidden:NO];
        
    } else {
        
        [self.auditudeView setFrame:self.viewPlayer.frame];
        
        [self.viewPlayer addSubview:self.auditudeView];
        
        [self.viewPlayer addSubview:self.viewAuditudeSkipLayer];
        [self.viewPlayer bringSubviewToFront:self.viewAuditudeSkipLayer];
        
        [self.viewPlayer568 setHidden:YES];
        [self.viewPlayer setHidden:NO];
    }
}

- (void)setupNotifications {
    
    //Adicionando listeners para verificar se existe uma tela por cima do player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationDidEnterBackground:)
                                                 name:@"PlayerBackground"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationDidEnterForeground:)
                                                 name:@"PlayerForeground"
                                               object:nil];
}

- (void)showViewLoading {
    
    [self.view bringSubviewToFront:self.viewLoading];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [self.viewLoading setAlpha:1.0f];
                     }];
}

- (void)hideViewLoading {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [self.viewLoading setAlpha:0.0f];
                     }];
}

#pragma mark - public

- (void)startPlayback {
    
    self.player = [[WebmediaPlayer alloc] initWithVideoId:self.episode.id_globo_videos];
    
    [self.player setEventListener:self];
    [self.player setAuthenticationToken:self.access_token];
    [self.player setFullscreen:YES];
    
    Login *login = [LoginManager getSavedLogin];
    
    [self.player setPayTvServiceProvider:login.operadora];

    if (self.shouldSkipAuditude == NO) {
        [self addAuditudeObservers];
        
        self.isMainVideoPlaying = YES;
        self.isPostRoll = NO;
    }
    
    // Corrige o problema em que o player não aceita o tamanho correto da view na segunda vez que tocarmos um vídeo
    if ([UIHelper isIPhone5] && self.isSizeCorrectionEnabled == YES) {
        
        [self.viewLoading setFrame:CGRectMake(209.0f,
                                              self.viewLoading.frame.origin.y,
                                              self.viewLoading.frame.size.width,
                                              self.viewLoading.frame.size.height)];
        
        [self.viewPlayer setHidden:YES];
        [self.viewPlayer568 setHidden:NO];
        
        [self.player attachTo:self.viewPlayer568];

    } else {
        
        self.isSizeCorrectionEnabled = YES;
        
        [self.viewPlayer setHidden:NO];
        [self.viewPlayer568 setHidden:YES];
        
        [self.player attachTo:self.viewPlayer];
    }
}

- (void)removeAllObservers {
    
    [self removeAuditudeObservers];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"PlayerBackground"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"PlayerForeground"
                                                  object:nil];
}

#pragma mark - UITouch

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //Caso o usuário clique na tela, o player executará o resume ou o pause.
    
    for (UITouch *aTouch in touches) {
        
        if (aTouch.tapCount < 2) {
            
            CGPoint touchLocation = [aTouch locationInView:self.view];
            
            if (CGRectContainsPoint(self.viewPlayer.frame, touchLocation)) {
                
                if ([self.player playing]) {
                    
                    [self.player pause];
                    
                } else {
                    
                    [self.player play];
                }
            }
        }
    }
}

#pragma mark - NSNotification methods

- (void)notificationDidEnterBackground:(NSNotification *)notificationName {
    
    NSLog(@"%@", notificationName.name);
    
    if ([self.player playing]) {
        [self.player pause];
    }
}

- (void)notificationDidEnterForeground:(NSNotification *)notificationName {
    
    NSLog(@"%@", notificationName.name);
    
    if (![self.player playing]) {
        [self.player play];
    }
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 0) {
            
            if ([self.delegate respondsToSelector:@selector(dismissVideoPlayer)]) {
                [self.delegate dismissVideoPlayer];
            }
        }
    }
}

#pragma mark - WebMediaPlayer Listeners

- (void)onPlayReady {
    // Called when the streaming is requested
    NSLog(@"onPlayReady");
    
    [self showViewLoading];
    
    if (self.shouldSkipAuditude == NO) {
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:kMovieDurationAvaiable forKey:kMovieDurationAvaiable];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMovieDurationAvaiable
                                                            object:self.player
                                                          userInfo:userInfo];
    }
    
    NSString *label = [NSString stringWithFormat:@"vod|%@|%@/%@|fechado|gloob|%@|%@|%@|%@|%@",
                       @(self.episode.id_globo_videos),
                       [[[UIDevice currentDevice] model] slugalize],
                       [[[UIDevice currentDevice] systemVersion] slugalize],
                       [self.episode.category slugalize],
                       [self.episode.program.title slugalize],
                       (self.episode.season > 0 ? @(self.episode.season) : @""),
                       (self.episode.number > 0 ? @(self.episode.number) : @""),
                       [self.episode.title slugalize]];

    [[GAHelper sharedInstance] trackEventWithCategory:@"Video - Primeiro Play" andLabel:label];
    
    NSString *labelRTConteudo = [NSString stringWithFormat:@"%@|%@",
                                 [self.episode.program.title slugalize],
                                 [self.episode.title slugalize]];
    
    NSString *labelRTCOperadora = [[LoginManager  getSavedLogin].operadora slugalize];
    
    [[GAHelper sharedInstance] trackEventWithCategory:@"RT - Conteudo" andLabel:labelRTConteudo];
    [[GAHelper sharedInstance] trackEventWithCategory:@"RT - Operadora" andLabel:labelRTCOperadora];
    
    NSString *label_play = [NSString stringWithFormat:@"vod|%@||fechado|gloob|%@|%@|%@|%@|%@", @(self.episode.id_globo_videos),
                       [self.episode.category slugalize],
                       [self.episode.program.title slugalize],
                       (self.episode.season > 0 ? @(self.episode.season) : @""),
                       (self.episode.number > 0 ? @(self.episode.number) : @""),
                       [self.episode.title slugalize]];
    
    [[GAHelper sharedInstance] trackEventWithCategory:@"Video - Play" andLabel:label_play];
}

- (void)onPlay {
    // Called when the streaming starts.
    NSLog(@"onPlay");
    
    // Notification para que os botoes fiquem ativos na tela por um determinado periodo de tempo
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowButtonsMenuPlayerWithTime"
                                                        object:nil
                                                      userInfo:nil];
    [self startTimer];
}

- (void)onPause {
    // Called when the Pause button or Home button is pressed or when the video is finished.
    NSLog(@"onPause");
    
    // Mark Achievements Event
    [self markEventIfVideoUpHalf];
    
    // Notification para que os botoes fiquem ativos até que o usuário clique na tela novamente
    // ou o play seja retomado
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowButtonsMenuPlayer"
                                                        object:nil
                                                      userInfo:nil];
    
    double halfVideoDuration = self.videoDuration / 2;
    
    if (self.playerInterval >= halfVideoDuration && ![[NSUserDefaults standardUserDefaults] boolForKey:kMovieHasAlreadyBeenMarked]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kMovieHasAlreadyBeenMarked];
        [[EventLog sharedInstance] logViewEventWithProgramId:self.episode.program.program_id];
        [[EventLog sharedInstance] logMarkEventWithKey:@"video"];
    }
    
    [self invalidateTimer];
}
- (void) markEventIfVideoUpHalf {
    self.playerInterval = self.player.currentPosition;
    self.videoDuration = self.player.duration;
}
- (void)onStop {
    // Called when the video is finished.
    NSLog(@"onStop");
    
    if ([self.delegate respondsToSelector:@selector(showViewButtons)]) {
        [self.delegate showViewButtons];
    }
    
    
}

- (void)onLoadStateChanged {
    // Called when the streaming load state changes (when the streaming is requested, started or interrupted).
    NSLog(@"onLoadStateChanged : %ld", (long)self.player.loadState);
    
    switch (self.player.loadState) {
        case 3:
            [self hideViewLoading];
            break;
            
        case 5:
            [self showViewLoading];
            break;
            
        default:
            break;
    }
}

- (void)onStreamingPauseTooLong:(NSTimeInterval)pausedPosition {
    NSLog(@"onStreamingPauseTooLong: %.2f", pausedPosition);
}

- (void)onCompletion {
    // Called when the video presentation is finished or when the return button is pressed. (Playlist must not be enabled)
    NSLog(@"onCompletion");
    
    if (self.shouldSkipAuditude == NO) {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:kMoviePlaybackDidFinish forKey:kMoviePlaybackDidFinish];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMoviePlaybackDidFinish
                                                            object:self.player
                                                          userInfo:userInfo];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kMovieHasAlreadyBeenMarked]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kMovieHasAlreadyBeenMarked];
    }
    
    if ([self.delegate respondsToSelector:@selector(playNextVideoInPlaylist)]) {
        [self.delegate playNextVideoInPlaylist];
    }
    
    if (self.player.duration > 0) {
        double videoProgress = 100 * self.player.currentPosition / self.player.duration;
        
        // 90% do vídeo reproduzido
        if (videoProgress >= 90) {
            NSString *label = [NSString stringWithFormat:@"vod|%@||fechado|gloob|%@|%@|%@|%@|%@",
                               @(self.episode.id_globo_videos),
                               [self.episode.category slugalize],
                               [self.episode.program.title slugalize],
                               (self.episode.season > 0 ? @(self.episode.season) : @""),
                               (self.episode.number > 0 ? @(self.episode.number) : @""),
                               [self.episode.title slugalize]];
            
            [[GAHelper sharedInstance] trackEventWithCategory:@"Video - Finalizado" andLabel:label];
        }
    }
}

- (void)onWillEnterFullscreen {
    // Called when the player will enter full screen mode.
    NSLog(@"onWillEnterFullscreen");
}

- (void)onWillExitFullscreen {
    // Called when the player will exit full screen mode.
    NSLog(@"onWillExitFullscreen");
}

- (void)onDidEnterFullscreen {
    // Called when the player entered full screen mode.
    NSLog(@"onDidEnterFullscreen");
}

- (void)onDidExitFullscreen {
    // Called when the player exited full screen mode.
    NSLog(@"onDidExitFullscreen");
}

- (void)onError:(NSError *)error cause:(NSError *)cause {
    
    NSString *message;
    
    NSLog(@"cause.code: %li", (long)cause.code);
    
    switch (cause.code) {
        case AUTHENTICATION_REQUIRED:
            NSLog(@"AUTHENTICATION_REQUIRED");
            message = @"AUTHENTICATION_REQUIRED";
            break;
            
        case DEVICE_UNAUTHORIZED:
            NSLog(@"DEVICE_UNAUTHORIZED");
            message = @"DEVICE_UNAUTHORIZED";
            break;
            
        case GEOBLOCKING:
            NSLog(@"GEOBLOCKING");
            message = @"GEOBLOCKING";
            break;
            
        case SIMULTANEOUS_ACCESS:
            NSLog(@"SIMULTANEOUS_ACCESS");
            message = @"SIMULTANEOUS_ACCESS";
            break;
            
        case PLAYBACK_FAILED:
            NSLog(@"PLAYBACK_FAILED");
            message = @"PLAYBACK_FAILED";
            break;
            
        case UNSUPPORTED_MEDIA:
            NSLog(@"UNSUPPORTED_MEDIA");
            message = @"UNSUPPORTED_MEDIA";
            break;
            
        case VIDEO_NOT_FOUND:
            NSLog(@"VIDEO_NOT_FOUND");
            message = @"VIDEO_NOT_FOUND";
            break;
            
        case -1009:
            NSLog(@"SEM CONEXÃO");
            message = @"Sem conexão com a internet.";
            break;
        default:
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    
    [alert setTag:1];
    
    [alert show];
}

#pragma mark - Auditude

- (void)startAuditude {
    
    self.isMainVideoPlaying = NO;
    self.isPostRoll = NO;
    
    [self.auditudeView endBreak];
    
    POCViewController *controller = (POCViewController *)self.delegate;
    
    [controller.scrView setScrollEnabled:NO];
    
    if (self.player) {
        [self.player stop];
    }
    
    self.playHeadTime = -1.0f;
    self.skipBreak = NO;
    
    //videoUrl = url;
    
    // passing key-value pairs to Auditude
    NSMutableDictionary *cp = [[NSMutableDictionary alloc] init];
    //[cp setValue:@"ipad" forKey:@"device"];
    
    // requesting ads requires three parameters and one optional parameter
    // a) the video id
    // b) zoneId
    // c) domain
    // d) customParams (optional)
    // wait for 'auditudeInitComplete' delegate method to be called.
    //[self.auditudeView requestAdsForVideo:@"aud_test_asset1"
    //								   zoneId:9797 domain:@"auditude.com" targetingParams:cp];
    
    // Production
    [self.auditudeView setContentDuration:78];
    
    [self.auditudeView requestAdsForVideo:[NSString stringWithFormat:@"globo_%li", (long)self.episode.id_globo_videos]
                                   zoneId:232450
                                   domain:@"auditude.com"
                          targetingParams:cp];
    
//    // Debug
//    [self.auditudeView setContentDuration:78];
//    
//    [self.auditudeView requestAdsForVideo:@"aud_test_asset1"
//                                   zoneId:9797
//                                   domain:@"auditude.com"
//                          targetingParams:cp];
}

- (void)addAuditudeObservers {
    
    if (self.listenersAdded == NO) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieDurationAvailable:)
                                                     name:kMovieDurationAvaiable
                                                   object:self.player];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackDidFinish:)
                                                     name:kMoviePlaybackDidFinish
                                                   object:self.player];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieLoadStateDidChange:)
                                                     name:kMovieLoadStateDidChange
                                                   object:self.player];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackStateDidChange:)
                                                     name:kMoviePlaybackStateDidChange
                                                   object:self.player];
        
        self.listenersAdded = YES;
        
    }
    
    [self removePlayHeadTimer];
    
    self.playHeadTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                          target:self
                                                        selector:@selector(moviePlaybackProgress)
                                                        userInfo:nil
                                                         repeats:YES];
    
}

- (void)removeAuditudeObservers {
    
    if (self.listenersAdded == YES) {
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kMovieDurationAvaiable
                                                      object:self.player];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kMoviePlaybackDidFinish
                                                      object:self.player];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kMovieLoadStateDidChange
                                                      object:self.player];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kMoviePlaybackStateDidChange
                                                      object:self.player];
        
        self.listenersAdded = NO;
    }
    
    [self removePlayHeadTimer];
}

- (void)movieDurationAvailable:(NSNotification*)notification {
    
    NSLog(@"movie duration available.");
}

- (void)moviePlaybackDidFinish:(NSNotification*)notification {
    
    [self removeAuditudeObservers];
    
    if ([notification userInfo]) {
        
        NSString *reason = [[notification userInfo] valueForKey:kMoviePlaybackDidFinish];
        
        if ([reason isEqualToString:kMoviePlaybackDidFinish] && self.isMainVideoPlaying == YES) {
            
            self.isMainVideoPlaying = NO;
            self.isPostRoll = YES;
            
            [self.auditudeView notify:AuditudeNotificationTypeVideoPlaybackComplete
                   notificationParams:nil];
            
            if (self.shouldSkipAuditude == NO) {
                [self.auditudeView beginBreak];
            }
        }
    }
}

- (void)movieLoadStateDidChange:(NSNotification *)notification {
    
}

- (void)moviePlaybackStateDidChange:(NSNotification*)notification {
    
}

- (void)removePlayHeadTimer {
    
    if (self.playHeadTimer != nil) {
        
        [self.playHeadTimer invalidate];
        
        self.playHeadTimer = nil;
    }
}

- (void)moviePlaybackProgress {
    
    if (!self.player || [self.player duration] <= 0.0f) {
        return;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:[NSNumber numberWithDouble:self.player.currentPosition] forKey:PlayheadTimeInSeconds];
    [params setValue:[NSNumber numberWithDouble:self.player.duration] forKey:TotalTimeInSeconds];
    
    if (self.auditudeView) {
        [self.auditudeView notify:AuditudeNotificationTypeVideoPlayheadUpdate notificationParams:params];
    }
}

- (void)buttonPressed {
    
    NSLog(@"Button Pressed!");
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:_viewSource forKey:viewSourceKey];
    
    [self.auditudeView notify:AuditudeNotificationTypeAdClick notificationParams:params];
}

- (void)shouldSkipBreak {
    self.skipBreak = YES;
}

- (void)shouldUnskipBreak {
    self.skipBreak = NO;
}

#pragma mark - Auditude Delegate

- (BOOL)shouldAuditudeSkipUpcomingBreak {
	return YES;
}

- (void)auditudeInitComplete:(NSDictionary *)data {
    
    NSLog(@"AdEvent: init complete");
    // ads are initialized. begin the break to show the ads.
    [self.lblAuditudeTimer setHidden:NO];
    [self.auditudeView beginBreak];
}

- (void)auditudeBreakBegin:(NSDictionary *)data {
    
    NSLog(@"AdEvent: break begin");
    self.skipBreak = NO;
}

- (void)auditudeLinearAdBegin:(NSDictionary *)data {
    
    NSLog(@"AdEvent: linear ad begin");
    
    [self setupAuditudeSkipTimer];
    
    if (data) {
        
        self.session = [data valueForKey:@"session"];
        
        self.viewSource = [data valueForKey:@"viewSource"];

        if (self.useButtonForAdClick == YES) {
            
//            if (self.clickButton) {
//                
//                [self.clickButton removeFromSuperview];
//                
//                self.clickButton = nil;
//            }
//            
//            self.clickButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//            
//            [self.clickButton setFrame:CGRectMake(5.0f, self.view.frame.size.height - 60.0f, 200.0f, 30.0f)];
//            
//            [self.clickButton addTarget:self
//                                 action:@selector(buttonPressed)
//                       forControlEvents:UIControlEventTouchUpInside];
//            
//            //set the button's title
//            NSString *clickTitle = @"no clickTile";
//            if (self.viewSource && self.viewSource.primaryAsset && self.viewSource.primaryAsset.clickTitle) {
//                clickTitle = self.viewSource.primaryAsset.clickTitle;
//            }
//            
//            [self.clickButton setTitle:clickTitle forState:UIControlStateNormal];
//            
//            //add the button to the view
//            [self.viewPlayer addSubview:self.clickButton];
//            [self.viewPlayer bringSubviewToFront:self.clickButton];
            
//            [self.viewPlayer addSubview:[self setupSkipAuditudeLayer]];
//            [self.viewPlayer bringSubviewToFront:self.viewAuditudeSkipLayer];
        }
    }
}

- (void)auditudeLinearAdEnd:(NSDictionary *)data {
    
    NSLog(@"AdEvent: linear ad end");
    
    if (self.clickButton) {
        
        [self.clickButton removeFromSuperview];
        
        self.clickButton = nil;
    }
}

- (void)auditudeLinearAdProgress:(NSTimeInterval)currentPlaybackTime totalTime:(NSTimeInterval)totalTime {
    //NSLog(@"AdEvent: linear ad progress: %f", currentPlaybackTime);
}

- (void)auditudeBreakEnd:(NSDictionary *)data; {
    
    NSLog(@"AdEvent: break end received");
    
    if (!self.isMainVideoPlaying && !self.isPostRoll) {
        
        [self toggleAutitudeTimer];
        
        POCViewController *controller = (POCViewController *)self.delegate;
        
        [controller.scrView setScrollEnabled:YES];
        
        [self startPlayback];
    }
    
    self.skipBreak = YES;
    
    [self performSelector:@selector(shouldUnskipBreak)
               withObject:nil
               afterDelay:20.0f];
}

- (void)auditudePausePlayback {
    
    NSLog(@"AdEvent: pause playback received");
}

- (void)auditudeResumePlayback {
    
    NSLog(@"AdEvent: resume playback received");
    
    if (self.player && !self.isPostRoll) {
        [self.player play];
    }
}

- (void)auditudeAdClickThrough:(NSString *)url {
    
    NSLog(@"AdEvent: AdClickThrough url:%@", url);
    
    if (self.useButtonForAdClick == YES) {
        // Open WebBrowser
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setValue:url forKey:urlKey];
        
        [self.auditudeView notify:AuditudeNotificationTypeOpenBrowser notificationParams:params];
    }
}

- (void) auditudeBrowserOpen:(NSString *)url {
    
    NSLog(@"AdEvent: BrowserOpen url:%@", url);
    
    // -- pause ad and/or content.
    if (self.player) {
        [self.player pause];
    }
    
    if (self.session) {
        [self.session notifyPause];
    }
}

- (void) auditudeBrowserClose {
    
    NSLog(@"AdEvent: BrowserClose");
    
    // -- resume ad and/or content.
    [self auditudeResumePlayback];
    
    if (self.session) {
        [self.session notifyPlay];
    }
    
}

// set this to true for mid-rolls. Auditude requires playhead update events to be passed
// to show mid-rolls. return NO if you plan to call breakBegin at the appropriate time.
- (BOOL)shouldAuditudeManageBreaks {
    return NO;
}

- (UIView *)getBannerContainerForSize:(CGSize)bannerSize {
    
    if (bannerSize.width <= self.bannerView.frame.size.width && bannerSize.height <= self.bannerView.frame.size.height) {
        
        [self.bannerView setHidden:NO];
        
        return self.bannerView;
    }
    
    return nil;
}

- (UIViewController *)getModalViewController {
    
    return self.viewController;
}

- (void)setupSkipAuditudeLayer {
    
    CGRect viewFrame = CGRectMake(0.0f, 0.0f, [UIHelper screenSize].width, 55.0f);
    CGRect labelFrame;
    CGRect buttonFrame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        viewFrame = CGRectMake(0.0f, 0.0f, 1024.0f, 55.0f);
        labelFrame = CGRectMake(10.0f, 0.0f, 400.0f, viewFrame.size.height);
        buttonFrame = CGRectMake(893.0f, 10.0f, 121.0f, 35.0f);
        
    } else {
        
        if ([UIHelper isIPhone5]) {

            labelFrame = CGRectMake(10.0f, 0.0f, 339.0f, viewFrame.size.height);
            buttonFrame = CGRectMake(437.0f, 10.0f, 121.0f, 35.0f);
            
        } else {

            labelFrame = CGRectMake(10.0f, 0.0f, 339.0f, viewFrame.size.height);
            buttonFrame = CGRectMake(349.0f, 10.0f, 121.0f, 35.0f);
            
        }
    }
    
    self.viewAuditudeSkipLayer = [[UIView alloc] initWithFrame:viewFrame];
    
    [self.viewAuditudeSkipLayer setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.6f]];
    
    self.lblAuditudeTimer = [[UILabel alloc] initWithFrame:labelFrame];
    [self.lblAuditudeTimer setHidden:YES];
    [self.lblAuditudeTimer setBackgroundColor:[UIColor clearColor]];
    [self.lblAuditudeTimer setTextColor:[UIColor whiteColor]];
    [self.lblAuditudeTimer setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:25.0f]];
    [self.lblAuditudeTimer setText:@"PUBLICIDADE"];
    
    [self.viewAuditudeSkipLayer addSubview:self.lblAuditudeTimer];
    
    self.btnSkipAuditude = [[UIButton alloc] initWithFrame:buttonFrame];
    
    [self.btnSkipAuditude setImage:[UIImage imageNamed:@"bt_pularanuncio"] forState:UIControlStateNormal];
    [self.btnSkipAuditude setAlpha:0.0f];
    
    [self.btnSkipAuditude addTarget:self
                             action:@selector(didTouchAuditudeSkipButton:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.viewAuditudeSkipLayer addSubview:self.btnSkipAuditude];
}

- (void)setupAuditudeSkipTimer {
    
    if (self.isTimerSet == NO) {
        
        [self toggleAutitudeTimer];
        
        self.auditudeSkipCounter = 5;
        
        self.auditudeSkipTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                                  target:self
                                                                selector:@selector(updateAuditudeSkipTimer:)
                                                                userInfo:nil
                                                                 repeats:YES];
        
        [self.auditudeSkipTimer fire];
        
        self.isTimerSet = YES;
    }
}

- (void)updateAuditudeSkipTimer:(NSTimer *)timer {
    
    if (self.auditudeSkipCounter == 0 ) {
        
        [self.lblAuditudeTimer setText:@"PUBLICIDADE"];
        
        [self toggleAutitudeTimer];
        
        [self showAuditudeSkipButton];
        
    } else if (self.auditudeSkipCounter == 1) {
        
        [self.lblAuditudeTimer setText:[NSString stringWithFormat:@"PUBLICIDADE Aguarde %li segundo", (long)self.auditudeSkipCounter]];
        
        self.auditudeSkipCounter--;
        
    } else {
        
        [self.lblAuditudeTimer setText:[NSString stringWithFormat:@"PUBLICIDADE Aguarde %li segundos", (long)self.auditudeSkipCounter]];
        
        self.auditudeSkipCounter--;
    }
}

- (void)toggleAutitudeTimer {
    
    if ([self.auditudeSkipTimer isValid]) {
        [self.auditudeSkipTimer invalidate];
    }
}

- (void)showAuditudeSkipButton {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [self.btnSkipAuditude setAlpha:1.0f];
                     }];
}

- (void)didTouchAuditudeSkipButton:(id)sender {
    
    [self.auditudeView endBreak];
    
    [self toggleAutitudeTimer];
    
    POCViewController *controller = (POCViewController *)self.delegate;
    
    [controller.scrView setScrollEnabled:YES];
    
    [self.viewAuditudeSkipLayer removeFromSuperview];
    
    [self auditudeBreakEnd:nil];
}

#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    
    return [NSString stringWithFormat:@"/videos/%@/%@", [self.episode.program.title slugalize], [self.episode.title slugalize]];
}
@end
