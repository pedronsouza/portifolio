//
//  CurrentProfileViewController.h
//  gloob
//
//  Created by zeroum on 02/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "FavoriteView.h"
#import "GAHelper.h"

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


@interface CurrentProfileViewController : GloobViewController <UIScrollViewDelegate, UIAlertViewDelegate, FavoriteViewDelegate, UIScrollViewDelegate, GATrackableScreen>
@property (weak, nonatomic) IBOutlet UIPageControl *selfiesPageControl;

@end
