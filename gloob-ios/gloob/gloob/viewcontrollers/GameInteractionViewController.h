//
//  GameInteractionViewController.h
//  gloob
//
//  Created by zeroum on 11/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "Game.h"
#import "GAHelper.h"

@interface GameInteractionViewController : GloobViewController <UIWebViewDelegate, GATrackableScreen>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSURLRequest *request;
@property (nonatomic, strong) Game *game;

@property (nonatomic,assign) id delegate;

- (void)loadRequest:(NSURLRequest *)request;

@end

@protocol GameInteractionViewControllerDelegate <NSObject>

- (void)dismissGame;

@end
