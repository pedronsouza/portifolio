//
//  SelfieUnlockViewController.m
//  gloob
//
//  Created by Pedro on 3/20/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SelfieUnlockViewController.h"
#import "ProfileManager.h"
#define kSelfieUnlockMainButtonText @"VER SUAS SELFIES"
#define kBackgroundImageName @"pf_surpresa_bg"
@interface SelfieUnlockViewController ()

@end

@implementation SelfieUnlockViewController

- (void)viewDidLoad {
    Avatar *avatar = [ProfileManager sharedManager].currentProfile.currentAvatar;
    [((Selfie *)self.achievement) setRelatedAvatarId:avatar.achievementId];
    [super viewDidLoad];
}

- (NSString *)textForMainButton {
    return kSelfieUnlockMainButtonText;
}

- (void)mainButtonClickAction:(id)sender {
    ParentViewController *parent = (ParentViewController *)self.parentViewController;
    [parent dismissPopUp];
    [parent openProfileView:sender];
}

- (UIImage *)imageForBackground {
    UIImage *image = [UIImage imageNamed:kBackgroundImageName];
    
    if ([UIHelper isIPhone5]) {
        image = [UIImage imageNamed:@"pf_surpresa_bg_wide"];
    }
    
    return image;
}

- (NSString *)textForAchievementMessage {
    return @"uma nova selfie";
}

@end
