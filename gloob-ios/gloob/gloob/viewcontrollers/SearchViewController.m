//
//  SearchViewController.m
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SearchViewController.h"
#import "ColoredButton.h"
#import "GloobTextField.h"
#import "EpisodeFound.h"
#import "ProgramFound.h"
#import "UIColor+HexColor.h"
#import "Program.h"
#import "ProgramManager.h"
#import "Episode.h"
#import "ParentViewController.h"
#import "LoadingView.h"
#import "Login.h"
#import "LoginManager.h"
#import "POCViewController.h"
#import "EventLog.h"
#import "UIImageView+WebCache.h"

@interface SearchViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewLeft;
@property (weak, nonatomic) IBOutlet UIView *viewRight;
@property (weak, nonatomic) IBOutlet UIView *viewResults;
@property (weak, nonatomic) IBOutlet UIView *viewNoResults;
@property (weak, nonatomic) IBOutlet UIView *viewMap;

@property (weak, nonatomic) IBOutlet UILabel *lblSearchTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblResultsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblResultsInfo;

@property (weak, nonatomic) IBOutlet UILabel *lblNoResultsInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblNoResultsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblNoResults1;
@property (weak, nonatomic) IBOutlet UILabel *lblNoResults2;
@property (weak, nonatomic) IBOutlet UILabel *lblSearchResult;

@property (strong, nonatomic) IBOutlet UIView *viewHeader;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewAvatar;

@property (weak, nonatomic) IBOutlet ColoredButton *btnSearch;
@property (weak, nonatomic) IBOutlet ColoredButton *btnShowCharacters;

@property (weak, nonatomic) IBOutlet GloobTextField *txtFieldSearch;

@property (weak, nonatomic) IBOutlet LoadingView *viewLoading;

@property (nonatomic, strong) UITableView *tbViewResults;

@property (nonatomic, strong) SearchParser *parser;

@property (nonatomic, strong) NSString *keyword;

@property (nonatomic, strong) NSString *nextPage;

- (IBAction)startSearch:(UIButton *)sender;
- (IBAction)showCharacters:(UIButton *)sender;

- (void)setupInterface;
- (void)setupTbViewResults;
- (void)onKeyboardHide:(NSNotification *)notification;
- (void)moveUpViewLeft;
- (void)moveDownViewLeft;
- (void)startSearchTask;
- (void)showViewLoading;
- (void)hideViewLoading;
- (void)setupCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath inTableView:(UITableView *)tableview;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInterface];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(onKeyboardHide:)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
    
    self.nextPage = EMPTY_FIELD;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private

- (void)setupInterface {
    
    CGFloat fontSize1;
    CGFloat fontSize2;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        fontSize1 = 35.0f;
        fontSize2 = 18.0f;
        
    } else {
        
        fontSize1 = 26.0f;
        fontSize2 = 15.0f;
    }
    
    // Label Titles
    [self.lblSearchTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:fontSize1]];
    [self.lblResultsTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:fontSize1]];
    [self.lblSearchTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:fontSize1]];
    [self.lblResultsTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:fontSize1]];
    
    // No Results Labels
    [self.lblNoResults1 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:fontSize2]];
    [self.lblNoResults2 setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:fontSize2]];
    [self.lblNoResultsInfo setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:fontSize2]];
    [self.lblNoResultsTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:fontSize1]];
    
    // Results info
    [self.lblResultsInfo setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:fontSize2]];
    [self.lblResultsInfo setAdjustsFontSizeToFitWidth:YES];
    [self.lblResultsInfo setMinimumScaleFactor:0.5f];
    
    [self.lblSearchResult setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];

    // Buttons
    [self.btnSearch.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:20.0f]];
    [self.btnShowCharacters.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
}

- (void)setupTbViewResults {
    
    if (!self.tbViewResults) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {

            self.tbViewResults = [[UITableView alloc] initWithFrame:CGRectMake(0.0f,
                                                                               115.0f,
                                                                               678.0f,
                                                                               555.0f)];
            
        } else {

            self.tbViewResults = [[UITableView alloc] initWithFrame:CGRectMake(5.0f,
                                                                               0.0f,
                                                                               self.viewResults.frame.size.width - 5.0f,
                                                                               self.viewResults.frame.size.height)];
            
            [self.tbViewResults setTableHeaderView:self.viewHeader];
        }
        
        [self.tbViewResults setDelegate:self];
        [self.tbViewResults setDataSource:self];
        
        [self.tbViewResults registerNib:[UINib nibWithNibName:[UIHelper deviceViewName:@"FoundProgramCell"]
                                                       bundle:nil]
                 forCellReuseIdentifier:@"customCellProgram"];
        
        [self.tbViewResults registerNib:[UINib nibWithNibName:[UIHelper deviceViewName:@"FoundEpisodeCell"]
                                                       bundle:nil]
                 forCellReuseIdentifier:@"customCellEpisode"];
        
        [self.viewResults addSubview:self.tbViewResults];
        
        
    } else {
        [self.tbViewResults reloadData];
    }
}

- (void)onKeyboardHide:(NSNotification *)notification {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self moveDownViewLeft];
    }
}

- (void)moveUpViewLeft {
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.viewLeft setFrame:CGRectMake(0.0f,
                                                            -250.0f,
                                                            self.viewLeft.frame.size.width,
                                                            self.viewLeft.frame.size.height)];
                     }];
}

- (void)moveDownViewLeft {
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.viewLeft setFrame:CGRectMake(0.0f,
                                                            0.0f,
                                                            self.viewLeft.frame.size.width,
                                                            self.viewLeft.frame.size.height)];
                     }];
}

- (void)startSearchTask {
    
    [[EventLog sharedInstance] logSearchEventWithKey:@"search"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self moveDownViewLeft];
    }
    
    [self.txtFieldSearch resignFirstResponder];
    
    [self setKeyword:self.txtFieldSearch.text];
    
    if (!self.parser) {
        self.parser = [[SearchParser alloc] initWithDelegate:self];
    }
    
    [self.viewLoading showLoadingViewWithCompletion:nil];
    
    [self.parser searchWithKeyword:self.keyword];
}

- (void)showViewLoading {
    
    [self.view setUserInteractionEnabled:NO];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.viewLoading setAlpha:1.0f];
                         
                     }];
}

- (void)hideViewLoading {
    
    [self.view setUserInteractionEnabled:YES];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self.viewLoading setAlpha:0.0f];
                         
                     }];
}

- (void)setupProgramCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath inTableView:(UITableView *)tableview {
    
    UIImageView *icon = (UIImageView *)[cell viewWithTag:1];
    UILabel *title = (UILabel *)[cell viewWithTag:2];
    UILabel *noContent = (UILabel *)[cell viewWithTag:10];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [title setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
    } else {
        [title setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:22.0f]];
    }
    
    if (self.parser.objects_programs.count == 0) {
        [icon setHidden:YES];
        [title setHidden:YES];
        [noContent setHidden:NO];
    } else {
        ProgramFound *program = [[self.parser objects_programs] objectAtIndex:indexPath.row];
        Program *prog = [[ProgramManager sharedManager] findProgramByID:program.program_id];
        
        if (prog.alternative_poster_image != nil) {
            [icon sd_setImageWithURL:[NSURL URLWithString:prog.alternative_poster_image]];
        }
        
        [title setText:program.title];
        [icon setHidden:NO];
        [title setHidden:NO];
        [noContent setHidden:YES];
    }
}

- (void)setupEpisodeCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath inTableView:(UITableView *)tableview {
    
    UIImageView *icon = (UIImageView *)[cell viewWithTag:1];
    UILabel *title = (UILabel *)[cell viewWithTag:2];
    UILabel *noContent = (UILabel *)[cell viewWithTag:10];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [title setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
    } else {
        [title setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:22.0f]];
    }
    
    UILabel *lblEpNumber = (UILabel *)[cell viewWithTag:4];
    UILabel *lblProgram = (UILabel *)[cell viewWithTag:5];
    UILabel *lblDescription = (UILabel *)[cell viewWithTag:6];
    
    if (self.parser.objects_episodes.count == 0) {
        [icon setHidden:YES];
        [title setHidden:YES];
        [noContent setHidden:NO];
        [lblEpNumber setHidden:YES];
        [lblProgram setHidden:YES];
        [lblDescription setHidden:YES];
    } else {
        
        EpisodeFound *episode = [[self.parser objects_episodes] objectAtIndex:indexPath.row];
        
        Episode *ep = [[Episode alloc] init];
        [ep setEpisode_id:episode.episode_id];
        [ep setOptimized_thumb:[NSString stringWithFormat:@"ep_busca_%li", (long)episode.episode_id]];
        [ep setThumb_image:episode.image];
        
        [icon setImage:nil];
        
        if (episode.image == nil || [episode.image isEqualToString:@""] == YES) {
            [icon setImage:[UIImage imageNamed:@"placeholder_small"]];
        } else {
            [icon sd_setImageWithURL:[NSURL URLWithString:episode.image]];
        }
        
        [title setText:episode.title];
        [lblEpNumber setHidden:YES];
        [lblProgram setText:episode.episode_program];
        [lblProgram setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:14.0f]];
        [lblDescription setText:[NSString stringWithFormat:@"TEMP.%li", (long)episode.season]];
        [lblDescription setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:14.0f]];
        
        [icon setHidden:NO];
        [title setHidden:NO];
        [noContent setHidden:YES];
        [lblEpNumber setHidden:YES];
        [lblProgram setHidden:NO];
        [lblDescription setHidden:NO];
    }
    
    if (indexPath.row == [self.parser.objects_episodes count] - 1) {
        
        if ([self.parser canLoadMoreEpisodes]) {
            [self.tbViewResults setTableFooterView:self.viewFooter];
            [self.parser loadNextPage];
        }
    } else {
        [self.tbViewResults setTableFooterView:nil];
    }
}

- (void)setupCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath inTableView:(UITableView *)tableview {

    if (tableview.numberOfSections == 2) {
        
        if (indexPath.section == 0) {
            [self setupProgramCell:cell forRowAtIndexPath:indexPath inTableView:tableview];
        } else {
            [self setupEpisodeCell:cell forRowAtIndexPath:indexPath inTableView:tableview];
        }
    } else if (tableview.numberOfSections == 1) {
        
        if (self.parser.objects_programs.count == 0) {
            [self setupEpisodeCell:cell forRowAtIndexPath:indexPath inTableView:tableview];
        } else {
            [self setupProgramCell:cell forRowAtIndexPath:indexPath inTableView:tableview];
        }
    }
}

#pragma mark - UITableView DataSource / Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    uint numberOfSections = 0;
    
    if (self.parser.objects_programs.count > 0) {
        numberOfSections++;
    }
    
    if (self.parser.objects_episodes.count > 0) {
        numberOfSections++;
    }
    
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.numberOfSections == 2) {
        
        if (section == 0) {
            
            if (self.parser.objects_programs.count == 0) {
                return 0;
            } else {
                return self.parser.objects_programs.count;
            }
            
        } else {
            
            if (self.parser.objects_episodes.count == 0) {
                return 1;
            } else {
                return self.parser.objects_episodes.count;
            }
        }
        
    } else if (tableView.numberOfSections == 1) {
        
        if (self.parser.objects_programs.count == 0) {
            return self.parser.objects_episodes.count;
        } else {
            return self.parser.objects_programs.count;
        }
        
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = nil;
    NSString *nibName = nil;
    
    if (tableView.numberOfSections == 2) {
        
        if (indexPath.section == 0) {
            identifier = @"customCellProgram";
            nibName = @"FoundProgramCell";
        } else {
            identifier = @"customCellEpisode";
            nibName = @"FoundEpisodeCell";
        }
        
    } else {
        
        if (self.parser.objects_programs.count == 0) {
            identifier = @"customCellEpisode";
            nibName = @"FoundEpisodeCell";
        } else if (self.parser.objects_episodes.count == 0) {
            identifier = @"customCellProgram";
            nibName = @"FoundProgramCell";
        }
        
    }
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    [[NSBundle mainBundle] loadNibNamed:nibName
                                  owner:self
                                options:nil];
    
    [self setupCell:cell forRowAtIndexPath:indexPath inTableView:tableView];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                           0.0f,
                                                           tableView.frame.size.width,
                                                           24.0f)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15.0f,
                                                               0.0f,
                                                               tableView.frame.size.width,
                                                               24.0f)];

    [view setBackgroundColor:[UIColor colorWithHexString:@"0xffffff"]];
    [title setTextColor:[UIColor colorWithHexString:@"0x4a4a4a"]];
    
    [title setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:18.0f]];
    
    if (tableView.numberOfSections == 2) {
        
        if (section == 0) {
            [title setText:@"PERSONAGENS"];
        } else {
            [title setText:@"VÍDEOS"];
        }
        
    } else if (tableView.numberOfSections == 1) {
        
        if (self.parser.objects_programs.count == 0) {
            [title setText:@"VÍDEOS"];
        } else if (self.parser.objects_episodes.count == 0) {
            [title setText:@"PERSONAGENS"];
        }
        
    }
    
    [view addSubview:title];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 24.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 110.0f;
    } else {
        
        if (indexPath.section == 0) {
            return 80.0f;
        } else {
            return 65.0f;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.numberOfSections == 2) {
        
        if (indexPath.section == 0) {
            
            if (self.parser.objects_programs.count > 0 ) {
                
                ProgramFound *foundProgram = [self.parser.objects_programs objectAtIndex:indexPath.row];
                
                Program *program = [[ProgramManager sharedManager] findProgramByID:foundProgram.program_id];
                
                ParentViewController *controller = (ParentViewController *)self.parentViewController;
                
                [controller displaySelectedCharacterPopUp:program];
                
                NSString *label = [NSString stringWithFormat:@"%@|personagem|%@|",
                                   [self.txtFieldSearch.text slugalize],
                                   [program.title slugalize]];
                
                [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Resultado de Busca" andLabel:label];
            }
            
        } else {
            
            if (self.parser.objects_episodes.count > 0 ) {
                
                [self.viewLoading showLoadingViewWithCompletion:nil];
                
                EpisodeFound *foundEpisode = [self.parser.objects_episodes objectAtIndex:indexPath.row];
                
                NSString *label = [NSString stringWithFormat:@"%@|videos|%@|%@",
                                   [self.txtFieldSearch.text slugalize],
                                   [foundEpisode.episode_program slugalize],
                                   [foundEpisode.title slugalize]];
                
                [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Resultado de Busca" andLabel:label];
                
                [self.parser findEpisodeInformationByID:foundEpisode.episode_id];
            }
        }
    } else {
        
        if (self.parser.objects_programs.count == 0) {
            
            if (self.parser.objects_episodes.count > 0 ) {
                
                [self.viewLoading showLoadingViewWithCompletion:nil];
                
                EpisodeFound *foundEpisode = [self.parser.objects_episodes objectAtIndex:indexPath.row];
                
                NSString *label = [NSString stringWithFormat:@"%@|videos|%@|%@",
                                   [self.txtFieldSearch.text slugalize],
                                   [foundEpisode.episode_program slugalize],
                                   [foundEpisode.title slugalize]];
                
                [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Resultado de Busca" andLabel:label];
                
                [self.parser findEpisodeInformationByID:foundEpisode.episode_id];
            }
        } else {
            
            if (self.parser.objects_programs.count > 0 ) {
                
                ProgramFound *foundProgram = [self.parser.objects_programs objectAtIndex:indexPath.row];
                
                Program *program = [[ProgramManager sharedManager] findProgramByID:foundProgram.program_id];
                
                ParentViewController *controller = (ParentViewController *)self.parentViewController;
                
                NSString *label = [NSString stringWithFormat:@"%@|videos|%@|",
                                   [self.txtFieldSearch.text slugalize],
                                   [program.title slugalize]];
                
                [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Resultado de Busca" andLabel:label];
                
                [controller displaySelectedCharacterPopUp:program];
            }
        }
        
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - SearchParser delegate

- (void)parserDidFinishWithoutResults {
    
    if (self.tbViewResults) {
        [self.tbViewResults removeFromSuperview];
        self.tbViewResults = nil;
    }
    
    [self.viewLoading hideLoadingViewWithCompletion:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {

        [self.lblResultsInfo setText:[NSString stringWithFormat:@"Nenhum resultado encontrado para \"%@\"", self.keyword]];
        
        [self.viewMap setHidden:YES];
        [self.viewNoResults setHidden:NO];
        [self.viewResults setHidden:NO];
        
    } else {
        
        [self.lblNoResultsInfo setText:[NSString stringWithFormat:@"Nenhum resultado encontrado para \"%@\"", self.keyword]];
        
        [self.viewRight setHidden:NO];
        [self.viewNoResults setHidden:NO];
        [self.viewResults setHidden:NO];
    }
}

- (void)parserDidFailedWithError:(NSError *)error {
    
    [self.viewLoading hideLoadingViewWithCompletion:nil];
    
    [UIHelper alertViewWithMessage:[NSString stringWithFormat:@"Erro ao realizar o carregamento dos dados.\nErro: #%li", (long)error.code]
                          andTitle:@"Erro"];
}

- (void)parserDidFinishLoadFirstPage {
    
    [self.tbViewResults setUserInteractionEnabled:YES];
    
    NSString *info = [NSString stringWithFormat:@"Exibindo %li de %li resultados encontrados para \"%@\"",
                      (long)(self.parser.objects_programs.count + self.parser.objects_episodes.count),
                      (long)self.parser.resultCount,
                      self.keyword];
    
    [self.lblResultsInfo setText:info];

    [self setupTbViewResults];
    
    [self.viewLoading hideLoadingViewWithCompletion:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.viewMap setHidden:YES];
        [self.viewNoResults setHidden:YES];
        [self.viewResults setHidden:NO];
        
    } else {
        
        [self.viewRight setHidden:NO];
        [self.viewNoResults setHidden:YES];
        [self.viewResults setHidden:NO];
    }
}

- (void)parserDidFinishLoadAnotherPage {
    
    NSString *info = [NSString stringWithFormat:@"Exibindo %li de %li resultados encontrados para \"%@\"", (long)(self.parser.objects_programs.count + self.parser.objects_episodes.count), (long)self.parser.resultCount, self.keyword];
    
    [self.lblResultsInfo setText:info];
    
    [self.viewLoading hideLoadingViewWithCompletion:nil];
    
    [self.tbViewResults reloadData];
}

- (void)parserDidFinishedLoadingEpisodeInfo:(NSDictionary *)episodeInfo {
    
    [self.viewLoading hideLoadingViewWithCompletion:nil];
    
    Program *program = [[ProgramManager sharedManager] findProgramByID:[[[episodeInfo objectForKey:@"program"] objectForKey:@"id"] integerValue]];
    
    Episode *episode = [[Episode alloc] initWithDictionary:episodeInfo
                                                andProgram:program];
    
    [self playEpisode:episode];
}

#pragma mark - LoginViewController delegate

- (void)playEpisode:(Episode *)episode {
    
    Login *login = [LoginManager getSavedLogin];
    
    if (login.access_token && [LoginManager isTokenActive]) {
        
        POCViewController *controller = [[POCViewController alloc] initWithNibName:[UIHelper deviceViewName:@"POCView"]
                                                                            bundle:nil];
        
        [controller setEpisode:episode];
        [controller setAccess_token:login.access_token];
        
        NSString *label = [NSString stringWithFormat:@"busca|vod|%@||fechado|gloob|%@|%@|%@|%@|%@", @(episode.id_globo_videos),
                           [episode.category slugalize],
                           [episode.program.title slugalize],
                           (episode.season > 0 ? @(episode.season) : @""),
                           (episode.number > 0 ? @(episode.number) : @""),
                           [episode.title slugalize]];
        
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Play" andLabel:label];
        [self presentViewController:controller animated:YES completion:nil];
        
        [controller hideCarouselView];
        
    } else {
        
        LoginViewController *controller = [[LoginViewController alloc] initWithNibName:[UIHelper deviceViewName:@"LoginView"]
                                                                                bundle:nil];
        
        [controller setDelegate:self];
        [controller setEpisode:episode];
        [controller setWillPlayNow:YES];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self moveUpViewLeft];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.tbViewResults setUserInteractionEnabled:NO];
    
    [self startSearchTask];

    return YES;
}

#pragma mark - IBActions

- (IBAction)startSearch:(UIButton *)sender {
    
    [self.tbViewResults setUserInteractionEnabled:NO];
    
    [self startSearchTask];
}

- (IBAction)showCharacters:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(openCharactersViewController)]) {
        [self.delegate openCharactersViewController];
    }
}

- (NSString *) screenNameForScreenEvent {
    return @"/busca";
}

@end
