//
//  SelectProfileViewController.h
//  gloob
//
//  Created by Pedro on 4/30/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobViewController.h"
#import "AvatarView.h"
#import "ProfileSelectionView.h"

@interface SelectProfileViewController : GloobViewController<ProfileSelectionViewDelegate>
@property (nonatomic, strong) void (^completionBlock)();
@end
