//
//  EpisodesListViewController.m
//  gloob
//
//  Created by zeroum on 27/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "EpisodesListViewController.h"
#import "Program.h"
#import "Episode.h"
#import "LoadingView.h"
#import "UIColor+HexColor.h"
#import "ImageCache.h"

@interface EpisodesListViewController ()

@property (weak, nonatomic) IBOutlet LoadingView *viewLoading;

@property (weak, nonatomic) IBOutlet UITableView *tbViewEpisodes;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, strong) NSMutableArray *arrayEpisodes;

- (IBAction)closeWindow:(UIButton *)sender;

- (void)setupInterface;
- (void)setupCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@implementation EpisodesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInterface];
    
    [self.program loadEpisodesWithCompletionHandler:^(BOOL success, NSError *error) {
        
        if (success) {

            [self.tbViewEpisodes reloadData];
            
            [self.viewLoading hideLoadingViewWithCompletion:^(BOOL finished) {
                
                if (finished) {
                    [self showViewContent];
                }
            }];
        }
        
        if (error) {
            
            [UIHelper alertViewWithMessage:@"Houve falha ao carregar os dados do servidor. Verifique sua conexão com a internet e tente novamente."
                                  andTitle:@"Ops!"];
        }
    }];
}

- (void)dealloc {
    [self.program.episodes removeAllObjects];
}

#pragma mark - Private

- (void)setupInterface {
    
    [self.lblTitle setText:[NSString stringWithFormat:@"Vídeos de %@", self.program.title]];
    [self.lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:25.0f]];
    [self.lblTitle setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    [self.lblTitle setAdjustsFontSizeToFitWidth:YES];
    [self.lblTitle setMinimumScaleFactor:0.75f];
    
    [self.tbViewEpisodes registerNib:[UINib nibWithNibName:[UIHelper deviceViewName:@"EpisodeListCell"]
                                                    bundle:nil]
              forCellReuseIdentifier:@"customCell"];
}

- (void)showViewContent {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.tbViewEpisodes setAlpha:1.0f];
                     }];
}

- (void)setupCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIImageView *icon = (UIImageView *)[cell viewWithTag:1];
    UILabel *episodeTitle = (UILabel *)[cell viewWithTag:2];
    UILabel *episodeNumber = (UILabel *)[cell viewWithTag:3];
    UILabel *episodeDescription = (UILabel *)[cell viewWithTag:4];
    
    Episode *episode = [[self.program episodesFromSeasonNumber:indexPath.section + 1] objectAtIndex:indexPath.row];
    
    if (episode) {
        
        [icon setHidden:YES];
        
        [ImageCache downloadAndResizeThumbFromEpisode:episode
                                withCompletionHandler:^(BOOL success, UIImage *image) {
                                    
                                    if (success) {
                                        [icon setImage:image];
                                    } else {
                                        [icon setImage:[UIImage imageNamed:@"placeholder_medium"]];
                                    }
                                    
                                    [icon setHidden:NO];
                                }];
        
        [episodeTitle setText:episode.title];
        [episodeNumber setText:[NSString stringWithFormat:@"EP.%li", (long)episode.number]];
        [episodeDescription setText:episode.ep_description];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            [episodeTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:20.0f]];
            [episodeNumber setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:12.0f]];
            [episodeDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:14.0f]];
            
        } else {
            
            [episodeTitle setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:20.0f]];
            [episodeNumber setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:12.0f]];
            [episodeDescription setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:14.0f]];
            
        }
    }
}

#pragma mark - UITableView DataSource / Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.program.seasons count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.program episodesFromSeasonNumber:section + 1] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *title = [NSString stringWithFormat:@"TEMP.%li", (long)(section + 1)];
    
    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"customCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                            forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    [[NSBundle mainBundle] loadNibNamed:@"EpisodeListCell"
                                  owner:self
                                options:nil];
    
    [self setupCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Episode *episode = [[self.program episodesFromSeasonNumber:indexPath.section + 1] objectAtIndex:indexPath.row];
    
    if ([self.delegate respondsToSelector:@selector(playEpisode:)]) {
        [self.delegate playEpisode:episode];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    CGRect frame = CGRectMake(0.0f, 0.0f, 540.0f, 21.0f);
    
    UILabel *title = [[UILabel alloc] initWithFrame:frame];
    
    [title setBackgroundColor:[UIColor colorWithHexString:@"0xEEEEEE"]];
    
    [title setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:15.0f]];
    [title setText:[NSString stringWithFormat:@" %liº TEMPORADA", (long)(section + 1)]];
    [title setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    
    return title;
}

#pragma mark - IBActions

- (IBAction)closeWindow:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

@end
