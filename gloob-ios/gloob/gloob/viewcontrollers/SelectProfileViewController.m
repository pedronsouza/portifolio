//
//  SelectProfileViewController.m
//  gloob
//
//  Created by Pedro on 4/30/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SelectProfileViewController.h"
#import "ProfileManager.h"
#import "UIColor+HexColor.h"
#import "ParentViewController.h"


@interface SelectProfileViewController ()
@property (nonatomic, strong) NSArray *profiles;
@property (nonatomic, strong) Profile *selectedProfile;
@property (weak, nonatomic) IBOutlet UIScrollView *scrProfiles;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)didClickSelectProfile:(id)sender;
@end

@implementation SelectProfileViewController

- (instancetype)init {
    self = [super initWithNibName:[UIHelper deviceViewName:@"SelectProfileView"] bundle:nil];
    
    if (self) {
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInterface];
}


- (void) setupInterface {
    self.profiles = [[ProfileManager sharedManager] getAllProfiles];
    CGSize viewSize;
    CGFloat x = (self.scrProfiles.frame.size.width / 2) + 72.5;
    CGFloat y = 0.0f;
    CGFloat space = 0.0f;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        viewSize = CGSizeMake(145.0f, 180.0f);
        space = 15.0f;
        
    } else {
        
        viewSize = CGSizeMake(100.0f, 125.0f);
        space = 5.0f;
    }
    
    CGRect frame = CGRectMake(x, y, viewSize.width, viewSize.height);
    
    if ([[ProfileManager sharedManager] canCreateMoreProfiles]) {
        frame.origin.y += 40;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            
        }
        
        UIButton *button = [[UIButton alloc] initWithFrame:frame];
        
        [button setImage:[UIImage imageNamed:@"pf_bt_adicionar"]
                forState:UIControlStateNormal];
        
        [button setImage:[UIImage imageNamed:@"pf_bt_adicionar_tap"]
                forState:UIControlStateHighlighted];
        
        [button addTarget:self action:@selector(didClickAddNewProfile:) forControlEvents:UIControlEventAllEvents];
        
        [self.scrProfiles addSubview:button];
        x -= (viewSize.width + space);
    } else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            x -= 25;
        }
    }
    
    for (int i = 0; i < self.profiles.count; i++) {
        Profile *profile = self.profiles[i];
        ProfileSelectionView * avatarView = [[ProfileSelectionView alloc]
                                             initWithProfile:profile andAvatar:profile.currentAvatar];
        [avatarView setDelegate:self];
        frame = avatarView.view.frame;
        frame.origin.x = x;
        [avatarView.view setFrame:frame];
        
        [self.scrProfiles addSubview:avatarView.view];
        [self addChildViewController:avatarView];
        x -= (viewSize.width + space);
    }
    
}

- (IBAction)didClickSelectProfile:(id)sender {
    if (self.selectedProfile) {
        [[ProfileManager sharedManager] setCurrentProfile:self.selectedProfile];
        [[ProfileManager sharedManager] saveLatestUsedProfile];
        [[ProfileManager sharedManager] saveProfilesToUserDefaults];
        [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange object:self.selectedProfile.currentAvatar];
        ParentViewController *parent = (ParentViewController *)self.parentViewController;
        [parent dismissPopUp];
        
        if (self.completionBlock) {
            self.completionBlock();
        }
    }
}

- (void) didClickAddNewProfile:(id)sender {
    __block ParentViewController *parent = (ParentViewController *) self.parentViewController;
    
    [parent dismissPopUpWithCompletionBlock:^{
        [parent openProfileCreatorWizard];
    }];
}

- (void) didSelectProfile:(Profile *)profile {
    self.selectedProfile = profile;
}

@end
