//
//  CurrentProfileViewController.m
//  gloob
//
//  Created by zeroum on 02/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "CurrentProfileViewController.h"
#import "ColoredButton.h"
#import "Episode.h"
#import "POCViewController.h"
#import "Login.h"
#import "LoginManager.h"
#import "Profile.h"
#import "ProfileManager.h"
#import "AchievementsFactory.h"
#import "SelfiesListViewController.h"
#import "ParentViewController.h"
#import "ChangeAvatarViewController.h"
#import "AchievementRepository.h"

@interface CurrentProfileViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewIphone;

@property (nonatomic, assign) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewSelfies;
@property (weak, nonatomic) IBOutlet UIScrollView *scrViewFavorites;

@property (weak, nonatomic) IBOutlet ColoredButton *btnChangeAvatar;
@property (weak, nonatomic) IBOutlet ColoredButton *btnPlayFavorites;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btNext;

@property (weak, nonatomic) IBOutlet UILabel *lblAvatarName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSelfies;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFavorites;
@property (weak, nonatomic) IBOutlet UILabel *lblNoFavorites;
@property (nonatomic, assign) CGFloat scrollViewSelfiesContentSize;
@property (nonatomic, weak) Episode *selectedFavorite;

@property (nonatomic, strong) UISwipeGestureRecognizer *swipeLeft;

@property (nonatomic, strong) UISwipeGestureRecognizer *swipeRight;

- (void)setupInterface;
- (void)setupFavorites;
- (void)handleSwipe:(UISwipeGestureRecognizer *)sender;

- (IBAction)changeAvatar:(UIButton *)sender;
- (IBAction)playAllFavorites:(UIButton *)sender;
- (IBAction)didClickNextPage:(id)sender;
- (IBAction)didClickPreviousPage:(id)sender;


@end

@implementation CurrentProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupAvatarInfo:) name:kAvatarChange object:nil];
    [self setupInterface];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    
    self.swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                               action:@selector(handleSwipe:)];
    
    [self.swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    
    self.swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(handleSwipe:)];
    
    [self.swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [self.scrViewSelfies addGestureRecognizer:self.swipeLeft];
    [self.scrViewSelfies addGestureRecognizer:self.swipeRight];
    
    if ([UIHelper isIPhone5]) {
        CGRect imgAvatarFrame = self.imgViewAvatar.frame;
        CGRect lbAvatar = self.lblAvatarName.frame;
        CGRect btnChangeAvatar = self.btnChangeAvatar.frame;
        
        CGRect lbSuaSelfie = self.lblTitleSelfies.frame;
        CGRect scrSelfiesFrame = self.scrViewSelfies.frame;
        CGRect setaEsqFrame = self.btPrevious.frame;
        CGRect setaDirFrame = self.btNext.frame;
        CGRect pageControl = self.selfiesPageControl.frame;
        
        imgAvatarFrame.origin.x += 20;
        lbAvatar.origin.x += 20;
        btnChangeAvatar.origin.x += 20;
        
        [self.imgViewAvatar setFrame:imgAvatarFrame];
        [self.lblAvatarName setFrame:lbAvatar];
        [self.btnChangeAvatar setFrame:btnChangeAvatar];
        
        lbSuaSelfie.origin.x -= 25;
        scrSelfiesFrame.origin.x -= 20;
        setaEsqFrame.origin.x -= 20;
        setaDirFrame.origin.x -= 20;
        pageControl.origin.x -= 20;
        
        [self.lblTitleSelfies setFrame:lbSuaSelfie];
        [self.scrViewSelfies setFrame:scrSelfiesFrame];
        [self.btPrevious setFrame:setaEsqFrame];
        [self.btNext setFrame:setaDirFrame];
        [self.selfiesPageControl setFrame:pageControl];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self setupFavorites];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    for (UIView *view in self.scrViewFavorites.subviews) {
        
        if ([view isKindOfClass:[FavoriteView class]]) {
            
            FavoriteView *favoriteView = (FavoriteView *)view;
            
            [favoriteView removeFromSuperview];
        }
    }
}

#pragma mark - Private

- (void)handleSwipe:(UISwipeGestureRecognizer *)sender {
    
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        
        if (self.selfiesPageControl.currentPage == 0) {
            [self.scrViewSelfies setContentOffset:CGPointMake(self.scrViewSelfies.frame.size.width, 0.0)  animated:YES];
            self.selfiesPageControl.currentPage = 1;
        }
    }
    
    if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        
        if (self.selfiesPageControl.currentPage == 1) {
            [self.scrViewSelfies setContentOffset:CGPointMake(0.0, 0.0)  animated:YES];
            self.selfiesPageControl.currentPage = 0;
        }
    }
}

- (void) setupAvatarInfo:(NSNotification *)notification {
    Avatar *currentAvatar = (Avatar *)[ProfileManager sharedManager].currentProfile.currentAvatar;
    self.lblAvatarName.text = [ProfileManager sharedManager].currentProfile.name;
    self.imgViewAvatar.image = [currentAvatar iconForProfile];
}
- (void)setupInterface {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.lblTitleSelfies setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:35.0f]];
        [self.lblAvatarName setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:25.0f]];
        [self.lblTitleFavorites setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:35.0f]];
        [self.lblNoFavorites setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:18.0f]];
        
    } else {
        [self.btnChangeAvatar.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:10.0f]];
        [self.btnPlayFavorites.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:10.0f]];
        
        
    }
    
    self.lblTitleSelfies.text = [NSString stringWithFormat:self.lblTitleSelfies.text, @([[AchievementRepository sharedInstance] getUnlockedSelfies].count)];
    
    [self setupAvatarInfo:nil];
    [self setupSelfies];
   
}

- (void) setupSelfies {
    NSMutableArray *allSelfies = [[NSMutableArray alloc] initWithArray:[[AchievementsFactory sharedInstance].selfies copy]];
    NSMutableArray *pages = [NSMutableArray new];
    
    double totalPages = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 2.0 : 3.0;
    double selfiesCount = (0.0 + allSelfies.count);
    
    NSInteger itemsPerPage = ceil(selfiesCount / totalPages);
    
    void (^addObjectToPage)() = ^void() {
        NSMutableArray *page = [NSMutableArray new];
        for (int x = 0; x < itemsPerPage; x++) {
            if (x < allSelfies.count) {
                [page addObject:[allSelfies objectAtIndex:x]];
            }
            
        }
        
        [allSelfies removeObjectsInArray:page];
        [pages addObject:page];
    };
    
    for (int i = 0; i < totalPages; i++) {
        addObjectToPage();
    }
    
    for (int i = 0; i < pages.count; i++) {
        [self loadSelfiesViewWithSelfies:pages[i] andCurrentPageIndex:i];
    }
    
    self.scrViewSelfies.contentSize = CGSizeMake(self.scrollViewSelfiesContentSize, self.scrViewSelfies.frame.size.height);
    [self.scrViewSelfies setDelegate:self];
}

- (void) loadSelfiesViewWithSelfies:(NSArray *)selfies andCurrentPageIndex:(NSInteger )pageIndex {
    SelfiesListViewController *controller = [[SelfiesListViewController alloc] initWithNibName:[UIHelper deviceViewName:@"SelfiesListView"] bundle:nil andSelfies:selfies];
    NSInteger spacing = 20;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        spacing = 0;
    }
    
    [controller willMoveToParentViewController:self];

    CGFloat controllerViewSize = controller.view.frame.size.width;
    if (pageIndex > 0) {
        [controller.view setFrame:CGRectMake((controllerViewSize * pageIndex) + spacing, 0.0f, self.scrViewSelfies.frame.size.width, self.scrViewSelfies.frame.size.height)];
    } else {
        [controller.view setFrame:CGRectMake(0.0f, 0.0f, self.scrViewSelfies.frame.size.width, self.scrViewSelfies.frame.size.height)];
    }
    self.scrollViewSelfiesContentSize += controllerViewSize;
    [self.scrViewSelfies addSubview:controller.view];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [self.scrViewSelfies bringSubviewToFront:controller.view];

}

- (void)setupFavorites {
    
    NSMutableArray *favorites = [[[ProfileManager sharedManager] currentProfile] favorites];
    
    if (favorites.count > 0) {
        
        CGSize viewSize = CGSizeMake(200.0f, 167.0f);
        CGFloat space = 20.0f;
        CGFloat x = 20.0f;
        
        NSInteger i = 0;

        for (Episode *episode in favorites) {
            
            CGRect frame = CGRectMake(x, 0.0f, viewSize.width, viewSize.height);
            
            FavoriteView *view = [[FavoriteView alloc] initWithFrame:frame
                                                            favorite:episode
                                                            delegate:self];
            
            [view setVideoIndex:i];
            
            [self.scrViewFavorites addSubview:view];
            
            x += (viewSize.width + space);
            
            i++;
        }
        
        [self.scrViewFavorites setContentSize:CGSizeMake(x + space, self.scrViewFavorites.frame.size.height)];
        
        [self.lblNoFavorites setHidden:YES];
        [self.btnPlayFavorites setHidden:NO];
        CGRect meusFavsLbl = self.lblTitleFavorites.frame;
        meusFavsLbl.origin.x = self.btnPlayFavorites.frame.origin.x;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.scrViewIphone setContentSize:CGSizeMake(self.view.frame.size.width, 400.0f)];
        }
        
    } else {
        
        [self.lblNoFavorites setHidden:NO];
        [self.btnPlayFavorites setHidden:YES];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.scrViewIphone setContentSize:CGSizeMake(self.view.frame.size.width, 280.0f)];
        }
        
    }
    
    [self.lblTitleFavorites setText:[NSString stringWithFormat:@"VÍDEOS FAVORITOS (%li)", (long)favorites.count]];
}

#pragma mark - FavoriteView Delegate

- (void)deleteFavorite:(Episode *)favorite {

    self.selectedFavorite = favorite;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Deseja apagar este vídeo da sua lista de favoritos?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Apagar", nil];
    
    [alert setTag:1];
    
    [alert show];
}

- (void)playEpisode:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from {
    
    Login *login = [LoginManager getSavedLogin];
    
    if (login.access_token && [LoginManager isTokenActive]) {
        
        POCViewController *controller = [[POCViewController alloc] initWithNibName:[UIHelper deviceViewName:@"POCView"]
                                                                            bundle:nil];
        
        [controller setEpisode:episode];
        [controller setAccess_token:login.access_token];
        [controller setArrayVideosSource:[[[ProfileManager sharedManager] currentProfile] favorites]];
        [controller setCurrentIndex:index];
        [controller setOpeningFrom:from];
        NSString *label = [NSString stringWithFormat:@"seu-perfil|vod|%@||fechado|gloob|%@|%@|%@|%@|%@", @(episode.id_globo_videos),
                           [episode.category slugalize],
                           [episode.program.title slugalize],
                           (episode.season > 0 ? @(episode.season) : @""),
                           (episode.number > 0 ? @(episode.number) : @""),
                           [episode.title slugalize]];
        
        [[GAHelper sharedInstance] trackEventWithCategory:@"Clique - Play" andLabel:label];
        [self presentViewController:controller animated:YES completion:nil];
        
    } else {
        
        LoginViewController *controller = [[LoginViewController alloc] initWithNibName:[UIHelper deviceViewName:@"LoginView"]
                                                                                bundle:nil];
        
        [controller setDelegate:self];
        [controller setEpisode:episode];
        [controller setWillPlayNow:YES];
        
        [controller setArrayVideosSource:[[[ProfileManager sharedManager] currentProfile] favorites]];
        [controller setVideoAtIndex:index];
        [controller setOpeningFrom:from];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 1) {
            
            [self.selectedFavorite removeFromFavorites];
            
            for (UIView *subview in self.scrViewFavorites.subviews) {
                
                if ([subview isKindOfClass:[FavoriteView class]]) {
                    [subview removeFromSuperview];
                }
            }
            
            [self setupFavorites];
        }
    }
}

#pragma mark - IBActions

- (IBAction)changeAvatar:(UIButton *)sender {
    [self.parentViewController.parentViewController performSelector:@selector(displayChangeAvatar) withObject:nil];
}

- (IBAction)playAllFavorites:(UIButton *)sender {
    
    Episode *episode = [[[[ProfileManager sharedManager] currentProfile] favorites] objectAtIndex:0];
    
    [self playEpisode:episode
             fromList:[[[ProfileManager sharedManager] currentProfile] favorites]
                index:0
          openingFrom:@"MEUS FAVORITOS"];
}

- (IBAction)didClickNextPage:(id)sender {
    switch (self.selfiesPageControl.currentPage) {
        case 0:
        {
            [self.scrViewSelfies setContentOffset:CGPointMake(self.scrViewSelfies.frame.size.width, 0.0)  animated:YES];
            self.selfiesPageControl.currentPage = 1;
        }
            break;
        case 1:
        {
            if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
                [self.scrViewSelfies setContentOffset:CGPointMake(self.scrViewSelfies.frame.size.width * 2, 0.0)  animated:YES];
                self.selfiesPageControl.currentPage = 2;
            }
        }
        default:
            break;
    }
}

- (IBAction)didClickPreviousPage:(id)sender {
    switch (self.selfiesPageControl.currentPage) {
        case 1:
        {
            [self.scrViewSelfies setContentOffset:CGPointMake(0.0, 0.0)  animated:YES];
            self.selfiesPageControl.currentPage = 0;
        }
            break;
        case 2:
        {
            [self.scrViewSelfies setContentOffset:CGPointMake(self.scrViewSelfies.frame.size.width , 0.0)  animated:YES];
            self.selfiesPageControl.currentPage = 1;
        }
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGFloat pageWidth = self.scrViewSelfies.frame.size.width; // you need to have a **iVar** with getter for scrollView
        float fractionalPage = self.scrViewSelfies.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        self.selfiesPageControl.currentPage = page; // you need to have a **iVar** with getter for pageControl
    }
}
#pragma mark GATrackableScreen
- (NSString *)screenNameForScreenEvent {
    return @"/seu-perfil";
}

@end
