//
//  AchievementUnlockViewController.m
//  gloob
//
//  Created by Pedro on 3/20/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "AchievementUnlockViewController.h"
#import "AchievementRepository.h"

@interface AchievementUnlockViewController ()

@end

@implementation AchievementUnlockViewController


#pragma mark Intialization
- (instancetype)initWithAchievement:(Achievement *)achievement {
    self = [super initWithNibName:[UIHelper deviceViewName:@"AchievementView"] bundle:nil];
    
    if (self) {
        self.achievement = achievement;
    }
    
    return self;
}

#pragma mark Events

- (void)viewDidLoad {
    [super viewDidLoad];
    // Setting contents for View
    if (self.achievement) {
        [[AchievementRepository sharedInstance] addUnlockedAchievementWithAchievement:self.achievement];
        self.bgImage.image = [self imageForBackground];
        [self.mainButton setTitle:[self textForMainButton] forState:UIControlStateNormal];
        self.descriptionLabel.text = self.achievement.descriptionOn;
        self.titleLabel.text = [self.achievement.title uppercaseString];
        self.subtilteLabel.text = [self.achievement.subtitle uppercaseString];
        self.avatar.image = [self.achievement iconForAchievementUnlock];
        self.lblAchievementMessage.text = [NSString stringWithFormat:self.lblAchievementMessage.text, [self textForAchievementMessage]];
        
        if ([UIHelper isIPhone5]) {
            self.bgImage.frame = CGRectMake((self.bgImage.frame.origin.x - 40), self.bgImage.frame.origin.y, [UIHelper screenSize].width, [UIHelper screenSize].height);
            
            [self.closeButton setCenter:CGPointMake(((self.closeButton.center.x - 40) + 12.0f), self.closeButton.center.y)];
            [self.container setCenter:CGPointMake((self.container.center.x - 40) + 59.0f, self.container.center.y)];
            [self.lblAchievementMessage setFrame:CGRectMake((self.lblAchievementMessage.frame.origin.x - 40), self.lblAchievementMessage.frame.origin.y, self.lblAchievementMessage.frame.size.width, self.lblAchievementMessage.frame.size.height)];
            [self.lblAchievementMessage setCenter:CGPointMake(self.lblAchievementMessage.center.x + 22, self.lblAchievementMessage.center.y)];
        }
        NSString *avatarKind = ([self.achievement isKindOfClass:[Avatar class]]) ? @"avatar" : @"selfie";
        NSString *label = [NSString stringWithFormat:@"%@|%@", avatarKind, [self.achievement.name slugalize]];
        [[GAHelper sharedInstance] trackEventWithCategory:@"Prêmio - Tipo Premio" andLabel:label];
        
    } else {
        [NSException raise:NSInternalInconsistencyException
                    format:@"You must init this class with an Achievement object", nil];
    }
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingShouldRemoveFromQueue object:self.achievement];
}

- (void)didClickClose:(id)sender {
    if ([self.delegate respondsToSelector:@selector(dismissPopUp)]) {
        [self.delegate dismissPopUp];
    }
}

- (void)didClickMainButton:(id)sender {
    [self mainButtonClickAction:sender];
}

#pragma mark Abstract Methods
- (NSString *)textForMainButton {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (void)mainButtonClickAction:(id)sender {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

- (UIImage *)imageForBackground {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSString *)textForAchievementMessage {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

@end
