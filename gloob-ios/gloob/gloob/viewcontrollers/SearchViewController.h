//
//  SearchViewController.h
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "LoginViewController.h"
#import "SearchParser.h"

@interface SearchViewController : GloobViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, SearchParserDelegate, LoginViewControllerDelegate, GATrackableScreen>

@property (nonatomic, assign) id delegate;

@end

@protocol SearchViewControllerDelegate <NSObject>

@optional

- (void)openCharactersViewController;

@end
