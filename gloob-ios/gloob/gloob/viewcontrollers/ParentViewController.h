//
//  ParentViewController.h
//  gloob
//
//  Created by zeroum on 07/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "PanelViewController.h"
#import "VideosViewController.h"
#import "GamesViewController.h"
#import "CharactersViewController.h"
#import "SearchViewController.h"
#import "SelectedCharacterViewController.h"
#import "ProfileViewController.h"
#import "ProfileCreatorViewController.h"
#import "Selfie.h"
#import <AVC/AVC.h>

@class Program;

@interface ParentViewController : GloobViewController <UIWebViewDelegate, PanelViewControllerDelegate, VideosViewControllerDelegate, SearchViewControllerDelegate, ProfileViewControllerDelegate, SelectedCharacterViewControllerDelegate, ProfileCreatorViewControllerDelegate, AVCDelegate>

- (void)loadData;
- (void) displayChangeAvatar;
- (void)dismissPopUpWithCompletionBlock:(void(^)())completionBlock;
- (void) displaySelfieDetails:(Selfie *)selfie;
- (void)loadPanelViewController;
- (IBAction)openProfileView:(UIButton *)sender;
- (void)openProfileCreatorWizard;
@end
