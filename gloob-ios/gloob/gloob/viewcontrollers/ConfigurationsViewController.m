//
//  ConfigurationsViewController.m
//  gloob
//
//  Created by zeroum on 15/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ConfigurationsViewController.h"
#import "ColoredButton.h"
#import "GloobTextField.h"
#import "Profile.h"
#import "ProfileManager.h"
#import "UIColor+HexColor.h"
#import "Login.h"
#import "LoginManager.h"
#import "UIHelper.h"
#import "ParentViewController.h"

@interface ConfigurationsViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewIphone;

@property (weak, nonatomic) IBOutlet UIView *viewInfo;

@property (weak, nonatomic) IBOutlet UIScrollView *scrViewProfiles;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleAccount;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleProfiles;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblProvider;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblUserProvider;

@property (weak, nonatomic) IBOutlet UILabel *lblNoLogin;
@property (weak, nonatomic) IBOutlet ColoredButton *btnLogOut;

@property (nonatomic, weak) Profile *temp_profile;

@property (nonatomic, assign) CGFloat contentSize;

- (IBAction)logOutFromProfile:(UIButton *)sender;

- (void)setupInterface;
- (void)setupInfo;
- (void)setupProfiles;
- (void)setupNewProfileButton;
- (void)setupNumberOfProfilesInTitle;
- (void) setupProfilesByNotification: (NSNotification *)notification;
- (void)didTouchUpInside:(UIButton *)sender;

@end

@implementation ConfigurationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInterface];
    [self setupInfo];
    [self setupProfiles];
    [self setupNumberOfProfilesInTitle];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupProfilesByNotification:) name:kAvatarChange object:nil];
}

- (void) setupProfilesByNotification: (NSNotification *)notification {
    [self setupInterface];
}

- (void)viewDidDisappear:(BOOL)animated {
    if (![LoginManager isAuthenticated]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange object:nil];
    }
}

#pragma mark - Private

- (void)setupInterface {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        [self.lblTitleAccount setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:32.0f]];
        [self.lblTitleProfiles setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:32.0f]];
        
        [self.lblName setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.lblEmail setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        [self.lblProvider setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
        [self.lblUserName setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:21.0f]];
        [self.lblUserEmail setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:21.0f]];
        [self.lblUserProvider setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:21.0f]];
        
        [self.btnLogOut.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
        [self.lblNoLogin setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:21.0f]];
        
    } else {
        
        [self.lblTitleAccount setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:26.0f]];
        [self.lblTitleProfiles setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:26.0f]];
        
        [self.lblName setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:13.0f]];
        [self.lblEmail setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:13.0f]];
        [self.lblProvider setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:13.0f]];
        
        [self.lblUserName setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:16.0f]];
        [self.lblUserEmail setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:16.0f]];
        [self.lblUserProvider setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:16.0f]];
        
        [self.btnLogOut.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
        [self.lblNoLogin setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:17.0f]];
        
        [self.scrViewIphone setContentSize:CGSizeMake([UIHelper screenSize].width, 420.0f)];
    }
}

- (void)setupInfo {
    
    Login *login = [LoginManager getSavedLogin];
    
    if (login) {
        
        [self.lblNoLogin setHidden:YES];
        [self.viewInfo setHidden:NO];
        
        if (login.name == nil || login.name.length == 0) {
            [self.lblUserName setText:@"Não disponível"];
        } else {
            [self.lblUserName setText:login.name];
        }
        
        if (login.email == nil || login.email.length == 0) {
            [self.lblUserEmail setText:@"Não disponível"];
        } else {
            [self.lblUserEmail setText:login.email];
        }
        
        if (login.operadora == nil || login.operadora.length == 0) {
            [self.lblUserProvider setText:@"Não disponível"];
        } else {
            [self.lblUserProvider setText:login.operadora];
        }
        
    } else {
        
        [self.lblNoLogin setHidden:NO];
        [self.viewInfo setHidden:YES];
    }
}

- (void)setupProfiles {
    for (int i = 0; i < self.scrViewProfiles.subviews.count; i++) {
        UIView *view = self.scrViewProfiles.subviews[i];
        [view removeFromSuperview];
    }
    
    CGSize viewSize;

    CGFloat space = 0.0f;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        viewSize = CGSizeMake(200.0f, 250.0f);
        space = 40.0f;
        
    } else {
        viewSize = CGSizeMake(150.0f, 175.0f);
        space = 10.0f;
    }
    
    for (Profile *profile in [[ProfileManager sharedManager] getAllProfiles]) {
        
        CGRect frame = CGRectMake(self.contentSize, 0.0f, viewSize.width, viewSize.height);
        
        AvatarView *view = [[AvatarView alloc] initDeletableAvatarWithFrame:frame
                                                                    profile:profile
                                                                     avatar:profile.currentAvatar
                                                                   delegate:self];
        
        if ([Profile currentProfile].profileId == profile.profileId) {
            view = [[AvatarView alloc] initDeletableAvatarWithFrame:frame
                                                            profile:profile
                                                             avatar:[Profile currentProfile].currentAvatar
                                                           delegate:self];
        }
        
        
        if ([profile isCurrentProfile]) {
            [view setSelected:YES];
        } else {
            [view setSelected:NO];
        }
        
        [self.scrViewProfiles addSubview:view];
        
        self.contentSize += (viewSize.width + space);
    }
    
    if ([[ProfileManager sharedManager] canCreateMoreProfiles]) {
        [self setupNewProfileButton];
    }
    
    [self.scrViewProfiles setContentSize:CGSizeMake(self.contentSize,
                                                    self.scrViewProfiles.frame.size.height)];
    
    [self.scrViewProfiles setFrame:CGRectMake(0.0f,
                                              self.scrViewProfiles.frame.origin.y,
                                              self.scrViewProfiles.contentSize.width,
                                              self.scrViewProfiles.frame.size.height)];
    
    [self.scrViewProfiles setCenter:CGPointMake([UIHelper screenSize].width / 2, self.scrViewProfiles.center.y)];
}

- (void)setupNumberOfProfilesInTitle {
    
    [self.lblTitleProfiles setText:[NSString stringWithFormat:@"PERFIS DA CONTA (%li/3)", (long)[[[ProfileManager sharedManager] objects] count]]];
}

- (void)setupNewProfileButton {
    
    CGSize viewSize;
    
    CGRect frame1;
    CGRect frame2;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        self.contentSize += 25.0f;
        
        viewSize = CGSizeMake(145.0f, 250.0f);
        frame1 = CGRectMake(39.0f, 40.0f, 67.0f, 71.0f);
        frame2 = CGRectMake(0.0f, 155.0f, 145.0f, 23.0f);
        
    } else {

        viewSize = CGSizeMake(145.0f, 162.0f);
        frame1 = CGRectMake(39.0f, 20.0f, 67.0f, 71.0f);
        frame2 = CGRectMake(0.0f, 107.5f, 145.0f, 23.0f);
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(self.contentSize, 0.0f, viewSize.width, viewSize.height)];
    
    [self.scrViewProfiles addSubview:view];
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame1];
    
    [button setImage:[UIImage imageNamed:@"pf_bt_adicionar"]
            forState:UIControlStateNormal];
    
    [button setImage:[UIImage imageNamed:@"pf_bt_adicionar_tap"]
            forState:UIControlStateHighlighted];
    
    [button addTarget:self
               action:@selector(didTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:button];
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame2];
    
    [label setText:@"Criar novo perfil"];
    [label setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:16.0f]];
    
    [view addSubview:label];
    
    self.contentSize += viewSize.width;
}

- (void)didTouchUpInside:(UIButton *)sender {
    ProfileViewController *parent =  (ProfileViewController *)self.parentViewController;
    
    if ([parent respondsToSelector:@selector(openProfileCreator)]) {
        [parent openProfileCreator];
    }
}

#pragma mark - Public

- (void)updateProfiles {
    
    self.contentSize = 0.0f;
    
    for (UIView *view in self.scrViewProfiles.subviews) {
        [view removeFromSuperview];
    }
    
    [self setupProfiles];
    [self setupNumberOfProfilesInTitle];
}

#pragma mark - AvatarView Delegate

- (void)didSelectProfile:(Profile *)profile {
    
    self.temp_profile = profile;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Deseja trocar de perfil?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Trocar", nil];
    
    [alert setTag:3];
    
    [alert show];
}

- (void)deleteProfile:(Profile *)profile {
    
    self.temp_profile = profile;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Deseja excluir este perfil?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Excluir", nil];
    
    [alert setTag:2];
    
    [alert show];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    switch (alertView.tag) {
        case 1: //logout
        {
            if (buttonIndex == 1) {
                [LoginManager logOutWithCompletionBlock:^{
                    ParentViewController * parent = (ParentViewController *)self.parentViewController.parentViewController;
                    [parent dismissProfile];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange object:nil];
                }];
            }
        }
            break;
            
        case 2:
        {
            if (buttonIndex == 1) {
                
                [[ProfileManager sharedManager] deleteProfile:self.temp_profile];
                
                [self updateProfiles];
            }
        }
            break;
        case 3:
        {
            if (buttonIndex == 1) {
                [[ProfileManager sharedManager] setCurrentProfile:self.temp_profile];
                [[ProfileManager sharedManager] saveLatestUsedProfile];
                [self updateProfiles];
                [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarChange object:nil];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - IBActions

- (IBAction)logOutFromProfile:(UIButton *)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sair da conta"
                                                    message:@"Deseja sair da sua conta?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Sair", nil];
    
    [alert setTag:1];
    
    [alert show];
}
#pragma mark GATrackableScreen

- (NSString *)screenNameForScreenEvent {
    return @"/configuracoes";
}
@end
