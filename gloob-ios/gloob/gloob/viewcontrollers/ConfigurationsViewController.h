//
//  ConfigurationsViewController.h
//  gloob
//
//  Created by zeroum on 15/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "AvatarView.h"
#import "GAHelper.h"

@interface ConfigurationsViewController : GloobViewController <UIAlertViewDelegate, AvatarViewDelegate, GATrackableScreen>

@property (nonatomic, assign) id delegate;

- (void)updateProfiles;

@end

@protocol ConfigurationsViewControllerDelegate <NSObject>

@optional

- (void)openProfileCreator;

@end
