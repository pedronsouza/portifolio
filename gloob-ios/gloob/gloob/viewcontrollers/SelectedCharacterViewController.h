//
//  SelectedCharacterViewController.h
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GloobViewController.h"
#import "LoginViewController.h"
#import "ProfileViewController.h"
#import "ScheduleListViewController.h"
#import "EpisodeButton.h"

@class Program;
@class Episode;

@interface SelectedCharacterViewController : GloobViewController <UICollectionViewDataSource, UICollectionViewDelegate, LoginViewControllerDelegate, ProfileViewControllerDelegate, ScheduleListViewControllerDelegate, EpisodeButtonDelegate, GATrackableScreen>

@property (nonatomic, strong) Program *program;

@property (nonatomic, assign) id delegate;

@property (nonatomic, assign) BOOL isInsidePlayer;

@end

@protocol SelectedCharacterViewControllerDelegate <NSObject>

@optional

- (void)dismissPopUp;
- (void)playSelectedEpisodeInCurrentPlayer:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from;
- (void)openCharactersView;

@end
