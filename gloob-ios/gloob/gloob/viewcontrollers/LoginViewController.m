//
//  LoginViewController.m
//  gloob
//
//  Created by zeroum on 16/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "LoginViewController.h"
#import "Login.h"
#import "LoginManager.h"
#import "Episode.h"
#import "LoadingView.h"
#import "ProfileManager.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet LoadingView *viewLoading;

- (IBAction)closeWindow:(UIButton *)sender;

- (void)loadRequest;
- (void)showViewLoading;
- (void)hideViewLoading;
- (void)getAuthenticationToken:(NSString *)code;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[GAHelper sharedInstance] trackScreenEventWithTrackableScreenDelegate:self];
    [self loadRequest];
}

#pragma mark - Private

- (void)loadRequest {
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:GLOOB_LOGIN_URL]];
    
    [self.webView loadRequest:request];
    
    [self showViewLoading];
}

- (void)showViewLoading {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.viewLoading setAlpha:1.0f];
                     }];
}

- (void)hideViewLoading {
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         [self.viewLoading setAlpha:0.0f];
                     }];
}

- (void)getAuthenticationToken:(NSString *)code {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://auth.globosat.tv/oauth/token/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0f];
    [request setHTTPMethod:@"POST"];
    
    NSString *postString = [NSString stringWithFormat:@"grant_type=authorization_code&code=%@&redirect_uri=http://gloob/token/", code];
    
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               NSDictionary *dict_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                            options:kNilOptions
                                                                                              error:nil];
                               
//                               NSLog(@"dict_content: %@", dict_content);
                               
                               Login *login = [[Login alloc] initWithDictionary:dict_content];
                               
                               [LoginManager saveLogin:login];
                               [[GAHelper sharedInstance] startNewSession];
                               
                               if (login.access_token) {
                                   
                                   __block LoginViewController *self2 = self;
                                   void (^completionBlock)() = ^void() {
                                       if (self2.willPlayNow) {
                                           if ([self2.delegate respondsToSelector:@selector(playEpisode:fromList:index:openingFrom:)]) {
                                               
                                               [self2.delegate playEpisode:self2.episode
                                                                  fromList:self2.arrayVideosSource
                                                                     index:self2.videoAtIndex
                                                               openingFrom:self2.openingFrom];
                                           }
                                           
                                           if ([self2.delegate respondsToSelector:@selector(playEpisode:)]) {
                                               [self2.delegate playEpisode:self2.episode];
                                           }
                                           if (self2.afterCloseBlock) {
                                               self2.afterCloseBlock();
                                           }
                                       }
                                   };
                                   
                                   NSArray *profiles = [[ProfileManager sharedManager] getAllProfiles];
                                   
                                   if (profiles && profiles.count > 0) {
                                       [[NSNotificationCenter defaultCenter] postNotificationName:kLoginWithProfile object:completionBlock];
                                   } else {
                                       completionBlock();
                                   }
                                   
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }
                           }];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 0) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            if ([self.delegate respondsToSelector:@selector(playEpisode:fromList:index:openingFrom:)]) {
                
                [self.delegate playEpisode:self.episode
                                  fromList:self.arrayVideosSource
                                     index:self.videoAtIndex
                               openingFrom:self.openingFrom];
            }
        }
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self showViewLoading];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self hideViewLoading];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([[[request URL] host] isEqualToString:@"gloob"]) {
        
        NSString *query = [[request URL] query];
        
        [self getAuthenticationToken:[[query componentsSeparatedByString:@"="] objectAtIndex:1]];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - IBActions

- (IBAction)closeWindow:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}


#pragma mark GATrackableScreen 
- (NSString *)screenNameForScreenEvent {
    return @"/login";
}
@end
