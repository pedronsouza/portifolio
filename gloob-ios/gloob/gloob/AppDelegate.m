//
//  AppDelegate.m
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "AppDelegate.h"
#import "ParentViewController.h"
#import "ScheduleAlert.h"
#import "ScheduleAlertManager.h"
#import "EventLog.h"
#import "Profile.h"
#import "GamingManager.h"
#import "ProfileManager.h"
#import "CSCore.h"

#import "RulesFactory.h"
#import "AchievementsFactory.h"

#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

- (void)setupPermissionForLocalNotificationForApplication:(UIApplication *)application;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [NewRelicAgent startWithApplicationToken:@"AA679de280d5c4c482b3c10e82baeed4136a67afc5"];
    
    [Crashlytics startWithAPIKey:@"8577fe703edf53982bb2038ff8b6bc3c3c0b0f91"];
    
    [CSComScore setAppContext];
    
    [CSComScore setAppName:@"Gloob Play App"];
    [CSComScore setCustomerC2:@"6035227"];
    [CSComScore setPublisherSecret:@"8a413f756f67be3002f0f236a2f6ac95"];
    
    [GAHelper sharedInstance]; // initializing the tracker on app bootstrap
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    
    [ProfileManager sharedManager];
    
    ParentViewController *controller = [[ParentViewController alloc] initWithNibName:[UIHelper deviceViewName:@"ParentView"]
                                                                              bundle:nil];
    
    
    [self.window setRootViewController:controller];
    
    [self setupPermissionForLocalNotificationForApplication:application];
    
    // Warm up data for Gmaing
    [GamingManager sharedInstance];
    [[RulesFactory sharedInstance] warmUp];
    [[AchievementsFactory sharedInstance] warmUp];
    [[RulesFactory sharedInstance] warmUpAchievementsForRules];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [[ProfileManager sharedManager] recoverLatestUsedProfile];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShouldMarkAccessEvent];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[ProfileManager sharedManager] saveLatestUsedProfile];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [[ProfileManager sharedManager] saveLatestUsedProfile];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[ProfileManager sharedManager] recoverLatestUsedProfile];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[ProfileManager sharedManager] saveLatestUsedProfile];
}

#pragma mark - UILocalNotification

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    ScheduleAlert *schedule = [[ScheduleAlertManager sharedManager] getScheduleByNotification:notification];
    
    if (schedule) {
        
        NSString *message = [NSString stringWithFormat:@"Dentro de instantes, EP.%li - %@ de %@ no canal GLOOB", (long)schedule.numeroEpisodio, schedule.nomeOriginal, schedule.nomePrograma];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta de programa"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }
}

#pragma mark - Private

- (void)setupPermissionForLocalNotificationForApplication:(UIApplication *)application {
    
    //-- Set Notification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    } else  {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
}

@end
