//
//  AppDelegate.h
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProgramManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *access_token;

@end
