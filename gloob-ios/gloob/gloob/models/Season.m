//
//  Season.m
//  gloob
//
//  Created by zeroum on 25/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "Season.h"

#define KEY_SEASON_NUMBER @"number"
#define KEY_SEASON_YEAR @"year"

@implementation Season

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.number = [self parseIntegerFromKey:[dictionary objectForKey:KEY_SEASON_NUMBER]];
        
        self.year = [self parseIntegerFromKey:[dictionary objectForKey:KEY_SEASON_YEAR]];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super init]) {
        self.number = [aDecoder decodeIntegerForKey:KEY_SEASON_NUMBER];
        
        self.year = [aDecoder decodeIntegerForKey:KEY_SEASON_YEAR];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.number forKey:KEY_SEASON_NUMBER];
    
    [aCoder encodeInteger:self.year forKey:KEY_SEASON_YEAR];
}

@end
