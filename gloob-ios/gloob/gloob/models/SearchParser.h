//
//  SearchParser.h
//  gloob
//
//  Created by zeroum on 28/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

#define EMPTY_FIELD @"https://api.vod.globosat.tv/globosatplay/search.json?q=&channel=gloob"

@interface SearchParser : NSObject

@property (nonatomic, strong) NSMutableArray *objects_programs;
@property (nonatomic, strong) NSMutableArray *objects_episodes;

@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, assign) NSInteger resultCount;

@property (nonatomic, assign) id delegate;

- (id)initWithDelegate:(id)delegate;

- (void)searchWithKeyword:(NSString *)keyword;
- (BOOL)canLoadMorePrograms;
- (BOOL)canLoadMoreEpisodes;
- (void)loadNextPage;

- (void)findEpisodeInformationByID:(NSInteger)episodeID;

@end

@protocol SearchParserDelegate <NSObject>

- (void)parserDidFinishWithoutResults;
- (void)parserDidFailedWithError:(NSError *)error;
- (void)parserDidFinishLoadFirstPage;
- (void)parserDidFinishLoadAnotherPage;
- (void)parserDidFinishedLoadingEpisodeInfo:(NSDictionary *)episodeInfo;

@end


