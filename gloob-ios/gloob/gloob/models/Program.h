//
//  Program.h
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GloobModel.h"

@interface Program : GloobModel <NSCoding>

@property (nonatomic, assign) NSInteger program_id;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *alternative_poster_image;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *prog_description;
@property (nonatomic, strong) NSMutableArray *seasons;

@property (nonatomic, strong) NSString *optimized_thumb;

@property (nonatomic, strong) NSMutableArray *episodes;
@property (nonatomic, strong) NSString *nextPage;

@property (nonatomic, assign) BOOL isBookmarked;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

typedef void(^completionBlock)(BOOL success, NSError *error);

- (void)loadEpisodesWithCompletionHandler:(completionBlock)finished;

- (NSArray *)episodesFromSeasonNumber:(NSInteger)number;
//- (void)bookmark;

@end
