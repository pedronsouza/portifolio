//
//  PlayRule.m
//  gloob
//
//  Created by Pedro on 3/25/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "PlayRule.h"
#import "Event.h"
#import "Profile.h"
#import "EventRepository.h"
#import "EventKeyFactory.h"
@implementation PlayRule

- (BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (event.value) {
        return (((NSNumber *)event.value).integerValue >= self.eventAmount);
    }
    
    return NO;
}

- (BOOL)shouldMark {
    return (![self isValidated]);
}


- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:self.condition
                                                                            andParams:@[
                                                                                        @(profile.profileId),
                                                                                        @([self ruleId])]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

@end
