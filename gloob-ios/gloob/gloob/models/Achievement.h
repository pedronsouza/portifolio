//
//  Achievement.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Achievement : NSObject<NSCoding>
@property (nonatomic, assign) NSInteger achievementId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *subtitle;
@property (nonatomic, strong) NSString *descriptionOff;
@property (nonatomic, strong) NSString *descriptionOn;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSArray *rules;

- (UIViewController *) viewControllerForAchievementUnlocked;
- (instancetype)initWithAchievementRaw:(NSDictionary *)achievementRaw;
- (BOOL) hasAlreadyUnlocked;
- (instancetype) initWithAvatarId:(NSInteger)avatarId;
- (instancetype) initWithSelfieId:(NSInteger)selfieId;
+ (NSArray *) createDefaultAchievements;
- (UIImage *)iconForProfile;
- (UIImage *) iconForAchievementUnlock;
@end
