//
//  SearchRule.m
//  gloob
//
//  Created by ZeroUm on 13/07/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SearchRule.h"
#import "EventRepository.h"
#import "Event.h"
#import "Profile.h"
#import "EventKeyFactory.h"

@implementation SearchRule

- (BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (event.value) {
        return (((NSNumber *)event.value).integerValue >= self.eventAmount);
    }
    
    return NO;
}

- (BOOL)shouldMark {
    return (![self isValidated]);
}


- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:@"search"
                                                                            andParams:@[
                                                                                        @(profile.profileId),
                                                                                        self.condition]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

@end
