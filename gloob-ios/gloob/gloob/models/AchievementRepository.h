//
//  AchievementRepository.h
//  gloob
//
//  Created by Pedro on 3/20/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Achievement.h"
#import "Avatar.h"
#import "Selfie.h"
@interface AchievementRepository : NSObject
+ (AchievementRepository *)sharedInstance;
- (Achievement *) getUnlockedAchievementById:(NSInteger ) achievementId;
- (BOOL) addUnlockedAchievementWithAchievement:(Achievement *) achievement;
- (NSArray *) getUnlockedAchievements;
- (NSArray *) getUnlockedAvatars;
- (NSArray *) getUnlockedSelfies;
- (Avatar *) getUnlockedAvatarById:(NSInteger) avatarId;
- (Selfie *) getUnlockedSelfieById:(NSInteger) selfieId;
@end
