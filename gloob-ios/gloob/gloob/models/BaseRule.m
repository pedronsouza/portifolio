//
//  BaseRule.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "BaseRule.h"

#define kBaseRuleNSCodingKeyRuleRaw @"kBaseRuleNSCodingKeyRuleRaw"
#define kBaseRuleNSCodingKeyRuleId @"kBaseRuleNSCodingKeyRuleId"
#define kBaseRuleNSCodingKeyRuleDescription @"kBaseRuleNSCodingKeyRuleDescription"
#define kBaseRuleNSCodingKeyInterval @"kBaseRuleNSCodingKeyInterval"
#define kBaseRuleNSCodingKeyEventAmount @"kBaseRuleNSCodingKeyEventAmount"
#define kBaseRuleNSCodingKeyCondition @"kBaseRuleNSCodingKeyCondition"
#define kBaseRuleNSCodingKeyAchievements @"kBaseRuleNSCodingKeyAchievements"
#define kBaseRuleNSCodingKeyAvatarsRaw @"kBaseRuleNSCodingKeyAvatarsRaw"
#define kBaseRuleNSCodingKeySelfiesRaw @"kBaseRuleNSCodingKeySelfiesRaw"
#define kBaseRuleNSCodingKeyAvatars @"kBaseRuleNSCodingKeyAvatars"
#define kBaseRuleNSCodingKeySelfies @"kBaseRuleNSCodingKeySelfies"

@interface BaseRule ()
@property (nonatomic, weak) NSArray *avatarsRaw;
@property (nonatomic, weak) NSArray *selfiesRaw;
@end
@implementation BaseRule

- (id)initWithRuleRaw:(NSDictionary *)ruleRaw {
    self = [super init];
    
    if (self) {
        self.ruleRaw =          ruleRaw;
        self.ruleId =           [ruleRaw[@"id"] intValue];
        self.ruleDescription =  ruleRaw[@"description"];
        self.interval =         [ruleRaw[@"interval"] intValue];
        self.eventAmount =      [ruleRaw[@"event_amount"] intValue];
        self.condition =         ruleRaw[@"condition"];

        self.avatarsRaw = ruleRaw[@"avatars"];
        self.selfiesRaw = ruleRaw[@"selfies"];
    }
    
    return self;
}

- (void) initAchievementsForRule {
    NSMutableArray *mutableAvatars = [NSMutableArray new];
    NSMutableArray *mutableSelfies = [NSMutableArray new];
    NSInteger avatarsSize = self.avatarsRaw.count;
    NSInteger selfiesSize = self.selfiesRaw.count;
    
    NSInteger interatorA = avatarsSize;
    NSInteger interatorB = selfiesSize;
    
    while (interatorA > 0) {
        interatorA -= 1;
        NSNumber *achievementId = self.avatarsRaw[interatorA];
        Achievement *achievement = [[Achievement alloc] initWithAvatarId:achievementId.integerValue];
        [mutableAvatars addObject:achievement];
    }
    
    while (interatorB > 0) {
        interatorB -= 1;
        NSNumber *achievementId = self.selfiesRaw[interatorB];
        [mutableSelfies addObject:[[Achievement alloc] initWithSelfieId:achievementId.integerValue]];
    }
    
    self.avatars = mutableAvatars;
    self.selfies = mutableSelfies;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        self.ruleRaw = [aDecoder decodeObjectForKey:kBaseRuleNSCodingKeyRuleRaw];
        self.ruleId = [aDecoder decodeIntegerForKey:kBaseRuleNSCodingKeyRuleId];
        self.ruleDescription = [aDecoder decodeObjectForKey:kBaseRuleNSCodingKeyRuleDescription];
        self.interval = [aDecoder decodeIntegerForKey:kBaseRuleNSCodingKeyInterval];
        self.eventAmount = [aDecoder decodeIntegerForKey:kBaseRuleNSCodingKeyEventAmount];
        self.condition = [aDecoder decodeObjectForKey:kBaseRuleNSCodingKeyCondition];
        self.avatarsRaw = [aDecoder decodeObjectForKey:kBaseRuleNSCodingKeyAvatarsRaw];
        self.avatars = [aDecoder decodeObjectForKey:kBaseRuleNSCodingKeyAvatars];
        self.selfies = [aDecoder decodeObjectForKey:kBaseRuleNSCodingKeySelfies];        
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.ruleRaw forKey:kBaseRuleNSCodingKeyRuleRaw];
    [aCoder encodeInteger:self.ruleId forKey:kBaseRuleNSCodingKeyRuleId];
    [aCoder encodeObject:self.ruleDescription forKey:kBaseRuleNSCodingKeyRuleDescription];
    [aCoder encodeInteger:self.interval forKey:kBaseRuleNSCodingKeyInterval];
    [aCoder encodeInteger:self.eventAmount forKey:kBaseRuleNSCodingKeyEventAmount];
    [aCoder encodeObject:self.condition forKey:kBaseRuleNSCodingKeyCondition];
    [aCoder encodeObject:self.avatarsRaw forKey:kBaseRuleNSCodingKeyAvatarsRaw];
    [aCoder encodeObject:self.avatars forKey:kBaseRuleNSCodingKeyAvatars];
    [aCoder encodeObject:self.selfies forKey:kBaseRuleNSCodingKeySelfies];
}

@end
