//
//  FeaturedHome.m
//  gloob
//
//  Created by zeroum on 17/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "FeaturedHome.h"

#define KEY_OPTIMIZED_THUMB @"optimized_thumb"

@implementation FeaturedHome

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.position = [self parseIntegerFromKey:[[dictionary objectForKey:@"item"] objectForKey:@"position"]];
        
        self.alternative_poster_image = [self parseStringFromKey:[[dictionary objectForKey:@"program_data"] objectForKey:@"alternative_poster_image"]];
        
        self.id_globo_videos = [self parseIntegerFromKey:[[dictionary objectForKey:@"program_data"] objectForKey:@"id_globo_videos"]];
        
        self.id_featuredHome = [self parseIntegerFromKey:[[dictionary objectForKey:@"program_data"] objectForKey:@"id"]];
        
        self.optimized_thumb = [NSString stringWithFormat:@"featuredhome_%@_%li", KEY_OPTIMIZED_THUMB,(long)self.id_featuredHome];
    }
    
    return self;
}

@end
