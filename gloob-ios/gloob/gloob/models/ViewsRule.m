//
//  ViewsRule.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ViewsRule.h"
#import "Profile.h"
#import "EventKeyFactory.h"
#import "EventRepository.h"
#import "Event.h"
#define kNSCodingKeyForProgramId @"kNSCodingKeyForProgramId"

@implementation ViewsRule

- (id)initWithRuleRaw:(NSDictionary *)ruleRaw {
    self = [super initWithRuleRaw:ruleRaw];
    
    if (self) {
        self.programId = [[ruleRaw objectForKey:@"program_id"] intValue];
    }
    
    return self;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.programId = [[self.ruleRaw objectForKey:@"program_id"] intValue];
    }
    
    return self;
}

-(BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (event) {
        return (((NSNumber *)event.value).intValue >= self.eventAmount);
    }
    
    return NO;
}


- (BOOL) shouldMark {
    Event *event = [self getEvent];
    
    if (!event.created_at) {
        return YES;
    } else {
        if (self.interval == 0) {
            return (((NSNumber *)event.value).intValue < self.eventAmount);
        } else {
            NSTimeInterval intervalBetweenDates = [[NSDate date] timeIntervalSinceDate:event.created_at];
            double secondsInAnHour = 3600;
            
            NSInteger hoursBetweenDates = intervalBetweenDates / secondsInAnHour;
            return (self.interval <= hoursBetweenDates);
        }
        
    }
    
    return YES;
}

- (Event *) getEvent {
    self.programId = [[self.ruleRaw objectForKey:@"program_id"] intValue];
    Profile *profile = [Profile currentProfile];

    if (self.programId) {
        NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:self
                                                                       andParams:@[
                                                                                   @(profile.profileId),
                                                                                   @(self.programId)]];
        Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
        return event;
    } else {
        [NSException raise:@"You must set value for programId before validate" format:nil];
    }
    
    return nil;
}
@end
