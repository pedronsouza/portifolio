//
//  ProfileManager.h
//  gloob
//
//  Created by zeroum on 24/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Profile.h"
#define kMaxProfiles 3

@class Episode;

@interface ProfileManager : NSObject

@property (nonatomic, strong) NSMutableArray *objects;

@property (nonatomic, strong) Profile *currentProfile;

+ (ProfileManager *)sharedManager;

- (Profile *)getCurrentProfile;
- (NSMutableArray *)getAllProfiles;
- (void)deleteProfile:(Profile *)profile;

- (BOOL)canCreateMoreProfiles;
- (BOOL)isSignedInAnyProfile;


- (void)verifyProfiles;
- (void)loadAllProfilesFromUserDefaults;
- (void)saveProfilesToUserDefaults;
- (void)addProfileToList:(Profile *)profile;

- (void)recoverLatestUsedProfile;
- (void)saveLatestUsedProfile;

- (void)addFavoriteEpisode:(Episode *)favorite;
- (void)removeFavoriteEpisode:(Episode *)favorite;
- (BOOL)isEpisodeFavorite:(Episode *)episode;
- (Profile *) getLatestUsedProfile;
@end
