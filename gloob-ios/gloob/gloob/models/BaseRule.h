//
//  BaseRule.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Achievement.h"

@interface BaseRule : NSObject<NSCoding>

@property (nonatomic, strong) NSDictionary *ruleRaw;
@property (nonatomic, assign) NSInteger ruleId;
@property (nonatomic, strong) NSString *ruleDescription;
@property (nonatomic, assign) NSInteger interval;
@property (nonatomic, assign) NSInteger eventAmount;
@property (nonatomic, strong) NSString *condition;
@property (nonatomic, strong) NSArray *avatars;
@property (nonatomic, strong) NSArray *selfies;

- (id) initWithRuleRaw:(NSDictionary *) ruleRaw;
- (void) initAchievementsForRule;
@end
