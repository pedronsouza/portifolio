//
//  SearchRule.h
//  gloob
//
//  Created by ZeroUm on 13/07/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "BaseRule.h"
#import "RulesProtocol.h"

@interface SearchRule : BaseRule<RulesProtocol>

@end
