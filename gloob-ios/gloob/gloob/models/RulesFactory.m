//
//  RulesFactory.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "RulesFactory.h"
#import "AccessRule.h"
#import "ViewsRule.h"
#import "DateRule.h"
#import "BirthdayRule.h"
#import "VideoRule.h"
#import "CarnavalRule.h"
#import "MarkRule.h"
#import "BookmarkRule.h"
#import "PlayRule.h"
#import "PlayUnique.h"
#import "AchievementsFactory.h"
#import "SearchRule.h"

static RulesFactory *_sharedManager = nil;

@implementation RulesFactory

+ (id)sharedInstance {
    @synchronized([RulesFactory class]) {
        if (!_sharedManager)
            _sharedManager = [[self alloc] init];
        
        return _sharedManager;
    }
    
    return nil;
}

- (void) warmUp {
    NSError *error = nil;
    
    NSString *rule_file;
    
    if (IS_DEBUG == YES) {
        rule_file = @"rules_debug";
    } else {
        rule_file = @"rules";
    }
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:rule_file ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    
    if (error) {
        NSLog(@"Error while loading rules.json file %@", error);
    }
    NSMutableArray *rulesAux = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:&error];
    NSMutableArray *rules = [NSMutableArray new];
    
    for (NSDictionary *ruleRaw in rulesAux) {
        [rules addObject:[self createInstanceWithRuleRaw:ruleRaw]];
    }
    
    self.rules = rules;
}

- (void) warmUpAchievementsForRules {
    NSArray *rules = self.rules;
    NSMutableArray *newRules = [NSMutableArray new];
    self.rules = nil;
    void (^setAchievementsBlock)(BaseRule *rule) = ^void(BaseRule *rule){
        [rule initAchievementsForRule];
        [newRules addObject:rule];
    };
    
    
    for (BaseRule *rule in rules) {
        setAchievementsBlock(rule);
    }
    
    self.rules = newRules;
}

- (id<RulesProtocol>)createInstanceWithRuleId:(NSInteger)ruleId {
    return self.rules[ruleId];
}

- (id<RulesProtocol>)createInstanceWithRuleRaw:(NSDictionary *)ruleRaw {
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"new_%@_rule:", ruleRaw[@"condition"]]);
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    return [self performSelector:selector withObject:ruleRaw];
    #pragma clang diagnostic pop
    
}


- (id<RulesProtocol>)new_access_rule:(id)ruleRaw {
    return [[AccessRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_views_rule:(id)ruleRaw {
    return [[ViewsRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_date_rule:(id)ruleRaw {
    return [[DateRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_birthday_rule:(id)ruleRaw {
    return [[BirthdayRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_video_rule:(id)ruleRaw {
    return [[VideoRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_carnaval_rule:(id)ruleRaw {
    return [[CarnavalRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_mark_rule:(id)ruleRaw {
    return [[MarkRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_bookmark_rule:(id)ruleRaw {
    return [[BookmarkRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_play_rule:(id)ruleRaw {
    return [[PlayRule alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_play_unique_rule:(id)ruleRaw {
    return [[PlayUnique alloc] initWithRuleRaw:ruleRaw];
}

- (id<RulesProtocol>)new_search_rule:(id)ruleRaw {
    return [[SearchRule alloc] initWithRuleRaw:ruleRaw];
}

- (NSString *)getRuleTypeWithRuleId:(NSInteger) ruleId {
    return ((BaseRule *)self.rules[ruleId]).condition;
}

- (NSArray *) rulesByCondition:(NSString *)condition {
    NSMutableArray *filteredRules = [NSMutableArray new];
    NSArray *rules =  self.rules;
    
    for (BaseRule *rule in rules) {
        if ([rule.condition isEqualToString:condition]) {
            [filteredRules addObject:rule];
        }
    }
    
    return filteredRules;
}
@end
