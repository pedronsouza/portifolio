//
//  AchievementsFactory.h
//  gloob
//
//  Created by Pedro on 3/24/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Avatar.h"
#import "Selfie.h"

@interface AchievementsFactory : NSObject
+ (instancetype) sharedInstance;
- (Avatar *) createInstanceWithAvatarId:(NSInteger) avatarId;
- (Selfie *) createInstanceWithSelfieId:(NSInteger) selfieId;
- (NSArray *) getDefaultAvatars;
- (void) warmUp;
@property (nonatomic, strong) NSArray *avatars;
@property (nonatomic, strong) NSArray *selfies;
@end
