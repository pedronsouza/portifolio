//
//  AccessRules.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RulesProtocol.h"
#import "BaseRule.h"
@interface AccessRule : BaseRule<RulesProtocol>

@end
