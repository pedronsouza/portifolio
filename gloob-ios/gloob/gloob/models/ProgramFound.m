//
//  ProgramFound.m
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ProgramFound.h"

#define KEY_ID @"id"
#define KEY_TITLE @"title"
#define KEY_IMAGE @"image"
#define KEY_EPISODES_COUNT @"episodes_count"
#define KEY_SEASONS_COUNT @"seasons_count"

@implementation ProgramFound

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.program_id = [self parseIntegerFromKey:[dictionary objectForKey:KEY_ID]];
        
        self.title = [self parseStringFromKey:[dictionary objectForKey:KEY_TITLE]];
        
        self.image = [self parseStringFromKey:[dictionary objectForKey:KEY_IMAGE]];
        
        self.episodes_count = [self parseIntegerFromKey:[dictionary objectForKey:KEY_EPISODES_COUNT]];
        
        self.seasons_count = [self parseIntegerFromKey:[dictionary objectForKey:KEY_SEASONS_COUNT]];
    }
    
    return self;
}

@end
