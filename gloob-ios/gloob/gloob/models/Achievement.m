//
//  Achievement.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Achievement.h"
#import "AchievementRepository.h"
#import "RulesFactory.h"
#import "GamingManager.h"
#import "AchievementsFactory.h"
#import "AvatarUnlockViewController.h"
#import "AchievementsFactory.h"

#define kAchievementNSCodingKeyName @"kAchievementNSCodingKeyName"
#define kAchievementNSCodingKeyTitle @"kAchievementNSCodingKeyTitle"
#define kAchievementNSCodingKeySubtitle @"kAchievementNSCodingKeySubtitle"
#define kAchievementNSCodingKeyDescriptionOff @"kAchievementNSCodingKeyDescriptionOff"
#define kAchievementNSCodingKeyDescriptionOn @"kAchievementNSCodingKeyDescriptionOn"
#define kAchievementNSCodingKeyIcon @"kAchievementNSCodingKeyIcon"
#define kAchievementNSCodingKeyRules @"kAchievementNSCodingKeyRules"
#define kAchievementNSCodingKeyAchievementId @"kAchievementNSCodingKeyAchievementId"


@implementation Achievement

- (BOOL)hasAlreadyUnlocked {
    Achievement *liberatedAchievement = nil;
    if ([self isKindOfClass:[Selfie class]]) {
        liberatedAchievement = [[AchievementRepository sharedInstance] getUnlockedSelfieById:self.achievementId];
    } else {
        liberatedAchievement = [[AchievementRepository sharedInstance] getUnlockedAvatarById:self.achievementId];
    }
    return (liberatedAchievement != nil);
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.name forKey:kAchievementNSCodingKeyName];
    [aCoder encodeObject:self.title forKey:kAchievementNSCodingKeyTitle];
    [aCoder encodeObject:self.subtitle forKey:kAchievementNSCodingKeySubtitle];
    [aCoder encodeObject:self.descriptionOff forKey:kAchievementNSCodingKeyDescriptionOff];
    [aCoder encodeObject:self.descriptionOn forKey:kAchievementNSCodingKeyDescriptionOn];
    [aCoder encodeObject:self.icon forKey:kAchievementNSCodingKeyIcon];
    [aCoder encodeObject:self.rules forKey:kAchievementNSCodingKeyRules];
    [aCoder encodeInteger:self.achievementId forKey:kAchievementNSCodingKeyAchievementId];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        self.name = [aDecoder decodeObjectForKey:kAchievementNSCodingKeyName];
        self.title = [aDecoder decodeObjectForKey:kAchievementNSCodingKeyTitle];
        self.subtitle = [aDecoder decodeObjectForKey:kAchievementNSCodingKeySubtitle];
        self.descriptionOff = [aDecoder decodeObjectForKey:kAchievementNSCodingKeyDescriptionOff];
        self.descriptionOn = [aDecoder decodeObjectForKey:kAchievementNSCodingKeyDescriptionOn];
        self.icon = [aDecoder decodeObjectForKey:kAchievementNSCodingKeyIcon];
        self.rules = [aDecoder decodeObjectForKey:kAchievementNSCodingKeyRules];
        self.achievementId = [aDecoder decodeIntegerForKey:kAchievementNSCodingKeyAchievementId];
    }
    
    return self;
}

- (instancetype)initWithAchievementRaw:(NSDictionary *)achievementRaw {
    self = [super init];
    
    if (self) {
        [self setName:achievementRaw[@"achievement"]];
        [self setTitle:achievementRaw[@"title"]];
        [self setSubtitle:achievementRaw[@"subtitle"]];
        [self setDescriptionOff:achievementRaw[@"description_off"]];
        [self setDescriptionOn:achievementRaw[@"description_on"]];
        [self setIcon:achievementRaw[@"icon"]];
        [self setAchievementId: [[achievementRaw objectForKey:@"id"] intValue]];
        NSArray *rules = achievementRaw[@"rules"];
        NSMutableArray *rulesObjects = [NSMutableArray new];
        
        for (int i = 0; i < rules.count; i++) {
            [rulesObjects addObject:[[RulesFactory sharedInstance] createInstanceWithRuleId:[rules[i] integerValue]]];
        }
        
        [self setRules:rulesObjects];
    }
    
    return self;
}

- (instancetype) initWithAvatarId:(NSInteger)avatarId {
    self = [AchievementsFactory sharedInstance].avatars[avatarId];
    return self;
}

- (instancetype) initWithSelfieId:(NSInteger)selfieId {
    self = [AchievementsFactory sharedInstance].selfies[selfieId];
    return self;
}


+ (NSArray *)createDefaultAchievements {
    return [[AchievementsFactory sharedInstance] getDefaultAvatars];
}

- (UIImage *)iconForProfile {
    return nil;
}

- (UIImage *)iconForAchievementUnlock {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (UIViewController *)viewControllerForAchievementUnlocked {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}
@end
