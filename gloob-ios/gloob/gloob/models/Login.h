//
//  Login.h
//  gloob
//
//  Created by zeroum on 30/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GloobModel.h"

@interface Login : GloobModel <NSCoding>

@property (nonatomic, assign) NSInteger user_number;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *operadora;

@property (nonatomic, strong) NSString *client_id;
@property (nonatomic, strong) NSString *access_token;
@property (nonatomic, assign) NSInteger expires_in;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end
