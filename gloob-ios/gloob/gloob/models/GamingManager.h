//
//  GamingManager.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kGammmingEventHasBeenRegisteredKey @"kGammmingEventHasBeenRegisteredKey"
#define kGammmingShouldExhibitAchievement @"kGammmingShouldExhibitAchievement"
#define kGammmingShouldRemoveFromQueue @"kGammmingShouldRemoveFromQueue"
#define kGammmingShouldProcessNext @"kGammmingShouldProcessNext"
@interface GamingManager : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, readonly) NSMutableArray *achievementsQueue;
@end
