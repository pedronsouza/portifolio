//
//  PlayRule.h
//  gloob
//
//  Created by Pedro on 3/25/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "BaseRule.h"
#import "RulesProtocol.h"

@interface PlayRule : BaseRule<RulesProtocol>
@property (nonatomic, assign) NSInteger gameId;
@end
