//
//  Program.m
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "Program.h"
#import "Season.h"
#import "Episode.h"

// curl -X GET "https://api.vod.globosat.tv/globosatplay/episodes.json?program_id=74" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

#define GLOOB_ENTITY_EPISODES @"https://api.vod.globosat.tv/globosatplay/episodes.json?program_id="

#define KEY_PROGRAM_ID @"id"
#define KEY_PROGRAM_IMAGE @"image"
#define KEY_PROGRAM_ALTERNATIVE_IMAGE @"alternative_poster_image"
#define KEY_PROGRAM_TITLE @"title"
#define KEY_PROGRAM_DESCRIPTION @"description"
#define KEY_PROGRAM_SEASONS @"seasons"

#define KEY_PROGRAM_IS_BOOKMARKED @"is_bookmarked"

#define KEY_OPTIMIZED_THUMB @"optimized_thumb"

@interface Program ()

- (NSMutableArray *)loadSeasons:(NSArray *)array;
- (void)parseEpisodesJSON:(NSDictionary *)json;

@end

@implementation Program

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.program_id = [self parseIntegerFromKey:[dictionary objectForKey:KEY_PROGRAM_ID]];
        
        self.image = [self parseStringFromKey:[dictionary objectForKey:KEY_PROGRAM_IMAGE]];
        
        self.alternative_poster_image = [self parseStringFromKey:[dictionary objectForKey:KEY_PROGRAM_ALTERNATIVE_IMAGE]];
        
        self.title = [self parseStringFromKey:[dictionary objectForKey:KEY_PROGRAM_TITLE]];
        
        self.prog_description = [self parseStringFromKey:[dictionary objectForKey:KEY_PROGRAM_DESCRIPTION]];
        
        self.nextPage = [NSString stringWithFormat:@"%@%li", GLOOB_ENTITY_EPISODES, (long)self.program_id];
        
        if ([self isObjectForKeyNull:[dictionary objectForKey:KEY_PROGRAM_SEASONS]]) {
            self.seasons = [self loadSeasons:[dictionary objectForKey:KEY_PROGRAM_SEASONS]];
        }
        
        self.optimized_thumb = [NSString stringWithFormat:@"program_%@_%li", KEY_OPTIMIZED_THUMB, (long)self.program_id];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super init]) {
        
        self.program_id = [aDecoder decodeIntegerForKey:KEY_PROGRAM_ID];
        
        self.image = [aDecoder decodeObjectForKey:KEY_PROGRAM_IMAGE];
        
        self.alternative_poster_image = [aDecoder decodeObjectForKey:KEY_PROGRAM_ALTERNATIVE_IMAGE];
        
        self.title = [aDecoder decodeObjectForKey:KEY_PROGRAM_TITLE];
        
        self.prog_description = [aDecoder decodeObjectForKey:KEY_PROGRAM_DESCRIPTION];
        
        self.seasons = [aDecoder decodeObjectForKey:KEY_PROGRAM_SEASONS];
        
        self.isBookmarked = [aDecoder decodeBoolForKey:KEY_PROGRAM_IS_BOOKMARKED];
        
        self.optimized_thumb = [aDecoder decodeObjectForKey:KEY_OPTIMIZED_THUMB];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.program_id forKey:KEY_PROGRAM_ID];
    
    [aCoder encodeObject:self.image forKey:KEY_PROGRAM_IMAGE];
    
    [aCoder encodeObject:self.alternative_poster_image forKey:KEY_PROGRAM_ALTERNATIVE_IMAGE];
    
    [aCoder encodeObject:self.title forKey:KEY_PROGRAM_TITLE];
    
    [aCoder encodeObject:self.prog_description forKey:KEY_PROGRAM_DESCRIPTION];
    
    [aCoder encodeObject:self.seasons forKey:KEY_PROGRAM_SEASONS];
    
    [aCoder encodeBool:self.isBookmarked forKey:KEY_PROGRAM_IS_BOOKMARKED];
    
    [aCoder encodeObject:self.optimized_thumb forKey:KEY_OPTIMIZED_THUMB];
}

#pragma mark - Private Methods

- (NSMutableArray *)loadSeasons:(NSArray *)array {

    NSMutableArray *arraySeasons = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in array) {
        
        Season *season = [[Season alloc] initWithDictionary:dict];
        
        [arraySeasons addObject:season];
    }
    
    return arraySeasons;
}

- (void)parseEpisodesJSON:(NSDictionary *)json {

    if (self.episodes == nil) {
        
        self.episodes = [[NSMutableArray alloc] init];
    }
    
    for (NSDictionary *dict in [json objectForKey:@"results"]) {
        
        Episode *episode = [[Episode alloc] initWithDictionary:dict
                                                    andProgram:self];

        [self.episodes addObject:episode];
    }
}

#pragma mark - Public Methods

- (void)loadEpisodesWithCompletionHandler:(completionBlock)finished {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.nextPage]];
    
    [request addValue:GLOOB_AUTH_TOKEN forHTTPHeaderField:GLOOB_HEADER_FIELD];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (connectionError) {
                                   
                                   // reset initial page
                                   self.nextPage = [NSString stringWithFormat:@"%@%li", GLOOB_ENTITY_EPISODES, (long)self.program_id];
                                   
                                   finished(NO, connectionError);
                                   
                               } else {
                                   
                                   NSDictionary *dict_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                options:kNilOptions
                                                                                                  error:nil];
                                   
                                   self.nextPage = [dict_content objectForKey:@"next"];
                                   
                                   [self parseEpisodesJSON:dict_content];
                                   
                                   if (self.nextPage == (id)[NSNull null]) {
                                       
                                       // reset initial page
                                       self.nextPage = [NSString stringWithFormat:@"%@%li", GLOOB_ENTITY_EPISODES, (long)self.program_id];
                                       
                                       finished(YES, nil);
                                       
                                   } else {
                                       
                                       [self loadEpisodesWithCompletionHandler:finished];
                                   }
                               }
                           }];
}

- (NSArray *)episodesFromSeasonNumber:(NSInteger)number {
    
    NSMutableArray *episodes = [[NSMutableArray alloc] init];
    NSArray *orderedEpisodes = [[NSArray alloc] init];
    
    for (Episode *episode in self.episodes) {
        
        if (episode.season == number) {
            
            [episodes addObject:episode];
        }
    }
    
    NSSortDescriptor *sortDescriptor;
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"number"
                                                 ascending:NO];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    orderedEpisodes = [episodes sortedArrayUsingDescriptors:sortDescriptors];

    return orderedEpisodes;
}

@end
