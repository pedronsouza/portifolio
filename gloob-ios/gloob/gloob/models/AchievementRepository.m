//
//  AchievementRepository.m
//  gloob
//
//  Created by Pedro on 3/20/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "AchievementRepository.h"
#import "BaseRule.h"
#import "DateRule.h"
#import "Profile.h"
#import "BirthdayRule.h"
#import "CarnavalRule.h"

#define kAchievementsHasBeenAlreadyBeenUnlockedKey @"kAchievementHasBeenAlreadyBeenUnlockedKey_%ld"

@interface AchievementRepository ()
@property (nonatomic, strong) NSUserDefaults *preferences;
@end

@implementation AchievementRepository

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.preferences = [NSUserDefaults standardUserDefaults];
    }
    
    return self;
}

static AchievementRepository *_sharedInstance = nil;
+ (id)sharedInstance {
    @synchronized([AchievementRepository class]) {
        if (!_sharedInstance)
        _sharedInstance = [[self alloc] init];
        
        return _sharedInstance;
    }
    
    return nil;
}

- (Achievement *) getUnlockedAchievementById:(NSInteger) achievementId {
    Profile *profile = [Profile currentProfile];
    NSString *key = [NSString stringWithFormat:kAchievementsHasBeenAlreadyBeenUnlockedKey, (long)profile.profileId];
    NSData *data = [self.preferences objectForKey:key];
    NSMutableArray *unlockedAchievements = [NSMutableArray arrayWithArray:[Achievement createDefaultAchievements]];
    
    if (data) {
        unlockedAchievements = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (unlockedAchievements) {
            for (Achievement *achievement in unlockedAchievements) {
                if (achievement.achievementId == achievementId) {
                    return achievement;
                    break;
                }
            }
        }
    }
    
    return nil;
}

- (NSArray *) getUnlockedAchievements {
    Profile *profile = [Profile currentProfile];
    NSString *key = [NSString stringWithFormat:kAchievementsHasBeenAlreadyBeenUnlockedKey, (long)profile.profileId];
    NSData *data = [self.preferences objectForKey:key];
    NSMutableArray *unlockedAchievements = [NSMutableArray arrayWithArray:[Achievement createDefaultAchievements]];
    
    if (data) {
        unlockedAchievements = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return unlockedAchievements;
}

- (BOOL) addUnlockedAchievementWithAchievement:(Achievement *) achievement {
    Profile *profile = [Profile currentProfile];
    NSString *key = [NSString stringWithFormat:kAchievementsHasBeenAlreadyBeenUnlockedKey, (long)profile.profileId];
    NSMutableArray *unlockedAchievements = [NSMutableArray new];
    NSData *data = [self.preferences objectForKey:key];
    
    if (data) {
        unlockedAchievements = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    [unlockedAchievements addObject:achievement];
    data = [NSKeyedArchiver archivedDataWithRootObject:unlockedAchievements];
    [self.preferences setObject:data forKey:key];
    if ([self.preferences synchronize]) {
        NSLog(@"New Achievement Unlock For Current User achievement: %@", achievement.name);
        return YES;
    }
    
    return NO;
}

- (NSArray *) getUnlockedAvatars {
    Profile *profile = [Profile currentProfile];
    NSString *key = [NSString stringWithFormat:kAchievementsHasBeenAlreadyBeenUnlockedKey, (long)profile.profileId];
    NSData *data = [self.preferences objectForKey:key];
    NSMutableArray *unlockedAvatars = [NSMutableArray arrayWithArray:[Achievement createDefaultAchievements]];
    
    if (data) {
        __block NSMutableArray *unlockedAchievements = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        for (Achievement *achievement in unlockedAchievements) {
            if ([achievement isKindOfClass:[Avatar class]]) {
                [self validateDateRulesFromAvatar:((Avatar *)achievement) andCompletionBlock:^(BOOL shouldConsiderAsUnlocked) {
                    if (shouldConsiderAsUnlocked) {
                        [unlockedAvatars addObject:achievement];
                    }
                }];
            }
        }
    }
    
    return unlockedAvatars;
}

- (NSArray *) getAllEverUnlockedAvatars {
    Profile *profile = [Profile currentProfile];
    NSString *key = [NSString stringWithFormat:kAchievementsHasBeenAlreadyBeenUnlockedKey, (long)profile.profileId];
    NSData *data = [self.preferences objectForKey:key];
    NSMutableArray *unlockedAvatars = [NSMutableArray arrayWithArray:[Achievement createDefaultAchievements]];
    
    if (data) {
        __block NSMutableArray *unlockedAchievements = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        for (Achievement *achievement in unlockedAchievements) {
            if ([achievement isKindOfClass:[Avatar class]]) {
                [unlockedAvatars addObject:achievement];
            }
        }
    }
    
    return unlockedAvatars;
}


- (NSArray *) getUnlockedSelfies {
    Profile *profile = [Profile currentProfile];
    NSString *key = [NSString stringWithFormat:kAchievementsHasBeenAlreadyBeenUnlockedKey, (long)profile.profileId];
    NSData *data = [self.preferences objectForKey:key];
    NSMutableArray *unlockedSelfies = [NSMutableArray new];
    
    if (data) {
        NSMutableArray *unlockedAchievements = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        for (Achievement *achievement in unlockedAchievements) {
            if ([achievement isKindOfClass:[Selfie class]]) {
                [unlockedSelfies addObject:achievement];
            }
        }
    }
    
    return unlockedSelfies;
}

- (Avatar *) getUnlockedAvatarById:(NSInteger) avatarId {
    NSArray *unlockedAvatars = [self getAllEverUnlockedAvatars];
    if (unlockedAvatars) {
        for (Avatar *avatar in unlockedAvatars) {
            if (avatar.achievementId == avatarId) {
                return avatar;
            }
        }
    }
    
    return nil;
}
- (Selfie *) getUnlockedSelfieById:(NSInteger) selfieId; {
    NSArray *unlockedSelfies = [self getUnlockedSelfies];
    if (unlockedSelfies) {
        for (Selfie *selfie in unlockedSelfies) {
            if (selfie.achievementId == selfieId) {
                return selfie;
            }
        }
    }
    
    return nil;
}

- (void) validateDateRulesFromAvatar:(Avatar *)avatar andCompletionBlock:(void(^)(BOOL shouldConsiderAsUnlocked))completionBlock{
    BOOL shouldConsiderAsUnlocked = YES;
    for (BaseRule<RulesProtocol> *rule in avatar.rules) {
        if ([rule isKindOfClass:[DateRule class]] || [rule isKindOfClass:[BirthdayRule class]] || [rule isKindOfClass:[CarnavalRule class]]) {
            shouldConsiderAsUnlocked = ([rule isValidated]);
        }
    }
    
    if (completionBlock) {
        completionBlock(shouldConsiderAsUnlocked);
    }
}
@end
