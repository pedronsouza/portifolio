//
//  EventRepository.m
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "EventRepository.h"



@interface EventRepository ()
@property (nonatomic, strong) NSUserDefaults *preferences;
@end

@implementation EventRepository
static EventRepository *_sharedInstance = nil;

+ (id)sharedInstance {
    @synchronized([EventRepository class]) {
        if (!_sharedInstance)
            _sharedInstance = [[self alloc] init];
        
        return _sharedInstance;
    }
    
    return nil;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.preferences = [NSUserDefaults standardUserDefaults];
    }
    
    return self;
}

- (Event *) getByKey:(NSString *)key {
    NSData *data = [self.preferences objectForKey:key];
    
    if (data) {
        Event *event = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return event;
    }
    
    return [Event new];
}

- (BOOL) saveWithKey:(NSString *)key andEvent:(Event *)entity {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:entity];
    
    if (data) {
        [self.preferences setObject:data forKey:key];
        [self.preferences synchronize];
        return true;
    }
    
    return false;
}
@end
