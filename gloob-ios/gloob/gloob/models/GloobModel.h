//
//  GloobModel.h
//  gloob
//
//  Created by zeroum on 26/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GloobModel : NSObject

- (BOOL)isObjectForKeyNull:(id)keyValue;
- (NSString *)parseStringFromKey:(id)keyValue;
- (NSInteger)parseIntegerFromKey:(id)keyValue;

@end
