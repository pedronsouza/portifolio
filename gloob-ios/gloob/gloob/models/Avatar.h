//
//  Avatar.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Achievement.h"

@interface Avatar : Achievement
- (UIImage *)iconForProfile;
- (UIImage *)iconForSelection;
- (UIImage *)iconForNewProfile;
- (UIImage *)iconForHome;
- (UIImage *) iconForProfileCreation;
- (UIImage *) iconForProfileCreationSelected;
@end
