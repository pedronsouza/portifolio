//
//  AchievementsFactory.m
//  gloob
//
//  Created by Pedro on 3/24/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "AchievementsFactory.h"
#import "Achievement.h"
#import "Avatar.h"
#import "Selfie.h"

@implementation AchievementsFactory
static AchievementsFactory * _sharedInstance = nil;

- (void) warmUp {
    if (!self.avatars || !self.selfies) {
        
        NSString *avatar_file;
        NSString *selfies_file;
        
        if (IS_DEBUG == YES) {
            avatar_file = @"avatars_debug";
            selfies_file = @"selfies_debug";
        } else {
            avatar_file = @"avatars";
            selfies_file = @"selfies";
        }
        
        NSArray *avatars = [self getFileContentsWithFilename:avatar_file];
        NSArray *selfies = [self getFileContentsWithFilename:selfies_file];
        
        NSMutableArray *avatarsAux = [NSMutableArray new];
        NSMutableArray *selfiesAux = [NSMutableArray new];
        
        for (NSDictionary *avatar in avatars) {
            [avatarsAux addObject:[[Avatar alloc] initWithAchievementRaw:avatar]];
        }
        
        for (NSDictionary *selfie in selfies) {
            [selfiesAux addObject:[[Selfie alloc] initWithAchievementRaw:selfie]];
        }
        
        self.avatars = avatarsAux;
        self.selfies = selfiesAux;
    }
}


+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}
- (Avatar *)createInstanceWithAvatarId:(NSInteger)avatarId {
    return [[Avatar alloc] initWithAchievementRaw:self.avatars[avatarId]];
}

- (Selfie *)createInstanceWithSelfieId:(NSInteger)selfieId {
    return [[Selfie alloc] initWithAchievementRaw:self.selfies[selfieId]];
}

- (NSArray *) getFileContentsWithFilename:(NSString *)fileName {
    NSLog(@"Opening file: %@.json", fileName);
    NSMutableArray *fileContent = [NSMutableArray new];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
    fileContent = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:nil];
    
    return fileContent;
}

- (NSArray *)getDefaultAvatars {
    NSMutableArray *defaulAvatars = [NSMutableArray new];
    
    for (int i =0; i < self.avatars.count; i++) {
        Avatar *avatar = self.avatars[i];
        
        if (avatar.rules == nil || avatar.rules.count == 0) {
            [defaulAvatars addObject:avatar];
        }
    }
    
    return defaulAvatars;
}

@end
