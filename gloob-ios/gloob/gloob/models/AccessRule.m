//
//  AccessRules.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "AccessRule.h"
#import "Profile.h"
#import "EventKeyFactory.h"
#import "EventRepository.h"
#import "Event.h"

@implementation AccessRule

-(BOOL)isValidated {
    Event *event = [self getEvent];
    if (event.value) {
        return (((NSNumber *)event.value).integerValue >= self.eventAmount);
    }
    
    return NO;
}

- (BOOL)shouldMark {
    Event *event = [self getEvent];
    
    if (![self isValidated]) {
        if (!event.created_at) {
            return YES;
        } else {
            NSTimeInterval intervalBetweenDates = [[NSDate date] timeIntervalSinceDate:event.created_at];
            double secondsInAnHour = 3600;
            
            NSInteger hoursBetweenDates = intervalBetweenDates / secondsInAnHour;
            return (self.interval <= hoursBetweenDates);
        }
    }
    
    return NO;
}

- (Event *) getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:self
                                                                   andParams:@[
                                                                               @(profile.profileId)]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

@end
