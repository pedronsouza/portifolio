//
//  EventLog.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventLog : NSObject
+ (id)sharedInstance;
- (void)logViewEventWithProgramId:(NSInteger) programId;
- (void)logAccessEvent;
- (void)logMarkEventWithKey:(NSString *) key;
- (void)logSearchEventWithKey:(NSString *) key;
- (void) verifyAndLogDateEvents;
- (void) logPlayEventWithGameId:(NSInteger) gameId;
- (void)logBookmarkEvent;
@end
