//
//  ScheduleAlert.m
//  gloob
//
//  Created by zeroum on 10/07/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "ScheduleAlert.h"
#import "ScheduleAlertManager.h"
#import "Program.h"
#import "ProgramManager.h"

#define KEY_SCHEDULE_DATA @"data"
#define KEY_SCHEDULE_DATA_SHORT @"data_short"
#define KEY_SCHEDULE_FIRE_DATE @"fire_date"
#define KEY_SCHEDULE_NOMEPROGRAMA @"nomePrograma"
#define KEY_SCHEDULE_NOME @"nome"
#define KEY_SCHEDULE_NOMEORIGINAL @"nomeOriginal"
#define KEY_SCHEDULE_NUMEROEPISODIO @"numeroEpisodio"
#define KEY_SCHEDULE_SINOPSE @"sinopse"
#define KEY_SCHEDULE_NOTIFICATION_NAME @"notification_name"

#define KEY_SCHEDULE_PROGRAM @"program"

@interface ScheduleAlert ()

- (NSString *)rearrangeDate:(NSString *)string;
- (NSString *)getShortData:(NSString *)string;
- (NSDate *)subtractFiveMinutesFromDate:(NSDate *)date;

@end

@implementation ScheduleAlert

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.data = [self rearrangeDate:[self parseStringFromKey:[dictionary objectForKey:KEY_SCHEDULE_DATA]]];
        
        self.data_short = [self getShortData:[self parseStringFromKey:[dictionary objectForKey:KEY_SCHEDULE_DATA]]];
        
        self.nomePrograma = [self parseStringFromKey:[dictionary objectForKey:KEY_SCHEDULE_NOMEPROGRAMA]];
        
        self.nome = [self parseStringFromKey:[dictionary objectForKey:KEY_SCHEDULE_NOME]];
        
        self.nomeOriginal = [self parseStringFromKey:[dictionary objectForKey:KEY_SCHEDULE_NOMEORIGINAL]];
        
        self.numeroEpisodio = [self parseIntegerFromKey:[dictionary objectForKey:KEY_SCHEDULE_NUMEROEPISODIO]];
        
        self.sinopse = [self parseStringFromKey:[dictionary objectForKey:KEY_SCHEDULE_SINOPSE]];
        
        self.notification_name = [NSString stringWithFormat:@"%@-%@", self.data, self.nomePrograma];
        
        self.program = [[ProgramManager sharedManager] findProgramByName:self.nomePrograma];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super init]) {
        
        self.data = [aDecoder decodeObjectForKey:KEY_SCHEDULE_DATA];
        
        self.data_short = [aDecoder decodeObjectForKey:KEY_SCHEDULE_DATA_SHORT];
        
        self.fireDate = [aDecoder decodeObjectForKey:KEY_SCHEDULE_FIRE_DATE];
        
        self.nomePrograma = [aDecoder decodeObjectForKey:KEY_SCHEDULE_NOMEPROGRAMA];
        
        self.nome = [aDecoder decodeObjectForKey:KEY_SCHEDULE_NOME];
        
        self.nomeOriginal = [aDecoder decodeObjectForKey:KEY_SCHEDULE_NOMEORIGINAL];
        
        self.numeroEpisodio = [aDecoder decodeIntForKey:KEY_SCHEDULE_NUMEROEPISODIO];
        
        self.sinopse = [aDecoder decodeObjectForKey:KEY_SCHEDULE_SINOPSE];
        
        self.notification_name = [aDecoder decodeObjectForKey:KEY_SCHEDULE_NOTIFICATION_NAME];
        
        self.program = [aDecoder decodeObjectForKey:KEY_SCHEDULE_PROGRAM];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.data forKey:KEY_SCHEDULE_DATA];
    
    [aCoder encodeObject:self.data_short forKey:KEY_SCHEDULE_DATA_SHORT];
    
    [aCoder encodeObject:self.fireDate forKey:KEY_SCHEDULE_FIRE_DATE];
    
    [aCoder encodeObject:self.nomePrograma forKey:KEY_SCHEDULE_NOMEPROGRAMA];
    
    [aCoder encodeObject:self.nome forKey:KEY_SCHEDULE_NOME];
    
    [aCoder encodeObject:self.nomeOriginal forKey:KEY_SCHEDULE_NOMEORIGINAL];
    
    [aCoder encodeInteger:self.numeroEpisodio forKey:KEY_SCHEDULE_NUMEROEPISODIO];
    
    [aCoder encodeObject:self.sinopse forKey:KEY_SCHEDULE_SINOPSE];
    
    [aCoder encodeObject:self.notification_name forKey:KEY_SCHEDULE_NOTIFICATION_NAME];
    
    [aCoder encodeObject:self.program forKey:KEY_SCHEDULE_PROGRAM];
}

#pragma mark - Private Methods

- (NSString *)rearrangeDate:(NSString *)string {
    
    // XML element uses RFC 3339 date-time (see Listing 2 - Parsing an RFC 3339 date-time)
    // https://developer.apple.com/library/ios/documentation/cocoa/Conceptual/DataFormatting/Articles/dfDateFormatting10_4.html
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];

    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [rfc3339DateFormatter dateFromString:string];
    
    // Setting an NSDate for alert 5 minutes before the program start
    self.fireDate = [self subtractFiveMinutesFromDate:date];

    NSString *userVisibleDateString;
    NSString *userVisibleTimeString;
    NSString *fullDateString;
    
    if (date != nil) {
        
        userVisibleDateString = [self get:@"date" fromDate:date];
        userVisibleTimeString = [self get:@"time" fromDate:date];
        
        // Substitui o "c" por "ç" no mês de marco(março)
        NSString *dateString = [userVisibleDateString stringByReplacingOccurrencesOfString:@"c" withString:@"ç"];
        
        fullDateString = [NSString stringWithFormat:@"%@ às %@", dateString, userVisibleTimeString];
        
    } else {
        
        fullDateString = @"Não Disponível";
        
    }
    
    return [fullDateString capitalizedString];
}

- (NSString *)getShortData:(NSString *)string {
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    
    NSDate *date = [rfc3339DateFormatter dateFromString:string];
    
    NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
    
    [userVisibleDateFormatter setDateStyle:NSDateFormatterShortStyle];
    [userVisibleDateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    return [userVisibleDateFormatter stringFromDate:date];
}

- (NSString *)get:(NSString *)kind fromDate:(NSDate *)date {
    
    // Convert the date object to a user-visible date string.
    NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
    
    assert(userVisibleDateFormatter != nil);
    
    if ([kind isEqualToString:@"date"]) {
        [userVisibleDateFormatter setDateStyle:NSDateFormatterFullStyle];
    }
    
    if ([kind isEqualToString:@"time"]) {
        [userVisibleDateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }
    
    return [userVisibleDateFormatter stringFromDate:date];
}

- (NSDate *)subtractFiveMinutesFromDate:(NSDate *)date {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];

    [dateComponents setMinute:-5];
    
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0]; // Release
    
//    [dateComponents setMinute:1]; // DEBUG
//    return [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0]; // DEBUG!
}

#pragma mark - Public Methods

- (void)enableScheduleAlertNotification {
    [[ScheduleAlertManager sharedManager] addScheduleAlert:self];
}

- (void)disableScheduleAlertNotification {
    [[ScheduleAlertManager sharedManager] removeScheduleAlert:self];
}

- (BOOL)isScheduleAlertEnabled {
    return [[ScheduleAlertManager sharedManager] verifyAlert:self];
}

@end

