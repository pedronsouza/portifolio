//
//  Season.h
//  gloob
//
//  Created by zeroum on 25/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GloobModel.h"

@interface Season : GloobModel <NSCoding>

@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NSInteger year;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end
