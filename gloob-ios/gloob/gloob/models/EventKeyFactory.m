//
//  EventKeyFactory.m
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//
#import "EventKeyFactory.h"

#define kEventKeyFactoryFormatForViews      @"%@_kEventKeyFactoryFormatForViews_%@" //program id + profile id
#define kEventKeyFactoryFormatForAccess     @"%@_kEventKeyFactoryFormatForAccess" // profile id
#define kEventKeyFactoryFormatForBirthday   @"%@_kEventKeyFactoryFormatForBirthday_%@" // profile id
#define kEventKeyFactoryFormatForDate       @"%@_kEventKeyFactoryFormatForDate_%@" // profile id
#define kEventKeyFactoryFormatForPlay       @"%@_kEventKeyFactoryFormatForPlay_%@" // profile id
#define kEventKeyFactoryFormatForVideo      @"%@_kEventKeyFactoryFormatForVideo_%@" //profile id
#define kEventKeyFactoryFormatForCarnaval   @"%@_kEventKeyFactoryFormatForCarnaval_%@" //profile id + year
#define kEventKeyFactoryFormatForMark       @"%@_kEventKeyFactoryFormatForMark_%@" //profile id + mark identifier (could be anyting)
#define kEventKeyFactoryFormatForBookmark   @"%@_kEventKeyFactoryFormatForBookmark" //profile id
#define kEventKeyFactoryFormatForPlayUnique @"%@_kEventKeyFactoryFormatForPlayUnique_%@" //profile id
#define kEventKeyFactoryFormatForSearch     @"%@_kEventKeyFactoryFormatForSearch_%@" // search


@implementation EventKeyFactory
static EventKeyFactory *_sharedInstance = nil;

+ (id)sharedInstance {
    @synchronized([EventKeyFactory class]) {
        if (!_sharedInstance)
            _sharedInstance = [[self alloc] init];
        
        return _sharedInstance;
    }
    
    return nil;
}

- (NSString *) createKeyWithRuleCondition:(NSString *)condition andParams:(NSArray *) params {
    NSString *formatedSelectorName = [NSString stringWithFormat:@"keyFor_%@:", condition];
    SEL selector = NSSelectorFromString(formatedSelectorName);
    
    if ([self respondsToSelector:selector]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        return [self performSelector:selector withObject:params];
    #pragma clang diagnostic pop
        
    }
    
    return nil;
}

- (NSString *) createKeyWithRule:(BaseRule *)rule andParams:(NSArray *) params {
    NSString *formatedSelectorName = [NSString stringWithFormat:@"keyFor_%@:", rule.condition];
    SEL selector = NSSelectorFromString(formatedSelectorName);
    
    if ([self respondsToSelector:selector]) {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        return [self performSelector:selector withObject:params];
        #pragma clang diagnostic pop
        
    }
    
    return nil;
}

- (NSString *) keyFor_views:(NSArray *) params {
    
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForViews andParams:params];
}

- (NSString *) keyFor_access:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForAccess andParams:params];
}

- (NSString *) keyFor_birthday:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForBirthday andParams:params];
}

- (NSString *) keyFor_date:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForDate andParams:params];
}

- (NSString *) keyFor_video:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForVideo andParams:params];
}

- (NSString *) keyFor_carnaval:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForCarnaval andParams:params];
}

- (NSString *) keyFor_mark:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForMark andParams:params];
}

- (NSString *) keyFor_search:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForSearch andParams:params];
}

- (NSString *) keyFor_bookmark:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForBookmark andParams:params];
}

- (NSString *) keyFor_play_unique:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForPlayUnique andParams:params];
}

- (NSString *) keyFor_play:(NSArray *) params {
    return [self parseKeyWithFormatKey:kEventKeyFactoryFormatForPlay andParams:params];
}

- (NSString *) parseKeyWithFormatKey: (NSString *)key andParams:(NSArray *)params {
    NSRange range = NSMakeRange(0, [params count]);
    NSMutableData* data = [NSMutableData dataWithLength: sizeof(id) * [params count]];
    
    [params getObjects:(__unsafe_unretained id *)data.mutableBytes range:range];
    NSString *formatedKey = [[NSString alloc] initWithFormat:key arguments:data.mutableBytes];
    
    return formatedKey;
}
@end
