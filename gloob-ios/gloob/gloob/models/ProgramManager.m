//
//  ProgramManager.m
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "ProgramManager.h"
#import "Program.h"
#import "Episode.h"
#import "FeaturedHome.h"

// curl -X GET "https://api.vod.globosat.tv/globosatplay/programs.json?channel_id=1&page=2" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

// curl -X GET "https://api.vod.globosat.tv/globosatplay/programs.json?channel_id=1&page=3" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

// curl -X GET "https://api.vod.globosat.tv/globosatplay/programs.json?page=1&channel_id=1" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

// curl -X GET "https://api.vod.globosat.tv/globosatplay/episodes.json?program_id=74&season=2" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

// curl -X GET "https://api.vod.globosat.tv/globosatplay/episodes.json?channel_id=1" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

// curl -X GET "http://api.apps.globosat.tv/gloobplay/featured_home.json" -H 'Authorization: Token 55a22324ed08c3b302fa6f661778710bd63bba12'

// Episodes - curl -X GET "http://api.apps.globosat.tv/gloobplay/featured_home.json" -H 'Authorization: Token 55a22324ed08c3b302fa6f661778710bd63bba12'

// TOP5 curl -X GET "http://api.apps.globosat.tv/gloobplay/top.json" -H 'Authorization: Token 55a22324ed08c3b302fa6f661778710bd63bba12'

// Featured_Home curl -X GET "http://api.apps.globosat.tv/gloobplay/featured_home.json" -H 'Authorization: Token 55a22324ed08c3b302fa6f661778710bd63bba12'

#define GLOOB_PROGRAMS_URL @"https://api.vod.globosat.tv/globosatplay/programs.json?page=1&channel_id=1"
#define GLOOB_FEATURED_EPISODES @"https://api.vod.globosat.tv/globosatplay/episodes.json?channel_id=1"
#define GLOOB_TOP_FIVE @"http://api.apps.globosat.tv/gloobplay/top.json"
#define GLOOB_FEATURED_HOME @"http://api.apps.globosat.tv/gloobplay/featured_home.json"

#define GLOOB_EPISODES_ORDERED @"https://api.vod.globosat.tv/globosatplay/episodes.json?channel_id=1&ordering=-first_publish_date"

@interface ProgramManager ()

@property (nonatomic, assign) NSInteger video_views;

- (void)parseProgramsJSON:(NSDictionary *)json;
- (void)parseFeaturedEpisodesJSON:(NSArray *)json;
- (void)parseTopFiveJSON:(NSArray *)json;
- (void)parseFeaturedHomeJSON:(NSArray *)json;

@end

@implementation ProgramManager

static ProgramManager *_sharedManager = nil;

+ (ProgramManager *)sharedManager {
	@synchronized([ProgramManager class]) {
		if (!_sharedManager)
			_sharedManager = [[self alloc] init];
		
		return _sharedManager;
	}
	
	return nil;
}

+ (id)alloc {
	@synchronized([ProgramManager class]) {
		NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedManager = [super alloc];
		return _sharedManager;
	}
	
	return nil;
}

- (id)init {
	
    if (self = [super init]) {
        
        self.objects = [[NSMutableArray alloc] init];
        self.objectsFeaturedEpisode = [[NSMutableArray alloc] init];
        self.objectsFeaturedHome = [[NSMutableArray alloc] init];
        self.objectsTopFive = [[NSArray alloc] init];
        
        self.nextPage = GLOOB_PROGRAMS_URL;
//        self.nextPageFeaturedEpisode = GLOOB_FEATURED_EPISODES;
        self.nextPageFeaturedEpisode = GLOOB_EPISODES_ORDERED;
        self.nextPageFeaturedHome = GLOOB_FEATURED_HOME;
    }
	
	return self;
}

#pragma mark - Private

- (void)parseProgramsJSON:(NSDictionary *)json {
    
    for (NSDictionary *dict in [json objectForKey:@"results"]) {
        
        Program *program = [[Program alloc] initWithDictionary:dict];
        
        [self.objects addObject:program];
    }
}

- (void)parseFeaturedEpisodesJSON:(NSArray *)json {
    
    for (NSInteger i = 0; i < [json count]; i++) {
        
        NSInteger programID = [[[[json objectAtIndex:i] objectForKey:@"program"] objectForKey:@"id"] integerValue];
        
        Program *program = [self findProgramByID:programID];
        
        Episode *episode = [[Episode alloc] initWithDictionary:[json objectAtIndex:i]
                                                    andProgram:program];

        [self.objectsFeaturedEpisode addObject:episode];
    }
}

- (void)parseTopFiveJSON:(NSArray *)json {
    
    NSDictionary *dict_content = [json objectAtIndex:0];
    
    NSMutableArray *temp_array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict_episode in [dict_content objectForKey:@"episodes"]) {
        
        Program *program = [[Program alloc] initWithDictionary:[dict_episode objectForKey:@"program"]];
        
        Episode *episode = [[Episode alloc] initWithDictionary:dict_episode
                                                    andProgram:program];
        
        [temp_array addObject:episode];
    }
    
    NSSortDescriptor *sortDescriptor;
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"videos_views"
                                                 ascending:NO];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    self.objectsTopFive = [temp_array sortedArrayUsingDescriptors:sortDescriptors];
}

- (void)parseFeaturedHomeJSON:(NSArray *)json {
    
    NSMutableArray *temp_array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in json) {
        
        FeaturedHome *featured = [[FeaturedHome alloc] initWithDictionary:dict];
        
        [temp_array addObject:featured];
    }
    
    if (temp_array.count < 8) {
        
        for (NSDictionary *dict in json) {
            
            FeaturedHome *featured = [[FeaturedHome alloc] initWithDictionary:dict];
            
            [temp_array addObject:featured];
        }
    }
    
    NSSortDescriptor *sortDescriptor;
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position"
                                                 ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    self.objectsFeaturedHome = (NSMutableArray *)[temp_array sortedArrayUsingDescriptors:sortDescriptors];
}

#pragma mark - Public

- (void)loadProgramsWithCompletionHandler:(completionBlock)finished {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.nextPage]];
    
    [request addValue:GLOOB_AUTH_TOKEN forHTTPHeaderField:GLOOB_HEADER_FIELD];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (connectionError) {
                                   
                                   // reset initial page
                                   self.nextPage = GLOOB_PROGRAMS_URL;
                                   
                                   finished(NO, connectionError);
                                   
                               } else {
                                   
                                   NSDictionary *dict_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                options:kNilOptions
                                                                                                  error:nil];
                                   
                                   self.nextPage = [dict_content objectForKey:@"next"];
                                   
                                   [self parseProgramsJSON:dict_content];
                                   
                                   if (self.nextPage == (id)[NSNull null]) {
                                       
                                       // reset initial page
                                       self.nextPage = GLOOB_PROGRAMS_URL;
                                       
                                       finished(YES, nil);
                                       
                                   } else {
                                       
                                       [self loadProgramsWithCompletionHandler:finished];
                                       
                                   }
                               }
                               
                           }];
}

- (void)loadFeaturedEpisodesWithCompletionHandler:(completionBlock)finished {
    
    if (self.objectsFeaturedEpisode.count >= 45) {
        finished(YES, nil);
    } else {
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.nextPageFeaturedEpisode]];
        
        [request addValue:GLOOB_AUTH_TOKEN forHTTPHeaderField:GLOOB_HEADER_FIELD];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                   
                                   if (connectionError) {
                                       
                                       // reset initial page
                                       self.nextPageFeaturedEpisode = GLOOB_EPISODES_ORDERED;
                                       
                                       finished(NO, connectionError);
                                       
                                   } else {
                                       
                                       NSDictionary *dict_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                    options:kNilOptions
                                                                                                      error:nil];
                                       
                                       self.nextPageFeaturedEpisode = [dict_content objectForKey:@"next"];
                                       
                                       NSArray *array_content = [dict_content objectForKey:@"results"];
                                       
                                       [self parseFeaturedEpisodesJSON:array_content];
                                       
                                       if (self.nextPageFeaturedEpisode == (id)[NSNull null]) {
                                           
                                           // reset initial page
                                           self.nextPageFeaturedEpisode = GLOOB_EPISODES_ORDERED;
                                           
                                           finished(YES, nil);
                                           
                                       } else {
                                           
                                           [self loadFeaturedEpisodesWithCompletionHandler:finished];
                                           
                                       }
                                   }
                               }];
    }
}

- (void)loadFeaturedHomeWithCompletionHandler:(completionBlock)finished {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.nextPageFeaturedHome]];
    
    [request addValue:GLOOB_AUTH_TOKEN_B forHTTPHeaderField:GLOOB_HEADER_FIELD];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (connectionError) {
                                   
                                   // reset initial page
                                   self.nextPageFeaturedHome = GLOOB_FEATURED_HOME;
                                   
                                   finished(NO, connectionError);
                                   
                               } else {
                                   if (data == nil) {
                                       finished(NO, connectionError);
                                   } else {
                                       NSArray *array_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                options:kNilOptions
                                                                                                  error:nil];
                                       
                                       [self parseFeaturedHomeJSON:array_content];
                                       
                                       finished(YES, nil);
                                   }
                               }
                           }];
}

- (void)loadTopFiveWithCompletionHandler:(completionBlock)finished {
    
    if (self.objectsTopFive.count >= 5) {
        
        finished(YES, nil);
        
    } else {
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:GLOOB_TOP_FIVE]];
        
        [request addValue:GLOOB_AUTH_TOKEN_B forHTTPHeaderField:GLOOB_HEADER_FIELD];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                   
                                   if (connectionError) {
                                       
                                       finished(NO, connectionError);
                                       
                                   } else {
                                       
                                       NSArray *array_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                    options:kNilOptions
                                                                                                      error:nil];
                                       
                                       [self parseTopFiveJSON:array_content];
                                       
                                       finished(YES, nil);
                                   }
                               }];
    }
}

- (Program *)findProgramByID:(NSInteger)programID {
    
    Program *program;
    
    for (Program *prog in self.objects) {

        if (prog.program_id == programID) {
            
            program = prog;
            
            break;
        }
    }
    
    return program;
}

- (Program *)findProgramByName:(NSString *)programName {
    
    Program *program;
    
    for (Program *prog in self.objects) {
        
        if ([programName isEqualToString:prog.title]) {
            
            program = prog;
            
            break;
        }
    }
    
    return program;
}

@end
