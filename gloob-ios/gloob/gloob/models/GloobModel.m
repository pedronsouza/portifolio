//
//  GloobModel.m
//  gloob
//
//  Created by zeroum on 26/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobModel.h"

@implementation GloobModel

- (BOOL)isObjectForKeyNull:(id)keyValue {
    
    if (keyValue != (id)[NSNull null]) {
        return YES;
    }
    
    return NO;
}

- (NSString *)parseStringFromKey:(id)keyValue {
    
    if (keyValue != (id)[NSNull null]) {
        return keyValue;
    }
    
    return nil;
}

- (NSInteger)parseIntegerFromKey:(id)keyValue {
    
    if (keyValue != (id)[NSNull null]) {
        return [keyValue integerValue];
    }
    
    return 0;
}

@end
