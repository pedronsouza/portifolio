//
//  GamingManager.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GamingManager.h"
#import "RulesProtocol.h"
#import "Achievement.h"
#import "Avatar.h"
#import "Selfie.h"
#import "BaseRule.h"
#import "Profile.h"
#import "Event.h"
#import "ParentViewController.h"
#import "AchievementUnlockViewController.h"
#import "AchievementRepository.h"
#import "RulesFactory.h"
#import "AchievementsFactory.h"

#define kKVOAchievementsQueue @"achievementsQueue"

@interface GamingManager()
@property (nonatomic, readwrite) NSMutableArray *achievementsQueue;
@property (nonatomic, assign) BOOL queueInExecution;
@end

@implementation GamingManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.achievementsQueue = [NSMutableArray new];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didGamingEventHasBeenRegistered:)
                                                     name:kGammmingEventHasBeenRegisteredKey
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(removeGamingEventFromQueue:)
                                                     name:kGammmingShouldRemoveFromQueue
                                                   object:nil];
    }
    return self;
}

- (void) removeGamingEventFromQueue:(NSNotification *) notification {
    Achievement *achievement = notification.object;
    [self removeObjectFromProcessQueueWithDismissedAchievement:achievement];
    self.queueInExecution = false;
    [self processNextFromQueue];
}

- (void) processNextFromQueue {
    for (int i = 0; i < self.achievementsQueue.count; i++) {
        Achievement *achi = [self.achievementsQueue objectAtIndex:i];
        [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingShouldExhibitAchievement object:achi];
    }
}

static GamingManager *_sharedInstance = nil;
+ (instancetype)sharedInstance {
    @synchronized([GamingManager class]) {
        if (!_sharedInstance)
            _sharedInstance = [[self alloc] init];
        return _sharedInstance;
    }
    
    return nil;
}

-(void) didGamingEventHasBeenRegistered:(NSNotification *)notification {
    Event *event = notification.object;
    NSArray *rules = [[RulesFactory sharedInstance] rulesByCondition:event.condiditon];
    
    for (BaseRule<RulesProtocol> *rule in rules) {
        if ([rule isValidated]) {
            NSMutableArray *achievements = [NSMutableArray arrayWithArray:rule.avatars];
            [achievements addObjectsFromArray:rule.selfies];
            
            for (Achievement * achievement in achievements) {
                if ([achievement hasAlreadyUnlocked] == NO && [self isAchievementInQueue:achievement] == NO) {
                    [self.achievementsQueue addObject:achievement];
                    
                    if ([achievement isKindOfClass:[Selfie class]]) {
                        [((Selfie *)achievement) setRelatedAvatarId:[Profile currentProfile].currentAvatar.achievementId];
                    }
                }
            }
        }
    }
    
    [self processNextFromQueue];
}

- (void) removeObjectFromProcessQueueWithDismissedAchievement:(Achievement *)achievement {
    for (int i = 0; i < self.achievementsQueue.count; i++) {
        Achievement *a = self.achievementsQueue[i];
        
        if (achievement.achievementId == a.achievementId) {
            [self.achievementsQueue removeObjectAtIndex:i];
        }
    }
}

- (BOOL)isAchievementInQueue:(Achievement *)achievement  {
    for (int i = 0; i < self.achievementsQueue.count; i++) {
        Achievement *a = self.achievementsQueue[i];
        
        if (achievement.achievementId == a.achievementId) {
            return YES;
        }
    }
    
    return NO;
}

@end
