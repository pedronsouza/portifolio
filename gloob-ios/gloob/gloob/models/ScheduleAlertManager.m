//
//  ScheduleAlertManager.m
//  gloob
//
//  Created by zeroum on 10/07/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "ScheduleAlertManager.h"
#import "Program.h"
#import "Profile.h"

#define KEY_SCHEDULE_LIST @"scheduleList_%@"

#define GLOOB_SCHEDULE_XML @"http://gradephp.canaisglobosat.globo.com/uploads/gradesdoscanais/grade_canal=GLOB_dia="

@interface ScheduleAlertManager ()

@property (nonatomic, strong) NSMutableArray *objects;

- (NSMutableArray *)getDatesAheadForNumberOfDays:(NSInteger)days;
- (void)loadAllSchedules;
- (void)saveAllSchedules;
- (NSString *)keyForScheduledAlerts;
@end

@implementation ScheduleAlertManager
- (NSString *)keyForScheduledAlerts {
    Profile *profile = [Profile currentProfile];
    return [NSString stringWithFormat:KEY_SCHEDULE_LIST, @(profile.profileId)];
}

static ScheduleAlertManager *_sharedManager = nil;

+ (ScheduleAlertManager *)sharedManager {
	@synchronized([ScheduleAlertManager class]) {
		if (!_sharedManager)
			_sharedManager = [[self alloc] init];
		
		return _sharedManager;
	}
	
	return nil;
}

+ (id)alloc {
	@synchronized([ScheduleAlertManager class]) {
		NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedManager = [super alloc];
		return _sharedManager;
	}
	
	return nil;
}

- (id)init {
	
    if (self = [super init]) {
        
        if (!self.objects_alerts) {
            [self loadAllSchedules];
        }
        
        if (self.objects) {
            [self.objects removeAllObjects];
        }
    }
	
	return self;
}

#pragma mark - Private

- (NSMutableArray *)getDatesAheadForNumberOfDays:(NSInteger)days {
    
    NSMutableArray *nextFiveDays = [[NSMutableArray alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    for (NSInteger daysAhead = 0; daysAhead < days; daysAhead++) {
        
        NSDateComponents *offSetComponents = [[NSDateComponents alloc] init];
        
        [offSetComponents setDay:daysAhead];
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDate *newDate = [gregorian dateByAddingComponents:offSetComponents
                                                     toDate:[NSDate date]
                                                    options:0];
        
        [nextFiveDays addObject:newDate];
    }
    
    return nextFiveDays;
}

- (void)loadAllSchedules {
    
    self.objects_alerts = [[NSMutableArray alloc] init];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForScheduledAlerts]];
    
    if (data == nil) {
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.objects_alerts];
        
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:[self keyForScheduledAlerts]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForScheduledAlerts]];
        
        self.objects_alerts = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}

- (void)saveAllSchedules {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.objects_alerts];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:[self keyForScheduledAlerts]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Public

- (void)loadNextFiveDaysSchedule:(completionBlock)finished {
    
    self.objects = [[NSMutableArray alloc] init];
    
    NSMutableArray *arrayDates = [self getDatesAheadForNumberOfDays:5];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    for (NSDate *date in arrayDates) {
        
        NSString *filepath = [NSString stringWithFormat:@"%@%@.xml", GLOOB_SCHEDULE_XML, [dateFormatter stringFromDate:date]];
        
//        NSLog(@"filepath: %@", filepath);
        
        NSData *xmlData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:filepath]];
        
        NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
        
        [parser setDelegate:self];
        
        [parser parse];
    }
    
    if (finished != nil) {
        finished(YES, nil);
    }
}

- (NSMutableArray *)getAllSchedulesForProgramNamed:(NSString *)programNamed {
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    for (ScheduleAlert *alert in self.objects) {
        
        if ([alert.nomePrograma isEqualToString:programNamed]) {
            // Get only new dates
            if ([alert.fireDate timeIntervalSinceNow] > 0) {
                [results addObject:alert];
            }
        }
    }
    
    return results;
}
- (void) saveAlertToDatabase:(ScheduleAlert *)alert {
    NSMutableArray *alerts = [NSMutableArray new];
    [alerts addObjectsFromArray:[self getAllValidAlerts]];
    [alerts addObject:alert];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:alerts];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:[self keyForScheduledAlerts]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)addScheduleAlert:(ScheduleAlert *)alert {
    [self saveAlertToDatabase:alert];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    NSString *message = [NSString stringWithFormat:@"Dentro de instantes, EP.%li de %@ no canal GLOOB", (long)alert.numeroEpisodio, alert.nomePrograma];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:alert.notification_name forKey:@"notification_name"];
    
    NSLog(@"FIREDATE: %@", alert.fireDate);
    
    [notification setFireDate:alert.fireDate];
    [notification setAlertBody:message];
    [notification setSoundName:UILocalNotificationDefaultSoundName];
//    [notification setApplicationIconBadgeNumber:[[UIApplication sharedApplication] applicationIconBadgeNumber] + 1];
    [notification setUserInfo:userInfo];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

- (void)removeScheduleAlert:(ScheduleAlert *)alert {
    
    for (UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        
        if ([[notification.userInfo objectForKey:@"notification_name"] isEqualToString:alert.notification_name]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
    
    NSMutableArray *toDelete = [NSMutableArray array];
    self.objects_alerts = [[NSMutableArray alloc] initWithArray:[self getAllValidAlerts]];
    for (ScheduleAlert *schedule in self.objects_alerts) {
        
        if ([alert.notification_name isEqualToString:schedule.notification_name] && alert.numeroEpisodio == schedule.numeroEpisodio) {
            [toDelete addObject:schedule];
        }
    }
    
    [self.objects_alerts removeObjectsInArray:toDelete];
    
    [self saveAllSchedules];
}

- (NSMutableArray *)getAllValidAlerts {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForScheduledAlerts]];
    NSMutableArray *alerts = [NSMutableArray new];
    
    if (data != nil) {
        alerts = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    NSMutableArray *validAlerts = [[NSMutableArray alloc] init];
    
    for (ScheduleAlert *schedule in alerts) {
        if ([self verifyAlert:schedule]) {
            [validAlerts addObject:schedule];
        }
    }
    
    [alerts removeAllObjects];
    [alerts addObjectsFromArray:validAlerts];
    
    return alerts;
}

- (BOOL)verifyAlert:(ScheduleAlert *)alert {
    
    BOOL isEnabled = NO;
    
    for (UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        
        if ([[notification.userInfo objectForKey:@"notification_name"] isEqualToString:alert.notification_name]) {
            isEnabled = YES;
        }
    }
    
    return isEnabled;
}

- (ScheduleAlert *)getScheduleByNotification:(UILocalNotification *)notification {
    
    ScheduleAlert *schedule;
    
    NSString *userInfo = [notification.userInfo objectForKey:@"notification_name"];
    
    for (ScheduleAlert *sched in self.objects_alerts) {
        
        if ([sched.notification_name isEqualToString:userInfo]) {
            schedule = sched;
        }
    }
    
    return schedule;
}

+ (BOOL)checkIfProgramHaveAtLeastOneAlert:(Program *)program {
    
    BOOL value = NO;
    
    for (UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        
        NSString *userInfo = [notification.userInfo objectForKey:@"notification_name"];
        
        if ([userInfo rangeOfString:program.title].location != NSNotFound && userInfo != nil) {
            value = YES;
            break;
        }
    }
    
    return value;
}

#pragma mark - NSXMLParser Delegate

- (void)parserDidStartDocument:(NSXMLParser *)parser {
//    NSLog(@"parserDidStartDocument");

}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
//    NSLog(@"parserDidEndDocument");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ([elementName isEqualToString:@"show"]) {
        
        self.showAttributes = [[NSMutableDictionary alloc] init];
        
        return;
    }
    
    if ([elementName isEqualToString:@"data"] || [elementName isEqualToString:@"nomePrograma"] ||
        [elementName isEqualToString:@"nome"] || [elementName isEqualToString:@"nomeOriginal"] ||
        [elementName isEqualToString:@"numeroEpisodio"] || [elementName isEqualToString:@"sinopse"]) {
        
        NSString *codeValue = [attributeDict objectForKey:@"value"];
        
        if (codeValue) {
            
//            self.objects = [[NSMutableArray alloc] init];
            
        } else {
            
            self.currentTag = elementName;

            self.tagContent = [[NSMutableString alloc] init];
        }
        
        return;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([self.currentTag isEqualToString:@"data"] || [self.currentTag isEqualToString:@"nomePrograma"] ||
        [self.currentTag isEqualToString:@"nome"] || [self.currentTag isEqualToString:@"nomeOriginal"] ||
        [self.currentTag isEqualToString:@"numeroEpisodio"] || [self.currentTag isEqualToString:@"sinopse"]) {
        
        [self.tagContent appendString:string];
        
        return;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([self.currentTag isEqualToString:@"data"] || [self.currentTag isEqualToString:@"nomePrograma"] ||
        [self.currentTag isEqualToString:@"nome"] || [self.currentTag isEqualToString:@"nomeOriginal"] ||
        [self.currentTag isEqualToString:@"numeroEpisodio"] || [self.currentTag isEqualToString:@"sinopse"]) {
        
        [self.showAttributes setObject:self.tagContent forKey:self.currentTag];
        
        self.currentTag = nil;
        self.tagContent = nil;
        
        return;
    }
    
    if ([elementName isEqualToString:@"show"]) {
        
        ScheduleAlert *show = [[ScheduleAlert alloc] initWithDictionary:self.showAttributes];
        
        [self.objects addObject:show];
        
        return;
    }
}

@end
