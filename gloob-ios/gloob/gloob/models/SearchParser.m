//
//  SearchParser.m
//  gloob
//
//  Created by zeroum on 28/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "SearchParser.h"
#import "ProgramFound.h"
#import "EpisodeFound.h"
#import "NSString+URLEncoding.h"

// curl -X GET "https://api.vod.globosat.tv/globosatplay/search.json?q=gaby&channel=gloob" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'
// curl -X GET "https://api.vod.globosat.tv/globosatplay/episodes.json?q=gaby&page=2&channel=gloob" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

// curl -X GET "https://api.vod.globosat.tv/globosatplay/episodes.json?q=gaby&page=2&channel=gloob" -H 'Authorization: Token 5d4fc2ed820f6dae18b3e2e958f247d01a75d5b8'

#define BASE_SEARCH_URL @"https://api.vod.globosat.tv/globosatplay/search.json?q="
#define SEARCH_CHANNEL @"&channel=gloob"

#define BASE_EPISODE_SEARCH_URL @"https://api.vod.globosat.tv/globosatplay/episodes/"

#define KIND_PROGRAMS @"programs"
#define KIND_EPISODES @"episodes"

@interface SearchParser ()

@property (nonatomic, strong) NSString *firstPage;
@property (nonatomic, strong) NSString *nextPrograms;
@property (nonatomic, strong) NSString *nextEpisodes;

typedef void(^completionBlock)(BOOL success, NSError *error);

- (void)loadPageWithPath:(NSString *)path andCompletionHandler:(completionBlock)finished;

- (void)parseProgramsJSON:(NSDictionary *)json;
- (void)parseEpisodesJSON:(NSDictionary *)json;

@end

@implementation SearchParser

- (id)initWithDelegate:(id)delegate {
    
    if (self = [super init]) {
        
        self.delegate = delegate;
        
        self.objects_programs = [[NSMutableArray alloc] init];
        self.objects_episodes = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark - Public

- (void)searchWithKeyword:(NSString *)keyword {
    
    if (self.objects_programs.count > 0) {
        [self.objects_programs removeAllObjects];
    }

    if (self.objects_episodes.count > 0) {
        [self.objects_episodes removeAllObjects];
    }
    
    self.resultCount = 0;
    self.keyword = [keyword urlencode];
    
    self.nextPrograms = nil;
    self.nextEpisodes = nil;
    
    self.firstPage = [NSString stringWithFormat:@"%@%@%@", BASE_SEARCH_URL, self.keyword, SEARCH_CHANNEL];
    
    [self loadPageWithPath:self.firstPage
      andCompletionHandler:^(BOOL success, NSError *error) {
          
          if (success) {
              NSString *label = [NSString stringWithFormat:@"%@", [keyword slugalize]];

              if (self.resultCount == 0) {
                  label = [NSString stringWithFormat:@"%@|nenhum-resultado", [keyword slugalize]];
                  if ([self.delegate respondsToSelector:@selector(parserDidFinishWithoutResults)]) {
                      [self.delegate parserDidFinishWithoutResults];
                  }
                  
              } else {
                  
                  if ([self.delegate respondsToSelector:@selector(parserDidFinishLoadFirstPage)]) {
                      [self.delegate parserDidFinishLoadFirstPage];
                  }
              }
              
            [[GAHelper sharedInstance] trackEventWithCategory:@"Geral - Busca" andLabel:label];
          }
          
          if (error) {
              
              if ([self.delegate respondsToSelector:@selector(parserDidFailedWithError:)]) {
                  [self.delegate parserDidFailedWithError:error];
              }
          }
      }];
}

- (BOOL)canLoadMorePrograms {
    
    if (self.nextPrograms != (id)[NSNull null]) {
        return YES;
    }
    
    return NO;
}

- (BOOL)canLoadMoreEpisodes {
    
    if (self.nextEpisodes != (id)[NSNull null]) {
        return YES;
    }
    
    return NO;
}

- (void)loadNextPage {
    
   
      [self loadPageWithPath:self.nextEpisodes
        andCompletionHandler:^(BOOL success, NSError *error) {
            
            if (error) {
                
                if ([self.delegate respondsToSelector:@selector(parserDidFailedWithError:)]) {
                    [self.delegate parserDidFailedWithError:error];
                }
            }
            
            if (success) {
                
                if ([self.delegate respondsToSelector:@selector(parserDidFinishLoadAnotherPage)]) {
                    [self.delegate parserDidFinishLoadAnotherPage];
                }
            }
        }];
}

- (void)findEpisodeInformationByID:(NSInteger)episodeID {
    
    NSString *path = [NSString stringWithFormat:@"%@%li.json", BASE_EPISODE_SEARCH_URL, (long)episodeID];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:path]];
    
    [request addValue:GLOOB_AUTH_TOKEN forHTTPHeaderField:GLOOB_HEADER_FIELD];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                              
                               if (connectionError) {
                                   
                                   if ([self.delegate respondsToSelector:@selector(parserDidFailedWithError:)]) {
                                       [self.delegate parserDidFailedWithError:connectionError];
                                   }
                                   
                               } else {
                                   
                                   NSDictionary *dict_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                options:kNilOptions
                                                                                                  error:nil];
                                   
                                   if ([self.delegate respondsToSelector:@selector(parserDidFinishedLoadingEpisodeInfo:)]) {
                                       [self.delegate parserDidFinishedLoadingEpisodeInfo:dict_content];
                                   }
                               }
                           }];
}

#pragma mark - Private

- (void)loadPageWithPath:(NSString *)path andCompletionHandler:(completionBlock)finished {
    
    if (path != (id)[NSNull null]) {
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:path]];
        
        [request addValue:GLOOB_AUTH_TOKEN forHTTPHeaderField:GLOOB_HEADER_FIELD];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                   
                                   if (connectionError) {
                                       
                                       finished(NO, connectionError);
                                       
                                   } else {
                                       
                                       NSDictionary *dict_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                    options:kNilOptions
                                                                                                      error:nil];
                                       
                                       if ([path isEqualToString:self.firstPage]) {
                                           
                                           self.resultCount = [[dict_content objectForKey:@"count"] integerValue];
                                           
                                           NSArray *array_results = [dict_content objectForKey:@"results"];
                                           
                                           for (NSDictionary *dict in array_results) {
                                               
                                               if ([[dict objectForKey:@"kind"] isEqualToString:KIND_PROGRAMS]) {
                                                   [self parseProgramsJSON:dict];
                                               } else {
                                                   [self parseEpisodesJSON:dict];
                                               }
                                           }
                                           
                                       } else {
                                           
                                           if ([path isEqualToString:self.nextPrograms]) {
                                               [self parseProgramsJSON:dict_content];
                                           }
                                           
                                           if ([path isEqualToString:self.nextEpisodes]) {
                                               [self parseEpisodesJSON:dict_content];
                                           }
                                       }
                                       
                                       finished(YES, nil);
                                   }
                               }];
    } else {
        finished(YES, nil);
    }
}

- (void)parseProgramsJSON:(NSDictionary *)json {
    
    self.nextPrograms = [json objectForKey:@"next"];
    
    for (NSDictionary *dict_results in [json objectForKey:@"results"]) {
        
        ProgramFound *program = [[ProgramFound alloc] initWithDictionary:dict_results];
        
        [self.objects_programs addObject:program];
    }
}

- (void)parseEpisodesJSON:(NSDictionary *)json {
    
    self.nextEpisodes = [json objectForKey:@"next"];
    
    for (NSDictionary *dict_results in [json objectForKey:@"results"]) {
        
        EpisodeFound *episode = [[EpisodeFound alloc] initWithDictionary:dict_results];
        
        [self.objects_episodes addObject:episode];
    }
}

@end
