//
//  BirthdayRule.m
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "BirthdayRule.h"
#import "Profile.h"
#import "EventKeyFactory.h"
#import "EventRepository.h"

@implementation BirthdayRule

- (BOOL)isValidated {
    Event *event = [self getEvent];
    if (event.value)  {
        NSDate *date = [NSDate date];
        NSDate *birthday = [Profile currentProfile].birthday;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM"];
        
        return ([[dateFormat stringFromDate:date] isEqual:[dateFormat stringFromDate:birthday]]);
    }
    
    return false;
}

- (BOOL)shouldMark {
    Event *event = [self getEvent];
    
    if (!event.created_at) {
        return YES;
    }
    
    return ![self isValidated];
}


- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:self
                                                                   andParams:@[
                                                                               @(profile.profileId),
                                                                               @(self.ruleId)]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

@end
