//
//  Profile.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Profile.h"
#import "ProfileManager.h"

@interface Profile ()

- (NSInteger)generateUniqueProfileID;

@end

@implementation Profile

- (id)initNewProfileWithName:(NSString *)name birthday:(NSDate *)birthday avatar:(Avatar *)avatar {
    
    if (self = [super init]) {
        
        self.profileId = [self generateUniqueProfileID];
        
        self.name = name;
        self.birthday = birthday;
        self.currentAvatar = avatar;
        
        self.favorites = [[NSMutableArray alloc] init];
        self.avatars = [[NSMutableArray alloc] init];
        self.selfies = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.profileId = [[dictionary objectForKey:KEY_PROFILE_ID] integerValue];
        
        self.name = [dictionary objectForKey:KEY_PROFILE_NAME];
        
        self.birthday = (NSDate *)[dictionary objectForKey:KEY_PROFILE_BIRTHDAY];
        
        self.currentAvatar = (Avatar *)[dictionary objectForKey:KEY_PROFILE_CURRENT_AVATAR];
        
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super init]) {
        
        self.profileId = [aDecoder decodeIntegerForKey:KEY_PROFILE_ID];
        
        self.name = [aDecoder decodeObjectForKey:KEY_PROFILE_NAME];
        
        self.birthday = [aDecoder decodeObjectForKey:KEY_PROFILE_BIRTHDAY];
        
        self.currentAvatar = [aDecoder decodeObjectForKey:KEY_PROFILE_CURRENT_AVATAR];
        
        self.favorites = [aDecoder decodeObjectForKey:KEY_PROFILE_FAVORITES];
        
        self.avatars = [aDecoder decodeObjectForKey:KEY_PROFILE_AVATARS];
        
        self.selfies = [aDecoder decodeObjectForKey:KEY_PROFILE_SELFIES];
    }
    
    return self;
}


- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.profileId forKey:KEY_PROFILE_ID];
    
    [aCoder encodeObject:self.name forKey:KEY_PROFILE_NAME];
    
    [aCoder encodeObject:self.birthday forKey:KEY_PROFILE_BIRTHDAY];
    
    [aCoder encodeObject:self.currentAvatar forKey:KEY_PROFILE_CURRENT_AVATAR];
    
    [aCoder encodeObject:self.favorites forKey:KEY_PROFILE_FAVORITES];
    
    [aCoder encodeObject:self.avatars forKey:KEY_PROFILE_AVATARS];
    
    [aCoder encodeObject:self.selfies forKey:KEY_PROFILE_SELFIES];
}

static Profile * _sharedInstance = nil;
+ (instancetype)currentProfile {
   return [[ProfileManager sharedManager] currentProfile];
}

+ (void)createInstanceAndSetupDefaultAvatar {
    
    [self currentProfile];
    _sharedInstance.birthday = [NSDate date];
}

#pragma mark - Private

- (NSInteger)generateUniqueProfileID {
    
    NSInteger uniqueID = arc4random() % 10000;
    
    return uniqueID;
}

#pragma mark - Public

- (UIImage *)getHomeAvatarImage {
    return [UIImage imageNamed:[NSString stringWithFormat:@"avatar_home_%@", self.currentAvatar.icon]];
}

- (UIImage *)getCreateAvatarImage {
    return [UIImage imageNamed:[NSString stringWithFormat:@"avatar_criar_%@", self.currentAvatar.icon]];
}

- (UIImage *)getCreateProfileImage {
    return [UIImage imageNamed:[NSString stringWithFormat:@"avatar_seuperfil_%@", self.currentAvatar.icon]];
}

- (BOOL)isCurrentProfile {
    
    BOOL isCurrentProfile = NO;
    
    if (self.profileId == [[ProfileManager sharedManager] currentProfile].profileId) {
        isCurrentProfile = YES;
    }
    
    return isCurrentProfile;
}

@end
