//
//  LoginManager.m
//  gloob
//
//  Created by zeroum on 30/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "LoginManager.h"
#import "ProfileManager.h"

@interface LoginManager ()

+ (void)setTokenExpireDate:(NSInteger)expires_in;
+ (void)scheduleTokenExpiredAlarm;
+ (void)eraseToken;

@end

@implementation LoginManager 

static LoginManager *_sharedManager = nil;

+ (LoginManager *)sharedManager {
	@synchronized([LoginManager class]) {
		if (!_sharedManager)
			_sharedManager = [[self alloc] init];
		
		return _sharedManager;
	}
	
	return nil;
}

+ (id)alloc {
	@synchronized([LoginManager class]) {
		NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedManager = [super alloc];
		return _sharedManager;
	}
	
	return nil;
}

- (id)init {
	
    if (self = [super init]) {
        
    }
	
	return self;
}

#pragma mark - Public Methods

+ (void)checkOrCreateCurrentLoginKey {
    
    NSData *currentLogin = [[NSUserDefaults standardUserDefaults] objectForKey:GLOOB_CURRENT_LOGIN];
    
    if (currentLogin == nil) {
        // Create a Key in NSUserDefaults to store current login data
        currentLogin = [[NSData alloc] init];
        
        [[NSUserDefaults standardUserDefaults] setObject:currentLogin forKey:GLOOB_CURRENT_LOGIN];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)saveLogin:(Login *)login {
    
    NSData *currentUser = [NSKeyedArchiver archivedDataWithRootObject:login];
    
    [[NSUserDefaults standardUserDefaults] setObject:currentUser forKey:GLOOB_CURRENT_LOGIN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self setTokenExpireDate:login.expires_in];
}

+ (Login *)getSavedLogin {
    
    NSData *currentUser = [[NSUserDefaults standardUserDefaults] objectForKey:GLOOB_CURRENT_LOGIN];
    
    if (currentUser.length > 0) {
        
        Login *login = (Login *)[NSKeyedUnarchiver unarchiveObjectWithData:currentUser];
        
        return login;
        
    } else {
        
        return nil;
    }
}

+ (BOOL) isAuthenticated {
    return ([LoginManager getSavedLogin] != nil);
}

+ (BOOL)isTokenActive {
    
    NSDate *expireDate = [[NSUserDefaults standardUserDefaults] objectForKey:GLOOB_ACCESS_TOKEN_EXPIRE_DATE];
    
    NSDate *today = [NSDate date];
    
    NSTimeInterval distanceBetweenDates = [expireDate timeIntervalSinceDate:today];
    
    if (distanceBetweenDates < 0) {
        
        [self eraseToken];
        
        return NO;
    }
    
    return YES;
}

+ (void)eraseToken {
    
    Login *login = [self getSavedLogin];
    
    if (login.access_token != nil) {
        
        login.access_token = nil;
        login.expires_in = 0;
    }
    
    [self saveLogin:login];
    
    NSDate *nullToken;
    
    [[NSUserDefaults standardUserDefaults] setObject:nullToken forKey:GLOOB_ACCESS_TOKEN_EXPIRE_DATE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Private Methods

+ (void)setTokenExpireDate:(NSInteger)expires_in {
    
//    NSDate *expireDate = [NSDate dateWithTimeIntervalSinceNow:60]; // 1 minute
    
    NSDate *expireDate = [NSDate dateWithTimeIntervalSinceNow:expires_in]; // Default
    
    [[NSUserDefaults standardUserDefaults] setObject:expireDate forKey:GLOOB_ACCESS_TOKEN_EXPIRE_DATE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self scheduleTokenExpiredAlarm];
}

+ (void)scheduleTokenExpiredAlarm {
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    NSDate *expireDate = [[NSUserDefaults standardUserDefaults] objectForKey:GLOOB_ACCESS_TOKEN_EXPIRE_DATE];
    
    localNotification.fireDate = expireDate;
    
    localNotification.alertBody = @"Sua sessão expirou! Faça o login no sistema para acessar o conteúdo novamente.";
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
    // Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"login expired" forKey:@"type"];
    localNotification.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}


+ (void) logOutWithCompletionBlock:(void(^)())completionBlock {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences removeObjectForKey:GLOOB_CURRENT_LOGIN];
    [preferences removeObjectForKey:GLOOB_ACCESS_TOKEN_EXPIRE_DATE];
    [preferences synchronize];
    
    [[ProfileManager sharedManager] setCurrentProfile:nil];
    [[ProfileManager sharedManager] saveLatestUsedProfile];
    
    if (completionBlock) {
        completionBlock();
    }
}
@end
