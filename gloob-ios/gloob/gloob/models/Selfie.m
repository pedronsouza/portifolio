//
//  Selfie.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Selfie.h"
#import "Avatar.h"
#import "ProfileManager.h"
#import "SelfieUnlockViewController.h"
#import "AchievementsFactory.h"
#import "AchievementRepository.h"
#define kIndexForProfileCreationSelfie 0
#define kNsCodignKeyForRelatedAvatar @"kNsCodignKeyForRelatedAvatar"
@implementation Selfie
- (instancetype)initWithAchievementRaw:(NSDictionary *)achievementRaw {
    self = [super initWithAchievementRaw:achievementRaw];
    
    if (self) {
        self.relatedAvatarId = -1;
    }
    
    return self;
}

- (instancetype)initWithSelfieId:(NSInteger)selfieId {
    self = [super initWithSelfieId:selfieId];
    
    if (self) {
        self.relatedAvatarId = -1;
    }
    
    return self;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.relatedAvatarId = -1;
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.relatedAvatarId forKey:kNsCodignKeyForRelatedAvatar];
    [super encodeWithCoder:aCoder];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.relatedAvatarId = [aDecoder decodeIntegerForKey:kNsCodignKeyForRelatedAvatar];
    }
    
    return self;
}


- (UIImage *)iconForSelfieList {
    Selfie *savedSelfie = self;
    savedSelfie = [[AchievementRepository sharedInstance] getUnlockedSelfieById:self.achievementId];
    Avatar *avatar = [[AchievementRepository sharedInstance] getUnlockedAvatarById:savedSelfie.relatedAvatarId];
    
    if (!savedSelfie && !avatar) {
        avatar = [[ProfileManager sharedManager] currentProfile].currentAvatar;
    }
    
    NSString *imageName = [NSString stringWithFormat:@"selfie_%@_%@", avatar.icon, self.icon];
    return [UIImage imageNamed:imageName];
}

- (UIImage *)iconForAchievementUnlock {
    Avatar *avatar = [ProfileManager sharedManager].currentProfile.currentAvatar;
    NSString *imageName = [NSString stringWithFormat:@"selfie_%@_%@", avatar.icon, self.icon];
    return [UIImage imageNamed:imageName];
}

- (UIViewController *) viewControllerForAchievementUnlocked {
    return [[SelfieUnlockViewController alloc] initWithAchievement:self];
}

+ (Selfie *) selfieForProfileCreation {
    Selfie *selfie = (Selfie *) [AchievementsFactory sharedInstance].selfies[kIndexForProfileCreationSelfie];
    return selfie;
}
@end
