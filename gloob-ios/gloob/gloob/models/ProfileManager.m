//
//  ProfileManager.m
//  gloob
//
//  Created by zeroum on 24/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ProfileManager.h"
#import "Episode.h"
#import "LoginManager.h"
#import "EventLog.h"

#define kStoredProfiles @"stored_profiles_%@"
#define kLatestProfile @"latest_profile_%@"
@interface ProfileManager()
- (NSString *) keyForStoredProfiles;
- (NSString *) keyForLatestProfile;
@end

@implementation ProfileManager

static ProfileManager *_sharedManager = nil;

+ (ProfileManager *)sharedManager {
    @synchronized([ProfileManager class]) {
        if (!_sharedManager)
            _sharedManager = [[self alloc] init];
        if (!_sharedManager.currentProfile) {
            [_sharedManager recoverLatestUsedProfile];
        }
        return _sharedManager;
    }
    
    return nil;
}

+ (id)alloc {
    @synchronized([ProfileManager class]) {
        NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedManager = [super alloc];
        return _sharedManager;
    }
    
    return nil;
}

- (id)init {
    
    if (self = [super init]) {
        
        self.objects = [[NSMutableArray alloc] init];
        
        [self verifyProfiles];
        if ([LoginManager isAuthenticated]) {
            [self recoverLatestUsedProfile];
        }
        
    }
    
    return self;
}

#pragma mark key format methods
- (NSString *)keyForLatestProfile {
    Login *login = [LoginManager getSavedLogin];
    return [NSString stringWithFormat:kLatestProfile, @(login.user_number)];
}

- (NSString *)keyForStoredProfiles {
    Login *login = [LoginManager getSavedLogin];
    return [NSString stringWithFormat:kStoredProfiles, @(login.user_number)];
}

#pragma mark - Public

- (Profile *)getCurrentProfile {
    return self.currentProfile;
}

- (NSMutableArray *)getAllProfiles {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForStoredProfiles]];
    if (data) {
        self.objects = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return self.objects;
}

- (BOOL)canCreateMoreProfiles {
    
    BOOL canCreate = NO;
    
    if (self.objects.count < kMaxProfiles) {
        canCreate = YES;
    }
    
    return canCreate;
}

- (BOOL)isSignedInAnyProfile {
    
    BOOL isSigned = NO;
    
    if (self.currentProfile != nil) {
        isSigned = YES;
    }
    
    return isSigned;
}

- (void)deleteProfile:(Profile *)profile {
    
    NSMutableArray *discardedItem = [NSMutableArray array];
    
    for (Profile *prof in self.objects) {
        
        if (profile.profileId == prof.profileId) {
            [discardedItem addObject:prof];
        }
    }
    
    [self.objects removeObjectsInArray:discardedItem];
    
    [self saveProfilesToUserDefaults];
}

- (void)verifyProfiles {
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForStoredProfiles]];
    
    if (data == nil) {
        
        NSData *newData = [NSKeyedArchiver archivedDataWithRootObject:self.objects];
        
        [[NSUserDefaults standardUserDefaults] setObject:newData forKey:[self keyForStoredProfiles]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        [self loadAllProfilesFromUserDefaults];
    }
}

- (void)loadAllProfilesFromUserDefaults {
    
    if (self.objects) {
        
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForStoredProfiles]];
        
        self.objects = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}

- (void)saveProfilesToUserDefaults {
    NSMutableArray *objects = [[NSMutableArray alloc] initWithArray:self.objects];
    
    for (int i = 0; i < self.objects.count; i++) {
        Profile *profile = [self.objects objectAtIndex:i];
        if (profile.profileId == self.currentProfile.profileId) {
            [objects replaceObjectAtIndex:i withObject:self.currentProfile];
        }
    }
    
    self.objects = [[NSMutableArray alloc] initWithArray:objects];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.objects];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:[self keyForStoredProfiles]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)addProfileToList:(Profile *)profile {

    if ([self canCreateMoreProfiles]) {
        
        [self.objects addObject:profile];
        
        [self saveProfilesToUserDefaults];
        
        self.currentProfile = profile;
        
        [self saveLatestUsedProfile];
    }
}

- (void)recoverLatestUsedProfile {
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForLatestProfile]];
    
    if (data == nil) {
        
        NSData *newData = [NSKeyedArchiver archivedDataWithRootObject:self.currentProfile];
        
        [[NSUserDefaults standardUserDefaults] setObject:newData forKey:[self keyForLatestProfile]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.currentProfile = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}
- (Profile *) getLatestUsedProfile {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self keyForLatestProfile]];
    if (data) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return nil;
}

- (void)saveLatestUsedProfile {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.currentProfile];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:[self keyForLatestProfile]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)addFavoriteEpisode:(Episode *)favorite {
    
    if (!self.currentProfile.favorites) {
        self.currentProfile.favorites = [[NSMutableArray alloc] init];
    }
    
    [self.currentProfile.favorites addObject:favorite];
    
    [self saveLatestUsedProfile];
    
    [[ProfileManager sharedManager] saveProfilesToUserDefaults];
    [[EventLog sharedInstance] logBookmarkEvent];
    
}

- (void)removeFavoriteEpisode:(Episode *)favorite {
    NSMutableArray *removedItem = [NSMutableArray array];
    
    for (Episode *ep in self.currentProfile.favorites) {
        if (ep.episode_id == favorite.episode_id) {
            [removedItem addObject:ep];
        }
    }
    
    [self.currentProfile.favorites removeObjectsInArray:removedItem];
    [[ProfileManager sharedManager] saveProfilesToUserDefaults];
    [self saveLatestUsedProfile];
}

- (BOOL)isEpisodeFavorite:(Episode *)episode {
    
    BOOL isFavorite = NO;
    
    for (Episode *ep in self.currentProfile.favorites) {
        
        if (ep.episode_id == episode.episode_id) {
            
            isFavorite = YES;
            
            break;
        }
    }
    
    return isFavorite;
}
@end
