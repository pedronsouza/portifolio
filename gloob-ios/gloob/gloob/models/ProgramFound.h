//
//  ProgramFound.h
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GloobModel.h"

@interface ProgramFound : GloobModel

@property (nonatomic, assign) NSInteger program_id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, assign) NSInteger episodes_count;
@property (nonatomic, assign) NSInteger seasons_count;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
