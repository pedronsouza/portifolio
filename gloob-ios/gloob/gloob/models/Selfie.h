//
//  Selfie.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Achievement.h"
#import "Avatar.h"
@interface Selfie : Achievement<NSCoding>
@property (nonatomic, assign) NSInteger relatedAvatarId;
- (UIImage *) iconForSelfieList;
+ (Selfie *) selfieForProfileCreation;
@end
