//
//  Event.m
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Event.h"

#define kCodingKeyForValue @"kCodingKeyForValue"
#define kCodingKeyForCreatedAt @"kCodingKeyForCreatedAt"
#define kCodingKeyForCondition @"kCodingKeyForCondition"

@implementation Event

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.value forKey:kCodingKeyForValue];
    [aCoder encodeObject:self.created_at forKey:kCodingKeyForCreatedAt];
    [aCoder encodeObject:self.condiditon forKey:kCodingKeyForCondition];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        self.value = [aDecoder decodeObjectForKey:kCodingKeyForValue];
        self.created_at = [aDecoder decodeObjectForKey:kCodingKeyForCreatedAt];
        self.condiditon = [aDecoder decodeObjectForKey:kCodingKeyForCondition];
    }
    
    return self;
}

@end
