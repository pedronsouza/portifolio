//
//  Game.h
//  gloob
//
//  Created by zeroum on 27/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GloobModel.h"

@interface Game : GloobModel

@property (nonatomic, assign) NSInteger game_id;
@property (nonatomic, strong) NSString *game_file;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *game_descritpion;

@property (nonatomic, strong) NSString *optimized_thumb;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
