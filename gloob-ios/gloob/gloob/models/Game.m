//
//  Game.m
//  gloob
//
//  Created by zeroum on 27/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Game.h"

#define KEY_OPTIMIZED_THUMB @"optimized_thumb"

@implementation Game

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.game_id = [self parseIntegerFromKey:[dictionary objectForKey:@"id"]];
        
        self.game_file = [self parseStringFromKey:[dictionary objectForKey:@"game_file"]];
        
        self.image = [self parseStringFromKey:[dictionary objectForKey:@"image"]];
        
        self.name = [self parseStringFromKey:[dictionary objectForKey:@"name"]];
        
        self.game_descritpion = [self parseStringFromKey:[dictionary objectForKey:@"description"]];
        
        self.optimized_thumb = [NSString stringWithFormat:@"game_%@_%li", KEY_OPTIMIZED_THUMB, (long)self.game_id];
    }
    
    return self;
}

@end
