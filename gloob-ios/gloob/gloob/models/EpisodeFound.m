//
//  EpisodeFound.m
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "EpisodeFound.h"

#define KEY_ID @"id"
#define KEY_TITLE @"title"
#define KEY_IMAGE @"image"
#define KEY_PROGRAM_NAME @"program"
#define KEY_SEASON @"season"

@implementation EpisodeFound

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.episode_id = [self parseIntegerFromKey:[dictionary objectForKey:KEY_ID]];
        
        self.title = [self parseStringFromKey:[dictionary objectForKey:KEY_TITLE]];
        
        self.image = [self parseStringFromKey:[dictionary objectForKey:KEY_IMAGE]];
        
        self.episode_program = [self parseStringFromKey:[dictionary objectForKey:KEY_PROGRAM_NAME]];
        
        self.season = [self parseIntegerFromKey:[dictionary objectForKey:KEY_SEASON]];
        
        self.optmized_thumb = [NSString stringWithFormat:@"episodefound_%li", (long)self.episode_id];
    }
    
    return self;
}

@end
