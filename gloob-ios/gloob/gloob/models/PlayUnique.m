//
//  PlayUnique.m
//  gloob
//
//  Created by Pedro on 3/23/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "PlayUnique.h"
#import "EventRepository.h"
#import "Event.h"
#import "Profile.h"
#import "EventKeyFactory.h"

@implementation PlayUnique

- (BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (event.value) {
        return (((NSNumber *)event.value).integerValue >= self.eventAmount);
    }
    
    return NO;
}


- (BOOL)shouldMark {
    return (![self isValidated]);
}

- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:self
                                                                   andParams:@[
                                                                               @(profile.profileId),
                                                                               @(self.ruleId)]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}
@end
