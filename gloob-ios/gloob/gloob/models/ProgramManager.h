//
//  ProgramManager.h
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Program;

@interface ProgramManager : NSObject

@property (nonatomic, strong) NSMutableArray *objects;
@property (nonatomic, strong) NSMutableArray *objectsFeaturedEpisode;
@property (nonatomic, strong) NSMutableArray *objectsFeaturedHome;
@property (nonatomic, strong) NSArray *objectsTopFive;
@property (nonatomic, strong) NSString *nextPage;
@property (nonatomic, strong) NSString *nextPageFeaturedEpisode;
@property (nonatomic, strong) NSString *nextPageFeaturedHome;

+ (ProgramManager *)sharedManager;

typedef void(^completionBlock)(BOOL success, NSError *error);

- (void)loadProgramsWithCompletionHandler:(completionBlock)finished;
- (void)loadFeaturedEpisodesWithCompletionHandler:(completionBlock)finished;
- (void)loadFeaturedHomeWithCompletionHandler:(completionBlock)finished;
- (void)loadTopFiveWithCompletionHandler:(completionBlock)finished;

- (Program *)findProgramByID:(NSInteger)programID;
- (Program *)findProgramByName:(NSString *)programName;

@end
