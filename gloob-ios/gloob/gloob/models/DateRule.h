//
//  DateRule.h
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRule.h"
#import "RulesProtocol.h"

@interface DateRule : BaseRule<RulesProtocol>
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@end
