//
//  Episode.m
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "Episode.h"
#import "Program.h"
#import "ProfileManager.h"

#define KEY_EPISODE_PROGRAM @"program"
#define KEY_EPISODE_SEASON @"season"

#define KEY_EPISODE_EPISODE_ID @"id"
#define KEY_EPISODE_CATEGORIES @"categories"
#define KEY_EPISODE_CONTENT_RATING @"content_rating"
#define KEY_EPISODE_THUMB_IMAGE @"thumb_image"
#define KEY_EPISODE_TITLE @"title"
#define KEY_EPISODE_DURATION @"duration"
#define KEY_EPISODE_ID_GLOBO_VIDEOS @"id_globo_videos"
#define KEY_EPISODE_ID_GLOBO_VIDEOS_DUBBED @"id_globo_videos_dubbed"
#define KEY_EPISODE_ID_GLOBO_VIDEOS_PREVIEW @"id_globo_videos_preview"
#define KEY_EPISODE_DESCRIPTION @"description"
#define KEY_EPISODE_NUMBER @"number"
#define KEY_EPISODE_YEAR @"year"
#define KEY_EPISODE_IMAGE_CROPPED @"image_cropped"
#define KEY_EPISODE_CATEGORY @"categories"

#define KEY_VIDEO_VIEWS @"video_views"

#define KEY_OPTIMIZED_THUMB @"optimized_thumb"

@implementation Episode

- (id)initWithDictionary:(NSDictionary *)dictionary andProgram:(Program *)program {
    
    if (self = [super init]) {
        
        self.program = program;
        
        self.episode_id = [self parseIntegerFromKey:[dictionary objectForKey:KEY_EPISODE_EPISODE_ID]];

        self.thumb_image = [self parseStringFromKey:[dictionary objectForKey:KEY_EPISODE_THUMB_IMAGE]];
        
        self.image_cropped = [self parseStringFromKey:[dictionary objectForKey:KEY_EPISODE_IMAGE_CROPPED]];

        self.title = [self parseStringFromKey:[dictionary objectForKey:KEY_EPISODE_TITLE]];

        self.duration = [self parseIntegerFromKey:[dictionary objectForKey:KEY_EPISODE_DURATION]];
        
        self.id_globo_videos = [self parseIntegerFromKey:[dictionary objectForKey:KEY_EPISODE_ID_GLOBO_VIDEOS]];
        
        self.ep_description = [self parseStringFromKey:[dictionary objectForKey:KEY_EPISODE_DESCRIPTION]];
        
        self.number = [self parseIntegerFromKey:[dictionary objectForKey:KEY_EPISODE_NUMBER]];
        
        self.season = [self parseIntegerFromKey:[[dictionary objectForKey:KEY_EPISODE_SEASON] objectForKey:KEY_EPISODE_NUMBER]];
        
        self.videos_views = [self parseIntegerFromKey:[dictionary objectForKey:KEY_VIDEO_VIEWS]];
        
        NSArray *categories = [dictionary objectForKey:@"categories"];
        
        if ([categories count] > 0) {
            self.category = categories[0];
        } else {
            self.category = @"";
        }
        
        self.optimized_thumb = [NSString stringWithFormat:@"episode_%@_%li", KEY_OPTIMIZED_THUMB, (long)self.episode_id];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super init]) {
        
        self.program = [aDecoder decodeObjectForKey:KEY_EPISODE_PROGRAM];
        
        self.episode_id = [aDecoder decodeIntegerForKey:KEY_EPISODE_EPISODE_ID];
        
        self.thumb_image = [aDecoder decodeObjectForKey:KEY_EPISODE_THUMB_IMAGE];
        
        self.image_cropped = [aDecoder decodeObjectForKey:KEY_EPISODE_IMAGE_CROPPED];
        
        self.title = [aDecoder decodeObjectForKey:KEY_EPISODE_TITLE];
        
        self.duration = [aDecoder decodeIntegerForKey:KEY_EPISODE_DURATION];
        
        self.id_globo_videos = [aDecoder decodeIntegerForKey:KEY_EPISODE_ID_GLOBO_VIDEOS];
        
        self.ep_description = [aDecoder decodeObjectForKey:KEY_EPISODE_DESCRIPTION];
        
        self.number = [aDecoder decodeIntegerForKey:KEY_EPISODE_NUMBER];
        
        self.season = [aDecoder decodeIntegerForKey:KEY_EPISODE_SEASON];
        
        self.videos_views = [aDecoder decodeIntegerForKey:KEY_VIDEO_VIEWS];
        
        self.optimized_thumb = [aDecoder decodeObjectForKey:KEY_OPTIMIZED_THUMB];
        
        self.category = [aDecoder decodeObjectForKey:KEY_EPISODE_CATEGORY];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.program forKey:KEY_EPISODE_PROGRAM];
    
    [aCoder encodeInteger:self.episode_id forKey:KEY_EPISODE_EPISODE_ID];
    
    [aCoder encodeObject:self.thumb_image forKey:KEY_EPISODE_THUMB_IMAGE];
    
    [aCoder encodeObject:self.image_cropped forKey:KEY_EPISODE_IMAGE_CROPPED];
    
    [aCoder encodeObject:self.title forKey:KEY_EPISODE_TITLE];
    
    [aCoder encodeInteger:self.id_globo_videos forKey:KEY_EPISODE_ID_GLOBO_VIDEOS];
    
    [aCoder encodeObject:self.ep_description forKey:KEY_EPISODE_DESCRIPTION];
    
    [aCoder encodeInteger:self.number forKey:KEY_EPISODE_NUMBER];
    
    [aCoder encodeInteger:self.season forKey:KEY_EPISODE_SEASON];
    
    [aCoder encodeInteger:self.videos_views forKey:KEY_VIDEO_VIEWS];
    
    [aCoder encodeObject:self.optimized_thumb forKey:KEY_OPTIMIZED_THUMB];
    
    [aCoder encodeObject:self.category forKey:KEY_EPISODE_CATEGORY];
}

#pragma mark - Public

- (void)favoriteThisEpisode {
    [[ProfileManager sharedManager] addFavoriteEpisode:self];
}

- (void)removeFromFavorites {
    [[ProfileManager sharedManager] removeFavoriteEpisode:self];
}

- (BOOL)isFavorite {
    return [[ProfileManager sharedManager] isEpisodeFavorite:self];
}

@end