//
//  LoginManager.h
//  gloob
//
//  Created by zeroum on 30/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Login.h"

@interface LoginManager : NSObject

+ (LoginManager *)sharedManager;
+ (BOOL) isAuthenticated;
+ (void)checkOrCreateCurrentLoginKey;
+ (void)saveLogin:(Login *)login;
+ (Login *)getSavedLogin;
+ (void) logOutWithCompletionBlock:(void(^)())completionBlock;
+ (BOOL)isTokenActive;
+ (void)eraseToken;

@end
