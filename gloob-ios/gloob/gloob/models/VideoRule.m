//
//  VideoRule.m
//  gloob
//
//  Created by Pedro on 3/23/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "VideoRule.h"
#import "Profile.h"
#import "EventKeyFactory.h"
#import "EventRepository.h"

@implementation VideoRule

- (BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (!event.value) {
        return NO;
    }
    
    return (((NSNumber *)event.value).intValue >= self.eventAmount);
}

- (BOOL)shouldMark {
    Event *event = [self getEvent];
    
    if (!event.value) {
        return YES;
    } else {
        return (((NSNumber *)event.value).intValue <= self.eventAmount);
    }
}

- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:self
                                                         andParams:@[
                                                                     @(profile.profileId),
                                                                     self.condition]];
    
    
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

@end
