//
//  EventKeyFactory.h
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRule.h"

@interface EventKeyFactory : NSObject
+ (id)sharedInstance;
- (NSString *) createKeyWithRule:(BaseRule *)rule andParams:(NSArray *) params;
- (NSString *) createKeyWithRuleCondition:(NSString *)condition andParams:(NSArray *) params;
@end
