//
//  MarkRule.m
//  gloob
//
//  Created by Pedro on 3/23/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "MarkRule.h"
#import "EventRepository.h"
#import "Event.h"
#import "Profile.h"
#import "EventKeyFactory.h"

@implementation MarkRule

- (BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (event.value) {
        return (((NSNumber *)event.value).integerValue >= self.eventAmount);
    }
    
    return NO;
}

- (BOOL)shouldMark {
    return (![self isValidated]);
}


- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:@"mark"
                                                                            andParams:@[
                                                                                        @(profile.profileId),
                                                                                        self.condition]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

@end
