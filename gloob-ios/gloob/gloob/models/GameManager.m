//
//  GameManager.m
//  gloob
//
//  Created by zeroum on 26/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GameManager.h"
#import "Game.h"
#import "NSMutableArray+Shuffling.h"

// curl -X GET "http://api.apps.globosat.tv/gloobplay/games.json" -H 'Authorization: Token 55a22324ed08c3b302fa6f661778710bd63bba12'

#define FEATURED_GAMES @"http://api.apps.globosat.tv/gloobplay/games.json"

@interface GameManager ()

- (void)parseGamesJSON:(NSDictionary *)json;

@end

@implementation GameManager

static GameManager *_sharedManager = nil;

+ (GameManager *)sharedManager {
    @synchronized([GameManager class]) {
        if (!_sharedManager)
            _sharedManager = [[self alloc] init];
        
        return _sharedManager;
    }
    
    return nil;
}

+ (id)alloc {
    @synchronized([GameManager class]) {
        NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedManager = [super alloc];
        return _sharedManager;
    }
    
    return nil;
}

- (id)init {
    
    if (self = [super init]) {
        
        self.objects = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark - Private

- (void)parseGamesJSON:(NSDictionary *)json {
    
    for (NSDictionary *dict in [json objectForKey:@"results"]) {
        
        Game *game = [[Game alloc] initWithDictionary:dict];
        
        [self.objects addObject:game];
    }
}

#pragma mark - Public

- (void)loadGamesWithCompletionHandler:(completionBlock)finished {
    
    if (self.objects) {
        [self.objects removeAllObjects];
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:FEATURED_GAMES]];
    
    [request addValue:GLOOB_AUTH_TOKEN_B forHTTPHeaderField:GLOOB_HEADER_FIELD];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (connectionError) {
                                   
                                   finished(NO, connectionError);
                                   
                               } else {
                                   
                                   NSDictionary *dict_content = [NSJSONSerialization JSONObjectWithData:data
                                                                                                options:kNilOptions
                                                                                                  error:nil];
                                   
                                   [self parseGamesJSON:dict_content];
                                   
                                   finished(YES, nil);
                               }
                           }];
}

- (NSMutableArray *)getGamesInRandomOrder {
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.objects];
    
    [array shuffle];
    
    return array;
}

@end
