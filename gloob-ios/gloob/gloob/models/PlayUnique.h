//
//  PlayUnique.h
//  gloob
//
//  Created by Pedro on 3/23/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "BaseRule.h"
#import "RulesProtocol.h"

@interface PlayUnique : BaseRule<RulesProtocol>

@end
