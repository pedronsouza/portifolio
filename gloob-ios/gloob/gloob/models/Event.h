//
//  Event.h
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobModel.h"
@interface Event : GloobModel<NSCoding>
@property (nonatomic, strong) id value;
@property (nonatomic, strong) NSDate *created_at;
@property (nonatomic, assign) NSString *condiditon;
@end
