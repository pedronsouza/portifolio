//
//  GameManager.h
//  gloob
//
//  Created by zeroum on 26/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameManager : NSObject

@property (nonatomic, strong) NSMutableArray *objects;

+ (GameManager *)sharedManager;

typedef void(^completionBlock)(BOOL success, NSError *error);

- (void)loadGamesWithCompletionHandler:(completionBlock)finished;

- (NSMutableArray *)getGamesInRandomOrder;

@end
