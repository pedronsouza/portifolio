//
//  BookmarkRule.m
//  gloob
//
//  Created by Pedro on 3/23/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "BookmarkRule.h"
#import "Profile.h"
#import "ProfileManager.h"

@implementation BookmarkRule
- (BOOL)isValidated {

    NSMutableArray *favorites = [[[ProfileManager sharedManager] getCurrentProfile] favorites];
    
    if (favorites) {
        return (favorites.count >= self.eventAmount);
    }
    
    return NO;
}

- (BOOL)shouldMark {
    NSMutableArray *favorites = [[[ProfileManager sharedManager] getCurrentProfile] favorites];
    return (favorites.count <= self.eventAmount);
}
@end
