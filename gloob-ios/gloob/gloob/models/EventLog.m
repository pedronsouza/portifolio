
//
//  EventLog.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "EventLog.h"
#import "Profile.h"
#import "BaseRule.h"
#import "RulesProtocol.h"
#import "GamingManager.h"
#import "Event.h"
#import "EventKeyFactory.h"
#import "EventRepository.h"
#import "RulesFactory.h"
#import "PlayRule.h"
#import "ProfileManager.h"
#import "BookmarkRule.h"
#import "CarnavalRule.h"

@implementation EventLog
static EventLog *_sharedInstance = nil;

+ (id)sharedInstance {
    @synchronized([EventLog class]) {
        if (!_sharedInstance)
            _sharedInstance = [[self alloc] init];
        
        return _sharedInstance;
    }
    
    return nil;
}

- (BOOL) shouldMarkEvent {
    return ([[ProfileManager sharedManager] isSignedInAnyProfile]);
}

- (void) logWithOptions:(NSDictionary *)options {
    
}

- (void)logViewEventWithProgramId:(NSInteger) programId {
    if ([self shouldMarkEvent]) {
        Profile *profile = [Profile currentProfile];
        NSString *condition = @"views";
        NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:condition andParams:@[@(profile.profileId), @(programId)]];
        
        Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
        NSNumber *eventCount = @(1);
        
        if (event.value) {
            eventCount = (NSNumber *)event.value;
            NSInteger val = [eventCount integerValue];
            val += 1;
            eventCount = [NSNumber numberWithInteger:val];
        }
        event.value = eventCount;
        event.created_at = [NSDate date];
        event.condiditon = condition;
        
        if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
            NSLog(@"Registering View Event for profile: %@ with count %i", profile, [eventCount intValue]);
            [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
        }
    }
}

- (void)logMarkEventWithKey:(NSString *) key {
    if ([self shouldMarkEvent]) {
        Profile *profile = [Profile currentProfile];
        NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:@"video"
                                                                                andParams:@[
                                                                                            @(profile.profileId),
                                                                                            key]];
        Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
        NSNumber *eventCount = @(1);
        
        if (event.value) {
            eventCount = (NSNumber *)event.value;
            NSInteger val = [eventCount integerValue];
            val += 1;
            eventCount = [NSNumber numberWithInteger:val];
        }
        
        event.value = eventCount;
        event.created_at = [NSDate date];
        event.condiditon = key;
        
        NSLog(@"MARKING: %@", eventKey);
        
        if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
            NSLog(@"Registering Mark Event for profile: %@ for rule %@ with count %i" , @(profile.profileId), key, [eventCount intValue]);
            [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
        }
    }
}

- (void)logSearchEventWithKey:(NSString *) key {
    if ([self shouldMarkEvent]) {
        Profile *profile = [Profile currentProfile];
        NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:@"search"
                                                                                andParams:@[
                                                                                            @(profile.profileId),
                                                                                            key]];
        Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
        NSNumber *eventCount = @(1);
        
        if (event.value) {
            eventCount = (NSNumber *)event.value;
            NSInteger val = [eventCount integerValue];
            val += 1;
            eventCount = [NSNumber numberWithInteger:val];
        }
        
        event.value = eventCount;
        event.created_at = [NSDate date];
        event.condiditon = key;
        
        if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
            NSLog(@"Registering Search Event for profile: %@ for rule %@" , @(profile.profileId), key);
            [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
        }
    }
}

- (void)logAccessEvent {
    if ([self shouldMarkEvent]) {
        Profile *profile = [Profile currentProfile];
        NSString *condition = @"access";
        NSArray *accessRules = [[RulesFactory sharedInstance] rulesByCondition:condition];
        
        for (BaseRule<RulesProtocol> *rule in accessRules) {
            if ([rule shouldMark]) {
                NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:condition
                                                                                        andParams:@[
                                                                                                    @(profile.profileId)]];
                Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
                NSNumber *eventCount = @(1);
                
                if (event.value) {
                    eventCount = (NSNumber *)event.value;
                    NSInteger val = [eventCount integerValue];
                    val += 1;
                    eventCount = [NSNumber numberWithInteger:val];
                }
                
                event.value = eventCount;
                event.created_at = [NSDate date];
                event.condiditon = condition;
                
                if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
                    NSLog(@"Registering Access Event for profile: %@", profile);
                    [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
                    break;
                }
            }
        }
    }
}

- (void) verifyAndLogDateEvents {
    if ([self shouldMarkEvent]) {
        Profile *profile = [Profile currentProfile];
        BaseRule<RulesProtocol> *birthdayRule = [[RulesFactory sharedInstance] rulesByCondition:@"birthday"][0];
        BaseRule<RulesProtocol> *carnavalRule = [[RulesFactory sharedInstance] rulesByCondition:@"carnaval"][0];
        
        NSMutableArray *dateRules = [NSMutableArray arrayWithArray:[[RulesFactory sharedInstance] rulesByCondition:@"date"]];
        [dateRules addObject:carnavalRule];
        
        for (BaseRule<RulesProtocol> *rule in dateRules) {
            if ([rule shouldMark]) {
                NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:rule.condition
                                                                                        andParams:@[@(profile.profileId), @(rule.ruleId)]];
                Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
                event.value = [NSDate date];
                event.created_at = [NSDate date];
                event.condiditon = rule.condition;
                
                if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
                    
                    if ([rule isKindOfClass:[CarnavalRule class]]) {
                        NSLog(@"Registering Carnaval Event for profile %@ - %@", profile, eventKey);
                    } else {
                        NSLog(@"Registering Access Event for date %@ - %@", profile, eventKey);
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
                }
            }
        }
        
        NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRuleCondition:birthdayRule.condition
                                                                                andParams:@[@(profile.profileId), @(birthdayRule.ruleId)]];
        
        Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
        
        event.value = [NSDate date];
        event.created_at = [NSDate date];
        event.condiditon = birthdayRule.condition;
        
        if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
            NSLog(@"Registering Access Event for birthday %@", profile);
            [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
        }
    }
}

- (void)logPlayEventWithGameId:(NSInteger)gameId {
    if ([self shouldMarkEvent]) {
        Profile *profile = [Profile currentProfile];
        NSArray *playRules = [NSMutableArray arrayWithArray:[[RulesFactory sharedInstance] rulesByCondition:@"play"]];
        
        for (PlayRule<RulesProtocol> *rule in playRules) {
            if ([rule shouldMark]) {
                rule.gameId = gameId;
                NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:rule andParams:@[@(profile.profileId), @([rule ruleId])]];                
                Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
                NSNumber *eventCount = [NSNumber numberWithInt:1];
                
                if (event.value) {
                    eventCount = (NSNumber *)event.value;
                    NSInteger val = [eventCount integerValue];
                    val += 1;
                    eventCount = [NSNumber numberWithInteger:val];
                }
                
                event.value = eventCount;
                event.created_at = [NSDate date];
                event.condiditon = rule.condition;
                
                if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
                    NSLog(@"Registering Play Event for profile %@ with count %i with event key %@", @(profile.profileId), [eventCount intValue], eventKey);
                    [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
                }
            }
        }
    }
}

- (void)logBookmarkEvent {
    if ([self shouldMarkEvent]) {
        Profile *profile = [Profile currentProfile];
        NSArray *rules = [[RulesFactory sharedInstance] rulesByCondition:@"bookmark"];
        
        for (BaseRule<RulesProtocol> *rule in rules) {
            if ([rule shouldMark]) {
                NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:rule andParams:@[@(profile.profileId)]];;
                Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
                NSNumber *eventCount = [NSNumber numberWithInt:1];
                
                if (event.value) {
                    eventCount = (NSNumber *)event.value;
                    NSInteger val = [eventCount integerValue];
                    val += 1;
                    eventCount = [NSNumber numberWithInteger:val];
                }
                
                event.value = eventCount;
                event.created_at = [NSDate date];
                event.condiditon = rule.condition;
                
                if ([[EventRepository sharedInstance] saveWithKey:eventKey andEvent:event]) {
                    NSLog(@"Registering Bookmark Event for profile %@", @(profile.profileId));
                    [[NSNotificationCenter defaultCenter] postNotificationName:kGammmingEventHasBeenRegisteredKey object:event];
                }
            }
        }
    }
}

- (void) strippOutRulesWithRulesSet:(NSArray *)rules andCompletionBlock:(void (^)(id<RulesProtocol> rule))completionBlock {
    for (int i = 0; i < rules.count; i++) {
        id<RulesProtocol> objRule = rules[i];
        
        if (completionBlock) {
            completionBlock(objRule);
        }
    }
}

- (void) retrieveOrCreateEventWithValue:(id) value {
    
}

@end
