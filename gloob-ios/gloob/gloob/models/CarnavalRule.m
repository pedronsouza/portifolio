//
//  CarnavalRule.m
//  gloob
//
//  Created by Pedro on 3/23/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "CarnavalRule.h"
#import "EventRepository.h"
#import "Event.h"
#import "Profile.h"
#import "EventKeyFactory.h"

#define kCarnavalRuleNSCodingKeyRuleRaw @"kCarnavalRuleNSCodingKeyRuleRaw"

@interface CarnavalRule()
@property (nonatomic, strong) NSArray *validDates;
@end

@implementation CarnavalRule

- (id)initWithRuleRaw:(NSDictionary *)ruleRaw {
    self = [super initWithRuleRaw:ruleRaw];
    
    if (self) {
#if IS_DEBUG
        self.validDates = @[@"07/2015", @"08/2015", @"09/2015", @"10/2015", @"11/2015"];
#else
        self.validDates = @[@"02/2016", @"02/2017", @"02/2018", @"03/2019", @"02/2020"];
#endif
    }
    
    return self;
}

- (BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (event.value) {
        return [self isValidDate];
    }
    
    return NO;
}

- (BOOL)isValidDate {
    NSDate *markedDate = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/YYYY"];
    
    for (NSString *date in self.validDates) {
        if ([[dateFormat stringFromDate:markedDate] isEqual:date]) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)shouldMark {
    if ([self isValidDate]) {
        return YES;
    }
    
    return NO;
}

- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:self
                                                                   andParams:@[
                                                                               @(profile.profileId),
                                                                               @(self.ruleId)]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.validDates = [aDecoder decodeObjectForKey:kCarnavalRuleNSCodingKeyRuleRaw];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.validDates forKey:kCarnavalRuleNSCodingKeyRuleRaw];
}

@end
