//
//  DateRule.m
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "DateRule.h"
#import "EventRepository.h"
#import "Event.h"
#import "EventKeyFactory.h"
#import "Profile.h"
#import "NSDate+Extensions.h"

#define kDateRuleStartDateNSCodingKeyRuleRaw @"kDateRuleStartDateNSCodingKeyRuleRaw"
#define kDateRuleEndDateNSCodingKeyRuleRaw @"kDateRuleEndDateNSCodingKeyRuleRaw"

@implementation DateRule

- (id)initWithRuleRaw:(NSDictionary *)ruleRaw {
    self = [super initWithRuleRaw:ruleRaw];
    
    if (self) {
        [self setupDates];
    }
    
    return self;
}

- (void) setupDates {
    NSDateFormatter *yearFormat = [[NSDateFormatter alloc] init];
    [yearFormat setDateFormat:@"yyyy"];
    NSString *year = [yearFormat stringFromDate:[NSDate date]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    self.startDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@/%@", self.ruleRaw[@"start_date"], year]];
    self.endDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@/%@", self.ruleRaw[@"end_date"], year]];
}

- (BOOL)isValidated {
    Event *event = [self getEvent];
    
    if (event.created_at) {
        return [self isValidDate];
    }
        
    return NO;
}

- (BOOL)isValidDate {
    NSDate *eventDate = [NSDate date];
    
    return ([eventDate isBetweenDate:self.startDate andDate:self.endDate]);
}

- (BOOL)shouldMark {
    
    if ([self isValidDate]) {
        return YES;
    }
    
    return NO;
}

- (Event *)getEvent {
    Profile *profile = [Profile currentProfile];
    NSString *eventKey = [[EventKeyFactory sharedInstance] createKeyWithRule:self
                                                                   andParams:@[
                                                                               @(profile.profileId),
                                                                               @(self.ruleId)]];
    Event *event = [[EventRepository sharedInstance] getByKey:eventKey];
    return event;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.startDate = [aDecoder decodeObjectForKey:kDateRuleStartDateNSCodingKeyRuleRaw];
        self.endDate = [aDecoder decodeObjectForKey:kDateRuleEndDateNSCodingKeyRuleRaw];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.startDate forKey:kDateRuleStartDateNSCodingKeyRuleRaw];
    [aCoder encodeObject:self.endDate forKey:kDateRuleEndDateNSCodingKeyRuleRaw];
}

@end
