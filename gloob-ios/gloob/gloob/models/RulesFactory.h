//
//  RulesFactory.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Achievement.h"
#import "RulesProtocol.h"

@interface RulesFactory : NSObject

- (id<RulesProtocol>) createInstanceWithRuleId:(NSInteger) ruleId;
+ (RulesFactory *) sharedInstance;
- (id<RulesProtocol>)new_views_rule:(id)ruleRaw;
- (id<RulesProtocol>)createInstanceWithRuleRaw:(NSDictionary *)ruleRaw;
- (void) warmUp;
@property (nonatomic, strong) NSArray *rules;
- (void) warmUpAchievementsForRules;
- (NSArray *) rulesByCondition:(NSString *)condition;
@end
