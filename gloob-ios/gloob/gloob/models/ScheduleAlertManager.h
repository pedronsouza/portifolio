//
//  ScheduleAlertManager.h
//  gloob
//
//  Created by zeroum on 10/07/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ScheduleAlert.h"

@class Program;

@interface ScheduleAlertManager : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *objects_alerts;
@property (nonatomic, strong) NSMutableDictionary *showAttributes;

@property (nonatomic, strong) NSString *currentTag;
@property (nonatomic, strong) NSMutableString *tagContent;

@property (nonatomic, strong) ScheduleAlert *scheduleAlert;

+ (ScheduleAlertManager *)sharedManager;

typedef void(^completionBlock)(BOOL success, NSError *error);

- (void)loadNextFiveDaysSchedule:(completionBlock)finished;
- (NSMutableArray *)getAllSchedulesForProgramNamed:(NSString *)programNamed;
- (void)addScheduleAlert:(ScheduleAlert *)alert;
- (void)removeScheduleAlert:(ScheduleAlert *)alert;

- (NSMutableArray *)getAllValidAlerts;
- (BOOL)verifyAlert:(ScheduleAlert *)alert;
- (ScheduleAlert *)getScheduleByNotification:(UILocalNotification *)notification;

+ (BOOL)checkIfProgramHaveAtLeastOneAlert:(Program *)program;

@end
