//
//  Avatar.m
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "Avatar.h"
#import "RulesFactory.h"
#import "AvatarUnlockViewController.h"

@implementation Avatar

- (instancetype)init {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"avatars" ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
    return [self initWithAchievementRaw:[NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:nil][0]];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
}

- (UIImage *)iconForProfile {
    NSString *imageName = [NSString stringWithFormat:@"avatar_perfil_%@", self.icon];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        imageName = [NSString stringWithFormat:@"avatar_criar_selecionado_%@", self.icon];
    }
    
    return [UIImage imageNamed:imageName];
}

- (UIImage *)iconForAchievementUnlock {
    NSString *imageName = [NSString stringWithFormat:@"avatar_novoperfil_selecionado_%@", self.icon];
    return [UIImage imageNamed:imageName];
}

- (UIImage *)iconForNewProfile {
    NSString *imageName = [NSString stringWithFormat:@"avatar_criar_%@", self.icon];
    return [UIImage imageNamed:imageName];
}

- (UIImage *) iconForProfileCreation {
    NSString *imageName = [NSString stringWithFormat:@"avatar_novoperfil_%@", self.icon];
    return [UIImage imageNamed:imageName];
}

- (UIImage *) iconForProfileCreationSelected {
    NSString *imageName = [NSString stringWithFormat:@"avatar_novoperfil_selecionado_%@", self.icon];
    return [UIImage imageNamed:imageName];
}

- (UIImage *)iconForHome {
    NSString *imageName = [NSString stringWithFormat:@"avatar_home_%@", self.icon];
    return [UIImage imageNamed:imageName];
}

- (UIImage *)iconForSelection {
    NSString *imageName = [NSString stringWithFormat:@"avatar_criar_selecionado_%@", self.icon];
    return [UIImage imageNamed:imageName];
}


- (UIViewController *) viewControllerForAchievementUnlocked {
    return [[AvatarUnlockViewController alloc] initWithAchievement:self];
}
@end
