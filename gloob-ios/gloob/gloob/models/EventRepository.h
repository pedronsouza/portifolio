//
//  EventRepository.h
//  gloob
//
//  Created by Pedro on 3/19/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"

@interface EventRepository : NSObject
+ (id)sharedInstance;
- (BOOL) saveWithKey:(NSString *)key andEvent:(Event *)entity;
- (Event *) getByKey:(NSString *)key;
@end
