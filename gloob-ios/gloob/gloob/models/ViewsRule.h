//
//  ViewsRule.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RulesProtocol.h"
#import "BaseRule.h"
@interface ViewsRule : BaseRule<RulesProtocol, NSCoding>
@property (nonatomic, assign) NSInteger programId;
@end
