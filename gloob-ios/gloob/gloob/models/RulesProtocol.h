//
//  RulesProtocol.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RulesProtocol
- (BOOL) isValidated;
- (BOOL) shouldMark;
@end
