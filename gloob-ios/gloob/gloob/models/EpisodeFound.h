//
//  EpisodeFound.h
//  gloob
//
//  Created by zeroum on 23/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GloobModel.h"

@interface EpisodeFound : GloobModel

@property (nonatomic, assign) NSInteger episode_id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *episode_program;
@property (nonatomic, assign) NSInteger season;

@property (nonatomic, strong) NSString *optmized_thumb;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
