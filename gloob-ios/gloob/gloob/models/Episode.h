//
//  Episode.h
//  gloob
//
//  Created by ZeroUm on 20/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GloobModel.h"
#import "Program.h"

@interface Episode : GloobModel <NSCoding>

@property (nonatomic, strong) Program *program;

@property (nonatomic, assign) NSInteger episode_id;
@property (nonatomic, strong) NSString *thumb_image;
@property (nonatomic, strong) NSString *image_cropped;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSInteger duration;
@property (nonatomic, assign) NSInteger id_globo_videos;
@property (nonatomic, strong) NSString *ep_description;
@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NSInteger season;
@property (nonatomic, strong) NSString *category;

@property (nonatomic, strong) NSString *optimized_thumb;

// Needed for Top 5 API
@property (nonatomic, assign) NSInteger videos_views;

- (id)initWithDictionary:(NSDictionary *)dictionary andProgram:(Program *)program;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

- (void)favoriteThisEpisode;
- (void)removeFromFavorites;
- (BOOL)isFavorite;

@end
