//
//  Profile.h
//  gloob
//
//  Created by Pedro on 3/18/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobModel.h"
#import "Avatar.h"
#import "Episode.h"
#import "Selfie.h"

#define KEY_PROFILE_ID @"profile_id"
#define KEY_PROFILE_NAME @"name"
#define KEY_PROFILE_BIRTHDAY @"birthday"
#define KEY_PROFILE_CURRENT_AVATAR @"current_avatar"
#define KEY_PROFILE_FAVORITES @"favorites"
#define KEY_PROFILE_AVATARS @"avatars"
#define KEY_PROFILE_SELFIES @"selfies"

@interface Profile : GloobModel <NSCoding>

@property (nonatomic, assign) NSInteger profileId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *birthday;
@property (nonatomic, strong) Avatar *currentAvatar;
@property (nonatomic, strong) NSMutableArray *favorites; // <- Episodes
@property (nonatomic, strong) NSMutableArray *avatars;
@property (nonatomic, strong) NSMutableArray *selfies;

+ (instancetype)currentProfile;
+ (void) createInstanceAndSetupDefaultAvatar;

- (id)initNewProfileWithName:(NSString *)name birthday:(NSDate *)birthday avatar:(Avatar *)avatar;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

- (UIImage *)getHomeAvatarImage;
- (UIImage *)getCreateAvatarImage;
- (UIImage *)getCreateProfileImage;
- (BOOL)isCurrentProfile;

@end
