//
//  FeaturedHome.h
//  gloob
//
//  Created by zeroum on 17/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GloobModel.h"

@interface FeaturedHome : GloobModel

@property (nonatomic, assign) NSInteger position;
@property (nonatomic, strong) NSString *alternative_poster_image;
@property (nonatomic, assign) NSInteger id_globo_videos;
@property (nonatomic, assign) NSInteger id_featuredHome;

@property (nonatomic, strong) NSString *optimized_thumb;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
