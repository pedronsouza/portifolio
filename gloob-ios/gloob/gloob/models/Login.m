//
//  Login.m
//  gloob
//
//  Created by zeroum on 30/06/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import "Login.h"
#import "LoginManager.h"

#define KEY_LOGIN_USUARIO @"usuario"
#define KEY_LOGIN_USER_NUMBER @"uid"
#define KEY_LOGIN_NAME @"nome"
#define KEY_LOGIN_EMAIL @"email"
#define KEY_LOGIN_CLIENT_ID @"ueid"
#define KEY_LOGIN_AUTORIZADOR @"autorizador"
#define KEY_LOGIN_ACCESS_TOKEN @"access_token"
#define KEY_LOGIN_EXPIRES_IN @"expires_in"
#define KEY_LOGIN_EXPIRE_DATE @"expire_date"

@implementation Login

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        
        self.user_number = [self parseIntegerFromKey:[[dictionary objectForKey:KEY_LOGIN_USUARIO] objectForKey:KEY_LOGIN_USER_NUMBER]];
        
        self.name = [self parseStringFromKey:[[dictionary objectForKey:KEY_LOGIN_USUARIO] objectForKey:KEY_LOGIN_NAME]];
        
        self.email = [self parseStringFromKey:[[dictionary objectForKey:KEY_LOGIN_USUARIO] objectForKey:KEY_LOGIN_EMAIL]];
        
        self.client_id = [self parseStringFromKey:[[dictionary objectForKey:KEY_LOGIN_USUARIO] objectForKey:KEY_LOGIN_CLIENT_ID]];
        
        self.operadora = [self parseStringFromKey:[[dictionary objectForKey:KEY_LOGIN_AUTORIZADOR] objectForKey:KEY_LOGIN_NAME]];
        
        self.access_token = [self parseStringFromKey:[dictionary objectForKey:KEY_LOGIN_ACCESS_TOKEN]];
        
        self.expires_in = [self parseIntegerFromKey:[dictionary objectForKey:KEY_LOGIN_EXPIRES_IN]];
    }
    
    return self;
}

#pragma mark - Public Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super init]) {
        
        self.user_number = [aDecoder decodeIntegerForKey:KEY_LOGIN_USER_NUMBER];
        
        self.name = [aDecoder decodeObjectForKey:KEY_LOGIN_NAME];
        
        self.email = [aDecoder decodeObjectForKey:KEY_LOGIN_EMAIL];
        
        self.client_id = [aDecoder decodeObjectForKey:KEY_LOGIN_CLIENT_ID];
        
        self.operadora = [aDecoder decodeObjectForKey:KEY_LOGIN_AUTORIZADOR];
        
        self.access_token = [aDecoder decodeObjectForKey:KEY_LOGIN_ACCESS_TOKEN];
        
        self.expires_in = [aDecoder decodeIntegerForKey:KEY_LOGIN_EXPIRES_IN];
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.user_number forKey:KEY_LOGIN_USER_NUMBER];
    
    [aCoder encodeObject:self.name forKey:KEY_LOGIN_NAME];
    
    [aCoder encodeObject:self.email forKey:KEY_LOGIN_EMAIL];
    
    [aCoder encodeObject:self.client_id forKey:KEY_LOGIN_CLIENT_ID];
    
    [aCoder encodeObject:self.operadora forKey:KEY_LOGIN_AUTORIZADOR];
    
    [aCoder encodeObject:self.access_token forKey:KEY_LOGIN_ACCESS_TOKEN];
    
    [aCoder encodeInteger:self.expires_in forKey:KEY_LOGIN_EXPIRES_IN];
}

@end
