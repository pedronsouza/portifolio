//
//  ScheduleAlert.h
//  gloob
//
//  Created by zeroum on 10/07/14.
//  Copyright (c) 2014 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GloobModel.h"

@class Program;

@interface ScheduleAlert : GloobModel <NSCoding>

@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSString *data_short;
@property (nonatomic, strong) NSDate *fireDate;
@property (nonatomic, strong) NSString *nomePrograma;
@property (nonatomic, strong) NSString *nome;
@property (nonatomic, strong) NSString *nomeOriginal;
@property (nonatomic, assign) NSInteger numeroEpisodio;
@property (nonatomic, strong) NSString *sinopse;
@property (nonatomic, strong) NSString *notification_name;

@property (nonatomic, strong) Program *program;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

- (void)enableScheduleAlertNotification;
- (void)disableScheduleAlertNotification;
- (BOOL)isScheduleAlertEnabled;

@end
