//
//  Constants.h
//  WebmediaPlayer
//
//  Created by Bruno Torres on 5/20/13.
//  Copyright (c) 2013 Globo.com. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString* const kWebmediaPlayerParameterEnvironment;
