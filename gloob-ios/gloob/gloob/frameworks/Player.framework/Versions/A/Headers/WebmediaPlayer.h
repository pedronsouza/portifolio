//
//  WebmediaPlayer.h
//  Globo.com WebmediaPlayer
//
//  Created by Bruno Torres on 10/10/11.
//  Copyright (c) 2013 Globo.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebmediaPlayerDeviceData.h"

FOUNDATION_EXPORT NSString* const WebmediaPlayerErrorDomain;

FOUNDATION_EXPORT NSString* const WebmediaPlayerErrorAuthenticationRequired;
FOUNDATION_EXPORT NSString* const WebmediaPlayerErrorGeoblocking;
FOUNDATION_EXPORT NSString* const WebmediaPlayerErrorSimultaneousAccess;
FOUNDATION_EXPORT NSString* const WebmediaPlayerErrorPlaybackFailed;
FOUNDATION_EXPORT NSString* const WebmediaPlayerErrorUnsupportedMedia;
FOUNDATION_EXPORT NSString* const WebmediaPlayerErrorVideoNotFound;

#define PSDK_ERROR_3322 3322
#define PSDK_ERROR_3346 3346

#define WMP_PAUSE_TOO_LONG 300

typedef enum
{
    AUTHENTICATION_REQUIRED,
    DEVICE_UNAUTHORIZED,
    GEOBLOCKING,
    SIMULTANEOUS_ACCESS,
    PLAYBACK_FAILED,
    UNSUPPORTED_MEDIA,
    VIDEO_NOT_FOUND
}
PlayerErrorCode;

/**
 Event Listener Protocol
 
 Apps implementing this protocol can listen to events of the Player such as onError and onPlay.
*/
@protocol WebmediaPlayerEventListener < NSObject >

@optional
-( void )onCompletion;
/**
 This method is invoked when a known playback error occurs. The list of errors is defined by PlayerErrorCode enum:

     typedef enum
     {
         AUTHENTICATION_REQUIRED,
         DEVICE_UNAUTHORIZED,
         GEOBLOCKING,
         SIMULTANEOUS_ACCESS,
         PLAYBACK_FAILED,
         UNSUPPORTED_MEDIA,
         VIDEO_NOT_FOUND
     }
     PlayerErrorCode;
 */
-( void )onError:( NSError* )error cause:( NSError* )cause;
-( void )onStreamingPauseTooLong:( NSTimeInterval )pausedPosition;
-( void )onPause;
-( void )onPlay;
-( void )onPlayReady;
-( void )onStop;

// iOS specific events
-( void )onWillEnterFullscreen;
-( void )onWillExitFullscreen;
-( void )onDidEnterFullscreen;
-( void )onDidExitFullscreen;

-( void )onLoadStateChanged;

@end

/**
 Globo.com iOS Native Player

 This class is used to encapsulate the Globo.com API access, media model knowledge and DRM protection mechanisms (Globo.com Hash and Adobe Access) needed to video playback hosted on the Webmedia Video Platform. Once instantiated it should be [attached to](attachTo:) a UIView before playback can begin. The player will occupy the whole View and resize the video to fit that area. Applications requiring finer control over playback should implement the WebmediaPlayerEventListener protocol and register this listener to the player setting the eventListener property.

 By default the player will try to play the video immediately after being attached, using the production environment. The application can override these options using a dictionary passed to either initWithOptions: or initWithVideoId:options:.

 **Attention:** This player only supports iOS 6 and iOS 7.

 **Examples:**

     // Basic "hello, world" embed
     @interface HelloWorldViewController ()
     {
         WebmediaPlayer* player;
     }
     @end

     @implementation HelloWorldViewController

     #pragma mark - View lifecycle

     - (void)viewDidLoad
     {
         [super viewDidLoad];

         player = [[WebmediaPlayer alloc] initWithVideoId: 2313403];
         [player attachTo: self.view];
     }
     @end

 **Requirements and installation for iOS 6.x projects**

 - Add to the pre-compile header file (Supporting Files/YourProjectName.pch) after UIKit and Foundation:
    #import <Player/Player.h>

 - Add the flags -ObjC, -all_load and -lstdc++ at Build Settings, Linking, Other linker flags;

 - Add frameworks to the project at Build Phases, Link Binary With Libraries:
    * libc++.dylib
    * AVfoundation.framework
    * MediaPlayer.framework
    * AudioToolbox.framework
    * Security.framework

 **(c) 2013 Globo.com - All rights reserved - Usage of this player is restricted to parties with written permission.**
 */
@interface WebmediaPlayer : NSObject

///---------------------------------------------------------------------------------------
/// @name Public Properties
///---------------------------------------------------------------------------------------

/**
 Registers an eventListener.

 The application can listen to the player state changes and asynchronous procedures conclusions using an eventListener that implements WebmediaPlayerEventListener and registered with this method.
 */
@property (nonatomic, weak) id<WebmediaPlayerEventListener> eventListener;

/**
 Sets the user playback authentication token.
 
 The application using this player is expected to implement its own authentication and authorization flow. Once it has obtained a user authentication token (e.g. a GLBID) it must inform the player the authentication token before attempting to begin playback. The known authentication mechanisms for videos are documented elsewhere. The token should be in a cookie format (e.g. GLBID=1b0f0)
 */
@property (nonatomic, strong) NSString* authenticationToken;

/**
 Sets the user pay tv service provider used for authorization, for TV-everywhere services. This field is used only for tracking.
 */
@property (nonatomic, strong) NSString* payTvServiceProvider;

/**
 Gets current playback position as a NSTimeInterval (in seconds).
 */
@property (nonatomic, readonly) NSTimeInterval currentPosition;

/**
 Gets video duration as a NSTimeInterval (in seconds).
 */
@property (nonatomic, readonly) NSTimeInterval duration;

/**
 Whether or not the video is playing.
 */
@property (nonatomic, readonly) BOOL playing;

// Load state - internal usage only
@property (nonatomic, readonly) NSInteger loadState;

// User data - internal usage only
@property (nonatomic, readonly) NSDictionary* userData;

///---------------------------------------------------------------------------------------
/// @name Public methods
///---------------------------------------------------------------------------------------

/**
 Creates a player with no videoId nor options.

 Same as constructing with initWithVideoId:options: with videoId 0 and null options.
 */
-( id )init;

/**
 Creates a player with no options.

 Same as constructing with initWithVideoId:options: with null options.

 @param videoId a valid videoId.
 */
-( id )initWithVideoId:( NSUInteger )videoId;


-( id )initWithUrl:( NSString* )url;

/**
 Creates a player with no videoId.
 
 Same as constructing with initWithVideoId:options: with videoId 0.
 
 @param videoId a valid videoId.
 */
-( id )initWithOptions:( NSDictionary* )options;

/**
 Creates a player.

 The player created must be attached with attachTo: to a UIView before being used. The player returned can later on be reused to play other videoIds by using load:.

 **Example:**

 player = [[WebmediaPlayer alloc] initWithVideoId: 10002725 options: @{kWebmediaPlayerParameterEnvironment: [WebmediaPlayerEnvironment qa1]}];

 @param videoId a valid videoId.
 @param options a NSDictionary of key-value options. Currently it supports the options: <br>
 - kWebmediaPlayerParameterEnvironment (default: [WebmediaPlayerEnvironment production]).
 */
-( id )initWithVideoId:( NSUInteger )videoId options:( NSDictionary* )options;

/**
 Attaches the player to a UIView.

 After the player has been created, it must be included within a UIView using this method. The player will resize itself and fit the video under the containing view without distorting. It will immediately start to load the video poster if available. Then it will try to automatically play the video.

 @param view a visible UIView created by the application using this player.
 */
-( void )attachTo:( UIView* )view;


/**
 * Gets the device data used for access control before player is embedded.
 */
+( WebmediaPlayerDeviceData* )deviceData;

/**
 Loads another video on the same player.

 Once a player is created, it can be used to play other videos on the same instance and using the same configuration options by loading another videoId. This will reset the player state. Once the video is ready to play the player will fire [WebmediaPlayerEventListener onPlayReady]. Then it will try to automatically play the video.

 @param videoId a valid videoId.
 */
-( void )load:( NSUInteger )videoId;

-( void )loadUrl:( NSString* )url;

/**
 Pauses the video playback.

 If the video is playing, the playback is paused. Otherwise it is ignored. (Optional) Applications overriding the default iOS media controls must use this method to pause the video and then listen to the [WebmediaPlayerEventListener onPause] event, registered earlier at eventListener.
 */
-( void )pause;

/**
 Plays the video.

 If the video is ready to play (e.g. loaded on poster screen without auto play, or paused) it begins/resumes playback. If the video has not been loaded yet it loads the video metadata and then attempts to play it, firing metadata events along the way. (Optional) Applications overriding the default iOS media controls must use this method to play/resume the video and then listen to the [WebmediaPlayerEventListener onPlay] event, registered earlier at eventListener.
 */
-( void )play;

/**
 Move current playback position.

 If the video is ready (e.g. playing or paused) it seeks to the closest possible second indicated and resumes the original playback state. Before invoking this method, duration must be checked to find the seekable limits. Seeking outside this bounds will fail in undefined ways. (Optional) Applications overriding the default iOS media controls must use this method to implement its scrubber or equivalent seek control and then listen to the [WebmediaPlayerEventListener onPlay] event, registered earlier at eventListener.

 @param time position to seek to in seconds.
 */
-( void )seekTo:( NSTimeInterval )time;

/**
 Stops video playback.

 Stops the video playback and reset its state. Once the video is stopped, the application must listen to a [WebmediaPlayerEventListener onStop] event before attempting to use the player. After this, the same video can be played again by invoking play, or another video loaded with load:.
 */
-( void )stop;

/**
 Causes the player to enter or exit fullscreen mode.

 @param fullscreen whether or not the player should be in fullscreen mode.
 @param animated whether or not to animate the transition.
 */
-( void )setFullscreen:( BOOL )fullscreen animated:( BOOL )animated;

/**
 Causes the player to enter or exit fullscreen mode. No animation is performed.

 @param fullscreen whether or not the player should be in fullscreen mode.
 */
-( void )setFullscreen:( BOOL )fullscreen;

/**
 Whether the player is in fullscreen mode.
 */
-( BOOL )fullscreen;

//-( void )showDeviceActionSheetFromRect:( CGRect )rect inView:( UIView* )view;

@end
