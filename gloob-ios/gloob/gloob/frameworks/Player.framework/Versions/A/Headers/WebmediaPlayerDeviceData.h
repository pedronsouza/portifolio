//
//  WMPDeviceData.h
//  Player
//
//  Created by Danilo Moret on 2013-30-07.
//  Copyright (c) 2013 Globo.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebmediaPlayerDeviceData : NSObject

-( WebmediaPlayerDeviceData* )init;

@property (nonatomic, strong) NSString* udid;
@property (nonatomic, strong) NSString* deviceName;

@end
