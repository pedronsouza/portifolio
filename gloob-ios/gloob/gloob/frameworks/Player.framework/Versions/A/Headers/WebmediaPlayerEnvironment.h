//
//  WebmediaPlayerEnvironment.h
//  WebmediaPlayer
//
//  Created by Bruno Torres on 5/20/13.
//  Copyright (c) 2013 Globo.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebmediaPlayerEnvironment : NSObject

@property (nonatomic, readonly) NSString* securityBaseUrl;
@property (nonatomic, readonly) NSString* liveVideoBaseUrl;
@property (nonatomic, readonly) NSString* playerBaseUrl;
@property (nonatomic, readonly) NSString* thumbBaseUrl;
@property (nonatomic, readonly) NSString* statsBaseUrl;
@property (nonatomic, readonly) NSString* cookiesDomain;

+( WebmediaPlayerEnvironment * )qa1;
+( WebmediaPlayerEnvironment * )production;

@end
