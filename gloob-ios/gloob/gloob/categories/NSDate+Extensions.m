//
//  NSDate+Extensions.m
//  gloob
//
//  Created by Pedro on 3/25/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "NSDate+Extensions.h"

@implementation NSDate (Extensions)
- (BOOL)isBetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate
{
    if ([self compare:earlierDate] == NSOrderedDescending) {
        
        if ( [self compare:laterDate] == NSOrderedAscending ) {
            return YES;
        }
    }
    
    return NO;
}
@end
