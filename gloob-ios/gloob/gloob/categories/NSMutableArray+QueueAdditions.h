//
//  NSMutableArray+QueueAdditions.h
//  gloob
//
//  Created by zeroum on 03/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (QueueAdditions)

- (id)dequeue;
- (void)enqueue:(id)object;

@end
