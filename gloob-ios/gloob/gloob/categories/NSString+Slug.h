//
//  NSString+Slug.h
//  gloob
//
//  Created by ZeroUm on 22/06/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Slug)

- (NSString *)slugalize;

@end
