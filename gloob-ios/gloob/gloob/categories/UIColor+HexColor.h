//
//  UIColor+HexColor.h
//  gloob
//
//  Created by zeroum on 29/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (UIColor *)hexColor:(int)hex;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert alpha:(CGFloat)alpha;

@end
