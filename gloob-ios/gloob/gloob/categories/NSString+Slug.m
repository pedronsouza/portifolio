//
//  NSString+Slug.m
//  gloob
//
//  Created by ZeroUm on 22/06/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "NSString+Slug.h"

@implementation NSString (Slug)

- (NSString *)stringWithoutAccentsFromString:(NSString*)s {
    if (!s) return nil;
    NSMutableString *result = [NSMutableString stringWithString:s];
    CFStringFold((CFMutableStringRef)result, kCFCompareDiacriticInsensitive, NULL);
    return result;
}

/**
 * Port of the slugalized helper created by @henrik
 * https://github.com/RobertAudi/slugalizer
 */
- (NSString *)slugalize {
    NSString        *separator        = @"-";
    NSMutableString *slugalizedString = [NSMutableString string];
    NSRange         replaceRange      = NSMakeRange(0, self.length);

    // Remove all non ASCII characters
    NSError *nonASCIICharsRegexError = nil;
    NSRegularExpression *nonASCIICharsRegex = [NSRegularExpression regularExpressionWithPattern:@"[^\\x00-\\x7F]+"
                                                                                        options:0
                                                                                          error:&nonASCIICharsRegexError];
    
    slugalizedString = [[nonASCIICharsRegex stringByReplacingMatchesInString:[self stringWithoutAccentsFromString:self]
                                                                     options:0
                                                                       range:replaceRange
                                                                withTemplate:@""] mutableCopy];
    
    // Turn non-slug characters into separators
    NSError *nonSlugCharactersError = nil;
    NSRegularExpression *nonSlugCharactersRegex = [NSRegularExpression regularExpressionWithPattern:@"[^a-z0-9\\-_\\+]+"
                                                                                            options:NSRegularExpressionCaseInsensitive
                                                                                              error:&nonSlugCharactersError];
    replaceRange      = NSMakeRange(0, slugalizedString.length);
    slugalizedString = [[nonSlugCharactersRegex stringByReplacingMatchesInString:slugalizedString
                                                                         options:0
                                                                           range:replaceRange
                                                                    withTemplate:separator] mutableCopy];
    
    // No more than one of the separator in a row
    NSError *repeatingSeparatorsError = nil;
    NSRegularExpression *repeatingSeparatorsRegex = [NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"%@{2,}", separator]
                                                                                              options:0
                                                                                                error:&repeatingSeparatorsError];
    
    replaceRange      = NSMakeRange(0, slugalizedString.length);
    slugalizedString = [[repeatingSeparatorsRegex stringByReplacingMatchesInString:slugalizedString
                                                                           options:0
                                                                             range:replaceRange
                                                                      withTemplate:separator] mutableCopy];
    
    // Remove leading/trailing separator
    slugalizedString = [[slugalizedString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:separator]] mutableCopy];
    
    return [slugalizedString lowercaseString];
}

@end
