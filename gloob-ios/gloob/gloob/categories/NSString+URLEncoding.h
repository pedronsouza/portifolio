//
//  NSString+URLEncoding.h
//  gloob
//
//  Created by zeroum on 29/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncoding)

- (NSString *)urlencode;

@end
