//
//  NSMutableArray+QueueAdditions.m
//  gloob
//
//  Created by zeroum on 03/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "NSMutableArray+QueueAdditions.h"

@implementation NSMutableArray (QueueAdditions)

#pragma mark - Public

- (id)dequeue {
    
    if (self.count == 0) {
        return nil;
    }
    
    id headObject = [self objectAtIndex:0];
    
    if (headObject != nil) {
        [self removeObjectAtIndex:0];
    }
    
    return headObject;
}

- (void)enqueue:(id)object {
    
    [self addObject:object];
}

@end
