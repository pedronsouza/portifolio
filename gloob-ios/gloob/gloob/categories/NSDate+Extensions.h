//
//  NSDate+Extensions.h
//  gloob
//
//  Created by Pedro on 3/25/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extensions)
- (BOOL)isBetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate;
@end
