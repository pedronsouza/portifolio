//
//  NSMutableArray+Shuffling.h
//  gloob
//
//  Created by zeroum on 29/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

/** This category enhances NSMutableArray by providing methods to randomly
 * shuffle the elements using the Fisher-Yates algorithm.
 */
@interface NSMutableArray (Shuffling)

- (void)shuffle;

@end

#import "NSMutableArray+Shuffling.h"

@implementation NSMutableArray (Shuffling)

- (void)shuffle {
    
    for (uint i = 0; i < self.count; ++i) {
        
        // Select a random element between i and end of array to swap with.
        int nElements = (int)self.count - i;
        int n = arc4random_uniform(nElements) + i;
        
        [self exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}

@end