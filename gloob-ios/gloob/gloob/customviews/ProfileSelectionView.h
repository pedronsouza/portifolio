//
//  AvatarSelectionView.h
//  gloob
//
//  Created by Pedro on 4/30/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Avatar.h"
#import "Profile.h"
#define kAvatarHasBeenSelected @"kAvatarHasBeenSelected"
@protocol ProfileSelectionViewDelegate
@optional
- (void) didSelectProfile:(Profile *)profile;
@end

@interface ProfileSelectionView : UIViewController
- (instancetype) initWithProfile:(Profile *)profile andAvatar:(Avatar *)avatar;
@property (nonatomic, strong) id<ProfileSelectionViewDelegate> delegate;
@property (nonatomic, weak) IBOutlet UIImageView *imgAvatar;

@property (nonatomic, weak) IBOutlet UIImageView *imgSelection;
@property (nonatomic, weak) IBOutlet UILabel *lblProfileName;
@end
