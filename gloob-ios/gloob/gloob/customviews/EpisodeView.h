//
//  EpisodeView.h
//  gloob
//
//  Created by zeroum on 12/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Episode;

@interface EpisodeView : UIView

@property (nonatomic, assign) id delegate;

@property (nonatomic, assign) NSMutableArray *videosList;
@property (nonatomic, assign) NSInteger videoIndex;
@property (nonatomic, strong) NSString *openingFrom;

- (id)initWithFrame:(CGRect)frame episode:(Episode *)episode delegate:(id)delegate;

@end

@protocol EpisodeViewDelegate <NSObject>

@optional

- (void)playEpisode:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from;

@end