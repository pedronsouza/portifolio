//
//  EpisodeButton.h
//  gloob
//
//  Created by zeroum on 31/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Episode;

@interface EpisodeButton : UIButton

@property (nonatomic, strong) Episode *episode;

@property (nonatomic, assign) NSInteger indexPath;
@property (nonatomic, assign) NSInteger section;

@property (nonatomic, assign) id delegate;

- (id)initWithFrame:(CGRect)frame episode:(Episode *)episode indexPath:(NSInteger)indexPath section:(NSInteger)section delegate:(id)delegate;

- (void)addTarget;

@end

@protocol EpisodeButtonDelegate <NSObject>

- (void)didSelectEpisode:(Episode *)episode atIndexPath:(NSInteger)indexPath inSection:(NSInteger)section;

@end
