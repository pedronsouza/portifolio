//
//  ScheduleAlertView.m
//  gloob
//
//  Created by zeroum on 05/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ScheduleAlertView.h"
#import "ScheduleAlert.h"
#import "Program.h"
#import "UIColor+HexColor.h"
#import "ColoredButton.h"
#import "UIImageView+WebCache.h"

@interface ScheduleAlertView ()

@property (nonatomic, strong) Program *program;

@property (nonatomic, strong) ScheduleAlert *schedule;

@property (nonatomic, assign) id delegate;

- (void)setupCharacterImage;
- (void)setupAlertInfo;
- (void)setupButton;

- (void)didTouchUpInside:(UIButton *)sender;

@end

@implementation ScheduleAlertView

- (id)initWithFrame:(CGRect)frame schedule:(ScheduleAlert *)schedule delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.schedule = schedule;
        self.program = schedule.program;
        self.delegate = delegate;
        
        [self setupCharacterImage];
        [self setupAlertInfo];
        [self setupButton];
    }
    
    return self;
}

#pragma mark - Private

- (void)setupCharacterImage {

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 160.0f)];
    
    if (self.program.alternative_poster_image) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:self.program.alternative_poster_image]];
    }
    
    [self addSubview:imgView];
}

- (void)setupAlertInfo {
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 160.0f, 160.0f, 20.0f)];
    
    [title setText:self.schedule.nomePrograma];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:16.0f]];
    [title setTextColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
    
    [self addSubview:title];
    
    UILabel *episode = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 180.0f, 160.0f, 20.0f)];
    
    [episode setText:self.schedule.nomeOriginal];
    [episode setTextAlignment:NSTextAlignmentCenter];
    [episode setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
    [episode setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    
    [self addSubview:episode];

    UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 200.0f, 160.0f, 20.0f)];
    
    [date setText:self.schedule.data_short];
    [date setTextAlignment:NSTextAlignmentCenter];
    [date setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:16.0f]];
    [date setTextColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
    
    [self addSubview:date];
}

- (void)setupButton {

    ColoredButton *button = [[ColoredButton alloc] initWithFrame:CGRectMake(0.0f, 225.0f, 160.0f, 52.0f)];
    
    [button setValue:@"orange" forKeyPath:@"CustomStyle"];
    
    [button setTitle:@" REMOVER"
            forState:UIControlStateNormal];
    
    [button setTitle:@" REMOVER"
            forState:UIControlStateHighlighted];
    
    [button addTarget:self
               action:@selector(didTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [button awakeFromNib];
    
    [button setImage:[UIImage imageNamed:@"pf_ico_alarme"]
            forState:UIControlStateNormal];
    
    [button setImage:[UIImage imageNamed:@"pf_ico_alarme"]
            forState:UIControlStateHighlighted];
    
    [button.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:19.0f]];
    
    [self addSubview:button];
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(removeScheduleFromList:)]) {
        [self.delegate removeScheduleFromList:self.schedule];
    }
}

@end
