//
//  GloobTextField.m
//  gloob
//
//  Created by zeroum on 16/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GloobTextField.h"

@implementation GloobTextField

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self setBorderStyle:UITextBorderStyleNone];
    
    [self setBackground:[[UIImage imageNamed:@"pf_campo_flex"] resizableImageWithCapInsets:UIEdgeInsetsMake(12.0f, 12.0f, 12.0f, 12.0f)]];
    
    [self setFont:[UIFont fontWithName:GLOOB_FONT_REGULAR size:19.0f]];
    [self setTextAlignment:NSTextAlignmentCenter];
    
    [self setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self setClearsOnBeginEditing:YES];
    [self setReturnKeyType:UIReturnKeyDone];
}

@end
