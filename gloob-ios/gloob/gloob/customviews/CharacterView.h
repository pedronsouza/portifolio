//
//  CharacterView.h
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Program;

@interface CharacterView : UIView

@property (nonatomic, assign) id delegate;

- (id)initWithFrame:(CGRect)frame program:(Program *)program delegate:(id)delegate;

@end

@protocol CharacterViewDelegate <NSObject>

- (void)displayCharacterInPopUp:(Program *)character;

@end