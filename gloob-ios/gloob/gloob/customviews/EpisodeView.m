//
//  EpisodeView.m
//  gloob
//
//  Created by zeroum on 12/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "EpisodeView.h"
#import "Episode.h"
#import "UIColor+HexColor.h"
#import "UIButton+WebCache.h"

@interface EpisodeView ()

@property (nonatomic, weak) Episode *episode;

- (void)setupButtonWithFrame:(CGRect)frame;
- (void)didTouchUpInside:(UIButton *)sender;
- (void)setupEpisodeInfoWithFrame:(CGRect)frame;
- (void)setupActivityIndicatorWithFrame:(CGRect)frame;

@end

@implementation EpisodeView

- (id)initWithFrame:(CGRect)frame episode:(Episode *)episode delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {

        self.episode = episode;
        self.delegate = delegate;
        
        [self setBackgroundColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
        
        [self setupActivityIndicatorWithFrame:frame];
        [self setupButtonWithFrame:frame];
        [self setupEpisodeInfoWithFrame:frame];
    }
    
    return self;
}

#pragma mark - Private

- (void)setupButtonWithFrame:(CGRect)frame {

    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
    
    if (self.episode.image_cropped == nil) {
        [button setBackgroundImage:[UIImage imageNamed:@"placeholder_large"]
                          forState:UIControlStateNormal];
    } else {
        [button sd_setImageWithURL:[NSURL URLWithString:self.episode.image_cropped] forState:UIControlStateNormal];
    }
    
    [button addTarget:self
               action:@selector(didTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:button];
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(playEpisode:fromList:index:openingFrom:)]) {
        
        [self.delegate playEpisode:self.episode
                          fromList:self.videosList
                             index:self.videoIndex
                       openingFrom:self.openingFrom];
    }
}

- (void)setupEpisodeInfoWithFrame:(CGRect)frame {
    
    CGFloat margin = 2.0f;
    CGSize imgSize = CGSizeMake(19.0f, 16.0f);
    
    CGRect playFrame = CGRectMake(margin,
                                  frame.size.height - (imgSize.height + margin),
                                  imgSize.width,
                                  imgSize.height);
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:playFrame];
    
    [imgView setImage:[UIImage imageNamed:@"hm_ico_play"]];
    
    [self addSubview:imgView];
    
    CGRect lblFrame = CGRectMake(margin * 2 + imgSize.width,
                                 frame.size.height - (imgSize.height + margin),
                                 75.0f,
                                 imgSize.height);
    
    UILabel *lblEpisode = [[UILabel alloc] initWithFrame:lblFrame];
    
    [lblEpisode setText:[NSString stringWithFormat:@"EP. %li", (long)self.episode.number]];
    [lblEpisode setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:12.0f]];
    [lblEpisode setBackgroundColor:[UIColor clearColor]];
    [lblEpisode setTextColor:[UIColor whiteColor]];
    
    [self addSubview:lblEpisode];
}

- (void)setupActivityIndicatorWithFrame:(CGRect)frame {
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] init];
    
    [spinner setCenter:CGPointMake(frame.size.width / 2, frame.size.height / 2)];
    [spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [spinner setHidesWhenStopped:YES];
    
    [self addSubview:spinner];
    
    [spinner startAnimating];
}

@end
