//
//  GameView.m
//  gloob
//
//  Created by zeroum on 03/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GameView.h"
#import "Game.h"
#import "UIColor+HexColor.h"
#import "UIButton+WebCache.h"

@interface GameView ()

@property (nonatomic, strong) UIActivityIndicatorView *spinner;

- (void)setupBackgroundImageWithFrame:(CGRect)frame;
- (void)setupActivityIndicatorViewInFrame:(CGRect)frame;
- (void)setupButtonWithFrame:(CGRect)frame;

- (void)didTouchUpInside:(UIButton *)sender;

@end

@implementation GameView

- (id)initWithFrame:(CGRect)frame game:(Game *)game delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.game = game;
        self.delegate = delegate;
        
        CGRect rect = CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height);
        
        [self setBackgroundColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
        
        [self setupActivityIndicatorViewInFrame:rect];
        [self setupBackgroundImageWithFrame:rect];
//        [self setupButtonWithFrame:rect];
        
    }
    
    return self;
}

#pragma mark - Private

- (void)setupBackgroundImageWithFrame:(CGRect)frame {
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    
    [button addTarget:self
               action:@selector(didTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
    
    if (self.game.image == nil) {
        [button setImage:[UIImage imageNamed:@"placeholder_large"] forState:UIControlStateNormal];
    } else {
        [button sd_setImageWithURL:[NSURL URLWithString:self.game.image] forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self.spinner stopAnimating];
        }];
    }
    
    [self addSubview:button];
}

- (void)setupActivityIndicatorViewInFrame:(CGRect)frame {
    
    CGFloat x = (frame.size.width - 20.0f) / 2;
    CGFloat y = (frame.size.height - 20.0f) / 2;
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(x, y, 20.0f, 20.0f)];
    
    [self.spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [self.spinner setHidesWhenStopped:YES];
    
    [self.spinner startAnimating];
    
    [self addSubview:self.spinner];
}

- (void)setupButtonWithFrame:(CGRect)frame {
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    
    [button addTarget:self
               action:@selector(didTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:button];
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(openGame:)]) {
        [self.delegate openGame:self.game];
    }
}

@end
