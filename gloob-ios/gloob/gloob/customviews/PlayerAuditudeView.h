//
//  PlayerAuditudeView.h
//  gloob
//
//  Created by zeroum on 06/04/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuditudeView.h"

@interface PlayerAuditudeView : UIView <AuditudeViewDelegate>

@end
