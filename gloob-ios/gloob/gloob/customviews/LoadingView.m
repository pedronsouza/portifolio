//
//  LoadingView.m
//  gloob
//
//  Created by zeroum on 02/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//


// Default size for LoadingView is 150x100

#import "LoadingView.h"

@implementation LoadingView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 150.0f, 100.0f)];
    
    [view setBackgroundColor:[UIColor colorWithRed:0.0f
                                             green:0.0f
                                              blue:0.0f
                                             alpha:0.6f]];
    
    [view.layer setCornerRadius:12.0f];
    
    [self addSubview:view];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(57.0f, 17.0f, 37.0f, 37.0f)];
    
    [spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner startAnimating];
    
    [view addSubview:spinner];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8.0f, 62.0f, 134.0f, 21.0f)];
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:17.0f]];
    
    if (self.LabelText) {
        [label setText:self.LabelText];
    } else {
        [label setText:@"Carregando..."];
    }
    
    [view addSubview:label];
}

#pragma mark - Public

- (void)showLoadingViewWithCompletion:(completion)completed {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self setAlpha:1.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             
                             if (completed != nil) {
                                 completed(YES);
                             }
                             
                         }
                     }];
}

- (void)hideLoadingViewWithCompletion:(completion)completed {
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         [self setAlpha:0.0f];
                         
                     } completion:^(BOOL finished) {
                         
                         if (finished) {
                             
                             if (completed != nil) {
                                 completed(YES);
                             }
                         }
                     }];
}


@end
