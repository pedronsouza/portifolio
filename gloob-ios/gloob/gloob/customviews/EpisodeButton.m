//
//  EpisodeButton.m
//  gloob
//
//  Created by zeroum on 31/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "EpisodeButton.h"
#import "Episode.h"

@interface EpisodeButton ()

- (void)didTouchUpInside:(UIButton *)sender;

@end

@implementation EpisodeButton

- (id)initWithFrame:(CGRect)frame episode:(Episode *)episode indexPath:(NSInteger)indexPath section:(NSInteger)section delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.episode = episode;
        self.indexPath = indexPath;
        self.section = section;
        self.delegate = delegate;
        
        [self addTarget];
    }
    
    return self;
}

- (void)addTarget {
    [self addTarget:self
             action:@selector(didTouchUpInside:)
   forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Private

- (void)didTouchUpInside:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(didSelectEpisode:atIndexPath:inSection:)]) {
        [self.delegate didSelectEpisode:self.episode atIndexPath:self.indexPath inSection:self.section];
    }
}

@end
