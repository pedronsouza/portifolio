//
//  FavoriteView.h
//  gloob
//
//  Created by zeroum on 27/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Episode;

@interface FavoriteView : UIView

@property (nonatomic, assign) id delegate;

@property (nonatomic, weak) Episode *favorite;

@property (nonatomic, assign) NSInteger videoIndex;

- (id)initWithFrame:(CGRect)frame favorite:(Episode *)favorite delegate:(id)delegate;

@end

@protocol FavoriteViewDelegate <NSObject>

- (void)deleteFavorite:(Episode *)favorite;

@optional

- (void)playEpisode:(Episode *)episode fromList:(NSMutableArray *)list index:(NSInteger)index openingFrom:(NSString *)from;

@end