//
//  AvatarView.m
//  gloob
//
//  Created by zeroum on 24/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "AvatarView.h"
#import "Avatar.h"
#import "Profile.h"
#import "UIColor+HexColor.h"

@interface AvatarView ()

@property (nonatomic, strong) UIImageView *imgViewSelector;

@property (nonatomic, strong) UIButton *btnAvatar;
@property (nonatomic, strong) UIButton *btnAvatarSelected;
@property (nonatomic, strong) UIButton *btnChangeProfile;
@property (nonatomic, strong) UIButton *btnDelete;

@property (nonatomic, strong) UILabel *lblCurrentProfile;

@property (nonatomic, assign) BOOL isHorizontal;
@property (nonatomic, assign) BOOL isCurrentProfile;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) CGRect btnAvatarRect;
@property (nonatomic, assign) CGRect viewFrame;
@property (nonatomic, strong) Profile *profile;
@property (nonatomic, assign) BOOL isProfileSelection;

- (void)setupHorizontalButton;
- (void)setupHorizontalSelector;

- (void)setupVerticalButton;
- (void)setupVerticalSelector;

- (void)setupSignInButton;
- (void)setupProfileInfo;
- (void)setupDeleteProfileButton;

- (void)didTouchUpInside:(UIButton *)sender;
- (void)didTouchUpInsideSelectButton:(UIButton *)sender;
- (void)didTouchUpInsideDeleteButton:(UIButton *)sender;

@end

@implementation AvatarView

- (id)initHorizontalScrollingAvatarWithFrame:(CGRect)frame avatar:(Avatar *)avatar delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        self.viewFrame = frame;
        self.avatar = avatar;
        self.delegate = delegate;
        self.isHorizontal = YES;
        
        [self setupHorizontalButton];
        [self setupHorizontalSelector];
    }
    
    return self;
}

- (id)initVerticalScrollingAvatarWithFrame:(CGRect)frame avatar:(Avatar *)avatar delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.avatar = avatar;
        self.delegate = delegate;
        self.isHorizontal = NO;
        
        [self setupVerticalSelector];
        [self setupVerticalButton];
    }
    
    return self;
}

- (id)initDeletableAvatarWithFrame:(CGRect)frame profile:(Profile *)profile avatar:(Avatar *)avatar delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.profile = profile;
        self.avatar = avatar;
        self.delegate = delegate;
        
        [self setupSignInButton];
        [self setupProfileInfo];
        [self setupDeleteProfileButton];
    }
    
    return self;
}

#pragma mark - Public

- (void)setSelected:(BOOL)selected {
    
    [self.btnAvatar setSelected:selected];
    
    if (!self.profile) {
        
        if (self.isHorizontal) {
            
            if (selected) {
                
                
                [self.btnAvatar setUserInteractionEnabled:NO];
                [self.btnAvatar setHidden:YES];
                CGRect viewFrame = self.viewFrame;
                viewFrame.size.height += 40;
                viewFrame.size.width += 40;
                CGRect selectorFrame = self.imgViewSelector.frame;

                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    selectorFrame.origin.y = 25;
                    viewFrame.origin.y = -25;
                }
                
                self.frame = viewFrame;
                self.imgViewSelector.frame = selectorFrame;
                [self.imgViewSelector setHidden:NO];
                [self.btnAvatarSelected setHidden:NO];
                
            } else {
                
                [self.imgViewSelector setHidden:YES];
                [self.btnAvatar setUserInteractionEnabled:YES];
                [self.btnAvatar setHidden:NO];
                self.frame = self.viewFrame;
                [self.btnAvatarSelected setHidden:YES];
            }
            
        } else {
            
            if (selected) {
                
                [self.imgViewSelector setHidden:NO];
                CGRect selectorFrame = self.imgViewSelector.frame;
                selectorFrame.origin.x = (self.btnAvatar.frame.size.width / 2) - 10;
                selectorFrame.origin.y = (self.btnAvatar.frame.size.height /2) -20;
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    selectorFrame.origin.y = (self.btnAvatar.frame.size.height /2);
                }
                
                CGRect btnAvatarRect = self.btnAvatar.frame;
                btnAvatarRect.origin.x = self.btnAvatar.frame.size.width - 20;
                [self.btnAvatar setFrame:btnAvatarRect];
                self.imgViewSelector.frame = selectorFrame;
                
                [self.btnAvatar setFrame:btnAvatarRect];
                self.imgViewSelector.frame = selectorFrame;
                [self.btnAvatar setUserInteractionEnabled:NO];
                
            } else {
                
                [self.imgViewSelector setHidden:YES];

                CGRect selectorFrame = self.imgViewSelector.frame;
                selectorFrame.origin.x = self.btnAvatar.frame.size.width / 2;
                selectorFrame.origin.y = (self.btnAvatar.frame.size.height /2) +20;
                
                CGRect btnAvatarRect = self.btnAvatar.frame;
                btnAvatarRect.origin.x = self.btnAvatar.frame.size.width / 2;
                
                [self.btnAvatar setFrame:btnAvatarRect];
                self.imgViewSelector.frame = selectorFrame;
                [self.btnAvatar setUserInteractionEnabled:YES];
            }
        }
        
    } else {
        
        if (selected) {
            
            [self.lblCurrentProfile setHidden:NO];
            [self.btnDelete setHidden:YES];
            [self.btnChangeProfile setHidden:YES];
            [self.btnAvatar setUserInteractionEnabled:NO];
            
        } else {
            
            [self.lblCurrentProfile setHidden:YES];
            [self.btnDelete setHidden:NO];
            [self.btnChangeProfile setHidden:NO];
            [self.btnAvatar setUserInteractionEnabled:YES];
        }
    }
}

#pragma mark - Horizontal

- (void)setupHorizontalButton {
    
    CGRect frame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        frame = CGRectMake(0.0f, self.frame.size.height - 135, 145.0f, 135.0f);
    } else {
        frame = CGRectMake(0.0f, self.frame.size.height - 93, 100.0f, 93.0f);
    }
    
    self.btnAvatar = [[UIButton alloc] initWithFrame:frame];
    self.btnAvatarSelected = [[UIButton alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y + 30, frame.size.width + 30, frame.size.height + 30)];
    [self.btnAvatarSelected setHidden:YES];
    [self.btnAvatar setImage:[self.avatar iconForProfileCreation] forState:UIControlStateNormal];
    [self.btnAvatarSelected setImage:[self.avatar iconForProfileCreationSelected] forState:UIControlStateNormal];
    [self.btnAvatar setContentMode:UIViewContentModeCenter];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGRect selectedBtFrame = self.btnAvatarSelected.frame;
        selectedBtFrame.size = CGSizeMake(selectedBtFrame.size.width * 0.70, selectedBtFrame.size.height * 0.70);
        selectedBtFrame.origin = CGPointMake(selectedBtFrame.origin.x, selectedBtFrame.origin.y - 30);
        self.btnAvatarSelected.frame = selectedBtFrame;
    }
    
    CGRect avatarFrame = self.btnAvatar.frame;
    avatarFrame.size = CGSizeMake(self.btnAvatarSelected.frame.size.width, self.btnAvatarSelected.frame.size.height);
    [self.btnAvatar setFrame:avatarFrame];
    [self.btnAvatar setContentMode:UIViewContentModeCenter];
    
    [self.btnAvatar addTarget:self
                       action:@selector(didTouchUpInside:)
             forControlEvents:UIControlEventTouchUpInside];
    [self.btnAvatarSelected addTarget:self
                       action:@selector(didTouchUpInside:)
             forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.btnAvatar];
    [self addSubview:self.btnAvatarSelected];
}

- (void)setupHorizontalSelector {
    
    CGRect frame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        frame = CGRectMake(75.0f, 0.0f, 27.0f, 44.0f);
    } else {
        frame = CGRectMake(37.0f, 0.0f, 18.0f, 30.0f);
    }
    
    self.imgViewSelector = [[UIImageView alloc] initWithFrame:frame];
    
    [self.imgViewSelector setImage:[UIImage imageNamed:@"pf_mao_vertical"]];
    
    [self.imgViewSelector setHidden:YES];
    
    [self addSubview:self.imgViewSelector];
}

#pragma mark - Vertical

- (void)setupVerticalButton {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.btnAvatarRect = CGRectMake(90 / 2, self.btnAvatar.frame.origin.y, 90.0f, 83.0f);
    } else {
        self.btnAvatarRect  = CGRectMake(28.0f, 4.0f, 62.0f, 57.0f);
    }
    
    self.btnAvatar = [[UIButton alloc] initWithFrame:self.btnAvatarRect];
    [self.btnAvatar setImage:[self.avatar iconForNewProfile]
                    forState:UIControlStateNormal];
    
    [self.btnAvatar addTarget:self
                       action:@selector(didTouchUpInside:)
             forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.btnAvatar];
}

- (void)setupVerticalSelector {
    
    CGRect frame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        frame = CGRectMake(0.0f, 8.0f, 44.0f, 28.0f);
    } else {
        frame = CGRectMake(0.0f, 4.0f, 30.0f, 19.0f);
    }
    
    self.imgViewSelector = [[UIImageView alloc] initWithFrame:frame];
    
    [self.imgViewSelector setImage:[UIImage imageNamed:@"pf_mao"]];
    
    [self.imgViewSelector setHidden:YES];
    
    [self addSubview:self.imgViewSelector];
}

#pragma mark - Sign in

- (void)setupSignInButton {
    
    CGRect frame;
    CGRect frame2;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        frame = CGRectMake(27.0f, 10.0f, 145.0f, 135.0f);
        frame2 = CGRectMake(27.0f, 190.0f, 145.0f, 21.0f);
    } else {
        frame = CGRectMake(25.0f, 14.0f, 100.0f, 94.0f);
        frame2 = CGRectMake(0.0f, 149.0f, 150.0f, 21.0f);
    }
    
    self.btnAvatar = [[UIButton alloc] initWithFrame:frame];
    
    
    [self.btnAvatar  setImage:[self.avatar iconForAchievementUnlock]
                     forState:UIControlStateNormal];
    
    [self.btnAvatar  addTarget:self
                        action:@selector(didTouchUpInsideSelectButton:)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.btnAvatar];
    
    self.btnChangeProfile = [[UIButton alloc] initWithFrame:frame2];
    
    [self.btnChangeProfile setTitleColor:[UIColor colorWithHexString:@"0x7765AF"] forState:UIControlStateNormal];
    [self.btnChangeProfile setTitle:@"TROCAR PERFIL" forState:UIControlStateNormal];
    [self.btnChangeProfile.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
    
    [self.btnChangeProfile addTarget:self
                              action:@selector(didTouchUpInsideSelectButton:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.btnChangeProfile];
    
    self.lblCurrentProfile = [[UILabel alloc] initWithFrame:frame2];
    
    [self.lblCurrentProfile setText:@"(PERFIL ATIVO)"];
    [self.lblCurrentProfile setTextAlignment:NSTextAlignmentCenter];
    [self.lblCurrentProfile setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
    [self.lblCurrentProfile setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    
    [self addSubview:self.lblCurrentProfile];
}

- (void)setupProfileInfo {
    
    CGRect frame1;
    CGRect frame2;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        frame1 = CGRectMake(0.0f, 155.0f, 200.0f, 20.0f);
        frame2 = CGRectMake(0.0f, 175.0f, 200.0f, 20.0f);
        
    } else {
        
        frame1 = CGRectMake(0.0f, 109.0f, 150.0f, 20.0f);
        frame2 = CGRectMake(0.0f, 131.0f, 150.0f, 20.0f);
    }
    
    UILabel *name = [[UILabel alloc] initWithFrame:frame1];
    
    [name setText:self.profile.name];
    [name setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:16.0f]];
    [name setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    [name setTextAlignment:NSTextAlignmentCenter];
    [name setAdjustsFontSizeToFitWidth:YES];
    [name setMinimumScaleFactor:0.75f];
    
    [self addSubview:name];
    
    UILabel *birthday = [[UILabel alloc] initWithFrame:frame2];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    
    [birthday setText:[dateFormatter stringFromDate:self.profile.birthday]];
    [birthday setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:16.0f]];
    [birthday setTextColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
    [birthday setTextAlignment:NSTextAlignmentCenter];
    [birthday setAdjustsFontSizeToFitWidth:YES];
    [birthday setMinimumScaleFactor:0.75f];
    
    [self addSubview:birthday];
}

- (void)setupDeleteProfileButton {
    
    CGRect frame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        frame = CGRectMake(155.0f, 0.0f, 45.0f, 48.0f);
    } else {
        frame = CGRectMake(114.0f, 0.0f, 36.0f, 38.0f);
    }
    
    self.btnDelete = [[UIButton alloc] initWithFrame:frame];
    
    [self.btnDelete setImage:[UIImage imageNamed:@"bt_fechar"] forState:UIControlStateNormal];
    [self.btnDelete setImage:[UIImage imageNamed:@"bt_fechar_tap"] forState:UIControlStateHighlighted];
    
    [self.btnDelete addTarget:self
                       action:@selector(didTouchUpInsideDeleteButton:)
             forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnDelete setHidden:YES];
    
    [self addSubview:self.btnDelete];
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(didSelectedAvatar:)]) {
        [self.delegate didSelectedAvatar:self.avatar];
    }
}

- (void)didTouchUpInsideSelectButton:(UIButton *)sender {
    
    if (self.profile) {
        
        if ([self.delegate respondsToSelector:@selector(didSelectProfile:)]) {
            [self.delegate didSelectProfile:self.profile];
        }
    }
}

- (void)didTouchUpInsideDeleteButton:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(deleteProfile:)]) {
        [self.delegate deleteProfile:self.profile];
    }
}
- (void) setTitleForAvatarLabel:(NSString *)title {
    [self.lblCurrentProfile setText:title];
    [self.btnChangeProfile setHidden:YES];
    self.isProfileSelection = YES;
}
@end
