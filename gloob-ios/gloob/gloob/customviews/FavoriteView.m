//
//  FavoriteView.m
//  gloob
//
//  Created by zeroum on 27/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "FavoriteView.h"
#import "Episode.h"
#import "Program.h"
#import "UIColor+HexColor.h"
#import "ImageCache.h"
#import "UIButton+WebCache.h"

@interface FavoriteView ()

- (void)setupThumbnailButton;
- (void)setupLabels;
- (void)setupDeleteButton;

- (void)didTouchUpInside:(UIButton *)sender;

@end

@implementation FavoriteView

- (id)initWithFrame:(CGRect)frame favorite:(Episode *)favorite delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.favorite = favorite;
        self.delegate = delegate;
        
        [self setupThumbnailButton];
        [self setupLabels];
        [self setupDeleteButton];
    }
    
    return self;
}

#pragma mark - Private

- (void)setupThumbnailButton {
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(70.0f, 35.0f, 20.0f, 20.0f)];
    
    [spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [spinner startAnimating];
    
    [self addSubview:spinner];

    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(20.0f, 15.0f, 160.0f, 90.0f)];
    
    [btn setBackgroundColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
    
    [self addSubview:btn];
    
    [btn addTarget:self
            action:@selector(didTouchUpInside:)
  forControlEvents:UIControlEventTouchUpInside];
    
    if (self.favorite.image_cropped == nil) {
        
        [btn setImage:[UIImage imageNamed:@"placeholder_medium"]
             forState:UIControlStateNormal];
    } else {
        
        [btn sd_setImageWithURL:[NSURL URLWithString:self.favorite.image_cropped] forState:UIControlStateNormal];
    }
    
}

- (void)setupLabels {
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 107.0f, 170.0f, 20.0f)];
    
    [lblTitle setText:self.favorite.program.title];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:15.0f]];
    [lblTitle setTextColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
    
    [self addSubview:lblTitle];
    
    UILabel *lblEpisode = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 127.0f, 170.0f, 20.0f)];
    
    [lblEpisode setText:self.favorite.title];
    [lblEpisode setTextAlignment:NSTextAlignmentCenter];
    [lblEpisode setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:17.0f]];
    [lblEpisode setTextColor:[UIColor colorWithHexString:@"0x4A4A4A"]];
    
    [self addSubview:lblEpisode];
    
    UILabel *lblInfo = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 147.0f, 170.0f, 20.0f)];
    
    [lblInfo setText:[NSString stringWithFormat:@"TEMP.%li - EP.%li", (long)self.favorite.season, (long)self.favorite.number]];
    [lblInfo setTextAlignment:NSTextAlignmentCenter];
    [lblInfo setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:15.0f]];
    [lblInfo setTextColor:[UIColor colorWithHexString:@"0x7A7A7A"]];
    
    [self addSubview:lblInfo];
}

- (void)setupDeleteButton {

    UIButton *btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(167.0f, 8.0f, 20.0f, 21.0f)];
    
    [btnDelete setTag:1];
    
    [btnDelete setImage:[UIImage imageNamed:@"bt_favdelete"]
               forState:UIControlStateNormal];
    
    [btnDelete setImage:[UIImage imageNamed:@"bt_favdelete"]
               forState:UIControlStateHighlighted];
    
    [btnDelete addTarget:self
                  action:@selector(didTouchUpInside:)
        forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:btnDelete];
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    if (sender.tag == 1) {
        
        if ([self.delegate respondsToSelector:@selector(deleteFavorite:)]) {
            [self.delegate deleteFavorite:self.favorite];
        }
        
    } else {
        
        if ([self.delegate respondsToSelector:@selector(playEpisode:fromList:index:openingFrom:)]) {
            [self.delegate playEpisode:self.favorite
                              fromList:nil
                                 index:self.videoIndex
                           openingFrom:@"MEUS FAVORITOS"];
        }
    }
}

@end
