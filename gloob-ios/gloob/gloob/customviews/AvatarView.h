//
//  AvatarView.h
//  gloob
//
//  Created by zeroum on 24/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Profile;
@class Avatar;

@interface AvatarView : UIView

@property (nonatomic, strong) Avatar *avatar;

- (id)initHorizontalScrollingAvatarWithFrame:(CGRect)frame avatar:(Avatar *)avatar delegate:(id)delegate;
- (id)initVerticalScrollingAvatarWithFrame:(CGRect)frame avatar:(Avatar *)avatar delegate:(id)delegate;
- (id)initDeletableAvatarWithFrame:(CGRect)frame profile:(Profile *)profile avatar:(Avatar *)avatar delegate:(id)delegate;
- (void)setTitleForAvatarLabel:(NSString *)title;
- (void)setSelected:(BOOL)selected;

@end

@protocol AvatarViewDelegate <NSObject>

@optional

- (void)didSelectedAvatar:(Avatar *)avatar;
- (void)didSelectProfile:(Profile *)profile;
- (void)deleteProfile:(Profile *)profile;


@end
