//
//  LoadingView.h
//  gloob
//
//  Created by zeroum on 02/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (nonatomic, weak) NSString *LabelText;

typedef void(^completion)(BOOL finish);

- (void)showLoadingViewWithCompletion:(completion)completed;
- (void)hideLoadingViewWithCompletion:(completion)completed;

@end
