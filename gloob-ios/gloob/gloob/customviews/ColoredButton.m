//
//  ColoredButton.m
//  gloob
//
//  Created by zeroum on 22/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ColoredButton.h"
#import "UIColor+HexColor.h"

@implementation ColoredButton

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    UIEdgeInsets insets;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        insets = UIEdgeInsetsMake(14.0f, 14.0f, 14.0f, 14.0f);
    } else {
        insets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    }
    
    NSString *normal, *highlighted, *selected, *normalColor, *highlightedColor, *selectedColor;
    
    if ([_customStyle isEqualToString:@"green"]) {
        
        normal = @"vd_bt_verde";
        highlighted = @"vd_bt_verde_tap";
        selected = @"vd_bt_verde_tap";
        
        normalColor = @"0x7765AF";
        highlightedColor = @"0x7765AF";
        selectedColor = @"0x7765AF";
        
        [self.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];

    } else if ([_customStyle isEqualToString:@"yellow"]) {
        
        normal = @"pf_bt_amarelo";
        highlighted = @"pf_bt_amarelo_tap";
        selected = @"pf_bt_amarelo_tap";
        
        normalColor = @"0x7765AF";
        highlightedColor = @"0x7765AF";
        selectedColor = @"0x7765AF";
        
        [self.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
    } else if ([_customStyle isEqualToString:@"pink"]) {
        
        normal = @"bs_bt_rosa";
        highlighted = @"bs_bt_rosa_tap";
        selected = @"bs_bt_rosa_tap";
        
        normalColor = @"0xFFCD2C";
        highlightedColor = @"0xFFCD2C";
        selectedColor = @"0xFFCD2C";
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        } else {
            [self.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:30.0f]];
        }
        
    } else if ([_customStyle isEqualToString:@"orange"]) {
        
        normal = @"pf_bt_laranja";
        highlighted = @"pf_bt_laranja_tap";
        selected = @"pf_bt_laranja_tap";
        
        normalColor = @"0xFFFFFF";
        highlightedColor = @"0xFFFFFF";
        selectedColor = @"0xFFFFFF";
        
        [self.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
    } else if ([_customStyle isEqualToString:@"yellow_selectable"]) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            insets = UIEdgeInsetsMake(7.0f, 7.0f, 7.0f, 7.0f);
        }
        
        normal = @"pf_bt_nav";
        highlighted = @"pf_bt_nav_tap";
        selected = @"pf_bt_nav_tap";
        
        normalColor = @"0x7765AF";
        highlightedColor = @"0x7765AF";
        selectedColor = @"0x4A4A4A";
        
        [self.titleLabel setFont:[UIFont fontWithName:GLOOB_FONT_BLACK size:16.0f]];
        
    }
    
    [self setBackgroundImage:[[UIImage imageNamed:normal]resizableImageWithCapInsets:insets]
                    forState:UIControlStateNormal];
    
    [self setBackgroundImage:[[UIImage imageNamed:highlighted]resizableImageWithCapInsets:insets]
                    forState:UIControlStateHighlighted];
    
    [self setBackgroundImage:[[UIImage imageNamed:selected]resizableImageWithCapInsets:insets]
                    forState:UIControlStateSelected];
    
    [self setTitleColor:[UIColor colorWithHexString:normalColor]
               forState:UIControlStateNormal];
    
    [self setTitleColor:[UIColor colorWithHexString:highlightedColor]
               forState:UIControlStateHighlighted];
    
    [self setTitleColor:[UIColor colorWithHexString:selectedColor]
               forState:UIControlStateSelected];

}

@end
