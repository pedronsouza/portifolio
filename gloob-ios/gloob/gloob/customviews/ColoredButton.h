//
//  ColoredButton.h
//  gloob
//
//  Created by zeroum on 22/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColoredButton : UIButton

@property (nonatomic, weak) NSString *customStyle;

@end
