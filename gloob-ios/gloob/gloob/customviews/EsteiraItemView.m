//
//  EsteiraItemView.m
//  bezier
//
//  Created by ZeroUm on 07/04/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "EsteiraItemView.h"
#import <GLKit/GLKit.h>
#import <QuartzCore/QuartzCore.h>
#include <stdlib.h>
#import "FeaturedHome.h"
#import "UIImageView+WebCache.h"
#import "Program.h"

@interface EsteiraItemView ()

@property (nonatomic) double offset;

@property (nonatomic) double duration;

@end

@implementation EsteiraItemView

- (id)initWithFrame:(CGRect)frame program:(FeaturedHome *)program delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.program = program;
        self.delegate = delegate;
        
        [self setUserInteractionEnabled:NO];
        
        self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        if (self.program.alternative_poster_image) {
            
            [self.icon sd_setImageWithURL:[NSURL URLWithString:self.program.alternative_poster_image]];

        }
        
        [self addSubview:self.icon];
        
        self.duration = 0.2;
        
        [self resume];
    }
    
    return self;
}

- (void)animate {
    
    int variation_y = 0;
    
    if (self.degrees < 360) {
        self.degrees += 30;
    } else {
        self.degrees = 0;
    }
    
    // sobe a imagem
    if (self.frame.origin.x > -45 && self.frame.origin.x < 45) {
        
        variation_y -= 10;
    }
    
    // desce a imagem
    if (self.frame.origin.x > 795) {
        variation_y += 10;

        if (self.frame.origin.x < [UIHelper screenSize].width) {
            
        } else {
            // Volta a imagem pro começo
            variation_y = 0;
            self.frame = CGRectMake(self.inital_x, self.superview.frame.size.height - 10, self.frame.size.width, self.frame.size.height);

        }

    }
    
    // animacao
    self.offset = 4 * sin(GLKMathDegreesToRadians(self.degrees));
    self.offset += variation_y;
    
    [UIView animateKeyframesWithDuration:self.duration
                                   delay:0.0
                                 options:UIViewKeyframeAnimationOptionCalculationModeCubicPaced + UIViewAnimationOptionCurveLinear
                              animations:^{
                                  
                                  self.center = CGPointMake(self.center.x+5, self.center.y+self.offset);
                                  
                              } completion:nil];
}

- (void)pause {
    
    [_timer invalidate];
    _timer = nil;
}

- (void)resume {
    
    self.timer = [NSTimer timerWithTimeInterval:self.duration
                                         target:self
                                       selector:@selector(animate)
                                       userInfo:nil
                                        repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    
    [runner addTimer:self.timer forMode: NSDefaultRunLoopMode];
}

- (void)openSelectedCharacter {
    
    if ([self.delegate respondsToSelector:@selector(didSelectFeaturedHomeItem:)]) {
        
        [self.delegate didSelectFeaturedHomeItem:self.program];
        
    }
}

@end
