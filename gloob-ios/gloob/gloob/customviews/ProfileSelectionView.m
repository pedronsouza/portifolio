//
//  AvatarSelectionView.m
//  gloob
//
//  Created by Pedro on 4/30/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ProfileSelectionView.h"


@interface ProfileSelectionView ()

@property (nonatomic, strong) Avatar *avatar;
@property (nonatomic, strong) Profile *profile;
@property (nonatomic, assign) CGRect avatarDefaultFrame;
@property (nonatomic, assign) CGRect selectionDefaulFrame;

- (IBAction)didClickAvatar:(id)sender;
@end

@implementation ProfileSelectionView

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideSelection:)
                                                 name:kAvatarHasBeenSelected object:nil];
    [self setupInterface];
}

- (instancetype)initWithProfile:(Profile *)profile andAvatar:(Avatar *)avatar {
    self = [super initWithNibName:[UIHelper deviceViewName:@"ProfileSelectionView"] bundle:nil];
    if (self) {
        self.avatar = avatar;
        self.profile = profile;
    }
    
    return self;
}

- (void) setupInterface {
    [self.imgAvatar setImage:[self.avatar iconForProfileCreation]];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self.imgAvatar action:@selector(didClickAvatar:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    
    [self.imgAvatar setUserInteractionEnabled:YES];
    [self.imgAvatar addGestureRecognizer:tap];
    [self.lblProfileName setText:self.profile.name];
    
    self.avatarDefaultFrame = self.imgAvatar.frame;
    self.selectionDefaulFrame = self.imgSelection.frame;
}

- (IBAction)didClickAvatar:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kAvatarHasBeenSelected object:nil];
    [self.delegate didSelectProfile:self.profile];
    
    
    UIImage *icon = [self.avatar iconForProfileCreationSelected];
    CGRect imgAvatarFrame = self.imgAvatar.frame;
    
//    imgAvatarFrame.size.width = icon.size.width;
//    imgAvatarFrame.size.height = icon.size.height;
//    imgAvatarFrame.origin.x = (self.avatarDefaultFrame.origin.x - ((icon.size.width - self.avatarDefaultFrame.size.width)/2));
//    imgAvatarFrame.origin.y = (self.avatarDefaultFrame.origin.y - ((icon.size.height - self.avatarDefaultFrame.size.height) / 2));
    
    [self.imgAvatar setFrame:imgAvatarFrame];
    [self.imgAvatar setImage:icon];
    [self.imgSelection setHidden:NO];
}


- (void) hideSelection:(NSNotification *)notification {
    [self.imgSelection setHidden:YES];
    UIImage *icon = [self.avatar iconForProfileCreation];
    CGRect imgAvatarFrame = self.imgAvatar.frame;
    CGRect imgSelectionFrame = self.imgSelection.frame;
    
//    imgAvatarFrame.size.width = icon.size.width;
//    imgAvatarFrame.size.height = icon.size.height;
//    imgAvatarFrame.origin.x = self.avatarDefaultFrame.origin.x;
//    imgAvatarFrame.origin.y = self.avatarDefaultFrame.origin.y;
//    imgSelectionFrame.origin.y = self.selectionDefaulFrame.origin.y - 18;
    
    [self.imgAvatar setFrame:imgAvatarFrame];
    [self.imgSelection setFrame:imgSelectionFrame];
    [self.imgAvatar setImage:icon];
    
}

@end