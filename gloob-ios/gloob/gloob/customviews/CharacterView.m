//
//  CharacterView.m
//  gloob
//
//  Created by zeroum on 14/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "CharacterView.h"
#import "Program.h"
#import "UIColor+HexColor.h"
#import "UIButton+WebCache.h"

@interface CharacterView ()

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *title;

@property (nonatomic, weak) Program *program;

- (void)setupCharacterButton;
- (void)didTouchUpInside:(UIButton *)sender;
- (void)setupTitle;

@end

@implementation CharacterView

- (id)initWithFrame:(CGRect)frame program:(Program *)program delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.program = program;
        self.delegate = delegate;
        
        [self setupCharacterButton];
        [self setupTitle];

    }
    
    return self;
}

#pragma mark - Private

- (void)setupCharacterButton {
    
    CGRect frame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        frame = CGRectMake(0.0f, 0.0f, 140.0f, 140.0f);
    } else{
        frame = CGRectMake(0.0f, 0.0f, 77.0f, 77.0f);
    }
    
    self.button = [[UIButton alloc] initWithFrame:frame];
    
    [self.button sd_setImageWithURL:[NSURL URLWithString:self.program.alternative_poster_image] forState:UIControlStateNormal];
    
    [self.button addTarget:self
                    action:@selector(didTouchUpInside:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.button];
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(displayCharacterInPopUp:)]) {
        [self.delegate displayCharacterInPopUp:self.program];
    }
}

- (void)setupTitle {
    
    CGRect frame;
    CGFloat fontSize;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        frame = CGRectMake(0.0f, 136.0f, 140.0f, 44.0f);
        fontSize = 16.0f;
        
    } else {
        
        frame = CGRectMake(0.0f, 77.0f, 77.0f, 30.0f);
        fontSize = 14.0f;
        
    }
    
    self.title = [[UILabel alloc] initWithFrame:frame];
    
    [self.title setBackgroundColor:[UIColor clearColor]];
    [self.title setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:fontSize]];
    [self.title setTextColor:[UIColor colorWithHexString:@"0x3E3E3E"]];
    [self.title setTextAlignment:NSTextAlignmentCenter];
    [self.title setNumberOfLines:2];

    [self.title setText:self.program.title];
    
    [self addSubview:self.title];
}

@end
