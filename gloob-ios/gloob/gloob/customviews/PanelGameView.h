//
//  PanelGameView.h
//  gloob
//
//  Created by zeroum on 12/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Game;

@interface PanelGameView : UIView

@property (nonatomic, assign) id delegate;

- (id)initWithFrame:(CGRect)frame game:(Game *)game delegate:(id)delegate;

@end

@protocol PanelGameViewDelegate <NSObject>

- (void)playGame:(Game *)game;

@end
