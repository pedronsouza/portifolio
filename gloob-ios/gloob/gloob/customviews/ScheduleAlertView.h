//
//  ScheduleAlertView.h
//  gloob
//
//  Created by zeroum on 05/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Program;
@class ScheduleAlert;

@interface ScheduleAlertView : UIView

- (id)initWithFrame:(CGRect)frame schedule:(ScheduleAlert *)schedule delegate:(id)delegate;

@end

@protocol ScheduleAlertViewDelegate <NSObject>

- (void)removeScheduleFromList:(ScheduleAlert *)schedule;

@end
