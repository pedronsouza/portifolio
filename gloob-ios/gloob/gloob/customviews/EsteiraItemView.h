//
//  EsteiraItemView.h
//  bezier
//
//  Created by ZeroUm on 07/04/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FeaturedHome;

@interface EsteiraItemView : UIView

@property (nonatomic, strong) UIImageView *icon;

@property (nonatomic, strong) FeaturedHome *program;

@property (nonatomic, assign) id delegate;

@property (nonatomic, assign) NSInteger viewID;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) int degrees;
@property (nonatomic) int inital_x;

- (id)initWithFrame:(CGRect)frame program:(FeaturedHome *)program delegate:(id)delegate;

- (void)pause;
- (void)resume;
- (void)openSelectedCharacter;

@end

@protocol EsteiraItemViewDelegate <NSObject>

- (void)didSelectFeaturedHomeItem:(FeaturedHome *)program;

@end