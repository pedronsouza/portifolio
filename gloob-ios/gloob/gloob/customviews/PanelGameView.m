//
//  PanelGameView.m
//  gloob
//
//  Created by zeroum on 12/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "PanelGameView.h"
#import "Game.h"
#import "UIButton+WebCache.h"

@interface PanelGameView ()

@property (nonatomic, weak) Game *game;

- (void)setupButtonWithFrame:(CGRect)frame;
- (void)didTouchUpInside:(UIButton *)sender;
- (void)setupGameTitle;

@end

@implementation PanelGameView

- (id)initWithFrame:(CGRect)frame game:(Game *)game delegate:(id)delegate {
    
    if (self = [super initWithFrame:frame]) {
        
        self.game = game;
        self.delegate = delegate;
        
        [self setupButtonWithFrame:frame];
        [self setupGameTitle];
    }
    
    return self;
}

#pragma mark - Private

- (void)setupButtonWithFrame:(CGRect)frame {
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
    
    UIEdgeInsets inset;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        inset = UIEdgeInsetsMake(5.0f, 5.0f, 92.0f, 5.0f);
    } else {
        inset = UIEdgeInsetsMake(5.0f, 5.0f, 72.0f, 5.0f);
    }
    
    [button setImageEdgeInsets:inset];
    
    [button setBackgroundImage:[UIImage imageNamed:@"hm_jogo_quadro"]
                      forState:UIControlStateNormal];
    
//    [button setBackgroundImage:[UIImage imageNamed:@"hm_jogo_quadro_tap"]
//                      forState:UIControlStateHighlighted];
    
    if (self.game.image == nil) {
        [button setImage:[UIImage imageNamed:@"placeholder_small"]
                forState:UIControlStateNormal];
    } else {
        
        [button sd_setImageWithURL:[NSURL URLWithString:self.game.image] forState:UIControlStateNormal];
    }
    
    [button addTarget:self
               action:@selector(didTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:button];
}

- (void)didTouchUpInside:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(playGame:)]) {
        [self.delegate playGame:self.game];
    }
}

- (void)setupGameTitle {
    
    CGRect frame;
    CGFloat fontSize;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        frame = CGRectMake(4.0f, 97.0f, 150.0f, 45.0f);
        fontSize = 20.0f;
    } else {
        frame = CGRectMake(5.0f, 71.0f, 110.0f, 35.0f);
        fontSize = 15.0f;
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont fontWithName:GLOOB_FONT_BOLD size:fontSize]];
    [label setText:self.game.name];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:2];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    [label setAdjustsFontSizeToFitWidth:YES];
    [label setMinimumScaleFactor:0.5f];
    
    [self addSubview:label];
}

@end
