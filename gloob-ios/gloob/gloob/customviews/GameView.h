//
//  GameView.h
//  gloob
//
//  Created by zeroum on 03/02/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Game;

@interface GameView : UIView

@property (nonatomic, strong) Game *game;

@property (nonatomic, assign) id delegate;

- (id)initWithFrame:(CGRect)frame game:(Game *)game delegate:(id)delegate;

@end

@protocol GameViewDelegate <NSObject>

- (void)openGame:(Game *)game;

@end
