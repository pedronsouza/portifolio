//
//  UIDeviceHardware.m
//
//  usage :
//      NSString *platform = [UIDeviceHardware platform];
//

#import "UIDeviceHardware.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@implementation UIDeviceHardware

+ (NSString *) platform{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

+ (NSString *) platformString{
    NSString *platform = [self platform];
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone";
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod";
    
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad";
    
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad";
    
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad";
    
    
    if ([platform isEqualToString:@"i386"])         return [UIDevice currentDevice].model;
    if ([platform isEqualToString:@"x86_64"])       return [UIDevice currentDevice].model;
    
    return platform;
}

@end
