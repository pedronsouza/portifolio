/*************************************************************************
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by all applicable intellectual property
 * laws, including trade secret and copyright laws.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

#import <Foundation/Foundation.h>

/**
 * AUDBanner is the base class for retreiving all metadata on an Banner asset
 */
@interface AUDBanner : NSObject

/**
 * @return NSString, the source for the banner ( can contain a url to static or Iframe, or HMTL code )
 */
@property (nonatomic, strong) NSString *source;

/**
 * @return NSString, the AdId assosiated with the banner
 */
@property (nonatomic, strong) NSString *adId;

/**
 * @return int, display width of the banner
 */
@property (nonatomic) int width;

/**
 * @return int, display height for the banner.
 */
@property (nonatomic) int height;

/**
 * @return NSString, resource type of the banner: : static, iframe, text/html.
 */
@property (nonatomic, strong) NSString *resourceType;

/**
 * @return NSString, original creative type of the banner: static, iframe, html.
 */
@property (nonatomic, strong) NSString *creativeType;


@end
