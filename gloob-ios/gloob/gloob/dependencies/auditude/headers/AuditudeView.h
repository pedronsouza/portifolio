/*************************************************************************
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2010 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by all applicable intellectual property
 * laws, including trade secret and copyright laws.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/


/**
 @mainpage Auditude Advertising API
 
 The Auditude Advertising API is a Cocoa/Objective-C Library for rendering video and companion banners
 ads on iOS 4.0 or above.
*/


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "AuditudeViewDelegateProtocol.h"

extern NSString *PlayheadTimeInSeconds;
extern NSString *TotalTimeInSeconds;
extern NSString *viewSourceKey;
extern NSString *urlKey;
extern NSString *audienceManagerURL;
extern NSString *creativeRepackagingFormat;
extern NSString *adRequestDomain;

@interface AuditudeView : UIView
{
	id<AuditudeViewDelegate> __weak _delegate;
    BOOL _auditudeHandlesClick;
    BOOL _prefetchNetworkAds;
}

/**
 @brief The enumeration of various video playback status notifications
 */
typedef enum
{
	AuditudeNotificationTypeVideoPlaybackStarted = 0,
	AuditudeNotificationTypeVideoPlaybackPaused,
	AuditudeNotificationTypeVideoPlaybackResumed,
	AuditudeNotificationTypeVideoPlaybackComplete,
	AuditudeNotificationTypeVideoPlayheadUpdate,
	AuditudeNotificationTypeVideoSeek,
    AuditudeNotificationTypeAdClick,
    AuditudeNotificationTypeOpenBrowser
} AuditudeNotificationType;

/**
 @brief The AuditudeViewDelegate instance required by the library to notify ad playback messages.
 */
@property (nonatomic, weak) id<AuditudeViewDelegate> delegate;


/**
 Defines whether Auditude Plugin will handle the Video Ad Click and open the internal Browser.
 Default value is NO.
*/
@property (nonatomic) BOOL auditudeHandlesClick;

/**
 Defines whether Auditude Plugin should prefetch all Network ads at the begin of the break.
 */
@property (nonatomic) BOOL prefetchNetworkAds;

/**
 Defines the default video ids to be passed to server ad call.
 */
@property (nonatomic, strong) NSArray *defaultVideoIds;


#pragma mark -
#pragma mark AuditudeView Class Functions

/**
 Returns the current version of the Auditude Ad SDK
 */
+ (NSString *)auditudeSDKVersion;

/**
 Toggles debug logging from the Auditude SDK on or off.
 Default is YES.
 */
+ (void)enableDebugLog:(BOOL)enable;


#pragma mark -
#pragma mark Ad Request Functions

/**
 @brief Request ads for a video
 
 Request ads for a video with an id, domain and publisher zone id.
 */
- (void)requestAdsForVideo:(NSString *)videoId
					zoneId:(NSInteger)publisherZoneId
					domain:(NSString *)auditudeDomain;

/**
 @brief Request ads for a video
 
 Request ads for a video with an id, domain, publisher zone id and key-value targeting parameters
 */
- (void)requestAdsForVideo:(NSString *)videoId
                    zoneId:(NSInteger)zoneId
                    domain:(NSString *)domain
           targetingParams:(NSDictionary *)targetingParams;

/**
 @brief Request ads for a video with custom params (key-values)

  Request ads for a video with an id, domain, publisher zone id and key-value pairs and pass-through parameters.
 */
- (void)requestAdsForVideo:(NSString *)videoId
					zoneId:(NSInteger)publisherZoneId
					domain:(NSString *)auditudeDomain
		   targetingParams:(NSDictionary *)targetingParams
          passThroughParams:(NSDictionary *)passThroughParams;

/**
 set the contentDuration - needed for VMAP and VAST %
*/
- (void)setContentDuration:(int)durationInSecond;

#pragma mark -
#pragma mark Ad Break Control Functions

/**
 @brief Notify the library to start playing ads from the next break
 
 This method should be called only after the auditudeInitComplete delegate function is invoked.
 Applications wishing to show a pre-roll ad should call this before the main video starts playing.
 */
- (void)beginBreak;

/**
 @brief Notify the library to stop ad playback
 
 The library is informed to stopped all linear ad playback and end the break.
 This forces the library to call auditudeBreakEnd if it is currently handling a break.
 */
- (void)endBreak;

/**
 Poll the library of the current break play status. This will return YES if the library
 is currently in a break and NO otherwise.
 */
- (BOOL)isHandlingBreak;

/**
 * Sets the specified value for the key.
 *
 * @param value The value for the key. Any existing value for the key is replaced by the new value
 * @param key The name of the key to be set.
 */
- (void)setValue:(id)value forKey:(NSString *)key;

/**
 * Returns the value of the specified key.
 *
 * The key must be a non empty value.
 *
 * @param key The name of the key whose value  is to be returned.
 * @return The value for the specified key or nil if there is no corresponding value for the key
 */
- (id)valueForKey:(NSString *)key;


#pragma mark -
#pragma mark Content Playback Notifications

/**
 @brief Notify the library about current video playback status
 
 This method should be called to inform the library about current video playback status.
 It should be called when the main video started and completed playback. It should also
 be called when the main video is paused or resumed.
 */
- (void)notify:(AuditudeNotificationType)type notificationParams:(NSDictionary *)params;


@end
