/*************************************************************************
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by all applicable intellectual property
 * laws, including trade secret and copyright laws.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/


#import <Foundation/Foundation.h>
@class AUDAsset;

/**
 * AUDExternalAsset is the base class for retreiving all metadata on an auditude asset
 */
@interface AUDExternalAsset : NSObject
{
    AUDAsset *_audAsset;
}

/**
 * @return NSString, the Asset id for the asset
 */
@property (readonly,nonatomic, strong) NSString *assetId;

/**
 * @return NSString, the AdId for the Asset
 */
@property (readonly,nonatomic, strong) NSString *adId;

/**
 @return NSString, the format of this asset
 */
@property (readonly,nonatomic, strong) NSString *format;

/**
 @return NSString, the source (url) for the asset 
 */
@property (readonly,nonatomic, strong) NSString *source;

/**
 @return NSString, the resourceType for the asset 
 */
@property (readonly,nonatomic, strong) NSString *resourceType;

/**
 @return NSString, reative Type for the asset 
 */
@property (readonly,nonatomic, strong) NSString *creativeType;

/**
 @return NSString, the Click Through ULR ( this is the url that shodl be open when the asset is clicked ) 
 */
@property (readonly, nonatomic, strong) NSString *clickThroughURL;

/**
 @return NSString, the click title for the asset 
 */
@property (readonly, nonatomic, strong) NSString *clickTitle;

/**
 @return int, skip offset in sec for the asset ( ad should not be skipable until it reache the skipoffet time. 
 */
@property (readonly, nonatomic) int skipoffset;

/**
 @return int, the duration of the ad in seconds 
 */
@property (readonly, nonatomic) int duration;

/**
 @return int, the width of the creative.
 */
@property (readonly, nonatomic) int width;

/**
 @return int, the height of the creative.
 */
@property (readonly, nonatomic) int heigth;

/**
 @return BOOL. Whether the ad can be skip or not. 
 */
@property (readonly, nonatomic) BOOL isSkippable;

- (id)initWithAsset:(AUDAsset*)asset;

@end
