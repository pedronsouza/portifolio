/*************************************************************************
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by all applicable intellectual property
 * laws, including trade secret and copyright laws.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

#import <Foundation/Foundation.h>
#import "AUDExternalAsset.h"

@class AUDViewSource;

/** AUDExternalViewSource
 * The AUDExternalViewSource get pass to the auditudeLinearAdBegin event
 * and can be retrived out of the data Dictionary : [data valueForKey:@"viewSource"];
 * the AUDExternalViewSource is pass as a parameter in the AuditudeNotificationTypeOpenBrowser and/or AuditudeNotificationTypeAdClick
 * 'NSMutableDictionary *params = [[[NSMutableDictionary alloc] init] autorelease];
 * [params setValue:_viewSource forKey:viewSourceKey];	
 * [_auditudeView notify:AuditudeNotificationTypeAdClick notificationParams:params]; '
 */
@interface AUDExternalViewSource : NSObject
{
    AUDViewSource *_viewSource;
    AUDExternalAsset *_primaryAsset;
}

/**
* @return AUDExternalAsset. the current primary Asset being display.
*/
@property (readonly,nonatomic, strong) AUDExternalAsset *primaryAsset;

/**
* @return AUDViewSource.Used internaly by the Auditude plugin during notification.
*/
@property (readonly,nonatomic, strong) AUDViewSource *viewSource;

- (id) initWithViewSource:(AUDViewSource *) viewSource;

@end
