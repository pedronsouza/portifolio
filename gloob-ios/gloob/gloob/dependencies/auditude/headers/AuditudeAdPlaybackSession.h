/*************************************************************************
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2011 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by all applicable intellectual property
 * laws, including trade secret and copyright laws.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

#import <Foundation/Foundation.h>

/**
 * AuditudeAdPlaybackSession provides an interface to control ad playback behavior.
 * Every ad begin notification will provide a handle to this session instance.
 * This instance can be used to play/pause/stop/skip an ad.
 */
@interface AuditudeAdPlaybackSession : NSObject
{

}

/** @name Managing Ad Playback */
/**
 * Notifies the Auditude SDK to pause the current ad
 */
- (void)notifyPause;

/** @name Managing Ad Playback */
/**
 * Notifies the Auditude SDK to play the current ad after a pause notification
 */
- (void)notifyPlay;

/** @name Managing Ad Playback */
/**
 * Notifies the Auditude SDK to stop the current ad. This will force the SDK to play the next ad within the break.
 */
- (void)notifyStop;

@end
