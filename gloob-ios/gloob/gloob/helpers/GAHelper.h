//
//  GAHelper.h
//  gloob
//
//  Created by zeroum on 06/04/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Slug.h"

@protocol GAEventDelegate
@required
- (NSString *) categoryForEvent;
- (NSString *) labelForEvent;
@end

@protocol GATrackableScreen
@required
- (NSString *) screenNameForScreenEvent;
@end

@interface GAHelper : NSObject
+ (instancetype) sharedInstance;
- (void)trackScreenEventWithTrackableScreenDelegate:(id<GATrackableScreen>)delegate;
- (void)trackEventWithTrackableDelegate:(id<GAEventDelegate>)delegate;
- (NSString *) normalizeStringForEvent:(NSString *)eventTitle;
- (void) trackEventWithCategory:(NSString *)category andLabel:(NSString *)label;
- (void)startNewSession;
@end
