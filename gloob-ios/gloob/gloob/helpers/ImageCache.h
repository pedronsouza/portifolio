//
//  ImageCache.h
//  gloob
//
//  Created by zeroum on 17/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Program;
@class Episode;
@class Game;
@class FeaturedHome;

@interface ImageCache : UIImage

typedef void(^completed)(BOOL success, UIImage *image);

+ (void)downloadAndResizeThumbFromProgram:(Program *)program withCompletionHandler:(completed)finish;
+ (void)downloadAndResizeThumbFromEpisode:(Episode *)episode withCompletionHandler:(completed)finish;
+ (void)downloadAndResizeThumbFromSearchEpisode:(Episode *)episode withCompletionHandler:(completed)finish;
+ (void)downloadAndResizeThumbFromGame:(Game *)game withCompletionHandler:(completed)finish;
+ (void)downloadAndResizeThumbFromFeaturedHome:(FeaturedHome *)featuredHome withCompletionHandler:(completed)finish;

+ (BOOL)isImageOptimized:(NSString *)filename;
+ (UIImage *)getOptimizedImageWithFilename:(NSString *)filename;

@end
