//
//  ImageCache.m
//  gloob
//
//  Created by zeroum on 17/03/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "ImageCache.h"
#import "Program.h"
#import "FeaturedHome.h"
#import "Episode.h"
#import "Game.h"

static NSMutableDictionary *cacheControl;

@interface ImageCache ()

+ (NSString *)getPathForSavedImages;

+ (NSString *)getOptimizedImagePathWithFilename:(NSString *)filename;

+ (void)downloadImageAtPath:(NSString *)path
                   resizeTo:(CGSize)size
                andRenameTo:(NSString *)newName
                      toPNG:(BOOL)isPNG withCompletionHandler:(completed)finish;

+ (UIImage *)resizeImage:(UIImage *)imageToResize
                  toSize:(CGSize)newSize;

+ (BOOL)saveImage:(UIImage *)imageToSave
     withFilename:(NSString *)filename
            toPNG:(BOOL)isPNG;

+ (CGSize)getSizeForDevice:(CGSize)size;

@end

@implementation ImageCache

+ (NSMutableDictionary *)cacheControl {
    
    if (cacheControl == nil) {
        cacheControl = [[NSMutableDictionary alloc] init];
    }
    
    return cacheControl;
}

+ (void)addToCacheControl:(NSString *)path {
    
    if ([ImageCache isDownloading:path] == NO) {
        [[ImageCache cacheControl] setObject:[NSNumber numberWithInt:1] forKey:path];
    }
}

+ (void)removeFromCacheControl:(NSString *)path {
    [[ImageCache cacheControl] removeObjectForKey:path];
}

+ (BOOL)isDownloading:(NSString *)path {
    if ([ImageCache cacheControl][path] != nil) {
        return YES;
    }
    
    return NO;
}

+ (void)downloadAndResizeThumbFromProgram:(Program *)program withCompletionHandler:(completed)finish {
    
    if (program == nil) {
        
        if (finish != nil) {
            finish(YES, nil);
        }
        
    } else {
        
        if ([ImageCache isImageOptimized:program.optimized_thumb] == YES) {
            
            if (finish != nil) {
                finish(YES, [ImageCache getOptimizedImageWithFilename:program.optimized_thumb]);
            }
            
        } else {
            
            [ImageCache downloadImageAtPath:program.alternative_poster_image
                             resizeTo:[ImageCache getSizeForDevice:CGSizeMake(160.0f, 160.0f)]
                          andRenameTo:program.optimized_thumb
                                toPNG:YES
                withCompletionHandler:^(BOOL success, UIImage *image) {
                    
                    if (success) {
                        finish(YES, image);
                    } else {
                        finish(NO, nil);
                    }
                }];
        }
    }
}

+ (void)downloadAndResizeThumbFromEpisode:(Episode *)episode withCompletionHandler:(completed)finish {
    
    if (episode == nil) {
        
        if (finish != nil) {
            finish(YES, nil);
        }
        
    } else {
        
        if ([ImageCache isImageOptimized:episode.optimized_thumb] == YES) {
            
            if (finish != nil) {
                finish(YES, [ImageCache getOptimizedImageWithFilename:episode.optimized_thumb]);
            }
            
        } else {
            CGSize size;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                size = CGSizeMake(160.0f, 90.0f);
            } else {
                size = CGSizeMake(120.0f, 67.0f);
            }
            
            [ImageCache downloadImageAtPath:episode.image_cropped
//                             resizeTo:[ImageCache getSizeForDevice:CGSizeMake(320.0f, 180.0f)]
                             resizeTo:[ImageCache getSizeForDevice:size]
                          andRenameTo:episode.optimized_thumb
                                toPNG:NO
                withCompletionHandler:^(BOOL success, UIImage *image) {
                    
                    if (success) {
                        finish(YES, image);
                    } else {
                        finish(NO, nil);
                    }
                }];
        }
    }
}

+ (void)downloadAndResizeThumbFromSearchEpisode:(Episode *)episode withCompletionHandler:(completed)finish {
    
    if (episode == nil) {
        
        if (finish != nil) {
            finish(YES, nil);
        }
        
    } else {
        
        if ([ImageCache isImageOptimized:episode.optimized_thumb] == YES) {
            
            if (finish != nil) {
                finish(YES, [ImageCache getOptimizedImageWithFilename:episode.optimized_thumb]);
            }
            
        } else {
            
            [ImageCache downloadImageAtPath:episode.thumb_image
                             resizeTo:[ImageCache getSizeForDevice:CGSizeMake(320.0f, 180.0f)]
                          andRenameTo:episode.optimized_thumb
                                toPNG:NO
                withCompletionHandler:^(BOOL success, UIImage *image) {
                    
                    if (success) {
                        finish(YES, image);
                    } else {
                        finish(NO, nil);
                    }
                }];
        }
    }
}

+ (void)downloadAndResizeThumbFromGame:(Game *)game withCompletionHandler:(completed)finish {
    
    if (game == nil) {
        
        if (finish != nil) {
            finish(YES, nil);
        }
        
    } else {
        
        if ([ImageCache isImageOptimized:game.optimized_thumb] == YES) {
            
            if (finish != nil) {
                finish(YES, [ImageCache getOptimizedImageWithFilename:game.optimized_thumb]);
            }
            
        } else {
            
            [ImageCache downloadImageAtPath:game.image
                             resizeTo:[ImageCache getSizeForDevice:CGSizeMake(320.0f, 180.0f)]
                          andRenameTo:game.optimized_thumb
                                toPNG:NO
                withCompletionHandler:^(BOOL success, UIImage *image) {
                    
                    if (success) {
                        finish(YES, image);
                    } else {
                        finish(NO, nil);
                    }
                }];
        }
    }
}

+ (void)downloadAndResizeThumbFromFeaturedHome:(FeaturedHome *)featuredHome withCompletionHandler:(completed)finish {
    
    if (featuredHome == nil) {
        
        if (finish != nil) {
            finish(YES, nil);
        }
        
    } else {
        
        if ([ImageCache isImageOptimized:featuredHome.optimized_thumb] == YES) {
            
            if (finish != nil) {
                finish(YES, [ImageCache getOptimizedImageWithFilename:featuredHome.optimized_thumb]);
            }
            
        } else {
            
            [ImageCache downloadImageAtPath:featuredHome.alternative_poster_image
                             resizeTo:[ImageCache getSizeForDevice:CGSizeMake(160.0f, 160.0f)]
                          andRenameTo:featuredHome.optimized_thumb
                                toPNG:YES
                withCompletionHandler:^(BOOL success, UIImage *image) {
                    
                    if (success) {
                        finish(YES, image);
                    } else {
                        finish(NO, nil);
                    }
                }];
        }
    }
}

+ (void)downloadImageAtPath:(NSString *)path resizeTo:(CGSize)size andRenameTo:(NSString *)newName toPNG:(BOOL)isPNG withCompletionHandler:(completed)finish {
    
    if (path == nil || path.length == 0) {
        
        if (finish != nil) {
            finish(NO, nil);
        }
        
    } else {
        
        [ImageCache addToCacheControl:path];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSError *error = nil;
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]
                                                      options:0
                                                        error:&error];            

            UIImage *downloadedImage = [UIImage imageWithData:imageData];
            
            if ([ImageCache saveImage:[ImageCache resizeImage:downloadedImage toSize:size] withFilename:newName toPNG:isPNG] == YES) {
                
                UIImage *optimizedImage = [ImageCache getOptimizedImageWithFilename:newName];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    [ImageCache removeFromCacheControl:path];
                    
                    if (finish != nil) {
                        finish(YES, optimizedImage);
                    }
                    
                });
                
            } else {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    [ImageCache removeFromCacheControl:path];
                    
                    if (finish != nil) {
                        finish(NO, nil);
                    }
                });
            }
            
        });
    }
}

+ (BOOL)isImageOptimized:(NSString *)filename {
    
    NSString *path = [[ImageCache getPathForSavedImages] stringByAppendingPathComponent:filename];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Private

+ (NSString *)getPathForSavedImages {
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (UIImage *)getOptimizedImageWithFilename:(NSString *)filename {
    return [UIImage imageWithContentsOfFile:[ImageCache getOptimizedImagePathWithFilename:filename]];
}

+ (NSString *)getOptimizedImagePathWithFilename:(NSString *)filename {
    return [[ImageCache getPathForSavedImages] stringByAppendingPathComponent:filename];
}

+ (UIImage *)resizeImage:(UIImage *)imageToResize toSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContext(newSize);
    
    [imageToResize drawInRect:CGRectMake(0.0f, 0.0f, newSize.width, newSize.height)];
    
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resizedImage;
}

+ (BOOL)saveImage:(UIImage *)imageToSave withFilename:(NSString *)filename toPNG:(BOOL)isPNG {
    NSData *binaryImageData = nil;
    
    if (isPNG) {
        binaryImageData = UIImagePNGRepresentation(imageToSave);
    } else {
        binaryImageData = UIImageJPEGRepresentation(imageToSave, 0.85);
    }

    if ([binaryImageData writeToFile:[[ImageCache getPathForSavedImages] stringByAppendingPathComponent:filename] atomically:YES]) {
        return YES;
    } else {
        return NO;
    }
}

+ (CGSize)getSizeForDevice:(CGSize)size {
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] >= 2.0) {
        // Retina
        return CGSizeMake(size.width * 2, size.height * 2);
    } else {
        // Not Retina
        return CGSizeMake(size.width, size.height);
    }
}

@end
