//
//  GAHelper.m
//  gloob
//
//  Created by zeroum on 06/04/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "GAHelper.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LoginManager.h"
#import "UIDeviceHardware.h"

@interface GAHelper ()
- (void) defineCustomDimensionsForSession;
- (NSString *) parseKeyWithFormatKey: (NSString *)key andParams:(NSArray *)params;
@end

@implementation GAHelper
static GAHelper * _sharedInstance;
- (instancetype)init {
    self = [super init];
    
    if (self) {
#if DEBUG
        [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANALYTICS_ID_DEBUG];
        [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
#else
        [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANALYTICS_ID];
#endif
        [self defineCustomDimensionsForSession];
    }
    
    return self;
}

#pragma mark Public Methods
+ (instancetype) sharedInstance {
    @synchronized([GAHelper class]) {
        if (!_sharedInstance) {
            _sharedInstance = [[self alloc] init];
        }
        
        return _sharedInstance;
    }
    
    return nil;
}

+ (void)logEventWitParameters:(NSDictionary *)parameters {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    if ([parameters objectForKey:kGAIEventAction] == nil) {
        [parameters setValue:@"Gloob Play" forKey:kGAIEventAction];
    }
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[parameters objectForKey:kGAIEventCategory]
                                                          action:[parameters objectForKey:kGAIEventAction]
                                                           label:[parameters objectForKey:kGAIEventLabel]
                                                           value:[parameters objectForKey:kGAIEventValue]]build]];
}


- (void)trackScreenEventWithTrackableScreenDelegate:(id<GATrackableScreen>)delegate {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[delegate screenNameForScreenEvent]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)trackEventWithTrackableDelegate:(id<GAEventDelegate>)delegate {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSString *category = [delegate categoryForEvent];
    NSString *action = @"Gloob Play";
    NSString *label = [delegate labelForEvent];
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:category
                                                                         action:action
                                                                          label:label
                                                                          value:0] build];
    
    [tracker send:event];
}

- (NSString *)normalizeStringForEvent:(NSString *)eventTitle {
    NSString *normalizedString = [eventTitle stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    return [normalizedString lowercaseString];
}

- (void) trackEventWithCategory:(NSString *)category andLabel:(NSString *)label {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSString *action = @"Gloob Play";
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:category
                                                                         action:action
                                                                          label:label
                                                                          value:0] build];
    
    [tracker send:event];
    NSLog(@"Tracking event with Category: %@ - Label: %@", category, label);
}

- (void)startNewSession {
    [self defineCustomDimensionsForSession];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
    [builder set:@"start" forKey:kGAISessionControl];
    [tracker send:[builder build]];
}

#pragma mark Private Methods

- (void)defineCustomDimensionsForSession {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    Login *login = [LoginManager getSavedLogin];

#if DEBUG
    NSLog(@"%@", [NSString stringWithFormat:@"%@_%@", login.client_id, login.operadora]);
    NSLog(@"%@", [LoginManager isAuthenticated] ? @"logado" : @"não logado");
    NSLog(@"%@", [NSString stringWithFormat:@"%@|iOS %@", [UIDeviceHardware platformString], [UIDevice currentDevice].systemVersion]);
#endif
    
    [tracker set:[GAIFields customDimensionForIndex:1] value:login.operadora];
    [tracker set:[GAIFields customDimensionForIndex:2] value:[NSString stringWithFormat:@"%@_%@", login.client_id, login.operadora]];
    [tracker set:[GAIFields customDimensionForIndex:3] value:([LoginManager isAuthenticated] ? @"logado" : @"não logado")];
    [tracker set:[GAIFields customDimensionForIndex:4] value:[NSString stringWithFormat:@"%@|iOS %@", [UIDeviceHardware platformString], [UIDevice currentDevice].systemVersion]];
}

- (NSString *) parseKeyWithFormatKey: (NSString *)key andParams:(NSArray *)params {
    NSRange range = NSMakeRange(0, [params count]);
    NSMutableData* data = [NSMutableData dataWithLength: sizeof(id) * [params count]];
    
    [params getObjects:(__unsafe_unretained id *)data.mutableBytes range:range];
    NSString *formatedKey = [[NSString alloc] initWithFormat:key arguments:data.mutableBytes];
    
    return formatedKey;
}
@end
