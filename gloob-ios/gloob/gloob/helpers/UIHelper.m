//
//  UIHelper.m
//  gloob
//
//  Created by zeroum on 07/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import "UIHelper.h"

@implementation UIHelper

+ (NSString *)deviceViewName:(NSString *)viewName {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return [NSString stringWithFormat:@"%@SD", viewName];
    }
    
    return viewName;
}

+ (void)alertViewWithMessage:(NSString *)message andTitle:(NSString *)title {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}

+ (CGSize)screenSize {
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) &&
        UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        
        return CGSizeMake(screenSize.height, screenSize.width);
        
    } else {
        
        return screenSize;
    }
}

+ (BOOL)isIPhone5 {
    
    if ([UIHelper screenSize].width == 568.0f) {
        return YES;
    }
    
    return NO;
}

@end
