//
//  UIHelper.h
//  gloob
//
//  Created by zeroum on 07/01/15.
//  Copyright (c) 2015 ZeroUm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIHelper : NSObject

+ (NSString *)deviceViewName:(NSString *)viewName;
+ (void)alertViewWithMessage:(NSString *)message andTitle:(NSString *)title;
+ (CGSize)screenSize;

+ (BOOL)isIPhone5;

@end
